<?php
	require_once("../config.php");
	require_once("db.php");
	require_once("../lumonata-admin/functions/globalAdmin.php");
	
	$ga = new globalAdmin();
	$db = new db();

	if(!empty($_POST['act']) && $_POST['act']=='newsletterSubmit'){
		//echo $_POST['emailAddress'];
		$str = "SELECT * FROM lumonata_newsletter_member WHERE lemail=%s AND lsubscribe=%d";
		$sql = $db->prepare_query($str, $_POST['email'],1);
		$que = $db->query($sql);
		$nnn = $db->num_rows($que);
		if($nnn<1){
			$str = "INSERT INTO lumonata_newsletter_member (
					lmember_id,lcat_id,lname,
					lemail,lstatus,lsubscribe) VALUES (
					%d, %d,%s,
					%s,%d,%d)";
			$sql = $db->prepare_query($str,'',1,$_POST['fname'],
							$_POST['email'],1,1);
			if($que = $db->query($sql)){
				echo 'success';
			}else{
				echo 'failed';
			}
		}else{
			echo 'exist';
		}
	}
?>