<?
define ("CARD_TYPE_MC", 0);
define ("CARD_TYPE_VS", 1);
define ("CARD_TYPE_AX", 2);
define ("CARD_TYPE_DC", 3);
define ("CARD_TYPE_DS", 4);
define ("CARD_TYPE_JC", 5);

	class cCreditCard{
		//clas user
		var $_ccName = '';  
		var $_ccType = '';  
		var $_ccNum = '';  
		var $_cExpM = 0;  
		var $_ccExpY = 0;
		
		// constructor
		function cCreditCard($name, $type, $num, $expm, $expy)
		{
			//set variabel
			if (!empty($name)){
				$this->_ccName = $name;
			}
			else{
				//die('Must pass name to constructor');
			}
		
		
			// Make sure card type is valid
			switch(strtolower($type))
			{
				
				case 'mc':
				case 'master card':
				case 'm':
				case '1':
				$this->_ccType = CARD_TYPE_MC;
				
				break;
				case 'vs':
				case 'visa card':
				case 'v':
				case '2':
				$this->_ccType = CARD_TYPE_VS;
				
				break;
				case 'ax':
				case 'american express':
				case 'a':
				case '3':
				$this->_ccType = CARD_TYPE_AX;
				
				break;
				case 'dc':
				case 'diners club':
				case '4':
				$this->_ccType = CARD_TYPE_DC;
				
				break;
				case 'ds':
				case 'discover':
				case '5':
				$this->_ccType = CARD_TYPE_DS;
				
				break;
				case 'jc':
				case 'jcb':
				case '6':
				$this->_ccType = CARD_TYPE_JC;
				
				break;
				default:
				//die('Invalid type ' . $type . ' passed to constructor');
				//$t->set_var('error', "<div class=\"error\"><b>Failed!</b><br />Invalid card type</div>");
			}
		
			// Don't check the number yet,
			// just kill all non numerics
			if(!empty($num)){
				//cardNumber = ereg_replace("[^0-9]", "", $num);
				$cardNumber = preg_replace("/[^0-9]/", "", $num);
				
				// Make sure the card number isnt empty
				if(!empty($cardNumber)){
					$this->_ccNum = $cardNumber;
				}
				else{
					//die('Must pass number to constructor');
				}
			}
			else{
				//die('Must pass number to constructor');
			}
			
			//get the curent month
			if (!is_numeric($expm) or $expm < 1 or $expm > 12){
				//die ('Invalid expiry monht of '.$expm.' passed to constructor');
			}
			else{
				$this->_cExpM =	 $expm;
			}
			
			//get the curent year
			$currentYear = date('Y');
			settype($currentYear, 'integer');
			
			if (!is_numeric($expy) or $expy < $currentYear or $expy > $currentYear + 10){
				//die ('invalid ecpiy year of '.$expy.'passed to constructor');	
			}else{
				$this->_ccExpY = $expy;
			}
		
		}
		
		function Name()
		{
			return $this->_ccName;
		}
		
		function Type()
		{
			switch($this->_ccType)
			{
			case CARD_TYPE_MC:
			return 'Master Card [1]';
			break;
			case CARD_TYPE_VS:
			return 'Visa Card [2]';
			break;
			case CARD_TYPE_AX:
			return 'Amex [3]';
			break;
			case CARD_TYPE_DC:
			return 'Diners Club [4]';
			break;
			case CARD_TYPE_DS:
			return 'Discover [5]';
			break;
			case CARD_TYPE_JC:
			return 'JCB [6]';
			break;
			default:
			return 'Unknown [-1]';
			}
		}
		
		function Number()
		{
			return $this->_ccNum;
		}
		
		function ExpiryMonth()
		{
			return $this->_ccExpM;
		}
		
		function ExpiryYear()
		{
			return $this->_ccExpY;
		}
		
		
		function SafeNumber($char = 'x', $numToHide = 4)
		{
			// Return only part of the number
			if($numToHide < 4){
				$numToHide = 4;
			}
			
			if($numToHide > 10){
				$numToHide = 10;
			}
			
			$cardNumber = $this->_ccNum;
			$cardNumber = substr($cardNumber, 0, strlen($cardNumber) - $numToHide);
			
			for($i = 0; $i < $numToHide; $i++){
				$cardNumber .= $char;
			}
		
		return $cardNumber;
		}
		
		function IsValid()
		{
		// Not valid by default
		$validFormat = false;
		$passCheck = false;
		$this->_ccType;
		$this->_ccNum;
			// Is the number in the correct format?
			switch($this->_ccType)
			{
			case CARD_TYPE_MC:
			$validFormat = preg_match("/^5[1-5][0-9]{14}$/", $this->_ccNum);
			break;
			case CARD_TYPE_VS:
			$validFormat = preg_match("/^4[0-9]{12}([0-9]{3})?$/", $this->_ccNum);
			break;
			case CARD_TYPE_AX:
			$validFormat = preg_match("/^3[47][0-9]{13}$/", $this->_ccNum);
			break;
			case CARD_TYPE_DC:
			$validFormat = preg_match("/^3(0[0-5]|[68][0-9])[0-9]{11}$/", $this->_ccNum);
			break;
			case CARD_TYPE_DS:
			$validFormat = preg_match("/^6011[0-9]{12}$/", $this->_ccNum);
			break;
			case CARD_TYPE_JC:
			$validFormat = preg_match("/^(3[0-9]{4}|2131|1800)[0-9]{11}$/", $this->_ccNum);
			break;
			default:
			// Should never be executed
			$validFormat = false;
			}
		
			// Is the number valid?
			$cardNumber = strrev($this->_ccNum);
			$numSum = 0;
			
			for($i = 0; $i < strlen($cardNumber); $i++){
				$currentNum = substr($cardNumber, $i, 1);
				
				// Double every second digit
				if($i % 2 == 1){
					$currentNum *= 2;
				}
				
				// Add digits of 2-digit numbers together
				if($currentNum > 9){
					$firstNum = $currentNum % 10;
					$secondNum = ($currentNum - $firstNum) / 10;
					$currentNum = $firstNum + $secondNum;
				}
				
				$numSum += $currentNum;
			}
			
			// If the total has no remainder it's OK
			$passCheck = ($numSum % 10 == 0);
			if($validFormat && $passCheck){
				return true;
			}else{
				return false;
			}
			
		}
		 
		
	}
?>