<?php
	require_once("../config.php");
	require_once("db.php");
	require_once("../lumonata-admin/functions/globalAdmin.php");
	
	$ga = new globalAdmin();
	$db = new db();

	if(!empty($_POST['act']) && $_POST['act']=='availibilityCheck'){
		$continue = true;
		$the24 = 24*60*60;
		$i = strtotime($_POST['cIn']);
		$dateRange = 0;
		while($continue){
			$nDate = strtotime(date('m/d/y', $i));
			$i = $nDate;
			
			$dateRange++;
			$i+=$the24;
			if($i>strtotime($_POST['cOut'])){ $continue=false; }
		}//endWhile
						
		//print_r($_POST);
		$str = "SELECT * FROM lumonata_availability WHERE lacco_type_id=%d AND ldate>=%d AND ldate<=%d ORDER BY ldate ASC";
		$sql = $db->prepare_query($str,$_POST['typID'],strtotime($_POST['cIn']),strtotime($_POST['cOut']));
		$res = $db->query($sql);
		$nnn = $db->num_rows($res);
		$available = true;
		$eType     = '';
		$eDetails  = '';
		$theRate   = 0;
		$iii       = 1;
		
		if($nnn<$dateRange){ $available = false; $eType[] = 'dateNA'; $eDetails['dateNA'] = 'not available at selected date range'; }
		while($rec = $db->fetch_array($res)){
			if($available){
				if( $rec['lrate']<=0 || empty($rec['lrate']) ){ $available = false; $eType[] = 'emptyPrice'; $eDetails['emptyPrice'] = 'not available for booking at selected date range'; }
				if( $rec['lstatus']==2 ){ $available = false; $eType[] = 'lstatus'; $eDetails['lstatus'] = 'Booked'; }
				if( (($rec['lcut_of_date']*24*60*60)+time())>strtotime($_POST['cIn'] ) && $iii==1 ){ $available = false; $eType[] = 'cutoffDate'; $eDetails['cutoffDate'] = 'Min. booking date : '.date('d M Y', ((($rec['lcut_of_date']+1)*24*60*60)+time()));}
				if( $rec['lallotment']<$_POST['nRoom'] ){ $available = false; $eType[] = 'noAllotment'; $eDetails['noAllotment'] = 'insuficient room' ;  }
			}
			
			
			if( ( strtotime($_POST['cIn'])==$rec['ldate'] || strtotime($_POST['cOut'])==$rec['ldate'] ) && $rec['lstatus']==1 ){
				$available = false; $eType[] = 'no-in-out'; $eDetails['no-in-out'] = 'No check in/out : '.$rec['lreason'];
			}
			if( strtotime($_POST['cIn'])==$rec['ldate'] && $rec['lstatus']==3 ){
				$available = false; $eType[] = 'no-in'; $eDetails['no-in'] = 'No check in : '.$rec['lreason'];
			}
			if( strtotime($_POST['cOut'])==$rec['ldate'] && $rec['lstatus']==4 ){
				$available = false; $eType[] = 'no-out'; $eDetails['no-out'] = 'No check out : '.$rec['lreason'];
			}
			
			
			if(strtotime($_POST['cOut'])!=$rec['ldate']){ $theRate += $rec['lrate']; }
			$iii++;
		}
		
		if($available){
			$all['status'] = 'available';
			$all['rate']   = $theRate;
			$all['earlybird']  = geEarlybirdPromo();
			$all['discPromo']  = getDiscountPromo();
			$all['surecharge'] = getSurecharge();
			
			echo json_encode($all);
		}else{
			$all['status']  = 'notavailable';
			$all['reasons'] = $eType;
			$all['details'] = $eDetails;
			
			echo json_encode($all);
		}
	}
	
	function getDiscountPromo(){
		$db  = new db();
		$str = "SELECT * FROM lumonata_accommodation_promo WHERE lpromo_type=%s AND lacco_type_id=%d AND ldate_from<=%d AND ldate_to>=%d";
		$sql = $db->prepare_query( $str, 'disc_promo', $_POST['typID'], strtotime($_POST['cIn']), strtotime($_POST['cOut']) );
		$res = $db->query($sql);
		$nnn = $db->num_rows($res);
		$notfound = true;
		if($nnn>0){
			while($rec = $db->fetch_array($res)){
				if($notfound){
					$itsOk = '';
					$data  = '';
					if( $rec['lstay']>0 && $rec['lstay_to']>0 ){
						if($_POST['nNight']>=$rec['lstay'] && $_POST['nNight']<=$rec['lstay_to']){
							$itsOk[0] = true;
						}
					}else if( $rec['lstay']>0 ){
						if($_POST['nNight']>=$rec['lstay']){
							$itsOk[0] = true;
						}
					}
					if($_POST['nRoom']>=$rec['lroom']){
						$itsOk[1] = true;
					}
					
					//-------------------------------------------------------------------------------
					if($itsOk[0] && $itsOk[1]){
						$data['ammout'] = $rec['lammount'];
						$data['unit']   = $rec['lammount_unit'];
					}
				}
			}
			if(empty($data)){
				return '0';
			}else{
				return $data;
			}
		}else{
			return '0';
		}
	}
	
	function geEarlybirdPromo(){
		$db  = new db();
		$str = "SELECT * FROM lumonata_accommodation_promo WHERE lpromo_type=%s AND lacco_type_id=%d AND ldate_from<=%d AND ldate_to>=%d";
		$sql = $db->prepare_query( $str, 'early_bird', $_POST['typID'], strtotime($_POST['cIn']), strtotime($_POST['cOut']) );
		$res = $db->query($sql);
		$nnn = $db->num_rows($res);
		$notfound = true;
		if($nnn>0){
			while($rec = $db->fetch_array($res)){
				if($notfound){
					$itsOk = '';
					$data  = '';
					if( $rec['lstay']>0 && $rec['lstay_to']>0 ){
						if($_POST['nNight']>=$rec['lstay'] && $_POST['nNight']<=$rec['lstay_to']){
							$itsOk[0] = true;
						}
					}else if( $rec['lstay']>0 ){
						if($_POST['nNight']>=$rec['lstay']){
							$itsOk[0] = true;
						}
					}
					if($_POST['nRoom']>=$rec['lroom']){
						$itsOk[1] = true;
					}
					
					$target = strtotime($_POST['cIn']) - ($rec['lday_before']*24*60*60);
					if( time()<=$target ){
						$itsOk[2] = true;
					}
					$itsOk[2] = true;
					
					//-------------------------------------------------------------------------------
					if($itsOk[0] && $itsOk[1] && $itsOk[2]){
						$data['ammout'] = $rec['lammount'];
						$data['unit']   = $rec['lammount_unit'];
						$data['debug1']  = $target;
						$data['debug2']  = time();
					}
				}
			}
			if(empty($data)){
				return '0';
			}else{
				return $data;
			}
		}else{
			return '0';
		}
	}
	
	function getSurecharge(){
		$db    = new db();
		$day[] = array('day' => 'mon', 'nnn'=>1); 
		$day[] = array('day' => 'tue', 'nnn'=>2);  
		$day[] = array('day' => 'wen', 'nnn'=>3);  
		$day[] = array('day' => 'thu', 'nnn'=>4);  
		$day[] = array('day' => 'fri', 'nnn'=>5);  
		$day[] = array('day' => 'sat', 'nnn'=>6);  
		$day[] = array('day' => 'sun', 'nnn'=>7); 
		
		$str = "SELECT * FROM lumonata_accommodation_surecharge LEFT JOIN lumonata_accommodation_surecharge_description
					ON lumonata_accommodation_surecharge.ldescription=lumonata_accommodation_surecharge_description.lsurecharge_desc_ID		
				WHERE lacco_type_id=%d AND 
					( (ldate_from<=%d AND ldate_to>=%d) OR (ldate_from>=%d AND ldate_to<=%d) OR (ldate_from>=%d AND ldate_to>=%d AND ldate_from<%d) OR (ldate_from<=%d AND ldate_to<=%d AND ldate_to>%d) )";
		$in  = strtotime($_POST['cIn']);
		$out = strtotime($_POST['cOut']);
		$sql = $db->prepare_query( $str, $_POST['typID'], $in, $out, $in, $out, $in, $out, $out, $in, $out, $in );
		$res = $db->query($sql);
		$nnn = $db->num_rows($res);
		$notfound = true;
		if($nnn>0){
			$data    = '';
			$theSure = 0;
			while($rec = $db->fetch_array($res)){
				$compVal = json_decode($rec['lday_of_week']);
				$selDays = '';
				foreach($day as $tmp){
					foreach($compVal as $val){
						if($tmp['day']==$val){
							$selDays[] = $tmp;
						}
					}
				}
					
				if($rec['lperNight']==1){ //is per night BEGIN
					if( $rec['ldate_from']<=$in && $rec['ldate_to']>=$in ){ $dBegin = $in; $dEnd = $out; }
					if( $rec['ldate_from']>=$in && $rec['ldate_to']<=$in ){ $dBegin = $rec['ldate_from']; $dEnd = $rec['ldate_to']; }
					if( $rec['ldate_from']>=$in && $rec['ldate_to']>=$in ){ $dBegin = $rec['ldate_from']; $dEnd = $out; }
					if( $rec['ldate_from']<=$in && $rec['ldate_to']<=$in ){ $dBegin = $in; $dEnd = $rec['ldate_to']; }	
					
					for($iii=$dBegin;$iii<=$dEnd;$iii+=(24*60*60)){
						foreach($selDays as $tmp){
							if(date('N',$iii)==$tmp['nnn'] && $out!=$iii){
								$nPerson = 0;
								if($rec['lperAdult']==1){ $nPerson+=$_POST['nAdult']; }
								if($rec['lperChild']==1){ $nPerson+=$_POST['nChild']; }
								if($nPerson==0){ $nPerson=1; }
								
								$theSure += $rec['lcharge']*$nPerson;
							}
						}
					}
				}//is per night END
				
				if($rec['lperBooking']==1){ //is per booking BEGIN
					$nPerson = 0;
					if($rec['lperAdult']==1){ $nPerson+=$_POST['adult']; }
					if($rec['lperChild']==1){ $nPerson+=$_POST['child']; }
					if($nPerson==0){ $nPerson=1; }
					
					$theSure += $rec['lcharge']*$nPerson;
				}//is per booking END
				
				if( $rec['lperNight']!=1 && $rec['lperBooking']!=1 && ($rec['lperAdult']==1 || $rec['lperChild']==1)){
					$nPerson = 0;
					if($rec['lperAdult']==1){ $nPerson+=$_POST['adult']; }
					if($rec['lperChild']==1){ $nPerson+=$_POST['child']; }
					if($nPerson==0){ $nPerson=1; }
					
					$theSure += $rec['lcharge']*$nPerson;
				}
				
			}//end WHILE
			$data['ammout'] = $theSure;
			$data['debug1'] = $sql;
			return $data;
		}else{
			$data['ammout'] = 0;
			$data['debug1'] = 'no data found';
			return $data;
		}
	}
?>