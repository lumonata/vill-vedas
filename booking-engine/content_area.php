<?php

if(!empty($_GET['act']) && is_dir(SUPPLIERPANEL_APP_PATH."/".$_GET['act'])){
	if(isset($_COOKIE['member_logged_ID'])){
		$className=$searchDir->getClassName("apps/".$_GET['act']);
		require_once("apps/".$_GET['act']."/class.".$className.".php");
		
		$objectClass = new $className($_GET['act']);
		
		$content=$objectClass->load();
		$t->set_var('content_area', $content);
		
		$globalSetting->setMetaTitle($objectClass->getMetaTitle());
		$META_TITLE=$globalSetting->getMetaTitle();
		
		$globalSetting->setMetaDescriptions($objectClass->getMetaDescriptions());
		$META_DESC=$globalSetting->getMetaDescriptions();
		
		$globalSetting->setMetaKeywords($objectClass->getMetaKeywords());
		$META_KEY=$globalSetting->getMetaKeywords();

		if(!empty($_COOKIE['state']) and !empty($_COOKIE['sub'])){
			//$t->set_var('admin_link', "<a href=\"http://".SITE_URL."/lumonata-admin/home.php?mod=".$_COOKIE['modul']."&prc=view&d=".$_COOKIE['product_type']."\" class=\"adminLink\">Back to Administrator Pages</a>");
			$t->set_var('admin_link', "<a href=\"http://".SITE_URL."/lumonata-admin/?state=".$_COOKIE['state']."&sub=".$_COOKIE['sub']."\" class=\"adminLink\">BACK TO ADMIN</a>");
		}			
	}else{
		header("location:http://".SSITE_URL."/member/login/");
		header("location:http://".SSITE_URL."/");
	}
	
}elseif(isset($_REQUEST['error'])){
	require("error_page.php");
	// begin the output buffer to send headers and resonse
	ob_start();
	@header("HTTP/1.1 $AA_STATUS_CODE $AA_REASON_PHRASE",1);
	@header("Status: $AA_STATUS_CODE $AA_REASON_PHRASE",1);
	if(!aa_print_html($AA_STATUS_CODE)){
		$t->set_var('content_area',"<h1>$AA_STATUS_CODE $AA_REASON_PHRASE</h1><h3>$AA_MESSAGE</h3>");
	}else{
		exit;
	}
}else if(!empty($_POST['act']) && $_POST['act']!='calendar'){//for ajax event with post
	if(isset($_COOKIE['member_logged_ID'])){
		$className=$searchDir->getClassName("apps/".$_POST['act']);
		require_once("apps/".$_POST['act']."/class.".$className.".php");
		$objectClass = new $className($_POST['act']);
		$objectClass->load();
	}else{
		echo 'you not logged yet.';
	}	
}elseif(empty($_GET['act']) /*&& empty($_POST['act'])*/){
	//echo 'dd';
	if(isset($_COOKIE['member_logged_ID'])){
		$className=$searchDir->getClassName("apps/allotment");
		require_once("apps/allotment/class.".$className.".php");
		$objectClass = new $className('allotment');
		$content=$objectClass->load();
		$t->set_var('content_area', $content);
		$globalSetting->setMetaTitle($objectClass->getMetaTitle());
		$META_TITLE=$globalSetting->getMetaTitle();
		
		$globalSetting->setMetaDescriptions($objectClass->getMetaDescriptions());
		$META_DESC=$globalSetting->getMetaDescriptions();
		
		$globalSetting->setMetaKeywords($objectClass->getMetaKeywords());
		$META_KEY=$globalSetting->getMetaKeywords();
		
		//if(!empty($_COOKIE['modul'])){
			$t->set_var('admin_link', "<a href=\"http://".SITE_URL."/lumonata-admin/\" class=\"adminLink\">BACK TO ADMIN</a>");
		//}
	}else{
		echo 'e2';
		header("location:http://".SSITE_URL."/member/login/");
		header("location:http://".SSITE_URL."/");
	}
}
?>