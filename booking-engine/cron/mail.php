<?php
class imap {	
	function imap($server){
		switch($server){
			case 'smtp.gmail.com':
				$this->server="{pop.gmail.com:995/pop3/ssl/novalidate-cert}$folder";
				break;
			case 'imap.gmail.com':		
				$this->server="{imap.gmail.com:993/imap/ssl}$folder";
				break;
			default:
				$this->server=$server;
		}
		
		
	}
	
	function imap_connect($email,$password){
		$result= imap_open($this->server, $email, $password)or die("Error");
		return $result;
	}
	function count_inbox($connection){
		$count= imap_num_msg($connection);
		return $count;
	}
	/**
	 * $date should be a string
	 * Example Formats Include:
	 * Fri, 5 Sep 2008 9:00:00
	 * Fri, 5 Sep 2008
	 * 5 Sep 2008
	 * I am sure other's work, just test them out.
	 */
	function getHeadersSince($connection,$date)
	{
	 $uids = $this->getMessageIdsSinceDate($connection,$date);
	 $messages = array();
	 foreach( $uids as $k=>$uid )
	 {
	  $messages[] = $this->retrieve_header($connection,$uid);
	 }
	 return $messages;
	}

	/**
	 * $date should be a string
	 * Example Formats Include:
	 * Fri, 5 Sep 2008 9:00:00
	 * Fri, 5 Sep 2008
	 * 5 Sep 2008
	 * I am sure other's work, just test them out.
	 */
	function getEmailSince($connection,$date)
	{
	 $uids = $this->getMessageIdsSinceDate($connection,$date);
	 $messages = array();
	 
	 if(count($uids)<=1){
	 	return $messages; 
	 }
	 
	 foreach($uids as $k=>$uid)
	 {
	  $messages[] = $this->retrieve_message($connection,$uid);
	 }
	 return $messages;
	}
	
	function getMessageIdsSinceDate($connection,$date)
	{
	 return imap_search($connection, 'SINCE "'.$date.'"');
	}
	
	function retrieve_header($connection,$messageid)
	{
	   $message = array();
	
	   $header = imap_header($connection, $messageid);
	   $structure = imap_fetchstructure($connection, $messageid);
	
	   $message['subject'] = $header->subject;
	   $message['fromaddress'] =   $header->fromaddress;
	   $message['toaddress'] =   $header->toaddress;
	   $message['ccaddress'] =   $header->ccaddress;
	   $message['date'] =   $header->date;
	
	   return $message;
	}

	function show_mailbox_list($connection){
		
		$mailboxes = imap_list($connection, $this->server, '*');
		foreach($mailboxes as $mailbox) {
			$shortname.= str_replace($server,'', $mailbox)."<br />";
		}
		return $shortname;
	}
	function change_folder($connection,$folder){
		$ropen= imap_reopen($connection, $this->server.$folder);	
	}
	
	function retrieve_message($mbox, $messageid)
	{
	   $message = array();	   
	   $header = imap_header($mbox, $messageid);
	   $structure = imap_fetchstructure($mbox, $messageid);
	   $message['subject'] = $header->subject;
	   $message['fromaddress'] =   $header->fromaddress;
	   $from =$header->from[0];
	   $message['personal']=$from->personal;
	   $message['from_email']=$from->mailbox.'@'.$from->host;
	   $message['toaddress'] =   $header->toaddress;
	   $message['ccaddress'] =   $header->ccaddress;
	   $message['date'] =   $header->date;
	   //$message['file']= $this->write_attachments_to_disk($mbox,$messageid,'attachment');	
	  if ($this->check_type($structure))
	  {
	   $message['body'] = imap_fetchbody($mbox,$messageid,"1"); ## GET THE BODY OF MULTI-PART MESSAGE

	   if(!$message['body']) {$message['body'] = '[NO TEXT ENTERED INTO THE MESSAGE]\n\n';}
	  }
	  else
	  {
	   $message['body'] = imap_body($mbox, $messageid);
	   if(!$message['body']) {$message['body'] = '[NO TEXT ENTERED INTO THE MESSAGE]\n\n';}
	  }
	 
	  return $message;
	}
	
	function check_type($structure) ## CHECK THE TYPE
	{
	  if($structure->type == 1)
		{
		 return(true); ## YES THIS IS A MULTI-PART MESSAGE
		}
	 else
		{
		 return(false); ## NO THIS IS NOT A MULTI-PART MESSAGE
		}
	}
	
	function write_attachments_to_disk($mailbox, $msg_number, $dir){
 
	  if (!file_exists($dir)){
		mkdir($dir);
	  }
	  $filename = "tmp.eml";
	  $email_file = $dir."/".$filename;
	  // write the message body to disk
	  imap_savebody  ($mailbox, $email_file, $msg_number);
	  $command = "munpack -C $dir -fq $email_file";
	  // invoke munpack which will
	  // write all the attachments to $dir
	  exec($command,$output);
	
	  // if($output[0]!='Did not find anything to unpack from $filename') {
	  $found_file = false;
	  foreach ($output as $attach) {
		$pieces = explode(" ", $attach);
		$part = $pieces[0];
		if (file_exists($dir.$part)){
		  $found_file = true;
		  $files[] = $part;
		}
	  }
	  if (!$found_file){
		//echo ("\nMail.php : no files found - cleaning up. ");
		// didn't find any output files - delete the directory and email file
		unlink($email_file);
		rmdir($dir);
		return false;
	  }
	  else {
		// found some files-  just delete the email file
		unlink($email_file);
		return $files;
	  }
	}
}	
require_once("../../config.php");
require_once("../../functions/db.php");

function saveProses($ticket_id,$email_body,$date,$email_address,$name){

	global $db;
	if($email_address=="support@localhost")
		$status=2;
	else
		$status=1;
		
	$sqlUser=$db->prepare_query("select * from lumonata_clients where lemail=%s",$email_address);
	$queryUser=$db->query($sqlUser);
	$dU=$db->fetch_array($queryUser);
	
	//jika email yang di pakai tidak valid atau bukan email dari support, maka tidak di simpan ke database
	if($db->num_rows($queryUser) > 0 && $email_address!="support@localhost")
		$do_save=true;
	elseif($db->num_rows($queryUser) == 0 && $email_address!="support@localhost")
		$do_save=false;
	elseif($db->num_rows($queryUser) == 0 && $email_address=="support@localhost")
		$do_save=true;
		
	
	if($do_save){ 
		$sql=$db->prepare_query("insert into  lumonata_tiket_detail(
									ltiket_id,
									lmessage,
									ldlu,
									lemail,
									lname
									)value(%s,%s,%d,%s,%s)",
									$ticket_id,
									$email_body,
									strtotime($date),
									$email_address,
									$name
									);
		$result=$db->query($sql);
		
		$sql2=$db->prepare_query("update lumonata_tiket SET
									lstatus=%d,
									ldlu=%d
									where ltiket_id=%s",
									$status,
									strtotime($date),
									$ticket_id);
									
		$query2=$db->query($sql2);
	
								
		if($result && $query2){
			return true;
		}else {
			
			return false;
		}
	}
													
}

function is_duplicate_email($email_body,$ticket_id){
	global $db;
	$sql=$db->prepare_query("select * from lumonata_tiket_detail where ltiket_id=%s",$ticket_id);
	$r=$db->query($sql);
	while($data=$db->fetch_array($r)){
		if(trim($data['lmessage'])==trim($email_body)){
			return true;
		}
	}
}

function is_ticket_exists($ticket_id){
	global $db;
	$sql=$db->prepare_query("select * from lumonata_tiket where ltiket_id=%s",$ticket_id);
	$r=$db->query($sql);
	if($db->num_rows($r)>0){
		return true;
	}else{
		return false;
	}
}

/*$email='support@mongkiki.com';
$password='pass123support';
$imap=new imap('imap.gmail.com');
*/
$email='support@localhost';
$password='123456';
$imap=new imap('{localhost}');
$email_key="MONGKITIKET";

$tag_allow="<br><a><p>";
$date=date("j M Y H:m:s",mktime(date("H",time()),date("m",time()),date("s",time()),date("m",time()),date("d",time())-2,date("Y",time())));

$connection=$imap->imap_connect($email,$password);

/*UNTUK MENGAMBIL INFORMASI HEADER DARI EMAIL*/
/*
$header=$imap->getHeadersSince($connection,$date);
for($i =count($header); $i >=1; $i--) {
	echo $i.".".$header[$i]['fromaddress']."<br />";
}
*/

/*UNTUK MENGAMBIL EMAIL DARI WAKTU TERTENTU*/
$message=$imap->getEmailSince($connection,$date);
for($i =count($message); $i >=1; $i--) {
	if(preg_match("/$email_key/i",$message[$i]['subject'])){
		preg_match('/(\w+)#(\w+)/',$message[$i]['subject'],$sbj);
		list($tiket_id,$subject)=explode(":",$message[$i]['subject']);
		if(is_ticket_exists($sbj[2])){
			if(!is_duplicate_email($message[$i]['body'],$sbj[2]))
				if(saveProses($sbj[2],$message[$i]['body'],$message[$i]['date'],$message[$i]['from_email'],$message[$i]['personal']))
					echo $sbj[2]."sukses <br />".$message[$i]['body'];
				else
					echo $sbj[2]."gagal simpan $message[$i][body]<br />";
			else
				echo $sbj[2]."gagal duplicate<br />";
		}else{
				echo $sbj[2].":ticket id si not exists<br />";
		}
	}
}


/* UNTUK MENGAMBIL SEMUA EMAIL YANG ADA DI SERVER */
/*
for($i=$imap->count_inbox($connection); $i >=1; $i--) {
	$message=$imap->retrieve_message($connection,$i);
	if(preg_match("/$email_key/i",$message['subject'])){
		preg_match('/(\w+)#(\w+)/',$message['subject'],$sbj);
		list($tiket_id,$subject)=explode(":",$message['subject']);
		
		if(!is_duplicate_email($message['body'],$sbj[2]))
			if(saveProses($sbj[2],$message['body'],$message['date'],$message['from_email']))
				echo $sbj[2]."sukses <br />";
			else
				echo $sbj[2]."gagal simpan $message[from_email]<br />";
		else
			echo $sbj[2]."gagal duplicate<br />";
				
		
		
	}
	
 }
*/	

imap_close($connection);
?>