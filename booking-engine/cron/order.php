<?php
	require_once("../../config.php");
	require_once("../../functions/db.php");
	
	function delete_files($order_id){
		global $db;
		//deleting files
		$sql="select * from lumonata_file where lorder_id='".$d['lorder_id']."'";
		$r=$db->query($sql);
		if($db->num_rows($r)>0){
			while($data=$db->fetch_array($r)){
				if(file_exists(IMAGE_PATH.'/Upload_file/'.$data['lfile1']))
					unlink(IMAGE_PATH.'/Upload_file/'.$data['lfile1']);
			}
			
			$sql="delete from lumonata_file where lorder_id='".$d['lorder_id']."'";
			$rdel=$db->query($sql);
		}
	}
	
	function next_queue($domain){
			global $db;
			$s=$db->prepare_query("select * from lumonata_order where ldomain_name=%s and lstatus = 'qq' order by ldate_order",$domain);
			$r=$db->query($s);
			$n=$db->num_rows($r);
			$d=$db->fetch_array($r);
			
			if ($n>0){
					
				//Check Domain Group
				$domain_name_array = explode('.', $d['ldomain_name']);
				if(count($domain_name_array)>2)
					$domain_tld=$domain_name_array[1].'.'.$domain_name_array[2];
				else
					$domain_tld=$domain_name_array[1];
				
				$sql="select * from lumonata_domain where ldomain_tld='$domain_tld'";
				$rd=$db->query($sql);
				$dd=$db->fetch_array($rd);
				
				if($dd['lgroup']==2){
					$status="pf";
				}else{
					$status="pp";
				}	
				
				$sql=$db->prepare_query("update lumonata_order set lstatus=%s,ldate_order=%d where lorder_id=%s",$status,time(),$d['lorder_id']);
				$update_r=$db->query($sql);
				
				if($update_r){
					$sql_client=$db->prepare_query("select * from lumonata_clients where lclient_id=%s",$d['lclient_id']);
					$r_client=$db->query($sql_client);
					$c=$db->fetch_array($r_client);
					$exp_date=mktime(0,0,0,date("m",time()),date("d",time())+7,date("Y",time()));
					alert_next_queue($c['lemail'],$c['lname'],$d['ldomain_name'],$d['lorder_id'],$status,$exp_date);
				
				}					
				
			}
				
	}
	
	function alert_next_queue($email,$name,$domain,$order_id,$status,$exp_date){
		$to=$email;
		$cc="";
		$bcc="";
		
		if($status=='pf'){
			$aksi="Silahkan upload dokumen - dokumen yang diperlukan dengan cara login ke Mongkipanel sebelum jangka waktu jatuh tempo.\n";
		}else{
			$aksi="Silahkan lakukan pembayaran ke salah satu rekening bank kami, kemudian jangan lupa untuk 
			mengkonfirmasi pembayaran tersebut dengan login terlebih dahulu ke Mongkipanel sebelum jangka waktu jatuh tempo.\n";
		}
			
		$message="
			Dear, $name\n\n
			Selamat Order Anda di Mongkiki.com saat ini sudah aktif. $aksi \n
			Berikut adalah detail Order Anda: \n\n
			
			Order ID: $order_id \n
			Domain:$domain \n
			Jatuh Tempo:". date("d F Y",$exp_date)." \n\n
			
			Best Regards\n\n
			
			
			Mongkiki Sales Team\n
			http://www.mongkiki.com\n
			sales@mongkiki.com\n";	

		ini_set("SMTP",SMTP_SERVER);
		ini_set("sendmail_from","automail@mongkiki.com");
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
		// Additional headers
		$subject = "Selamat Order Anda Mongkiki.com ($order_id / $domain) telah aktif";
		//$from = $email;
					
		$headers .= "From:  Mongkiki Team <automail@mongkiki.com>\r\nReply-To :'automail@mongkiki.com'\r\n";
		$headers .= "Cc: ".$cc."\r\n";
		$headers .= "Bcc: ".$bcc."\r\n";
		$send=mail("$to","$subject","$message","$headers");
	
	
		
		if($send)return TRUE;
		else return FALSE;
	}
	
	function delete_pp_order(){
		global $db;
		$sql="SELECT a.lorder_id,a.ldate_order,a.ldomain_name,a.lpackage_type,a.ldomain_price,a.lhosting_price,a.ltotal_diskon,b.lname,b.lemail
			  FROM lumonata_order a, lumonata_clients b 
			  WHERE (a.lstatus='pp' || a.lstatus='pf') and a.lclient_id=b.lclient_id";
			  
		$r=$db->query($sql);
		while($d=$db->fetch_array($r)){
			$date_to_exp=mktime(0,0,0,date("m",$d['ldate_order']),date("d",$d['ldate_order'])+7,date("Y",$d['ldate_order']));
			if($date_to_exp < time()){
			
				$sql="delete from lumonata_order where lorder_id='".$d['lorder_id']."'";
				$rdel=$db->query($sql);
				
				//hapus file
				delete_files($d['lorder_id']);
				
				if($rdel) {
					$subject = "Order Anda di Mongkiki telah terhapus#$d[lorder_id]($d[ldomain_name])";
					$message="
					<p>Dear $d[lname],</p>
					<p>Order Anda di Mongkiki dengan Order ID: #$d[lorder_id]($d[ldomain_name]) telah terhapus karena telah melewati masa tenggang. 
					Silahkan lakukan order ulang untuk mendaftarkan spesifikasi order yang sama</p>
					<p>
						Best Regards,<br /><br /><br /><br />
						Mongkiki Team
					</p>
					";
					
					send_alert($d['lemail'],$message,$subject);
					
					next_queue($d['ldomain_name']);// open for next queue
				}
				
			}else{
				$days3_to_exp=mktime(0,0,0,date("m",$date_to_exp),date("d",$date_to_exp)-3,date("Y",$date_to_exp));	
				$days2_to_exp=mktime(0,0,0,date("m",$date_to_exp),date("d",$date_to_exp)-2,date("Y",$date_to_exp));	
				$days1_to_exp=mktime(0,0,0,date("m",$date_to_exp),date("d",$date_to_exp)-1,date("Y",$date_to_exp));	
				
				
				switch($d['lpackage_type']){
					case 101:
						$order_type="New Domain";
						break;
					case 102:
						$order_type="New Domain &amp; Hosting";
						break;
					case 103:
						$order_type="New Hosting";
						break;
					case 104:
						$order_type="Transfer Domain";
						break;
					case 105:
						$order_type="Transfer Domain &amp; Hosting";
						break;
					case 106:
						$order_type="Domain Forwarding";
						break;
					case 107:
						$order_type="Email Hosting";
						break;
					case 108:
						$order_type="Renew Domain";
						break;
					case 109:
						$order_type="Renew Hosting";
						break;
					case 110:
						$order_type="Renew Domain &amp; Hosting";
						break;
					case 111:
						$order_type="Upgrade Hosting";
						break;
					case 112:
						$order_type="Upgrade Hosting";
						break;
				}
				$amount= number_format($d['ldomain_price']+$d['lhosting_price']-$d['ltotal_diskon']);
				
				$message="
				<p>Dear $d[lname],</p>
				
				<p>
					Order Anda di Mongkiki.com akan segera di hapus pada tanggal ". date("d F Y",$date_to_exp) .". 
					Silahkan lakukan proses pembayaran atau upload dokumen yang diperlukan sebelum tenggan waktu tersebut. 
					Jika Anda sudah membayar Order tersebut, kami harapkan Anda melakukan konfirmasi pembayaran segera. 
					Lakukan langakah - langkah berikut untuk melakukan konfirmasi pembayaran: <br />
					1. Login ke Mongkipanel dengan menggunakan username / email Anda <br />
					2. Pilih Menu Billing <br />
					3. Klik tombol Pay untuk mengkonfirmasi pembayaran terhadap order yang Anda lakukan.
				</p>
				
				<p>
					Berikut adalah detail order Anda:<br />
					<h1 style=\"font-size:20px;color:red;border-bottom:1px solid red;\">Detail Order</h1>
					Domain: $d[ldomain_name]<br />
					Order ID: $d[lorder_id]<br />
					Order Type: $order_type<br />
					Amount: $amount<br />
				</p>
				
				<p>
					<a href=\"http://".MONGKIPANEL_SITE_URL."/login/\">Klik disini</a> untuk login ke <strong>Mongkipanel</strong> dan melakukan konfirmasi pembayaran.
				</p>
				<p>
					Pembayaran dapat di lakukan dengan cara mentransfer ke rekening Bank yang kami miliki berikut:<br />
					<h1 style=\"font-size:20px;color:red;border-bottom:1px solid red;\">Bank Account</h1>
					<strong>BCA</strong><br />
					a.n : Simon Purwa <br />
					no. : 6 130 113 131 <br /><br />
					
					<strong>Mandiri</strong><br />
					a.n : I.G Agung Purwa Suajaya <br />
					no. : 131-00-0478514-5 <br />
				</p>
				
				<p>
					<strong>Support</strong><br>
					Untuk memperoleh layanan support, silahkan hubungi kami di:<br /><br /><br />
					
					<em>Technical Support:</em><br />
					Website: http://www.mongkiki.com<br />
					Email Address: <a href=\"mailto:support@mongkiki.com\">support@mongkiki.com</a><br />
					Tel: +62.361754926<br />
					Fax: +62.361751442<br /><br /><br />
					
					
					<em>Billing Contact:</em><br />
					Email Address: <a href=\"mailto:billing@mongkiki.com\">billing@mongkiki.com</a><br />
					Tel: +62.361754926<br />
					Fax: +62.361751442<br />
				</p>
				<p>
					Best Regards,<br /><br /><br /><br />
					Mongkiki Team
				</p>
				";	
				
				if(date("d m Y",$days3_to_exp) == date("d m Y",time())){
					$subject = "Order Anda di Mongkiki akan segera di hapus dalam waktu 3 hari - Mohon segera lakukan konfirmasi pembayaran.";
					send_alert($d['lemail'],$message,$subject );
				}elseif(date("d m Y",$days2_to_exp)==date("d m Y",time())){
					$subject = "Order Anda di Mongkiki akan segera di hapus dalam waktu 2 hari - Mohon segera lakukan konfirmasi pembayaran.";
					send_alert($d['lemail'],$message,$subject );
				}elseif(date("d m Y",$days1_to_exp)==date("d m Y",time())){
					$subject = "Order Anda di Mongkiki akan segera di hapus dalam waktu 1 hari - Mohon segera lakukan konfirmasi pembayaran.";
					send_alert($d['lemail'],$message,$subject );
				}elseif(date("d m Y",$date_to_exp)== date("d m Y",time())){
					$subject = "Order Anda di Mongkiki akan segera di hapus - Mohon segera lakukan konfirmasi pembayaran.";
					send_alert($d['lemail'],$message,$subject );
				}
				
				
				
				
			}
		}
	
	}
	function delete_exp_order(){
		global $db;	
		$sql="SELECT a.lorder_id,a.ldate_order,a.ldomain_name,a.lpackage_type,a.ldomain_price,a.lhosting_price,a.ltotal_diskon,b.lname,b.lemail,a.lexpired_contract
			  FROM lumonata_order a, lumonata_clients b 
			  WHERE a.lstatus='aa' and a.lclient_id=b.lclient_id";
		$r=$db->query($sql);
		while($d=$db->fetch_array($r)){
			$month1_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) - 1,date("d",$d['lexpired_contract']),date("Y",$d['lexpired_contract']));
			$day15_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])-15,date("Y",$d['lexpired_contract']));
			$day7_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])-7,date("Y",$d['lexpired_contract']));
			$day3_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])-3,date("Y",$d['lexpired_contract']));
			$day2_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])-2,date("Y",$d['lexpired_contract']));
			$day1_to_exp=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])-1,date("Y",$d['lexpired_contract']));
			
			$month1_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+10,date("Y",$d['lexpired_contract']));
			$day15_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+25,date("Y",$d['lexpired_contract']));
			$day7_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+33,date("Y",$d['lexpired_contract']));
			$day3_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+37,date("Y",$d['lexpired_contract']));
			$day2_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+38,date("Y",$d['lexpired_contract']));
			$day1_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+39,date("Y",$d['lexpired_contract']));
			$day_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+40,date("Y",$d['lexpired_contract']));
			
			$message_exp="<p>Dear $d[lname],</p>
			<p>Email ini untuk informasi bahwa account anda di Mongkiki.com harus segera di perpanjang.</p>
			<p>Jika account Anda sudah melawati batas expired, maka Account Anda akan dinonaktifkan dan selanjutnya dihapus dari Mongkiki.com server. 
			Jadi mohon diperhatikan dan diperpanjang account Anda dibawah, untuk memastikan account Anda berfungsi normal. Berikut adalah detail order Anda:</p>";
			
			$message_suspend="<p>Dear $d[lname],</p>
			<p>Email ini untuk informasi bahwa account anda di Mongkiki.com telah dinonaktifkan.</p>
			<p>Agar account Anda bergungsi dengan noramal kembali mohon melakukan proses perpanjangan. Berikut adalah detail order Anda:</p>";
			
			$message_delete="<p>Dear $d[lname],</p>
			<p>Email ini untuk informasi bahwa account anda di Mongkiki.com akan segera di hapus dari server karena sudah melewati masa tenggang.</p>
			<p>Agar account Anda bergungsi dengan noramal kembali mohon melakukan proses perpanjangan. Berikut adalah detail order Anda:</p>";
			
			$message_deleted="<p>Dear $d[lname],</p>
			<p>Email ini untuk informasi bahwa account anda di Mongkiki.com telah terhapus dari server dan Anda tidak bisa melakukan proses perpanjangan.Berikut adalah detail order Anda</p>
			<p>
				<h1 style=\"font-size:20px;color:red;border-bottom:1px solid red;\">Detail Order</h1>
				Order ID: $d[lorder_id]<br />
				Domain Name: $d[ldomain_name]<br />
				Expired Date: ".date("d F, Y",$d['lexpired_contract'])."<br />
				Suspension Date:" .date("d F, Y",$d['lexpired_contract'])."<br />
				Delettion Date:" .date("d F, Y",$day_to_deleted)."<br />
				
			</p>
			<p>
				Best Regards,<br /><br /><br />
				
				Mongkiki Team				
				<em>Info:</em><br />
				Email Address: <a href=\"mailto:info@mongkiki.com\">info@mongkiki.com</a><br />
				Tel: +62.361754926<br />
				Fax: +62.361751442<br />
			</p>";
			
			$message="<p>
				<h1 style=\"font-size:20px;color:red;border-bottom:1px solid red;\">Detail Order</h1>
				Order ID: $d[lorder_id]<br />
				Domain Name: $d[ldomain_name]<br />
				Expired Date: ".date("d F, Y",$d['lexpired_contract'])."<br />
				Suspension Date:" .date("d F, Y",$d['lexpired_contract'])."<br />
				Delettion Date:" .date("d F, Y",$day_to_deleted)."<br />
				
			</p>
			<p>
				<a href=\"http://".MONGKIPANEL_SITE_URL."/login/\">Klik disini</a> untuk login ke <strong>Mongkipanel</strong> dan memperpanjang account Anda.
			</p>
			<p>
				<strong>Support</strong><br>
				Untuk memperoleh layanan support, silahkan hubungi kami di:<br /><br /><br />
				
				<em>Technical Support:</em><br />
				Website: http://www.mongkiki.com<br />
				Email Address: <a href=\"mailto:support@mongkiki.com\">support@mongkiki.com</a><br />
				Tel: +62.361754926<br />
				Fax: +62.361751442<br /><br /><br />
				
				
				<em>Billing Contact:</em><br />
				Email Address: <a href=\"mailto:billing@mongkiki.com\">billing@mongkiki.com</a><br />
				Tel: +62.361754926<br />
				Fax: +62.361751442<br />
			</p>
			<p>
				Best Regards,<br /><br /><br />
				
				Mongkiki Team	
			</p>
			";
			
			if(date("d m Y",$month1_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 1 bulan";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$day15_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 15 hari";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$day7_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 7 hari";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$day3_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 3 hari";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$day2_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 2 hari";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$day1_to_exp) == date("d m Y",time())){
				$subject = "Renewal Reminder # $d[ldomain_name] - Account Anda di Mongkiki akan segera expired dalam waktu 1 hari";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$d['lexpired_contract'])==date("d m Y",time())){
				suspend_account($d[lorder_id]);
				$subject = "Account has been suspended  # $d[ldomain_name] - Account Anda di Mongkiki telah di suspend";
				send_alert($d['lemail'],$message_exp.$message,$subject);
			}elseif(date("d m Y",$month1_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 30 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day15_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 15 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day7_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 7 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day3_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 3 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day2_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 2 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day1_to_deleted)==date("d m Y",time())){
				$subject = "Account Deleted Soon # $d[ldomain_name] - Account Anda di Mongkiki akan segera di hapus dalam waktu 1 hari";
				send_alert($d['lemail'],$message_delete.$message,$subject);
			}elseif(date("d m Y",$day_to_deleted)==date("d m Y",time())){
				delete_account($order_id);
				delete_files($order_id);
				$subject = "Account Has been deleted # $d[ldomain_name] - Account Anda di Mongkiki telah terhapus";
				send_alert($d['lemail'],$message_deleted,$subject);
			}
			
		}
	}
	function suspend_account($order_id){
		global $db;	
		$sql=$db->prepare_query("update lumonata_order set lstatus='sp' where lorder_id=%s",$order_id);
		if($db->query($sql))
			return true;
		else
			return false;
	}
	function delete_account($order_id){
		global $db;	
		$sql=$db->prepare_query("update lumonata_order set lstatus='ar' where lorder_id=%s",$order_id);
		if($db->query($sql))
			return true;
		else
			return false;
	}
	function suspend_alert(){
		global $db;	
		$row=false;
		$sql="SELECT a.lorder_id,a.ldate_order,a.ldomain_name,a.lpackage_type,a.ldomain_price,a.lhosting_price,a.ltotal_diskon,b.lname,b.lemail,a.lexpired_contract
			  FROM lumonata_order a, lumonata_clients b 
			  WHERE a.lstatus='aa' and a.lclient_id=b.lclient_id";
		$r=$db->query($sql);
		
			$message="
			<p>
				Berikut adalah daftar account yang harus di suspend pada hari ini:</p>
			<p>
			<table cellpadding=\"3\" style=\"border-bottom:1px solid #cccccc;\">
				<tr>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Order ID</td>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Domain Name</td>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Suspension Date</td>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Deletion Date</td>
				</tr>";
				
			while($d=$db->fetch_array($r)){
				if(date("d m Y",$d['lexpired_contract'])==date("d m Y",time())){
					$message.="<tr>
					<td>$d[lorder_id]</td>
					<td>$d[ldomain_name]</td>
					<td>".date("d F Y",$d[lexpired_contract])."</td>
					<td>".date("d F Y",mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+40,date("Y",$d['lexpired_contract'])))."</td>
				</tr>";
				$row=true;
				}else{
					$row=false;
				}
			}
			
			$message.="</table>
			</p>
			<p>
				Untuk melakukan suspend, silahkan login ke <strong>WHM</strong> Control Panel Kemudian pilih menu <strong>Suspend / Unsuspend Account</strong>. 
				Kemudian tentukan account mana yang ingin Anda suspend. 
			</p>
			";
			
			if($row)
			send_alert("support@localhost",$message,"Daftar Account Hosting yang harus disuspend di WHM");
		
	}
	function delete_alert(){
		global $db;	
		$sql="SELECT a.lorder_id,a.ldate_order,a.ldomain_name,a.lpackage_type,a.ldomain_price,a.lhosting_price,a.ltotal_diskon,b.lname,b.lemail,a.lexpired_contract
			  FROM lumonata_order a, lumonata_clients b 
			  WHERE a.lstatus='aa' and a.lclient_id=b.lclient_id";
		$r=$db->query($sql);
		
		if($db->num_rows($r) > 0){
			$message="
			<p>
				Berikut adalah daftar account yang harus di hapus pada hari ini:</p>
			<p>
			<table cellpadding=\"3\" style=\"border-bottom:1px solid #cccccc;\">
				<tr>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Order ID</td>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Domain Name</td>
					<td bgcolor=\"#f0f0f0\" style=\"border-bottom:1px solid #cccccc;\">Suspension Date</td>
					<td bgcolor=\"#f0f0f0\"  style=\"border-bottom:1px solid #cccccc;\" >Deletion Date</td>
				</tr>";
				
			while($d=$db->fetch_array($r)){
				$day_to_deleted=mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+40,date("Y",$d['lexpired_contract']));
				if(date("d m Y",$day_to_deleted)==date("d m Y",time())){
					$message.="<tr>
					<td>$d[lorder_id]</td>
					<td>$d[ldomain_name]</td>
					<td>".date("d F Y",$d[lexpired_contract])."</td>
					<td>".date("d F Y",mktime(0,0,0,date("m",$d['lexpired_contract']) ,date("d",$d['lexpired_contract'])+40,date("Y",$d['lexpired_contract'])))."</td>
				</tr>";
				$row=true;
				}else{
					$row=false;
				}
			}
			
			$message.="</table></p>
			<p>
				Untuk melakukan suspend, silahkan login ke <strong>WHM</strong> Control Panel Kemudian pilih menu <strong>Terminate Account</strong>. 
				Kemudian tentukan account mana yang ingin Anda hapus. 
			</p>
			";
			
			if($row)
			send_alert("support@localhost",$message,"Daftar Account Hosting yang harus dihapus di WHM");
		}
	}
	function send_alert($to,$message,$subject){
			$cc="";
			$bcc="";

			ini_set("SMTP",SMTP_SERVER);
			ini_set("sendmail_from","automail@mongkiki.com");
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Additional headers
			
			//$from = $email;
						
			$headers .= "From:  Mongkiki Team <automail@mongkiki.com>\r\nReply-To :'automail@mongkiki.com'\r\n";
			$headers .= "Cc: ".$cc."\r\n";
			$headers .= "Bcc: ".$bcc."\r\n";
			$send=mail("$to","$subject","$message","$headers");
			
			//$send=true;
			
			if($send)return TRUE;
			else return FALSE;
	}
	
	delete_pp_order();
	delete_exp_order();
	suspend_alert();
	delete_alert();
	
?>