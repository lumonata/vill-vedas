<?php
/*
//BEGIN MENU
$t->set_var('top_menu', $navigationMenu->getMenu('T'));
$t->set_var('main_menu', $navigationMenu->getMenu('M'));
$t->set_var('bottom_menu', $navigationMenu->getMenu('B'));
$t->set_var('left_menu', $navigationMenu->getMenu('L'));
$t->set_var('right_menu', $navigationMenu->getMenu('R'));
//END MENU

//BANNER
$t->set_var('top_banners', $banners->load('Top'));
$t->set_var('right_banners', $banners->load('Right'));
$t->set_var('left_banners', $banners->load('Left'));
$t->set_var('bottom_banners', $banners->load('Bottom'));

//END BANNER
*/


require("plugin.php");

require("content_area.php");


/*BEGIN GLOBAL VARIABLE*/
/*
if(isset($_SESSION['AGENTPANEL_clientid'])){
	$t->set_var('login',"Logout");
	$t->set_var('logaction',"logout");
	$t->set_var('user',"<a href=\"http://".AGENTPANEL_SITE_URL."/profile/\">Hallo ".$_SESSION['AGENTPANEL_username']."</a> | ");
}else{
	$t->set_var('login',"Login");
	$t->set_var('logaction',"login");
}
*/

$t->set_var('main_site_url',SITE_URL);	
$t->set_var('address', $globalSetting->getSettingValue("address","Global Setting"));
$t->set_var('meta_title', $META_TITLE);
$t->set_var('meta_desc', $META_DESC);
$t->set_var('meta_key', $META_KEY);
$t->set_var('js_url', SUPPLIERPANEL_JS_URL);
$t->set_var('rootsite_url', SITE_URL);
$t->set_var('site_url', SUPPLIERPANEL_SITE_URL);
$t->set_var('orion_site_url',SITE_URL);
$t->set_var('root_path', SUPPLIERPANEL_ROOT_PATH);
$t->set_var('apps_url', SUPPLIERPANEL_APPS_URL);
$t->set_var('lang', LANGUAGE);
$t->set_var('year', date("Y",time()));
$t->set_var('day', date("D",time()));
$t->set_var('d', date("d",time()));
$t->set_var('mon', date("M",time()));
$t->set_var('date', date("G.i.s",time()));
$t->set_var('hour', date("H",time()));
$t->set_var('minute', date("i",time()));
$t->set_var('template_url',SUPPLIERPANEL_THEME_URL);
//$t->set_var('admin_url',ADMIN_URL);

if (!empty($_SERVER["HTTPS"])){
	$t->set_var('http',"https://");
	$t->set_var('rootsite_url', SSITE_URL);
	$t->set_var('admin_url',    SSITE_URL."/lumonata-admin");
}else{
	$t->set_var('http',"http://");
	$t->set_var('rootsite_url', SITE_URL);
	$t->set_var('admin_url',    SITE_URL."/lumonata-admin");
}

$t->set_var('main_menu', $navigationMenu->getMenu('M'));
//$t->set_var('right_banners',"<div class=\"otherbaner\">".$banners->load('Right')."</div>");



/*END GLOBAL VARIABLE*/



?>