
<?
class paging{
	var $top_page;
	var $view;
	var $limit_view;
	var $num_rows;
	var $page_view;
	function paging($top_page,$view,$num_rows,$page_view){
		$this->top_page=$top_page;
		$this->view=$view;
		$this->num_rows=$num_rows;
		$this->page_view=$page_view;
	}
	function page_count(){
		$kel=$this->num_rows/$this->view;
		if ($kel==floor($this->num_rows/$this->view)){
			return $kel;
		}
		else{
			return floor($this->num_rows/$this->view)+1;
		} 
	}
	function pagination($url){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<div id=\"paging\"><ul><li><b><a href=\"$url"."1/\" style=\"	text-decoration:none;\">Pertama</a></b></li>";
		
		if ($this->top_page!=1)
		{
	
			$html.="<li><b><a href=\"$url".$prev."\" title=\"Prev\" style=\"	text-decoration:none;\">Sebelumnya</a></b></li>";
	
		}else{
	
			$html.="<li>Sebelumnya</li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li><span style=\"font-size:14px;\">$i</span></li>";
	
			}else{
	
				$html.= "<li><b><a href=\"$url".$i."\" title=\"$i\" style=\"text-decoration:none;\" >$i</a></b></li>";
	
			}
	
		}
		
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li><b><a href=\"$url".$next."\" title=\"Next\" style=\"text-decoration:none;\">Berikutnya</a></b></li>";
	
		}else{
			$html.= "<li>Berikutnya</li>";
	
		}
	
		$html.="<li><b><a href=\"$url".$this->page_count()."\" style=\"text-decoration:none;\">Terakhir</a></b></li></ul></div>";
		
		
		return $html;

	}
	
	function ajaxPagination($serverPage,$cat_id=0){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<div id=\"paging\">
				<ul>
					<li>
						<b><a href=\"#\" onclick=\"ajaxPaging('$serverPage','tutorial',1,$cat_id)\" style=\"text-decoration:none;\">Pertama</a></b>
					</li>";
		
		if ($this->top_page!=1)
		{
	
			$html.="<li>
						<b>
						<a href=\"#\" onclick=\"ajaxPaging('$serverPage','tutorial',$prev,$cat_id)\" title=\"Prev\" style=\"text-decoration:none;\">
							Sebelumnya
						</a>
						</b>
					</li>";
	
		}else{
	
			$html.="<li>Prev</li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li><span style=\"font-size:14px;\">$i</span></li>";
	
			}else{
	
				$html.= "<li>
						<b>
						<a href=\"#\" title=\"$i\" onclick=\"ajaxPaging('$serverPage','tutorial',$i,$cat_id)\" style=\"text-decoration:none;\" >$i</a>
						</b>
						</li>";
	
			}
	
		}
		
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li>
					<b>
					<a href=\"#\" onclick=\"ajaxPaging('$serverPage','tutorial',$next,$cat_id)\" title=\"Next\" style=\"text-decoration:none;\">
					Berikutnya
					</a>
					</b>
					</li>";
	
		}else{
			$html.= "<li>Berikutnya</li>";
	
		}
	
		$html.="<li>
				<b>
				<a href=\"#\" onclick=\"ajaxPaging('$serverPage','tutorial',".$this->page_count().",$cat_id)\" style=\"text-decoration:none;\">Terakhir</a>
				</b>
				</li>
				</ul>
				</div>";
		
		
		return $html;

	}
	
	function ajaxMemberPagination($serverPage,$reff=''){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<div id=\"paging\">
				<ul>
					<li>
						<b><a href=\"#network\" onclick=\"ajaxPaging('$serverPage','network',1,'$reff')\" style=\"text-decoration:none;\">Pertama</a></b>
					</li>";
		
		if ($this->top_page!=1)
		{
	
			$html.="<li>
						<b>
						<a href=\"#network\" onclick=\"ajaxPaging('$serverPage','network',$prev,'$reff')\" title=\"Prev\" style=\"text-decoration:none;\">
							Sebelumnya
						</a>
						</b>
					</li>";
	
		}else{
	
			$html.="<li>Sebelumnya</li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li><span style=\"font-size:14px;\">$i</span></li>";
	
			}else{
	
				$html.= "<li>
						<b>
						<a href=\"#network\" title=\"$i\" onclick=\"ajaxPaging('$serverPage','network',$i,'$reff')\" style=\"text-decoration:none;\" >$i</a>
						</b>
						</li>";
	
			}
	
		}
		
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li>
					<b>
					<a href=\"#network\" onclick=\"ajaxPaging('$serverPage','network',$next,'$reff')\" title=\"Next\" style=\"text-decoration:none;\">
					Selanjutnya
					</a>
					</b>
					</li>";
	
		}else{
			$html.= "<li>Selanjutnya</li>";
	
		}
	
		$html.="<li>
				<b>
				<a href=\"#network\" onclick=\"ajaxPaging('$serverPage','network',".$this->page_count().",'$reff')\" style=\"text-decoration:none;\">Terakhir</a>
				</b>
				</li>
				</ul>
				</div>";
		
		
		return $html;

	}
	
	function searchPagination($url){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<div id=\"paging\"><ul><li><b><a href=\"$url"."1/\" style=\"text-decoration:none;\"><<</a></b></li>";
		
		if ($this->top_page!=1)
		{
	
			$html.="<li><b><a href=\"$url".$prev."/\" title=\"Prev\" style=\"text-decoration:none;\">Prev</a></b></li>";
	
		}else{
	
			$html.="<li>Prev</li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li><span style=\"font-size:14px;\">$i</span></li>";
	
			}else{
	
				$html.= "<li><b><a href=\"$url".$i."\" title=\"$i\" style=\"text-decoration:none;\">$i</a></b></li>";
	
			}
	
		}
		
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li><b><a href=\"$url".$next."\" title=\"Next\" style=\"text-decoration:none;\">Next</a></b></li>";
	
		}else{
			$html.= "<li>Next</li>";
	
		}
	
		$html.="<li><b><a href=\"$url".$this->page_count()."\" style=\"text-decoration:none;\">>></a></li></b></ul></div>";
		
		
		return $html;

	}
	
	function jsPagination($url){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		$html="<ul><li><b><a href=\"$url"."1')\">&lt;&lt;</a></b></li>";
		
		if ($this->top_page!=1)
		{
	
			$html.="<li><b><a href=\"$url".$prev."')\" title=\"Prev\" style=\"text-decoration:none;\">&lt;</a></b></li>";
	
		}else{
	
			$html.="<li><span style=\"font-size:11px;\">&lt;</span></li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li><span style=\"font-size:11px;\">$i</span></li>";
	
			}else{
	
				$html.= "<li><b><a href=\"$url".$i."')\" title=\"$i\"  style=\"text-decoration:none;\">$i</a></b></li>";
	
			}
	
		}
		
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li><b><a href=\"$url".$next."')\" title=\"Next\" style=\"text-decoration:none;\">&gt;</a></b></li>";
	
		}else{
			$html.= "<li><span style=\"font-size:11px;\">&gt;</span></li>";
	
		}
	
		$html.="<li><b><a href=\"$url".$this->page_count()."')\" style=\"text-decoration:none;\">&gt;&gt;</a></b></li></ul>";
		
		
		return $html;

	}
	function nextPrevPagination($url,$prev_icon,$next_icon){
		$prev=$this->top_page-1;
		$next=$this->top_page+1;
		$page_range=floor($this->page_view/ 2);
		
		//$html="<ul><li><a href=\"$url"."1/\"></a></li>";
		$html="<ul>";
		if ($this->top_page!=1)
		{
	
			$html.="<li style=\"padding:20px 0 0 0;\">
					<b><a href=\"$url".$prev."/#tab\" title=\"Prev\" style=\"text-decoration:none;\">
						<img src=\"$prev_icon\" border=\"0\" width=\"10\" />
					</a></b>
					</li>";
	
		}else{
	
			$html.="<li><img src=\"$prev_icon\" border=\"0\" width=\"10\" /></li>";
	
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($this->top_page<$this->page_view && $this->page_count() >= $this->page_view){
			$top_go=1;
			$page_go=$this->page_view;
	
		}elseif($this->top_page< $this->page_view && $this->page_count() < $this->page_view){
					
			if($this->top_page - $page_range <=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			$page_go=$this->page_count();
			
		}elseif($this->top_page >= $this->page_view && $this->top_page <= $this->page_count()){

			if($this->top_page - $page_range<=1)
			{
				$top_go=1;
			}
			else
			{
				$top_go=$this->top_page - $page_range;
			}
			
			if($this->top_page +$page_range > $this->page_count())
			{
				$page_go=$this->page_count();
			}
			else
			{
				$page_go=$this->top_page + $page_range;
			}
		}
	    //stop lipatan
		
		for($i=$top_go;$i<=$page_go;$i++)
	
		{
	
			if ($i==$this->top_page)
	
			{
	
				$html.= "<li>$i</li>";
	
			}else{
	
				$html.= "<li><b><a href=\"$url".$i."/\" title=\"$i\" style=\"text-decoration:none;\">$i</a></b></li>";
	
			}
	
		}
		$html.=" / ".$this->page_count();
		if ($this->top_page!=$this->page_count())
	
		{
	
			$html.="<li><b><a href=\"$url".$next."/#tab\" title=\"Next\" style=\"text-decoration:none;\">&nbsp;&nbsp;<img src=\"$next_icon\" border=\"0\" width=\"10\" /></a></b></li>";
	
		}else{
			$html.= "<li>&nbsp;<img src=\"$next_icon\" border=\"0\" width=\"10\" /></li>";
	
		}
	
		//$html.="<li><a href=\"$url".$this->page_count()."/\">Last Page</a></li></ul>";
		$html.="</ul>";
		
		return $html;

	}
}

?>