<?php
//A list of all names.
//Generally this would be in a database of some sort.
$names = array ("Lee Babin","Joe Smith","John Doe","John Doe","John Doe","John Doe","John Doe","John Doe","John Doe","John Doe","John Doe","John Doe");
$foundarr = array ();
//Go through the names array and load any matches into the foundarr array.
if ($_GET['sstring'] != ""){
	for ($i = 0; $i < count ($names); $i++){
		if (substr_count (strtolower ($names[$i]), strtolower ($_GET['sstring'])) > 0){
				$foundarr[] = $names[$i];
		}
	}
}

//If we have any matches.
if (count ($foundarr) > 0){
//Then display them.
?>
<div class="autocomp">
	
	<?php for ($i = 0; $i < count ($foundarr); $i++){ ?>
	
	<div class="search_result" onclick="setvalue('<?php echo $foundarr[$i]; ?>')" onmouseover="setMouseOver(this)" onmouseout="setMouseOut(this)" id="rs<?=$i+1;?>">
		<a href="#" tabindex="<?=$i+1;?>" style="display:block;"><?php echo $foundarr[$i]; ?></a>
	</div>
	
	<?php }?>
	
</div>
<?php
}
?>