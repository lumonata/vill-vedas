<?php
class list_villa extends db {
		var $appName;
		
		function list_villa($appName){
			$this->appName=$appName;
			$this->template_var="list_villa";
		}
		
		function load(){
			if (isset($_COOKIE['member_logged_ID'])){
				return $this->all_villa();
			}
		}
		
		function just_option(){
			if (isset($_COOKIE['member_logged_ID'])){
				return $this->all_villa(true);
			}
		}
		
		
		
		function all_villa($just_option=false){
			global $db;
			//get all villa
			$qa = $db->prepare_query("select * from lumonata_articles where larticle_type=%s",'villas');
			$ra = $db->do_query($qa);
			if($just_option==false)$all = "<select name=\"acco_type_cat_id[0]\">";
			else $all ="";
			
			while($da = $db->fetch_array($ra)){
				$villa_name = $da['larticle_title'];
				$villa_id = $da['larticle_id'];
				
				//get categories
				$rc = $this->data_additional_villa($villa_id,'categories');
				$dc = $db->fetch_array($rc);
				$category_villa = $dc['lname'];
				
				//get roomtype
				$rr = $this->data_additional_villa($villa_id,'room_type');
				$dr = $db->fetch_array($rr);
				if(!empty($dr)) {
					$roomtype_villa = $dr['lname'];
					$category_villa  = $category_villa.',';
				}else $roomtype_villa = "";	
				
				if($all==""){
					$selected = 'selected="selected"';
					//$temp->set_var('villa_id',$villa_id);	
				}
				else $selected = "";
				
				$all .= "<option value=\"$villa_id\" $selected>$villa_name - $category_villa $roomtype_villa</option>";
				
			}
			

			if($just_option==false) $all .= "</select>" ;
			
			return $all;
		}
		
		
		function data_additional_villa($id,$type){
			global $db;
			$qc = $db->prepare_query("SELECT a.*,b.lapp_id
							 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
							 WHERE a.lrule=%s AND 
							 (a.lgroup=%s OR a.lgroup=%s) AND 
							 a.lrule_id=b.lrule_id AND
							 b.lapp_id=c.larticle_id AND
							 c.lshare_to=0 AND
							 b.lapp_id = c.larticle_id AND
							 b.lapp_id = %d
							 GROUP BY a.lrule_id
							 ORDER BY a.lorder",$type,'villas','villas',$id);
			$rc = $db->do_query($qc);
			return $rc;			
		}
		
		function getTemplateVar(){
			return $this->template_var;
		}
		
}
?>