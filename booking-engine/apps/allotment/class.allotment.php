<?php
	class allotment extends db{
		function allotment($appName){
			$this->appName=$appName;
			$this->setMetaTitle("allotment");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function load(){
			global $db;
			if(!isset($_POST['do_act'])) $OUT_TEMPLATE="template.html";
			else $OUT_TEMPLATE="ajax.html";
			
			
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			$t->set_block('home', 'mainBlock',  'mBlock');
		
			require_once("../booking-engine/apps/calendar/class.calendar.php");
			$this->calendar = new calendar('calendar');
			$t->set_var('content', $this->calendar->load());			
	
			return $t->Parse('mBlock', 'mainBlock', false);
			
			
		}
		
		
		
		
		function get_data_roomtype_villa(){
			
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>