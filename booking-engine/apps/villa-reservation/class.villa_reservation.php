<?php 
class villa_reservation extends db{
	function villa_reservation($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Villa Reservation");	
	}
	function load(){
		if(isset($_POST['act']) && $_POST['act']=='use-ajax'){
			return $this->validate_ajax();	
		}else{
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			$t->set_var('roomTypeOpt',$this->get_temp_all_villa($t,true,false));
			$t->set_var('site_url',SUPPLIERPANEL_SITE_URL);
			
			$data_booking = $this->get_first_data();
			
			$t->set_var('list_reservation_history_all',$data_booking->temp);
			if(isset($data_booking->pagging))$t->set_var('pagging_list',$data_booking->pagging);
			$t->set_var('grand_total',$data_booking->grand_total);
			
			$data_villa = $this->get_data('lumonata_articles',"where larticle_type='villas'",'array');
			$id_firsd_villa = $data_villa['larticle_id'];
			
			//View list booking first atau pakai ajax
			$t->Parse('vC', 'viewContent', true); 	
			
			return $t->Parse('mBlock', 'mainBlock', false);	
		}
		
	}
		
	function get_first_data(){
		$month = date('m');
		$year = date('Y');
		$val_1 = mktime(0, 0, 0, $month, 1, $year); 
	    $val_2 = mktime(0, 0, 0, $month+1, 1, $year); 		
		$page = 1;		
		$first_data = 	$this->get_all_list_accom_history($val_1,$val_2,true,'all',$page);	
		$ob_fd = json_decode($first_data);
		return  $ob_fd;		
	}
	
	function validate_ajax(){
		if($_POST['do_act']=='get-list-booking-history'){
			$type_search = $_POST['type_search'];
			$villa_id 	 = $_POST['villa_id'];
			$year	 	 = $_POST['year'];
			$status		 = $_POST['status'];
			$page		 = $_POST['page'];
						
			if($type_search=='by_month') {
				$month	 = $_POST['month'];
				$val_1 = mktime(0, 0, 0, $month, 1, $year); 
				$val_2 = mktime(0, 0, 0, $month+1, 1, $year); 
			}else{
				$val_1 = mktime(0, 0, 0, 1, 1, $year);
				$val_2 = mktime(0, 0, 0, 1, 1, $year+1);
			}
			
			//echo $status;
			if($villa_id=='all') return $this->get_all_list_accom_history($val_1,$val_2,true,$status,$page);
			else return $this->get_list_accom_history_single_villa($villa_id,$val_1,$val_2,true,$status,$page);			
		}else if($_POST['do_act']=='get-single-detail-booking-history'){
			$book_id = $_POST['book_id'];
			return $this->single_detail_book_history($book_id);
		}else if($_POST['do_act']=='change-status-reservation'){
			$book_id = $_POST['book_id'];
			$status  = $_POST['status_id'];
			return $this->change_status_reservation($book_id,$status);
		}else if($_POST['do_act']=='update-detail-booking'){
			$this->update_booking_detail();
		}else if($_POST['do_act']=='change-status-reservation-and-detail'){
			$this->update_bookdet_and_status();
		}
	}
	
	function update_bookdet_and_status(){
		global $db;
		
		$book_id = $_POST['book_id'];
		$dbk = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
		//print_r($dbk);
		if(!empty($dbk)){
			$status = $_POST['status_id'];
			$return =array();
			if($status==10){
				$this->delete_booking_data($book_id);
				$return['process']='success';
			}else{
				$error = 0;
				$fname = $_POST['fname'];
				$lname = $_POST['lname'];
				$email = $_POST['email'];
				$adult = $_POST['adult'];
				$phone = $_POST['phone'];
				$child = $_POST['child'];
				$message = nl2br($_POST['message']);
				$status = $_POST['status_id'];
				
				
										
				$u = $db->prepare_query("update lumonata_accommodation_booking set 
										lfirst_name=%s,llast_name=%s,lemail=%s,lphone=%s,
										ladult=%s,lchild=%s,lspecial_note=%s,lstatus=%d 
										where lbook_id=%d",$fname,$lname,$email,$phone,
										$adult,$child,$message,$status,$book_id);
				
				$ru = $db->do_query($u);	
				//$ru = true;
				if($ru){
					$dMd = $this->get_data('lumonata_meta_data',"where lmeta_name='send_email_change_booking_status' and lapp_name='global_setting'",'array');	
					$dtDeposit = json_decode($dbk['ldeposit']);			
					//print_r($dMd);exit;					
					/*if($status==2 && $dMd['lmeta_value']=='yes'){$this->send_email_waiting_down_payment($book_id);} 
					if($status==3 && $dMd['lmeta_value']=='yes'){$this->send_email_waiting_full_payment($book_id);} 
					if($status==6) {$this->sync_book_to_availability($book_id,0);}*/					
					if($status==1)$return['status_txt']='Check Availability';
					if($status==2){
						$return['status_txt']='Awaiting Down Payment';	
						if($dMd['lmeta_value']=='yes') $this->send_email_waiting_down_payment($book_id);
					}
					if($status==3){
						$return['status_txt']=($dtDeposit->deposit ==100 ? 'Awaiting Full Payment':'Awaiting Balance Payment');
						if($dMd['lmeta_value']=='yes') $this->send_email_waiting_full_payment($book_id);
					}
					if($status==4){
						$return['status_txt']='Confirm';
						$this->sync_book_to_availability($book_id,0);	
					}
					if($status==5)$return['status_txt']='Unconfirmed';
					if($status==6){
						$this->sync_book_to_availability($book_id,0);
						$return['status_txt']='Receive Down Payment';	
					}
					if($status==0) $this->sync_book_to_availability($book_id,1);					
					
					$return['status']=$status;
					$return['process']='success';
				}else{
					$return['process']='failed';
				}
				
			}//end if status delete	
			echo json_encode($return);
		}
	}
	
	function delete_booking_data($book_id){
		global $db;
		$this->sync_book_to_availability($book_id, 1);
		
		$qd = $db->prepare_query("delete from lumonata_accommodation_booking where lbook_id=%d",$book_id);	
		$db->do_query($qd);
		
		$qd2 = $db->prepare_query("delete from lumonata_accommodation_booking_detail where lbook_id=%d",$book_id);
		$db->do_query($qd2);
		
		$qd3 = $db->prepare_query("delete from lumonata_accommodation_payment where lbook_id=%d",$book_id);
		$db->do_query($qd3);
	}
	
	function cancel_book_n_availability($book_id){
		$this->sync_book_to_availability($book_id, 1);
	}
	
	
	function  sync_book_to_availability($book_id,$status){
		global $db;
		$str 	= $db->prepare_query("select b.larticle_id, a.lcheck_in, a.lcheck_out from lumonata_accommodation_booking a inner join
									 lumonata_accommodation_booking_detail b on a.lbook_id=b.lbook_id where a.lbook_id=%d",$book_id);
		$r		= $db->do_query($str);
		while($dt_book= $db->fetch_array($r)){
			$accom_id = $dt_book['larticle_id'];
			$date_start = $dt_book['lcheck_in'];
			$date_finish = $dt_book['lcheck_out'];
			$days = ($date_finish - $date_start) / 86400;
			$this->sync_tabel_availability($accom_id,$date_start,$date_finish,$days,$status);
		}
	}
	
	
	function sync_tabel_availability($accom_id,$date_start,$date_finish,$days,$status){
		global $db;
		$username = $_SESSION['upd_by'];
		for($i=0;$i<=$days;$i++){
			$date = $date_start + ($i * 86400);	
			$data_availability 	= $db->prepare_query("select * from lumonata_availability WHERE ldate=%d AND lacco_id=%d",$date,$accom_id);
			$rda = $db->do_query($data_availability);
			$dt = $db->fetch_array($rda);
			if(!empty($dt)){
				$error = 0;
				
				if($date == $date_start){//check if another check in
					if($this->is_cIn_COut_booking($accom_id,'lcheck_out',$date)) $error++;
				}
				if($date == $date_finish){//check if another check in
					if($this->is_cIn_COut_booking($accom_id,'lcheck_in',$date)) $error++;
				}
				if($error==0){
					$query_update = $db->prepare_query("update lumonata_availability 
															set lstatus=%d where ldate=%d and lacco_id=%d",
															$status,$date,$accom_id);
					$db->do_query($query_update);	
				}														
			}//if num on availability 0
		}//end for
	}
	
	function is_cIn_COut_booking($acco_id,$field,$date){
		global $db;
		$q = $db->prepare_query("select * from lumonata_accommodation_booking lab inner join lumonata_accommodation_booking_detail labd on
								lab.lbook_id=labd.lbook_id where lab.$field=%d and labd.larticle_id=%d",$date,$acco_id);
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);
		if(!empty($d)) return true;
		else return false;
	}
	
	
	
	
	function change_status_availability($book_id,$status = 0){
		global $db;
		$list_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id");
		while($dt_booking = $db->fetch_array($list_booking)){
			$accom_id = $dt_booking['larticle_id'];
			$ss = $dt_booking['lcheck_in'];
			$es = $dt_booking['lcheck_out'];
			$days = ($es - $ss)/86400;
			$error=0;
			
			for($i=0;$i<$days;$i++){
				$date = $ss + ($i * 86400);
				$num_availability = $this->get_data('lumonata_availability',"where ldate=$date AND lacco_id=$accom_id",'num_row');
				if($num_availability > 0){
					$update_availability = $db->prepare_query("update lumonata_availability set lstatus=%d where ldate=%d and lacco_id=%d",
										   $status,$date,$accom_id);
					$db->do_query($update_availability);					   
				}
			}
		}	
	}
	
	function send_email_waiting_down_payment($book_id){
		$content_email = $this->email_waiting_down_payment($book_id);
		//echo $content_email['content_email'];
		//exit;
		if(!empty($content_email)){
			$data_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');			
			$data_contact = $this->get_contact_us_detail();
			$email_admin =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			$name_to = $data_booking['lfirst_name'].' '.$data_booking['llast_name'];
			
			require_once("../booking-engine/apps/program-reservation/class.program_reservation.php");
			$this->program_reservation = new program_reservation();	
			$data_smtp = $this->program_reservation->get_smtp_info();
			$email_smtp = $data_smtp['email'];
			$pass_smtp = $data_smtp['pass'];
			
			require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
			$mail = new PHPMailer(true);
			$web_name = 'Villavedas.com';
			$SMTP_SERVER = get_meta_data('smtp');
			try {
				$mail->SMTPDebug  = 1;
				$mail->IsSMTP();
				$mail->Host = $SMTP_SERVER;
				$mail->SMTPAuth = true;
				$mail->Port       = 2525; 
				$mail->Username   = $email_smtp;  // GMAIL username
				$mail->Password   = $pass_smtp; // GMAIL password
				$mail->AddReplyTo($email_admin, $web_name);
				$mail->AddAddress($email_to,'juliartha');
				$mail->SetFrom($email_admin, $web_name);
				$mail->AddReplyTo($email_admin, $web_name);
				$mail->Subject = "Villa Vedas Booking Invoice: $book_id";
				$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
				$mail->MsgHTML($content);
				$mail->Send();
			}catch (phpmailerException $e) {
				echo $e->errorMessage().$sukhavati_email;
			}catch (Exception $e) {
				echo $e->getMessage();
			}//end try	
		}//end empty content
	}
	
	function email_waiting_down_payment($book_id){
		global $db;		
		require_once("../lumonata-plugins/reservation/class.email_reservation.php");
		$this->email_reservation = new email_reservation();		
		$OUT_TEMPLATE="email-html/email-waiting-downpayment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'email_wdp_block',  'shb');		
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');				
		$result = array();		
		if($num_booking>0){		
			$q = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b
									on a.lbook_id=b.lbook_id  inner join lumonata_articles c on b.larticle_id=c.larticle_id
									where a.lbook_id=%d",$book_id);
									
			$r = $db->do_query($q);	
			$subject = "";					
			while($dt = $db->fetch_array($r)){
				$t->set_var('guest_name',$dt['lfirst_name'].' '.$dt['llast_name']);
				$t->set_var('villa_name',$dt['larticle_title']);
				$t->set_var('check_in',date('d M Y',$dt['lcheck_in']));
				$t->set_var('check_out',date('d M Y',$dt['lcheck_out']));
				$subject = $dt['larticle_title']." is available for (".date('d M Y',$dt['lcheck_in'])."/".date('d M Y',$dt['lcheck_out']).")";
				$result['email'] = $dt['lemail'];
				$result['subject'] = $subject;
				$result['name_to'] = $dt['lfirst_name'].' '.$dt['llast_name'];
			}
			
			$url_array  = array();
			$url_array['val_date'] 	= time()+ (86400 * 3);
			$url_array['book_id'] 	= $book_id;
			$url_array['type'] 		= 'dp';
			
			$link = base64_encode(json_encode($url_array));
			$t->set_var('url_payment','http://'.SITE_URL.'/payment/'.$link.'/');
			
			$t->set_var('booking_detail',$this->email_reservation->just_detail_booking($book_id,'dp',true));	
			$t->set_var('best_regrads',$this->email_reservation->best_regrads_email());	
					
			$content = $t->Parse('shb', 'email_wdp_block', false);
			$content_email = $this->email_reservation->set_content_to_main_email_template($subject,$content);
			$result['content_email'] = $content_email;
		}
		
		return $result;
		
	}
	
	function td_surcharge_booking_detail($the_surcharge, $temp_surcharge,$grand_total){
		
		$temp = "<tr>
					<td  style=\"padding :10px;border-top:solid 1px #bbb; border-right:solid 1px #bbb;\" colspan=\"2\">
						<label style=\"float: left;\">Surcharge</label>
						<ul style=\"float: left;margin: 0;\">$temp_surcharge</ul>				
					</td>
					<td  style=\"padding :10px;border-top:solid 1px #bbb;text-align:center;\">$the_surcharge</td>
				</tr>
				<tr>
					<td  style=\"padding :10px;border-top:solid 1px #bbb; border-right:solid 1px #bbb;\" colspan=\"2\">Grand Total</td>
					<td  style=\"padding :10px;border-top:solid 1px #bbb;text-align:center;\">$grand_total</td>
				</tr>";
		return $temp;		
	}	
	
	
	function send_email_waiting_full_payment($book_id){
		$dbk = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
		$dtDeposit  = json_decode($dbk['ldeposit']);
		if($dtDeposit->deposit < 100) $content_email = $this->email_waiting_full_payment($book_id);
		else $content_email = $this->email_waiting_down_payment($book_id);
		//print_r($content_email);exit;
		//$content_email = $this->email_waiting_full_payment($book_id);
		if(!empty($content_email)){
			require_once("../lumonata-plugins/reservation/class.email_reservation.php");
			$email_reservation = new email_reservation();
			$message = $content_email['content_email'];
			$to = $content_email['email'];
			$guest_name = $content_email['name_to'];
			$dtContact = $email_reservation->contact_admin();
			$contact_us_email = $dtContact['cu_email'];
			$contact_us_email_cc = $dtContact['cu_email_cc'];
			
			$data_smtp = $email_reservation->smtp_info();
			$email_smtp = $data_smtp['email'];
			$pass_smtp = $data_smtp['pass'];
			$web_name = trim(get_meta_data('web_title'));
			$SMTP_SERVER = get_meta_data('smtp');
			$subject = $content_email['subject'];
			//echo $message;exit;
			//echo "$contact_us_email,$contact_us_email_cc,$email_smtp,$pass_smtp,$web_name,$SMTP_SERVER,$to,$guest_name";exit;	
			require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
			$mail = new PHPMailer(true);
			$web_name = 'Villavedas.com';
			$SMTP_SERVER = get_meta_data('smtp');
			try {
				$mail->SMTPDebug  = 1;
				$mail->IsSMTP();
				$mail->Host = $SMTP_SERVER;
				$mail->SMTPAuth = true;
				$mail->Port       = 2525; 
				$mail->Username   = $email_smtp;  // GMAIL username
				$mail->Password   = $pass_smtp; // GMAIL password
				$mail->AddReplyTo($contact_us_email, $web_name);
				$mail->AddAddress($to,$guest_name);
				$mail->SetFrom($contact_us_email, $web_name);
				$mail->Subject = $subject;
				$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
				$mail->MsgHTML($message);
				$mail->Send();
			}catch (phpmailerException $e) {
				echo $e->errorMessage().$web_name;
			}catch (Exception $e) {
				echo $e->getMessage();
			}	
		}
		
	}
	
	function email_waiting_full_payment($book_id){
		global $db;
		require_once("../lumonata-plugins/reservation/class.email_reservation.php");
		$this->email_reservation = new email_reservation();
		
		$OUT_TEMPLATE="email-html/email-waiting-balance-payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		
		//set block
		$t->set_block('home', 'ewfp_Block',  'shb');	
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		if($num_booking>0){
			$q = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b
									on a.lbook_id=b.lbook_id  inner join lumonata_articles c on b.larticle_id=c.larticle_id
									where a.lbook_id=%d",$book_id);
									
			$r = $db->do_query($q);	
			$subject = "";					
			while($dt = $db->fetch_array($r)){
				$t->set_var('name',$dt['lfirst_name'].' '.$dt['llast_name']);
				$t->set_var('villa_name',$dt['larticle_title']);
				$t->set_var('check_in',date('d M Y',$dt['lcheck_in']));
				$t->set_var('check_out',date('d M Y',$dt['lcheck_out']));
				$subject = $dt['larticle_title']." is available for (".date('d M Y',$dt['lcheck_in'])."/".date('d M Y',$dt['lcheck_out']).")";
				
				$subject = "Payment of the balance is due. ".$dt['larticle_title'].", (".date('d M Y',$dt['lcheck_in'])."/".date('d M Y',$dt['lcheck_out']).").";
				$result['email'] = $dt['lemail'];
				$result['subject'] = $subject;
				$result['name_to'] = $dt['lfirst_name'].' '.$dt['llast_name'];
			}
			$url_array  = array();
			$url_array['val_date'] 	= time()+ (86400 * 3);
			$url_array['book_id'] 	= $book_id;
			$url_array['type'] 		= 'fp';
			
			$link = base64_encode(json_encode($url_array));
			$t->set_var('link','http://'.SITE_URL.'/payment/'.$link.'/');
			$t->set_var('best_regrads',$this->email_reservation->best_regrads_email());	
			$content = $t->Parse('shb', 'ewfp_Block', false);
			$content_email = $this->email_reservation->set_content_to_main_email_template($subject,$content);
			$result['content_email'] = $content_email;
			//print_r($result);exit;
		}
		return $result;	
	}
	
	
	function content_invoice($book_id){
		global $db;
		$OUT_TEMPLATE="invoice_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
				
		if($num_booking>0){
			$t->set_var('book_id',$book_id);			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',number_format($total,2));
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];	
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);		
			}
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);
			//down payment
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('down_payment_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= $dt_paypal_dp['lgross'];		
			$t->set_var('downpayment',$downpayment);
			//full payment
			$dt_paypal_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
			$full_payment= $dt_paypal_fp['lgross'];		
			$t->set_var('full_payment',$full_payment);
			
			
			$t->set_var('rest_payment',$grand_total-($downpayment+$full_payment));
			$t->set_var('paypal_id',$dt_paypal_fp['ltxn_id']);
			$t->set_var('receiver_email',$dt_paypal_fp['lemail']);
			$t->set_var('payment_date',$dt_paypal_fp['lcreated_date']);
			
			
			$t->set_var('grand_total',number_format($grand_total,2));
		}
		
		return $t->Parse('shb', 'the_email_block', false);	
	}
	
	function generate_pdf_file($content,$book_id){	
		set_include_path(ROOT_PATH."/dompdf/");
		
		require_once "dompdf_config.inc.php";
		 
		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->render();
		 
		$output = $dompdf->output();
		$file_path = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";
		unlink($file_path);
		file_put_contents($file_path, $output);
		
	}
	
	function send_email_confirm($book_id){
		$content_email = $this->email_confirm_payment($book_id);
		$message = $content_email['content_email'];
		$email_to = $content_email['email'];
		
		//generate invoice
		$content_invoice =  $this->content_invoice($book_id);
		$this->generate_pdf_file($content_invoice,$book_id);
		
		$path_invoice = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";;
		
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			//print_r($data_contact);
			$sukhavati_email = $data_contact['contact_us_email'];
			
			require_once("../booking-engine/apps/program-reservation/class.program_reservation.php");
			$this->program_reservation = new program_reservation();	
			
			$data_smtp = $this->program_reservation->get_smtp_info();
			$email_smtp = $data_smtp['email'];
			$pass_smtp = $data_smtp['pass'];			
			
			//$sukhavati_email = 'adi@lumonatalabs.com';
			require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
			
			$mail = new PHPMailer(true);
			$web_name = 'sukhavati.com';
			//$email_to = 'juli4rth4@yahoo.com';
			$SMTP_SERVER = get_meta_data('smtp');
			//$SMTP_SERVER = 'mail.lumonatalabs.com';
			try {
				$mail->SMTPDebug  = 1;
				$mail->IsSMTP();
				$mail->Host = $SMTP_SERVER;
				$mail->SMTPAuth = true;
				$mail->Port       = 2525; 
				$mail->Username   = $email_smtp;  // GMAIL username
				$mail->Password   = $pass_smtp; // GMAIL password
				$mail->AddReplyTo($sukhavati_email, 'sukhavati.com');
				$mail->AddAddress($email_to,'juliartha');
				$mail->SetFrom($sukhavati_email, $web_name);
				$mail->AddReplyTo($sukhavati_email, $web_name);
				$mail->Subject = "Sukhavati Booking Invoice: $book_id";
				$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
				$mail->MsgHTML($message);
				$mail->AddAttachment($path_invoice);
				$mail->Send();
			}catch (phpmailerException $e) {
				echo $e->errorMessage().$sukhavati_email;
			}catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		
		
		
	}
	
	
	function send_email_confirm_old($book_id){
		$content_email = $this->email_confirm_payment($book_id);
		$message = $content_email['content_email'];
		
		//generate invoice
		$content_invoice =  $this->content_invoice($book_id);
		$this->generate_pdf_file($content_invoice,$book_id);
		
		$path_invoice = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";;
		
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			//$sukhavati_email = 'adisend@localhost';
			//$email_to = 'adisend2@localhost';
			
			$file_attachment = $this->prepareAttachment($path_invoice);
			/*$file = $path_invoice;
			$file_size = filesize($file);
			$handle = fopen($file, "r");
			$content = fread($handle, $file_size);
			
			$file_content = chunk_split(base64_encode($content));
			fclose($handle);
			$uid = md5(uniqid(time()));
			$name = basename($file);*/
			
			
			$header = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$header .= "Reply-To: ".$sukhavati_email."\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $message."\r\n\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
			$header .= $file_content."\r\n\r\n";
			$header .= "--".$uid."--";		
			//echo $header;
			$subject = "Sukhavati Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
		
	}
	
	function prepareAttachment($path) {
        $rn = "\r\n";

        if (file_exists($path)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $ftype = finfo_file($finfo, $path);
            $file = fopen($path, "r");
            $attachment = fread($file, filesize($path));
            $attachment = chunk_split(base64_encode($attachment));
            fclose($file);

            $msg = 'Content-Type: \'' . $ftype . '\'; name="' . basename($path) . '"' . $rn;
            $msg .= "Content-Transfer-Encoding: base64" . $rn;
            $msg .= 'Content-ID: <' . basename($path) . '>' . $rn;
//            $msg .= 'X-Attachment-Id: ebf7a33f5a2ffca7_0.1' . $rn;
            $msg .= $rn . $attachment . $rn . $rn;
            return $msg;
        } else {
            return false;
        }
    }
	
	
	
	function email_confirm_payment($book_id){
		global $db;
		$OUT_TEMPLATE="email_confirm_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];	
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);
				$result['email'] =	$dt_book['email'];				
			}
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);
			//down payment
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('down_payment_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= $dt_paypal_dp['lgross'];		
			$t->set_var('downpayment',$downpayment);
			//full payment
			$dt_paypal_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
			$full_payment= $dt_paypal_fp['lgross'];		
			$t->set_var('full_payment',$full_payment);
						
			$t->set_var('rest_payment',$grand_total-($downpayment+$full_payment));
			$t->set_var('paypal_id',$dt_paypal_fp['ltxn_id']);
			$t->set_var('receiver_email',$dt_paypal_fp['lemail']);
			$t->set_var('payment_date',$dt_paypal_fp['lcreated_date']);
						
			$t->set_var('grand_total',$grand_total);
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		
		return $result;
	}
	
	function send_email_unconfirm($book_id){
		//echo $book_id;
		$content_email = $this->email_unconfirm_payment($book_id);
		//echo $content_email['content_email'];
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			/*$sukhavati_email = 'adisend@localhost';
			$email_to = 'adisend2@localhost';*/
			
			ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Unconfirm Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
		
	}
	
	function email_unconfirm_payment($book_id){
		global $db;
		$OUT_TEMPLATE="email_unconfirm_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];			
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);
				$result['email'] =	$dt_book['email'];				
			}
			$t->set_var('book_id',$book_id);			
			
			$t->set_var('grand_total',$grand_total);
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
			
		}
		
		return $result;
	}
	
	
	function send_email_cancel($book_id){
		//echo $book_id;
		$content_email = $this->email_cancel_payment($book_id);
		//echo $content_email['content_email'];
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			/*$sukhavati_email = 'adisend@localhost';
			$email_to = 'adisend2@localhost';*/
			
			ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Cancel Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
	}
	
	function email_cancel_payment($book_id){
		global $db;
		$OUT_TEMPLATE="email_cancel_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];			
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);
				$result['email'] =	$dt_book['email'];				
			}
			$t->set_var('book_id',$book_id);			
			
			$t->set_var('grand_total',$grand_total);
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
			
		}
		
		return $result;
	}
	
	
	
	
	function send_email_html(){
		
	}
	
	function get_contact_us_detail(){
		$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];	
		$result = array();
				
		$result['contact_us_email']= get_additional_field( $post_id, 'contact_us_email', $article_type);
		$result['contact_us_phone'] = get_additional_field( $post_id, 'contact_us_phone', $article_type);
		return $result;
	}
	
	
	function get_all_discount($data_booking){
		$cur= "USD";
		$dtDisc 	= json_decode($data_booking['ldiscount']);
		$valDisc 	= $dtDisc->discount_total + $dtDisc->discount_alltime_total;
		$txt_disc 	= ($valDisc>0? "Discount: $cur ".number_format($valDisc,2):"");
		$dtEarly 	= json_decode($data_booking['learly_bird']);
		$valEarly 	= $dtEarly->early_bird_total + $dtEarly->early_bird_alltime_total;
		$txt_disc	= $txt_disc.($txt_disc!="" && $valEarly>0 ?" and ":"").($valEarly>0? "Early Bird: $cur ".number_format($valEarly,2):"");
		$return = array();
		$return['txt_disc'] = $txt_disc;
		$return['val_disc'] = $valDisc + $valEarly;
		return $return;		
	}
	
	function get_all_surecharge($data_booking){
		$cur= "USD";
		$dtS 	= json_decode($data_booking['lsurecharge']);
		//print_r($dtS);
		$li = "";$valSc=0;
		if(!empty($dtS)){
			
			$valSc	= $dtS->surecharge_total+ $dtS->surecharge_alltime_total;
			$li = ($dtS->surecharge_total!=0 ? "<li>".$dtS->surecharge_txt_full." = $cur ".number_format($dtS->surecharge_total,0)."</li>":"");
			$li.= ($dtS->surecharge_alltime_total!=0? "<li>".$dtS->surecharge_alltime_txt_full." = $cur ".number_format($dtS->surecharge_alltime_total,0)."</li>":"");	
		}
		
		$return = array();
		$return['li_surecharge'] = $li;
		$return['valSc'] = $valSc;
		return $return;		
	}
	
	
	
	function single_detail_book_history($book_id){
		global $db;
		$OUT_TEMPLATE="single-history.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'single_history_block',  'shb');
		
		$result = array();
		$num_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'num_row');		
		
		$arrival_date=0;$departure_date=0;
		if($num_booking>0){
			$grand_total = 0; 
			$q = $db->prepare_query("select *, a.ltotal total_booking, b.ltotal total_each_villa from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b on a.lbook_id=b.lbook_id where a.lbook_id=%d",$book_id);
			$r = $db->do_query($q);
			
			$sub_total_all = 0;$total = 0;$li_surecharge = "";$surechargeVal = 0;
			while($data_booking = $db->fetch_array($r)){
				
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $data_booking['total_booking'];
				$total_each_villa = $data_booking['total_each_villa'];
				
				//data discount
				$data_discount = $this->get_all_discount($data_booking);
				$list_discount = "";$class_total="";$discount_text="";			
				if(!empty($data_discount['val_disc'])){
					$discount_text= $data_discount['txt_disc'];	
					$class_total = 'strike-through';
					$list_discount = "<li>".number_format($data_discount['val_disc'],2)."</li>";
				}
				$sub_total_all = $total_each_villa - $data_discount['val_disc'];
				
				$data_surecharge = $this->get_all_surecharge($data_booking);
				$li_surecharge .= $data_surecharge['li_surecharge'];
				$surechargeVal += $data_surecharge['valSc'];
				$t->set_var('discount_text',$discount_text);	
				$list_total = "<li class=\"$class_total\">".number_format($total_each_villa,2)."</li>";
				$temp_total = "<ul class=\"list-total\">$list_total $list_discount</ul>";				
				$t->set_var('txt_discount',($discount_text!="" ? "<br>$discount_text":""));
				$t->set_var('sub_total',$temp_total);
				$status = $dt_book['book_status'];				
				
				$t->set_var('book_status',$status);
				$t->Parse('lbb', 'list_book_block', true);
				$arrival_date=$data_booking['lcheck_in'];
				$departure_date=$data_booking['lcheck_out'];
				
				//0:cancel,1:check-availability;2: dp-waiting,3:full-pay waiting,4:confirm,5:uncofirm,6:receive deposite payment,7:waiting balance payment			
				$dt_deposit = json_decode($data_booking['ldeposit']);
				$payment_mode = "";
				if($dt_deposit->deposit < 100){
					$payment_mode = "<li class=\"waiting-dp-payment\"><span></span><div>Awaiting Down Payment</div></li>
									<li class=\"receive-dp-payment\"><span></span><div>Receive Down Payment</div></li>
									<li class=\"waiting-balance-payment\"><span></span><div>Awaiting Balance Payment</div></li>
									";
				}else{
					$payment_mode = "<li class=\"waiting-full-payment\"><span></span><div>Awaiting Full Payment</div></li>";
				}
				
				$t->set_var('payment_mode',$payment_mode);	
				//print_r($dt_deposit);
				//$t->set_var('full_mode',($dt_deposit->deposit==100?'':'style="display:none;"'));	
				//$t->set_var('balance_mode',($dt_deposit->deposit<100?'':'style="display:none;"'));
			}
			
			
			$t->set_var('book_id',$book_id);
			$t->set_var('sub_total_all',number_format($sub_total_all,2));
			
			//get surcharge
			if($surechargeVal!=0){
				$surcharge_temp = $this->cont_surcharge_booking_detail($surechargeVal,$li_surecharge,$total);
			}	
			$t->set_var('surcharge_temp',$surcharge_temp);			
			//detail data
			$dt_book = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
			$t->set_var('name', $dt_book['lfirst_name'].' '.$dt_book['llast_name']);
			$t->set_var('fname', $dt_book['lfirst_name']);
			$t->set_var('lname', $dt_book['llast_name']);
			$t->set_var('email',$dt_book['lemail']);
			$t->set_var('phone',$dt_book['lphone']);
			$t->set_var('adult',$dt_book['ladult']);
			$t->set_var('child',$dt_book['lchild']);
			$t->set_var('message',$dt_book['lspecial_note']);	
			$t->set_var('message_val',str_replace('<br />','',$dt_book['lspecial_note']));	
			
			$is_paid = 0;
			$rest_paid = $grand_total;
			//get_downpayment
			$downpayment = "";
			$data_dp = $this->get_downpayment_single_history($book_id);
			if(!empty($data_dp)){
				$downpayment = $data_dp['container'];
				$rest_paid = $rest_paid - $data_dp['dp'];
				$is_paid++;
			}
			$t->set_var("downpayment",$downpayment);
			
			$balancepayment = "";
			if($dt_deposit->deposit <100){
				$data_fp = $this->get_fullpayment_single_history($book_id);
				if(!empty($data_fp)){
					$balancepayment = $data_fp['container'];
					$rest_paid = $rest_paid - $data_fp['fp'];
					$is_paid++;
				}	
			}
			
			$t->set_var("fullpayment",$balancepayment);
			
			$cont_rest_paid = "";
			//if($is_paid>0){$cont_rest_paid = $this->get_restpayment($rest_paid);}
			$t->set_var("restpayment",$cont_rest_paid);
			
			//$this->set_detail_booking_new($t,$book_id);
			
			
			$result['process'] = 'success';
			$result['status']  = $status;
			$result['deposit'] = $dt_deposit->deposit;
			$result['data'] = $t->Parse('shb', 'single_history_block', false);	
			
		}else{
			$result['process'] = 'failed';
		}	
		
		//return $t->Parse('shb', 'single_history_block', false);
		echo json_encode($result);
	}
	
	function set_detail_booking_new($t,$book_id){
		$detail_booking = $this->get_data('lumonata_accommodation_booking_detail',"where lbook_id=$book_id",'array');
		$additional_service = json_decode($detail_booking['ladditional_service']);
		$text_additional_service="";
		$checked = 'checked="checked"';
		$ads_array  =array('Detox'=>0,'Stress Relief'=>0,'Weight Loss'=>0,'Meditation'=>0,'Chronic Pain'=>0,'Others'=>0);
		foreach($additional_service as $as){
			if (array_key_exists($as,$ads_array)) $ads_array[$as]=1;
			else {
				$ads_array['Others']=1;
				$t->set_var('checked_other',$checked);
				$t->set_var('other_ads',$as);
			}			
			if($text_additional_service!="") $text_additional_service .= ', '.$as;
			else $text_additional_service = $as;
		}
		
		//validasi checked
		if($ads_array['Detox']==1) $t->set_var('checked_detox',$checked);
		if($ads_array['Stress Relief']==1) $t->set_var('checked_stress_relief',$checked);
		if($ads_array['Weight Loss']==1) $t->set_var('checked_weight_lost',$checked);
		if($ads_array['Meditation']==1) $t->set_var('checked_meditation',$checked);
		if($ads_array['Chronic Pain']==1) $t->set_var('checked_chronic_pain',$checked);
		if($ads_array['Others']==0) $t->set_var('other_ads','');
		
		
		if($text_additional_service=="")$text_additional_service="-";
		$t->set_var('additional_service',$text_additional_service);
		if($detail_booking['ladditional_cost']==1){
			$t->set_var('additional_cost','Yes');
			$t->set_var('yes_ac',$checked);	
		}else{
			$t->set_var('additional_cost','No');
			$t->set_var('no_ac',$checked);	
		} 		
		
		if($detail_booking['lheard_about_us'] !=""){
			$t->set_var('heard_about_us',$detail_booking['lheard_about_us']);	
			$t->set_var($detail_booking['lheard_about_us'],'selected');			}
		else $t->set_var('heard_about_us',"-");
	}
	
	function cont_surcharge_booking_detail($the_surcharge, $temp_surcharge,$grand_total){
		$temp = "
					<div class=\"desc_gt surcharge cust\">
    					<div><label>Surcharge</label><ul class=\"list-surcharge\">$temp_surcharge</ul></div>
     					<div>".number_format($the_surcharge,2)."</div>
        				<div class=\"clear\"></div>
    				</div>		
					<div class=\"desc_gt cust\">
    					<div>Grand Total</div>
     					<div>".number_format($grand_total,2)."</div>
        				<div class=\"clear\"></div>
    				</div>";
		return $temp;		
	}	
	
	
	function get_data_discount($villa_id,$arrival_date,$derpature_date,$price,$date_book){
		global $db;		
		
		//get roomtype
		$rr = $this->get_data_additional_villa($villa_id,'room_type');
		$dr = $db->fetch_array($rr);			
		//get discount
		$room_type_id = $dr['lrule_id'];
		//echo $room_type_id.'-'.$villa_id.'-'.$arrival_date.'-'.$derpature_date.'-'.$price.'-'.$date_book.'#';
		$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$date_book);
		return $data_discount;						  
					  
	}
	
	
	
	function get_downpayment_single_history($book_id){
		$result = array();
		$data_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
		$dbk = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
		if(!empty($data_dp)){
			$downpayment = $data_dp['lgross'];
			$dtDp  = json_decode($dbk['ldeposit']); //$data_dp['lpresentase_dp'].'%';
			$presentase = $dtDp->deposit.'%';
			
			$dp_container = "<div class=\"desc_gt\">
								<div>".($dtDp->deposit==100? "Full Payment":"Down Payment ($presentase)")."</div>
								<div>".number_format($downpayment,2)."</div>
								<div class=\"clear\"></div>
							</div>";
			$result['container'] =  $dp_container;
			$result['dp'] = $downpayment;				
			//$t->set_var("downpayment",$dp_container);
		}		
		return $result;
		
	}
	
	function get_fullpayment_single_history($book_id){
		$result = array();
		$data_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='fp'",'array');
		if(!empty($data_fp)){
			$fullpayment = number_format($data_fp['lgross'],2);
			
			$fp_container = "<div class=\"desc_gt\">
								<div>Balance Payment</div>
								<div>$fullpayment</div>
								<div class=\"clear\"></div>
							</div>";
			$result['container'] =  $fp_container;
			$result['fp'] = $fullpayment;				
			//$t->set_var("downpayment",$dp_container);
		}
		
		return $result;
		
	}
	
	
	function get_restpayment($rest_payement){
		$dp_container = "<div class=\"desc_gt\">
							<div>Rest Payment</div>
							<div>".number_format($rest_payement,2)."</div>
							<div class=\"clear\"></div>
						</div>";						
		return $dp_container;				
	}
	
	function get_data_booking($data_booking){
		$result = array();
		$id_villa 					= $data_booking['larticle_id'];
		$result['villa_name'] 		= $this->get_villa_name($id_villa);
		$result['arrival_date'] 	= date('d M Y',$data_booking['lcheck_in']);
		$result['derpature_date'] 	= date('d M Y',$data_booking['lcheck_out']);
		$result['days'] 			= ($data_booking['lcheck_out']-$data_booking['lcheck_in'])/86400;
		$result['total']			= $data_booking['total_booking'];
		
		
		if($data_booking['lbook_by']==0){
			$name = $data_booking['lfirst_name'].' '.$data_booking['llast_name'];	
		}else{
			$data_user = $this->get_data('lumonata_users',"where luser_id=".$data_booking['lbook_by'],'array');	
			$name = $data_user['ldisplay_name'].'('.ucfirst($data_user['luser_type']).')';
		}
		/*if(isset($data_booking['lusername']) && $data_booking['lusername'] !='' && isset($data_booking['lname']) && $data_booking['lname'] =='admin'){
			$username = $data_booking['lusername'];
			$data_user = $this->get_data('lumonata_users',"where lusername='$username'",'array');	
			$name = $data_user['ldisplay_name'].'('.ucfirst($data_user['luser_type']).')';
		}else{
			$name = $data_booking['lname'];	
		}*/
		
		
		
		$result['name']				= $name;
		$result['email']			= $data_booking['lemail'];
		$result['phone']			= $data_booking['lphone'];
		$result['message']			= $data_booking['lspecial_note'];
		//$result['country']		= $data_booking['lcountry'];
		//$result['guest']			= $data_booking['lguest'];
		$result['guest']			= $data_booking['ladult'] + $data_booking['lchild'];
		$result['book_status']		= $data_booking['lstatus'];
		$result['book_date']		= date('d M Y',$data_booking['lbook_id']);
		return $result;
	}
	
	function get_all_list_accom_history($val_1,$val_2,$show_label_villa=true,$status='all',$page){
		global $db;
		$OUT_TEMPLATE="list-villa-reservation.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'booking_block', 'bb');
		$t->set_block('home', 'list_block',  'lb');
		
		$view = DEFAULT_SHOW_CONTENT;		
		$pages = ($page-1)*$view; 
		//$limits = "";		
		$limits = "limit $pages,$view";
		
		if($status=='all'){
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa' order by lcheck_in");
			$num_booking = $this->get_data('lumonata_accommodation_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa'  group by lbook_id order by lcheck_in",'num_row');		
		}else {
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa' and lstatus=$status order by lcheck_in");		
			$num_booking = $this->get_data('lumonata_accommodation_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa' and lstatus=$status group by lbook_id order by lcheck_in",'num_row');	
		}
			
		$new_list_booking = array(); //make new list
		while($data_booking = $db->fetch_array($list_booking)){
			$id_booking  = $data_booking['lbook_id'];
			$index_booking = (isset($new_list_booking[$id_booking]) ? count($new_list_booking[$id_booking]) : 0);
			$new_list_booking[$id_booking][$index_booking] = $data_booking;	
		}
		
		//validate limit list
		$new_list_booking_2 = array();
		$the_view = $pages+ $view;
		//echo $the_view;
		$n = 0;
		foreach($new_list_booking as $id_book => $data_books){
			if($n >= $pages && $n <$the_view){
				//$index_book = count($new_list_booking_2[$id_book]);
				$new_list_booking_2[$id_book]= $data_books;		
			}
			$n++;
		}
		
		
		$grand_total = 0;
		foreach($new_list_booking_2 as $id_booking=> $list_book_villa){
			//just for get villa name & grand_total
			$total = 0;
			$villa_list ="";
			$arrival_date=0;$departure_date=0;
			foreach($list_book_villa as $real_book_data){
				$book_id = $real_book_data['lbook_id'];
				$book_detail = $this->get_data('lumonata_accommodation_booking_detail',"where lbook_id=$book_id",'array');
				$id = $book_detail['larticle_id'];
				$villa_name = $this->get_villa_name($id);
				$villa_list .= "<li>$villa_name</li>";
				$days = ($real_book_data['lcheck_out']-$real_book_data['lcheck_in'])/86400;
				$t->set_var('check',$real_book_data['lbook_id']);
				$t->set_var('id_booking',$real_book_data['lbook_id']);
				$t->set_var('arrival_date',date('d M Y',$real_book_data['lcheck_in']));
				$t->set_var('derpature_date',date('d M Y',$real_book_data['lcheck_out']));
				$t->set_var('night',$days);								
				$total = $real_book_data['ltotal'];
				
				if($data_booking['lusername'] !=''){
					$username = $real_book_data['lusername'];
					$data_user = $this->get_data('lumonata_users',"where lusername='$username'",'array');	
					$name = $data_user['ldisplay_name'].'('.ucfirst($real_book_data['luser_type']).')';
				}else{
					$name = $real_book_data['lfirst_name'].' '.$real_book_data['llast_name'];	
				}
				$t->set_var('order_by',$name);
				$dtDeposit = json_decode($real_book_data['ldeposit']);
				$t->set_var('status',$this->get_status_book_name($real_book_data['lstatus'],$dtDeposit->deposit));
				$arrival_date=$real_book_data['lcheck_in'];$departure_date=$real_book_data['lcheck_out'];
			}//end real book data			
			if($show_label_villa)$t->set_var('villa_label',"<ul class=\"list-villa\">$villa_list</ul>");
			$t->set_var('total',number_format($total,2));
			$t->Parse('bb', 'booking_block', true); 
			$grand_total = $grand_total + $total;			
		}
		
		$result = array();
		if($grand_total==0) {
			$result['temp'] = "<div class=\"wrapper form table list-reservation-history center old-data\">No Booking history found...</div>";	
		}else{
			$result['pagging'] = $this->get_pagging($val_1,$val_2,$status,$page,$num_booking);			 
			$result['temp'] = $t->Parse('lb', 'list_block', false);		
		}
		$result['grand_total'] = number_format($grand_total,2);
		//print_r($result);
		return json_encode($result);
	}
	
	
	function get_pagging($val_1,$val_2,$status='all',$page=0,$num_booking){
		$view = DEFAULT_SHOW_CONTENT;
		if(empty($num_booking) or $num_booking=='null') $num_booking= 0;
		$num_list_pagging = ceil($num_booking/ $view);	
		//echo $num_list_pagging;
		if($num_list_pagging > 1){
			//$page = $page + 1;
			$list = "";
			for($i=1;$i<=$num_list_pagging;$i++){
				if($page==$i) $class = 'class="selected"';
				else $class = '';
				$list .= "<li $class rel=\"$i\">$i</li>";	
			}
			
			$cont = "<div class=\"wrapper form table list-reservation-history cont-pagging old-data $num_booking\">
						<ul class=\"list_pagging\">
							$list
						</ul>             
					 </div>	";	
		}else {
			$cont = "";	
		}
		
		return $cont;	
			
	}
	
	function get_status_book_name($id=1,$deposit){
		if($id==1) return 'Check Availability';
		else if($id==2)return 'Awaiting Down Payment';
		else if($id==3)return ($deposit==100?'Awaiting Full Payment':'Awaiting Balance Payment');
		else if($id==4)return 'Confirm';
		else if($id==5)return 'Unconfirm';
		else if($id==6)return 'Receive Down Payment';
		else if($id==0)return 'Canceled';
	}
	
	function get_list_accom_history_single_villa($id,$val_1,$val_2,$show_label_villa=false,$status='all',$page=1,$view=1){
		global $db;
		$OUT_TEMPLATE="list-villa-reservation.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'booking_block', 'bb');
		$t->set_block('home', 'list_block',  'lb');
		
		$view = DEFAULT_SHOW_CONTENT;
		$pages = ($page-1)*$view; 
		//$limits = "";		
		$limits = "limit $pages,$view";
		
		if($status=='all'){
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where larticle_id=$id and lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa' order by lcheck_in $limits");
			$num_booking = 	 $this->get_data('lumonata_accommodation_booking',"where larticle_id=$id and lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='villa' order by lcheck_in",'num_row');
		}else {
			$list_booking = $this->get_data('lumonata_accommodation_booking',"where larticle_id=$id and lcheck_in >= $val_1 and lcheck_in <$val_2 and lstatus=$status and lbook_type ='villa' order by lcheck_in $limits");
			$num_booking = $this->get_data('lumonata_accommodation_booking',"where larticle_id=$id and lcheck_in >= $val_1 and lcheck_in < $val_2 and lstatus=$status and lbook_type ='villa' order by lcheck_in",'num_row');
		}
		
		
		$new_list_booking = array();
		while($data_booking = $db->fetch_array($list_booking)){
		//	print_r($data_booking);
			$id_booking = $data_booking['lbook_id'];
			$the_same_booking = $this->get_data('lumonata_accommodation_booking',"where lbook_id=$id_booking and lcheck_in >= $val_1 and lcheck_in <$val_2 order by lcheck_in");
			
			while($same_data_booking = $db->fetch_array($the_same_booking)){
				$index_booking = count($new_list_booking[$id_booking]);
				$new_list_booking[$id_booking][$index_booking] = $same_data_booking;	
			}
		}
		
		$grand_total = 0;
		foreach($new_list_booking as $id_booking=> $list_book_villa){
			//just for get villa name & grand_total
			$total = 0;
			$villa_list ="";
			$arrival_date=0;$departure_date=0;
			foreach($list_book_villa as $real_book_data){
				$id = $real_book_data['larticle_id'];
				$villa_name = $this->get_villa_name($id);
				$villa_list .= "<li>$villa_name</li>";
				$days = ($real_book_data['lcheck_out']-$real_book_data['lcheck_in'])/86400;
				$t->set_var('check',$real_book_data['lbook_id']);
				$t->set_var('id_booking',$real_book_data['lbook_id']);
				$t->set_var('arrival_date',date('d M Y',$real_book_data['lcheck_in']));
				$t->set_var('derpature_date',date('d M Y',$real_book_data['lcheck_out']));
				$t->set_var('night',$days);
				
				$price = $this->price_villas($id,$real_book_data['lcheck_in'],$real_book_data['lcheck_out'],$days);
				//$total = $total + $price;
				//data discount
				$data_discount = $this->get_data_discount($real_book_data['larticle_id'],$real_book_data['lcheck_in'],$real_book_data['lcheck_out'],$price,$real_book_data['lbook_id']);
				//$list_discount = "";$class_total="";$discount_text="";			
				if(!empty($data_discount))	$price = $data_discount['price_discount'];							
				$total = $total + $price;				
				
				
				if($data_booking['lusername'] !=''){
					$username = $real_book_data['lusername'];
					$data_user = $this->get_data('lumonata_users',"where lusername='$username'",'array');	
					$name = $data_user['ldisplay_name'].'('.ucfirst($real_book_data['luser_type']).')';
				}else{
					$name = $real_book_data['lname'];	
				}
				$t->set_var('order_by',$name);
				$dtDeposit = json_decode($real_book_data['ldeposit']);
				$t->set_var('status',$this->get_status_book_name($real_book_data['lstatus'],$dtDeposit->deposit));
				$arrival_date=$real_book_data['lcheck_in'];$departure_date=$real_book_data['lcheck_out'];
			}//end real book data
			//echo 'ds';
			if($show_label_villa)$t->set_var('villa_label',"<ul class=\"list-villa\">$villa_list</ul>");
			
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$departure_date,$total,'array');
			if(!empty($data_surcharge)){	
				$total = $total + $data_surcharge['the_surcharge'];
				
			}			
			$t->set_var('total',$total);
			$t->Parse('bb', 'booking_block', true); 
			
			
			$grand_total = $grand_total + $total;
		}
		
		$result = array();
		if($grand_total==0) $result['temp'] = "<div class=\"wrapper form table list-reservation-history center old-data\">
													No Booking history found...
												</div>";
		else{ 
			$result['pagging'] = $this->get_pagging($val_1,$val_2,$status,$page,$num_booking);	
			$result['num_booking'] = $num_booking.'#';
			$result['temp'] = $t->Parse('lb', 'list_block', false);	
		}
		$result['grand_total'] = $grand_total;
		echo json_encode($result);
	}
	
	
	
	
	
	
	function price_villas($villa_id,$arrival_date,$departure_date,$days,$book_id=0){
		$error = 0;
		$price = 0;
		for($i=0 ;$i<=$days ; $i++){
			$the_date = $arrival_date + ($i*86400);
			if($the_date >= $arrival_date && $the_date < $departure_date) {
				$data = $this->get_data('lumonata_availability',"where ldate=$the_date and lacco_id=$villa_id",'array');
				if(!empty($data)) $price = $price + $data['lrate'];
				else $error++;
			}
		}//end for
		
		if($error==0)
			return $price;
		else return 0;
	}
	
	function get_villa_name($villa_id){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->villa_room_type_name($villa_id);
	}
	
	
	function get_data($t,$w,$r='result_only'){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->data_tabels($t,$w,$r);
	}
	
	function get_data_additional_villa($id,$type){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->get_data_additional_villa($id,$type);
	}
	
	function single_villa_discount_and_earlybird($roomtype_id,$arrival_date,$departure_date,$price,$return_type,$date_now){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->single_villa_discount_and_earlybird($roomtype_id,$arrival_date,$departure_date,$price,$return_type,$date_now);
	}	
	
	function single_villa_get_surcharge($arrival_date,$departure_date,$price,$return_type='array'){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->single_villa_get_surcharge($arrival_date,$departure_date,$price,$return_type);	
	}	
	
	
	function get_temp_all_villa($temp,$show_all,$show_empty){
		require_once("../booking-engine/apps/calendar/class.calendar.php");
		$this->calendar = new calendar();
		return $this->calendar->temp_all_villa($temp,$show_all,$show_empty);	
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		//return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		//return $this->meta_key;
	}
	
}
?>