<?php
	class account extends db{
		function account($appName){
			$this->appName=$appName;
			$this->setMetaTitle("account");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			//print_r($_POST);
			//print_r($_GET);//, [bool return])
			//print_r($_COOKIE);//, [bool return])
			
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
				
				$_SESSION['othUsrID'] = $_COOKIE['member_logged_ID'];
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
				
				$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lproduct_id=%d AND lproduct_type=%d", $_SESSION['product_id'], $_SESSION['product_type']);
				$res2=parent::query($sql);
				$user2=parent::fetch_array($res2);
				
				$_SESSION['othUsrID'] = $user2['lmember_id'];
			}			
			
			global $itemPerPage;
			$itemPerPage = 10;
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			
			if( !empty($_POST['theFormAction']) && !empty($_POST['values']) ){ $this->doThePayment();	}
			if( !empty($_POST) && empty($_POST['searchNow']) && empty($_POST['showBill']) && empty($_POST['showEarning']) && empty($_POST['theFormAction']) ){
				// CHECK EMPTY ===============================================================
				$notEmpty = true;
				if(empty($_POST['realSalutation'])){ $notEmpty = false;}
				if(empty($_POST['fName'])){ $notEmpty = false;}
				if(empty($_POST['lName'])){ $notEmpty = false;}
				if(empty($_POST['email'])){ $notEmpty = false;}
				if(empty($_POST['phone'])){ $notEmpty = false;}
				if(empty($_POST['country'])){ $notEmpty = false;}
				if(empty($_POST['state'])){ $notEmpty = false;}
				if(empty($_POST['city'])){ $notEmpty = false;}
				if(empty($_POST['postalCode'])){ $notEmpty = false;}
				
				if($notEmpty){
					$time 			= time();
					$lsalutation 	= $_POST['realSalutation'];
					$lfname 		= $_POST['fName'];
					$llname 		= $_POST['lName'];
					$lemail 		= $_POST['email'];
					$lphone 		= $_POST['phone'];
					$lcountry_id 	= $_POST['country'];
					$lstate 		= $_POST['state'];
					$lcity 			= $_POST['city'];
					$lpostal_code 	= $_POST['postalCode'];
					
					$tmp = "UPDATE lumonata_members SET 
							lsalutation=%s,
							lfname=%s,
							llname=%s,
							lemail=%s,
							lphone=%s,
							lcountry_id=%d,
							lstate=%s,
							lcity=%s,
							lpostal_code=%s, 
							ldlu=%d 
							WHERE lmember_id=%s";
					$sql=parent::prepare_query($tmp, $lsalutation, $lfname, $llname, $lemail, $lphone, $lcountry_id, $lstate, $lcity, $lpostal_code, $time, $_SESSION['othUsrID']);
					$res=parent::query($sql);
					if($res){
						$error .=   "jQuery('.notifBlockSaved').slideDown().delay(3000).slideUp();";
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"account",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						}
					} 
					
					$old = $_POST['oldPass'];
					$new = $_POST['newPass'];
					if(!empty($new)){
						if(empty($old)){
							$error .=   "jQuery('.notifBlockPassNotOk').slideDown().delay(3000).slideUp();";
						}else{
							$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id=%s", $_SESSION['othUsrID']);
							$res=parent::query($sql);
							$user=parent::fetch_array($res);
							
							if( $user['lpassword']==md5($old) ){
								$tmp = "UPDATE lumonata_members SET 
										lpassword=%s,
										lpass=%s, 
										ldlu=%d 
										WHERE lmember_id=%s";
								$sql=parent::prepare_query($tmp, md5($new), md5($new), $time, $_SESSION['othUsrID']);
								$res=parent::query($sql);
								if($res){
									$error .=   "jQuery('.notifBlockPassOk').slideDown().delay(3000).slideUp();";
								}else{
									$error .=   "jQuery('.notifBlockPassNotOk').slideDown().delay(3000).slideUp();";
								}
							}else{
								$error .=   "jQuery('.notifBlockPassNotOk').slideDown().delay(3000).slideUp();";
							}
						}
					}
					
				}else{
					$error .= "jQuery('.notifBlockNotSaved').slideDown().delay(3000).slideUp();";
				}
				$t->set_var('jsAction', $error);
			}
			
			$sql  = parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id=%s", $_SESSION['othUsrID']);
			$res  = parent::query($sql);
			$user = parent::fetch_array($res);
			$key  = $user['lproduct_id'];
			
			$t->set_var('salutation',	$user['lsalutation']);
			$t->set_var('fName',		$user['lfname']);
			$t->set_var('lName',		$user['llname']);
			$t->set_var('email',		$user['lemail']);
			$t->set_var('phone',		$user['lphone']);
			$t->set_var('country_list',	$this->getCountryList($user['lcountry_id']));
			$t->set_var('state',		$user['lstate']);
			$t->set_var('city',			$user['lcity']);
			$t->set_var('postalCode',	$user['lpostal_code']);
			
			
			
			//=========================================================================================================================================================================
			$this->paymentTableInit();
			$t->set_var('thePaymentHistory', $this->paymentTableRead() );
			$t->set_var('theEarningHistory', $this->theEarning() );
			$t->set_var('selectThePeriod', $this->getThePeriod() );
			$t->set_var('selectTheEarnPeriod', $this->getTheEarnPeriod($_POST['earnPeriod']) );
			
			$t->set_var('bookingHistory', $this->getMyBooking() );
			$t->set_var('inDate' , $_POST['dateBegin']);
			$t->set_var('outDate', $_POST['dateEnd']);
			$t->set_var('jsAction2', "jQuery('[name=sType] option[value=".$_POST['sType']."]').attr('selected',true);");
			
			$page = $_GET['page'];
			if(empty($page)){ $page=1; }
			
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
						
			$sql=parent::prepare_query("SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d ", $pType, $pID);
			$res=parent::query($sql);					
			$nnn=parent::num_rows($res);
			
			$links = '';
			$nPage = floor($nnn/$itemPerPage);
			if( ( $nPage*$itemPerPage)<$nnn ){ $nPage++; }
			
			if($nPage>1 && empty($_POST['searchNow'])){
				for($jjj=1;$jjj<=$nPage;$jjj++){
					$thaUrl = '{http}{rootsite_url}/supplier/account/'.$jjj.'#history';
					$class = '';
					if($jjj==$page){ $class='class="active"'; }
					$links .= '<a '.$class.' href="'.$thaUrl.'">'.$jjj.'</a>';
				}
				
				if($page>1){ $prev = '<a href="{http}'.SITE_URL.'/supplier/account/'.($page-1).'#history">prev</a>'; }
				if($page<$nPage){ $next = '<a href="'.'{http}{rootsite_url}/supplier/account/'.($page+1).'#history">next</a>'; }
				
				$link = '<div class="navigation">
							'.$prev.'
							'.$links.'
							'.$next.'
						</div>';
			}
			$t->set_var('thePagingLinks', $link );
			
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function getCountryList($key){
			$sql=parent::prepare_query("SELECT * FROM lumonata_country");
			$res=parent::query($sql);
			while($rec=parent::fetch_array($res)){
				$add='';
				if($key==$rec['lcountry_id']){$add='selected="selected"';}
				$all .= '<option '.$add.' value="'.$rec['lcountry_id'].'">'.$rec['lcountry'].'</option>';
			}
			return $all;
		}
		
		function getMyBooking(){
			global $itemPerPage;
			
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			$tmp = "SELECT lcurrency FROM lumonata_currency WHERE lcurrency_id=%d";
			$sql=parent::prepare_query($tmp,$curID);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$theCurName = $result['lcurrency'];
			
			//=================================================================================================================
			//=================================================================================================================
			$addQue = '';
			if( !empty($_POST['searchNow']) ){
				if($_POST['sType']=='9'){
					$tmp = "";
				}else{
					$tmp = " AND lstatus=".$_POST['sType']." ";
				}
				$addQue .= $tmp;
				
				
				$tmp = ''; if(!empty($_POST['dateBegin'])){ $tmp = " AND lcreated_date>=".strtotime($_POST['dateBegin'])." "; } $addQue .= $tmp;
				$tmp = ''; if(!empty($_POST['dateEnd'])){ $tmp = " AND lcreated_date>=".strtotime($_POST['dateEnd'])." "; } $addQue .= $tmp;
			
				$addQue .= " ORDER BY lbooking_id DESC ";
				
				$sql=parent::prepare_query("SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d ".$addQue, $pType, $pID);
				$res=parent::query($sql);
			}else{
				$page = $_GET['page'];
				if(empty($page)){ $page=1; }
				$start = ($page-1)*$itemPerPage;
				$addQue = " ORDER BY lbooking_id DESC LIMIT ".$start.",".$itemPerPage;
				
				$sql=parent::prepare_query("SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d ".$addQue, $pType, $pID);
				$res=parent::query($sql);
			}
			//echo $sql;
			//=================================================================================================================
			//=================================================================================================================
			
			
			
			while($rec=parent::fetch_array($res)){
				$detConten1 = '';
				$detConten2 = '';
				$add        = '';
				
				if($rec['lproduct_type']==1){//acco
					$tmp = "SELECT * FROM lumonata_accommodation WHERE lacco_id=%d";  
				}else{//tour or activities
					$tmp = "SELECT * FROM lumonata_tour WHERE ltour_id=%d";
				}
				$sql=parent::prepare_query($tmp,$rec['lproduct_id']);
				$re2=parent::query($sql);
				$det=parent::fetch_array($re2);
				
				$detConten1  = '<p style="border-top: solid 1px #ccc; border-right: solid 1px #ccc; width:352px;">										
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Full Name</span>
										: '.$rec['salutation'].' '.$rec['lfname'].' '.$rec['llname'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Email</span>
										: '.$rec['lemail'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Phone</span>
										: '.$rec['lphone'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Country</span>
										: '.$country['lcountry'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">State / Province</span>
										: '.$rec['lstate'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">City</span>
										: '.$rec['lcity'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Postal Code</span>
										: '.$rec['lzip_code'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Number of Adult</span>
										: '.$rec['ladult'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Number of Children</span>
										: '.$rec['lchild'].'
									</span>
								</p>';
				
				
				
				if($rec['lproduct_type']==1){
					$tmp="SELECT lumonata_accommodation_booking_detail.*, lumonata_accommodation_type.lname AS roomName FROM lumonata_accommodation_booking_detail INNER JOIN lumonata_accommodation_type
						  ON lumonata_accommodation_booking_detail.lacco_type_id=lumonata_accommodation_type.lacco_type_id WHERE lbooking_id='%s'";
					$sql=parent::prepare_query($tmp,$rec['lbooking_id']);
					$re2=parent::query($sql);
					
					$detConten2 = '';
					$detConten3 = '';
					$detConten3 .= '<tr class="head">
										<td>No.</td>
										<td>Room Name</td>
										<td>Rate</td>
										<td>Req.<br />Room</td>
										<td>Sub Total</td>
										<td>Commission</td>
										<td>Comm.<br />Fee</td>
										<td>Name</td>
										<td>Email</td>
										<td>Aditional Request</td>
									</tr>';
					$jjj = 0;
					while( $book=parent::fetch_array($re2) ){
						$jjj++; 
						$tmp = "SELECT * FROM lumonata_country WHERE lcountry_id=%d";
						$sql=parent::prepare_query($tmp,$book['lcountry_id']);
						$re3=parent::query($sql);
						$country = parent::fetch_array($re3);
						
						$detConten3 .= '<tr>
											<td>'.$jjj.'</td>
											<td>'.$book['roomName'].'</td>
											<td>'.$theCurName.' '.number_format($book['lrate'],2).'</td>
											<td>'.$book['lroom'].'</td>
											<td>'.$theCurName.' '.number_format($book['ltotal'],2).'</td>
											<td>'.$book['lcommision'].'%</td>
											<td>'.$theCurName.' '.number_format($book['lcommision_fee'],2).'</td>
											<td>'.$book['salutation'].' '.$book['lfname'].' '.$book['llname'].'</td>
											<td>'.$book['lemail'].'</td>
											<td>'.$book['ladditional_request'].'</td>
										</tr>';
						
						$detConten2 .= '<p style="border-top: solid 1px #ccc;">
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">No</span>
												: '.$jjj.'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Room Name</span>
												: '.$book['roomName'].'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Rate</span>
												: '.$theCurName.' '.number_format($book['lrate'],2).'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Room Required</span>
												: '.$book['lroom'].'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Sub Total</span>
												: '.$theCurName.' '.number_format($book['ltotal'],2).'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Commision</span>
												: '.$book['lcommision'].'%
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Commision Fee</span>
												: '.$theCurName.' '.number_format($book['lcommision_fee'],2).'
											</span>
											
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Name</span>
												: '.$book['salutation'].' '.$book['lfname'].' '.$book['llname'].'
											</span>
											<span style="display:block; width:100%; line-height:1.2em;">
												<span style="float:left; width :140px;">Email</span>
												: '.$book['lemail'].'
											</span>
											<span style="display:block; width:100%; line-height:1.2em; ">
												<span style="float:left; width :140px;">Additional Req</span>
												<span style="float:left;">:&nbsp;</span>
												<span style="float left; width :550px;">'.$book['ladditional_request'].' </span>
											</span>
										</p>';			
					}
					$detConten3 = '<table width="100%"0 class="myBookingTable" >'.$detConten3.'</table>';
					$add = '<p style="background:#f6f6f6;border-top:solid 1px #ccc;">Room Details</p>'.$detConten3;
				}				
				
				$sqlx = parent::prepare_query('SELECT * FROM lumonata_accommodation_booking_payment WHERE lbooking_id=%s', $rec['lbooking_id'] );
				$quex = parent::query($sqlx);
				$paymentDetails = parent::fetch_array($quex);
				
				$paymentContent = '<p style="border-top: solid 1px #ccc; width:352px;">	
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Card Type</span>
										: '.$paymentDetails['lcard_type'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Credit Card No</span>
										: '.$paymentDetails['lcard_number'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Cardholder\'s Name</span>
										: '.$paymentDetails['lholder_name'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Expiry Date</span>
										: '.$paymentDetails['lexp_month'].'/'.$paymentDetails['lexp_year'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Issuing/Providing Bank</span>
										: '.$paymentDetails['lcard_bank'].'
									</span>
									<span style="display:block; width:100%; line-height:1.2em;">
										<span style="float:left; width :140px;">Country of Issuing Bank</span>
										: '.$this->getCountryName($paymentDetails['lbank_country']).'
									</span>
								</p>';
								
								
								
								
				//-----------------------------------------------------------------------------------------------------------------------
				//ADDITIONALSERVICES -------------------------------------------------------------------------------------------------------------
				$sqlx = parent::prepare_query('SELECT * FROM lumonata_accommodation_booking_additionalservices WHERE lbooking_id=%s', $rec['lbooking_id'] );
				$quex = parent::query($sqlx);
				$addServices = '';
				$iii = 1;
				$servToto = 0;
				while( $recx = parent::fetch_array($quex) ){
					$pName = $this->globalAdmin->getValueField("lumonata_accommodation_aditionalservice_package","lpackage","lpackage_id",$recx['lpackage_id']);
					$sID   = $this->globalAdmin->getValueField("lumonata_accommodation_aditionalservice_package","lservice_id","lpackage_id",$recx['lpackage_id']);
					$sName = $this->globalAdmin->getValueField("lumonata_accommodation_aditionalservice","ltitle","lservice_id",$sID);
					$addServices .= '<tr>
										<td style="text-align:center;">'.$iii.'</td>
										<td><strong>'.$sName.' : </strong>'.$pName.'</td>
										<td style="text-align:center;">'.number_format($recx['lprice'],2).'</td>
										<td style="text-align:center;">'.$recx['lqty'].'</td>
										<td style="text-align:center;">'.$recx['llday'].'</td>
										<td style="text-align:right;">'.number_format($recx['lprice']*$recx['lqty'],2).'</td>
									 </tr>';
					$servToto += $recx['lprice']*$recx['lqty'];
					$iii++;
				}
				$addServices = '<p style="background:#f6f6f6;border-top:solid 1px #ccc;">Additional Service</p>
								<table width="100%" cellpadding="0" cellspacing="0" class="myBookingTable">
									<tr class="head">
										<td width="32">No.</td>
										<td>Additional Services</td>
										<td width="100">Price</td>
										<td width="50">Qty</td>
										<td width="50">Day</td>
										<td width="120">Total Price</td>
									</tr>
									'.$addServices.'
									<tr>
										<td colspan="5" style="text-align:right;">Additional Service Total Price</td>
										<td style="text-align:right;">'.number_format($servToto,2).'</td>
									</tr>
								</table>';
				
				//$detConten3 = '<table width="100%"0 class="myBookingTable" >'.$detConten3.'</table>';
				//$add = '<p style="background:#f6f6f6;border-top:solid 1px #ccc;">Room Details</p>'.$detConten3;
				//ADDITIONALSERVICES -------------------------------------------------------------------------------------------------------------
				//-----------------------------------------------------------------------------------------------------------------------
				
				
				
				
				$details = '<div class="details">
								<p style="background:#f6f6f6; width:352px; float:left; border-right: solid 1px #ccc;">Booking Details</p>
								<p style="background:#f6f6f6; width:352px; float:right;">Payment Details</p>
								<div style="width:373px; float:left;">
									'.$detConten1.'
								</div>
								<div style="width:373px; float:right;">
									'.$paymentContent.'
								</div>
								
								<div style="width:746px; float:left;">
									'.$add.'
								</div>
								
								<div style="width:746px; float:left;">
									'.$addServices.'
								</div>

						   		<p style="background:#f6f6f6; text-align:right; border-top:solid 1px #ccc; height:25px;">
						   			<span class="buttonSetWrapper" style="float:right;">
										<a href="#print" class="thePrintThisPageButton" style="float:right; padding: 6px 10px 6px; background:#303030; color:#fff; font-weight:bold; text-decoration:none;">PRINT</a>
										<a href="#close" class="theCloseThisPageButton" style="display:none; float:right; padding: 6px 10px 6px; background:#303030; color:#fff; font-weight:bold; text-decoration:none; margin-right:10px;">CLOSE</a>
						   			</span>
								</p>
							</div>';
				
				$bookStatus[] = "hold";
				$bookStatus[] = "booked";
				$bookStatus[] = "confirm";
				$bookStatus[] = "cancel";				
				
				//==================================================================================================================================
				$strx = "SELECT SUM(lprice*lqty) as total FROM lumonata_accommodation_booking_additionalservices WHERE lbooking_id=%s"; 	
				$sqlx = parent::prepare_query($strx, $rec['lbooking_id']);
				$resx = parent::query($sqlx);
				$addServ = parent::fetch_array($resx);
				//==================================================================================================================================
				
				$all .= '<div class="wrapper bordered">
							<span class="zero" style="position:relative;">
								'.$rec['lbooking_id'].'
								<span class="bookingStatus '.$bookStatus[$rec['lstatus']].'">'.$bookStatus[$rec['lstatus']].'</span>
							</span>
							<span class="one">'.$det['lname'].'</span>
							
							<span class="three">'.date('j M Y',$rec['lcreated_date']).'</span>
							<span class="three" style="width:150px;">
								Check In : '.date('j M Y',$rec['lcheck_in']).' <br />
								Check Out : '.date('j M Y',$rec['lcheck_out']).'
							</span>
							
							<span class="four">
								A.S : '.$theCurName.' '.number_format($addServ['total'],2).'<br />
								S.C : '.$theCurName.' '.number_format($rec['lsurecharge'],2).'<br />
								D.C : '.$theCurName.' '.number_format($rec['ldiscount'],2).'<br />
								G.T : '.$theCurName.' '.number_format($rec['ltotal']+$addServ['total']+$rec['lsurecharge']-$rec['ldiscount'],2).' <br />
								T.C : '.$theCurName.' '.number_format($rec['lcommision'],2).' <br />
								T.A.C : '.$theCurName.' '.number_format($rec['ltotal_after']+$addServ['total']+$rec['lsurecharge']-$rec['ldiscount'],2).'
							</span>
							
							<span class="five narrow">
								<a class="showDetails">More Info <img class="down" src="{http}{template_url}/img/more-info-arrow-d.png" /><img class="up" style="display:none;" src="{http}{template_url}/img/more-info-arrow-u.png" /></a>
							</span>
							
							'.$details.'
							
						</div>';
			}
			return $all;
		}
		
		
		function paymentTableInit(){
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			
			$str = "SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d AND lcheck_in<=%d";
			
			$que = parent::prepare_query($str." ORDER BY lcheck_in ASC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataFirst = parent::fetch_array($res);
			$que = parent::prepare_query($str." ORDER BY lcheck_in DESC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataLast = parent::fetch_array($res);
			
			$curPeriod  = date('Y', time()).date('m', time());
			$monthStart = date('n', $dataFirst['lcheck_in']);
			$monthEnd   = date('n', $dataLast['lcheck_in']);
			$yearStart  = date('Y', $dataFirst['lcheck_in']);
			$yearEnd    = date('Y', $dataLast['lcheck_in']);
			
			if( !empty($dataFirst) && !empty($dataLast) ){
			for($y=$yearStart; $y<=$yearEnd; $y++){
				for($m=$monthStart; $m<=$monthEnd; $m++){
					$key1 = mktime(0,0,0,$m,1,$y);
					$key2 = mktime(0,0,0,$m+1,1,$y);
					if($month==12){ $key2 = mktime(0,0,0,1,1,$y+1); }
					
					$period  = date('Y', $key1).date('m', $key1);
					
					$str = "SELECT * FROM lumonata_payment WHERE lagent_ID=%s AND lperiod=%d";
					$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period);
					$res = parent::query($que);					
					$nnn = parent::num_rows($res);
					if($nnn<=0 && $period!=$curPeriod){
						$str = "SELECT SUM(ldebit) as totDebit, SUM(lcredit) as totCredit FROM lumonata_payment WHERE lagent_ID=%s AND lperiod<%d";
						$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period);
						$res = parent::query($que);	
						$rec = parent::fetch_array($res);
						$lasttimeBalance = $rec['totDebit']-$rec['totCredit'];
										
						$str = "SELECT SUM(lcommission_usd) AS theTotal FROM lumonata_accommodation_booking WHERE lcheck_in>=%d AND lcheck_in<%d AND lproduct_type=%d AND lproduct_id=%d";
						$que = parent::prepare_query($str, $key1, $key2, $pType, $pID);
						$res = parent::query($que);	
						$rec = parent::fetch_array($res);
						$debAmmount   = $rec['theTotal'];
						$theBalance   = $lasttimeBalance+$debAmmount;
						$periodString = '1 ' . date('M', $key1).' - '.date('t', $key1).' '.date('M', $key1);
						
						$str = "INSERT INTO lumonata_payment(lagent_ID, lperiod, ldescription, ldebit, lcredit, lbalance, ldlu)VALUES(%s, %d, %s, %s, %s, %s, %d)";
						$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period, 'Bill ('.$periodString.')', $debAmmount, 0, $theBalance,time());
						$resInsert = parent::query($que);	
					}else if($period==$curPeriod){
						//echo 'yahuii...';
						$str = "SELECT SUM(ldebit) as totDebit, SUM(lcredit) as totCredit FROM lumonata_payment WHERE lagent_ID=%s AND lperiod<%d";
						$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period);
						$res = parent::query($que);	
						$rec = parent::fetch_array($res);
						$lasttimeBalance = $rec['totDebit']-$rec['totCredit'];
						
						$str = "SELECT SUM(lcommission_usd) AS theTotal FROM lumonata_accommodation_booking WHERE lcheck_in>=%d AND lcheck_in<%d AND lproduct_type=%d AND lproduct_id=%d";
						$que = parent::prepare_query($str, $key1, $key2, $pType, $pID);
						$res = parent::query($que);	
						$rec = parent::fetch_array($res);
						$debAmmount   = $rec['theTotal'];
						$theBalance   = $lasttimeBalance+$debAmmount;
						$periodString = '1 ' . date('M', $key1).' - '.date('j', time()).' '.date('M', time());
						
						if($nnn<=0){
							$str = "INSERT INTO lumonata_payment(lagent_ID, lperiod, ldescription, ldebit, lcredit, lbalance, ldlu)VALUES(%s, %d, %s, %s, %s, %s, %d)";
							$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period, 'Bill ('.$periodString.')', $debAmmount, 0, $theBalance,time());
						}else{
							$str = "UPDATE lumonata_payment SET ldebit=%s, lbalance=%s, ldescription=%s, ldlu=%d WHERE lagent_ID=%s AND lperiod=%d AND linked_paymentID=0";
							$que = parent::prepare_query($str, $debAmmount, $theBalance, 'Bill ('.$periodString.')', time(), $_SESSION['othUsrID'], $period);
						}
						$resInsert = parent::query($que);
						
						$str = "SELECT * FROM lumonata_payment WHERE lagent_ID=%s AND lperiod=%d AND linked_paymentID!=0";
						$que = parent::prepare_query($str, $_SESSION['othUsrID'], $period);
						$re2 = parent::query($que);
						$nn2 = parent::num_rows($res);
						if($nn2>=1){						
							$data = parent::fetch_array($re2);
							$theBalance -= $data['lcredit'];
							
							$str = "UPDATE lumonata_payment SET lbalance=%s, ldlu=%d WHERE lagent_ID=%s AND lperiod=%d AND linked_paymentID!=0";
							$que = parent::prepare_query($str, $theBalance, time(), $_SESSION['othUsrID'], $period);
							$resUpdate = parent::query($que);
						}
						
					}else{
						//echo 'nothing...';
					}
				}
			}}
		}
		
		function paymentTableRead(){
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			if( !empty($_POST['showBill']) && !empty($_POST['billPeriod']) ){
				$str = "SELECT * FROM lumonata_payment WHERE lagent_ID=%s AND lperiod=%d ORDER BY lperiod ASC";
				$que = parent::prepare_query($str, $_SESSION['othUsrID'], date('Y',$_POST['billPeriod']).date('m',$_POST['billPeriod']) );
			}else{
				$str = "SELECT * FROM lumonata_payment WHERE lagent_ID=%s ORDER BY lperiod ASC";
				$que = parent::prepare_query($str, $_SESSION['othUsrID']);
			}
			$res = parent::query($que);		
			$jjj = 0;
			while( $data = parent::fetch_array($res) ){
				$year = substr($data['lperiod'],0,4);
				$month = str_replace($year,'',$data['lperiod']);
				
				$link = '';
				$payButton = '';
				if( $data['linked_paymentID']==0 ){
					$link = ' <a href="#billing" class="showBillingDetails" rel="billingDetail-'.$jjj.'">(show details)</a> ';
					
					$str = "SELECT * FROM lumonata_payment WHERE linked_paymentID=%d";
					$que = parent::prepare_query($str, $data['lpayment_ID']);
					$re3 = parent::query($que);	
					$nnn = parent::num_rows($re3);
					if($nnn<=0 && $curPeriod!=$data['lperiod']){
						$payButton = '<input rel="'.$data['lpayment_ID'].'" type="button" class="thePayBillButton" name="payButton" value="pay" style="position:absolute; right:-3px; top:-1px; cursor:pointer; display:block; float:right; background: #BE3125; border: none; color: #FFFFFF; font-weight: bold; padding: 0px 6px 2px; font-size:9px; -webkit-border-radius:4px; -moz-border-radius: 4px; border-radius: 4px;" />';
					}
				}
				
				
				$deb = '';
				$cre = '';
				if($data['ldebit']!=0)	{ $deb = 'USD '.number_format($data['ldebit'],2); }
				if($data['lcredit']!=0)	{ $cre = 'USD '.number_format($data['lcredit'],2); }
				
				
				$all .= '<tr class="content">
							<td>'.date('M Y', mktime(0,0,0,$month,1,$year) ).'</td>
							<td>
								<p style="position:relative; display:block;">
									'.$data['ldescription'].$link.$payButton.'
								</p>
							</td>
							<td style="text-align:right;">'.$deb.'</td>
							<td style="text-align:right;">'.$cre.'</td>
							<td style="text-align:right;">USD '.number_format($data['lbalance'],2).'</td>
						</tr>';
				
				
				if( !empty($data['lcredit']) ||$data['lcredit']>0 ){
				$key1 = mktime(0,0,0,$month,1,$year);
				$key2 = mktime(0,0,0,$month+1,1,$year);
				if($month==12){ $key2 = mktime(0,0,0,1,1,$year+1); }
				
				if($data['linked_paymentID']==0){
					$str = "SELECT * FROM lumonata_accommodation_booking WHERE lcheck_in>=%d AND lcheck_in<%d AND lproduct_type=%d AND lproduct_id=%d";
					$que = parent::prepare_query($str, $key1, $key2, $pType, $pID);
					$re2 = parent::query($que);
					$inner = '';
					while( $rec = parent::fetch_array($re2) ){
						$inner .= '<tr>
									<td>'.$rec['lbooking_id'].'</td>
									<td>'.$rec['lsalutation'].' '.$rec['lfname'].' '.$rec['llname'].'</td>
									<td style="width:140px; text-align:center;">'.date('j M Y', $rec['lcheck_in']).'</td>
									<td style="width:140px; text-align:right;">USD '.number_format($rec['lcommission_usd'],2).'</td>
								   </tr>';
					}
					$all .=  '<tr style="display:none;" id="billingDetail-'.$jjj.'"><td colspan="5"><table width="100%" class="inner">
									<tr class="head">
										<td>Booking ID</td>
										<td>Full Name</td>
										<td>Check In Date</td>
										<td>Commission</td>
									</tr>
									'.$inner.'
							   </table></td></tr>';
					}
				}
				$jjj++;
				
			}
			return $all;
		}
		
		function doThePayment(){
			//print_r($_POST);
			$str = "SELECT * FROM lumonata_payment WHERE linked_paymentID=%d";
			$que = parent::prepare_query($str, $_POST['values']);
			$res = parent::query($que);
			$nn2 = parent::num_rows($res);
			
			$str = "SELECT * FROM lumonata_payment WHERE lpayment_ID=%d";
			$que = parent::prepare_query($str, $_POST['values']);
			$res = parent::query($que);
			$nnn = parent::num_rows($res);
			$data = parent::fetch_array($res);
						
			if($nnn==1 && $nn2==0){
				$curPeriod  = date('Y', time()).date('m', time());
				$periodString = trim(str_replace('Bill','',$data['ldescription']));
				
				$str = "SELECT SUM(ldebit) as totDebit, SUM(lcredit) as totCredit FROM lumonata_payment WHERE lagent_ID=%s";
				$que = parent::prepare_query($str, $_SESSION['othUsrID']);
				$res = parent::query($que);	
				$rec = parent::fetch_array($res);
				$lasttimeBalance = $rec['totDebit']-$rec['totCredit'];
				$theBalance = $lasttimeBalance-$data['ldebit'];
				
				$str = "INSERT INTO lumonata_payment(lagent_ID, lperiod, ldescription, ldebit, lcredit, lbalance, ldlu, linked_paymentID, status)VALUES(%s, %d, %s, %s, %s, %s, %d, %d, %d)";
				$que = parent::prepare_query($str, $_SESSION['othUsrID'], $curPeriod, 'Payment '.$periodString, 0, $data['ldebit'], $theBalance,time(), $_POST['values'], 0);
				$resInsert = parent::query($que);	
			}else{
				//echo 'eee';
			}
		}
		
		
		
		function theEarning(){
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			$str = "SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d AND lcheck_in<=%d";
			
			$que = parent::prepare_query($str." ORDER BY lcheck_in ASC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataFirst = parent::fetch_array($res);
			$que = parent::prepare_query($str." ORDER BY lcheck_in DESC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataLast = parent::fetch_array($res);
			
			$curPeriod  = date('Y', time()).date('m', time());
			$monthStart = date('n', $dataFirst['lcheck_in']);
			$monthEnd   = date('n', $dataLast['lcheck_in']);
			$yearStart  = date('Y', $dataFirst['lcheck_in']);
			$yearEnd    = date('Y', $dataLast['lcheck_in']);
			
			if( !empty($_POST['showEarning']) && !empty($_POST['earnPeriod']) ){
				$monthStart = date('n', $_POST['earnPeriod']);
				$monthEnd   = date('n', $_POST['earnPeriod']);
				$yearStart  = date('Y', $_POST['earnPeriod']);
				$yearEnd    = date('Y', $_POST['earnPeriod']);
			}
			
			$jjj = 0;
			if( !empty($dataFirst) && !empty($dataLast) ){
			for($y=$yearStart; $y<=$yearEnd; $y++){
				for($m=$monthStart; $m<=$monthEnd; $m++){
					$key1 = mktime(0,0,0,$m,1,$y);
					$key2 = mktime(0,0,0,$m+1,1,$y);
					$period  = date('Y', $key1).date('m', $key1);
					$thePeriod = date('M Y', $key1);
										
					if($curPeriod==$period){
						$periodString = '1 ' . date('M', $key1).' - '.date('j', time()).' '.date('M', time());
					}else{
						$periodString = '1 ' . date('M', $key1).' - '.date('t', $key1).' '.date('M', $key1);
					}
					
					
					$str = "SELECT * FROM lumonata_accommodation_booking WHERE lcheck_in>=%d AND lcheck_in<%d AND lproduct_type=%d AND lproduct_id=%d";
					$que = parent::prepare_query($str, $key1, $key2, $pType, $pID);
					$re2 = parent::query($que);
					$nnn = parent::num_rows($re2);
					
					$str = "SELECT SUM(ltotal_after_usd) AS theTotal FROM lumonata_accommodation_booking WHERE lcheck_in>=%d AND lcheck_in<%d AND lproduct_type=%d AND lproduct_id=%d";
					$que = parent::prepare_query($str, $key1, $key2, $pType, $pID);
					$res = parent::query($que);	
					$data = parent::fetch_array($res);
								
					$link = '';
					if( $nnn>0 ){
						$link = ' <a href="#earning" class="showBillingDetails" rel="earningDetail-'.$jjj.'">(show details)</a> ';
					}				
									
					$all .= '<tr class="content">
								<td>'.date('M Y', mktime(0,0,0,$m,1,$y) ).'</td>
								<td>Earning ('.$periodString.') '.$link.'</td>
								<td style="text-align:right;">USD '.number_format($data['theTotal'],2).'</td>
							</tr>';
					$theGrandToto += $data['theTotal'];
					
					
					
					$inner = '';
					if($nnn>0){
					while( $rec = parent::fetch_array($re2) ){
						$inner .= '<tr>
									<td>'.$rec['lbooking_id'].'</td>
									<td>'.$rec['lsalutation'].' '.$rec['lfname'].' '.$rec['llname'].'</td>
									<td style="width:140px; text-align:center;">'.date('j M Y', $rec['lcheck_in']).'</td>
									<td style="width:140px; text-align:right;">USD '.number_format($rec['ltotal_after_usd'],2).'</td>
								   </tr>';
					}
					$all .=  '<tr style="display:none;" id="earningDetail-'.$jjj.'"><td colspan="3"><table width="100%" class="inner">
									<tr class="head">
										<td>Booking ID</td>
										<td>Full Name</td>
										<td>Check In Date</td>
										<td>Total After Comm.</td>
									</tr>
									'.$inner.'
							   </table></td></tr>';
					}
					
					$jjj++;
				}
			}}
			$all .= '<tr class="head">
						<td style="text-align:right;" colspan="2">TOTAL</td>
						<td style="text-align:right;">USD '.number_format($theGrandToto,2).'</td>
					</tr>';
							
			return $all;
		}
		
		function getThePeriod(){
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			
			$str = "SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d AND lcheck_in<=%d";
			$nn1 = parent::prepare_query($str, $pType, $pID, time());
			$nn2 = parent::query($nn1);
			$nnn = parent::num_rows($nn2);
			
			
			$que = parent::prepare_query($str." ORDER BY lcheck_in ASC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataFirst = parent::fetch_array($res);
			$que = parent::prepare_query($str." ORDER BY lcheck_in DESC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataLast = parent::fetch_array($res);
			
			$curPeriod  = date('Y', time()).date('m', time());
			$monthStart = date('n', $dataFirst['lcheck_in']);
			$monthEnd   = date('n', $dataLast['lcheck_in']);
			$yearStart  = date('Y', $dataFirst['lcheck_in']);
			$yearEnd    = date('Y', $dataLast['lcheck_in']);
			
			if($nnn>0){
				for($y=$yearStart; $y<=$yearEnd; $y++){
					for($m=$monthStart; $m<=$monthEnd; $m++){
						$key1 = mktime(0,0,0,$m,1,$y);
						if( $_POST['billPeriod']==$key1 ){
							$all .= '<option selected="selected" value="'.$key1.'">'.date('F Y',$key1).'</option>';
						}else{
							$all .= '<option value="'.$key1.'">'.date('F Y',$key1).'</option>';
						}
					}
				}
			}
			return $all;
		}
		function getTheEarnPeriod($theSel){
			$tmp = "SELECT * FROM lumonata_members WHERE lmember_id=%s";
			$sql=parent::prepare_query($tmp,$_SESSION['othUsrID']);
			$re3=parent::query($sql);
			$result = parent::fetch_array($re3);
			$curID = $result['lcurrency_id'];
			$pType = $result['lproduct_type'];
			$pID   = $result['lproduct_id'];
			
			
			$str = "SELECT * FROM lumonata_accommodation_booking WHERE lproduct_type=%d AND lproduct_id=%d AND lcheck_in<=%d";
			$nn1 = parent::prepare_query($str, $pType, $pID, time());
			$nn2 = parent::query($nn1);
			$nnn = parent::num_rows($nn2);
						
			
			$que = parent::prepare_query($str." ORDER BY lcheck_in ASC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataFirst = parent::fetch_array($res);
			$que = parent::prepare_query($str." ORDER BY lcheck_in DESC LIMIT 0,1" , $pType, $pID, time());
			$res = parent::query($que);
			$dataLast = parent::fetch_array($res);
			
			$curPeriod  = date('Y', time()).date('m', time());
			$monthStart = date('n', $dataFirst['lcheck_in']);
			$monthEnd   = date('n', $dataLast['lcheck_in']);
			$yearStart  = date('Y', $dataFirst['lcheck_in']);
			$yearEnd    = date('Y', $dataLast['lcheck_in']);
			
			if($nnn>0){
				for($y=$yearStart; $y<=$yearEnd; $y++){
					for($m=$monthStart; $m<=$monthEnd; $m++){
						$key1 = mktime(0,0,0,$m,1,$y);
						if( $theSel==$key1 ){
							$all .= '<option selected="selected" value="'.$key1.'">'.date('F Y',$key1).'</option>';
						}else{
							$all .= '<option value="'.$key1.'">'.date('F Y',$key1).'</option>';
						}
					}
				}
			}
			return $all;
		}
		
		
		
		
		
		
		function getCountryName($id){
			$sql=parent::prepare_query("SELECT * FROM lumonata_country WHERE lcountry_id=%d", $id);
			$res=parent::query($sql);
			$rec=parent::fetch_array($res);
			return $rec['lcountry'];
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>