<?php
	class season_edit extends db{
		function season_edit($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Season Edit");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			//echo $_SESSION['product_id'];
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			if(!empty($_POST)){
				$stmt = $db->prepare_query("UPDATE lumonata_accommodation_season SET
					lname=%s,
					ldate_start=%d,
					ldate_finish=%d,
					lmin_stay=%d,
					lusername=%s,
					ldlu=%d
					WHERE lseason_id =%d",
					$_POST['name'],
					strtotime($_POST['date_start']),
					strtotime($_POST['date_finish']),
					$_POST['min_stay'],
					$_SESSION['upd_by'],
					time(),
					$_POST['season_id']);
				$result = $db->do_query($stmt);
				if($result){
					
					$t->set_var('notif_message', "Update \"".$_POST['name']."\" was successfully processed.");
					$t->set_var('jsAction', 'notifBlockSaved();');		
					if(!isset($_POST['cut_of_date'])) $_POST['cut_of_date'] = 0;				
					$this->syncrone_TableAvailability($_POST['name'],strtotime($_POST['date_start']),strtotime($_POST['date_finish']),$_POST['cut_of_date']);
					
					//$this->syncTableAvailability();
					
					/*if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season : edit",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					} */					
				}else{
					$t->set_var('notif_message', "Update \"".$_POST['name']."\" was unsuccessfully processed.");
					$t->set_var('jsAction', 'notifFailedSaved();');	
					//$t->set_var('error', "<div class=\"error\"><b>Failed!</b><br />Add new \"".$_POST['name'][0]."\" was unsuccessfully processed.</div>");	
				}
					
				
			}
			$i = 0;
			$query =  $db->prepare_query("SELECT *
				FROM lumonata_accommodation_season
				WHERE lseason_id =%d",$_GET['act2']);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			if (isset($_POST['pilih'][$i])){
				$pilih = $_POST['pilih'][$i];
			}else{
				$pilih = '';
			}
			$t->set_var('pilih',$pilih);
			$t->set_var('i', $i);	
			$t->set_var('season_id', $data['lseason_id']);
			$t->set_var('min_stay', $data['lmin_stay']);	
			$t->set_var('date_start', date("m/d/Y",$data['ldate_start'])); 	 	
			$t->set_var('date_finish', date("m/d/Y",$data['ldate_finish'])); 
			$align_arr = array("Low Season","High Season","Peak Season"); //
			$nameoption = "<select name=\"name\" id=\"name\" class=\"required\" >
			<option value=\"\">Select Season Name</option>";
			for($j=0;$j<count($align_arr);$j++){
				if ($data['lname'] == $align_arr[$j]){
					$nameoption .= "<option value=\"$align_arr[$j]\" selected>$align_arr[$j]</option>";
				}else{
					$nameoption .= "<option value=\"$align_arr[$j]\">$align_arr[$j]</option>";
				}
			}
			$nameoption .= "</select>";	 	
			$t->set_var('season_name', $nameoption);	
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		function syncrone_TableAvailability($season_name,$date_start,$date_finish,$cut_of_date){
			require_once("../booking-engine/apps/season/class.season.php");
			$this->season = new season('');
			$this->season->sync_TableAvailability($season_name,$date_start,$date_finish,$cut_of_date);
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>