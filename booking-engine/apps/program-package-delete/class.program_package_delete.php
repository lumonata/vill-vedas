<?php 
class program_package_delete extends db{
	function program_package_delete($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Program Package Delete");
		
		
	}
	
	function load(){
		global $db;
		require_once("../booking-engine/apps/program-package/class.program_package.php");
		$this->program_package = new program_package();
		
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		//print_r($_POST);
		
		$str = $db->prepare_query("select * from lumonata_program_package where lpp_id=%d",$_GET['act2']);
		$result = $db->do_query($str);
		$data = $db->fetch_array($result);
		
		$id_program = $data['lprogram_id'];
		$data_program = $this->data_tabel('lumonata_articles',"where larticle_id=$id_program",'array');
		$t->set_var('program', $data_program['larticle_title']);
		
		$room_type = $this->program_package->get_room_type($data['lacco_type_id'],'get_name');
		//echo 'dsdsd';
		if(!empty($_POST['pp_id'])) {
			$this->program_package->do_delete($t);
			$t->set_var('msg_succsess', 'Program package was success deleted');
			$t->set_var('jsAction', 'notifBlockSaved();');	
			header("Refresh:3; url= http://".SUPPLIERPANEL_SITE_URL."/program-package/", true, 303);
			
		}	
		
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('i', $i);	
		$t->set_var('room_type', $room_type);	
		$t->set_var('pp_id', $data['lpp_id']);
		//$t->set_var('name', date('d-m-Y',$data['ldate_start']).' - '.date('d-m-Y',$data['ldate_end']));
		return $t->Parse('mBlock', 'mainBlock', false);
	}
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");	
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
}
?>