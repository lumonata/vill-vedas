<?php
	class surcharge extends db{
		function surcharge($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Surcharge");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin = new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function get_data($t,$w,$r='result_only'){
			require_once("../booking-engine/apps/general-func/class.general_func.php");
			$this->general_func = new general_func();
			return $this->general_func->data_tabels($t,$w,$r);
		}
		
		function is_valid_value($date_start='',$date_finish='',$all_time='',$dow='',$desc='',$ammount='',$ammount_unit='',$t){
			$error = 0;
			$msg ="";
			/*if($room_type ==''){
				$msg .= "<li>Please select room category</li>";
				$error++;		
			}*/
			
			if($all_time==''){
				if($date_start==''){
					$msg .= "<li>Please select date start</li>";
					$error++;		
				}	
				if($date_finish==''){
					$msg .= "<li>Please select date end</li>";
					$error++;		
				}
			}
			
			if($dow==''){
				$msg .= "<li>Please select day of week</li>";
				$error++;	
			}
			
			if($desc==''){
				$msg .= "<li>Please set surcharge description</li>";
				$error++;	
			}
			
			if($ammount=''){
				$msg .= "<li>Please set amount surchage</li>";
				$error++;	
			}
			
			if($ammount_unit==''){
				$msg .= "<li>Please choose amount charge type</li>";
				$error++;	
			}
			
			if($error > 0){
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifFailedValidation();');	
				return false;
			}else return true;
			
			
		}
		
		function is_valid_value_on_update($t){
			$error = 0;
			$msg ="";
			
			for($i=0;$i<count($_POST['surecharge_ID']);$i++){
				$surecharge_ID = $_POST['surecharge_ID'][$i];
				
				$date_start 	= $_POST['date_start'][$i]; 
				$date_finish 	= $_POST['date_finish'][$i]; 
				$all_time 		= (isset($_POST['all_time'][$i])?$_POST['all_time'][$i]:''); 
				$dow 			= $_POST['dow'][$i]; 
				$desc 			= $_POST['desc'][$i]; 
				$ammount 		= $_POST['ammount'][$i]; 
				$ammount_unit 	= $_POST['lammount_unit'][$i]; 
				if($this->is_valid_value($date_start,$date_finish,$all_time,$dow,$desc,$ammount,$ammount_unit,$t)==false)$error++;
			}			
			
			if($error > 0){
				$t->set_var('notif_message', "<ul><li>All value is required. Please choose one of the values​in each field</li></ul>");
				$t->set_var('jsAction', 'notifFailedValidation();');	
				return false;
			}else return true;
		}
		
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			if ((isset($_POST['prc']) And $_POST['prc'] == "new") || (isset($_POST['prc']) And $_POST['prc'] == "save_new")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; border-bottom:solid 1px #ccc;background:#f5f5f5;\"></div>");
				if($_POST['prc'] == "save_new"){	
					$surecharge_ID	= $this->globalAdmin->setCode("lumonata_accommodation_surecharge","lsurecharge_ID");
					$date_start 	= strtotime($_POST['date_start'][0]); 
					$date_finish 	= strtotime($_POST['date_finish'][0]); 
					$all_time 		= $_POST['all_time'][0]; 
					$dow 			= $_POST['dow'][0]; 
					$desc 			= $_POST['desc'][0]; 
					$ammount 		= $_POST['ammount'][0]; 
					$ammount_unit 	= $_POST['lammount_unit'][0]; 
					
					//validate value
					if($this->is_valid_value($date_start,$date_finish,$all_time,$dow,$desc,$ammount,$ammount_unit,$t)){
						if($all_time=='all_time') {
							$date_start = 0; $date_finish=0;	
						}
						
						$sql = $db->prepare_query("insert into lumonata_accommodation_surecharge 
												   (lsurecharge_ID, ldate_from, ldate_to, lday_of_week,
												    ldescription, lcharge, lammount_unit, ldlu) values 
												   (%d, %d, %d, %s,
												    %s, %s, %s, %d)",$surecharge_ID,$date_start,$date_finish,$dow,
													$desc,$ammount,$ammount_unit,time());
						
						$result = $db->do_query($sql);
						if($result){
							$t->set_var('notif_message', 'Data Saved');	
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}else {
							$t->set_var('notif_message', 'Failed to saved');
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}					
					}
				}
				
				$t->set_var('i', 0);
				$array = array('USD','%');
				$all = '';
				foreach($array as $value){
					$all .= '<option value="'.$value.'">'.$value.'</option>';
				}
				$t->set_var('lammount_unit',  $all); 
				
				//$t->set_var('room_category',$this->get_room_type());
				//$t->set_var('surechargeDesc', $all);
				
				
				$t->set_var('date_start',  "mm/dd/yyyy"); 	 	
				$t->set_var('date_finish', "mm/dd/yyyy"); 
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if ((isset($_POST['prc']) And $_POST['prc'] == "edit") || (isset($_POST['prc']) And $_POST['prc'] == "save_edit")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					if($this->is_valid_value_on_update($t)){
						$msg = '';
						$error = '';
						for($i=0;$i<count($_POST['surecharge_ID']);$i++){
							$surecharge_ID = $_POST['surecharge_ID'][$i];
							/*$room_type		= $_POST['roomtype'][$i];*/
							$all_time 		= $_POST['all_time'][$i]; 
							if(isset($_POST['all_time'][$i]) && $all_time == 'all_time'){
								$date_start 	= 0; 
								$date_finish 	= 0; 	
							}else{
								$date_start 	= strtotime($_POST['date_start'][$i]); 
								$date_finish 	= strtotime($_POST['date_finish'][$i]); 	
							}
							
							$dow 			= $_POST['dow'][$i]; 
							$desc 			= $_POST['desc'][$i]; 
							$ammount 		= $_POST['ammount'][$i]; 
							$ammount_unit 	= $_POST['lammount_unit'][$i]; 
							
							$str = $db->prepare_query("update lumonata_accommodation_surecharge set
													   ldate_from=%d,ldate_to=%d,lday_of_week=%s,
													   ldescription=%s,lcharge=%s,lammount_unit=%s,ldlu=%d
													   where lsurecharge_ID=%d
													   ",$date_start,$date_finish,$dow,
													   $desc,$ammount,$ammount_unit,time(),
													   $surecharge_ID);
			   
							$result = $db->do_query($str);
							if(!$result)$error++;	
						}//end for
						
						if($error==1){
							$t->set_var('notif_message', "<ul><li>Update was failed processed.</li></ul>");
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}else{
							$t->set_var('notif_message', "<ul><li>Update was successfully processed.</li></ul>");
							$t->set_var('jsAction', 'notifBlockSaved();');
						}
					}
					
				}
				
				//end edit process
				
				//start display data
				if (isset($_POST['pilih']) And sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; border-bottom:solid 1px #ccc;background:#f5f5f5;\"></div>");
						$surecharge_ID = $_POST['pilih'][$i];
						$t->set_var('surecharge_ID', $surecharge_ID); 
						$data_surcharge = $this->get_data('lumonata_accommodation_surecharge',"where lsurecharge_ID=$surecharge_ID",'array');
						
						$t->set_var('pilih',$surecharge_ID);
						$t->set_var('i', $i);	
						//$t->set_var('room_category',$this->get_room_type($data_surcharge['lacco_type_id']));
												
						$t->set_var('surechargeDesc', $data_surcharge['ldescription']);
						$array = array('USD','%');
						$all = '';
						foreach($array as $value){
							if($value==$data_surcharge['lammount_unit']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all .= '<option '.$sel.' value="'.$value.'">'.$value.'</option>';
						}
						$t->set_var('lammount_unit',  $all); 
						
						if($data_surcharge['ldate_from']==0 && $data_surcharge['ldate_to']==0){
							$t->set_var('all_time', 'checked'); 
							$t->set_var('date_start',''); 	 	
							$t->set_var('date_finish','');	 		
						}else{
							$t->set_var('date_start', 	date("m/d/Y",$data_surcharge['ldate_from'])); 	 	
							$t->set_var('date_finish', 	date("m/d/Y",$data_surcharge['ldate_to']));
							$t->set_var('all_time', ''); 
						}
						
						$t->set_var('dow',     $data_surcharge['lday_of_week']); 	 	
						$t->set_var('ammount', $data_surcharge['lcharge']);						
						$t->Parse('eC', 'editContent', true); 
					}//end for
					
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if ((isset($_POST['prc']) And $_POST['prc']=="delete")){
					if (sizeof($_POST['pilih']) != 0){
						$alltitle = '';
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = $db->prepare_query("SELECT * FROM lumonata_accommodation_surecharge
														WHERE lsurecharge_ID =%d",$_POST['pilih'][$i]);
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;
							
							$name = $data['ldescription'];
							if($data['ldate_from']==0 && $data['ldate_to']==0) $dates = "All Time";
							else $dates = date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to']);
							$alltitle = $alltitle."<li>".$name." : ".$dates."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, season below using in accommodation season data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if ((isset($_POST['action']) And $_POST['action']=="Yes")){
					$msg = '';
					$error = '';
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						$query = $db->prepare_query("SELECT *
														FROM lumonata_accommodation_surecharge
														WHERE lsurecharge_ID=%d",$_POST['delId'][$i]);
						$result = $db->do_query($query);
						$data   = $db->fetch_array($result);
						
						$stmt   = $db->prepare_query("DELETE FROM lumonata_accommodation_surecharge WHERE lsurecharge_ID=%d",$data['lsurecharge_ID']);
						$result = $db->do_query($stmt);
					
						$name = $data['ldescription'];
						if($data['ldate_from']==0 && $data['ldate_to']==0) $dates = "All Time";
						else $dates = date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to']);
						
						if($result){
							$msg .= "<li>Delete \"".$name." : ".$dates."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"".$name." : ".$dates."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
					}else{
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 
					}
				}// END Delete Process 
																	   
				$query =  $db->prepare_query("select * from lumonata_accommodation_surecharge");									   
				//BEGIN set var on viewContent Block
				$result = $db->do_query($query);
				$no = 0;
				while($data = $db->fetch_array($result)){
					$t->set_var('no', $no);	 	
					/*$name = $this->get_room_type($data['lacco_type_id'],'get_name');			
					$t->set_var('name', $name);*/
					$t->set_var('check', $data['lsurecharge_ID']);
					if($data['ldate_from']==0 && $data['ldate_to']==0){
						$t->set_var('date_start', 'All Time'); 	 	
						$t->set_var('date_finish', 'All Time'); 	
					}else{
						$t->set_var('date_start', date("d M Y",$data['ldate_from'])); 	 	
						$t->set_var('date_finish', date("d M Y",$data['ldate_to'])); 	
					}
										
					$days = json_decode($data['lday_of_week']);
					$iii=0;
					foreach($days as $day){
						if($iii==0){
							$all = $day;
						}else{
							$all .= ', '.$day;
						}
						$iii++;
					}
					
					$t->set_var('day_of_week', $all);
					$t->set_var('description', $data['ldescription']); 	 
					if ($data['lammount_unit'] == "USD" && $data['lcharge'] !=0 ){	
						$t->set_var('ammount',  $data['lammount_unit']." ".$data['lcharge']); 
					}else{
						$t->set_var('ammount',  $data['lcharge']." ".$data['lammount_unit']); 
					}
					
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		function syncTableAvailabilityWhenDeletAccur(){
			global $db;
			$sql  = $db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res  = $db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user = $db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			$tmp = "SELECT * FROM lumonata_availability WHERE lacco_id=%d  ORDER BY ldate ASC";
			$sql = $db->prepare_query($tmp, $_SESSION['product_id']);
			$re2 = $db->do_query($sql);
			
			$k = 1;
			$l = 1;
			
			while($avail = $db->fetch_array($re2)){
				$theTime = $avail['ldate'];
				
				$tmp = "SELECT * FROM lumonata_accommodation_season WHERE ldate_start<=%d AND ldate_finish>=%d AND lacco_id=%d";
				$qry = $db->prepare_query($tmp, $theTime, $theTime, $_SESSION['product_id']);
				$res = $db->do_query($qry);
				$num = $db->num_rows($res);
				
				if($num<=0){
					$str = "DELETE FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$sql = $db->prepare_query($str, $avail['ldate'], $avail['lacco_id'], $avail['lacco_type_id']);
					$re3 = $db->do_query($sql);
				}
			}
		}
		
		function syncTableAvailability(){
			global $db;
			$sql  = $db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res  = $db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user = $db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			//DELETING PART================================================================================================================
			$sql = $db->prepare_query("SELECT * FROM lumonata_accommodation_season WHERE lacco_id=%d", $_SESSION['product_id']);
			$res = $db->do_query($sql);
			while($season = $db->fetch_array($res)){
				$tmp = "SELECT * FROM lumonata_availability WHERE lacco_id=%d AND lseason=%s";
				$sql = $db->prepare_query($tmp, $_SESSION['product_id'], $season['lname']);
				$re2 = $db->do_query($sql);
				while($avail = $db->fetch_array($re2)){
					if($season['ldate_start']<=$avail['ldate'] && $season['ldate_finish']>=$avail['ldate']){
						//do nothing for now
					}else{
						//echo date('j F Y', $avail['ldate']).'<br />';
						$str = "DELETE FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lseason=%s";
						$sql = $db->prepare_query($str, $avail['ldate'], $avail['lacco_id'], $avail['lseason']);
						$re3 = $db->do_query($sql);
					}
				}
			}
			//DELETING PART================================================================================================================
			//UPDATE PART================================================================================================================
			$pField['Low Season']  = 'llow_season';
			$pField['High Season'] = 'lhigh_season';
			$pField['Peak Season'] = 'lpeak_season';
				
			//$limitBegin = strtotime($_POST['date_start']);
			//$limitEnd   = strtotime($_POST['date_finish']);
			
			$the24 = 24*60*60;
			$j = 0;
			$sql = $db->prepare_query("SELECT * FROM lumonata_accommodation_season WHERE lacco_id=%d", $_SESSION['product_id']);
			$sesRes = $db->do_query($sql);
			while($season = $db->fetch_array($sesRes)){
				//echo $season['lname'].'===========================================================================================<br />';
				$tmp = "SELECT * FROM lumonata_accommodation_type WHERE lacco_id=%d";
				$sql = $db->prepare_query($tmp, $_SESSION['product_id']);
				$re2 = $db->do_query($sql);
				while($type = $db->fetch_array($re2)){
					//echo $type['lname'].'===========================================================================================<br />';
					
					$str = "SELECT * FROM lumonata_accommodation_rate WHERE lacco_type_id=%d";
					$sql = $db->prepare_query($str, $type['lacco_type_id']);
					$res = $db->do_query($sql);
					$rate = $db->fetch_array($res);
					$nnRateRec = $db->num_rows($res);
					//echo $nnRateRec;
							
					$rangeStart  = $season['ldate_start'];
					$rangeFinish = $season['ldate_finish'];
					$typeID      = $type['lacco_type_id'];
					$prodID      = $_SESSION['product_id'];
					
					if($nnRateRec>0){
					for($i=$rangeStart; $i<=$rangeFinish; $i+=$the24){
						//if( $limitBegin<=$i && $limitEnd>=$i ){
							$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
							$sql = $db->prepare_query($str, $i, $prodID, $typeID);
							$res = $db->do_query($sql);
							$nnn = $db->num_rows($res);
							$avail = $db->fetch_array($res);
							$status = explode(';',$avail['ledit']);
							
							$uRate = $rate[$pField[$season['lname']]];
							$uAlot = $rate['lallotment'];
							
							//echo $uRate.'<br />';
							
							if(isset($status[0]) and $status[0]=='1'){ $uAlot=$avail['lallotment']; }
							if(isset($status[1]) and $status[1]=='1'){ $uRate=$avail['lrate']; }
							
							if($nnn>0){
								//DO Nothing FOr NOw
							}else{
								//echo 'que<br />';
								$str = "INSERT INTO lumonata_availability(
										ldate, lacco_id, lacco_type_id,
										lallotment, lrate, lcommision, lstatus, ledit,
										lcreated_by, lcreated_date, lusername, ldlu,
										lcut_of_date, lseason
										) VALUES (
										%d, %d, %d, 
										%d, %s, %s, %d, %s,
										%s, %d, %s, %d,
										%d, %s )";
								$sql = $db->prepare_query($str,
															 $i, $prodID, $typeID,
															 $uAlot, $uRate, $rate['lcommision'], 0, '0;0',
															 'appsSeasonEdit', time(), $_SESSION['product_id'], time(),
															 $season['lcut_of_date'], $season['lname']);
								$res = $db->do_query($sql);
							}
						//}
					}}//end if + end for
				}
				$j++;
			}
			//UPDATE PART================================================================================================================
		}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		function get_room_type($id,$action='get_list'){
			require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
			$this->general_category_villa = new general_category_villa();
					
			if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
			else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
		}
		function get_category_type_data($action,$id=0){
			require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
			$this->list_category_type_villa = new list_category_type_villa();
			if($action=='get_list') return $this->list_category_type_villa->load($id);	
			else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>