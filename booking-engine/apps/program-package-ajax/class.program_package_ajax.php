<?php 
class program_package_ajax extends db{
	function program_package_ajax($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Program Package Ajax");
	}
	
	function load(){
		global $db;
		if(isset($_POST['do_act']) && $_POST['do_act']=='get-list-program'){
			return $this->get_list_program();
		}else if(isset($_POST['do_act']) && $_POST['do_act']=='get-list-villa'){
			return $this->get_list_villa();
		}
		
		
		
	}
	
	function get_list_program(){
		global $db;
		$id_rule = $_POST['id_category'];
		//valid id
		$num_rule = $this->data_tabel('lumonata_rules',"where lrule_id=$id_rule",'num_row');
		$return = "";
		if($num_rule > 0){
			$result_program = $this->data_tabel('lumonata_rules as a, lumonata_rule_relationship as b, lumonata_articles as c',
												"where a.lrule_id = b.lrule_id and b.lapp_id = c.larticle_id and a.lgroup='ratesreservations' 
												 and (c.larticle_type='programs' or c.larticle_type='ratesreservations') and b.lrule_id=$id_rule order by c.lorder");
												 
			//$str = $db->prepare_query()
			$list = "";		
			//echo 'ds'							 ;
			while($data_program  = $db->fetch_array($result_program)){
				$id = $data_program['larticle_id'];
				$title = $data_program['larticle_title'];
				$list .= "<option value=\"$id\">$title</option>";
			}
			$return = $list;									
		}
		echo $return;
	}
	
	
	
	function get_list_villa(){
		global $db;
		$rt_id = $_POST['room_type'];
		
		$num_article = $this->data_tabel('lumonata_relationship_villa_category_room_type',"where lacco_type_cat_id='$rt_id'",'num_row');
		$return = "";
		if($num_article > 0){
			$result  = $this->data_tabel('lumonata_relationship_villa_category_room_type as a, lumonata_articles as b',
										 "where a.larticle_id=b.larticle_id and a.lacco_type_cat_id='$rt_id'");	
			$list = "";								 
			while($data = $db->fetch_array($result)){
				$id = $data['larticle_id'];
				$title = $data['larticle_title'];
				$list .= "<option value=\"$id\">$title</option>";
			}
			$return = $list;
		}
		echo $return;
	}
	
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");	
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}	
		
}?>