<?php
	class type extends db{
		function type($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Room Type");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			/*print_r($_GET); //, [bool return])
			echo "<br /><br />";
			print_r($_POST); //, [bool return])
			echo "<br /><br />";
			*/
			//print_r($_SESSION);
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			//echo $_SESSION['product_id'];
			
			
			if ($_POST['prc'] == "new" || $_POST['prc'] == "save_new"){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				if($_POST['prc'] == "save_new"){
					//print_r($_POST);
					$stmt = parent::prepare_query("INSERT INTO lumonata_accommodation_type (
					lacco_id,
					lname,
					lbrief,
					lorder_id,
					lcreated_by,
					lcreated_date) VALUES (%d,%s,'".$this->globalAdmin->setPrc2Code($_POST['brief'][0])."',%d,%s,%d)",
					$_SESSION['product_id'],
					$_POST['name'][0],
					1,
					$_SESSION['upd_by'],
					time());
					$this->globalAdmin->setOrderID("lumonata_accommodation_type",1);		
					$result = parent::query($stmt);
					if($result){
						$t->set_var('notif_message', 'Data Saved');	
						$t->set_var('jsAction', 'notifBlockSaved();');
						
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"type : new",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 	
					}else{
						$t->set_var('notif_message', 'Failed to saved');
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}
						
					
				}
				$t->set_var('i', 0);
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit"){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					for($i=0;$i<count($_POST['acco_type_id']);$i++){
							$stmt = parent::prepare_query("UPDATE lumonata_accommodation_type SET
							lname=%s,
							lbrief='".$this->globalAdmin->setPrc2Code($_POST['brief'][$i])."',
							lusername=%s,
							ldlu=%d
							WHERE lacco_type_id =%d",
							$_POST['name'][$i],
							$_SESSION['upd_by'],
							time(),
							$_POST['acco_type_id'][$i]);
							$result = parent::query($stmt);
							if($result){
								$msg .= "<li>Update \"".$_POST['name'][$i]."\" was successfully processed.</li>";
							}else{
								$error = 1;
								$msg .= "<li>Update \"".$_POST['name'][$i]."\" was unsuccessfully processed.</li>";
							}
					}// end for
						if($error==1){
							$t->set_var('notif_message', $msg);
							$t->set_var('jsAction', 'notifFailedSaved();');	
							
							if($bySupplier){
								$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"type : edit",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
							} 
						}else{
							$t->set_var('notif_message', $msg);
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}
				}
				//end edit process
				
				//start display data
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$query =  parent::prepare_query("SELECT *
							FROM lumonata_accommodation_type
							WHERE lacco_type_id =%d",$_POST['pilih'][$i]);
						$result = parent::query($query);
						$data = parent::fetch_array($result);
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						$t->set_var('acco_type_id', $data['lacco_type_id']);
						$t->set_var('brief', $this->globalAdmin->setCode2Prc($data['lbrief']));	
						$t->set_var('name', $data['lname']);
						
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-bottom:solid 1px #ccc; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if ($_POST['prc']=="delete"){
					if (sizeof($_POST['pilih']) != 0){
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = parent::prepare_query("SELECT *
								FROM lumonata_accommodation_type
								WHERE lacco_type_id =%d",$_POST['pilih'][$i]);
					
							$result = parent::query($query);
							$data = parent::fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = $this->globalAdmin->getNumRows("lumonata_accommodation_rate","lacco_type_id",$data['lacco_type_id']);
							if ($n1 != 0)
							{
								$title = $title."<li>".$data['lname']."</li>";
							}
							$alltitle = $alltitle."<li>".$data['lname']."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, room type below using in accommodation rate data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if ($_POST['action']=="Yes"){
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						$msgTitle = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$_POST['delId'][$i]); 
						$stmt = parent::prepare_query("DELETE FROM lumonata_accommodation_type WHERE lacco_type_id =%d",$_POST['delId'][$i]);
						$result = parent::query($stmt);
						if($result){
							$msg .= "<li>Delete \"".$msgTitle."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"".$msgTitle."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
						
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"type : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 
					}else{
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
					}
				}// END Delete Process 
				$query =  parent::prepare_query("SELECT t.lacco_type_id,
					t.lacco_id,
					a.lname as laccommodation_name,
					a.lacco_type_id as ltype_id,
					a.lcat_id,
					c.lcategory as ldestination,
					t.lname,
					t.lbrief,
					t.lorder_id,
					t.lcreated_by,
					t.lcreated_date,
					t.lusername,
					t.ldlu
					FROM lumonata_accommodation_type t
					LEFT JOIN lumonata_accommodation a ON t.lacco_id = a.lacco_id 
					LEFT JOIN lumonata_accommodation_categories c ON a.lcat_id = c.lcat_id
					WHERE a.lacco_id = %d
					ORDER BY t.lorder_id",$_SESSION['product_id']);
				//BEGIN set var on viewContent Block
				$result = parent::query($query);
				while($data = parent::fetch_array($result)){
					$t->set_var('no', $no);	 	
					$t->set_var('check', $data['lacco_type_id']);	 		
					$t->set_var('name', $data['lname']);
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>