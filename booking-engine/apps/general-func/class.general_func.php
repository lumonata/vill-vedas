<?php 
class general_func extends db{
	var $appName;
	function general_func($appName=""){
		$this->appName=$appName;
		$this->setMetaTitle("General Func");
	}
	function load(){
		
	}
	function data_tabels($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function villa_room_type_name($villa_id){	
		global $db;
		$data_villa = $this->data_tabels('lumonata_articles',"where larticle_id=$villa_id",'array');
		$name_villa = $data_villa['larticle_title'];
		//get categories
		$rc = $this->get_data_additional_villa($villa_id,'categories');
		$dc = $db->fetch_array($rc);
		$category_villa = $dc['lname'];
		
		//get roomtype
		$rr = $this->get_data_additional_villa($villa_id,'room_type');
		$dr = $db->fetch_array($rr);
		if(!empty($dr)) {
			$roomtype_villa = $dr['lname'];
			$category_villa  = $category_villa.',';
		}else $roomtype_villa = "";		
		
		//return $name_villa.' - '.$roomtype_villa;
		return $name_villa;
	}
	
	
	function get_data_additional_villa($id,$type){
		global $db;
		$qc = $db->prepare_query("SELECT a.*,b.lapp_id
						 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
						 WHERE a.lrule=%s AND 
						 (a.lgroup=%s OR a.lgroup=%s) AND 
						 a.lrule_id=b.lrule_id AND
						 b.lapp_id=c.larticle_id AND
						 c.lshare_to=0 AND
						 b.lapp_id = c.larticle_id AND
						 b.lapp_id = %d
						 GROUP BY a.lrule_id
						 ORDER BY a.lorder",$type,'villas','villas',$id);
		$rc = $db->do_query($qc);
		return $rc;			
	}
	
function single_villa_discount_and_earlybird($roomtype_id,$arrival_date,$departure_date,$price,$return_type='array',$date_now=0,$discount_for='villa'){
	global $db;
	$days = ($departure_date - $arrival_date)/ 86400;
	
	$the_price = $price;
	$the_text_promo = "";
	$result = array();
	//get discount promo
	$data_discount_promo = $this->get_discount_data($roomtype_id,$arrival_date,$departure_date,$price,'disc_promo','Discount Promo',$days,$date_now,$discount_for);
	if(!empty($data_discount_promo)) {
		$discount_promo_value = $data_discount_promo['disc_value'];
		$discount_promo_text = $data_discount_promo['temp_disc'];
		$the_text_promo = $discount_promo_text;	
		$the_price = $the_price - $discount_promo_value;
	}
	
	//get early bird promo
	$data_early_bird 	= $this->get_discount_data($roomtype_id,$arrival_date,$departure_date,$price,'early_bird','Early Bird',$days,$date_now,$discount_for);
	
	if(!empty($data_early_bird )){
		$early_bird_value 	= $data_early_bird['disc_value'];
		$early_bird_text 	= $data_early_bird['temp_disc'];
		if($the_text_promo !='') $the_text_promo = $the_text_promo." and ";
		$the_text_promo .= $early_bird_text;
		$the_price = $the_price - $early_bird_value;
	}
	if($the_text_promo !=''){
		$result['price_discount']	= $the_price;
		$result['discount_text']	= $the_text_promo;
	}
	
	//print_r($result);
	return $result;
	
}

function get_discount_data($roomtype_id,$arrival_date,$departure_date,$price,$disc_type='disc_promo',$disc_text='Discount Promo',$days,$date_now =0,$discount_for){
	$result = array();
	//echo $arrival_date.'-'.$departure_date.'#';
	$data_disc = $this->data_tabels('lumonata_accommodation_promo',"where lacco_type_id=$roomtype_id  and lpromo_for='$discount_for'
						and ldate_from <= $arrival_date and $departure_date <= ldate_to and lpromo_type='$disc_type'",'array');
	$temp_disc = "";
	$disc_value = 0;					
	
	if(!empty($data_disc)){
		$not_valid_discount = 1;		
		if($disc_type=='early_bird'){
			if($date_now == 0) $date_now = strtotime(date('d M Y', time()));
			else {
					$date_now = date('d M Y', $date_now);
					$date_now = strtotime($date_now);			
			}
			$days_before = $data_disc['lday_before'] * 86400;			
			$valid_date = $arrival_date - $days_before;
			if($date_now <= $valid_date) $not_valid_discount=0;
		}else $not_valid_discount=0;
		
		//validate stay
		$stay =$data_disc['lstay'];
		$stay_to = $data_disc['lstay_to'];
		$valid_stay = 1;
		if($days >=$stay && $days <= $stay_to)$valid_stay = 0;
		
		
		
		if($not_valid_discount==0 && $valid_stay==0){
			$ammount_unit = $data_disc['lammount_unit'];
			$ammount = $data_disc['lammount'];
			if($ammount_unit=='%'){
				$disc_value = $price *(round($ammount)/100);
				$disc_promo_text  = round($ammount).'%';
			}else{
				$disc_value = $ammount;
				$disc_promo_text  = '$ '.$ammount;
			}		
			$temp_disc = "$disc_text($disc_promo_text)";
		
			$result['temp_disc'] = $temp_disc;
			$result['disc_value'] = $disc_value;
		}
		
	}
	return $result;
}
	
function single_villa_get_surcharge($arrival_date,$departure_date,$price,$return_type='array'){
	global $db;
	$days = ($departure_date - $arrival_date)/ 86400;
	
	$list_surcharge  = $this->data_tabels('lumonata_accommodation_surecharge',"where (
								   (ldate_from >= $arrival_date and $departure_date <= ldate_to) or (ldate_from=0 and ldate_to=0) ) ");
	
	
	$result = array();
	$the_surcharge = 0;	
	$temp = "";						   
	while($data_surcharge = $db->fetch_array($list_surcharge)){
		//validation date of week
		$not_valid_day_of_week = 0;
		$day_of_week = json_decode($data_surcharge['lday_of_week']);
		
		if($not_valid_day_of_week==0){
			$ammount_unit = $data_surcharge['lammount_unit'];
			$ammount = $data_surcharge['lcharge'];
			$desc_surcharge = $data_surcharge['ldescription'];
			$surcharge = 0;
			
			if($ammount_unit=='USD'){
				//$surcharge = $price - $ammount;
				$temp .= "<li>$desc_surcharge = USD ".number_format($ammount,2)."</li>";
				$the_surcharge = $the_surcharge + $ammount; 	
			}else{
				$surcharge = $price * ($ammount / 100);	
				$surcharge_text = number_format($surcharge,2);			
				$temp .= "<li>$desc_surcharge ($price * ".intval($ammount)."%) = USD $surcharge_text</li>";
				$the_surcharge = $the_surcharge + $surcharge; 	
			}
			
		}
	}//end while
	
	$result['temp'] = $temp;
	$result['the_surcharge'] = $the_surcharge;
	
	if($return_type=='array') return $result;
	else return json_encode($result);
}	
	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		//return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		//return $this->meta_key;
	}
	
}
?>