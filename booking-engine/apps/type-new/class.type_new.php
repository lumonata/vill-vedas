<?php
	class type_new extends db{
		function type_new($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Room Type New");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			//print_r($_GET); //, [bool return])
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			if(!empty($_POST)){
				print_r($_POST);
				$stmt = parent::prepare_query("INSERT INTO lumonata_accommodation_type (
				lacco_id,
				lname,
				lbrief,
				lorder_id,
				lcreated_by,
				lcreated_date) VALUES (%d,%s,'".$this->globalAdmin->setPrc2Code($_POST['brief'])."',%d,%s,%d)",
				$_SESSION['product_id'],
				$_POST['name'],
				1,
				$_SESSION['upd_by'],
				time());
				$this->globalAdmin->setOrderID("lumonata_accommodation_type",1);		
				$result = parent::query($stmt);
				if($result){
					$t->set_var('jsAction', 'notifBlockSaved();');	
					
					if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"type : new",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					} 
				}else{
					$t->set_var('jsAction', 'notifFailedSaved();');	
					//$t->set_var('error', "<div class=\"error\"><b>Failed!</b><br />Add new \"".$_POST['name'][0]."\" was unsuccessfully processed.</div>");	
				}
					
				
			}

			
			return $t->Parse('mBlock', 'mainBlock', false);
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>