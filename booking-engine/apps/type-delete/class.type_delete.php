<?php
	class type_delete extends db{
		function type_delete($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Room Type Delete");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			/*print_r($_GET); //, [bool return])
			echo "<br /><br />";
			print_r($_POST); //, [bool return])
			echo "<br /><br />";
			print_r($_SESSION);*/
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			if(!empty($_POST)){
				$msgTitle = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$_POST['acco_type_id']); 
				$stmt = parent::prepare_query("DELETE FROM lumonata_accommodation_type WHERE lacco_type_id =%d",$_POST['acco_type_id']);
				$result = parent::query($stmt);
				if($result){
					if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"type : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					} 
					
					$t->set_var('jsAction', 'notifBlockSaved();');
					header("Location:".SUPPLIERPANEL_SITE_URL."/type/");
				}else{
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
			}

			$query =  parent::prepare_query("SELECT *
				FROM lumonata_accommodation_type
				WHERE lacco_type_id =%d",$_GET['act2']);
			$result = parent::query($query);
			$data = parent::fetch_array($result);
			$t->set_var('pilih',$_POST['pilih'][$i]);
			$t->set_var('i', $i);	
			$t->set_var('acco_type_id', $data['lacco_type_id']);
			$t->set_var('name', $data['lname']);
			return $t->Parse('mBlock', 'mainBlock', false);
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>