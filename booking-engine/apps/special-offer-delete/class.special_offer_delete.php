<?php 
class special_offer_delete extends db{
	function special_offer_delete($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Special Offer Delete");
		
		require_once("../booking-engine/apps/special-offer/class.special_offer.php");
		$this->special_offer = new special_offer();
	}
	
	function load(){
		global $db;
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		//print_r($_POST);
		if(!empty($_POST['so_id'])) {
			$this->special_offer->do_delete($t);
			$t->set_var('msg_succsess', 'Special Offer was success deleted');
			$t->set_var('jsAction', 'notifBlockSaved();');	
			header("Location: http://".SUPPLIERPANEL_SITE_URL."/special-offer/");
		}
		
		$query =  $db->prepare_query("SELECT * FROM lumonata_special_offer WHERE lspecial_id =%d",$_GET['act2']);
		$result = $db->do_query($query);
		$data = $db->fetch_array($result);
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('i', $i);	
		$t->set_var('so_id', $data['lspecial_id']);
		$t->set_var('name', date('d-m-Y',$data['ldate_start']).' - '.date('d-m-Y',$data['ldate_valid']));
		return $t->Parse('mBlock', 'mainBlock', false);
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
}
?>