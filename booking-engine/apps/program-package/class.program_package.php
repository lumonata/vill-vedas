<?php 
class program_package extends db{
	function program_package($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Program Package");
		
		require_once("../lumonata-admin/functions/globalAdmin.php");
		$this->globalAdmin=new globalAdmin();
		$this->to=$this->globalAdmin->getSettingValue('email');
		$this->cc=$this->globalAdmin->getSettingValue('cc');
		$this->bcc=$this->globalAdmin->getSettingValue('bcc');
		global $globalSetting;
	}
	
	function get_room_type($id=0,$action='get_list'){
		  require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
		  $this->general_category_villa = new general_category_villa();
				 
		  if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
		  else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
	  }
	
	/*function get_room_type($id){
		require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
		$this->general_category_villa = new general_category_villa();		
		return $this->general_category_villa->get_category_type_data($id);	
	}*/
	
	function load(){
		global $db;		
		if(isset($_POST['prc']) && ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){//new additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			//get category
			$t->set_var('category_program',$this->get_list_category_program());
			$t->set_var('display_program','style="display:none"');
			$t->set_var('room_type',$this->get_room_type(0));
			//t->set_var('list_villa',$this->get_list_villa());
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
			$t->set_var('title', "New");			
			$t->set_var('i', 0);
			
			if(!empty($_POST['category-programs'][0])) $this->save_new($t);		
		}elseif(isset($_POST['prc']) && ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){//edit additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if($_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}			
			if (sizeof($_POST['pilih']) != 0){$this->show_multiple_edit($t);}//show multiple edit
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);			
		}else{	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'deleteContent', 'dC');
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if (isset($_POST['prc']) && $_POST['prc']=="delete" && sizeof($_POST['pilih']) != 0) $this->show_delete($t);
			if (isset($_POST['action']) && $_POST['action']=="Yes")	$this->do_delete($t);				
			
			$theQStr = $db->prepare_query("select * from lumonata_program_package");
			//BEGIN set var on viewContent Block
			$result = $db->do_query($theQStr);
			while($data = $db->fetch_array($result)){
				$user =  $data['lusername'];
				$data_user = $this->data_tabel('lumonata_users',"where lusername='$user'",'array');
				$t->set_var('check', $data['lpp_id']);
				$t->set_var('author', $data_user['ldisplay_name']);
				
				//$t->set_var('date_start',  date("d-m-Y",$data['ldate_start']));
				//$t->set_var('date_end',  date("d-m-Y",$data['ldate_end']));
				//$t->set_var('price', $data['lprice']);
				$t->set_var('single_occupancy_price', $data['lsingle_occupancy_price']);
				$t->set_var('double_occupancy_price', $data['ldouble_occupancy_price']);
				
				$id_program = $data['lprogram_id'];
				$data_program = $this->data_tabel('lumonata_articles',"where larticle_id=$id_program",'array');
				$t->set_var('program', $data_program['larticle_title']);
				
				$room_type_id = $data['lacco_type_id'];
				$data_room_type = $this->data_tabel('lumonata_rules',"where lrule_id=$room_type_id",'array');
				$t->set_var('room_type', $data_room_type['lname']);
				
				/*$occupancy_type = $data['loccupancy_type'];
				if($occupancy_type==1) $occupancy_name = 'Single Occupancy';
				else if($occupancy_type==2)$occupancy_name = 'Double Occupancy';
				$t->set_var('occupancy_type', $occupancy_name);*/
				
				//$id_villa = $data['lvilla_id'];
				//$data_villa = $this->data_tabel('lumonata_articles',"where larticle_id=$id_villa",'array');
				//$t->set_var('villa', $data_villa['larticle_title']);
				
				$t->Parse('vC', 'viewContent', true);
			}
			
			//END set var on viewContent Block	
		}//end if validate post
		return $t->Parse('mBlock', 'mainBlock', false);	
		
	}
	
	function get_list_category_program($selected_parent=0,$index=0,$id_show=0, $not_show_parent = false){
		require_once("../booking-engine/apps/program-reservation/class.program_reservation.php");
		$this->program_reservation = new program_reservation();
		return $this->program_reservation->get_list_category_program($selected_parent,$index,$id_show,true);	
		
	}
	
	function get_list_category_program_old($selected_parent=0,$index=0,$id_show=0, $not_show_parent = false){
		//print_r($selected_parent);
		global $db;
		$str 	=  $db->prepare_query("select * from lumonata_rules where lgroup=%s",'ratesreservations');
		$result = $db->do_query($str);
		$parent = "";
		if($not_show_parent==false) $parent .= "<option value=\"0\">Parent</option>";
		while($data= $db->fetch_array($result)){
			if($data['lrule_id']!=$id_show){
				if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
				else $select_info='';
				if($data['lparent']==0){
					//$parent .= "<option $select_info value=".$data['lrule_id'].">".$data['lname']."</option>";
					$parent .= $this->get_child($data['lrule_id'],1,$selected_parent);
				}else{
					/*$level = $this->find_level($data['lrule_id']);
					$nbs = "";
					for($i=1;$i<=$level;$i++){
						$nbs .= "&nbsp;&nbsp;&nbsp;";
					}
					$parent .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";*/
				} 
			}
		}
		
		return $parent;
	}
	
	function get_child($id,$level=0,$selected_parent){
		global $db;
		$str = $db->prepare_query("select * from lumonata_rules where lparent=%d",$id);
		$result = $db->do_query($str);
		$child = "";
		while($data= $db->fetch_array($result)){
			//echo $data.','.$selected_parent.'#';
			if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
			else $select_info='';
			$nbs = "";
			for($i=1;$i<=$level;$i++){
				$nbs .= "&nbsp;&nbsp;&nbsp;";
			}
			$child .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";
			$child .= $this->get_child($data['lrule_id'],$level+1,$selected_parent); 
		}
		return $child;
		
	}
	
	function find_level($id,$level=0){
		global $db;
		$str = $db->prepare_query("select * from lumonata_rules where lrule_id=%d",$id);
		$result = $db->do_query($str);
		
		while($data= $db->fetch_array($result)){
			if($data['lparent']==0){
				return $level;
			}else{
				return $this->find_level($data['lparent'],$level+1);
			}			
		}
		
	}
	
	
	
	
	/*function get_list_category_program($selected_id='',$index=0){
		global $db;
		
		$str = $db->prepare_query("select * from lumonata_rules where lgroup=%s order by lrule_id",'ratesreservations');	
		$result = $db->do_query($str);
		$list = "<option value=\"\">Select Category Program</option>";
		while($data = $db->fetch_array($result)){
			$id = $data['lrule_id'];
			if($id==$selected_id)$class = 'selected';
			else $class ='';
			$name = $data['lname'];
			$list .="<option value=\"$id\" $class>$name</option>";
		}
		
		return $list;
	}*/
	
	function get_list_villa($selected_id='',$room_type='',$index=0){
		global $db;
		$num_article = $this->data_tabel('lumonata_relationship_villa_category_room_type',"where lacco_type_id='$room_type'",'num_row');
		$return = "";
		if($num_article > 0){
			$result  = $this->data_tabel('lumonata_relationship_villa_category_room_type as a, lumonata_articles as b',
										 "where a.larticle_id=b.larticle_id and a.lacco_type_id='$room_type'");	
			$list = "";								 
			while($data = $db->fetch_array($result)){
				$id = $data['larticle_id'];
				$title = $data['larticle_title'];
				if($id==$selected_id)$class = 'selected';
				else $class ='';
				$list .= "<option value=\"$id\" $class>$title</option>";
			}
			$return = $list;
		}
		return $return;
	}
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");	
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function get_category_type_data($action='get_list',$id=0,$view_empty_option =false){
		require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
		$this->list_category_type_villa = new list_category_type_villa();
		if($action=='get_list') return $this->list_category_type_villa->load($id,$view_empty_option);	
		else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
	}
	
	function validate_post_value($t){		
		$error = 0;
		$msg ="";
		foreach($_POST['category-programs'] as $key => $value){
			if($value==''){
				$msg .= "<li>Please select category program</li>";
				$error++;	
			}
			if($_POST['programs'][$key]==''){
				$msg .= "<li>Please select program</li>";
				$error++;	
			}
			if($_POST['room_type'][$key]==''){
				$msg .= "<li>Please select room type</li>";
				$error++;	
			}
			if($_POST['single_occupancy_price'][$key]==''){
				$msg .= "<li>Please insert single occupancy price</li>";
				$error++;	
			}
			if($_POST['double_occupancy_price'][$key]==''){
				$msg .= "<li>Please insert double occupancy price</li>";
				$error++;	
			}
			/*if($_POST['occupancy_type'][$key]==''){
				$msg .= "<li>Please select occupancy type</li>";
				$error++;	
			}*/
			
			/*if($_POST['villa'][$key]==''){
				$msg .= "<li>Please select villa</li>";
				$error++;	
			}
			if($_POST['date_start'][$key]==''){
				$msg .= "<li>Please select date start</li>";
				$error++;	
			}
			if($_POST['date_finish'][$key]==''){
				$msg .= "<li>Please select date finish</li>";
				$error++;			
			}*/
			/*if($_POST['price'][$key]==''){
				$msg .= "<li>Please set price</li>";
				$error++;		
			}*/
		}
		
		
		if($error > 0){
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifFailedValidation();');	
			return false;
		}else return true;
	}
	
	function is_not_same_on_datebase($program_id,$room_type,$t){
		global $db;
		//echo 		"where lprogram_id=$program_id and lacco_type_id=$room_type and loccupancy_type=$occupancy_type";
		$num_package = $this->data_tabel('lumonata_program_package',"where lprogram_id=$program_id and lacco_type_id=$room_type", 'num_row');
		//echo '#'.$num_package;
		
		if($num_package==0) return true;
		else {
			$t->set_var('notif_message', "<ul><li>The Program and Room Type already exist. Please select different Program or Room Type.</li></ul>");
			$t->set_var('jsAction', 'notifFailedValidation();');	
			return false;
		}
	}
	
	
	
	
	function save_new($t){
		if($this->validate_post_value($t))	{
			global $db;
			if(isset($_POST['all_villa'][0]) && $_POST['all_villa'][0]=='yes'){
				$this->save_multiple_package($t);
			}else{
				$rule_id = $_POST['category-programs'][0];
				$program_id = $_POST['programs'][0];
				$room_type  = $_POST['room_type'][0];
				$single_occupancy_price = $_POST['single_occupancy_price'][0];
				$double_occupancy_price = $_POST['double_occupancy_price'][0];
				$program_commences = $_POST['program_commences'][0];
				$program_inclusion = $_POST['program_inclusion'][0];
				/*$occupancy_type = $_POST['occupancy_type'][0];
				$price = $_POST['price'][0];*/
				$user  = $_COOKIE['username'];
				$time = time();
				
				if($this->is_not_same_on_datebase($program_id,$room_type,$t)){
					require_once("../lumonata-functions/settings.php");					
					$str = $db->prepare_query("insert into lumonata_program_package 
										   (lrule_id,lprogram_id,lacco_type_id,lsingle_occupancy_price,
										    ldouble_occupancy_price,lcreated_by,lcreated_date,lusername,ldlu) values 
										   (%d,%d,%d,%d,
										    %d,%s,%d,%s,%d)",
										   $rule_id,$program_id,$room_type,$single_occupancy_price,
										   $double_occupancy_price,$user,$time,$user,$time);
											   
					$result = $db->do_query($str);						   
					if($result){
						
						$id = $this->get_max_id_program();
						add_additional_field($id,'program_commences',$program_commences,'program-package',true);
						add_additional_field($id,'program_inclusion',$program_inclusion,'program-package',true);						
						$t->set_var('notif_message', 'data saved');	
						$t->set_var('jsAction', 'notifBlockSaved();');	
					}else{
						$t->set_var('notif_message', 'failed to saved');
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}
				}//if not same on database
				
			}//end if save_all			
		}
	}
	
	function get_max_id_program(){
		global $db;
		$str 	= $db->prepare_query("select max(lpp_id) from lumonata_program_package");	
		$r		= $db->do_query($str);
		$d		= $db->fetch_array($r);
		
		return ($d[0]);
	}
	
	function save_multiple_package($t){
		global $db;
		
		$rule_id = $_POST['category-programs'][0];
		$program_id = $_POST['programs'][0];
		$room_type  = $_POST['room_type'][0];
		$date_start = strtotime($_POST['date_start'][0]);
		$date_finish = strtotime($_POST['date_finish'][0]);
		$price = $_POST['price'][0];
		$user  = $_COOKIE['username'];
		$time = time();
		
		$num_article = $this->data_tabel('lumonata_relationship_villa_category_room_type',"where lacco_type_id='$room_type'",'num_row');
		$return = "";
		if($num_article > 0){
			$result  = $this->data_tabel('lumonata_relationship_villa_category_room_type as a, lumonata_articles as b',
										 "where a.larticle_id=b.larticle_id and a.lacco_type_id='$room_type'");	
			$i = 0;	
			$values = "";					 
			while($data = $db->fetch_array($result)){
				$villa = $data['larticle_id'];
				if($i>0) $values .= ",";
				$values .= "($rule_id,$program_id,'$room_type',$villa,$date_start,
						   $date_finish,$price,'$user',$time,'$user',
						   $time)";
				$i++;
			}//end while
			
			$str = $db->prepare_query("insert into lumonata_program_package 
										   (lrule_id,lprogram_id,lacco_type_id,lvilla_id,ldate_start,
										   ldate_end,lprice,lcreated_by,lcreated_date,lusername,
										   ldlu) values 
										   $values");						   
			$result = $db->do_query($str);							   
			if($result){
				$t->set_var('notif_message', 'data saved');	
				$t->set_var('jsAction', 'notifBlockSaved();');	
			}else{
				$t->set_var('notif_message', 'failed to saved');
				$t->set_var('jsAction', 'notifFailedSaved();');	
			}
			
		}
	}	
	
	function show_multiple_edit($t){
		global $db;
		require_once("../lumonata-functions/settings.php");
		if(isset($_POST['pilih'])) $data_pilih = $_POST['pilih'];
		else $data_pilih = array($_GET['act2']);
		for ($i=0; $i<sizeof($data_pilih); $i++) { 
			$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
			$id_package = $data_pilih[$i];
			$data_program_package = $this->data_tabel('lumonata_program_package',"where lpp_id=$id_package",'array');
			$t->set_var('double_occupancy_price',$data_program_package['ldouble_occupancy_price']);	
			$t->set_var('single_occupancy_price',$data_program_package['lsingle_occupancy_price']);	
			$t->set_var('category_program',$this->get_list_category_program($data_program_package['lrule_id']));
			$t->set_var('program',$this->get_list_program($data_program_package['lrule_id'],$data_program_package['lprogram_id']));
			$t->set_var('room_type',$this->get_room_type($data_program_package['lacco_type_id']));
			$t->set_var('program_commences',get_additional_field($id_package,'program_commences','program-package'));
			$t->set_var('program_inclusion',get_additional_field($id_package,'program_inclusion','program-package'));
			$t->set_var('i',$i);
			
			$t->set_var('i',$i);
			$id = $data_pilih[$i];
			$t->set_var('pp_id',$id);
			$t->set_var('pilih',$id);
			$t->Parse('eC', 'editContent', true);
		}
	}
	
	function is_not_same_on_datebase_when_update($t){
		$same_on_database = 0;
		for($i=0;$i<count($_POST['pp_id']);$i++){
			$pp_id = $_POST['pp_id'][$i];
			$rule_id = $_POST['category-programs'][$i];
			$program_id = $_POST['programs'][$i];
			$room_type  = $_POST['room_type'][$i];
			
			
			$data_package = $this->data_tabel('lumonata_program_package',"where lprogram_id=$program_id and lacco_type_id=$room_type", 'array');
			if(!empty($data_package)){
				if($data_package['lpp_id']!=$pp_id) $same_on_database++;	
			}//end if not empty			
		}//end for
		if($same_on_database==0) return true;
		else {
			$t->set_var('notif_message', "<ul><li>The data package will you update has already exists in the database. 
Please choose different Programs or Room Type to update.</li></ul>");
			$t->set_var('jsAction', 'notifFailedValidation();');	
			return false;
		}
				
	}
	
	function edit_multiple_data($t){
		global $db;require_once("../lumonata-functions/settings.php");
		$msg = "";
		$error = 0;
		
		//check is update data is not exist before
		if($this->is_not_same_on_datebase_when_update($t)){
			for($i=0;$i<count($_POST['pp_id']);$i++){
				$pp_id = $_POST['pp_id'][$i];
				$rule_id = $_POST['category-programs'][$i];
				$program_id = $_POST['programs'][$i];
				$room_type  = $_POST['room_type'][$i];
				$single_occupancy_price = $_POST['single_occupancy_price'][$i];
				$double_occupancy_price = $_POST['double_occupancy_price'][$i];
				$program_commences = $_POST['program_commences'][$i];
				$program_inclusion = $_POST['program_inclusion'][$i];
				$user  = $_COOKIE['username'];
				$time = time();			
				
				
				$str = $db->prepare_query("update lumonata_program_package set
											lrule_id=%d,lprogram_id=%d,lacco_type_id=%s,lsingle_occupancy_price=%d,
											ldouble_occupancy_price=%d,lusername=%s,ldlu=%d 
											where lpp_id=%d",$rule_id,$program_id,$room_type,$single_occupancy_price,
											$double_occupancy_price,$user,$time,$pp_id);
				
					$result = $db->do_query($str);	
					if($result){
						//upload image
						edit_additional_field($pp_id,'program_commences',$program_commences,'program-package',true);
						edit_additional_field($pp_id,'program_inclusion',$program_inclusion,'program-package',true);						
						$msg = "<li>Update was successfully processed.</li>";	
					}else{
						$error=1;
						$msg = "<li>Update was failed processed.</li>";	
					}	
										   
				
			}
			if($error==1){
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifFailedSaved();');	
			}else{
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifBlockSaved();');	
			}
		}
		
		
		
	}
	
	function get_list_program($id_rule,$selected_id=0){
		global $db;
		//valid id
		$num_rule = $this->data_tabel('lumonata_rules',"where lrule_id=$id_rule",'num_row');
		
		$return = "";
		if($num_rule>0){
			$result_program = $this->data_tabel('lumonata_rules as a, lumonata_rule_relationship as b, lumonata_articles as c',
												"where a.lrule_id = b.lrule_id and b.lapp_id = c.larticle_id and a.lgroup='ratesreservations' 
												 and c.larticle_type='ratesreservations' and b.lrule_id=$id_rule");
			$list = "";									 
			while($data_program  = $db->fetch_array($result_program)){
				$id = $data_program['larticle_id'];
				$title = $data_program['larticle_title'];
				if($id==$selected_id)$class = 'selected';
				else $class ='';
				$list .= "<option value=\"$id\" $class>$title</option>";
			}
			$return = $list;									
		}
		return $return;
	}
	
	function show_delete($t){
		global $db;
		$alltitle = '';
		
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query = $db->prepare_query("SELECT * FROM lumonata_program_package where lpp_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$t->set_var('date_start',  date("d-m-Y",$data['ldate_start']));
			$t->set_var('date_end',  date("d-m-Y",$data['ldate_end']));
			
			
			$id_program = $data['lprogram_id'];
			$data_program = $this->data_tabel('lumonata_articles',"where larticle_id=$id_program",'array');
			$program = $data_program['larticle_title'];
			
			
			$room_type = $this->get_room_type($data['lacco_type_id'],'get_name');
			/*$id_villa = $data['lvilla_id'];
			$data_villa = $this->data_tabel('lumonata_articles',"where larticle_id=$id_villa",'array');
			$villa = $data_villa['larticle_title'];*/
			//echo 'dd';
			
			$t->set_var('i', $i);
			$t->set_var('delId', $_POST['pilih'][$i]);
			$n1 = 0;
			
			if ($n1 != 0)
			{
				$title = $title."<li>$program - $room_type</li>";
			}
			$alltitle = $alltitle."<li>$program - $room_type</li>";
			$t->Parse('dC', 'deleteContent', true);
		}
		$msg = "<div class=\"confirm\"><b>Do you really want to DELETE Program Package for:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
		$t->set_var('notif_message', $msg);
	}
	
	function do_delete($t){
		global $db;require_once("../lumonata-functions/settings.php");
		$msg = '';
		$error = '';
		if(isset($_POST['delId'])) $data_pilih = $_POST['delId'];
		else $data_pilih = array($_POST['pp_id']);
		
		for ($i=0; $i<sizeof($data_pilih); $i++) {
			$str = $db->prepare_query("select * from lumonata_program_package where lpp_id=%d",$data_pilih[$i]);
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			
			$id_program = $data['lprogram_id'];
			$data_program = $this->data_tabel('lumonata_articles',"where larticle_id=$id_program",'array');
			$program = $data_program['larticle_title'];
			
			$room_type = $this->get_room_type($data['lacco_type_id'],'get_name');
			
			$str_delete = $db->prepare_query("delete from lumonata_program_package where lpp_id=%d",$data_pilih[$i]);
			$result_delete = $db->do_query($str_delete);
			if($result_delete) {
				delete_additional_field($data_pilih[$i],'program-package');
				$msg .= "<li>Delete Program Package for $program - $room_type was successfully processed.</li>";
			}else{
				$error = 1;
				$msg .= "<li>Delete Program Package for $program - $room_type was unsuccessfully processed.</li>";
			}
		}
		
		if($error==1){
			$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
		}else{
			$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
		}
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return (isset($this->meta_desc) ? $this->meta_desc:'');//$this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return (isset($this->meta_key)? $this->meta_key:'');
	}	
		
}?>