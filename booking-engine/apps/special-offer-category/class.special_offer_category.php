<?php 
class special_offer_category extends db{
	function special_offer_category($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Special Offer Category");
		
		require_once("../lumonata-admin/functions/globalAdmin.php");
		$this->globalAdmin=new globalAdmin();
		$this->to=$this->globalAdmin->getSettingValue('email');
		$this->cc=$this->globalAdmin->getSettingValue('cc');
		$this->bcc=$this->globalAdmin->getSettingValue('bcc');
		global $globalSetting;
	}
	
	function load(){
		global $db;
		if(isset($_POST['prc']) && ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){//new additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
			$t->set_var('title', "New");			
			$t->set_var('i', 0);
			
			if(isset($_POST['name'])) $this->save_category_special_offer($t);
				
		}elseif(isset($_POST['prc']) && ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){//edit additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if($_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}			
			if (sizeof($_POST['pilih']) != 0){$this->show_multiple_edit($t);}//show multiple edit
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}elseif(isset($_POST['prc']) && $_POST['prc'] == "category" ){//edit additional service
			header('Location: http://'.SITE_URL.'/booking-engine/special-offer-category/');
		}else{	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'deleteContent', 'dC');
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if (isset($_POST['prc']) && $_POST['prc']=="delete" && sizeof($_POST['pilih']) != 0) $this->show_delete($t);
			if (isset($_POST['action']) && $_POST['action']=="Yes")	$this->do_delete($t);				
			
			
			$theQStr = $db->prepare_query("select * from lumonata_special_offer_categories order by ldlu");
			//BEGIN set var on viewContent Block
			$result = $db->do_query($theQStr);
			
			while($data = $db->fetch_array($result)){
				//print_r($data);
				$t->set_var('name',$data['lcategory']);
				$t->set_var('check', $data['lcat_id']);
				/*$data_user = $this->data_table('lumonata_users','where lusername='.$data['lusername']);
				$data_user = $db->fetch_array($data_user);					
				$t->set_var('author', $data_user['ldisplay_name']);*/
				$t->set_var('decription',$data['ldescription']);
				$t->set_var('date',  date("Y-m-d H:i:s", $data['ldlu']));
				$t->Parse('vC', 'viewContent', true);
			}
			
			//END set var on viewContent Block	
		}//end if validate post
		return $t->Parse('mBlock', 'mainBlock', false);	
	}
	
	function save_category_special_offer($t){
		global $db;
		$name = $_POST['name'][0];
		$sef  =  strtolower(str_replace(' ','-',$name));
		$desc = $_POST['description'][0];
		$user = $_COOKIE['username'];
		$date = time();
		$id = $this->get_new_id_special_offer();
		
		
		
		$str =  $db->prepare_query("insert into lumonata_special_offer_categories 
									(lcat_id,lcategory,ldescription,lsef_url,lorder,
									 lcreated_by,lcreated_date,lusername,ldlu)
									values 
									(%d,%s,%s,%s,%d,
									 %s,%d,%s,%d)",
									 $id,$name,$desc,$sef,0,
									 $user,$date,$user,$date);
			
		$result = $db->do_query($str);
		if ($result){							
			$t->set_var('notif_message', 'data saved');	
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}else{
			$t->set_var('notif_message', 'failed to saved');
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}
	}
	
	function get_new_id_special_offer(){
		global $db;
		$str = $db->prepare_query("select * from lumonata_special_offer_categories");
	
		$result = $db->do_query($str);	
		$num = $db->num_rows($result);
		
		return $this->is_id_exist($num+1);
	}
	
	function is_id_exist($id){
		global $db;
		$str = $db->prepare_query("select * from lumonata_special_offer_categories where lcat_id=%d",$id);
		$result = $db->do_query($str);	
		$data = $db->fetch_array($result);
		
		if(empty($data)){
			return $id;
		}else{
			return $this->is_id_exist($id+1);	
		}
		
	}
	
	
	function show_multiple_edit($t){
		global $db;
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query =  $db->prepare_query("SELECT * FROM lumonata_special_offer_categories WHERE lcat_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$t->set_var('pilih',$_POST['pilih'][$i]);
			$t->set_var('asc_id', $data['lcat_id']);
			$t->set_var('i', $i);	
			$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
			$t->set_var('name', $data['lcategory']);
			$t->set_var('description', $data['ldescription']);
     		$t->Parse('eC', 'editContent', true); 
		}
	}
	
	function edit_multiple_data($t){
		global $db;
		$msg = "";
		$error = 0;
		for($i=0;$i<count($_POST['asc_id']);$i++){
			$name = $_POST['name'][$i];
			$sef =  strtolower(str_replace(' ','-',$name));
			$desc =  $_POST['description'][$i];
			
			$str  = $db->prepare_query("update lumonata_special_offer_categories set
									 lcategory=%s,
									 lsef_url=%s,
									 ldescription=%s
									 where lcat_id=%d
									",$name,$sef,$desc,$_POST['asc_id'][$i]);
											
			$result = $db->do_query($str);						
			if($result)$msg .= "<li>Update special offer for \"".$_POST['name'][$i]."\" was successfully processed.</li>";
			else{
				$error = 1;
				$msg .= "<li>Update special offer for \"".$_POST['name'][$i]."\" was unsuccessfully processed.</li>";
			} 				
		}
		
		if($error==1){
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}else{
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}
	}
	
	function show_delete($t){
		global $db;
		$alltitle = '';
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query = $db->prepare_query("SELECT * FROM lumonata_special_offer_categories where lcat_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$t->set_var('i', $i);
			$t->set_var('delId', $_POST['pilih'][$i]);
			$n1 = 0;
			

			if ($n1 != 0)
			{
				$title = $title."<li>".$data['lcategory']."</li>";
			}
						
			$alltitle = $alltitle."<li>".$data['lcategory']."</li>";
			$t->Parse('dC', 'deleteContent', true);
		}
		$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
		$t->set_var('notif_message', $msg);
	}
	
	function do_delete($t){
		global $db;
		$msg = '';
		$error = '';
		for ($i=0; $i<sizeof($_POST['delId']); $i++) {
			$str = $db->prepare_query("select * from lumonata_special_offer_categories where lcat_id=%d",$_POST['delId'][$i]);
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			
			$str_delete = $db->prepare_query("delete from lumonata_special_offer_categories where lcat_id=%d",$_POST['delId'][$i]);
			$result_delete = $db->do_query($str_delete);
			if($result_delete) {
				$msg .= "<li>Delete Additional Service Category for \"".$data['lcategory']."\" was successfully processed.</li>";
			}else{
				$error = 1;
				$msg .= "<li>Delete Additional Service Category for \"".$data['lcategory']."\" was unsuccessfully processed.</li>";
			}
		}
		
		if($error==1){
			$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
		}else{
			$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
		}
	}
	
	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
}
?>