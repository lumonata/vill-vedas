<?php
	class cancellation_policy extends db{
		function cancellation_policy($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Cancellation Policy");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin = new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];	
				$bySupplier = false;
			}
			
// START PROCESSING HERE =================================================================================================================================			
			if ((isset($_POST['prc']) And $_POST['prc'] == "new") || (isset($_POST['prc']) And $_POST['prc'] == "save_new")){
// ADD PROCESS START HERE =================================================================================================================================
				$OUT_TEMPLATE = "main.html";
				$t = new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				// NEW SAVED START =================================================================================================================================
				if($_POST['prc'] == "save_new"){
					$cancelpolicy_cat_ID = $this->globalAdmin->setCode("lumonata_accommodation_cancelationpolicy_categories","lcancelpolicy_cat_ID");
					$stmt = $db->prepare_query( "INSERT INTO lumonata_accommodation_cancelationpolicy_categories (
															lcancelpolicy_cat_ID, lacco_type_cat_id, ldate_from, ldate_to, 
															lrule1, lrule2, lrule3, ldlu,
															lcancellation_policy) 
													VALUES (%d, %s, %d, %d, %d, %d, %d, %d,
															%s)",
															$cancelpolicy_cat_ID, $_POST['roomtype'][0], strtotime($_POST['date_start'][0]), strtotime($_POST['date_finish'][0]),
															$_POST['lrule1'][0], $_POST['lrule2'][0], $_POST['lrule3'][0], time() ,
															$_POST['lcancellation_policy'][0]);
					$result = $db->do_query($stmt);
					if ($result){
						$qat = $db->prepare_query("Select * From lumonata_accommodation_type Where lno_of_bedroom = %d",$_POST['roomtype'][0]);
						$rat = $db->do_query($qat);
						while ($dat = $db->fetch_array($rat)){
							$stmt = $db->prepare_query( "INSERT INTO lumonata_accommodation_cancelationpolicy (
																lcancelpolicy_ID, lacco_type_id, ldate_from, ldate_to, 
																lrule1, lrule2, lrule3, ldlu,
																lcancelpolicy_cat_ID, lcancellation_policy) 
														VALUES (%d, %d, %d, %d, %d, %d, %d, %d,
																%d, %s)",
																'', $dat['lacco_type_id'], strtotime($_POST['date_start'][0]), strtotime($_POST['date_finish'][0]),
																$_POST['lrule1'][0], $_POST['lrule2'][0], $_POST['lrule3'][0], time() ,
																$cancelpolicy_cat_ID, $_POST['lcancellation_policy'][0]);
							$result = $db->do_query($stmt);
							
							
							if($result){
								if($bySupplier){
									$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season : new",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
								} 
								
								//$this->syncTableAvailability();
								//$t->set_var('notif_message', 'Data Saved');	
								//$t->set_var('jsAction', 'notifBlockSaved();');	
							}else{
								//$t->set_var('notif_message', 'Failed to saved');
								//$t->set_var('jsAction', 'notifFailedSaved();');	
							}
							
						}
					
						
						//$this->syncTableAvailability();
						$t->set_var('notif_message', 'Data Saved');	
						$t->set_var('jsAction', 'notifBlockSaved();');	
					}else{
						$t->set_var('notif_message', 'Failed to saved');
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}
					
					
					
				}
				// NEW SAVED END =================================================================================================================================
				
				$t->set_var('i', 0);
			/*	$qry = $db->prepare_query("SELECT * FROM lumonata_accommodation_type_categories Order by lname 	Asc ");
				$rs = $db->do_query($qry);
				$options = "";
				while($dt = $db->fetch_array($rs)){
					if (isset($item_id) And ($item_id == $dt['lacco_type_id'])){
						$options .= "<option value=\"$dt[lacco_type_cat_id]\" selected=\"selected\">$dt[lname]</option>";			
					}else{
						$options .= "<option value=\"$dt[lacco_type_cat_id]\">$dt[lname]</option>";	
					}
				}
				$options .= "";
				$t->set_var('room_category',$options);*/
				$t->set_var('room_category',$this->get_room_type());

				
				$t->set_var('date_start',  "mm/dd/yy"); 	 	
				$t->set_var('date_finish', "mm/dd/yy"); 
				
				$t->set_var('rule1List', $this->getRuleList(0,0));
				
				$t->set_var('rule2List', $this->getRuleList(1,0));
				$t->set_var('rule3List', $this->getRuleList(2,0));
				
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");
				
				return $t->Parse('mBlock', 'mainBlock', false);
// ADD PROCESS ENDS HERE =================================================================================================================================	
			}else if ((isset($_POST['prc']) And $_POST['prc'] == "edit") || (isset($_POST['prc']) And $_POST['prc'] == "save_edit")){
// EDIT PROCESS START HERE =================================================================================================================================
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				// NEW SAVED START =================================================================================================================================
				if($_POST['prc'] == "save_edit"){
					$msg = '';
					$error = '';
					for($i=0;$i<count($_POST['lcancelpolicy_ID']);$i++){
						
							$stmt = $db->prepare_query( "UPDATE lumonata_accommodation_cancelationpolicy_categories SET
															lacco_type_cat_id=%s,
															ldate_from=%d,
															ldate_to=%d,
															
															lrule1=%d,
															lrule2=%d,
															lrule3=%d,
															
															ldlu=%d,
															lcancellation_policy=%s
															WHERE lcancelpolicy_cat_ID=%d",
															$_POST['roomtype'][$i], strtotime($_POST['date_start'][$i]), strtotime($_POST['date_finish'][$i]),
															$_POST['lrule1'][$i], $_POST['lrule2'][$i], $_POST['lrule3'][$i], time(), $_POST['lcancellation_policy'][$i], 
															$_POST['lcancelpolicy_ID'][$i]);
							$result = $db->do_query($stmt);
							if($result){
								$qat = $db->prepare_query("Select * From lumonata_accommodation_type Where lno_of_bedroom = %d",$_POST['roomtype'][$i]);
								$rat = $db->do_query($qat);
								while ($dat = $db->fetch_array($rat)){
									$qs = $db->prepare_query("Select * From lumonata_accommodation_cancelationpolicy Where lacco_type_id=%d And lcancelpolicy_cat_ID=%d",$dat['lacco_type_id'], $_POST['lcancelpolicy_ID'][$i]);
									$rs = $db->do_query($qs);
									$js = $db->num_rows($rs);
									if ($js > 0){
										
										$stmt = $db->prepare_query( "UPDATE lumonata_accommodation_cancelationpolicy SET
															lacco_type_id=%d,
															ldate_from=%d,
															ldate_to=%d,
															
															lrule1=%d,
															lrule2=%d,
															lrule3=%d,
															
															ldlu=%d,
															lcancellation_policy=%s
															WHERE lcancelpolicy_cat_ID=%d",
															$dat['lacco_type_id'], strtotime($_POST['date_start'][$i]), strtotime($_POST['date_finish'][$i]),
															$_POST['lrule1'][$i], $_POST['lrule2'][$i], $_POST['lrule3'][$i], time(), $_POST['lcancellation_policy'][$i], 
															$_POST['lcancelpolicy_ID'][$i]);
										$result = $db->do_query($stmt);
									}else{
										$stmt = $db->prepare_query( "INSERT INTO lumonata_accommodation_cancelationpolicy (
																lcancelpolicy_ID, lacco_type_id, ldate_from, ldate_to, 
																lrule1, lrule2, lrule3, ldlu,
																lcancelpolicy_cat_ID, lcancellation_policy) 
														VALUES (%d, %d, %d, %d, %d, %d, %d, %d,
																%d, %s)",
																'', $dat['lacco_type_id'], strtotime($_POST['date_start'][$i]), strtotime($_POST['date_finish'][$i]),
																$_POST['lrule1'][$i], $_POST['lrule2'][$i], $_POST['lrule3'][$i], time() , $_POST['lcancellation_policy'][$i],
																$_POST['lcancelpolicy_ID'][$i]);
										$result = $db->do_query($stmt);
										
									}
								}

								//$msg .= "<li>Update \"".$_POST['lcancelpolicy_ID'][$i]."\" was successfully processed.</li>";
								$msg .= "<li>Update was successfully processed.</li>";
							}else{
								$error = 1;
								//$msg .= "<li>Update \"".$_POST['lcancelpolicy_ID'][$i]."\" was unsuccessfully processed.</li>";
								$msg .= "<li>Update was unsuccessfully processed.</li>";
							}
							
							
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<ul>".$msg."</ul>");
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}else{
						$t->set_var('notif_message', "<ul>".$msg."</ul>");
						$t->set_var('jsAction', 'notifBlockSaved();');	
						//$this->syncTableAvailability();
						
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 
					}
				}
				// NEW SAVED END =================================================================================================================================
				
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$query =  $db->prepare_query("SELECT * FROM lumonata_accommodation_cancelationpolicy_categories
														 WHERE lcancelpolicy_cat_ID=%d", $_POST['pilih'][$i]);
						$result = $db->do_query($query);
						$data   = $db->fetch_array($result);
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						
						/*$qry = $db->prepare_query("SELECT * FROM lumonata_accommodation_type_categories Order by lname 	Asc ");
						$rs = $db->do_query($qry);
						$options = "";
						while($dt = $db->fetch_array($rs)){
							if ($dt['lacco_type_cat_id']==$data['lacco_type_cat_id']){
								$options .= "<option value=\"$dt[lacco_type_cat_id]\" selected=\"selected\">$dt[lname]</option>";			
							}else{
								$options .= "<option value=\"$dt[lacco_type_cat_id]\">$dt[lname]</option>";	
							}
						}
						$options .= "";
						$t->set_var('room_category',$options);*/
						$t->set_var('room_category',$this->get_room_type($data['lacco_type_cat_id'],'get_list'));
						
						$t->set_var('rule1List', $this->getRuleList(0,$data['lrule1']));
						$t->set_var('rule2List', $this->getRuleList(1,$data['lrule2']));
						$t->set_var('rule3List', $this->getRuleList(2,$data['lrule3']));
						
						$t->set_var('cancellation_policy', $data['lcancellation_policy']); 	 	
						
						
						$t->set_var('date_start', date("m/d/Y",$data['ldate_from'])); 	 	
						$t->set_var('date_finish', date("m/d/Y",$data['ldate_to'])); 	
						$t->set_var('lcancelpolicy_ID', $_POST['pilih'][$i]); 					
						
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-bottom:solid 1px #ccc; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");					
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				$t->set_var('title', "Edit");
				
				return $t->Parse('mBlock', 'mainBlock', false);
// EDIT PROCESS ENDS HERE =================================================================================================================================	
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');	
				
				// DELETE PROCESS START ===============================================================================================================================
				if ((isset($_POST['prc']) And $_POST['prc'] == "delete")){
					if (sizeof($_POST['pilih']) != 0){
						$alltitle = '';
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							/*$query = $db->prepare_query("SELECT *
															FROM lumonata_accommodation_cancelationpolicy_categories
															WHERE lcancelpolicy_cat_ID=%d ",$_POST['pilih'][$i]);*/
							$query = $db->prepare_query("SELECT * FROM lumonata_accommodation_cancelationpolicy_categories														
															WHERE lcancelpolicy_cat_ID=%d",$_POST['pilih'][$i]);								
															
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;
							
							/*$name  = $this->globalAdmin->getValueField("lumonata_accommodation_type_categories","lname","lacco_type_cat_id",$data['lacco_type_cat_id']); */
							$name = $this->get_room_type($data['lacco_type_cat_id'],'get_name');
							
							if ($n1 != 0){
								$title = $title."<li>".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."</li>";
							}
							$alltitle = $alltitle."<li>".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, season below using in accommodation season data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				
				
				
				if ((isset($_POST['action']) And $_POST['action']=="Yes")){
					$msg = '';
					$error = '';
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						$query = $db->prepare_query("SELECT *
														FROM lumonata_accommodation_cancelationpolicy_categories
														WHERE lcancelpolicy_cat_ID=%d ", $_POST['delId'][$i]);
						$result = $db->do_query($query);
						$data   = $db->fetch_array($result);
						
						$stmt   = $db->prepare_query("DELETE FROM lumonata_accommodation_cancelationpolicy WHERE lcancelpolicy_cat_ID=%d",$data['lcancelpolicy_cat_ID']);
						$result = $db->do_query($stmt);
						
						$stmt   = $db->prepare_query("DELETE FROM lumonata_accommodation_cancelationpolicy_categories WHERE lcancelpolicy_cat_ID=%d",$_POST['delId'][$i]);
						$result = $db->do_query($stmt);
						
						$name   = $this->globalAdmin->getValueField("lumonata_accommodation_type_categories","lname","lacco_type_cat_id",$data['lacco_type_cat_id']); 
						if($result){
							$msg .= "<li>Delete \"".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
					}else{
						//$this->syncTableAvailabilityWhenDeletAccur();
						
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 	
					}
				}
				// DELETE PROCESS END =================================================================================================================================
				
				
				/*$query =  $db->prepare_query("SELECT lumonata_accommodation_cancelationpolicy_categories.*, lumonata_accommodation_type_categories.lname
												 FROM lumonata_accommodation_cancelationpolicy_categories LEFT JOIN lumonata_accommodation_type_categories ON lumonata_accommodation_cancelationpolicy_categories.lacco_type_cat_id=lumonata_accommodation_type_categories.lacco_type_cat_id");*/
				
				
				$query =  $db->prepare_query("select * from lumonata_accommodation_cancelationpolicy_categories order by ldate_from");		
				
				$result = $db->do_query($query);
				$no = 0;
				while($data = $db->fetch_array($result)){
					$t->set_var('no', $no);	 	
					$name = $this->get_room_type($data['lacco_type_cat_id'],'get_name');		
					$t->set_var('name', $name);
					$t->set_var('check', 		$data['lcancelpolicy_cat_ID']);
					$t->set_var('date_start', 	date("d M Y",$data['ldate_from'])); 	 	
					$t->set_var('date_finish', 	date("d M Y",$data['ldate_to'])); 
					
					$t->Parse('vC', 'viewContent', true); 	
				}
				return $t->Parse('mBlock', 'mainBlock', false);
			}
// END PROCESSING HERE =================================================================================================================================			
		}
		
		function getRuleList($level, $select){
			global $db;
			
			$str = "SELECT * FROM lumonata_accommodation_cancelationpolicy_master WHERE lparent=0";
			$sql = $db->prepare_query($str);
			$res = $db->do_query($sql);
			$all = '';
			while( $rec=$db->fetch_array($res) ){
				if($level==0){
					if($select==$rec['lcancelpolicy_master_ID']){ $sel = ' selected="selected" '; }else{ $sel=''; }
					$all  .= '<option class="level" '.$sel.' value="'.$rec['lcancelpolicy_master_ID'].'">'.$rec['ldescription'].' ('.number_format($rec['lcharge'],2).' '.$rec['lunit'].')</option>';
				}else{
					$str1 = "SELECT * FROM lumonata_accommodation_cancelationpolicy_master WHERE lparent=".$rec['lcancelpolicy_master_ID'];
					$sql1 = $db->prepare_query($str1);
					$res1 = $db->do_query($sql1);
					while( $rec1=$db->fetch_array($res1) ){
						if($level==1){
							if($select==$rec1['lcancelpolicy_master_ID']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all  .= '<option class="level" rel="'.$rec['lcancelpolicy_master_ID'].'" '.$sel.' value="'.$rec1['lcancelpolicy_master_ID'].'">'.$rec1['ldescription'].' ('.number_format($rec1['lcharge'],2).' '.$rec1['lunit'].')</option>';
						}else{
							$str2 = "SELECT * FROM lumonata_accommodation_cancelationpolicy_master WHERE lparent=".$rec1['lcancelpolicy_master_ID'];
							$sql2 = $db->prepare_query($str2);
							$res2 = $db->do_query($sql2);
							while( $rec2=$db->fetch_array($res2) ){
								if($select==$rec2['lcancelpolicy_master_ID']){ $sel = ' selected="selected" '; }else{ $sel=''; }
								$all  .= '<option class="level" rel="'.$rec1['lcancelpolicy_master_ID'].'" '.$sel.' value="'.$rec2['lcancelpolicy_master_ID'].'">'.$rec2['ldescription'].' ('.number_format($rec2['lcharge'],2).' '.$rec2['lunit'].')</option>';
							}
						}
					}
				}
								
			}
			return $all;	
		}
		
		function get_category_type_data($action,$id=0){
			require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
			$this->list_category_type_villa = new list_category_type_villa();
			if($action=='get_list') return $this->list_category_type_villa->load($id);	
			else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
		}
		
		function get_room_type($id,$action='get_list'){
			require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
			$this->general_category_villa = new general_category_villa();
					
			if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
			else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
		}
		/*function get_list_villa(){
			require_once("../booking-engine/apps/list_villa/class.list_villa.php");
			$this->list_villa = new list_villa();
			return $this->list_villa->just_option();	
		}*/
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>