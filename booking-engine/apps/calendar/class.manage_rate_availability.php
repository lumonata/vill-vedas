<?php
class manage_rate_availability extends calendar {
	function load(){
		//define variable
		global $db;
		$accom_id = $_POST['accom_id'];
		$rate = $_POST['rate'];		
		$reason = $_POST['reason'];
		$status = $_POST['status'];	
		$days = $_POST['days'];
		$ss = $_POST['ss'];
		$es = $_POST['es'];
		
		//get user data
		if(isset($_COOKIE['member_log'])){//if is member
			$username    = $_COOKIE['member_log']['id'];
			$user_type	 = 'member';
		}else{//if is admin
			$username  = $_COOKIE['username'];
			$user_type = $_COOKIE['user_type'];
		}		
		
		$error=0;
		for($i=0;$i<=$days;$i++){
			$date = $ss + ($i * 86400);	
			//find is exist
			$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%s";
			$query = $db->prepare_query($str, $date , $accom_id);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			$n = $db->num_rows($result);	
			
			if($n>0){ //if exist do update				
				if(($status==0 || $status==5 || $status==6) && $i==$days){				
					//just update rate
					if($rate!='' ||$rate != 0 ) {
						$str_update = "update lumonata_availability set lrate=%d, ledit=%s, ldlu=%d where ldate=%d and lacco_id=%d";
						$query_update = $db->prepare_query($str_update, $rate, $username, time(),$date,$accom_id);
						if($db->do_query($query_update)==false)$error ++;								
					}else{						
						if(!$this->exist_the_checkin($date,$es)){
							
							$str_update = "update lumonata_availability set lstatus=%d, ledit=%s, ldlu=%d where ldate=%d and lacco_id=%d";
							$query_update = $db->prepare_query($str_update, $status, $username, time(),$date,$accom_id);	
							if($db->do_query($query_update)==false)	$error ++;								
						}else{} //do nothing;						
					}					
				}else{					
					//check if this date not booking, booking-hold and owner
					if(!$this->exist_the_checkin($date,$es)){						
						if($rate=='' ||$rate == 0 ) {
							$str_update = "update lumonata_availability set 
									   		lstatus=%d, 
									   		lreason=%s, ledit=%s, ldlu=%d 
									   where ldate=%d and lacco_id=%d";
							$query_update = $db->prepare_query($str_update, 
															$status,
															$reason, $username, time(),$date,$accom_id);
						}else{
							$str_update = "update lumonata_availability set 
									   		lrate=%d, lstatus=%d, 
									   		lreason=%s, ledit=%s, ldlu=%d 
									   where ldate=%d and lacco_id=%d";
							$query_update = $db->prepare_query($str_update, 
															$rate , $status,
															$reason, $username, time(),$date,$accom_id);	
						}
						
						if($db->do_query($query_update)==false)	$error ++;	
					}					
				}				
			}else{ //do insert
				if($rate=='' || $rate==0)$rate = DEFAULT_PRICE_VILLA;
				//insert into availability
				if(($status==0 || $status==5 || $status==6) && $i==$days){//for the last book					
					$str_insert = "insert into lumonata_availability values (%d,%d,%d,%d,
																			 %s,%d,%s,%s,
																			 %s,%d,%s,%d)" ;
					$query_insert = $db->prepare_query($str_insert, 
														$date,$accom_id,$rate,$rate,
														'',1,$reason,$username,
														$username,time(),$username,time());
					//do nothing
				}else{
					
					$str_insert = "insert into lumonata_availability values (%d,%d,%d,%d,
																			 %s,%d,%s,%s,
																			 %s,%d,%s,%d)" ;
					$query_insert = $db->prepare_query($str_insert, 
														$date,$accom_id,$rate,$rate,
														'',$status,$reason,$username,
														$username,time(),$username,time());
				}
				//print_r($_POST);
				if($db->do_query($query_insert)==false){
					//echo 'hihih';
					$error ++;
				}				
			}
			
		}//end for
		
		if($error==0){
			//if status is not available
			if($status==0 ){//insert into booking 
				//echo exist_booking();
				if($this->exist_booking($_POST['accom_id'],$_POST['ss'],$_POST['es'])){//if is booking hold so just send email
				 	$this->send_the_emails();
				}else{			
					if($this->save_booking_admin($_POST,$status))echo 'success';
					else echo 'failed';	
				}
			}else if($_POST['status']==5){
				if(!$this->exist_booking($_POST['accom_id'],$_POST['ss'],$_POST['es'])){
					if($this->save_booking_admin($_POST,1))echo 'success';
					else echo 'failed';	
				}else{
					echo 'success';
				}							
			}else if($_POST['status']==1){
				
				//check if is exist on tabel booking
				$data_book = $this->exist_booking($_POST['accom_id'],$_POST['ss'],$_POST['es'],'array');
				if(!empty($data_book)){
					$book_id = $data_book['lbook_id'];
					$query_canceled_book = $db->prepare_query('update lumonata_accommodation_booking set lstatus=0 where lbook_id=%d', $book_id);
					$db->do_query($query_canceled_book);
					$this->sync_book_to_availability($book_id,$status);
					echo 'success';	
				}else{
					echo 'success';	
				}
			}else if($_POST['status']==2){//is no check in
				echo $this->valid_to_booking_tabel($accom_id,$ss,$es,$status);
			}else if($_POST['status']==3){
				echo $this->valid_to_booking_tabel($accom_id,$ss,$es,$status);
			}else if($_POST['status']==4){
				echo $this->valid_to_booking_tabel($accom_id,$ss,$es,$status);
			}else if($_POST['status']==7){
				echo $this->valid_to_booking_tabel($accom_id,$ss,$es,$status);
			}		
		}else{
			echo 'failed';	
		}
	}
	
	function save_booking_admin($dtPost,$status){
		global $db;
		require_once('class.data_price_villa.php');
		$dpv = new data_price_villa();
		$book_id = time();
		
		if(isset($_COOKIE['member_log'])){//if is member
			$username    = $_COOKIE['member_log']['id'];
			$user_type	 = 'member';
		}else{//if is admin
			$username  = $_COOKIE['username'];
			$user_type = $_COOKIE['user_type'];
		}	
		$data_user = $this->data_tabel("lumonata_users","where lusername = '$username'");	
		$dt_price = $dpv->load($dtPost['ss'],$dtPost['es'],$dtPost['days'],$dtPost['accom_id']);
		$price_without_discount = $dt_price['price_without_discount'];
		$discount = ($dt_price['discount_total'] + $dt_price['discount_alltime_total']);
		$early_bird = ($dt_price['early_bird_total'] + $dt_price['early_bird_alltime_total']);
		$surecharge = ($dt_price['surecharge_total'] + $dt_price['surecharge_alltime_total']);
		$total = $price_without_discount - ($discount + $early_bird) + $surecharge;
		
		if($this->save_reservation_detail($book_id,$dtPost['accom_id'],$price_without_discount,$dt_price)){
			if($this->save_reservation($book_id,$dtPost['ss'],$dtPost['es'],$data_user['ldisplay_name'],'',$data_user['lemail'],'',0,0,'',$total,'villa',$data_user['luser_id']))	{
				return true;
			}
		}
	}
	
	
	
	function save_reservation($book_id,$cIn,$cOut,$first_name,$last_name,$email,$phone,$adult,$child,$special_notes,$total,$type_reservation,$user_id){
		global $db;
		$q = $db->prepare_query("insert into lumonata_accommodation_booking 
								(lbook_id,lcheck_in,lcheck_out,lfirst_name,llast_name,
								 lemail,lphone,ladult,lchild,lspecial_note,
								 ltotal,lstatus,lbook_type,lbook_by) values 
								 (%d,%d,%d,%s,%s,
								  %s,%s,%d,%d,%s,
								  %d,%d,%s,%d)",
								  $book_id,$cIn,$cOut,$first_name,$last_name,
								  $email,$phone,$adult,$child,$special_notes,
								  $total,1,$type_reservation,$user_id);
		if($db->do_query($q))return true;
		else return false;						  
	}
	
	function save_reservation_detail($book_id,$acco_id,$price_without_discount,$dt_price){
		$dt_disc = array();
		$dt_disc['discount_total'] 			= $dt_price['discount_total'];
		$dt_disc['discount_txt'] 			= $dt_price['discount_txt'];
		$dt_disc['discount_alltime_total'] 	= $dt_price['discount_alltime_total'];
		$dt_disc['discount_alltime_txt'] 	= $dt_price['discount_alltime_txt'];
		$dt_disc = json_encode($dt_disc);
		
		$dt_early_bird = array();
		$dt_early_bird['early_bird_total'] 			= $dt_price['early_bird_total'];
		$dt_early_bird['early_bird_txt'] 			= $dt_price['early_bird_txt'];
		$dt_early_bird['early_bird_alltime_total'] 	= $dt_price['early_bird_alltime_total'];
		$dt_early_bird['early_bird_alltime_txt'] 	= $dt_price['early_bird_alltime_txt'];
		$dt_early_bird = json_encode($dt_early_bird);
		
		$dt_surecharge = array();		
		$dt_surecharge['surecharge_total'] 			= $dt_price['surecharge_total'];
		$dt_surecharge['surecharge_txt'] 			= $dt_price['surecharge_txt'];
		$dt_surecharge['surecharge_txt_full'] 		= $dt_price['surecharge_txt_full'];
		$dt_surecharge['surecharge_alltime_total'] 	= $dt_price['surecharge_alltime_total'];
		$dt_surecharge['surecharge_alltime_txt'] 	= $dt_price['surecharge_alltime_txt'];
		$dt_surecharge['surecharge_alltime_txt_full']	= $dt_price['surecharge_alltime_txt_full'];
		
		
		$dt_surecharge = json_encode($dt_surecharge);
		
		global $db;
		$q = $db->prepare_query("insert into lumonata_accommodation_booking_detail (lbook_id,larticle_id,ltotal,ldiscount,learly_bird,lsurecharge) 
								values (%d,%d,%d,%s,%s,%s)",$book_id,$acco_id,$price_without_discount,$dt_disc,$dt_early_bird,$dt_surecharge);
		if($db->do_query($q))return true;
		else return false;						
		
	}
	
	
	
	
	function  sync_book_to_availability($book_id,$status){
		global $db;
		$str 	= $db->prepare_query("select a.lcheck_in,a.lcheck_out,b.larticle_id from lumonata_accommodation_booking a
									  inner join lumonata_accommodation_booking_detail b on a.lbook_id=b.lbook_id
									  where a.lbook_id=%d",$book_id);
		$r		= $db->do_query($str);
		while($dt_book= $db->fetch_array($r)){
			$accom_id = $dt_book['larticle_id'];
			$date_start = $dt_book['lcheck_in'];
			$date_finish = $dt_book['lcheck_out'];
			$days = ($date_finish - $date_start) / 86400;
			$this->sync_tabel_availability($accom_id,$date_start,$date_finish,$days,$status);
		}
	}
	
	
	function sync_tabel_availability($accom_id,$date_start,$date_finish,$days,$status){
		global $db;
		//echo $accom_id.'#';
		$username = $_SESSION['upd_by'];
		for($i=0;$i<=$days;$i++){
			$date = $date_start + ($i * 86400);	
			$data_availability 	= $db->prepare_query("select * from lumonata_availability WHERE ldate=%d AND lacco_id=%d",$date,$accom_id);
			$rda = $db->do_query($data_availability);
			$dt = $db->fetch_array($rda);
			if(!empty($dt)){
				$query_update = $db->prepare_query("update lumonata_availability 
															set lstatus=%d where ldate=%d and lacco_id=%d",
															$status,$date,$accom_id);
				$db->do_query($query_update);											
			}//if num on availability 0
		}//end for
	}
	
	
	
	function exist_the_checkin($start_date,$post_enddate){
		global $db;
		
		if($start_date==$post_enddate){
			$accom_id =$_POST['accom_id'];
			$q = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b 
									on a.lbook_id=b.lbook_id where lcheck_in=%d and b.larticle_id=%d",$start_date,$accom_id);					
			
			$r = $db->do_query($q);						
			$data_book = $db->fetch_array($r);
			//$data_book = $this->data_tabel("lumonata_accommodation_booking", "WHERE lcheck_in = $start_date  AND larticle_id = $accom_id  and lstatus <> 0");
			if(!empty($data_book)){
				return true;
			}else{
				return false;
			}	
		}
		
	}
	
	
	function exist_booking($accom_id,$start_date,$end_date,$return=''){
		global $db;		
		$q = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b 
								on a.lbook_id=b.lbook_id where lcheck_in=%d and lcheck_out=%d and b.larticle_id=%d and a.lstatus <> %d",
								$start_date,$end_date,$accom_id,0);										
		$r = $db->do_query($q);						
		$data_book = $db->fetch_array($r);
		if(!empty($data_book)){
			if($return=='array')return $data_book;
			else return true;
		}else{
			if($return=='array')return $data_book;
			else return false;
		}
	}
	
	
	function send_the_emails(){		
		  //get data booking
		  $accom_id =$_POST['accom_id'];
		  $start_date = $_POST['ss'];
		  $end_date = $_POST['es'];
		  $data_book = $this->data_tabel("lumonata_accommodation_booking", "WHERE lcheck_in = $start_date AND lcheck_out = $end_date AND larticle_id = $accom_id ORDER BY lcheck_out DESC LIMIT 1");
		  if(!empty($data_book)){
			  //get email global
			  $emails = get_meta_data("email","global_setting");
			  if(trim($emails)!=''){
				  $email = 'adisend@localhost';
				  $this->sent_bookdata_to_email($emails,$data_book,'reservation',0);
				  //to client
				  if(isset($_POST['email'])){
					  $this->sent_bookdata_to_email($_POST['email'],$data_book,'client','a');									
				  }
			  }			  
			  echo 'success';		
		  }
	}
	
	function sent_bookdata_to_email($email,$data_book,$type,$s='a'){
		ini_set("SMTP", SMTP_SERVER);
		$from  = "info@sukhavati.com";
		ini_set("sendmail_from",$from );
		
		$OUT_TEMPLATE="email-booking.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/calendar/');
		$t->set_file('email_booking', $OUT_TEMPLATE);	
		$t->set_block('email_booking', 'cobaBlock',  'mBlock');	
		
		global $db;
		//get who is ordered
		$query_user = $db->prepare_query('select * from lumonata_users where lusername = %s',$data_book['lusername']);
		$ruq = $db->do_query($query_user);
		$duq = $db->fetch_array($ruq);
		$full_name = $duq['ldisplay_name'];
		$email_from = $duq['lemail'];
				
		
		//get data villa
		$query_villa = $db->prepare_query('select * from lumonata_articles where larticle_id = %d',$data_book['larticle_id']);
		$result_villa = $db->do_query($query_villa);
		$data_villa = $db->fetch_array($result_villa);
		
		//set variable
		$t->set_var('id',$data_book['lbook_id']);
		$t->set_var('date_book',date('d F Y',$data_book['lbook_id']));
		$t->set_var('periode_date',date('d F Y',$data_book['lcheck_in']).'&nbsp;-&nbsp;'.date('d F Y',$data_book['lcheck_out']));
		$t->set_var('name',$data_book['lname']);			
		$t->set_var('email',$data_book['lemail']);
		$t->set_var('address',$data_book['laddress']);
		$t->set_var('phone',$data_book['lphone']);
		$t->set_var('adult',$data_book['ladult']);
		$t->set_var('child',$data_book['lchild']);
		$t->set_var('infant',$data_book['linfant']);
		$t->set_var('note',$data_book['note']);
		$t->set_var('order_by',$full_name);
	
		if($type=='client'){
			$t->set_var('greeting', 'Hi,');		
			$t->set_var('desc', 'You have room reservations at '.$data_villa['larticle_title']);
		}else{
			$t->set_var('greeting', 'Congratulations,');		
			$t->set_var('desc', 'you get a booking at the '.$data_villa['larticle_title']);
		}
		$subject = 'Booking Info -  Sukhavati.com';
		
		$theMailContent =  $t->Parse('mBlock', 'cobaBlock', false);	
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";			
		$headers .= "From: $full_name <".$email_from.">\r\nReply-To :'".$email_from."'\r\n";
		
		$send = mail("$email","$subject","$theMailContent","$headers");
		
	}
	
	
	
	
	function get_data_owner(){
		global $db;
		$accom_id =$_POST['accom_id'];
		$query = $db->prepare_query("SELECT * FROM lumonata_owner_villa a inner join lumonata_members b where a.lmember_id=b.lmember_id and a.larticle_id=%d",$accom_id);
		$result = $db->do_query($query);
		$data = $db->fetch_array($result);
		
		return $data;
	}
	
	
	function data_tabel($tabel,$query){
		global $db;
		$query = $db->prepare_query("select * FROM $tabel $query");
		$result = $db->do_query($query);
		$data=$db->fetch_array($result);
		
		return $data;
	}
	
	function valid_to_booking_tabel($accom_id,$ss,$es,$status){
		global $db;
		//check if is exist on tabel booking
		
		$data_book = $this->exist_booking($accom_id,$ss,$es,'array');
		if(!empty($data_book)){
			$book_id = $data_book['lbook_id'];
			$query_canceled_book = $db->prepare_query('update lumonata_accommodation_booking set lstatus=0 where lbook_id=%d', $book_id);
			$db->do_query($query_canceled_book);
			$this->sync_book_to_availability($book_id,$status);
			echo 'success';	
		}else{
			echo 'success';	
		}
		
		
		/*$accom_id =$_POST['accom_id'];
		$start_date = $_POST['ss'];
		$end_date = $_POST['es'];
		
		$data_book = $this->data_tabel("lumonata_accommodation_booking", "WHERE lcheck_in = $start_date AND lcheck_out = $end_date AND larticle_id = $accom_id ORDER BY lcheck_out DESC LIMIT 1");
		if(!empty($data_book)){
			global $db;
			$qlb =  $db->prepare_query("select * from lumonata_accommodation_booking where lcheck_in = %d  AND lcheck_out = %d AND larticle_id = %d",
					$start_date, $end_date,$accom_id);
			$list_book = $db->do_query($qlb);
			while($true_data_book = $db->fetch_array($list_book)){
				$book_id = $true_data_book['lbook_id'];
				$query_canceled_book = $db->prepare_query('update lumonata_accommodation_booking  set lstatus=0 where lbook_id=%d', $book_id);
				$db->do_query($query_canceled_book);
				$this->sync_book_to_availability($book_id,$status);
			}
			echo 'success';
		}else{
			echo 'success';
		}*/
		
		
		
	}
	
	
}
?>