<?php 
class data_price_villa extends db {
	var $appName;
	
	function data_price_villa($appName=""){
		$this->appName=$appName;
		$this->template_var="data_price_villa";
	}
	
	function load($cIn,$cOut,$days,$acco_id){	
		$price_without_discount=0;
		$min_price_per_night=0;
		
		$discount_data= array();
		$discount_total = 0;
		$discount_alltime_data= array();
		$discount_alltime_total = 0;
		
		$early_bird_data = array();
		$early_bird_total = 0;
		$early_bird_alltime_data = array();
		$early_bird_alltime_total = 0;
		
		$surecharge_data = array();
		$surecharge_total = 0;
		$surecharge_alltime_data = array();
		$surecharge_alltime_total = 0;
		
		$total = 0;
		
		for($i=0 ;$i<=$days ; $i++){
			$the_date = $cIn + ($i*86400);
			if($the_date >= $cIn && $the_date < $cOut) {				
				$data = $this->data_tabel('lumonata_availability',"where ldate=$the_date and lacco_id=$acco_id",'array');
				if(!empty($data)){	
					$rate_now = $data['lrate'];
					$price_without_discount = $price_without_discount + $rate_now;	
					if($min_price_per_night == 0)$min_price_per_night = $rate_now;
					else if($min_price_per_night >0 && $min_price_per_night> $rate_now) $min_price_per_night = $rate_now;
					
					//if($acco_id==347){
						//get discount with check in and check out
						$disc_data = $this->get_disc_n_earlybird_rate($acco_id,$the_date,$data['lrate'],'disc_promo',$days,$cIn,$cOut);
						$discount_total = (!empty($disc_data) ? $discount_total+$disc_data[$the_date]['price'] : $discount_total);
						if(!empty($disc_data)) array_push($discount_data,$disc_data);
						
						//get discount all time
						$discount_alltime = $this->get_disc_n_earlybird_alltime($acco_id,$the_date,$data['lrate'],'disc_promo',$days,$cIn,$cOut);
						$discount_alltime_total = (!empty($discount_alltime) ? $discount_alltime_total+$discount_alltime[$the_date]['price'] : $discount_alltime_total);
						if(!empty($discount_alltime)) array_push($discount_alltime_data,$discount_alltime);
											
						//get early with check in and check out
						$early_bird = $this->get_disc_n_earlybird_rate($acco_id,$the_date,$data['lrate'],'early_bird',$days,$cIn,$cOut);
						$early_bird_total = (!empty($early_bird) ? $early_bird_total+$early_bird[$the_date]['price'] : $early_bird_total);
						if(!empty($early_bird)) array_push($early_bird_data,$early_bird);
						
						//get early bird all time
						$early_bird_alltime = $this->get_disc_n_earlybird_alltime($acco_id,$the_date,$data['lrate'],'early_bird',$days,$cIn,$cOut);
						$early_bird_alltime_total = (!empty($early_bird_alltime) ? $early_bird_alltime_total+$early_bird_alltime[$the_date]['price'] : $early_bird_alltime_total);
						if(!empty($early_bird_alltime)) array_push($early_bird_alltime_data,$early_bird_alltime);
						
						//get surcharge with check in and check out
						$surecharge = $this->get_surecharge($the_date,$data['lrate'],'');
						$surecharge_total = (!empty($surecharge) ? $surecharge_total + $surecharge[$the_date]['price'] : $surecharge_total);
						if(!empty($surecharge)) array_push($surecharge_data,$surecharge);
						
						//get surcharge all time
						$surecharge_alltime = $this->get_surecharge($the_date,$data['lrate'],'all_time');
						$surecharge_alltime_total = (!empty($surecharge_alltime) ? $surecharge_alltime_total + $surecharge_alltime[$the_date]['price'] : $surecharge_alltime_total);
						if(!empty($surecharge_alltime)) array_push($surecharge_alltime_data,$surecharge_alltime);
					//}
					
				}//end if data not empty
			}//end if cIn and cOut
		}//end for	
		
		//set return value
		$return = array();
		$return['price_without_discount'] 	= $price_without_discount;
		$return['min_price_per_night']		= $min_price_per_night;
		
		//$return['discount_data']			= $discount_data;
		$return['discount_total']			= $discount_total;
		$return['discount_txt']				= $this->discount_txt_thumb($discount_data);
		//$return['discount_alltime_data']	= $discount_alltime_data;
		$return['discount_alltime_total']	= $discount_alltime_total;
		$return['discount_alltime_txt']		= $this->discount_txt_thumb($discount_alltime_data);
		
		//$return['early_bird_data']		= $early_bird_data;
		$return['early_bird_total']			= $early_bird_total;
		$return['early_bird_txt']			= $this->discount_txt_thumb($early_bird_data);
		//$return['early_bird_alltime_data']= $early_bird_alltime_data;
		$return['early_bird_alltime_total']	= $early_bird_alltime_total;
		$return['early_bird_alltime_txt']	= $this->discount_txt_thumb($early_bird_alltime_data);
		
		//$return['surecharge_data']		= $surecharge_data;
		$return['surecharge_total']			= $surecharge_total;
		$return['surecharge_txt']			= $this->discount_txt_thumb($surecharge_data);
		$return['surecharge_txt_full']		= $this->get_single_txt_surecharge($surecharge_data)." ".$this->discount_txt_thumb($surecharge_data);
		//$return['surecharge_alltime_data']= $surecharge_alltime_data;
		$return['surecharge_alltime_total']	= $surecharge_alltime_total;
		$return['surecharge_alltime_txt']	= $this->discount_txt_thumb($surecharge_alltime_data);
		$return['surecharge_alltime_txt_full']	= $this->get_single_txt_surecharge($surecharge_alltime_data)." ".$this->discount_txt_thumb($surecharge_alltime_data);
		
		$all_discount						= $discount_total+$discount_alltime_total+$early_bird_total+$early_bird_alltime_total;
		$all_surcharge						= $surecharge_total+$surecharge_alltime_total; 
		
		$return['total']					= $price_without_discount - $all_discount + $all_surcharge;
		
		//if($acco_id==347) print_r($return);
		//print_r($return);
		return $return;	
	}
	
	function get_single_txt_surecharge($data){
		$txt = "";
		foreach($data as $surecharge_eachDate){
			foreach($surecharge_eachDate as $surecharge){
				if($surecharge['val']!=0 || $surecharge['val']!='')$txt = $surecharge['desc'];
			}
		}
		return $txt;	
	}

	
	
	function discount_txt_thumb ($discount_data){
		$discount_result = array();
		//kelompokkan jadi 2
		if(!empty($discount_data)){
			foreach($discount_data as $dd){
				foreach($dd as $discount){
					$type	= $discount['type'];
					$val 	= $discount['val'];
					if(!isset($discount_result['USD']))$discount_result['USD']=0;
					if(!isset($discount_result['%']))$discount_result['%']='';
					$discount_result['USD'] = ($type=='USD'? $discount_result['USD'] + $val : $discount_result['USD']);
					$discount_result['%'] = ($type=='%' && $discount_result['%']=='' ? $val : $discount_result['%']) ;//hanya satu saja yang ditampilkan
				}//each date
			}//each array
		}//end if
		//print_r($discount_result);
		$txt = "";	
		$txt = (!empty($discount_result['%']) ? number_format($discount_result['%'],0).'%' : '');
		$txt = ($txt!='' && !empty($discount_result['USD']) ? $txt.= ' + ':$txt);
		$txt = (!empty($discount_result['USD']) ? '$ '.number_format($discount_result['USD'],0) : $txt);
		return $txt;
		
	}
	
	function get_disc_n_earlybird_rate($acco_id,$date,$rate_now,$type_discount='disc_promo',$days=0,$cIn,$cOut){
		global $db;
		$roomtype_id = $this->get_roomtype_acco($acco_id);
		$return = array();
		$disc_data = array();
		$disc_data = $this->data_tabel('lumonata_accommodation_promo',"where lpromo_type='$type_discount' and lacco_type_id=$roomtype_id and $date>=ldate_from and $date<=ldate_to",'array');
		if(!empty($disc_data)){
			//validate for disc_promo
			if($type_discount=='disc_promo'){
				$days = json_decode($disc_data['lday_of_week']);
				$date_str = strtolower(date('D',$date));
				if(in_array($date_str,$days)){
					$return[$date]['type'] 	= $disc_data['lammount_unit'];
					$return[$date]['price'] = ($disc_data['lammount_unit']=='USD'?$disc_data['lammount']: ($disc_data['lammount']* $rate_now)/100 );
					$return[$date]['val'] 	= $disc_data['lammount'];	
				}			
			}
			if($type_discount=='early_bird'){
				$min_stay		= $disc_data['lstay'];
				$min_stay_to	= $disc_data['lstay_to'];
				if($days>=$min_stay && $days<=$min_stay_to){
					$date_now = time();
					$days_before_arrival	= $disc_data['lday_before'];
					$day_before_checkIn 	= ($cIn - $date_now)/86400;
					if($day_before_checkIn >=$days_before_arrival){
						$return[$date]['type'] 	= $disc_data['lammount_unit'];
						$return[$date]['price'] = ($disc_data['lammount_unit']=='USD'?$disc_data['lammount']: ($disc_data['lammount']* $rate_now)/100 );
						$return[$date]['val'] 	= $disc_data['lammount'];			
					}//end if day before arrival validate				
				}//end if min_stay validate
			}
		}
		return $return;
	}
	
	function get_disc_n_earlybird_alltime($acco_id,$date,$rate_now,$type_discount,$days=0,$cIn,$cOut){
		$roomtype_id = $this->get_roomtype_acco($acco_id);	
		$promo = $this->data_tabel('lumonata_accommodation_promo',"where lpromo_type='$type_discount' and lacco_type_id=$roomtype_id and ldate_from=0 and ldate_to=0",'array');
		$return = array();
		if(!empty($promo)){
			if($type_discount=='disc_promo'){
				$days = json_decode($promo['lday_of_week']);
				$date_str = strtolower(date('D',$date));
				if(in_array($date_str,$days)){
					$return[$date]['type'] 	= $promo['lammount_unit'];
					$return[$date]['price'] = ($promo['lammount_unit']=='USD'?$promo['lammount']: ($promo['lammount']*$rate_now)/100 );
					$return[$date]['val'] 	= $promo['lammount'];	
				}//end if in array
			}//end if disc promo
			
			if($type_discount=='early_bird'){	
				$min_stay		= $promo['lstay'];
				$min_stay_to	= $promo['lstay_to'];
				if($days>=$min_stay && $days<=$min_stay_to){
					$date_now = time();
					$days_before_arrival	= $promo['lday_before'];
					$day_before_checkIn 	= ($cIn - $date_now)/86400;
					if($day_before_checkIn >=$days_before_arrival){
						$return[$date]['type'] 	= $promo['lammount_unit'];
						$return[$date]['price'] = ($promo['lammount_unit']=='USD'?$promo['lammount']: ($promo['lammount']* $rate_now)/100 );
						$return[$date]['val'] 	= $promo['lammount'];			
					}//end if day before arrival validate				
				}//end if min_stay validate
			}
		}
		return $return;
	}
	
	function get_surecharge($date,$rate_now,$type_surecharge=''){
		if($type_surecharge=='') $surecharge = $this->data_tabel('lumonata_accommodation_surecharge',"where $date>=ldate_from and $date<=ldate_to",'array');		
		if($type_surecharge=='all_time') $surecharge =$this->data_tabel('lumonata_accommodation_surecharge',"where ldate_from=0 and ldate_to=0",'array');	
		
		$return = array();
		if(!empty($surecharge)){
			$days = json_decode($surecharge['lday_of_week']);
			$date_str = strtolower(date('D',$date));
			if(in_array($date_str,$days)){
					$return[$date]['type'] 	= $surecharge['lammount_unit'];
					$return[$date]['price'] = ($surecharge['lammount_unit']=='USD'? $surecharge['lcharge']: ($surecharge['lcharge']*$rate_now)/100 );
					$return[$date]['val'] 	= $surecharge['lcharge'];	
					$return[$date]['desc'] 	= $surecharge['ldescription'];
			}//end if in array
		}
		return $return;	
	}
	
	
	function get_roomtype_acco($acco_id){
		global $db;
		$q = $db->prepare_query("select c.lrule_id from lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
								where a.larticle_id=b.lapp_id and b.lrule_id=c.lrule_id and
								c.lrule=%s and c.lgroup=%s and
								a.larticle_type=%s and a.larticle_id=%d",'room_type','villas','villas',$acco_id);
		$r = $db->do_query($q);						
		$d = $db->fetch_array($r);
		if(!empty($d)) return $d['lrule_id'];	
	}

	function data_tabel($tabel,$query){
		global $db;
		$query = $db->prepare_query("select * FROM $tabel $query");
		$result = $db->do_query($query);
		$data=$db->fetch_array($result);
		
		return $data;
	}	
}
?>