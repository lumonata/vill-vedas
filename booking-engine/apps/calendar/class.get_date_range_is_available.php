<?php 
class get_date_range_is_available extends calendar {
	function load(){
		global $db;
		$lss = $_POST['lss'];
		$led = $_POST['led'];
		$lvid = $_POST['lvid'];
		$ldays = ($led-$lss)/86400;
		$not_valid=0;
		$the_date;
		
		for($i=0 ;$i<=$ldays ; $i++){
			$the_date = $lss + ($i*86400);
			if($the_date > $lss && $the_date < $led) {
				$query = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d",$the_date,$lvid);
				$result = $db->do_query($query);
				$data=$db->fetch_array($result);
				if(isset($data) && ($data['lstatus']=='0' || $data['lstatus']=='2' || $data['lstatus']=='3' || $data['lstatus']=='4' || $data['lstatus']=='5'  || $data['lstatus']=='6' || $data['lstatus']=='7' )){
					$not_valid++;
				}
			}
		}//end for
		
		$result=array();
		if($not_valid==0){		
			$result['date']='available';
		}else{
			$result['date']='not-available';	
		}
		
		echo json_encode($result);
	}
	
}

?>