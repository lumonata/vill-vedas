<?php 
class calendar extends db {
		var $appName;
		function calendar($appName=""){
			$this->appName=$appName;
			$this->template_var="calendar";
		}
				
		function load(){			
			if(isset($_POST['do_act'])){//use ajax
				return $this->validate_event_ajax();
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('themes','http://'.SUPPLIERPANEL_APPS_URL.'/calendar');
				$t->set_block('home', 'Bcalendar_availability_block',  'mBlock');			
				
				$t->set_var('roomTypeOpt',$this->temp_all_villa($t));
				
				$t->set_var('script-js',$this->script_calender($this->appName));
				
				return $t->Parse('mBlock', 'Bcalendar_availability_block', false);	
			}
			
		}
		
		function validate_event_ajax(){
			if($_POST['do_act']=='get-availability'){
				return $this->get_availability();
			}else if($_POST['do_act']=='check_available_range'){
				return $this->get_date_range_is_available();
			}else if($_POST['do_act']=='manage_rate_availability'){
				return $this->manage_rate_availability();
			}else if($_POST['do_act']=='get-date-range'){
				return $this->get_date_range();
			}		
			else if($_POST['do_act']=='testing_email_book'){
				return $this->testing_sent_bookdata_to_email();
			}else if($_POST['do_act']=='testing_sync_book'){
				//print_r($_POST);
				return $this->testing_sync_book(1401073188,1);
			}
			
		}
		
		function script_calender(){	
			$OUT_TEMPLATE="script-calendar-admin.html";
			$t = new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			
			$t->set_file('script', $OUT_TEMPLATE);
			$t->set_block('script', 'Bscript',  'mBlock');
			
			//set_template(SUPPLIERPANEL_APP_TEMPLATE_URL."/apps/calendar/script-calendar-admin.html",'script_block');
			
			return $t->Parse('mBlock', 'Bscript', false);
		}
		
		
		function get_availability(){
			require_once("../booking-engine/apps/calendar/class.get_availability.php");
			$this->get_availability = new get_availability('get_availability');
			return $this->get_availability->load();
		}
		
		function get_date_range_is_available(){
			require_once("../booking-engine/apps/calendar/class.get_date_range_is_available.php");
			$this->get_date_range_is_available = new get_date_range_is_available();
			return $this->get_date_range_is_available->load();
		}
		
		function manage_rate_availability(){
			require_once("../booking-engine/apps/calendar/class.manage_rate_availability.php");
			$this->manage_rate_availability = new manage_rate_availability();
			return $this->manage_rate_availability->load();
		}
		
		function testing_sync_book($book_id,$status){
			require_once("../booking-engine/apps/calendar/class.manage_rate_availability.php");
			$this->manage_rate_availability = new manage_rate_availability();
			return $this->manage_rate_availability->sync_book_to_availability($book_id,$status);
		}
		
		function get_date_range(){
			require_once("../booking-engine/apps/calendar/class.get_date_range.php");
			$this->get_date_range = new get_date_range();
			return $this->get_date_range->load();
		}
		
		function testing_sent_bookdata_to_email(){
			require_once("../booking-engine/apps/calendar/class.manage_rate_availability.php");
			$this->manage_rate_availability = new manage_rate_availability();
			return $this->manage_rate_availability->sent_bookdata_to_email();
		}
		
		
		function temp_all_villa($temp,$show_all=false,$show_empty=false){
			global $db;
			//get all villa
			$qa = $db->prepare_query("select * from lumonata_articles where larticle_type=%s",'villas');
			$ra = $db->do_query($qa);
			$all = "";
			
			if($show_empty) $all .= "<option value=\"\">Select Villa</option>";
			if($show_all) 	$all .= "<option value=\"all\">All Villa</option>";
			while($da = $db->fetch_array($ra)){
				$villa_name = $da['larticle_title'];
				$villa_id = $da['larticle_id'];
				
				//get categories
				$rc = $this->get_data_additional_villa($villa_id,'categories');
				$dc = $db->fetch_array($rc);
				$category_villa = $dc['lname'];
				
				//get roomtype
				$rr = $this->get_data_additional_villa($villa_id,'room_type');
				$dr = $db->fetch_array($rr);
				if(!empty($dr)) {
					$roomtype_villa = $dr['lname'];
					$category_villa  = $category_villa.',';
				}else $roomtype_villa = "";	
				
				if($all==""){
					$selected = 'selected="selected"';
					$temp->set_var('villa_id',$villa_id);	
				}
				else $selected = "";
				
				$all .= "<option value=\"$villa_id\" $selected>$villa_name - $roomtype_villa</option>";
				
			}
			return $all;
		}
		
		function get_data_additional_villa($id,$type){
			global $db;
			$qc = $db->prepare_query("SELECT a.*,b.lapp_id
							 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
							 WHERE a.lrule=%s AND 
							 (a.lgroup=%s OR a.lgroup=%s) AND 
							 a.lrule_id=b.lrule_id AND
							 b.lapp_id=c.larticle_id AND
							 c.lshare_to=0 AND
							 b.lapp_id = c.larticle_id AND
							 b.lapp_id = %d
							 GROUP BY a.lrule_id
							 ORDER BY a.lorder",$type,'villas','villas',$id);
			$rc = $db->do_query($qc);
			return $rc;			
		}
		
		
		/*function get_date_range_is_available(){
			global $db;
			$lss = $_POST['lss'];
			$led = $_POST['led'];
			$lvid = $_POST['lvid'];
			$ldays = ($led-$lss)/86400;
			$not_valid=0;
			$the_date;
			
			for($i=0 ;$i<=$ldays ; $i++){
				$the_date = $lss + ($i*86400);
				if($the_date > $lss && $the_date < $led) {
					$query = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d",$the_date,$lvid);
					$result = $db->do_query($query);
					$data=$db->fetch_array($result);
					if(isset($data) && ($data['lstatus']=='0' || $data['lstatus']=='2' || $data['lstatus']=='3' || $data['lstatus']=='4' || $data['lstatus']=='5'  || $data['lstatus']=='6' || $data['lstatus']=='7' )){
						$not_valid++;
					}
				}
			}//end for
			
			$result=array();
			if($not_valid==0){		
				$result['date']='available';
			}else{
				$result['date']='not-available';	
			}
			
			echo json_encode($result);
		}*/
			
		
		
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
		function getTemplateVar(){
		  return $this->template_var;
		}
		
		
		
}

?>