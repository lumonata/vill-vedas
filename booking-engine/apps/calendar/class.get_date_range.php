<?php 
class get_date_range extends calendar {
	function load(){
		if(isset($_POST['pkey']) && $_POST['pkey']=='get_range_book'){
			//print_r($_POST);
			$date_now=$_POST['date'];
			$accom_id=$_POST['accom_id'];
			//echo
			global $db;
			/*$query = $db->prepare_query("SELECT lbook_id, larticle_id, lcheck_in, lcheck_out, lusername 
			FROM lumonata_accommodation_booking WHERE lcheck_in <=%d AND lcheck_out>=%d AND larticle_id=%s ORDER BY lcheck_out DESC LIMIT 1",$date_now,$date_now,$accom_id);*/
			
			$query = $db->prepare_query("select * from lumonata_accommodation_booking where lcheck_in <=%d AND lcheck_out>=%d",$date_now,$date_now);
			$result = $db->do_query($query);
			$num = $db->num_rows($result);
			
			if(!empty($num)){
				$data = $db->fetch_array($result);
				$date_start=$data['lcheck_in'];
				$date_end=$data['lcheck_out'];
				$night= number_format((($date_end - $date_start)/ 86400),0);
				
				$text = date('M j, Y',$date_start).' - '.date('M j, Y',$date_end).' ('.$night.' nights)';
				
				$data_return = array();
				$data_return['data'] = 'success';
				$data_return['book_id'] = $data['lbook_id'];
				$data_return['text']    = $text;
				$data_return['date_start']= $date_start;
				$data_return['date_end']= $date_end;
				$data_return['reason'] ='';
				//get reason start_date 
				$query_rc = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d",$date_start,$accom_id);
				$result_rc = $db->do_query($query_rc);
				$data_rc = $db->fetch_array($result_rc);
				if(!empty($data_rc)){
					$data_return['reason'] = $data_rc['lreason'];
					//echo $reason_clicked;
				}
			}else{
				$data_return = array();
				$data_return['data'] = 'failed';
			}	
			
			echo json_encode($data_return);	
		}else if(isset($_POST['pkey']) && $_POST['pkey']=='get_range_reason'){
			//validate status
			$status='';
			if($_POST['type']=='no-check-in'){
				$status =2;
			}else if($_POST['type']=='no-check-out'){
				$status =3;
			}else if($_POST['type']=='no-check-in-out'){
				$status =4;
			}else if($_POST['type']=='maintenance'){
				$status =7;
			}
			
			$date_clicked 	= $_POST['date_clicked'];
			$date_start 	= $_POST['date_start'];
			$date_end 		= $_POST['date_end'];
			$accom_id		= $_POST['accom_id'];
			//get reason clicked date
			global $db;
			$query_rc = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d and lstatus=%d",$date_clicked,$accom_id,$status);
			$result_rc = $db->do_query($query_rc);
			$data_rc = $db->fetch_array($result_rc);
			$reason_clicked = '';
			if(!empty($data_rc)){
				$reason_clicked = $data_rc['lreason'];
				//echo $reason_clicked;
			}
			
			//get new start date
			$i = $date_start;
			$new_start_date = 0;
			while ($i <= $date_end) {
				$query_sd = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d and lstatus=%d and lreason=%s",$i,$accom_id,$status,$reason_clicked);
				$result_sd = $db->do_query($query_sd);
				$data_sd = $db->fetch_array($result_sd);
				if(!empty($data_sd)){
					$new_start_date = $data_sd['ldate'];
					break;
				}
				$i=$i+86400;			   
			}
			//echo $new_start_date;
			
			//get new end date
			$i = $new_start_date;
			$new_end_date = 0;
			while ($i <= $date_end) {
				$query_ed = $db->prepare_query("select * from lumonata_availability where ldate=%d and lacco_id=%d and lstatus=%d and lreason=%s",$i,$accom_id,$status,$reason_clicked);
				$result_ed = $db->do_query($query_ed);
				$data_ed = $db->fetch_array($result_ed);
				if(!empty($data_ed)){
					$new_end_date = $data_ed['ldate'];
				}else{
					break;
				}
				$i=$i+86400;			   
			}
			
			$data = array();
			$data['data'] = 'success';
			$data['reason'] = $reason_clicked;
			$data['new_start'] = $new_start_date;
			$data['new_end'] = $new_end_date;
			
			echo json_encode($data);
			
		}
		
		
		
	}
}

?>
