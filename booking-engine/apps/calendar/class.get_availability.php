<?php
class get_availability{
	//var $appName;
	/*function calendar($appName){
		$this->appName=$appName;
		$this->template_var="get_availability";
	}
	*/
	function load(){
		$accom_id = $_POST['villa_id'];
		$year=$_POST['year'];
		$month=$_POST['month'];
		$num  = date('t', mktime(0,0,0,$month,1,$year));
		echo $this->get_CalendarVariable($accom_id,$month,$year,$num);	
	}

	function get_CalendarVariable($accom_id,$mon,$year,$num){
		for($i=1;$i<=$num;$i++){//validate each date on this month
			//check if exist
			$theTime = mktime(0,0,0,$mon,$i,$year);
			$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
			global $db;
			$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%s";
			$query = $db->prepare_query($str, $theTime, $accom_id);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			$the_status = $data['lstatus'];
			$n = $db->num_rows($result);
					
			if($n > 0){//if exist
				//validate on tabel booking check if is check in booking
				$nci=0;
				$query_ci = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b 
									on a.lbook_id=b.lbook_id where a.lcheck_in=%d and b.larticle_id=%d and a.lstatus <> %d",$theTime,$accom_id,0);	
				$result_ci = $db->do_query($query_ci);
				$nci = $db->num_rows($result_ci);
				
				//validate on tabel booking check if is check out booking
				$nco=0;
				$query_co = $db->prepare_query("select * from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b 
									on a.lbook_id=b.lbook_id where a.lcheck_out=%d and b.larticle_id=%d and a.lstatus <> %d",$theTime,$accom_id,0);	
				
				$result_co = $db->do_query($query_co);
				$nco = $db->num_rows($result_co);
				
				//get data previous
				$thePrevTime = mktime(0,0,0,$mon,$i-1,$year);
				$data_prev = $this->data_tabel('lumonata_availability','WHERE ldate = '.$thePrevTime.' AND lacco_id='.$accom_id);
				
				if ($nci > 0 && $nco > 0 ){ //if is check out and check in booking
					if($the_status==0){
						if($data_prev['lstatus']==5) $class  = "start-booked end-booked booking end-book-hold book-hold";
						else $class= "start-booked end-booked booking";
						$title= "Booked";
						$link = '#booked_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==5){
						if($data_prev['lstatus']==5) $class  = "start-booked end-booked start-book-hold end-book-hold book-hold";
						else $class= "start-booked end-booked book-hold start-book-hold booking";
						$title= "Booking Hold";
						$link = '#book-hold_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==6){
						if(isset($_COOKIE['member_log']['type']) && $_COOKIE['member_log']['type']=='2'){
							$class= "start-booked end-booked booking";
							$title= "Booked";
							$link = '#booked_'.$accom_id.'_x_'.$theTime;						
						}else{
							$class= "start-booked end-booked book-owner";
							$title= "Owner";
							$link = '#book-owner_'.$accom_id.'_x_'.$theTime;	
						}
					}				
				}else if($nci > 0){ //if is check in booking
					if($the_status==0){
						if(isset($class) && ($class=='passed' || $class=='no-check-in')){//if before is passed
							$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
							if($theTime < $theTimeNow) $class= "start-booked booking passed";
							else $class= "start-booked booking";
							$title= "Booked";
							$link = '#booked_'.$accom_id.'_x_'.$theTime ;
						}else if(isset($class) && $class=='no-check-out'){//if before is no check out
							$class= "no-check-out start-booked booking";
							$title= "No Check Out";
							$link = '#booked_'.$accom_id.'_x_'.$theTime;
						}else{
							$class= "start-booked ".($theTime < $theTimeNow? "passed":"")." available booking";
							$title= ($theTime < $theTimeNow? "Passed":"Available");
							$link = '#booked_'.$accom_id.'_x_'.$theTime;					
						}
					}
					else if($the_status==1){
						//if($i==27) echo "$nci > 0 && $nco > 0  $the_status";
						$class= "start-booked ".($theTime < $theTimeNow? "passed":"")." available booking";
						$title= "Available";
						$link = '#booked_'.$accom_id.'_x_'.$theTime;	
					}
					else if($the_status==5){
						if(isset($class) && ($class=='passed' || $class=='no-check-in')){
							$title= "Booking Hold";		
							$class= "start-booked book-hold";
							if($theTime < $theTimeNow) $class .= " passed juju";
						}else{
							if($theTime < $theTimeNow){
								 $title= "Passed";		
								 $class= "start-booked book-hold available passed";
							}else{
								$title= "Available";
								$class= "start-booked book-hold available";
							}
						}
	
						$link = '#book-hold_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==6){
						if(isset($class) && ($class=='passed' || $class=='no-check-in')){
							if(isset($_COOKIE['member_log']['type']) && $_COOKIE['member_log']['type']=='2'){
								$title= "Booked";		
								$class= "start-booked booking";
							}else{
								$title= "Owner";		
								$class= "start-booked book-owner";	
							}	
							if($theTime < $theTimeNow) $class .= " passed";					
						}else{
							
							$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
							if($theTime < $theTimeNow){
								 $class= "start-booked book-owner passed";	
								 $title= "Owner";
							}else{
								 $class= "start-booked book-owner available";	
								 $title= "Available";
							}
						}
						$link = '#book-owner_'.$accom_id.'_x_'.$theTime;
					}
					
					
					
					
				}else if ($nco > 0){ //if is check out booking
					//find the next is available or not
					$theNextTime = mktime(0,0,0,$mon,$i+1,$year);
					$data_next = $this->data_tabel('lumonata_availability','WHERE ldate = '.$theNextTime.' AND lacco_id='.$accom_id);
					$thePrevTime = mktime(0,0,0,$mon,$i-1,$year);
					$data_prev = $this->data_tabel('lumonata_availability','WHERE ldate = '.$thePrevTime.' AND lacco_id='.$accom_id);
					
					if($the_status==2){//if is no-check-in
						if( !empty($data_next) && ($data_next['lstatus']==1)){
							if($theTime < $theTimeNow){
								$class= "end-booked passed booking";
								$title= "Passed";
							}else{
								if($data_prev['lstatus']==5) $class= "end-booked available book-hold";
								else $class= "end-booked available booking";
								$title= "Available";
							}
						}else if(!empty($data_next) && $data_next['lstatus']==3){
							$class= "end-booked booking";
							$title= "Booked";	
						}else{
							if($data_prev['lstatus']==5)$class= "end-booked available no-check-in book-hold";
							else $class= "end-booked available booking no-check-in";
							$title= "No Check In";	
						}
						
					}else if($data_prev['lstatus'] == 5){
						if($theTime < $theTimeNow){
							$class= "end-booked passed book-hold";
							$title= "Passed";
						}else{
							$class= "end-booked available book-hold";
							$title= "Available";	
						}
						
					}else if(isset($class) && $class == "book-owner"){
						if(isset($_COOKIE['member_log']['type']) && $_COOKIE['member_log']['type']=='2')	$class= "end-booked available booking";
						else $class= "end-booked available book-owner";
						$title= "Available";
					}else{
						$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
							
						if(!empty($data_next) && ($data_next['lstatus']==3 || $data_next['lstatus']==4)){
							$class= "end-booked booking";
							$title= "Booked";	
							
						}else if($data['ldate'] >= $theTimeNow){//new 25112013
							if($data_prev['lstatus']==5) $class  ="end-booked available book-hold";
							else $class= "end-booked available booking";
							$title= "Available";	
						}else if($data['ldate'] < $theTimeNow){//new 25112013
							$class= "end-booked booking passed";
							if(isset($title))$title= $title;	
							else{
								$title= "Passed";
							}//end if
						}
					}
					$link = '#booked_'.$accom_id.'_x_'.$theTime;
				}else{ //if is enother status calender					
				
					if($the_status==1){//if is available
						$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
						if($theTime<$theTimeNow){
							$class= "passed";
							$title= "Passed";
							$link = '#passed_'.$accom_id.'_x_'.$theTime;
						}else{
							$class= "available";
							$title= "Available";
							$link = '#available_'.$accom_id.'_x_'.$theTime;			
						}
						
					}else if($the_status==0){//if is not available
						//validate sementara
						//$theNextTime = mktime(0,0,0,$mon,$i+1,$year);
						$class= "book booking ".$the_status;
						$title= "Booked";
						$link = '#booked_'.$accom_id.'_x_'.$theTime.'_ee';
					}else if($the_status==5){//if is booking hold
						$class= "book-hold $the_status $theTime";
						$title= "Booking Hold";
						$link = '#book-hold_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==6){//if is booking owner
						if(isset($_COOKIE['member_log']['type']) && $_COOKIE['member_log']['type']=='2'){
							$class= "book booking";
							$title= "Booked";
							$link = '#booked_'.$accom_id.'_x_'.$theTime;							
						}else{
							$class= "book-owner";
							$title= "Owner";
							$link = '#book-owner_'.$accom_id.'_x_'.$theTime;
						}
						
					}	
									
					else if($the_status==2){//if is no check in
						if(isset($class) && $class=='available'){//if before is available
							$class= "available no-check-in";
							$title= "No Check In";
							$link = '#noCheckIn_'.$accom_id.'_x_'.$theTime;
						}else{
							$class= "no-check-in";
							$title= "No Check In";
							$link = '#noCheckIn_'.$accom_id.'_x_'.$theTime;
						}
						
					}else if($the_status==3){
						$class= "no-check-out";
						$title= "No Check Out";
						$link = '#noCheckout_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==4){
						$class= "no-check-in-out";
						$title= "No Check In/Out";
						$link = '#noCheckInOut_'.$accom_id.'_x_'.$theTime;
					}else if($the_status==7){
						$class= "maintenance";
						$title= "Maintenance";
						$link = '#maintenance_'.$accom_id.'_x_'.$theTime;
					}
					//end validate status	
				}
				
				$days_txt = date('D',$theTime);
				$theTimes = mktime(0,0,0,($mon),$i,$year);
				$start = date('c',$theTimes);
				
				
				$rate = preg_replace('~\.0+$~','',$data['lrate']);
				if($rate=='' || $rate==0) $rate = DEFAULT_PRICE_VILLA;
				$rate = '$ '.number_format($rate,0);
				
				$jsCalendarVariable[] = array(
											  "className" => $class." $days_txt",
											  "title" => $rate,
											  "start" => $start,
											  "url" => $link,
										 	  "description"=>$i
										  );
			}else{//if not exist
				//validate if is this available date or passed
				$the24 = 24*60*60;
				$theTimeNow = mktime(0,0,0,date('n',time()),date('j',time()),date('Y',time()));
				$theTime = mktime(0,0,0,$mon,$i,$year);
				$date = $theTime;
				if($theTime<$theTimeNow){
					$class= "passed $theTime";
					$title= "Passed";
					$link = '#passed_'.$accom_id.'_x_'.$date;
				}else{
					$class= "available";
					$title= "Available";			
					$link = '#available_'.$accom_id.'_x_'.$date;			
				}
				$days_txt = date('D',$theTime);
				$theTimeC = date('c',$theTime);
				$start = $theTimeC;
				
				if($data['lrate']=='' || $data['lrate']==0) $rate = "$ ".DEFAULT_PRICE_VILLA;
				else $rate = number_format($data['lrate'],0);
				
				
				
				$jsCalendarVariable[] = array(
											  "className" => $class." $days_txt",
											  "title" => $rate,
											  "start" => $start,
											  "url" => $link,
										  	  "description"=>$i
										  );			
										  
			}//end if exist
			
		}
		return json_encode($jsCalendarVariable);
		
	}
		
	function data_tabel($tabel,$query){
		global $db;
		$query = $db->prepare_query("select * FROM $tabel $query");

		$result = $db->do_query($query);
		$data=$db->fetch_array($result);
		
		return $data;
	}
	
		
}

?>