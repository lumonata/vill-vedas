<?php
	class discount_promo extends db{
		function discount_promo($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Discount Promo");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin = new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function is_valid_value($discount_for,$roomtype,$date_start,$date_finish,$day_before=0,$stay,$stay_to,$ammount,$ammount_unit,$t){
			$error = 0;
			$msg ="";
			
			if($discount_for==''){
				$msg .= "<li>Please select discount for</li>";
				$error++;	
			}
			
			if($roomtype==''){
				$msg .= "<li>Please select room type</li>";
				$error++;	
			}
			
			if($date_start==''){
				$msg .= "<li>Please set date start</li>";
				$error++;	
			}
			
			if($date_finish==''){
				$msg .= "<li>Please set date finish</li>";
				$error++;	
			}
			
			if($stay==''){
				$msg .= "<li>Please select stay from</li>";
				$error++;	
			}
			
			if($stay_to==''){
				$msg .= "<li>Please select stay to</li>";
				$error++;	
			}
			
			
			
			if($ammount=''){
				$msg .= "<li>Please set amount</li>";
				$error++;	
			}
			
			if($ammount_unit==''){
				$msg .= "<li>Please choose amount type</li>";
				$error++;	
			}
			
			if($error > 0){
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifFailedValidation();');	
				return false;
			}else return true;
			
			
		}
		
		function is_valid_value_on_update($t){
			$error = 0;
			$msg ="";
			
			for($i=0;$i<count($_POST['lpromo_ID']);$i++){
				$promo_id		= $_POST['lpromo_ID'][$i];
				$discount_for	= $_POST['discount_for'][$i];
				$roomtype 		= $_POST['roomtype'][$i];
				$date_start 	= strtotime($_POST['date_start'][$i]);
				$date_finish 	= strtotime($_POST['date_finish'][$i]);
				$day_before 	= 0;
				$stay  			= $_POST['lstay'][$i];
				$stay_to  		= $_POST['lstay_to'][$i];
				$room  			= $_POST['lroom'][$i];
				$ammount		= $_POST['lammount'][$i];
				$ammount_unit 	= $_POST['lammount_unit'][$i];
				
				if($this->is_valid_value($discount_for,$roomtype,$date_start,$date_finish,$day_before,$stay,$stay_to,$ammount,$ammount_unit,$t)==false)$error++;
				
			}//end for
			
				
			
			if($error > 0){
				$t->set_var('notif_message', "<ul><li>All value is required. Please choose one of the values​in each field</li></ul>");
				$t->set_var('jsAction', 'notifFailedValidation();');	
				return false;
			}else return true;
		}
		
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			//echo $_SESSION['product_id'];
			
			if ((isset($_POST['prc']) And $_POST['prc'] == "new") || (isset($_POST['prc']) And $_POST['prc'] == "save_new")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; border-bottom:solid 1px #ccc;background:#f5f5f5;\"></div>");
				if($_POST['prc'] == "save_new"){
					$promo_id		= $this->globalAdmin->setCode("lumonata_accommodation_promo","lpromo_ID");
					$discount_for	= $_POST['discount_for'][0];
					$roomtype 		= $_POST['roomtype'][0];
					$date_start 	= strtotime($_POST['date_start'][0]);
					$date_finish 	= strtotime($_POST['date_finish'][0]);
					$all_time 		= $_POST['all_time'][0]; 
					$day_before 	= 0;
					$stay  			= $_POST['lstay'][0];
					$stay_to  		= $_POST['lstay_to'][0];
					$dow 			= $_POST['dow'][0]; 
					$ammount		= $_POST['lammount'][0];
					$ammount_unit 	= $_POST['lammount_unit'][0];
					
					if($this->is_valid_value($discount_for,$roomtype,$date_start,$date_finish,$day_before,$stay,$stay_to,$ammount,$ammount_unit,$t)){
						if($all_time=='all_time') $date_start = 0; $date_finish=0;
						$str = $db->prepare_query( "INSERT INTO lumonata_accommodation_promo (
															lpromo_ID,lpromo_for,lpromo_type,lacco_type_id,ldate_from,
															ldate_to,lday_of_week,lday_before,lstay,lstay_to,
															lammount,lammount_unit,ldlu) 
															VALUES 
															(%d, %s, %s	,%d, %d, 
															 %d, %s, %d, %d, %d,
															 %s, %s, %d)",
															$promo_id,$discount_for,'disc_promo',$roomtype,$date_start,
															$date_finish,$dow,$day_before,$stay,$stay_to,
															$ammount,$ammount_unit,time());
						
						$result = $db->do_query($str);
						if ($result){	
							$t->set_var('notif_message', 'Data Saved');	
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}else{
							$t->set_var('notif_message', 'Failed to saved');
							$t->set_var('jsAction', 'notifFailedSaved();');		
						}
					}
					
										
				}
				
				$t->set_var('i', 0);
				
				
				$all = '';
				for($iii=1 ; $iii<=10 ; $iii++ ){
					if ($iii==1){
						$all .= '<option value="'.$iii.'" selected="selected">'.$iii.'</option>';
					}else{
						$all .= '<option value="'.$iii.'">'.$iii.'</option>';	
					}
					
				}
				$t->set_var('lstay',  $all); 
				$t->set_var('lstay_to',  $all); 
				$t->set_var('lroom',  $all); 
				
				$array = array('USD','%');
				$all = '';
				foreach($array as $value){
					$all .= '<option value="'.$value.'">'.$value.'</option>';
				}
				$t->set_var('lammount_unit',  $all); 
				$t->set_var('room_category',$this->get_room_type());
				//$t->set_var('room_category',$this->get_category_type_data('get_list'));
				
				
				
				$t->set_var('date_start',  "mm/dd/yy"); 	 	
				$t->set_var('date_finish', "mm/dd/yy"); 
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if ((isset($_POST['prc']) And $_POST['prc'] == "edit") || (isset($_POST['prc']) And $_POST['prc'] == "save_edit")){
//EDIT PART ===============================================================================================================================		
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					if($this->is_valid_value_on_update($t)){
						$msg = '';
						$error = '';
						
						for($i=0;$i<count($_POST['lpromo_ID']);$i++){
							$promo_id		= $_POST['lpromo_ID'][$i];
							$discount_for	= $_POST['discount_for'][$i];
							$roomtype 		= $_POST['roomtype'][$i];
							$all_time 		= $_POST['all_time'][$i];
							if(isset($_POST['all_time'][$i]) && $all_time == 'all_time'){
								$date_start 	= 0; 
								$date_finish 	= 0; 	
							}else{
								$date_start 	= strtotime($_POST['date_start'][$i]); 
								$date_finish 	= strtotime($_POST['date_finish'][$i]); 	
							}
							
							$dow 			= $_POST['dow'][$i]; 
							$day_before 	= 0;
							$stay  			= $_POST['lstay'][$i];
							$stay_to  		= $_POST['lstay_to'][$i];
							$room  			= $_POST['lroom'][$i];
							$ammount		= $_POST['lammount'][$i];
							$ammount_unit 	= $_POST['lammount_unit'][$i];
							
							$stmt = $db->prepare_query( "UPDATE lumonata_accommodation_promo SET
														lpromo_for=%s,lacco_type_id=%d,ldate_from=%d,ldate_to=%d,lday_of_week=%s,
														lday_before=%d,lstay=%d,lstay_to=%d,lammount=%s,lammount_unit=%s,ldlu=%d
														WHERE lpromo_ID=%d",
														$discount_for,$roomtype,$date_start,$date_finish,$dow,
														$day_before,$stay,$stay_to,$ammount,$ammount_unit,time(),$promo_id);
							
							$result = $db->do_query($stmt);
							if($result==false) $error++;
							
						}//end for
						
						if($error > 0){
							$t->set_var('notif_message', "<ul><li>Update was unsuccessfully processed.</li></ul>");
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}else{
							$t->set_var('notif_message', "<ul><li>Update was successfully processed.</li></ul>");
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}
					}
				}
				//end edit process
				//echo 'dsds';
				//
				//start display data
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						
						$query =  $db->prepare_query("SELECT *
														 FROM lumonata_accommodation_promo
														 WHERE lpromo_ID=%d AND lpromo_type=%s",$_POST['pilih'][$i],'disc_promo');
						
						$result = $db->do_query($query);
						$data   = $db->fetch_array($result);
						
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						
						if($data['lpromo_for']=='villa') {
							$t->set_var('villa_only','selected');
							$t->set_var('program_only','');
						}else if($data['lpromo_for']=='program'){
							$t->set_var('program_only','selected');
							$t->set_var('villa_only','');
						}
						
						$t->set_var('dow', $data['lday_of_week']); 
						$t->set_var('room_category',$this->get_room_type($data['lacco_type_id'],'get_list'));	
						
						$all = '';
						for($iii=1 ; $iii<=10 ; $iii++ ){
							if($iii==$data['lstay']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all .= '<option '.$sel.' value="'.$iii.'">'.$iii.'</option>';
						}
						$t->set_var('lstay',  $all); 
						$all = '';
						for($iii=1 ; $iii<=10 ; $iii++ ){
							if($iii==$data['lstay_to']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all .= '<option '.$sel.' value="'.$iii.'">'.$iii.'</option>';
						}
						$t->set_var('lstay_to',  $all); 
						$all = '';
						for($iii=1 ; $iii<=10 ; $iii++ ){
							if($iii==$data['lroom']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all .= '<option '.$sel.' value="'.$iii.'">'.$iii.'</option>';
						}
						$t->set_var('lroom',  $all); 
						
						$array = array('USD','%');
						$all = '';
						foreach($array as $value){
							if($value==$data['lammount_unit']){ $sel = ' selected="selected" '; }else{ $sel=''; }
							$all .= '<option '.$sel.' value="'.$value.'">'.$value.'</option>';
						}
						$t->set_var('lammount_unit',  $all); 
						
						/*$t->set_var('date_start', date("m/d/Y",$data['ldate_from'])); 	 	
						$t->set_var('date_finish', date("m/d/Y",$data['ldate_to'])); 		*/
						if($data['ldate_from']==0 && $data['ldate_to']==0){
							$t->set_var('all_time', 'checked'); 
							$t->set_var('date_start',''); 	 	
							$t->set_var('date_finish','');	 		
						}else{
							$t->set_var('date_start', 	date("m/d/Y",$data['ldate_from'])); 	 	
							$t->set_var('date_finish', 	date("m/d/Y",$data['ldate_to']));
							$t->set_var('all_time', ''); 
						}
						$t->set_var('lday_before', $data['lday_before']); 				
						$t->set_var('lammount', $data['lammount']); 
						$t->set_var('lpromo_ID', $_POST['pilih'][$i]); 
						
						
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-bottom:solid 1px #ccc; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
//EDIT PART END ===============================================================================================================================					
					
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if ((isset($_POST['prc']) And $_POST['prc']=="delete")){
					if (sizeof($_POST['pilih']) != 0){
						$alltitle = '';
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = $db->prepare_query("SELECT * FROM lumonata_accommodation_promo
															WHERE lpromo_ID=%d AND lpromo_type=%s",$_POST['pilih'][$i],'disc_promo');								
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;
							
							if($data['lpromo_for']=='villa') {
								$promo_for = 'Villa only';
							}else{
								$promo_for = 'Program';
							}
							
							$name = $this->get_room_type($data['lacco_type_id'],'get_name');
							
							if ($n1 != 0){
								$title = $title."<li>".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."</li>";
							}
							$alltitle = $alltitle."<li>$promo_for, ". $name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, season below using in accommodation season data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if ((isset($_POST['action']) And $_POST['action']=="Yes")){
					$msg = '';
					$error = '';
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						$query = $db->prepare_query("SELECT *
														FROM lumonata_accommodation_promo
														WHERE lpromo_ID=%d AND lpromo_type='disc_promo'",$_POST['delId'][$i]);
						$result = $db->do_query($query);
						$data   = $db->fetch_array($result);
						
						if($data['lpromo_for']=='villa') {
							$promo_for = 'Villa only';
						}else{
							$promo_for = 'Program';
						}
						
						$stmt   = $db->prepare_query("DELETE FROM lumonata_accommodation_promo WHERE lpromo_ID=%d",$data['lpromo_ID']);
						$result = $db->do_query($stmt);
						
						$name = $this->get_room_type($data['lacco_type_id'],'get_name');
						
						if($result){
							$msg .= "<li>Delete \"$promo_for, ".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"$promo_for, ".$name." : ".date("d M Y",$data['ldate_from'])." - ".date("d M Y",$data['ldate_to'])."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}else{
						//$this->syncTableAvailabilityWhenDeletAccur();
						$t->set_var('jsAction', 'notifBlockSaved();');	
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 
						
					}
					
					
				}// END Delete Process 
				
				$query =  $db->prepare_query("select * from lumonata_accommodation_promo
											  WHERE lpromo_type=%s										  
											  order by ldate_from",
											  'disc_promo');	
											  								 
												 
				//BEGIN set var on viewContent Block
				$result = $db->do_query($query);
				$no = 0;
				while($data = $db->fetch_array($result)){
					$t->set_var('no', $no);	 	
					$name = $this->get_room_type($data['lacco_type_id'],'get_name');		
					$t->set_var('name', $name);
					if($data['lpromo_for']=='program') $df = 'Program';
					else if($data['lpromo_for']=='villa') $df = 'Villa only';
					$t->set_var('discount_for', $df);
					
					$t->set_var('check', 		$data['lpromo_ID']);
					/*$t->set_var('date_start', 	date("d M Y",$data['ldate_from'])); 	 	
					$t->set_var('date_finish', 	date("d M Y",$data['ldate_to'])); */
					if($data['ldate_from']==0 && $data['ldate_to']==0){
						$t->set_var('date_start', 'All Time'); 	 	
						$t->set_var('date_finish', 'All Time'); 	
					}else{
						$t->set_var('date_start', date("d M Y",$data['ldate_from'])); 	 	
						$t->set_var('date_finish', date("d M Y",$data['ldate_to'])); 	
					}
					
					$days = json_decode($data['lday_of_week']);
					$iii=0;
					foreach($days as $day){
						if($iii==0) $all = $day;
						else $all .= ', '.$day;
						$iii++;
					}
					$t->set_var('day_of_week', $all);
					
					if ($data['lammount_unit'] == "USD" && $data['lammount'] !=0 ){	
						$t->set_var('ammount',  $data['lammount_unit']." ".$data['lammount']); 
					}else{
						$t->set_var('ammount',  $data['lammount']." ".$data['lammount_unit']); 
					}
						
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}
		
		function get_category_type_data($action,$id=0){
			require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
			$this->list_category_type_villa = new list_category_type_villa();
			if($action=='get_list') return $this->list_category_type_villa->load($id);	
			else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
		}
		
		function get_room_type($id,$action='get_list'){
			require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
			$this->general_category_villa = new general_category_villa();
					
			if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
			else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
		}
		
		/*function get_list_villa(){
			require_once("../booking-engine/apps/list_villa/class.list_villa.php");
			$this->list_villa = new list_villa();
			return $this->list_villa->just_option();	
		}*/
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>