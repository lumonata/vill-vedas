<?php
	class season extends db{
		function season($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Season Period");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			//testing ajax
			if(isset($_POST['do_act']) && $_POST['do_act']=='test_is_exist'){
				return $this->is_exist_date($_POST['sd'],$_POST['ed']);
				exit;
			}elseif(isset($_POST['do_act']) && $_POST['do_act']=='test_sincronice_availability'){
				return $this->sync_TableAvailability($_POST['name'],strtotime($_POST['date_start']),strtotime($_POST['date_finish']),0);
				exit;
			}
			
			
			if (isset($_POST['prc']) And ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				if($_POST['prc'] == "save_new"){
					if($this->is_exist_date(strtotime($_POST['date_start'][0]),strtotime($_POST['date_finish'][0]))==false ){
						$stmt = $db->prepare_query("INSERT INTO lumonata_accommodation_season (
							lname,ldate_start,ldate_finish,lmin_stay,
							lorder_id,lcreated_by,lcreated_date) 
							VALUES (%s,%d,%d,%d,
								%d,%s,%d)",
								$_POST['name'][0],strtotime($_POST['date_start'][0]),strtotime($_POST['date_finish'][0]),$_POST['min_stay'][0],
								1,$_SESSION['upd_by'],time());
							
						$this->globalAdmin->setOrderID("lumonata_accommodation_season",1);		
						$result = $db->do_query($stmt);
						if($result){
							$this->sync_TableAvailability($_POST['name'][0],strtotime($_POST['date_start'][0]),strtotime($_POST['date_finish'][0]),0); 
							$t->set_var('notif_message', 'Data Saved');	
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}else{
							$t->set_var('notif_message', 'Failed to saved');
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}
					}else{
						$t->set_var('notif_message', 'This season is exist');
						$t->set_var('jsAction', 'notifFailedSaved();');	
						
					}
					
				}
				$t->set_var('i', 0);
				$nameoption = "<select name=\"name[0]\" id=\"name0\" class=\"required\"   >
					 <option value=\"\">Select Season</option>
					 <option value=\"Low Season\">Low Season</option>
					 <option value=\"High Season\">High Season</option>
					 <option value=\"Peak Season\">Peak Season</option>
					 </select>";
				$t->set_var('season_name', $nameoption);
				$t->set_var('date_start', "mm/dd/yy"); 	 	
				$t->set_var('date_finish', "mm/dd/yy"); 
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if (isset($_POST['prc']) And ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				$t->set_var('bt-abu-none','bt-abu-none');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					$error = '';
					$msg = '';
					for($i=0;$i<count($_POST['season_id']);$i++){
							$stmt = $db->prepare_query("UPDATE lumonata_accommodation_season SET
								lname=%s,
								ldate_start=%d,
								ldate_finish=%d,
								lmin_stay=%d,
								lusername=%s,
								ldlu=%d
								WHERE lseason_id =%d",
								$_POST['name'][$i],
								strtotime($_POST['date_start'][$i]),
								strtotime($_POST['date_finish'][$i]),
								$_POST['min_stay'][$i],
								$_SESSION['upd_by'],
								time(),
								$_POST['season_id'][$i]);
							$result = $db->do_query($stmt);
							if($result){
								$msg .= "<li>Update \"".$_POST['name'][$i]."\" was successfully processed.</li>";
								$this->sync_TableAvailability($_POST['name'][$i],strtotime($_POST['date_start'][$i]),strtotime($_POST['date_finish'][$i]),0); 
							}else{
								$error = 1;
								$msg .= "<li>Update \"".$_POST['name'][$i]."\" was unsuccessfully processed.</li>";
							}
					}// end for
						if($error==1){
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}else{
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifBlockSaved();');
							
							if($bySupplier){
								$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
							} 
						}
				}
				//end edit process
				
				//start display data
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$query =  $db->prepare_query("SELECT *
							FROM lumonata_accommodation_season
							WHERE lseason_id =%d",$_POST['pilih'][$i]);
						$result = $db->do_query($query);
						$data = $db->fetch_array($result);
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						$t->set_var('season_id', $data['lseason_id']);
						$t->set_var('min_stay', $data['lmin_stay']);	
						$t->set_var('date_start', date("m/d/Y",$data['ldate_start'])); 	 	
						$t->set_var('date_finish', date("m/d/Y",$data['ldate_finish'])); 
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$align_arr = array("Low Season","High Season","Peak Season"); //
						$nameoption = "<select name=\"name[$i]\" id=\"name$i\" class=\"jsrequired\" >
						<option value=\"\">Select Season</option>";
						for($j=0;$j<count($align_arr);$j++){
							if ($data['lname'] == $align_arr[$j]){
								$nameoption .= "<option value=\"$align_arr[$j]\" selected>$align_arr[$j]</option>";
							}else{
								$nameoption .= "<option value=\"$align_arr[$j]\">$align_arr[$j]</option>";
							}
						}
						$nameoption .= "</select>";	 	
						$t->set_var('season_name', $nameoption);	
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if (isset($_POST['prc']) And $_POST['prc']=="delete"){
					if (isset($_POST['pilih']) and sizeof($_POST['pilih']) != 0){
						$alltitle = '';
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = $db->prepare_query("SELECT *
							FROM lumonata_accommodation_season 
							WHERE lseason_id =%d",$_POST['pilih'][$i]);
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;//$this->globalAdmin->getNumRows("lumonata_accommodation_season","lacco_type_id",$data['lacco_type_id']);
							
							if ($n1 != 0)
							{
								$title = $title."<li>".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."</li>";
							}
							$alltitle = $alltitle."<li>".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, season below using in accommodation season data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if (isset($_POST['action']) And $_POST['action']=="Yes"){
					$msg = '';
					$error = '';
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						$query = $db->prepare_query("SELECT *
						FROM lumonata_accommodation_season
						WHERE lseason_id =%d",$_POST['delId'][$i]);
						$result = $db->do_query($query);
						$data = $db->fetch_array($result);
						$stmt = $db->prepare_query("DELETE FROM lumonata_accommodation_season WHERE lseason_id =%d",$_POST['delId'][$i]);
						$result = $db->do_query($stmt);
						if($result){
							$msg .= "<li>Delete \"".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
					}else{						
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"season : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 	
					}
				}// END Delete Process 
				
				$query =  $db->prepare_query("SELECT * FROM lumonata_accommodation_season ORDER BY ldate_start, ldate_finish, lname");
				//BEGIN set var on viewContent Block
				$result = $db->do_query($query);
				while($data = $db->fetch_array($result)){
					//$t->set_var('no', $no);	 	
					$t->set_var('check', $data['lseason_id']);	 		
					$t->set_var('name', $data['lname']);	 		 	
					$t->set_var('min_stay', $data['lmin_stay']);		
					$t->set_var('date_start', date("d M Y",$data['ldate_start'])); 	 	
					$t->set_var('date_finish', date("d M Y",$data['ldate_finish'])); 
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		function sync_TableAvailability($season_name,$date_start,$date_finish,$cut_of_date){
			global $db;
			$date_now = $this->date_now();
			$days = $this->get_days($date_start,$date_finish);
			
			//syncronize only villa have relationship with category and room type
			//$data_rel = $this->data_tables('lumonata_relationship_villa_category_room_type');
			$data_rel = $this->data_tables("lumonata_rule_relationship as a, lumonata_rules as b 
											where a.lrule_id=b.lrule_id and b.lrule='room_type' and b.lgroup='villas'");
			
			
			while($villa_relationship = $db->fetch_array($data_rel)){
				$accom_id = $villa_relationship['lapp_id'];
				$cat_typeroom_id = $villa_relationship['lrule_id'];				
				$data_accom_rate = $this->data_tables('lumonata_accommodation_rate_categories',"where lacco_type_cat_id='".$cat_typeroom_id."'");
				$num_rates = $db->num_rows($data_accom_rate);
				if($num_rates > 0){
					$data_accom_rate = $this->data_tables('lumonata_accommodation_rate_categories',"where lacco_type_cat_id='".$cat_typeroom_id."'");
					$data_rate = $db->fetch_array($data_accom_rate);
					$rate = $this->get_accom_rate($data_rate,$season_name);
					$rate_additional = $this->get_accom_rate_additional($data_rate,$season_name);
					$this->insert_to_availability_table($accom_id,$date_now,$season_name,$date_start,$date_finish,$days,$rate,$rate_additional);
				}else if($num_rates==0){
					$this->insert_to_availability_table($accom_id,$date_now,$season_name,$date_start,$date_finish,$days);
				}//end if num rates 0
			}//end while villa realtionship
			
		}
		
		function insert_to_availability_table($accom_id,$date_now,$season_name,$date_start,$date_finish,$days,$rate='',$rate_additional=0){
			global $db;
			$username = $_SESSION['upd_by'];
			for($i=0;$i<=$days;$i++){
				$date = $date_start + ($i * 86400);	
				if($date >=$date_now){
					$data_availability = $this->data_tables("lumonata_availability","WHERE ldate=$date AND lacco_id=$accom_id");
					$n = $db->num_rows($data_availability);
					if($n>0){
						$data_availability_array = $db->fetch_array($data_availability);
						$status = $data_availability_array['lstatus'];
						if($status==0 || $status==5 ){
							 //do nothing
						}else{
							if($rate=='' or $rate==0){//just update season
								$query_update = $db->prepare_query("update lumonata_availability 
																	set lseason=%s,ledit=%s,ldlu=%d where ldate=%d and lacco_id=%d",
																	$season_name,$_SESSION['upd_by'],time(),$date,$accom_id);
							}else{
								$query_update = $db->prepare_query("update lumonata_availability 
																	set lrate=%s,lrate_additional=%s,lseason=%s,ledit=%s,ldlu=%d where ldate=%d and lacco_id=%d",
																	$rate,$rate_additional,$season_name,$username,time(),$date,$accom_id);
							}
							
							$db->do_query($query_update);
						}
					}else{
						$query_insert = $db->prepare_query("insert into lumonata_availability
															(ldate,lacco_id,lrate,lrate_additional,lseason,lstatus,
															 lreason,ledit,lcreated_by,lcreated_date,
															 lusername,ldlu) 
															 values(%d,%d,%s,%s,%s,
															 		%d,%s,%s,%s,%d,
																	%s,%d)",
															$date,$accom_id,$rate,$rate_additional,$season_name,
															1,'',$username,$username,time(),
															$username,time());		
						
						$db->do_query($query_insert);
					}//if num on availability 0
				}//end if is not passed
			}//end for
		}
		
		
		
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		function date_now(){
			$date_now  = date('m/d/Y');
			return strtotime($date_now);
		}
		
		function get_days($date_start,$date_finish){
			$days = ($date_finish-$date_start)/86400;
			return $days;
		}
		
		function get_accom_rate($data_rate, $season_name){
			if ($season_name=='Low Season') return $data_rate['llow_season'];
			else if($season_name=='High Season') return $data_rate['lhigh_season'];
			else if($season_name=='Peak Season') return $data_rate['lpeak_season'];
		}
		
		function get_accom_rate_additional($data_rate, $season_name){
			if ($season_name=='Low Season') return $data_rate['llow_additional'];
			else if($season_name=='High Season') return $data_rate['lhigh_additional'];
			else if($season_name=='Peak Season') return $data_rate['lpeak_additional'];
		}
		
		function data_tables($tabel,$where=''){
			global $db;
			$str = $db->prepare_query("select * from $tabel $where");
			$result = $db->do_query($str);
			return $result;
		}
			
		function is_exist_date($start_date,$end_date){
			global $db;
			$str = $db->prepare_query("select * from lumonata_accommodation_season where 
											(%d >= ldate_start and %d <= ldate_finish) or
											(%d < ldate_start and %d > ldate_start and %d <= ldate_finish) or
											(%d > ldate_start and %d <= ldate_finish and %d > ldate_finish )
											",
											$start_date,$end_date,
											$start_date,$end_date,$end_date,
											$start_date,$start_date,$end_date
											);								
			
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			if(!empty($data)) return true;
			else return false;
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
		
		function meta_desc(){
			
		}
		
		function meta_key(){
			
		}
		
		
	};
?>