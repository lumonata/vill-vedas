<?php
	class gallery_edit extends db{
		function gallery_edit($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Gallery Edit");
			
			require_once("../lumonata-admin/functions/upload.php");
			$this->upload = new upload();
			$this->upload->upload_constructor(IMAGE_PATH."/Accommodations/");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			/*print_r($_GET); //, [bool return])
			echo "<br /><br />";
			print_r($_POST); //, [bool return])
			echo "<br /><br />";
			print_r($_SESSION);*/
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			if(!empty($_POST)){
				
				$stmt = parent::prepare_query("UPDATE lumonata_accommodation_images SET
								ltitle=%s,
								lusername=%s,
								ldlu=%d
								WHERE limage_id =%s",
								$_POST['title'],
								$_SESSION['upd_by'],
								time(),
								$_POST['image_id']);
				$result = parent::query($stmt);
				if($result){
					$image_name = $_FILES['image']['name'];
					$image_size = $_FILES['image']['size'];
					$image_type = $_FILES['image']['type'];
					$image_tmp = $_FILES['image']['tmp_name'];
					$sef_img = $this->upload->sef_url($_POST['image_id']);
					$image = $this->upload->rename_file($image_name,$sef_img);
					if (!empty($_FILES['image']['name'])){ //not empty image
						$stmtImages = parent::prepare_query("UPDATE lumonata_accommodation_images SET limage=%s WHERE limage_id =%s",$image,$_POST['image_id']);
						$resultImages = parent::query($stmtImages);
						if ($image_name != $_POST['image_old']){
							$this->upload->delete_file($_POST['image_old'],0);
							$this->upload->delete_file($_POST['image_old'],1);
							$this->upload->delete_file($_POST['image_old'],2);
						}
						$dimensions = $this->globalAdmin->getImageDimensions(212); 
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[0],$dimensions[1],0);
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[2],$dimensions[3],1);
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[4],$dimensions[5],2);
					}
					$t->set_var('jsAction', 'notifBlockSaved();');	
					
					if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"gallery : edit",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					}
				}else{
					$t->set_var('jsAction', 'notifFailedSaved();');	
					//$t->set_var('error', "<div class=\"error\"><b>Failed!</b><br />Add new \"".$_POST['name'][0]."\" was unsuccessfully processed.</div>");	
				}
					
				
			}

			$query =  parent::prepare_query("SELECT *
				FROM lumonata_accommodation_images
				WHERE limage_id =%s",$_GET['act2']);
			$result = parent::query($query);
			$data = parent::fetch_array($result);
			$t->set_var('pilih',$_POST['pilih'][$i]);
			$t->set_var('i', $i);	
			$t->set_var('image_id', $data['limage_id']);
			$t->set_var('title', $data['ltitle']);
			$img = '{http}{rootsite_url}/images/Accommodations/'.$data['limage'];
			$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=152&h=103&src='.$img;
			$imgs = '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
			$existingImage = "<div class=\"wrapper form\" style=\"border-bottom:solid 1px #ccc;\">
						<label>Existing Image</label>
						<label style=\"width:160px; margin-right:10px;\">".$imgs."
						<input name=\"image_old\" id=\"image_old\" type=\"hidden\" value=\"".$data['limage']."\" />
						</label>
					</div>";
			$t->set_var('existingImage', $existingImage);
			return $t->Parse('mBlock', 'mainBlock', false);
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>