<?php
	class season_delete extends db{
		function season_delete($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Season Delete");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			
			if (isset($_GET['act2'])){
				$season_id = $_GET['act2'];
			}else{
				$season_id = $_POST['season_id'];
			}
			
			$query = $db->prepare_query("SELECT *
			FROM lumonata_accommodation_season
			WHERE lseason_id =%d",$season_id);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$msg_succsess = "Delete \"".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."\" was successfully processed.";
			$t->set_var('msg_succsess',$msg_succsess);
			$msg_error = "Delete \"".$data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish'])."\" was unsuccessfully processed.";
			$t->set_var('msg_error',$msg_error);
			
			if(!empty($_POST)){
				$stmt = $db->prepare_query("DELETE FROM lumonata_accommodation_season WHERE lseason_id =%d",$_POST['season_id']);
				$result = $db->do_query($stmt);
				//$result = 2;
				if($result){
					if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"seson : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					} 
					
					$this->syncTableAvailabilityWhenDeletAccur();
					
					$url = "http://".SUPPLIERPANEL_SITE_URL."/season/";
					$t->set_var('jsAction', "notifBlockSaved(); 
					var val = 'content-typeEdit';
					var url = '$url';
					displayNoneId(val);
					WindowLocation(url);
					");
					//header("Location:http://".SUPPLIERPANEL_SITE_URL."/season/");
				}else{
					$url = "http://".SUPPLIERPANEL_SITE_URL."/season/";
					$t->set_var('jsAction', "notifFailedSaved();
					var val = 'content-typeEdit';
					var url = '$url';
					displayNoneId(val);
					WindowLocation(url);
					");	
				}
			}else{
				//if (isset($_POST['pilih'][$i])){$pilih=$_POST['pilih'][$i];}else{$pilih=''}
				//$t->set_var('pilih',$pilih);
				//$t->set_var('i', $i);	
				$t->set_var('season_id', $data['lseason_id']);
				$t->set_var('name', $data['lname']." : ".date("d M Y",$data['ldate_start'])." - ".date("d M Y",$data['ldate_finish']));
				$save = 'save';
				$t->set_var('jsActionButton',"
				var val = 'save';
				displayNoneClass(val);
				");
			}

			

			return $t->Parse('mBlock', 'mainBlock', false);
		}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		function syncTableAvailabilityWhenDeletAccur(){
			global $db;
			$nnn = '';
			$sql  = $db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res  = $db->do_query($sql);
			$user = $db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			$tmp = "SELECT * FROM lumonata_availability WHERE lacco_id=%d  ORDER BY ldate ASC";
			$sql = $db->prepare_query($tmp, $_SESSION['product_id']);
			$re2 = $db->do_query($sql);
			
			$k = 1;
			$l = 1;
			
			while($avail = $db->fetch_array($re2)){
				$theTime = $avail['ldate'];
				
				$tmp = "SELECT * FROM lumonata_accommodation_season WHERE ldate_start<=%d AND ldate_finish>=%d AND lacco_id=%d";
				$qry = $db->prepare_query($tmp, $theTime, $theTime, $_SESSION['product_id']);
				$res = $db->do_query($qry);
				$num = $db->num_rows($res);
				
				if($num<=0){
					$str = "DELETE FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$sql = $db->prepare_query($str, $avail['ldate'], $avail['lacco_id'], $avail['lacco_type_id']);
					$re3 = $db->do_query($sql);
					
					//echo date('j F Y', $avail['ldate']).'===============<br />';
					//$l++;
				}else{
					//echo $k.'<br />';
					//$k++;
				}
			}
		}
		
		function syncTableAvailability(){
			$sql  = $db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res  = $db->do_query($sql);
			$user = $db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			//DELETING PART================================================================================================================
			$sql = $db->prepare_query("SELECT * FROM lumonata_accommodation_season WHERE lacco_id=%d", $_SESSION['product_id']);
			$res = $db->do_query($sql);
			while($season = $db->fetch_array($res)){
				$tmp = "SELECT * FROM lumonata_availability WHERE lacco_id=%d AND lseason=%s";
				$sql = $db->prepare_query($tmp, $_SESSION['product_id'], $season['lname']);
				$re2 = $db->do_query($sql);
				while($avail = $db->fetch_array($re2)){
					if($season['ldate_start']<=$avail['ldate'] && $season['ldate_finish']>=$avail['ldate']){
						//do nothing for now
					}else{
						//echo date('j F Y', $avail['ldate']).'<br />';
						$str = "DELETE FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lseason=%s";
						$sql = $db->prepare_query($str, $avail['ldate'], $avail['lacco_id'], $avail['lseason']);
						$re3 = $db->do_query($sql);
					}
				}
			}
			//DELETING PART================================================================================================================
			//UPDATE PART================================================================================================================
			$pField['Low Season']  = 'llow_season';
			$pField['High Season'] = 'lhigh_season';
			$pField['Peak Season'] = 'lpeak_season';
				
			//$limitBegin = strtotime($_POST['date_start']);
			//$limitEnd   = strtotime($_POST['date_finish']);
			
			$the24 = 24*60*60;
			$j = 0;
			$sql = $db->prepare_query("SELECT * FROM lumonata_accommodation_season WHERE lacco_id=%d", $_SESSION['product_id']);
			$sesRes = $db->do_query($sql);
			while($season = $db->fetch_array($sesRes)){
				//echo $season['lname'].'===========================================================================================<br />';
				$tmp = "SELECT * FROM lumonata_accommodation_type WHERE lacco_id=%d";
				$sql = $db->prepare_query($tmp, $_SESSION['product_id']);
				$re2 = $db->do_query($sql);
				while($type = $db->fetch_array($re2)){
					//echo $type['lname'].'===========================================================================================<br />';
					
					$str = "SELECT * FROM lumonata_accommodation_rate WHERE lacco_type_id=%d";
					$sql = $db->prepare_query($str, $type['lacco_type_id']);
					$res = $db->do_query($sql);
					$rate = $db->fetch_array($res);
					$nnRateRec = $db->num_rows($res);
					//echo $nnRateRec;
							
					$rangeStart  = $season['ldate_start'];
					$rangeFinish = $season['ldate_finish'];
					$typeID      = $type['lacco_type_id'];
					$prodID      = $_SESSION['product_id'];
					
					$aaa = date('d F Y', $rangeStart).' -- '.date('d F Y', $rangeFinish);
					
					if($nnRateRec>0){
						
						
					$continue = true;
					$i = $rangeStart;
					while($continue){
						$nDate 	= strtotime(date('m/d/y', $i));
						$i		= $nDate;
						
					//for($i=$rangeStart; $i<=$rangeFinish; $i+=$the24){
						//if( $limitBegin<=$i && $limitEnd>=$i ){
							$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
							$sql = $db->prepare_query($str, $i, $prodID, $typeID);
							$res = $db->do_query($sql);
							$nnn = $db->num_rows($res);
							$avail = $db->fetch_array($res);
							$status = explode(';',$avail['ledit']);
							
							$uRate = $rate[$pField[$season['lname']]];
							$uAlot = $rate['lallotment'];
							
							//echo $uRate.'<br />';
							$nDate = strtotime(date('m/d/y', $i));
							
							if($status[1]==1){ $uRate=$avail['lrate']; }
							if($status[0]==1){ $uAlot=$avail['lallotment']; }
							
							if($nnn>0){
								//DO Nothing FOr NOw
							}else{
								//echo 'que<br />';
								$str = "INSERT INTO lumonata_availability(
										ldate, lacco_id, lacco_type_id,
										lallotment, lrate, lcommision, lstatus, ledit,
										lcreated_by, lcreated_date, lusername, ldlu,
										lcut_of_date, lseason
										) VALUES (
										%d, %d, %d, 
										%d, %s, %s, %d, %s,
										%s, %d, %s, %d,
										%d, %s )";
								$sql = $db->prepare_query($str,
															 $i, $prodID, $typeID,
															 $uAlot, $uRate, $rate['lcommision'], 0, '0;0',
															 'appsSeasonEdit', time(), $_SESSION['product_id'], time(),
															 $season['lcut_of_date'], $season['lname']);
								$res = $db->do_query($sql);
							}
						//}
					//}//endFor
						
						
						$i += $the24;
						if($i>$rangeFinish){ $continue=false; }
					}//endWhile
					
					}//end if
				}
				$j++;
			}
			//UPDATE PART================================================================================================================
		}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>