<?php 
class additional_service_delete extends db{
	function additional_service_delete($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Additional Service Delete");
		
		require_once("../booking-engine/apps/additional-service/class.additional_service.php");
		$this->additional_service = new additional_service();
	}
	
	function load(){
		global $db;
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		
		if(!empty($_POST['as_id'])) {
			$this->additional_service->do_delete($t);
			$t->set_var('msg_succsess', 'Additional Service was success deleted');
			$t->set_var('jsAction', 'notifBlockSaved();');	
			header("Location: http://".SUPPLIERPANEL_SITE_URL."/additional-service/");
		}
		
		$query =  $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_id =%d",$_GET['act2']);
		$result = $db->do_query($query);
		$data = $db->fetch_array($result);
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('i', $i);	
		$t->set_var('as_id', $data['larticle_id']);
		$t->set_var('name', $data['larticle_title']);
		return $t->Parse('mBlock', 'mainBlock', false);
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
}
?>