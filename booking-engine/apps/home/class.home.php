<?php
	class home extends db{
		function home($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Home");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function load(){
			//print_r($_COOKIE);
			//echo '<br /><br />';
			
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];	
			}
			
			if(isset($_COOKIE['member_logged_ID'])){
				//echo $key;
								
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				$detail = $this->get_detail_content($key);
				//print_r($detail);
				$t->set_var('acco_img', 	$detail['mImage']);	
				$t->set_var('acco_name', 	$detail['title']);	
				$t->set_var('acco_star', 	$detail['star']);	
				$t->set_var('acco_address', strip_tags($detail['address'],"<i><em><b><strong><p><br>"));
				
				$t->set_var('thumbs', 		$detail['thumb']);
				$t->set_var('facilities', 	strip_tags($detail['facilities'],"<i><em><b><strong><ul><li><p><br>"));	
				$t->set_var('description', 	strip_tags($detail['desc'],"<i><em><b><strong><ul><li><p><br>"));
				
				$period =  $this->getSeasonPeriod($key);
				$t->set_var('seasons', $period['form'] );
				$t->set_var('lowPeriod',  $period['period']['low']);
				$t->set_var('highPeriod',  $period['period']['hi']);
				$t->set_var('peakPeriod',  $period['period']['peak']);
				$t->set_var('roomType', 	$this->get_room_type($key));
				
				return $t->Parse('mBlock', 'mainBlock', false);	
			}else{
				$_COOKIE['member_logged_ID']="";
				header("location:".'{http}'.SITE_URL."/member/login/");
			}
		}
		
		function get_detail_content($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=".$key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			$all['star'] = $one['lstar'];
			$all['accoID'] = $one['lacco_id'];
			$all['title'] = $one['lname'];
			$all['desc'] = $one['ldescription'];
			$all['address'] = $one['laddress'];
			$all['facilities'] = $one['lfacilities'];
			
			if($one['lacco_type_id']==1){$all['type'] = 'hotel';}
			if($one['lacco_type_id']==2){$all['type'] = 'villa';}
			
			$img = '{http}{rootsite_url}/images/Accommodations/'.$one['limage'];
			$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=168&h=117&src='.$img;
			$all['mImage'] = $imgurl;
			
			$key = $one['lacco_id'];
			$sql=parent::prepare_query("select * from lumonata_accommodation_images where lacco_id=".$key);
			$result=parent::query($sql);
			$imgs = '';
			while($rec=parent::fetch_array($result)){
				$img = '{http}{rootsite_url}/images/Accommodations/'.$rec['limage'];
				$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=180&h=140&src='.$img;
				$imgs .= '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
			}			
			$all['thumb'] = $imgs;
			
			return $all;
		}
		/*
		function get_room_type($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=".$key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			
			
			$sql=parent::prepare_query("select * from 
						lumonata_accommodation_type INNER JOIN lumonata_accommodation_rate 
						ON lumonata_accommodation_type.lacco_type_id=lumonata_accommodation_rate.lacco_type_id
						where lacco_id=".$one['lacco_id']);
			$result=parent::query($sql);			
			$all = '';
			while($two=parent::fetch_array($result)){
				$all .= '<div class="tContent one"><img class="infoIcon" src="{http}{template_url}/img/info-icon.gif" /><p>Double Standard<span>breakfast included</span></p></div>
						 <div class="tContent two">350</div>
						 <div class="tContent two">350</div>';
			}			
			return $all;
		}
		*/
		function get_room_type($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=".$key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			$sql=parent::prepare_query("select * from lumonata_accommodation_type INNER JOIN lumonata_accommodation_rate 
						ON lumonata_accommodation_type.lacco_type_id=lumonata_accommodation_rate.lacco_type_id
						where lacco_id=%d", $one['lacco_id']);
			$result=parent::query($sql);
			$all = '';
			while($two=parent::fetch_array($result)){
				if( $two['lbreakfast']==1 ){
					$bFast = 'breakfast included';
				}else{
					$bFast = 'breakfast not included';
				}
				
				$all .= '
					<div class="tContent one"><img class="infoIcon" original-title="'.strip_tags($two['lbrief']).'" src="{http}{template_url}/img/info-icon.gif" /><p>'.$two['lname'].'<span>'.$bFast.'</span></p></div>
					<div class="tContent two ">'.strip_tags($two['llow_season']).'</div>
					<div class="tContent two ">'.strip_tags($two['lhigh_season']).'</div>
					<div class="tContent two ">'.strip_tags($two['lpeak_season']).'</div>';
			}			
			return $all;
		}
		
		function getSeasonPeriod($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=%s", $key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			$sql=parent::prepare_query("select * from 
										lumonata_accommodation_season 
										where lacco_id=%d", $one['lacco_id']);
			$result=parent::query($sql);
			$all = '';
			while($two=parent::fetch_array($result)){
				if($two['lname']=='Low Season'){
					$name = 'lowSeason';
					$season['low'] .= date('j F Y', $two['ldate_start']).' - '.date('j F Y', $two['ldate_finish']).'<br />' ;
				}else if($two['lname']=='High Season'){
					$name = 'hiSeason';
					$season['hi'] .= date('j F Y', $two['ldate_start']).' - '.date('j F Y', $two['ldate_finish']).'<br />' ;
				}else{
					$name = 'peakSeason';
					$season['peak'] .= date('j F Y', $two['ldate_start']).' - '.date('j F Y', $two['ldate_finish']).'<br />' ;
				}
			
				$all .= '<input type="hidden" name="'.$name.'Start" 	value="'.$two['ldate_start'].'" />';
				$all .= '<input type="hidden" name="'.$name.'Finish"  value="'.$two['ldate_finish'].'" />';
				
				//print_r($two);
				//echo '<br><br><br>';
			}
			$ret['form'] = $all;
			$ret['period'] = $season;
						
			return $ret;
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>