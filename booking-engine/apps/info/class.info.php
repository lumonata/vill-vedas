<?php
	class info extends db{
		function info($appName){
			$this->appName=$appName;
			$this->setMetaTitle("info");
			
			require_once("../lumonata-admin/functions/upload.php");
			$this->upload = new upload();
			$this->upload->upload_constructor(IMAGE_PATH."/Accommodations/");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			 
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if(!empty($_POST)){
				//print_r($_POST);
				$sql=parent::prepare_query("UPDATE lumonata_accommodation SET
					lname=%s,
					lstar=%d,
					laddress=%s,
					ldlu=%d
					WHERE lacco_id=%d",
					$_POST['name'],
					$_POST['star'],
                    $_POST['theAddress'],
					time(),
					$key);
				$res=parent::query($sql);
				if($res){
					if (!empty($_FILES['image']['name'])){
						$image_name = $_FILES['image']['name'];
						$image_size = $_FILES['image']['size'];
						$image_type = $_FILES['image']['type'];
						$image_tmp = $_FILES['image']['tmp_name'];
						$sef_img = $this->upload->sef_url($_POST['name']."-".$key);
						$image = $this->upload->rename_file($image_name,$sef_img);
						$stmtImages = parent::prepare_query("UPDATE lumonata_accommodation SET limage=%s WHERE lacco_id =%d",$image,$key);
						$resultImages = parent::query($stmtImages);
						if ($image_name != $_POST['image_old']){
							$this->upload->delete_file($_POST['image_old'],0);
							$this->upload->delete_file($_POST['image_old'],1);
							$this->upload->delete_file($_POST['image_old'],2);
						}
						$dimensions = $this->globalAdmin->getImageDimensions(212);
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[0],$dimensions[1],0);
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[2],$dimensions[3],1);
						$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[4],$dimensions[5],2);
					}
					if($bySupplier){
						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"info",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
					}
					$t->set_var('jsAction', 'notifBlockSaved();');	
				}else{
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
			}

			$detail = $this->get_detail_content($key);
			for($i=1;$i<=5;$i++){
				$add = '';
				if($i==$detail['star']){$add='selected="selected"'; }
				$starList .= '<option '.$add.' value="'.$i.'">'.$i.'</option>';
			}
			$t->set_var('acco_star', 	$starList);	
			$t->set_var('acco_address', $detail['address']);
			$t->set_var('acco_name', $detail['title']);
			$img = '{http}{rootsite_url}/images/Accommodations/'.$detail['limage'];
			$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=152&h=103&src='.$img;
			$imgs = '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
			$existingImage = "<div class=\"wrapper form\" style=\"border-bottom:solid 1px #ccc;\">
						<label>Main Image</label>
						<label style=\"width:160px; margin-right:10px;\">".$imgs."
						<input name=\"image_old\" id=\"image_old\" type=\"hidden\" value=\"".$detail['limage']."\" />
						</label>
					</div>";
			$t->set_var('existingImage', $existingImage);
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function get_detail_content($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=%d",$key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			$all['star'] = $one['lstar'];
			$all['accoID'] = $one['lacco_id'];
			$all['title'] = $one['lname'];
			$all['desc'] = $one['ldescription'];
			$all['address'] = $one['laddress'];
			$all['facilities'] = $one['lfacilities'];
			$all['limage'] = $one['limage'];
			
			if($one['lacco_type_id']==1){$all['type'] = 'hotel';}
			if($one['lacco_type_id']==2){$all['type'] = 'villa';}
			
			$img = '{http}{rootsite_url}/images/Accommodations/'.$one['limage'];
			$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=168&h=117&src='.$img;
			$all['mImage'] = $imgurl;
			
			$key = $one['lacco_id'];
			$sql=parent::prepare_query("select * from lumonata_accommodation_images where lacco_id=%d",$key);
			$result=parent::query($sql);
			$imgs = '';
			while($rec=parent::fetch_array($result)){
				$img = '{http}{rootsite_url}/images/Accommodations/'.$rec['limage'];
				$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=180&h=140&src='.$img;
				$imgs .= '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
			}			
			$all['thumb'] = $imgs;
			
			return $all;
		} 
		 
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>