<?php
	class sync extends db{
		function sync($appName){
			/*
			$sql = "SELECT * FROM lumonata_accommodation_booking WHERE luser_currency=''";
			$result=parent::query($sql);
			while($data = parent::fetch_array($result)){
				$sq2  = "UPDATE lumonata_accommodation_booking SET luser_currency='USD', ltotal_usd=%s, lcommission_usd=%s, ltotal_after_usd=%s WHERE lbooking_id=%s";
				$stmt = parent::prepare_query($sq2, $data['ltotal'], $data['lcommision'], $data['ltotal_after'], $data['lbooking_id']);
				echo $re2  = parent::query($stmt);
				echo ' : <br />';
			}
			*/
			
			$sql=parent::prepare_query("SELECT a.lacco_id, b.lacco_type_id, a.lname as acco_name, b.lname as type_name
										FROM lumonata_accommodation a
										LEFT JOIN lumonata_accommodation_type b ON a.lacco_id = b.lacco_id
										WHERE a.lpublish=1");	
			$result=parent::query($sql);
			while($d=parent::fetch_array($result)){
				
				//seasson
				$query=parent::prepare_query("SELECT * FROM lumonata_accommodation_season 
						WHERE lacco_id=%d",$d['lacco_id']);
				$thresult=parent::query($query);
				$the24 = 24*60*60;
				while ($d2=parent::fetch_array($thresult)){
					for($i=$d2['ldate_start'];$i<=$d2['ldate_finish'];$i+=$the24){
						$qr=parent::prepare_query("SELECT *
												 	FROM lumonata_accommodation_rate 
													WHERE lacco_type_id=%d",$d['lacco_type_id']);
						$dr=parent::fetch_array(parent::query($qr));
						
						switch ($d2['lname']){
							case "Low Season":
								$rate=$dr['llow_season'];
								break;
							case "High Season":
								$rate=$dr['lhigh_season'];
								break;
							case "Peak Season":
								$rate=$dr['lpeak_season'];
								break;
						}
						
						$insert=parent::prepare_query("INSERT INTO lumonata_availability(ldate,
																						lacco_id,
																						lacco_type_id,
																						lallotment,
																						lrate,
																						lcommision,
																						lcut_of_date,
																						lseason,
																						lstatus,
																						ledit,
																						lcreated_by,
																						lcreated_date,
																						lusername,
																						ldlu)
																					VALUES(%d,
																						   %d,
																						   %d,
																						   %d,
																						   %s,
																						   %s,
																						   %d,
																						   %s,
																						   %d,
																						   %s,
																						   %s,
																						   %d,
																						   %s,
																						   %d)",
																							$i,
																							$d['lacco_id'],
																							$d['lacco_type_id'],
																							$dr['lallotment'],
																							$rate,
																							$dr['lcommision'],
																							$d2['lcut_of_date'],
																							$d2['lname'],
																							0,
																							'0;0',
																							$d2['lcreated_by'],
																							$d2['lcreated_date'],
																							$d2['lcreated_by'],
																							$d2['lcreated_date']
																							);
						
						$tsql=parent::prepare_query("SELECT * FROM lumonata_availability 
								WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d",$i,$d['lacco_id'],$d['lacco_type_id']);
						
						$tquery=parent::query($tsql);
						$tnum=parent::num_rows($tquery);
						
						if($tnum==0){																	
							$rd=parent::query($insert);
							if($rd){
								$this->thereport.="INSERT:".$d['acco_name']." | ".$d['type_name']." | ".date("d m Y",$i)." | ".$rate."<br />";
							}
						}else{
							
							$update=parent::prepare_query("UPDATE lumonata_availability
														   SET lallotment=%d,
																		lrate=%s,
																		lcommision=%s,
																		lcut_of_date=%d,
																		lseason=%s,
																		lstatus=%d,
																		ledit=%s,
																		lcreated_by=%s,
																		lcreated_date=%d,
																		lusername=%s,
																		ldlu=%d
																					
														WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d",
																							$dr['lallotment'],
																							$rate,
																							$dr['lcommision'],
																							$d2['lcut_of_date'],
																							$d2['lname'],
																							0,
																							'0;0',
																							$d2['lcreated_by'],
																							$d2['lcreated_date'],
																							$d2['lcreated_by'],
																							$d2['lcreated_date'],
																							$i,
																							$d['lacco_id'],
																							$d['lacco_type_id']
																							);
							
							$rd=parent::query($update);
							
							if($rd){
								$this->thereport.="UPDATE:".$d['acco_name']." | ".$d['type_name']." | ".date("d m Y",$i)." | ".$rate."<br />";
							}
						}
					}
				}
				
			}	
			return $report;	
		}
		function load(){
			return "Sukses";
		}
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>