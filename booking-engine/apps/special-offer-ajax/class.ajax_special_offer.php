<?php 
class ajax_special_offer extends db{
	var $appName;
	function ajax_special_offer($appName){
		$this->appName=$appName;
		$this->template_var="ajax_special_offer";
	}
	
	function load(){
		
	}
	
	function get_additional_service_price(){
		global $db;
		$data_id	 = explode('_',$_POST['val_id']);
		$id_rule 	 = $data_id[0];
		$id_adt		 = $data_id[1];
		$index_price = $data_id[2];
		$qty		 = $_POST['val_qty']; 
		
		$num_price_time = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					      "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						   and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'num_row');	
		$price = 0;				   
		if($num_price_time > 0){
			$data_prices = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'array');	
			$data_prices = explode(';',$data_prices['lvalue']);
			$price = $data_prices[$index_price];		
			$price = $price * $qty;
		}else{
			$data_price = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_price'",'array');	
			$days  = $_POST['val_day']; 				
			$price = ($data_price['lvalue'] * $qty) * $days;
		}
		echo $price ;			
	}
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function getTemplateVar(){
		return $this->template_var;
	}	
	
}
?>