<?php 
	class special_offer_edit extends db{
		function special_offer_edit($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Special Offer Edit");
			require_once("../booking-engine/apps/special-offer/class.special_offer.php");
			$this->special_offer = new special_offer();
			
		}
		
		function load(){
			global $db;	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			
			if(isset($_POST['prc']) && $_POST['prc'] == "save_edit"){$this->special_offer->edit_multiple_data($t);}			
			$this->special_offer->show_multiple_edit($t);
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
		
			
	}
?>