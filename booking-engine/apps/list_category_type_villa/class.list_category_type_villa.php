<?php 
class list_category_type_villa extends db {
		var $appName;
		
		function list_category_type_villa($appName){
			$this->appName=$appName;
			$this->template_var="list_category_type_villa";
		}
		
		function load($selected_id=0,$view_empty_option = false){
			if (isset($_COOKIE['member_logged_ID'])){
				if(isset($_POST['do_act']) && $_POST['do_act']=='testing_get_name' ){
					return $this->get_name_category_type_villa($_POST['id']);
					exit;
				}
				
				global $db;
				//get list category
				if($view_empty_option) $option = "<option value=\"\">Select Room Type</option>";
				else $option = "";
				$str_category = $db->prepare_query("select * from lumonata_rules where lrule=%s and lgroup=%s and lname!=%s",'categories','villas','Villas');
				$result_category = $db->do_query($str_category);
				while($data_category = $db->fetch_array($result_category)){
					$id_category  = $data_category['lrule_id'];
					$category_name = $data_category['lname'];
					
					$str_room_type = $db->prepare_query("select * from lumonata_rules where lrule=%s and lgroup=%s",'room_type','villas');
					$result_room_type = $db->do_query($str_room_type);
					while($data_room_type = $db->fetch_array($result_room_type)){
						$id_room_type = $data_room_type['lrule_id'];
						$name_room_type = $data_room_type['lname'];

						$the_id = $id_category.'_'.$id_room_type;
						if($the_id==$selected_id)$selected='selected="selected"';
						else $selected="";
						$option .= "<option value=\"$the_id\" $selected>$category_name, $name_room_type</option>";
					}
				}//end while category
				
				return $option;
			}//end if isset cookie member
		}
		
		function get_name_category_type_villa($id){
			global $db;
			//get category
			$name = "";
			$id_data = explode('_',$id);
			$category_id = $id_data[0];
			$roomtype_id = $id_data[1];
			$str_category = $db->prepare_query("select * from lumonata_rules where lrule=%s and lgroup=%s and lname!=%s and lrule_id=%d",'categories','villas','Villas',$category_id);
			$result_category = $db->do_query($str_category);
			
			while($data_category = $db->fetch_array($result_category)){
				$category_name = $data_category['lname'];
					
				$str_room_type = $db->prepare_query("select * from lumonata_rules where lrule=%s and lgroup=%s and lrule_id=%d",'room_type','villas',$roomtype_id);
				$result_room_type = $db->do_query($str_room_type);
				while($data_room_type = $db->fetch_array($result_room_type)){
					$name_room_type = $data_room_type['lname'];
					$name = $category_name.', '.$name_room_type;
				}
			}//end while category
			return $name;
		}
		
		
}
?>