<?php
	class additional_service extends db{
		function additional_service($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Additional Service");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin = new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
			
			require_once("../booking-engine/apps/additional-service-category/class.additional_service_category.php");
			$this->additional_service_category = new additional_service_category();
			
			require_once("../booking-engine/apps/additional-field/class.additional_field.php");
			$this->additional_field = new additional_field();
			
		}
		function load(){
			global $db;		
			if(isset($_POST['prc']) && ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){//new additional service
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				$this->show_empty_price_time($t);
				$t->set_var('parent', $this->additional_service_category->get_parent(0,0,0,true));
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title', "New");			
				$t->set_var('i', 0);
				
				if(isset($_POST['name'])) $this->save_additional_sercive($t);		
			}elseif(isset($_POST['prc']) && ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){//edit additional service
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				if($_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}			
				if (sizeof($_POST['pilih']) != 0){$this->show_multiple_edit($t);}//show multiple edit
				
				$t->set_var('title', "Edit");			
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}elseif(isset($_POST['prc']) && $_POST['prc'] == "category" ){//edit additional service
				header('Location: http://'.SITE_URL.'/booking-engine/additional-service-category/');
			}else{	
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				if (isset($_POST['prc']) && $_POST['prc']=="delete" && sizeof($_POST['pilih']) != 0) $this->show_delete($t);
				if (isset($_POST['action']) && $_POST['action']=="Yes")	$this->do_delete($t);
								
				$theQStr = $db->prepare_query("select * from lumonata_articles where larticle_type=%s",'additional_service');
				//BEGIN set var on viewContent Block
				$result = $db->do_query($theQStr);
				
				while($data = $db->fetch_array($result)){
					$t->set_var('name',$data['larticle_title']);
					$data_user = $this->data_table('lumonata_users','where luser_id='.$data['lupdated_by']);
					$data_user = $db->fetch_array($data_user);	
					$t->set_var('check', $data['larticle_id']);
					$t->set_var('author', $data_user['ldisplay_name']);
					$date = new DateTime($data['lpost_date']);
					$t->set_var('date',  $date->format('m-d-Y'));
					$t->Parse('vC', 'viewContent', true); 	
				}
				
				//END set var on viewContent Block	
			}//end if validate post
			return $t->Parse('mBlock', 'mainBlock', false);	
		}
		
		
		function show_empty_price_time($t,$index=0){
			$cont_additional_price = $this->temp_additional_field(0,$index,false,0,0,0,true);
			$t->set_var('cont_additional_price',$cont_additional_price);
			$t->set_var('clone_container',"<div class=\"clone-price-cont\"></div>");	
			$t->set_var('btn_add_price',"<div class=\"wrapper form bt-abu-true price_cont_$index cont_price use-time btn-add-cont-price $div_btn\" >
											<input type=\"button\" value=\"Add Price\" class=\"add_price_$index\" />
										</div>");	
			
		}
		
		function checkDataAvailable($theID){
			$str = parent::prepare_query("SELECT * FROM lumonata_accommodation_aditionalservice_package WHERE lservice_id=%d", $theID);
			$res = parent::query($str);
			$nnn = parent::num_rows($res);
			if($nnn<=0){
				$str2 = parent::prepare_query("DELETE FROM lumonata_accommodation_aditionalservice WHERE lservice_id=%d",$theID);
				$res2 = parent::query($str2);
			}
		}
		
		function getAdditionalServices($sel){
			$sql = parent::prepare_query("SELECT * FROM lumonata_accommodation_aditionalservice WHERE lacco_id=%d", $_SESSION['product_id']);
			$res = parent::query($sql);
			while($rec = parent::fetch_array($res) ){
				if($rec['lservice_id']==$sel){ $add='selected="selected"'; }else{ $add=''; }
				$all .= '<option '.$add.' value="'.$rec['lservice_id'].'">'.$rec['ltitle'].'</option>';
			}
			return $all;
			
		}
		
		function save_additional_sercive($t){
			global $db;
			$category  =  $_POST['parent'][0];
			$name = $_POST['name'][0];
			$sef =  strtolower(str_replace(' ','-',$name));
			$desc =  $_POST['description'][0];
			$status = 'publish';
			$type = 'additional_service';
			$comments = '';
			$share_to = 0;
			$str=$db->prepare_query("INSERT INTO lumonata_articles(larticle_title,
                                                                larticle_content,
                                                                larticle_status,
                                                                larticle_type,
                                                                lcomment_status,
                                                                lsef,
                                                                lpost_by,
                                                                lpost_date,
                                                                lupdated_by,
                                                                ldlu,
                                                                lshare_to)
                                VALUES(%s,%s,%s,%s,%s,%s,%d,%s,%d,%s,%d)",
                                                                $name,
                                                                $desc,
                                                                $status,
                                                                $type,
                                                                $comments,
                                                                $sef,
                                                                $_COOKIE['user_id'],
                                                                date("Y-m-d H:i:s"),
                                                                $_COOKIE['user_id'],
                                                                date("Y-m-d H:i:s"),
                                                                $share_to);
			
			
			$result = $db->do_query($str);
			if ($result){
				$post_id=mysql_insert_id();
				//insert additional fields
				$this->save_addtional_field($t,$post_id);
				//insert the categories into the rule_relationship table
				$this->additional_field->save_rule_relationship($post_id,$_POST['parent'][0]);
					
				$t->set_var('notif_message', 'data saved');	
				$t->set_var('jsAction', 'notifBlockSaved();');	
			}else{
				$t->set_var('notif_message', 'failed to saved');
				$t->set_var('jsAction', 'notifFailedSaved();');	
			}
		}
		
		function save_addtional_field($t,$post_id,$i=0){
			if(isset($_POST['use_time'])){
				$prices = '' ;
				foreach($_POST['price_time'][$i] as $price){
					if($price!=''){
						if($prices=='')$prices .= $price;
						else $prices .=';'.$price;	
					}
					
				}
				$times = '';
				foreach($_POST['time'][$i] as $key_time => $time){
					if($_POST['price_time'][$i][$key_time] !=''){
						if($times=='')$times .= $time;
						else $times .=';'.$time;	
					}
					
				}
				//save price
				$this->additional_field->save_additional_field($post_id,'additional_service_prices',$prices,'additional_service');
				//save time
				$this->additional_field->save_additional_field($post_id,'additional_service_times',$times,'additional_service');
				
			}else{
				//save price
				$this->additional_field->save_additional_field($post_id,'additional_service_price',$_POST['price'][0][0],'additional_service');
			}
		}
		
		function data_table($table,$where){
			global $db;
			$str =  $db->prepare_query("select * from $table $where");
			$result = $db->do_query($str);
			return $result;				
		}
		
		function show_multiple_edit($t){
			global $db;
			if(isset($_POST['pilih'])) $data_pilih = $_POST['pilih'];
			else $data_pilih = array($_GET['act2']);
			
			//for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			for ($i=0; $i<sizeof($data_pilih); $i++) { 
				
				$query =  $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_id =%d",$data_pilih[$i]);
				$result = $db->do_query($query);
				$data = $db->fetch_array($result);
				
				$t->set_var('pilih',$data_pilih[$i]);
				$t->set_var('i', $i);	
				$t->set_var('as_id', $data['larticle_id']);
				$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
				$t->set_var('name', $data['larticle_title']);
				$rule_id = $this->get_rule_id($data['larticle_id']);
				$t->set_var('parent', $this->additional_service_category->get_parent($rule_id,$i,0,true));
				
				//get additional field
				$this->show_edit_addtional_field($t,$data['larticle_id'],$i);
				
				
				$t->set_var('description', $data['larticle_content']);
				$t->Parse('eC', 'editContent', true); 
			}
			//print_r($data_pilih);
			//if(isset())
			
			//print_r($_GET);
		}
		
		function show_edit_addtional_field($t,$id,$index){
			global $db;
			$lkey_prices = 'additional_service_prices';
			$app_name = 'additional_service';
			$data_prices = $this->data_table('lumonata_additional_fields',"where lapp_id=$id and lkey='$lkey_prices' and lapp_name='$app_name'");
			$data_prices = $db->fetch_array($data_prices);
			$div_btn = '';
			
			if(!empty($data_prices)){
				$t->set_var('use_time', 'checked="checked"');
				$lkey_times  = 'additional_service_times';
				$data_times  = $this->data_table('lumonata_additional_fields',"where lapp_id=$id and lkey='$lkey_times' and lapp_name='$app_name'");
				$data_times  = $db->fetch_array($data_times);
				$data_prices = explode(';',$data_prices['lvalue']);
				$data_times  = explode(';',$data_times['lvalue']);
				
				$cont_additional_price = "";
				$last_index = count($data_prices)-1;
				$is_last = false;
				foreach($data_prices as $key => $price){
					$time = $data_times[$key];
					if($key==$last_index)$is_last=true;
					$cont_additional_price .= $this->temp_additional_field($key,$index,true,$price,$time,0,$is_last);
				}
				$cont_additional_price .= "";
				
				$t->set_var('cont_additional_price',$cont_additional_price);
				$div_btn = 'show-div';
			}else{
				//echo 'dsdsd';
				$lkey_price  = 'additional_service_price';
				$data_price = $this->data_table('lumonata_additional_fields',"where lapp_id=$id and lkey='$lkey_price' and lapp_name='$app_name'");
				$data_price = $db->fetch_array($data_price);
				//print_r($data_price);
				$cont_additional_price = $this->temp_additional_field(0,$index,false,0,0,$data_price['lvalue'],true);
				$t->set_var('cont_additional_price',$cont_additional_price);
				$t->set_var('clone_container',"<div class=\"clone-price-cont\"></div>");	
			}
			
			$t->set_var('btn_add_price',"<div class=\"wrapper form bt-abu-true price_cont_$index cont_price use-time btn-add-cont-price $div_btn\" >
											<input type=\"button\" value=\"Add Price\" class=\"add_price_$index\" />
										</div>");
			
			
		}
		
		function temp_additional_field($key,$i,$show_price_time=false,$price_time=0,$time=0,$price=0,$is_last=false){
			$temp="";
			
			if ($key>0){
				if($key==1){
					$temp = "<div class=\"clone-price-cont\">";
				}
				
				$temp .= "<div class=\"wrapper form bt-abu-true\" >
							<label>Price</label>
							<div class=\"price_cont_$i cont_price use-time show-div\">
								<input type=\"text\" name=\"price_time[$i][]\" class=\"text required number middle\" value=\"$price_time\" /> 
								<span>Time</span> <input type=\"text\" name=\"time[$i][]\" class=\"text required number middle\" value=\"$time\" />
								<div class=\"clear\"></div>                   
							</div>
						</div>";	
				
				
				if($is_last){
					$temp .= "</div>";
				}
				
			}else{
				$div_price_time = '';
				$div_price = '';
				if($show_price_time) {
					$div_price_time = 'show-div';
					$div_price = 'hide-div';
				}
				$temp = "<div class=\"wrapper form bt-abu-true the_price_cont\" >
							<label>Price</label>
							<div class=\"price_cont_$i cont_price use-time $div_price_time\">
								<input type=\"text\" name=\"price_time[$i][]\" class=\"text required number middle\" value=\"$price_time\" /> 
								<span>Time</span> <input type=\"text\" name=\"time[$i][]\" class=\"text required number middle\" value=\"$time\" />
								<div class=\"clear\"></div>                   
							</div>
							<div class=\"price_cont_$i not-use-time $div_price\">
								<input type=\"text\" name=\"price[$i][]\" class=\"text required number middle\" value=\"$price\" />
							 </div>
						</div>";					
			}
			
					
			return $temp;		
		}
		
		function edit_multiple_data($t){
			global $db;
			$msg = "";
			$error = 0;
			
			for($i=0;$i<count($_POST['as_id']);$i++){
				$id = $_POST['as_id'][$i];
				$name = $_POST['name'][$i];
				$parent  =  $_POST['parent'][$i];
				$desc =  $_POST['description'][$i];
				
				$old_rule_id = $this->get_rule_id($id);	
				//update article
				$str_update_article = $db->prepare_query("UPDATE lumonata_articles
											 SET larticle_title=%s,
												larticle_content=%s,
												lupdated_by=%s,
												ldlu=%s
											 WHERE larticle_id=%d",
												$name,
												$desc,
												$_COOKIE['user_id'],
												date("Y-m-d H:i:s"),
												$id);											
				$do_update = $db->do_query($str_update_article);								
				
				//update rules relationship		
				$this->additional_field->update_rule_relationship($id,$parent, $old_rule_id);				

				//update additional field
				$this->additional_field->delete_additional_field($id,'additional_service');	
				$this->save_addtional_field($t,$id,$i);
				
				
				$msg .= "<li>Update rate for \"".$_POST['name'][$i]."\" was successfully processed.</li>";		
			}
			
			if($error==1){
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifFailedSaved();');	
			}else{
				$t->set_var('notif_message', "<ul>".$msg."</ul>");
				$t->set_var('jsAction', 'notifBlockSaved();');	
			}
		}
		
		
		function get_rule_id($id){
			global $db;
			$str = $db->prepare_query("select * from lumonata_articles as a inner join
									   lumonata_rule_relationship as b 
									   on a.larticle_id=b.lapp_id inner join
									   lumonata_rules as c on
									   b.lrule_id=c.lrule_id where c.lgroup=%s and a.larticle_id=%d",'additional_service',$id);			   
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			return $data['lrule_id'];
		}		
		
		function show_delete($t){
			global $db;
			$alltitle = '';
			
			for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
				$query = $db->prepare_query("SELECT * FROM lumonata_articles where larticle_id =%d",$_POST['pilih'][$i]);
				$result = $db->do_query($query);
				$data = $db->fetch_array($result);
				
				$t->set_var('i', $i);
				$t->set_var('delId', $_POST['pilih'][$i]);
				$n1 = 0;
				
				if ($n1 != 0)
				{
					$title = $title."<li>".$data['larticle_title']."</li>";
				}
				$alltitle = $alltitle."<li>".$data['larticle_title']."</li>";
				$t->Parse('dC', 'deleteContent', true);
			}
			$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
			$t->set_var('notif_message', $msg);
		}
		
		function do_delete($t){
			global $db;
			$msg = '';
			$error = '';
			if(isset($_POST['delId'])) $data_pilih = $_POST['delId'];
			else $data_pilih = array($_POST['as_id']);
			
			//for ($i=0; $i<sizeof($_POST['delId']); $i++) {
			for ($i=0; $i<sizeof($data_pilih); $i++) {
				$str = $db->prepare_query("select * from lumonata_articles where larticle_id=%d",$data_pilih[$i]);
				$result = $db->do_query($str);
				$data = $db->fetch_array($result);
				$rule_id =$this->get_rule_id($data['larticle_id']);			
				
				$str_delete = $db->prepare_query("delete from lumonata_articles where larticle_id=%d",$data_pilih[$i]);
				$result_delete = $db->do_query($str_delete);
				if($result_delete) {
					//delete addtional field
					$this->additional_field->delete_additional_field($data_pilih[$i],'additional_service');	
					//delete rule realtionship
					$this->additional_field->delete_rule_relationship($data_pilih[$i],$rule_id);	
					
					$msg .= "<li>Delete Additional Service for \"".$data['larticle_title']."\" was successfully processed.</li>";
				}else{
					$error = 1;
					$msg .= "<li>Delete Additional Service for \"".$data['larticle_title']."\" was unsuccessfully processed.</li>";
				}
			}
			
			if($error==1){
				$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
			}else{
				$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
			}
		}
		
		
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	}
?>