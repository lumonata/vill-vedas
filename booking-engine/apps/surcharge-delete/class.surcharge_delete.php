<?php 
class surcharge_delete extends db{
	function surcharge_delete($appName){
		$this->appName=$appName;
		$this->meta_desc ='';
		$this->meta_key='';
		$this->setMetaTitle("Surcharge Delete");
		
		
	}
	
	function load(){
		global $db;
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		//print_r($_POST);
		if(!empty($_POST['pp_id'])) {
			$this->do_delete($_POST['pp_id']);
			$t->set_var('msg_succsess', 'Surcharge was success deleted');
			$t->set_var('jsAction', 'notifBlockSaved();');	
			header("Refresh:3; url= http://".SUPPLIERPANEL_SITE_URL."/surcharge/", true, 303);
		}
		
		$surecharge_ID = $_GET['act2'];;
		$data_surcharge = $this->data_tabel('lumonata_accommodation_surecharge',"where lsurecharge_ID=$surecharge_ID",'array');
		
		$t->set_var('desc', $data_surcharge['ldescription']);	
		if($data_surcharge['ldate_from']==0 && $data_surcharge['ldate_to']==0) $dates = "All Time";
		else $dates = date("d M Y",$data_surcharge['ldate_from'])." - ".date("d M Y",$data_surcharge['ldate_to']);
		$t->set_var('dates', $dates);		
		
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('i', 0);	
		$t->set_var('pp_id', $data_surcharge['lsurecharge_ID']);
		//$t->set_var('name', date('d-m-Y',$data['ldate_start']).' - '.date('d-m-Y',$data['ldate_end']));
		return $t->Parse('mBlock', 'mainBlock', false);
		
	}
	
	function do_delete($id){
		global $db;
		$str = $db->prepare_query("delete from lumonata_accommodation_surecharge where lsurecharge_ID=%d",$id);
		$db->do_query($str);	
	}
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");	
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
}
?>