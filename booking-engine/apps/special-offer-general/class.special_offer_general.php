<?php 
class special_offer_general extends db{
	var $appName;
	function special_offer_general($appName){
		$this->appName=$appName;
		$this->template_var="special_offer_general";
	}
	
	function load(){
		
	}
	
	function get_parent($selected_parent=0,$index=0,$id_show=0){
		global $db;
		$str 	=  $db->prepare_query("select * from lumonata_special_offer_categories order by ldlu");
		$result = $db->do_query($str);
		$parent = "<select name=\"parent[$index]\" class=\"big\">";
		while($data= $db->fetch_array($result)){
			if($data['lcat_id']!=$id_show){
				if($data['lcat_id']==$selected_parent) $select_info = 'selected="selected"';
				else $select_info='';
				//if($data['lparent']==0){
					$parent .= "<option $select_info value=".$data['lcat_id'].">".$data['lcategory']."</option>";
				/*}else{
					$level = $this->find_level($data['lrule_id']);
					$nbs = "";
					for($i=1;$i<=$level;$i++){
						$nbs .= "&nbsp;&nbsp;&nbsp;";
					}
					$parent .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";
				} */
			}
		}
		$parent .= "</select>";
		return $parent;
	}
	
	
	
	function get_additional_service_price_server($data_id,$qty,$days=0){
		global $db;
		$data_id_first  = $data_id;
		$data_id	 = explode('_',$data_id);
		$id_rule 	 = $data_id[0];
		$id_adt		 = $data_id[1];
		$index_price = $data_id[2];
		
		$num_price_time = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					      "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						   and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'num_row');	
		$price = 0;	
		if($num_price_time > 0){
			$data_prices = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'array');	
			
			$data_prices = explode(';',$data_prices['lvalue']);
			$price = $data_prices[$index_price];
			$price = $price * $qty;
		}else{
			
			$data_price = $this->data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_price'",'array');	
			//echo $data_price['lvalue'].'#'.$qty.'#'.$days;
			$price = ($data_price['lvalue'] * $qty) * $days;
			
		}
		return $price ;				
	}
	
	
	
	function get_list_additional_service($index,$data_selected=array()){
		global $db;
		
		$list = "";
		$data_categories = $this->data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=0");
		$num_categories = $this->data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=0",'num_row');
		$i=1;
		$class = "bb-abu";
		while($category = $db->fetch_array($data_categories)){
			if($i==$num_categories)$class='';
			$name = $category['lname'];
			$parent_id = $category['lrule_id'];
			$num_sub_categories = $this->data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$parent_id",'num_row');
			$sub_additional_service = "";
			if($num_sub_categories>0){
				$sub_additional_service = $this->get_sub_category_additional_service($category['lrule_id'],$index,$data_selected);
			}else{
				$sub_additional_service = $this->get_direct_list_sub_additional_service($category['lrule_id'],$name,$index,$data_selected);
			}
			
			$list .= "<li class=\"$class\">$name $sub_additional_service</li>";
			$i++;
			//
		}
		$temp = "<div class=\"list_additional_service\">
                    	<ul>
                        	$list
                        </ul>
                    </div>";
		
		return $temp;
	}
	
	function get_direct_list_sub_additional_service($rule_id,$name,$index,$data_selected){
		global $db;
		
		$list = "";
		$data_rule_relationship = $this->data_tabel('lumonata_rule_relationship',"where lrule_id=$rule_id");
		while($rule_relationship = $db->fetch_array($data_rule_relationship)){
			$id_adf = $rule_relationship['lapp_id'];
			$data_additional_service = $this->data_tabel('lumonata_articles',"where larticle_id=$id_adf",'array');
			$name = "<label>".$data_additional_service['larticle_title']."</label>";
			$data_price_additional_service = $this->data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf");
			
			$list_price = "";
			//validate if use time
			$num_price_time = $this->data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_times'",'num_row');
			//echo $id_adf.'#';
			if($num_price_time>0){
				$times = $this->data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_times'",'array');
				$times = explode(';',$times['lvalue']);
				$prices = $this->data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_prices'",'array');
				$prices  = explode(';',$prices['lvalue']);
				
				foreach ($prices as $i => $price){
					$time = $times[$i];
					$value = $rule_id.'_'.$id_adf.'_'.$i;
					//validate for selected	
					$class ='';
					$sub_total='';
					$checked='';
					if(!empty($data_selected[$value])){
						$class = 'show';
						$checked = 'checked="checked"';
						$sub_total = $data_selected[$value]['lprice'];
						$qty = $data_selected[$value]['lqty'];
					}			
					
					$list_price .= "<div class=\"price_additonal_service\">
										<input type=\"checkbox\" class=\"checkbox get_price_additional_service\" name=\"get_price_additional_service[$index][]\" 
										 value=\"$value\" rel=\"$id_adf\" $checked /> 
										<span>$price.00 - $time minutes</span>
										<input type=\"text\" value=\"$qty\" name=\"qty_additional_service_".$value."[$index]\" class =\"qty_additional_service $class\" />    
										<span class=\"sub_total_addtional_service\">$sub_total</span>
										<div class=\"clear\"></div>
									</div>";
				}
				
			}else{
				while($additional_price = $db->fetch_array($data_price_additional_service)){
					$price = $additional_price['lvalue'];
					$value = $rule_id.'_'.$id_adf.'_0';
					
					//validate for selected$class ='';
					$sub_total='';
					$checked='';
					$days='';
					if(!empty($data_selected[$value])){
						$class = 'show';
						$checked = 'checked="checked"';
						$sub_total = $data_selected[$value]['lprice'];
						$qty = $data_selected[$value]['lqty'];
						$days = $data_selected[$value]['llday'];
					}		
					
					
					$list_price .= "<div class=\"price_additonal_service\">
										<input type=\"checkbox\" class=\"checkbox get_price_additional_service\" name=\"get_price_additional_service[$index][]\" 		
										 rel=\"$id_adf\" value=\"$value\" $checked /> 
										<span>$price</span>
										<input type=\"text\" value=\"$qty\" name=\"qty_additional_service_".$value."[$index]\" class =\"qty_additional_service $class\" />
										<input type=\"text\" value=\"$days\" name=\"days_additional_service_".$value."[$index]\" class =\"days_additional_service $class\" />     									<span class=\"sub_total_addtional_service \">$sub_total</span>
										<div class=\"clear\"></div>
									</div>";
				}
			}
			$price_detail = "<div class=\"list_price\">$list_price<div class=\"clear\"></div></div>";
			
			$list .= "<li >
							$name$price_detail
							<div class=\"clear\"></div>
					  </li>";
		}
		$temp = "<ul>
					$list
				</ul>";
				
		return $temp;		
	}
	
	function get_sub_category_additional_service($parent_id,$index,$data_selected){
		global $db;
		
		$list_addtional_service = "";
		$data_sub_categories = $this->data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$parent_id");
		while($sub_category = $db->fetch_array($data_sub_categories)){
			//check if have child
			$rule_id = $sub_category['lrule_id'];
			$name = $sub_category['lname'];
			$data_child = $this->data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$rule_id",'num_row');
			if($data_child>0){
				//get_sub	
				$list_addtional_service .= $this->get_sub_category_additional_service($rule_id,$index,$data_selected);
			}else{
				$list_addtional_service .= $this->get_direct_list_sub_additional_service($rule_id,$name,$index,$data_selected);
			}
			//print_r($sub_category);

			//$list .= "<li class=\"$class\">$name</li>";
			//$sub_category = $this->get_sub_category_additional_service($category['lrule_id']);
		}
		$temp = "<ul>
					$list_addtional_service
				</ul>";
		
		
		/*$temp = "<ul>
					<li>
						<label>Thai Massage</label>
						<div class=\"list_price\">
							<div class=\"price_additonal_service\">
								<input type=\"checkbox\" class=\"checkbox\" /> 
								<span>60minutes - 100.00</span>
								<input type=\"text\" value=\"2\" />    
								<span class=\"sub_total_addtional_service\">200.00</span>
								<div class=\"clear\"></div>
							</div>
							
							<div class=\"price_additonal_service\">
								<input type=\"checkbox\" class=\"checkbox\" /> 
								<span>90minutes - 150.00</span>
								<input type=\"text\" value=\"2\" />    
								<span class=\"sub_total_addtional_service\">150.00</span>
								<div class=\"clear\"></div>
							</div>
						</div>
						<div class=\"clear\"></div>                                    
					</li>
					<li>Balinese Massage</li>
				</ul>";*/
				
		return $temp;		
		
	}	
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function getTemplateVar(){
		return $this->template_var;
	}	
}
?>