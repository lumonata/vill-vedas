<?php 
class additional_service_category extends db{
	function additional_service_category($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Additional Service Category");
		
		require_once("../lumonata-admin/functions/globalAdmin.php");
		$this->globalAdmin=new globalAdmin();
		$this->to=$this->globalAdmin->getSettingValue('email');
		$this->cc=$this->globalAdmin->getSettingValue('cc');
		$this->bcc=$this->globalAdmin->getSettingValue('bcc');
		global $globalSetting;
	}
	function load(){
		global $db;		
		if(isset($_POST['prc']) && ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){//new additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			$t->set_var('parent', $this->get_parent());
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
			$t->set_var('title', "New");			
			$t->set_var('i', 0);
			
			if(isset($_POST['name'])) $this->save_category_additional_sercive($t);			
		}elseif(isset($_POST['prc']) && ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){//edit additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if($_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}			
			if (sizeof($_POST['pilih']) != 0){$this->show_multiple_edit($t);}//show multiple edit
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}else{	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'deleteContent', 'dC');
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if (isset($_POST['prc']) && $_POST['prc']=="delete" && sizeof($_POST['pilih']) != 0){
				$this->show_delete($t);
			}

			if (isset($_POST['action']) && $_POST['action']=="Yes"){
				$this->do_delete($t);
			}
			
			$theQStr = $db->prepare_query("select * from lumonata_rules where lgroup=%s",'additional_service');
			//BEGIN set var on viewContent Block
			$result = $db->do_query($theQStr);
			while($data = $db->fetch_array($result)){
				if($data['lparent']==0) $t->set_var('parent','Root &raquo;');
				else $t->set_var('parent',$this->get_parent_desc($data['lparent']));
				$t->set_var('name',$data['lname']);
				$t->set_var('check', $data['lrule_id']);	
				$t->set_var('decription',$data['ldescription']);
				$t->Parse('vC', 'viewContent', true); 	
			}
			//END set var on viewContent Block
					
		}//end if validate post
		return $t->Parse('mBlock', 'mainBlock', false);	
	}
	
	function get_parent_desc($id,$str_before=''){
		global $db;
		
		$str = $db->prepare_query("select * from lumonata_rules where lrule_id=%d",$id);
		$result = $db->do_query($str);
		while($data= $db->fetch_array($result)){
			if($data['lparent']==0){
				return 'Root &raquo; '.$data['lname'].' &raquo; '.$str_before;
			}else{
				return $this->get_parent_desc($data['lparent'],$data['lname'].' &raquo; '.$str_before);
			}			
		}
	}
	
	
	function save_category_additional_sercive($tmp){
		global $db;
		$parent  =  $_POST['parent'][0];
		$name = $_POST['name'][0];
		$sef =  strtolower(str_replace(' ','-',$name));
		$desc =  $_POST['description'][0];
		$rule = 'categories';
		$group = 'additional_service';
		$count = 0;
		$order = 0;
		$subsite = 'arunna';
		
		$str =  $db->prepare_query("insert into lumonata_rules 
									(lparent,lname,lsef,ldescription,lrule,
									 lgroup,lcount,lorder,lsubsite)
									values 
									(%d,%s,%s,%s,%s,
									 %s,%d,%d,%s)",
									 $parent,$name,$sef,$desc,$rule,
									 $group,$count,$order,$subsite);
			
		
		$result = $db->do_query($str);
		if ($result){
							
			$tmp->set_var('notif_message', 'data saved');	
			$tmp->set_var('jsAction', 'notifBlockSaved();');	
		}else{
			$tmp->set_var('notif_message', 'failed to saved');
			$tmp->set_var('jsAction', 'notifFailedSaved();');	
		}
	}
	
	function show_multiple_edit($t){
		global $db;
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query =  $db->prepare_query("SELECT * FROM lumonata_rules WHERE lrule_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$t->set_var('pilih',$_POST['pilih'][$i]);
			$t->set_var('i', $i);	
			$t->set_var('asc_id', $data['lrule_id']);
			$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
			$t->set_var('name', $data['lname']);
			$t->set_var('asc_id', $data['lrule_id']);
			$t->set_var('parent', $this->get_parent($data['lparent'],$i,$_POST['pilih'][$i]));
			$t->set_var('description', $data['ldescription']);
     		$t->Parse('eC', 'editContent', true); 
		}
	}
	
	function edit_multiple_data($t){
		global $db;
		$msg = "";
		$error = 0;
		for($i=0;$i<count($_POST['asc_id']);$i++){
			$parent  =  $_POST['parent'][$i];
			$name = $_POST['name'][$i];
			$sef =  strtolower(str_replace(' ','-',$name));
			$desc =  $_POST['description'][$i];
			
			$str  = $db->prepare_query("update lumonata_rules SET
									 lparent=%d,
									 lname=%s,
									 lsef=%s,
									 ldescription=%s
									 where lrule_id=%d
									",$parent,$name,$sef,$desc,$_POST['asc_id'][$i]);
									
									
			$result = $db->do_query($str);						
			if($result)$msg .= "<li>Update rate for \"".$_POST['name'][$i]."\" was successfully processed.</li>";
			else{
				$error = 1;
				$msg .= "<li>Update rate for \"".$_POST['name'][$i]."\" was unsuccessfully processed.</li>";
			} 				
		}
		
		if($error==1){
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}else{
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}
	}
	
	function show_delete($t){
		global $db;
		$alltitle = '';
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query = $db->prepare_query("SELECT * FROM lumonata_rules where lrule_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$t->set_var('i', $i);
			$t->set_var('delId', $_POST['pilih'][$i]);
			$n1 = 0;
			
			if ($n1 != 0)
			{
				$title = $title."<li>".$data['lname']."</li>";
			}
			$alltitle = $alltitle."<li>".$data['lname']."</li>";
			$t->Parse('dC', 'deleteContent', true);
		}
		$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
		$t->set_var('notif_message', $msg);
	}
	
	function do_delete($t){
		global $db;
		$msg = '';
		$error = '';
		for ($i=0; $i<sizeof($_POST['delId']); $i++) {
			$str = $db->prepare_query("select * from lumonata_rules where lrule_id=%d",$_POST['delId'][$i]);
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			
			$str_delete = $db->prepare_query("delete from lumonata_rules where lrule_id=%d",$_POST['delId'][$i]);
			$result_delete = $db->do_query($str_delete);
			if($result_delete) {
				$msg .= "<li>Delete Additional Service Category for \"".$data['lname']."\" was successfully processed.</li>";
			}else{
				$error = 1;
				$msg .= "<li>Delete Additional Service Category for \"".$data['lname']."\" was unsuccessfully processed.</li>";
			}
		}
		
		if($error==1){
			$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
		}else{
			$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
		}
	}
	
	
	function get_parent($selected_parent=0,$index=0,$id_show=0, $not_show_parent = false){
		global $db;
		$str 	=  $db->prepare_query("select * from lumonata_rules where lgroup=%s",'additional_service');
		$result = $db->do_query($str);
		$parent = "<select name=\"parent[$index]\" class=\"big\">";
		if($not_show_parent==false) $parent .= "<option value=\"0\">Parent</option>";
		while($data= $db->fetch_array($result)){
			if($data['lrule_id']!=$id_show){
				if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
				else $select_info='';
				if($data['lparent']==0){
					$parent .= "<option $select_info value=".$data['lrule_id'].">".$data['lname']."</option>";
				}else{
					$level = $this->find_level($data['lrule_id']);
					$nbs = "";
					for($i=1;$i<=$level;$i++){
						$nbs .= "&nbsp;&nbsp;&nbsp;";
					}
					$parent .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";
				} 
			}
		}
		$parent .= "</select>";
		return $parent;
	}
	
	function find_level($id,$level=0){
		global $db;
		$str = $db->prepare_query("select * from lumonata_rules where lrule_id=%d",$id);
		$result = $db->do_query($str);
		
		while($data= $db->fetch_array($result)){
			if($data['lparent']==0){
				return $level;
			}else{
				return $this->find_level($data['lparent'],$level+1);
			}			
		}
		
	}
	
	function if_parent_is_not_me($id){
			
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
}
?>