<?php

class general_category_villa extends db{
	function general_category_villa($appName=''){
		$this->appName=$appName;
		$this->setMetaTitle("General Category Villa");
	}
		
	function get_category_type_data($id=0){
		global $db;
		$option = "";
		$list_category = $this->get_data('lumonata_rules',"where lrule='room_type' and lgroup='villas'");	
		while($data_category = $db->fetch_array($list_category)){
			$the_id = $data_category['lrule_id'];
			$category_name = $data_category['lname'];
			if($the_id==$id)$selected='selected="selected"';
			else $selected="";
			$option .= "<option value=\"$the_id\" $selected>$category_name</option>";
		}
		
		return $option;
	}
	
	function get_category_name($id=0){
		global $db;
		//echo "where lrule='room_type' and lgroup='villas' and lrule_id=$id";
		$data_category = $this->get_data('lumonata_rules',"where lrule='room_type' and lgroup='villas' and lrule_id=$id",'array');	
		$category_name = "";
		if(!empty($data_category)) $category_name = $data_category['lname'];
		return $category_name;
	}
	
	function get_data($t,$w,$r='result_only'){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->data_tabels($t,$w,$r);
	}	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}	
}
 ?>