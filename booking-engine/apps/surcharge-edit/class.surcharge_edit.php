<?php 
	class surcharge_edit extends db{
		function surcharge_edit($appName){
			$this->appName=$appName;
			$this->meta_desc = '';
			$this->meta_key = '';
			$this->setMetaTitle("Surcharge Edit");
			require_once(ROOT_PATH."/booking-engine/apps/surcharge/class.surcharge.php");
			$this->surcharge = new surcharge('');
			
		}
		
		function load(){
			global $db;	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			
			if(isset($_POST['prc']) && $_POST['prc'] == "save_edit"){$this->edit_data($t);}			
			$this->show_edit($t);
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function edit_data($t){
			global $db;
			require_once("../booking-engine/apps/surcharge/class.surcharge.php");
			$this->surcharge = new surcharge('');
			if($this->surcharge->is_valid_value_on_update($t)){
				$msg = '';
				$error = 0;
				//print_r($_POST);exit;
				for($i=0;$i<count($_POST['surecharge_ID']);$i++){
					$surecharge_ID = $_POST['surecharge_ID'][$i];
					if(isset($_POST['all_time'][$i])) $all_time 		= $_POST['all_time'][$i]; 
					
					if(isset($_POST['all_time'][$i]) && $all_time == 'all_time'){
						$date_start 	= 0; 
						$date_finish 	= 0; 	
					}else{
						$date_start 	= strtotime($_POST['date_start'][$i]); 
						$date_finish 	= strtotime($_POST['date_finish'][$i]); 	
					}
					
					$dow 			= $_POST['dow'][$i]; 
					$desc 			= $_POST['desc'][$i]; 
					$ammount 		= $_POST['ammount'][$i]; 
					$ammount_unit 	= $_POST['lammount_unit'][$i]; 
					$str = $db->prepare_query("update lumonata_accommodation_surecharge set
											   ldate_from=%d,ldate_to=%d,lday_of_week=%s,
											   ldescription=%s,lcharge=%s,lammount_unit=%s,ldlu=%d
											   where lsurecharge_ID=%d
											   ",$date_start,$date_finish,$dow,
											   $desc,$ammount,$ammount_unit,time(),
											   $surecharge_ID);
					
					$result = $db->do_query($str);
					if(!$result)$error++;	
				}//end for
				
				if($error==1){
					$t->set_var('notif_message', "<ul><li>Update was failed processed.</li></ul>");
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}else{
					$t->set_var('notif_message', "<ul><li>Update was successfully processed.</li></ul>");
					$t->set_var('jsAction', 'notifBlockSaved();');
				}
			}
			
		}
		
		function show_edit($t){
			$surecharge_ID = $_GET['act2'];
			$data_surcharge = $this->get_data('lumonata_accommodation_surecharge',"where lsurecharge_ID=$surecharge_ID",'array');
			$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; border-bottom:solid 1px #ccc;background:#f5f5f5;\"></div>");
			$t->set_var('pilih',$surecharge_ID);
			$t->set_var('surecharge_ID', $surecharge_ID); 
			$t->set_var('i', 0);
			//print_r($data_surcharge)	;
			//$t->set_var('room_category',$this->get_room_type($data_surcharge['lacco_type_id']));
									
			$t->set_var('surechargeDesc', $data_surcharge['ldescription']);
			$array = array('USD','%');
			$all = '';
			foreach($array as $value){
				if($value==$data_surcharge['lammount_unit']){ $sel = ' selected="selected" '; }else{ $sel=''; }
				$all .= '<option '.$sel.' value="'.$value.'">'.$value.'</option>';
			}
			$t->set_var('lammount_unit',  $all); 
			
			if($data_surcharge['ldate_from']==0 && $data_surcharge['ldate_to']==0){
				$t->set_var('all_time', 'checked'); 	 		
			}else{
				$t->set_var('date_start', 	date("m/d/Y",$data_surcharge['ldate_from'])); 	 	
				$t->set_var('date_finish', 	date("m/d/Y",$data_surcharge['ldate_to']));
			}
			
			$t->set_var('dow',     $data_surcharge['lday_of_week']); 	 	
			$t->set_var('ammount', $data_surcharge['lcharge']);						
			$t->Parse('eC', 'editContent', true); 		
		}		
		
		function get_data($t,$w,$r='result_only'){
			require_once("../booking-engine/apps/general-func/class.general_func.php");
			$this->general_func = new general_func();
			return $this->general_func->data_tabels($t,$w,$r);
		}
		function get_room_type($id,$action='get_list'){
			require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
			$this->general_category_villa = new general_category_villa();
					
			if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
			else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
		}
			
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
		
			
	}
?>