<?php
	class facilities extends db{
		function facilities($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Facilities");
            
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			//print_r($_GET);//, [bool return])
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if(!empty($_POST)){
				//print_r($_POST);
				$time = time();
				$sql=parent::prepare_query("UPDATE lumonata_accommodation SET lfacilities=%s, lfacilities_JSON=%s, ldlu=%d WHERE lacco_id=%d",$_POST['theFacilities'],$_POST['facilitiesJSON'],$time,$key);
				//$sql=($tmp);
				$res=parent::query($sql);
				if($res){
					$t->set_var('jsAction', 'notifBlockSaved();');
  					if($bySupplier){
  						$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"facilities",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
  					}
  				}
			}
	
			$detail = $this->get_detail_content($key);
			$t->set_var('theFacilities',  $detail['facilities']);
			$t->set_var('facilitiesJSON', $detail['facilitiesJSON']);
			$t->set_var('theFacilitiesList', $this->availableFacilities());
			
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function availableFacilities(){
			$sql = parent::prepare_query("select * from lumonata_accommodation_facilities");
			$res = parent::query($sql);
			$nnn = parent::num_rows($res);
			if($nnn>0){
			while($rec = parent::fetch_array($res)){
				$add = '';
				if($rec['lpublish']!=1){ $add = '<small style="float:left; margin-left:10px; color:#f00; font-size:0.9em;">waiting for moderation</small>'; }
				$all .= '<div class="one" id="theFacilities-'.$rec['lfacilities_ID'].'" rel="'.$rec['lfacilities'].'" ><p style="float:left;">'.$rec['lfacilities'].'</p>'.$add.'</div>';
			}}else{
				$all = '';
			}
			return $all;
		}
		
		function get_detail_content($key){
			$sql=parent::prepare_query("select * from lumonata_accommodation where lacco_id=%d",$key);
			$result=parent::query($sql);
			$one=parent::fetch_array($result);
			
			$all['star'] = $one['lstar'];
			$all['accoID'] = $one['lacco_id'];
			$all['title'] = $one['lname'];
			$all['desc'] = $one['ldescription'];
			$all['address'] = $one['laddress'];
			$all['facilities'] = $one['lfacilities'];
			$all['facilitiesJSON'] = $one['lfacilities_JSON'];
			
			if($one['lacco_type_id']==1){$all['type'] = 'hotel';}
			if($one['lacco_type_id']==2){$all['type'] = 'villa';}
			
			$img = '{http}{rootsite_url}/images/Accommodations/'.$one['limage'];
			$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=168&h=117&src='.$img;
			$all['mImage'] = $imgurl;
			
			$key = $one['lacco_id'];
			$sql=parent::prepare_query("select * from lumonata_accommodation_images where lacco_id=%d",$key);
			$result=parent::query($sql);
			$imgs = '';
			while($rec=parent::fetch_array($result)){
				$img = '{http}{rootsite_url}/images/Accommodations/'.$rec['limage'];
				$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=180&h=140&src='.$img;
				$imgs .= '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
			}			
			$all['thumb'] = $imgs;
			
			return $all;
		} 

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>