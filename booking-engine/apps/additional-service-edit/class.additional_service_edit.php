<?php 
	class additional_service_edit extends db{
		function additional_service_edit($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Additional Service Edit");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin = new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
			
			require_once("../booking-engine/apps/additional-service-category/class.additional_service_category.php");
			$this->additional_service_category = new additional_service_category();
			
			require_once("../booking-engine/apps/additional-field/class.additional_field.php");
			$this->additional_field = new additional_field();
			
			require_once("../booking-engine/apps/additional-service/class.additional_service.php");
			$this->additional_service = new additional_service();
			
		}
		
		function load(){
			global $db;	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			
			if(isset($_POST['prc']) && $_POST['prc'] == "save_edit"){$this->additional_service->edit_multiple_data($t);}			
			$this->additional_service->show_multiple_edit($t);
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
		
			
	}
?>