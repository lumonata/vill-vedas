<?php
	class rate extends db{
		function rate($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Rate");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;			
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			if(isset($_POST['do_act']) && $_POST['do_act']=='test_merge_to_availabel'){
				return $this->addTheNewRate();
				exit;
			}elseif(isset($_POST['do_act']) && $_POST['do_act']=='test_sincronice_availability'){
				return $this->sync_TableAvailability($_POST['acco_type_cat_id'],$_POST['low_season'],$_POST['high_season'],$_POST['peak_season'],
													$_POST['low_additional'],$_POST['high_additional'],$_POST['peak_additional']);
			}
			
			if ((isset($_POST['prc']) And $_POST['prc'] == "new") || (isset($_POST['prc']) And $_POST['prc'] == "save_new")){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				if($_POST['prc'] == "save_new"){
					if ($_POST['commision'][0] < $min_commision){
						$t->set_var('notif_message', 'failed to saved, minimal commission is '.$min_commision);
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}else{
						if (isset($_POST['currency'][0])){$currency = $_POST['currency'][0];}else{$currency = '';}
						if (isset($_POST['min_commision'][0])){$min_commision = $_POST['min_commision'][0];}else{$min_commision = '';}
						
						$low_additional = $_POST['low_additional'][0];
						$high_additional = $_POST['high_additional'][0];
						$peak_additional = $_POST['peak_additional'][0];
						
						$rate_cat_id = $this->globalAdmin->setCode("lumonata_accommodation_rate_categories","lrate_cat_id");
						$cat_roomtype_id = $_POST['acco_type_cat_id'][0];
						$stmt = $db->prepare_query("INSERT INTO lumonata_accommodation_rate_categories (
							lacco_type_cat_id,lcurrency,llow_season,lhigh_season,lpeak_season,
							lbreakfast,lallotment,lmin_commision,lcommision,lorder_id,
							lcreated_by,lcreated_date,llow_additional,lhigh_additional,lpeak_additional,
							lrate_cat_id)
							VALUES(
							%s,%s,%s,%s,%s,
							%d,%d,%s,%s,%d,
							%s,%d,%s,%s,%s,
							%d)",
							$cat_roomtype_id,$currency,$_POST['low_season'][0],$_POST['high_season'][0],$_POST['peak_season'][0],$_POST['breakfast'][0],1,		
							$min_commision,$_POST['commision'][0],1,$_SESSION['upd_by'],time(),
							$low_additional,$high_additional,$peak_additional,$rate_cat_id);
						
						$this->globalAdmin->setOrderID("lumonata_accommodation_rate_categories",1);		
						$result = $db->do_query($stmt);
						if ($result){
							//sync to table availability
							$this->sync_TableAvailability($cat_roomtype_id,$_POST['low_season'][0],$_POST['high_season'][0],$_POST['peak_season'][0],
															$low_additional,$high_additional,$peak_additional);
							
							$t->set_var('notif_message', 'data saved');	
							$t->set_var('jsAction', 'notifBlockSaved();');	
						}else{
							$t->set_var('notif_message', 'failed to saved');
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}
						
					}	
					
				}
				$t->set_var('i', 0);
				
				$qry = $db->prepare_query("SELECT * FROM lumonata_accommodation_type_categories Order by lname 	Asc ");
				$rs = $db->do_query($qry);
							
				$t->set_var('room_category',$this->get_category_type_data('get_list'));
				
				
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('min_commision',$min_commision);
				$t->set_var('title', "New");
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if ((isset($_POST['prc']) And $_POST['prc'] == "edit") || (isset($_POST['prc']) And $_POST['prc'] == "save_edit")){

				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					$msg = '';
					$error = '';
					for($i=0;$i<count($_POST['rate_id']);$i++){
						
					$low_additional = $_POST['low_additional'][$i];
					$high_additional = $_POST['high_additional'][$i];
					$peak_additional = $_POST['peak_additional'][$i];
					
					$cat_roomtype_id = $_POST['acco_type_cat_id'][$i];
					if (isset($_POST['currency'][$i])){$currency = $_POST['currency'][$i];}else{$currency = '';}
							$stmt = $db->prepare_query("UPDATE lumonata_accommodation_rate_categories SET
								lacco_type_cat_id=%s,
								lcurrency=%s,
								llow_season=%s,
								lhigh_season=%s,
								lpeak_season=%s,
								
								llow_additional=%s,
								lhigh_additional=%s,
								lpeak_additional=%s,
								
								lbreakfast=%d,
								lallotment=%s,
								lcommision=%s,
								lusername=%s,
								ldlu=%d
								WHERE lrate_cat_id =%d",
								$cat_roomtype_id,
								$currency,
								$_POST['low_season'][$i],
								$_POST['high_season'][$i],
								$_POST['peak_season'][$i],
								
								$low_additional,
								$high_additional,
								$peak_additional,
								
								$_POST['breakfast'][$i],
								1,
								$_POST['commision'][$i],
								$_SESSION['upd_by'],
								time(),
								$_POST['rate_id'][$i]);
							$result = $db->do_query($stmt);
							
							$query = $db->prepare_query("SELECT 
								tc.lname as room_category
								FROM lumonata_accommodation_rate_categories rc
								LEFT JOIN lumonata_accommodation_type_categories tc On rc.lacco_type_cat_id  = tc.lacco_type_cat_id
								WHERE rc.lrate_cat_id =%d",$_POST['rate_id'][$i]);
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							
							
							if ($result){
								$qd = $db->prepare_query("Delete From lumonata_accommodation_rate Where lrate_cat_id = %d",$_POST['rate_id'][$i]);
								$rd = $db->do_query($qd);
								$this->sync_TableAvailability($_POST['acco_type_cat_id'][$i],$_POST['low_season'][$i],$_POST['high_season'][$i],$_POST['peak_season'][$i],$low_additional,$high_additional,$peak_additional);								
								$msg .= "<li>Update rate for \"".$data['room_category']."\" was successfully processed.</li>";
							}else{
								$error = 1;
								$msg .= "<li>Update rate for \"".$data['room_category']."\" was unsuccessfully processed.</li>";
							}
							
					}// end for
						if($error==1){
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}else{
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifBlockSaved();');	
							if($bySupplier){
								$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"rate",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
							}
						}
				}
				//end edit process
				
				//start display data
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$query =  $db->prepare_query("SELECT *
							FROM lumonata_accommodation_rate_categories
							WHERE lrate_cat_id =%d",$_POST['pilih'][$i]);
						$result = $db->do_query($query);
						$data = $db->fetch_array($result);
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						$t->set_var('rate_id', $data['lrate_cat_id']);
						$t->set_var('currency', $data['lcurrency']);
						
						$t->set_var('low_season', $data['llow_season']);
						$t->set_var('high_season', $data['lhigh_season']);
						$t->set_var('peak_season', $data['lpeak_season']);
						
						$t->set_var('low_additional',  $data['llow_additional']);	 		 	
						$t->set_var('high_additional', $data['lhigh_additional']);
						$t->set_var('peak_additional', $data['lpeak_additional']);
						
						$t->set_var('allotment', $data['lallotment']);
						
						$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
						$t->set_var('min_commision',$min_commision);
						
						$t->set_var('commision', $data['lcommision']);	 	
						$t->set_var('order_id', $data['lorder_id']);
						if ($data['lbreakfast'] == 0){
							$t->set_var('check0', "checked=\"checked\"");
							$t->set_var('check1', "");
						}else{
							$t->set_var('check0', "");
							$t->set_var('check1', "checked=\"checked\"");
						}
						
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$t->set_var('room_category',$this->get_category_type_data('get_list',$data['lacco_type_cat_id']));
						
						
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if ((isset($_POST['prc']) And $_POST['prc']=="delete")){
					if (sizeof($_POST['pilih']) != 0){
						$alltitle = '';
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = $db->prepare_query("SELECT rc.lacco_type_cat_id
								FROM lumonata_accommodation_rate_categories rc
								LEFT JOIN lumonata_accommodation_type_categories tc On rc.lacco_type_cat_id  = tc.lacco_type_cat_id
								WHERE rc.lrate_cat_id =%d",$_POST['pilih'][$i]);
							$result = $db->do_query($query);
							$data = $db->fetch_array($result);
							
							$thename = $this->get_category_villa_name($data['lacco_type_cat_id']);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;//$this->globalAdmin->getNumRows("lumonata_accommodation_rate","lacco_type_id",$data['lacco_type_id']);
							
							if ($n1 != 0)
							{
								$title = $title."<li>".$thename."</li>";
							}
							$alltitle = $alltitle."<li>".$thename."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, room type below using in accommodation rate data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if ((isset($_POST['action']) And $_POST['action']=="Yes")){
					$msg = '';
					$error = '';
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						//$msgTitle = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$_POST['delId'][$i]); 
						$query = $db->prepare_query("SELECT 
								rc.lacco_type_cat_id 
							FROM lumonata_accommodation_rate_categories rc
							LEFT JOIN lumonata_accommodation_type_categories tc On rc.lacco_type_cat_id  = tc.lacco_type_cat_id
							WHERE rc.lrate_cat_id =%d",$_POST['delId'][$i]);
						$result = $db->do_query($query);
						$data 	= $db->fetch_array($result);
						$thename = $this->get_category_villa_name($data['lacco_type_cat_id']);
						//==========================================================================================================================================================================================	
						//==========================================================================================================================================================================================
							
						$selStrCat = "SELECT * FROM lumonata_accommodation_rate_categories WHERE lrate_cat_id =%d";
						//echo "<br >".
						$sqlCat    = $db->prepare_query($selStrCat, $_POST['delId'][$i]);
						$resCat    = $db->do_query($sqlCat);
						$theRatCat = $db->fetch_array($resCat);
						//==========================================================================================================================================================================================	
						//==========================================================================================================================================================================================
						//echo "<br >".
						$stmtCat 	= $db->prepare_query("DELETE FROM lumonata_accommodation_rate_categories WHERE lrate_cat_id =%d",$_POST['delId'][$i]);
						$resultCat = $db->do_query($stmtCat);
						//$resultCat = true;
						if($resultCat){
							//==========================================================================================================================================================================================	
							//==========================================================================================================================================================================================
								
							$selStr = "SELECT * FROM lumonata_accommodation_rate WHERE lrate_cat_id =%d";
							//echo "<br >".
							$sql    = $db->prepare_query($selStr, $theRatCat['lrate_cat_id']);
							$res    = $db->do_query($sql);
							
							//==========================================================================================================================================================================================	
							//==========================================================================================================================================================================================
							//echo "<br >".													
							$stmt 	= $db->prepare_query("DELETE FROM lumonata_accommodation_rate WHERE lrate_cat_id =%d",$theRatCat['lrate_cat_id']);
							$result = $db->do_query($stmt);
							//$result = true;
							if($result){
								
								while ($theRat = $db->fetch_array($res)){
							//==========================================================================================================================================================================================	
							//==========================================================================================================================================================================================	
								///echo "<br >".
								$str = "DELETE FROM lumonata_availability WHERE lacco_type_id=%d";
								$sql = $db->prepare_query($str, $theRat['lacco_type_id']);
								$re2 = $db->do_query($sql);
								//echo $sql;
								if($re2){
									//$msg .= "<li>Delete rate for \"".$data['lroom_type']."\" was successfully processed.</li>";
								}							
								//==========================================================================================================================================================================================	
								//==========================================================================================================================================================================================
								}	
								
							}
							$msg .= "<li>Delete rate for \"".$thename."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete date for \"".$thename."\" was unsuccessfully processed.</li>";
						}
						
						
					}// end for
					
					
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
					}else{
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
					}
				}// END Delete Process 
				
				$query =  $db->prepare_query("SELECT rc.lrate_cat_id,
						rc.lacco_type_cat_id,
						tc.lname as room_category,
						rc.lcurrency,
						rc.llow_season,
						rc.lhigh_season,
						rc.lpeak_season,
						rc.llow_additional,
						rc.lhigh_additional,
						rc.lpeak_additional,
						rc.lbreakfast,
						rc.lallotment,
						rc.lmin_commision,
						rc.lcommision,
						rc.lorder_id,
						rc.lcreated_by,
						rc.lcreated_date,
						rc.lusername,
						rc.ldlu
						FROM lumonata_accommodation_rate_categories rc
						LEFT JOIN lumonata_accommodation_type_categories tc ON rc.lacco_type_cat_id  = tc.lacco_type_cat_id");
				
				//BEGIN set var on viewContent Block
				$result = $db->do_query($query);
				$no = 0;
				while($data = $db->fetch_array($result)){
					$t->set_var('no', $no);	 	
					$t->set_var('check', $data['lrate_cat_id']);	
					$name = $this->get_category_villa_name($data['lacco_type_cat_id']);		
					$t->set_var('name', $name);
					$t->set_var('low_season',  $data['llow_season']);	 		 	
					$t->set_var('high_season', $data['lhigh_season']);
					$t->set_var('peak_season', $data['lpeak_season']);
					
					$t->set_var('low_additional',  $data['llow_additional']);	 		 	
					$t->set_var('high_additional', $data['lhigh_additional']);
					$t->set_var('peak_additional', $data['lpeak_additional']);
					
					
					if($data['lbreakfast']==0){
						$t->set_var('breakfast', "No");	
					}else{
						$t->set_var('breakfast', "Yes");
					}
					$t->set_var('allotment', $data['lallotment']);	 		 	
					$t->set_var('min_commision', $data['lmin_commision']);	 		 	
					$t->set_var('commision', $data['lcommision']);	 
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}
		
		function get_category_villa_name($id){
			$data = $this->get_data('lumonata_rules',"where lrule='room_type' and lgroup='villas' and lrule_id=$id",'array');
			return $data['lname'];
			/*require_once('../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php');
			$this->list_category_type_villa  = new list_category_type_villa();
			return  $this->list_category_type_villa->get_name_category_type_villa($id);*/
		}
		
		function sync_TableAvailability($catroomtype_id,$low_season,$high_season,$peak_season,
										$low_season_additional,$high_season_additional,$peak_season_additional){
			global $db;

			$date_now = $this->date_now();
			$rates_season = array();
			$rates_season['Low Season'] = $low_season;
			$rates_season['High Season'] = $high_season;
			$rates_season['Peak Season'] = $peak_season;
			
			$rates_season_additional = array();
			$rates_season_additional['Low Season'] = $low_season_additional;
			$rates_season_additional['High Season'] = $high_season_additional;
			$rates_season_additional['Peak Season'] = $peak_season_additional;
			//get list villa
			//$list_villa = $this->data_tables("lumonata_relationship_villa_category_room_type where lacco_type_cat_id='$catroomtype_id'");
			$list_villa = $this->data_tables("lumonata_rule_relationship as a, lumonata_rules as b 
											  where 
											  a.lrule_id=b.lrule_id and b.lrule='room_type' and b.lgroup='villas' and
											  a.lrule_id='$catroomtype_id'");					  
			$num_villa = $db->num_rows($list_villa);
			//get list season
			$list_season = $this->data_tables('lumonata_accommodation_season');
			$num_season = $db->num_rows($list_season);
			
			if($num_villa>0 && $num_season>0){
				//each villa check
				while($villa = $db->fetch_array($list_villa)){
					//print_r($villa);
					$accom_id = $villa['lapp_id'];
					$list_season = $this->data_tables('lumonata_accommodation_season');
					//each season check
					while($season = $db->fetch_array($list_season)){
						$season_name = $season['lname'];
						$date_start  = $season['ldate_start'];
						$date_finish	 = $season['ldate_finish'];
						$days = $this->get_days($date_start,$date_finish);
						$rate = $rates_season[$season_name];
						$rate_additional = $rates_season_additional[$season_name];
						$this->sync_TableAvailability_step_2($accom_id,$rate,$rate_additional,$season_name,$date_start,$date_finish,$days);	
					}//end while list season
				}//end while list villa			
			}//if have villa
			
		}
		
		function sync_TableAvailability_step_2($accom_id,$rate,$rate_additional,$season_name,$date_start,$date_finish,$days){
			global $db;
			$date_now = $this->date_now();
			$username = $_SESSION['upd_by'];
			for($i=0;$i<=$days;$i++){
				$date = $date_start + ($i * 86400);	
							
				if($date >=$date_now){
					$data_availability = $this->data_tables("lumonata_availability","WHERE ldate=$date AND lacco_id=$accom_id");
					$n = $db->num_rows($data_availability);
					if($n>0){
						$data_availability_array = $db->fetch_array($data_availability);
						$status = $data_availability_array['lstatus'];
						if($status==0 || $status==5 ){
							 //do nothing
						}else{
							
							if($rate=='' or $rate==0){//just update season
								$query_update = $db->prepare_query("update lumonata_availability 
																	set lseason=%s,ledit=%s,ldlu=%d where ldate=%d and lacco_id=%d",
																	$season_name,$username,time(),$date,$accom_id);
							}else{
								$query_update = $db->prepare_query("update lumonata_availability 
																	set lrate=%s,lrate_additional=%s,lseason=%s,ledit=%s,ldlu=%d where ldate=%d and lacco_id=%d",
																	$rate,$rate_additional,$season_name,$username,time(),$date,$accom_id);
							}
							$db->do_query($query_update);
						}		
					}else{
						$query_insert = $db->prepare_query("insert into lumonata_availability
															(ldate,lacco_id,lrate,lrate_additional,lseason,
															 lstatus,lreason,ledit,lcreated_by,lcreated_date,
															 lusername,ldlu) 
															 values(%d,%d,%s,%s,%s,
															 		%d,%s,%s,%s,%d,
																	%s,%d)",
															$date,$accom_id,$rate,$rate_additional,$season_name,1,
															'',$username,$username,time(),
															$username,time());	
																
						$db->do_query($query_insert);			
					
					}//if num on availability 0
				}//end if is not passed
			}//end for
			
		}
		
		
		function date_now(){
			$date_now  = date('m/d/Y');
			return strtotime($date_now);
		}
		
		function get_days($date_start,$date_finish){
			$days = ($date_finish-$date_start)/86400;
			return $days;
		}
		
		function get_accom_rate($data_rate, $season_name){
			if ($season_name=='Low Season') return $data_rate['llow_season'];
			else if($season_name=='High Season') return $data_rate['lhigh_season'];
			else if($season_name=='Peak Season') return $data_rate['lpeak_season'];
		}
		
		function data_tables($tabel,$where=''){
			global $db;
			$str = $db->prepare_query("select * from $tabel $where");
			$result = $db->do_query($str);
			return $result;
		}
		
		
		
		function addTheNewRate($acco_type_id=''){
			
			global $db;
			//==============================================================================================
			//==============================================================================================
			$acco_type_id = $_POST['accom_id'];
			
			//get all seasson first
			echo time();
			$str_season = $db->prepare_query("select * from lumonata");
			
			
			/*$season[] = 'Low Season';
			$season[] = 'High Season';
			$season[] = 'Peak Season';
			
			$pField[] = 'low_season';
			$pField[] = 'high_season';
			$pField[] = 'peak_season';
			
			$oField[] = 'low_additional';
			$oField[] = 'high_additional';
			$oField[] = 'peak_additional';

			$the24 = 24*60*60;
			$iii = 0;
							
			foreach($season as $key1){				
				$str = "SELECT larticle_id FROM lumonata_articles WHERE larticle_id=%d";
				$sql = $db->prepare_query($str, $acco_type_id);
				$res1 = $db->do_query($sql);
				$rec = $db->fetch_array($res1);
				$key2 = $rec['larticle_id'];
				
				$str = "SELECT ldate_start, ldate_finish, lcut_of_date FROM lumonata_accommodation_season WHERE lacco_id=%d and lname=%s";
				$sql = $db->prepare_query($str, $key2, $key1);
				$res2 = $db->do_query($sql);
				//echo $sql.'<br />';
				
				while($range = $db->fetch_array($res2)){
				//print_r(date('j F Y',$range['ldate_start']). '-' . date('j F Y',$range['ldate_finish']) . '<br />');
				//echo '==============================<br />';
				for($i=$range['ldate_start']; $i<=$range['ldate_finish']; $i+=$the24){
					//echo 'a<br />';
					$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d";
					$sql = $db->prepare_query($str, $i, $key2);
					$res3 = $db->do_query($sql);
					$nnn = $db->num_rows($res3);
					$avail = $db->fetch_array($res3);
					$status = explode(';',$avail['ledit']);
					
					$uRate = $_POST[$pField[$iii]][0];
					$oRate = $_POST[$oField[$iii]][0];
					
					$oRate =  ($uRate * $oRate) / 100;
					
					//$uAlot = $_POST['allotment'][0];
					$uAlot = 1;
					
					//if(isset($status[0]) and $status[0]=='1'){ $uAlot=$avail['lallotment']; }
					if(isset($status[0]) and $status[0]=='1'){ $uAlot=1; }
					if(isset($status[1]) and $status[1]=='1'){ $uRate=$avail['lrate']; }
					
					if($nnn>0){
						$str = "UPDATE lumonata_availability SET
								
								lrate=%s,
								lrate_published=%s,
								lcommision=%s,
								lstatus=%d,
								ledit=%s,
								lcreated_by=%s,
								lcreated_date=%d,
								lusername=%s,
								ldlu=%d,
								
								lcut_of_date=%d,
								lseason=%s			
								
								WHERE ldate=%d
								AND   lacco_id=%d
								AND   lacco_type_id=%d";
						$sql = $db->prepare_query($str, 
													 $uRate, $oRate, $_POST['commision'][0], $avail['lstatus'], $avail['ledit'], $avail['lcreated_by'], $avail['lcreated_date'], $avail['lusername'], time(),
													 $range['lcut_of_date'], $key1,
													 $i, $key2, $acco_type_id); 
					}else{
						$str = "INSERT INTO lumonata_availability(
								ldate, lacco_id, lacco_type_id,
								lallotment, lrate, lcommision, lstatus, ledit,
								lcreated_by, lcreated_date, lusername, ldlu,
								lcut_of_date, lseason, lrate_published
								) VALUES (
								%d, %d, %d, 
								%d, %s, %s, %d, %s,
								%s, %d, %s, %d,
								%d, %s, %s )";
						$sql = $db->prepare_query($str,
													 $i, $key2, $acco_type_id,
													 $uAlot, $uRate, $_POST['commision'][0], 0, '0;0',
													 'appsRateEdit', time(), $_SESSION['upd_by'], time(),
													 $range['lcut_of_date'], $key1, $oRate);
					}
					//echo $sql.'<br />';
					$res = $db->do_query($sql);
				}}
				$iii++;
			}	
			*/
		}
		
		function get_category_type_data($action,$id=0){
			global $db;
			$option = "";
			$list_category = $this->get_data('lumonata_rules',"where lrule='room_type' and lgroup='villas'");	
			while($data_category = $db->fetch_array($list_category)){
				$the_id = $data_category['lrule_id'];
				$category_name = $data_category['lname'];
				if($the_id==$id)$selected='selected="selected"';
				else $selected="";
				$option .= "<option value=\"$the_id\" $selected>$category_name</option>";
			}
			
			return $option;
		}
		
		function get_data($t,$w,$r='result_only'){
			require_once("../booking-engine/apps/general-func/class.general_func.php");
			$this->general_func = new general_func();
			return $this->general_func->data_tabels($t,$w,$r);
		}
		
		
		
		
		function get_category_type_data_old($action,$id=0){
			require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
			$this->list_category_type_villa = new list_category_type_villa();
			if($action=='get_list') return $this->list_category_type_villa->load($id);	
			else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
		}
		
		function merge_to_availabelity(){
			echo 'hohoh';
		}
		
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>