<?php  
class special_offer_category_edit extends db{
	function special_offer_category_edit($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Special Offer Category Edit");
		
	}
	function load(){
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		
		if(isset($_POST['prc']) && $_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}			
		$this->show_multiple_edit($t);
		
		$t->set_var('title', "Edit");			
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
		return $t->Parse('mBlock', 'mainBlock', false);
	}
	
	function show_multiple_edit($t){
		global $db;
		$query =  $db->prepare_query("SELECT * FROM lumonata_special_offer_categories WHERE lcat_id =%d",$_GET['act2']);
		$result = $db->do_query($query);
		$data = $db->fetch_array($result);
		
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('asc_id', $data['lcat_id']);
		$t->set_var('i', 0);	
		$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
		$t->set_var('name', $data['lcategory']);
		$t->set_var('description', $data['ldescription']);
		$t->Parse('eC', 'editContent', true); 
	}
	
	function get_parent($selected_parent=0,$index=0,$id_show=0){
		global $db;
		$str 	=  $db->prepare_query("select * from lumonata_rules where lgroup=%s",'additional_service');
		$result = $db->do_query($str);
		$parent = "<select name=\"parent[$index]\" class=\"big\">
						<option value=\"0\">Parent</option>";
		while($data= $db->fetch_array($result)){
			if($data['lrule_id']!=$id_show){
				if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
				else $select_info='';
				if($data['lparent']==0){
					$parent .= "<option $select_info value=".$data['lrule_id'].">".$data['lname']."</option>";
				}else{
					$level = $this->find_level($data['lrule_id']);
					$nbs = "";
					for($i=1;$i<=$level;$i++){
						$nbs .= "&nbsp;&nbsp;&nbsp;";
					}
					$parent .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";
				} 
			}
		}
		$parent .= "</select>";
		return $parent;
	}
	
	function edit_multiple_data($t){
		global $db;
		$msg = "";
		$error = 0;
		for($i=0;$i<count($_POST['asc_id']);$i++){
			$name = $_POST['name'][$i];
			$sef =  strtolower(str_replace(' ','-',$name));
			$desc =  $_POST['description'][$i];
			
			$str  = $db->prepare_query("update lumonata_special_offer_categories set
									 lcategory=%s,
									 lsef_url=%s,
									 ldescription=%s
									 where lcat_id=%d
									",$name,$sef,$desc,$_POST['asc_id'][$i]);
											
			$result = $db->do_query($str);						
			if($result)$msg .= "<li>Update special offer for \"".$_POST['name'][$i]."\" was successfully processed.</li>";
			else{
				$error = 1;
				$msg .= "<li>Update special offer for \"".$_POST['name'][$i]."\" was unsuccessfully processed.</li>";
			} 				
		}
		
		if($error==1){
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}else{
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}
	}
	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
		
}
?>