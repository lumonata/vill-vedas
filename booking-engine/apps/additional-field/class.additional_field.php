<?php 
	class additional_field extends db{
		var $appName;
		
		function additional_field($appName){
			$this->appName=$appName;
			$this->template_var="additional_field";
		}
		
		function load(){
			
		}
		
		function save_additional_field($app_id,$key,$value,$app_name){
			global $db;
			$str = $db->prepare_query("insert into lumonata_additional_fields 
									   (lapp_id,lkey,lvalue,lapp_name) 
									   values 
									   (%d,%s,%s,%s)",
									   $app_id,$key,$value,$app_name);					   
			$db->do_query($str);
		}
		
		function delete_additional_field($app_id,$app_name){
			global $db;
			$str = $db->prepare_query("delete from lumonata_additional_fields where
										lapp_id=%d and lapp_name=%s",
									   $app_id,$app_name);					   
			$db->do_query($str);
		}
		
		
		function save_rule_relationship($app_id,$rule_id){
			global $db;
			$str = $db->prepare_query("insert into lumonata_rule_relationship
									   (lapp_id,lrule_id,lorder_id) 
									   values 
									   (%d,%d,%d)",
									   $app_id,$rule_id,0);						   
			$db->do_query($str);
		}
		
		function update_rule_relationship($app_id,$rule_id_new, $rule_id_old){
			global $db;
			$str = $db->prepare_query("update lumonata_rule_relationship set
										lrule_id=%d
										where lapp_id=%d and lrule_id=%d",
									   $rule_id_new,$app_id,$rule_id_old);								   				   
			$db->do_query($str);
		}
		
		function delete_rule_relationship($app_id,$rule_id){
			global $db;
			$str = $db->prepare_query("delete from lumonata_rule_relationship 
										where lapp_id=%d and lrule_id=%d",
									   $app_id,$rule_id);			   
			$db->do_query($str);
		}
		
		
		
		
		
		function getTemplateVar(){
			return $this->template_var;
		}	
	}

?>