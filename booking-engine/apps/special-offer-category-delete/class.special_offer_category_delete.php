<?php 
class special_offer_category_delete extends db{
	function special_offer_category_delete($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Special Offer Category Delete");
	}
	
	function load(){
		global $db;
		$OUT_TEMPLATE="template.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
		//set block
		$t->set_block('home', 'mainBlock',  'mBlock');
		
		if(!empty($_POST['asc_id'])) $this->do_delete($t);
		
		$query =  $db->prepare_query("SELECT * FROM lumonata_special_offer_categories where lcat_id =%d",$_GET['act2']);
		$result = $db->do_query($query);
		$data = $db->fetch_array($result);
		$t->set_var('pilih',$_GET['act2']);
		$t->set_var('i', 0);	
		$t->set_var('asc_id', $data['lcat_id']);
		$t->set_var('name', $data['lcategory']);
		return $t->Parse('mBlock', 'mainBlock', false);
	}
	
	
	function do_delete($t){
		global $db;
		print_r($_POST);
		$str = $db->prepare_query("select * from lumonata_special_offer_categories where lcat_id=%d",$_POST['asc_id']);
		$result = $db->do_query($str);
		$data = $db->fetch_array($result);
		
		$t->set_var('name', $data['lcategory']);
		
		$str_delete = $db->prepare_query("delete from lumonata_special_offer_categories where lcat_id=%d",$_POST['asc_id']);
		$result_delete = $db->do_query($str_delete);
		if($result_delete) {
		    $t->set_var('msg_succsess', 'Special Offer Category was success deleted');
			$t->set_var('jsAction', 'notifBlockSaved();');	
			header("Location: http://".SUPPLIERPANEL_SITE_URL."/special-offer-category/");
		}else{
			$t->set_var('msg_succsess', 'Special Offer Category was failed deleted');
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}
		
		
	}
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
}
?>