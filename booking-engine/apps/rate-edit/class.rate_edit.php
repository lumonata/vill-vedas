<?php
	class rate_edit extends db{
		function rate_edit($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Rate Edit");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			if(!empty($_POST)){
				
				$low_additional = $_POST['low_additional'];
				$high_additional = $_POST['high_additional'];
				$peak_additional = $_POST['peak_additional'];
				
				if (isset($_POST['currency'])){$currency = $_POST['currency'];}else{$currency = '';}
					$stmt = $db->prepare_query("UPDATE lumonata_accommodation_rate_categories SET
						lacco_type_cat_id=%s,
						lcurrency=%s,
						llow_season=%s,
						lhigh_season=%s,
						lpeak_season=%s,
						
						llow_additional=%s,
						lhigh_additional=%s,
						lpeak_additional=%s,
						
						lbreakfast=%d,
						lallotment=%s,
						lcommision=%s,
						lusername=%s,
						ldlu=%d
						WHERE lrate_cat_id =%d",
						$_POST['acco_type_cat_id'],
						$currency,
						$_POST['low_season'],
						$_POST['high_season'],
						$_POST['peak_season'],
						
						$low_additional,
						$high_additional,
						$peak_additional,
						
						$_POST['breakfast'],
						$_POST['allotment'],
						$_POST['commision'],
						$_SESSION['upd_by'],
						time(),
						$_POST['rate_id']);
					$result = $db->do_query($stmt);
					
					$query = $db->prepare_query("SELECT 
						tc.lname as room_category
						FROM lumonata_accommodation_rate_categories rc
						LEFT JOIN lumonata_accommodation_type_categories tc On rc.lacco_type_cat_id  = tc.lacco_type_cat_id
						WHERE rc.lrate_cat_id =%d",$_POST['rate_id']);
					$result = $db->do_query($query);
					$data = $db->fetch_array($result);
					
					
					if ($result){
						$qd = $db->prepare_query("Delete From lumonata_accommodation_rate Where lrate_cat_id = %d",$_POST['rate_id']);
						$rd = $db->do_query($qd);						
						$this->syncronize_TableAvailability($_POST['acco_type_cat_id'],$_POST['low_season'],$_POST['high_season'],$_POST['peak_season'],
															$low_additional,$high_additional,$peak_additional);
						$t->set_var('jsAction', 'notifBlockSaved();');	
					}else{
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}
				
				
				
			}
			$i = 0;
			
			$query =  $db->prepare_query("SELECT *
				FROM lumonata_accommodation_rate_categories
				WHERE lrate_cat_id =%d",$_GET['act2']);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			if (isset($_POST['pilih'][$i])){
				$pilih = $_POST['pilih'][$i];
			}else{
				$pilih = '';
			}
			$t->set_var('pilih',$pilih);
			$t->set_var('i', $i);	
			$t->set_var('rate_id', $data['lrate_cat_id']);
			$t->set_var('currency', $data['lcurrency']);
			
			
			$t->set_var('low_season', $data['llow_season']);
			$t->set_var('high_season', $data['lhigh_season']);
			$t->set_var('peak_season', $data['lpeak_season']);
			
			$t->set_var('low_additional',  $data['llow_additional']);	 		 	
			$t->set_var('high_additional', $data['lhigh_additional']);
			$t->set_var('peak_additional', $data['lpeak_additional']);
			
			$t->set_var('allotment', $data['lallotment']);
			$min_commision = $this->globalAdmin->getSettingValue("min_commision","Global Setting");
			$t->set_var('min_commision',$min_commision);
			
			$t->set_var('commision', $data['lcommision']);
			if ($data['lbreakfast'] == 0){
				$t->set_var('check0', "checked=\"checked\"");
				$t->set_var('check1', "");
			}else{
				$t->set_var('check0', "");
				$t->set_var('check1', "checked=\"checked\"");
			}
		
			$t->set_var('room_category',$this->get_category_type_data('get_list',$data['lacco_type_cat_id']));
			
		
			return $t->Parse('mBlock', 'mainBlock', false);
		}
		
		function syncronize_TableAvailability($catroomtype_id,$low_season,$high_season,$peak_season,$low_additional,$high_additional,$peak_additional){
			require_once("../booking-engine/apps/rate/class.rate.php");
			$this->rate = new rate();
			$this->rate->sync_TableAvailability($catroomtype_id,$low_season,$high_season,$peak_season,$low_additional,$high_additional,$peak_additional);	
		}
		
		function get_category_type_data($action,$id=0){
			require_once("../booking-engine/apps/rate/class.rate.php");
			$this->rate = new rate();
			return $this->rate->get_category_type_data($action,$id);
		}
		
		
		
		function addTheNewRate($acco_type_id){
			
			global $db;
			//==============================================================================================
			//==============================================================================================
			//print_r($_POST);
			
			$season[] = 'Low Season';
			$season[] = 'High Season';
			$season[] = 'Peak Season';
			
			$pField[] = 'low_season';
			$pField[] = 'high_season';
			$pField[] = 'peak_season';
			
			$oField[] = 'low_additional';
			$oField[] = 'high_additional';
			$oField[] = 'peak_additional';

			$the24 = 24*60*60;
			$iii = 0;
							
			foreach($season as $key1){
				//echo $key1.'<br />';
				$str = "SELECT lacco_id FROM lumonata_accommodation_type WHERE lacco_type_id=%d";
				$sql = $db->prepare_query($str, $acco_type_id);
				$res1 = $db->do_query($sql);
				$rec = $db->fetch_array($res1);
				$key2 = $rec['lacco_id'];
				
				$str = "SELECT ldate_start, ldate_finish, lcut_of_date FROM lumonata_accommodation_season WHERE lacco_id=%d and lname=%s";
				$sql = $db->prepare_query($str, $key2, $key1);
				$res2 = $db->do_query($sql);
				//echo $sql.'<br />';
				
				while($range = $db->fetch_array($res2)){
				//print_r(date('j F Y',$range['ldate_start']). '-' . date('j F Y',$range['ldate_finish']) . '<br />');
				//echo '==============================<br />';
				for($i=$range['ldate_start']; $i<=$range['ldate_finish']; $i+=$the24){
					//echo 'a<br />';
					$str = "SELECT * FROM lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$sql = $db->prepare_query($str, $i, $key2, $acco_type_id);
					$res3 = $db->do_query($sql);
					$nnn = $db->num_rows($res3);
					$avail = $db->fetch_array($res3);
					$status = explode(';',$avail['ledit']);
					
					$uRate = $_POST[$pField[$iii]];
					$oRate = $_POST[$oField[$iii]];
					
					$oRate =  ($uRate * $oRate) / 100;
					
					//$uAlot = $_POST['allotment'];
					$uAlot = 1;
					
					if(isset($status[0]) and $status[0]=='1'){ $uAlot=$avail['lallotment']; }
					if(isset($status[1]) and $status[1]=='1'){ $uRate=$avail['lrate']; }
					
					if($nnn>0){
						$str = "UPDATE lumonata_availability SET
								
								lrate=%s,
								lrate_published=%s,
								lcommision=%s,
								lstatus=%d,
								ledit=%s,
								lcreated_by=%s,
								lcreated_date=%d,
								lusername=%s,
								ldlu=%d,
								
								lcut_of_date=%d,
								lseason=%s			
								
								WHERE ldate=%d
								AND   lacco_id=%d
								AND   lacco_type_id=%d";
						$sql = $db->prepare_query($str, 
													 $uRate, $oRate, $_POST['commision'], $avail['lstatus'], $avail['ledit'], $avail['lcreated_by'], $avail['lcreated_date'], $avail['lusername'], time(),
													 $range['lcut_of_date'], $key1,
													 $i, $key2, $acco_type_id); 
					}else{
						$str = "INSERT INTO lumonata_availability(
								ldate, lacco_id, lacco_type_id,
								lallotment, lrate, lcommision, lstatus, ledit,
								lcreated_by, lcreated_date, lusername, ldlu,
								lcut_of_date, lseason, lrate_published
								) VALUES (
								%d, %d, %d, 
								%d, %s, %s, %d, %s,
								%s, %d, %s, %d,
								%d, %s, %s )";
						$sql = $db->prepare_query($str,
													 $i, $key2, $acco_type_id,
													 $uAlot, $uRate, $_POST['commision'][0], 0, '0;0',
													 'appsRateEdit', time(), $_SESSION['upd_by'], time(),
													 $range['lcut_of_date'], $key1, $oRate);
					}
					//echo $sql.'<br />';
					$res = $db->do_query($sql);
				}}
				$iii++;
			}				
			//==============================================================================================
			//==============================================================================================
		}
		
		function get_list_villa(){
			require_once("../booking-engine/apps/list_villa/class.list_villa.php");
			$this->list_villa = new list_villa();
			return $this->list_villa->load();	
		}
		

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>