<?php
	/* supplier */
	class cancelation extends db{
		function cancelation($appName){
			$this->appName=$appName;
			$this->setMetaTitle("cancelation");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			//$this->to=$this->globalAdmin->getSettingValue('email');
			$this->to=$this->globalAdmin->getValueField("lumonata_meta_data", "lmeta_value", "lmeta_name", "email");
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function load(){
			global $db;
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_block('home', 'mainBlock', 'mBlock');
			
			$t->set_var('theSidebar', $this->findSidebar() );
			//$t->set_var('error',$error);
			
			
			
			if( !empty($_POST['yesThisOne']) ){
				$str = "UPDATE lumonata_accommodation_booking SET lstatus=3, lcancel_date=%d WHERE lbooking_id=%s AND lemail=%s";
				$sql = $db->prepare_query($str, time(), $_POST['yesID'], $_POST['yesMail']);
				$que = $db->do_query($sql);
				
				if($que){
					$t->set_var('jsAction', "jQuery('#bookDataCanceled').slideDown(300).delay(3000).slideUp(300);" );
					/* Send Email*/
					///*
					$this->sendTheEmail($_POST['yesID']);
					//*/
					//allotment update ============================================================
					//allotment update ============================================================
					$str = "SELECT * FROM lumonata_accommodation_booking WHERE lbooking_id=%s";
					$sql = $db->prepare_query($str, $_POST['yesID']);
					$que = $db->do_query($sql);
					$bookData = $db->fetch_array($que);
					
					if($bookData['lproduct_type']==1){
						$this->returnAllotment($bookData['lcheck_in'], $bookData['lcheck_out'], $bookData['lproduct_id'], $_POST['yesID']);
					}
					//allotment update ============================================================
					//allotment update ============================================================
				}
			}
			
			if( !empty($_POST['emailAddress']) && !empty($_POST['bookID']) ){
				$lbooking_id = $_POST['bookID'];
				$t->set_var('villa_reservation_details_admin_content', 
				"<div class=\"block_detail_villa_reservation_".$lbooking_id." block_detail_villa_reservation\" style=\"width: 730px; float:left;clear: both; margin: 10px 0 20px 0; \">".
				$this->villa_reservation_details_admin_content($lbooking_id,'')
				."</div>"
				);
				
				
				$str = "SELECT * FROM lumonata_accommodation_booking WHERE lbooking_id=%s AND lemail=%s AND lstatus!=3";
				$sql = $db->prepare_query($str, $_POST['bookID'], $_POST['emailAddress']);
				$que = $db->do_query($sql);
				$nRec = $db->num_rows($que);
				$content = $db->fetch_array($que);
				
				if($nRec>0){					
					$tmp = "SELECT lcountry FROM lumonata_country WHERE lcountry_id=%d";
					$sql = $db->prepare_query($tmp, $content['lcountry_id'] );
					$que = $db->do_query($sql);
					$rec = $db->fetch_array($que);
					$country = $rec['lcountry'];
					
					
					$tmpd = "SELECT * FROM lumonata_accommodation_booking_detail WHERE lbooking_id=%d";
					$sqld = $db->prepare_query($tmpd, $content['lbooking_id'] );
					$qued = $db->do_query($sqld);
					$recd = $db->fetch_array($qued);
					
					/*
					if($content['lproduct_type']==1){
						$tmp = "SELECT * FROM lumonata_accommodation WHERE lacco_id=%d";
					}else{
						$tmp = "SELECT * FROM lumonata_tour WHERE ltour_id=%d";
					}					
					$sql = $db->prepare_query($tmp, $content['lproduct_id'] );
					*/
					
					$tmp = "SELECT * FROM lumonata_accommodation_cancelationpolicy_categories WHERE ldate_from <= %d And ldate_to >%d";
					$sql = $db->prepare_query($tmp, $content['lcheck_in'], $content['lcheck_out'] );
					$que = $db->do_query($sql);
					$rec = $db->fetch_array($que);
					//$pName = $rec['lname'];
					
					
					
					if(!empty($rec['lcancellation_policy'])){
						$thePolicy = '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
	        						  <tr><td colspan="3" style="font-size:18px; font-weight:bold;" >Cancellation Policy</td></tr>
	        						  <tr><td colspan="3" >'.$rec['lcancellation_policy'].'</td></tr>';
						
						$t->set_var('thePolicy', $thePolicy );
					}
					
					
					$theRoomType = $this->globalAdmin->getValueField("lumonata_accommodation_type_categories","lname","lacco_type_cat_id",$recd['lacco_type_cat_id']);
					
					$t->set_var('theBookID', 		$content['lbooking_id'] );
					$t->set_var('theFullName', 		$content['lsalutation'].' '.$content['lfname'].' '.$content['llname'] );
					//$t->set_var('theProductName', 	$pName );
					$t->set_var('theRoomType', 		$theRoomType);
					$t->set_var('theCinDate', 		date('j F Y',$content['lcheck_in']) );
					$t->set_var('theCoutDate', 		date('j F Y',$content['lcheck_out']) );
					$t->set_var('theEmail', 		$content['lemail'] );
					$t->set_var('theCountry', 		$country );
					$t->set_var('theCity', 			$content['lcity'] );
					$t->set_var('theState', 		$content['lstate'] );
					$t->set_var('thePostal', 		$content['lzip_code'] );
					
					$t->set_var('thePrice', 		'USD '.number_format($content['ltotal'],2) );
					$t->set_var('theComm', 		    'USD '.number_format($content['lcommision'],2) );
					$t->set_var('thePriceAfter', 	'USD '.number_format($content['ltotal_after'],2) );
					
					$t->set_var('theEmail', 		$content['lemail'] );
					
					if($content['lproduct_type']==1){
						$t->set_var('theRoomDetails',   $this->theBookingDetails($content['lbooking_id']) );
					}else{
						$all = '<tr>
				        			<td>Number of Adult</td>
				        			<td>:</td>
				        			<td>'.$content['ladult'].'</td>
				        		</tr>
								<tr>
				        			<td>Number of Children</td>
				        			<td>:</td>
				        			<td>'.$content['lchild'].'</td>
				        		</tr>';
						$t->set_var('aditionalTourInfo', $all );
					}
					
					$sql = $db->prepare_query('SELECT * FROM lumonata_accommodation_booking_payment WHERE lbooking_id=%s', $content['lbooking_id'] );
					$que = $db->do_query($sql);
					$paymentDetails = $db->fetch_array($que);

					/*
					$t->set_var('cardType', 	$paymentDetails['lcard_type']); 
					$t->set_var('cardNumber', 	$paymentDetails['lcard_number']); 
					$t->set_var('cardName', 	$paymentDetails['lholder_name']); 
					$t->set_var('cardExpire', 	$paymentDetails['lexp_month'].'/'.$paymentDetails['lexp_year']); 
					$t->set_var('cardBank', 	$paymentDetails['lcard_bank']); 
					$t->set_var('cardCountry', 	$this->getCountryName($paymentDetails['lbank_country'])); 
					*/
					
					if (!empty($paymentDetails['first_name'])){
						$t->set_var('PDName', 	$paymentDetails['first_name']); 
						$t->set_var('PDPaid', 	'USD '.number_format($paymentDetails['lpaid'],2)); 
						$t->set_var('PDEmail', 	$paymentDetails['payer_email']); 
						$t->set_var('PDDate', 	date('d F Y',$paymentDetails['lcreated_date'])); 
					}else{
						$t->set_var('PDName', 	''); 
						$t->set_var('PDPaid', 	''); 
						$t->set_var('PDEmail', 	''); 
						$t->set_var('PDDate', 	''); 
					}
					
					$t->set_var('jsAction', "jQuery('#comfirmCancelBookingBox').slideDown(300);" );
					
				}else{
					$t->set_var('jsAction', "jQuery('#bookDataNotFound').slideDown(300).delay(3000).slideUp(300);" );
				}
			}
			
			
			return $t->Parse('mBlock', 'mainBlock', false);	
		}
		
		function theBookingDetails($theID){
			global $db;
			/*$tmp = "SELECT lumonata_accommodation_booking_detail.*, lumonata_accommodation_type.lname AS roomName
			        FROM lumonata_accommodation_booking_detail INNER JOIN lumonata_accommodation_type
			        ON lumonata_accommodation_booking_detail.lacco_type_id=lumonata_accommodation_type.lacco_type_id
			        WHERE lbooking_id=%s";
			*/
			$tmp = "SELECT * FROM lumonata_accommodation_booking_detail WHERE lbooking_id=%s";
			$sql = $db->prepare_query($tmp, $theID );
			$que = $db->do_query($sql);
			$all = '';
			$iii = 1;
			while($rec = $db->fetch_array($que)){
				$roomNameCat = $this->globalAdmin->getValueField("lumonata_accommodation_type_categories","lname","lacco_type_cat_id",$rec['lacco_type_cat_id']);
				$roomNameNum = $this->globalAdmin->getValueField("lumonata_accommodation_type","lnumber","lacco_type_id",$rec['lacco_type_id']);
				$roomName = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$rec['lacco_type_id']);
				$all    .= '<tr>
								<!--
			        			<td colspan="3" style="font-size:15px; font-weight:bold;">'.$iii.'. '.$roomNameCat.'</td>
			        			-->
			        			<td colspan="3" style="font-size:15px; font-weight:bold;">'.$roomNameCat.' ['.$roomNameNum.' '.$roomName.']</td>
			        		</tr>
			        		<!--
							<tr>
			        			<td>Name</td>
			        			<td>:</td>
			        			<td>'.$rec['lsalutation'].' '.$rec['lfname'].' '.$rec['llname'].'</td>
			        		</tr>
			        		
			        		
			        		<tr>
			        			<td>Address</td>
			        			<td>:</td>
			        			<td>'.$rec['laddress'].'</td>
			        		</tr>
			        		<tr>
			        			<td>City</td>
			        			<td>:</td>
			        			<td>'.$rec['lcity'].'</td>
			        		</tr>
			        		<tr>
			        			<td>State</td>
			        			<td>:</td>
			        			<td>'.$rec['lstate'].'</td>
			        		</tr>
			        		<tr>
			        			<td>Postal Code</td>
			        			<td>:</td>
			        			<td>'.$rec['lzip_code'].'</td>
			        		</tr>
			        	
			        		
			        		<tr>
			        			<td>Email</td>
			        			<td>:</td>
			        			<td>'.$rec['lemail'].'</td>
			        		</tr>
			        			-->
			        		<tr>
			        			<td>Rate</td>
			        			<td>:</td>
			        			<td>USD '.number_format($rec['lrate'],2).'</td>
			        		</tr>
			        		<tr>
			        			<td>Room Required</td>
			        			<td>:</td>
			        			<td>'.$rec['lroom'].'</td>
			        		</tr>
			        		<tr>
			        			<td>Sub Total</td>
			        			<td>:</td>
			        			<td>USD '.number_format($rec['ltotal'],2).'</td>
			        		</tr>
			        		<!--
			        		<tr>
			        			<td>Commision</td>
			        			<td>:</td>
			        			<td>'.number_format($rec['lcommision']).'%</td>
			        		</tr>
			        		<tr>
			        			<td>Commision Fee</td>
			        			<td>:</td>
			        			<td>USD '.number_format($rec['lcommision_fee'],2).'</td>
			        		</tr>
			        		
			        		<tr>
			        			<td>Additional Req</td>
			        			<td>:</td>
			        			<td>'.$rec['ladditional_request'].'</td>
			        		</tr>
			        		-->
			        		<tr>
			        			<td>&nbsp;</td>
			        			<td>&nbsp;</td>
			        			<td>&nbsp;</td>
			        		</tr>';
				$iii++;
			}
			$all = '<tr><td colspan="3" style="font-size:18px; font-weight:bold;">Villa Details</td></tr>'.$all;
			return $all;
		}
		
		function findSidebar(){
			global $db;
			
			if (isset($_POST['city'])){$city = $_POST['city'];}else{$city ='';}
			if (isset($_POST['in'])){$in = $_POST['in'];}else{$in ='';}
			if (isset($_POST['out'])){$out = $_POST['out'];}else{$out ='';}
			if (isset($_POST['star'])){$star = $_POST['star'];}else{$star ='';}
			if (isset($_POST['nRoom'])){$room = $_POST['nRoom'];}else{$room ='';}
			if (isset($_POST['hotelName'])){$name = $_POST['hotelName'];}else{$name ='';}

			$starSel ='';
			for($i=1;$i<=5;$i++){
				if($i==$star){
					$starSel .= '<option selected="selected" value="'.$i.'">'.$i.'</option>';
				}else{
					$starSel .= '<option value="'.$i.'">'.$i.'</option>';
				}
			}
			$all = '';
			if((isset($_GET['loc']) And $_GET['loc']=='hotel') || (isset($_GET['type']) And $_GET['type']=='hotel')){
				$all = '<div class="top">&nbsp;</div>
					<h3>Find Hotel</h3>
					<div class="content">
					<form id="findHotelForm" method="post" action="{http}{site_url}/accommodation/find/hotel/" >
					  <p>City <input type="text" name="city" value="'.$city.'" /></p>
					  
					  <p class="half"  style="position:relative;">Check In <input type="text" name="in" value="'.$in.'" />
					  	<img style="position:absolute; bottom:6px; left:30px; z-index:10; display:none" src="{http}{site_url}/templates/img/required-label.png" class="requiredLabel" />
					  </p>
					  <p class="half"  style="position:relative;">Check Out <input type="text" name="out" value="'.$out.'" />
					  	<img  style="position:absolute; bottom:6px; left:30px; z-index:10; display:none;" src="{http}{site_url}/templates/img/required-label.png" class="requiredLabel" />
					  </p>
					  
					  <p>Star Category
					    <select name="star">
					      <option value="">-select-</option>
					      '.$starSel.'
					    </select>
					  </p>
					  <p>Hotel Name <input type="text" name="hotelName" value="'.$name.'" /></p>
					</form>
					  <p><input rel="findHotelForm" type="submit" class="find" value="find" name="find" /></p>
					</div>
					<div class="bot">&nbsp;</div>';
			}else if((isset($_GET['loc']) And $_GET['loc']=='villa') || (isset($_GET['type']) And $_GET['type']=='villa')){
				$all = '<div class="top">&nbsp;</div>
					<h3>Find Villa</h3>
					<div class="content">
					<form id="findFillaForm" method="post" action="{http}{site_url}/accommodation/find/villa/" >
					  <p>City <input type="text" name="city" value="'.$city.'" /></p>
					  
					  <p class="half"  style="position:relative;">Check In <input type="text" name="in" value="'.$in.'" />
					  	<img style="position:absolute; bottom:6px; left:30px; z-index:10; display:none" src="{http}{site_url}/templates/img/required-label.png" class="requiredLabel" />
					  </p>
					  <p class="half"  style="position:relative;">Check Out <input type="text" name="out" value="'.$out.'" />
					  	<img  style="position:absolute; bottom:6px; left:30px; z-index:10; display:none;" src="{http}{site_url}/templates/img/required-label.png" class="requiredLabel" />
					  </p>
					  
					  <p>Number of Room 
					    <input type="text" style="width: 85px;" name="nRoom" value="'.$room.'" >
					  </p>
					  <p>Villa Name <input type="text" name="hotelName" value="'.$name.'" /></p>
					</form>
					  <p><input rel="findFillaForm" type="submit" class="find" value="find" name="find" /></p>
					</div>
					<div class="bot">&nbsp;</div>';
			}
			
			return $all;
		}

		
		function sendTheEmail($theID){
			global $db;
			
			$str = "SELECT * FROM lumonata_accommodation_booking WHERE lbooking_id=%s";
			$sql = $db->prepare_query($str, $theID);
			$que = $db->do_query($sql);
			$content = $db->fetch_array($que);
			$lgrand_total = $content['lgrand_total'];
			$lbooking_id = $theID;
			
			$tmp = "SELECT lcountry FROM lumonata_country WHERE lcountry_id=%d";
			$sql = $db->prepare_query($tmp, $content['lcountry_id'] );
			$que = $db->do_query($sql);
			$rec = $db->fetch_array($que);
			$country = $rec['lcountry'];
			
			if($content['lproduct_type']==1){
				$tmp = "SELECT lname FROM lumonata_accommodation WHERE lacco_id=%d";
			}else{
				$tmp = "SELECT lname FROM lumonata_tour WHERE ltour_id=%d";
			}					
			$sql = $db->prepare_query($tmp, $content['lproduct_id'] );
			$que = $db->do_query($sql);
			$rec = $db->fetch_array($que);
			$pName = $rec['lname'];
			
			
			if($content['lproduct_type']==1){
				/*$tmp = "SELECT lumonata_accommodation_booking_detail.*, lumonata_accommodation_type.lname AS roomName
				        FROM lumonata_accommodation_booking_detail INNER JOIN lumonata_accommodation_type
				        ON lumonata_accommodation_booking_detail.lacco_type_id=lumonata_accommodation_type.lacco_type_id
				        WHERE lbooking_id=%s";
				*/
				$tmp = "SELECT * FROM lumonata_accommodation_booking_detail WHERE lbooking_id=%s";        
				$sql = $db->prepare_query($tmp, $theID );
				$que = $db->do_query($sql);
				
				$all = '';
				$allNo = '';
				$iii = 1;
				while($rec = $db->fetch_array($que)){
					$roomName = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$rec['lacco_type_id']);
					$roomNameCat = $this->globalAdmin->getValueField("lumonata_accommodation_type_categories","lname","lacco_type_cat_id",$rec['lacco_type_cat_id']);
					$allNo    .= '<tr>
				        			<td colspan="3" style="font-size:15px; font-weight:bold;">'.$roomNameCat.'</td>
				        		</tr>
				        		<tr>
				        			<td>Name</td>
				        			<td>:</td>
				        			<td>'.$rec['lsalutation'].' '.$rec['lfname'].' '.$rec['llname'].'</td>
				        		</tr>
				        		
				        		<tr>
				        			<td>Address</td>
				        			<td>:</td>
				        			<td>'.$rec['laddress'].'</td>
				        		</tr>
				        		<tr>
				        			<td>City</td>
				        			<td>:</td>
				        			<td>'.$rec['lcity'].'</td>
				        		</tr>
				        		<tr>
				        			<td>State</td>
				        			<td>:</td>
				        			<td>'.$rec['lstate'].'</td>
				        		</tr>
				        		<tr>
				        			<td>Postal Code</td>
				        			<td>:</td>
				        			<td>'.$rec['lzip_code'].'</td>
				        		</tr>
				        		<tr>
				        			<td>Email</td>
				        			<td>:</td>
				        			<td>'.$rec['lemail'].'</td>
				        		</tr>
				        		
				        		
				        		<tr>
				        			<td>Rate</td>
				        			<td>:</td>
				        			<td>USD '.number_format($rec['lrate'],2).'</td>
				        		</tr>
				        		<tr>
				        			<td>Qty</td>
				        			<td>:</td>
				        			<td>'.$rec['lroom'].'</td>
				        		</tr>
				        		<tr>
				        			<td>Sub Total</td>
				        			<td>:</td>
				        			<td>USD '.number_format($rec['ltotal'],2).'</td>
				        		</tr>
				        		<tr>
				        			<td>&nbsp;</td>
				        			<td>&nbsp;</td>
				        			<td>&nbsp;</td>
				        		</tr>';
					$iii++;
				}
				
					
				$allNo = '<tr><td colspan="3" style="font-size:18px; font-weight:bold;">Villa Details</td></tr>'.$all;
			}else{
				$add = '<tr>
		        			<td>Number of Adult</td>
		        			<td>:</td>
		        			<td>'.$content['ladult'].'</td>
		        		</tr>
						<tr>
		        			<td>Number of Children</td>
		        			<td>:</td>
		        			<td>'.$content['lchild'].'</td>
		        		</tr>';
			}
			
			$tmp = "SELECT * FROM lumonata_accommodation_cancelationpolicy_categories WHERE ldate_from <= %d And ldate_to >%d";
			$sql = $db->prepare_query($tmp, $content['lcheck_in'], $content['lcheck_out'] );
			$que = $db->do_query($sql);
			$rec = $db->fetch_array($que);
			//$pName = $rec['lname'];
			
			
			
			if(!empty($rec['lcancellation_policy'])){
				$thePolicy = '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        					  <tr><td colspan="3" style="font-size:18px; font-weight:bold;" >Cancellation Policy</td></tr>
        					  <tr><td colspan="3" >'.$rec['lcancellation_policy'].'</td></tr>';
				
			}
			
			$allNo = '<h3 style="font-size:20px; padding:0 0 10px;">Booking Details</h3>
		        	<table class="delBookDetail">
		        		<tr>
		        			<td width="140">Booking ID</td><td>:</td><td>'.$content['lbooking_id'].'</td>
		        		</tr>
		        		<tr>
		        			<td>Full Name</td><td>:</td><td>'.$content['lsalutation'].' '.$content['lfname'].' '.$content['llname'].'</td>
		        		</tr>
		        		<tr>
		        			<td>Email</td><td>:</td><td>'.$content['lemail'].'</td>
		        		</tr>
		        		<tr>
		        			<td>Room Type</td><td>:</td><td>'.$roomNameCat.'</td>
		        		</tr>
		        		<tr>
		        			<td>Check In Date</td><td>:</td><td>'.date('j F Y',$content['lcheck_in']).'</td>
		        		</tr>
		        		<tr>
		        			<td>Check Out Date</td><td>:</td><td>'.date('j F Y',$content['lcheck_out']).'</td>
		        		</tr>
		        		<tr>
		        			<td>Country</td><td>:</td><td>'.$country.'</td>
		        		</tr>
		        		<tr>
		        			<td>City</td><td>:</td><td>'.$content['lcity'].'</td>
		        		</tr>
						<tr>
		        			<td>State/Province</td><td>:</td><td>'.$content['lstate'].'</td>
		        		</tr>
		        		<tr>
		        			<td>Postal Code</td><td>:</td><td>'.$content['lzip_code'].'</td>
		        		</tr>
		        		'.$thePolicy.'
		        	</table>';
			
			
			
			//Payment Detail 
			$query_p=$db->prepare_query("Select * From lumonata_accommodation_booking_payment Where lbooking_id=%s",$lbooking_id);
			$result_p=$db->do_query($query_p);
			$dp=$db->fetch_array($result_p);
			$jp=$db->num_rows($result_p);
			$style_display_tr_payment_required = 0;
			$style_display_tr_return_money = 0;
			if (!empty($jp)){
				$total_booking_price = $lgrand_total;
				$style_display_tr_return_money = 1;
			}else{
				$total_booking_price = 0;
				$style_display_tr_payment_required = 1;
			}
			
			//Cancelation Fee and Details
			$canceltation_fee = $this->get_canceltation_fee($lbooking_id);
			$administrator_fee = $this->globalAdmin->getValueField("lumonata_meta_data","lmeta_value","lmeta_name","administration_fee");
			$return_money = $total_booking_price - ($canceltation_fee["price"] + $administrator_fee);
			$payment_required = ($canceltation_fee["price"] + $administrator_fee);

			$total_booking_price = number_format($total_booking_price,2);
			$canceltation_fee = number_format($canceltation_fee["price"],2);
			$administrator_fee = number_format($administrator_fee,2);
			$return_money = number_format($return_money,2);
			$payment_required = number_format($payment_required,2);
			
			if ($style_display_tr_payment_required==1){
				$payment_PR = '<tr>
				        <td>Payment Required</td>
				        <td>:</td>
				        <td>USD. '.$payment_required.'</td>
				    </tr>';
			}else if ($style_display_tr_return_money==1){
				$payment_PR = '<tr>
				        <td>Returned Money</td>
				        <td>:</td>
				        <td>USD. '.$return_money.'</td>
				    </tr>';
			}
			
			
			$allNew='<table cellspacing="3" cellpadding="3" border="0">
					<tbody><tr>
				    	<td colspan="3"><h1 style="margin:0;padding:0">Booking Cancelation Details</h1></td>
				    </tr>
				    <tr>
				    	<td width="160">Booking ID</td>
				        <td width="10">:</td>
				        <td>'.$content['lbooking_id'].'</td>
				    </tr>
				    <tr>
				        <td>Total Booking Price</td>
				        <td>:</td>
				        <td>'.$total_booking_price.'</td>
				    </tr>
				    <tr>
				        <td>Cancelation Fee<br><span style="font-size:0.8em;font-style:italic"></span></td>
				        <td>:</td>
				        <td>'.$canceltation_fee.'</td>
				    </tr>
				    <tr>
				        <td>Administration Fee</td>
				        <td>:</td>
				        <td>'.$administrator_fee.'</td>
				    </tr>
				    '.$payment_PR.'
				    
				</tbody></table>';
			/*
			 '.$add.'
		        		<tr>
		        			<td>Total Price</td><td>:</td><td>'.$content['ltotal'].'</td>
		        		</tr>
		        		<tr>
		        			<td colspan="3">&nbsp;</td>
		        		</tr>
		        		'.$all.'
			 */
		    
		    $all = '<p>The following booking have been canceled :</p>'.$all.$allNew;   
		    $subject = '[ CANCEL BOOKING REPORT : '.$content['lbooking_id'].' ]';
		    $subject = '[ CANCEL BOOKING REPORT : '.$content['lbooking_id'].' ]'; 	 	
			//======================================================================================
			$tmp = "SELECT lemail FROM lumonata_members WHERE lproduct_type=%d AND lproduct_id=%d";
			$sql = $db->prepare_query($tmp, $content['lproduct_type'], $content['lproduct_id'] );
			$que = $db->do_query($sql);
			$rec = $db->fetch_array($que);		
			
			
			
			$admnMail = $this->to;
			$accoMail = $rec['lemail'];
			$custMail = $content['lemail'];
			
			$fromMail = $admnMail;
			$web_title = $this->globalAdmin->getValueField("lumonata_meta_data", "lmeta_value", "lmeta_name", "web_title");
			
			/*
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: ".$fromMail." \r\n";
			$headers .= "Cc: \r\n";
			$haders .= "Bcc: \r\n";
			*/
			
			$subject = "[$web_title] Booking Cancelation Details - Booking Id: ".$content['lbooking_id'];
			//$subject = "[$web_title] Cancel Booking Report - Booking Id: ".$content['lbooking_id'];
			//$name_client	= $data['lprefix'].' '.$data['lfirst_name'].' '.$data['llast_name'];
			//$email_client 	= $data['lemail'];
			
			$headers  = "From: $web_title <$admnMail>\r\n"; 
		    //$headers .= "Reply-To: $email_client\r\n"; 
		    //$headers .= "Return-Path: $sendToMail\r\n"; 
		    $headers .= "X-Mailer: Drupal\n"; 
		    $headers .= 'MIME-Version: 1.0' . "\n"; 
		    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			mail($admnMail,$subject,$all,$headers);
			//mail($accoMail,$subject,$all,$headers);
			mail($custMail,$subject,$all,$headers);
		}
		
		
		function returnAllotment($inDate, $outDate, $pID, $bookID){
			global $db;
			
			$str = "SELECT lacco_type_id, lroom FROM lumonata_accommodation_booking_detail WHERE lbooking_id=%s";
			$sql = $db->prepare_query($str, $bookID);
			$que = $db->do_query($sql);
			while($data = $db->fetch_array($que)){
				$str = "SELECT lallotment, ldate FROM lumonata_availability WHERE lacco_id=%d AND lacco_type_id=%d AND ldate>=%d AND ldate<=%d";
				$sql = $db->prepare_query($str, $pID, $data['lacco_type_id'], $inDate, $outDate);
				$res = $db->do_query($sql);
				while($allot = $db->fetch_array($res)){
					if($allot['ldate']==$outDate){
						/*
						$str = "UPDATE lumonata_availability SET lstatus=%d WHERE lacco_id=%d AND lacco_type_id=%d AND ldate=%d";
						$sql = $db->prepare_query($str, 0, $pID, $data['lacco_type_id'], $allot['ldate']);
						$re2 = $db->do_query($sql);
						*/
						$newAllotment = $allot['lallotment']+$data['lroom'];
						$str = "UPDATE lumonata_availability SET lallotment=%d, lstatus=%d WHERE lacco_id=%d AND lacco_type_id=%d AND ldate=%d";
						$sql = $db->prepare_query($str, $newAllotment, 0, $pID, $data['lacco_type_id'], $allot['ldate']);
						$re2 = $db->do_query($sql);
					}else{
						$newAllotment = $allot['lallotment']+$data['lroom'];
						$str = "UPDATE lumonata_availability SET lallotment=%d, lstatus=%d WHERE lacco_id=%d AND lacco_type_id=%d AND ldate=%d";
						$sql = $db->prepare_query($str, $newAllotment, 0, $pID, $data['lacco_type_id'], $allot['ldate']);
						$re2 = $db->do_query($sql);
					}
				}
			}
		}
		
		function getCountryName($id){
			global $db;
			
			$sql=$db->prepare_query("SELECT * FROM lumonata_country WHERE lcountry_id=%d",$id);
			$res=$db->do_query($sql);
			$rec=$db->fetch_array($res);
			return $rec['lcountry'];
		}
		
		
function villa_reservation_details_admin_content($booking_id,$send_status){
	global $db;
	$OUT_TEMPLATE="template_admin_villa_reservation_details.html";
	$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
	$t->set_file('home', $OUT_TEMPLATE);
	$t->set_block('home', 'Bvilla_reservation_details_admin', 'mBlock');
	
	$t->set_var('site_url_new',site_url());
	
	//global $db;
	//set_template(PLUGINS_PATH."/reservation/template_admin_villa_reservation_details.html",'the_villa_reservation_details_admin'.$booking_id);
	//add_block('Bvilla_reservation_details_admin','bvrda','the_villa_reservation_details_admin'.$booking_id);
	//$t->set_var('site_url',SITE_URL);
	//$t->set_var('template_url_imgs',"http://".TEMPLATE_URL."/images");
	//$t->set_var('url_action','http://'.SITE_URL.'/villa-reservation-details/#villa');
	
	
	
	
	if ($send_status='member'){
		$t->set_var('block_success','<div class="block_success" style="width:675px;  margin-bottom:35px;">
	    Thank you for confirm your reservation at Dipan Villas Rental. A confirmation has been sent to your email address.
	    </div>');
	}else{
		$t->set_var('block_success','');
	}
	
	
	/*
	if (isset($_SESSION['set_villa_reservation_id'])){
		$booking_id = $_SESSION['set_villa_reservation_id'];
		//unset($_SESSION['set_villa_reservation_id']);	
	}else{
		header('location: http://'.SITE_URL.'/availability/');
	}
	
	if (isset($_SESSION['villa_availability'])){
		//echo "M";
		$villa_availability = $_SESSION['villa_availability'];
		foreach ($villa_availability as $val){
			$acco_type_id = $val['acco_type_id'];
			
			if (isset($_SESSION['theAdditionalService'][$acco_type_id])){
				//echo "M";
				$additional_service = $_SESSION['theAdditionalService'][$acco_type_id];
				unset($_SESSION['theAdditionalService'][$acco_type_id]);
				unset($_SESSION['theAdditionalService']);
				//print_r($additional_service);
			}
			
			if (isset($_SESSION['theAdditionalServiceCategories'][$acco_type_id])){
				//echo "M";
				$theAdditionalServiceCategories = $_SESSION['theAdditionalServiceCategories'][$acco_type_id];
				unset($_SESSION['theAdditionalServiceCategories'][$acco_type_id]);
				unset($_SESSION['theAdditionalServiceCategories']);
				//print_r($theAdditionalServiceCategories);
			}
			
			if (isset($_SESSION['theTotAdditionalServicePrice'][$acco_type_id])){
				//echo "M";
				$theTotAdditionalServicePrice = $_SESSION['theTotAdditionalServicePrice'][$acco_type_id];
				unset($_SESSION['theTotAdditionalServicePrice'][$acco_type_id]);
				unset($_SESSION['theTotAdditionalServicePrice']);
				//print_r($theTotAdditionalServicePrice);
			}
			

		}
		unset($_SESSION['villa_availability']);
		unset($_SESSION['theAdditionalServiceCategories']);
		//print_r($villa_availability);
	}
	
	if (isset($_SESSION['booking'])){
		unset($_SESSION['booking']);
	}
	
	if (isset($_SESSION['searchKeys'])){
		unset($_SESSION['searchKeys']);
	}
	
	//print_r($_SESSION);
	*/
	 
	
	$query=$db->prepare_query("Select * From lumonata_accommodation_booking Where lbooking_id=%s",$booking_id);
	$result=$db->do_query($query);
	$val=$db->fetch_array($result);
	
	
		$lbooking_id = $val['lbooking_id'];
		$lcheck_in = $val['lcheck_in'];
		$lcheck_out = $val['lcheck_out']; 	
		$lsalutation = $val['lsalutation']; 	
		$lfname = $val['lfname']; 	
		$llname = $val['llname']; 	
		$lcountry_id = $val['lcountry_id']; 	
		$laddress = $val['laddress']; 	
		$lcity = $val['lcity']; 	
		$lstate = $val['lstate']; 	
		$lzip_code = $val['lzip_code']; 	
		$lphone = $val['lphone']; 	
		$lemail = $val['lemail']; 	
		$ladult = $val['ladult']; 	
		$lchild = $val['lchild']; 	
		$lstatus = $val['lstatus']; 
		$ltotal = $val['ltotal']; 
		$lcommision = $val['lcommision']; 	
		$ltotal_after = $val['ltotal_after']; 	
		$lagent_fee = $val['lagent_fee']; 	
		$luser_currency = $val['luser_currency']; 	
		$ltotal_usd = $val['ltotal_usd']; 	
		$lcommission_usd = $val['lcommission_usd']; 	
		$ltotal_after_usd = $val['ltotal_after_usd']; 	
		$lagent_fee_usd = $val['lagent_fee_usd']; 	
		$lsurecharge = $val['lsurecharge']; 	
		$ldiscount = $val['ldiscount']; 	
		$lhigh_season_surcharge = $val['lhigh_season_surcharge']; 	
		$lother_surcharge = $val['lother_surcharge']; 	
		$lsubtotal = $val['lsubtotal']; 	
		$ltax = $val['ltax']; 	
		$lgrand_total = $val['lgrand_total']; 	
		$lgrand_aud = $val['lgrand_aud']; 	
		$lagent_id = $val['lagent_id']; 
		$lcreated_by = $val['lcreated_by']; 	
		$lcreated_date = $val['lcreated_date'];
		$lusername = $val['lusername']; 	
		$ldlu = $val['ldlu'];
		$lagent_id = $val['lagent_id'];
		
		
		$query_d=$db->prepare_query("Select * From lumonata_accommodation_booking_detail Where lbooking_id=%s",$booking_id);
		$result_d=$db->do_query($query_d);
		$d=$db->fetch_array($result_d);
		$special_id = $d['lspecial_id'];
		$acco_type_cat_id = $d['lacco_type_cat_id'];

		$acco_type_id = $d['lacco_type_id'];
		$rate = $d['lrate'];
		$total = $d['ltotal'];
		$room = $d['lroom'];
		$additional_request= $d['ladditional_request'];
		
		$query_a=$db->prepare_query("Select * From lumonata_accommodation_type Where lacco_type_id=%d",$acco_type_id);
		$result_a=$db->do_query($query_a);
		$da=$db->fetch_array($result_a);
		$no_of_bedroom = $da['lno_of_bedroom'];
		
		
		if (!empty($special_id)){
			$d = $this->get_special_offer_detail($special_id);
	    	//$sRoomDefault = $d["d3"]['lno_of_bedroom'];
	    	$sRoomDefault = $d["d4"]['lno_of_bedroom'];
	    	//$so_title = $d["d2"]['lcategory'].' - '.$d["d3"]['lname'];
	    	$so_title = $d["d2"]['lcategory'].' - '.$d["d4"]['lname'];
	    	$villa_name = $da['lnumber'].' - '.$da['lname'];
	    	$villa_name = $so_title.'<br />'.$villa_name;
		}else{
			$villa_name = $da['lnumber'].' - '.$da['lname'];
		}
		
		
		//Personal Detail 
		if (empty($lagent_id)){
			$t->set_var('personal_or_owner','Personal');
		}else{			
			$t->set_var('personal_or_owner','Owner');
		}
		
		
		$t->set_var('full_name',$lsalutation.' '.$lfname.' '.$llname);
		$t->set_var('full_name',$lsalutation.' '.$lfname.' '.$llname);
		$t->set_var('email',$lemail);
		$t->set_var('phone',$lphone);
		$t->set_var('address',$laddress);
		$t->set_var('city',$lcity);
		$t->set_var('pos_code',$lzip_code);
		$country =  $this->globalAdmin->getValueField("lumonata_country", 'lcountry', 'lcountry_id', $lcountry_id);
		$t->set_var('country',$country);
		$t->set_var('state',$lstate);
		
		
		//Payment Detail 
		$query_p=$db->prepare_query("Select * From lumonata_accommodation_booking_payment Where lbooking_id=%s",$lbooking_id);
		$result_p=$db->do_query($query_p);
		$dp=$db->fetch_array($result_p);
		$jp=$db->num_rows($result_p);
		if (!empty($jp)){
			$t->set_var('first_name',$dp['first_name']);
			$t->set_var('paid', "USD ".number_format($dp['lpaid'],2));
			$t->set_var('payer_email',$dp['payer_email']);
			$t->set_var('payment_date',date("d F Y",$dp['lcreated_date']));
			$total_booking_price = $lgrand_total;
			$t->set_var('style_display_tr_payment_required','display: none;');
		}else{
			$t->set_var('first_name',"");
			$t->set_var('paid',"");
			$t->set_var('payer_email',"");
			$t->set_var('payment_date',"");
			$total_booking_price = 0;
			$t->set_var('style_display_tr_return_money','display: none;');
			$t->set_var('style_display_tr_cancel','display: none;');
		}
		
		//Cancelation Fee and Details
		
		$canceltation_fee = $this->get_canceltation_fee($lbooking_id);
		$administrator_fee = $this->globalAdmin->getValueField("lumonata_meta_data","lmeta_value","lmeta_name","administration_fee");
		$return_money = $total_booking_price - ($canceltation_fee["price"] + $administrator_fee);
		$payment_required = ($canceltation_fee["price"] + $administrator_fee);
		/*
		 echo "<br /> ". $canceltation_fee["unit"];
		 echo "<br /> ". $canceltation_fee["charge"];
		 echo "<br /> ". $canceltation_fee['nightBefore'];
		 echo "<br /> ". $canceltation_fee['night'];
		*/
		$t->set_var('total_booking_price',number_format($total_booking_price,2));
		$t->set_var('canceltation_fee',number_format($canceltation_fee["price"],2));
		$t->set_var('administrator_fee',number_format($administrator_fee,2));
		$t->set_var('return_money',number_format($return_money,2));
		$t->set_var('payment_required',number_format($payment_required,2));
		
		$tmp = "SELECT * FROM lumonata_accommodation_cancelationpolicy_categories WHERE ldate_from <= %d And ldate_to >%d And lacco_type_cat_id=%d";
		$sql = $db->prepare_query($tmp, $lcheck_in, $lcheck_out ,$acco_type_cat_id);
		$que = $db->do_query($sql);
		$rec = $db->fetch_array($que);
		//$pName = $rec['lname'];
		
		if(!empty($rec['lcancellation_policy'])){
			$t->set_var('cancellation_policy',$rec['lcancellation_policy']);
		}
		
		
		$t->set_var('booking_id', $lbooking_id);
		$t->set_var('booking_date', date("d F Y",$lcreated_date));
		$t->set_var('adult', $ladult);
		$t->set_var('child', $lchild);
		$t->set_var('additional_request', $additional_request);
		$night= ($lcheck_out - $lcheck_in)/ 86400;
		
		
		
		$t->set_var('villa_name', $villa_name);
		$t->set_var('check_in',date("d F Y",$lcheck_in));
		$t->set_var('check_out',date("d F Y",$lcheck_out));
		$t->set_var('total_cost',number_format($total,2));
		$t->set_var('night',$night);
		$t->set_var('qty', $room);
		

		$query_abd="Select sum(lprice * lqty) as theTotAdditionalServicePrice From lumonata_accommodation_booking_additionalservices Where lbooking_id='$booking_id' And lstatus=1";
		$result_abd=$db->do_query($query_abd);
		$d_abd=$db->fetch_array($result_abd);
		$theTotAdditionalServicePrice = $d_abd['theTotAdditionalServicePrice'];
		

		$the_total_rate = $ltotal;
		$the_subtotal = $lsubtotal;
		$the_grandtotal = $lgrand_total;
		
		$t->set_var('theGrandTotal',number_format($the_grandtotal,2));
		$t->set_var('theTotal',number_format($the_total_rate,2));
		
		$the_high_season_surcharge = $lhigh_season_surcharge;
		if ($the_high_season_surcharge != 0){
			$the_high_season_surcharge_text = $this->globalAdmin->getValueField("lumonata_accommodation_surecharge_description","ldescription","lsurecharge_desc_ID",2);
			$t->set_var('theSurchargeHighSeason','
			<tr>
				<td style="text-align:right; padding:2px 10px 2px 0; " colspan="7">
				'.$the_high_season_surcharge_text.': <span style="font-size:14px; " id="theSurchargeHighSeason"> USD. '.number_format($the_high_season_surcharge,2).'</span>
				</td>
			</tr>');
			
		}else{
			$t->set_var('theSurchargeHighSeason',"");
		}
		
		
		$the_tax_val = $ltax;
		if ($the_tax_val != 0){
			$the_tax_text = $this->globalAdmin->getValueField("lumonata_accommodation_surecharge_description","ldescription","lsurecharge_desc_ID",1); 
			
			$t->set_var('theSurchargeTax','
			<tr>
				<td style="text-align:right; padding:2px 10px 2px 0; " colspan="7">
				'.$the_tax_text.': <span style="font-size:14px; " id="theSurchargeTax"> USD. '.number_format($the_tax_val,2).'</span>
				</td>
			</tr>');
		}else{
			$t->set_var('theSurchargeTax',"");
		}
		
		$the_other_surcharge = $lother_surcharge;
		if ($the_other_surcharge != 0){
			$t->set_var('theSurchargeOther','
			<tr>
				<td style="text-align:right; padding:2px 10px 2px 0; " colspan="7">
				Surcharge: <span style="font-size:14px; " id="theSurchargeOther"> USD. '.number_format($the_other_surcharge,2).'</span>
				</td>
			</tr>');
		}else{
			$t->set_var('theSurchargeOther',"");
		}
		
		$the_promo = $ldiscount;
		if ($the_promo != 0){
			$t->set_var('theDiscount','
			<tr>
				<td style="text-align:right; padding:2px 10px 2px 0; " colspan="7">
				Discount: <span style="font-size:14px; " id="theDiscount"> USD. -'.number_format($the_promo,2).'</span>
				</td>
			</tr>');
		}else{
			$t->set_var('theDiscount',"");
		}
		
		
		if ($the_high_season_surcharge != 0 || $the_other_surcharge != 0 || $the_promo != 0){
			
			if ($theTotAdditionalServicePrice){
				$t->set_var('theSubTotal','
				<tr>
					<td style="text-align:right; padding:2px 10px 2px 0; " colspan="4">
					</td>
					<td style="text-align:right; padding:2px 10px 2px 0; " colspan="3">
					<span style="border-top: 1px solid #BBBBBB; width:100%; float:right;">
					Sub Total: <span style="font-size:14px; "  id="theSubTotal"> USD. '.number_format($the_subtotal,2).'</span>
					</span>
					</td>
				</tr>');
				$t->set_var('style_display_tr','');
			}else{
				$t->set_var('theSubTotal','
				<tr>
					<td style="text-align:right; padding:2px 10px 2px 0; " colspan="6">
					</td>
					<td style="text-align:right; padding:2px 10px 2px 0; width: 250px;" colspan="1">
					<p style="border-top: 1px solid #EEEEEE;">
					Sub Total: <span style="font-size:14px; "  id="theSubTotal"> USD. '.number_format($the_subtotal,2).'</span>
					</p>
					</td>
				</tr>');
				$t->set_var('style_display_tr','display:none;');
			}
			
			//$t->set_var('additional_service_categories_selected_detail_admin',additional_service_categories_selected_detail_admin($booking_id));
			
		}else{
			$t->set_var('theSubTotal',"");
			$t->set_var('the_subtotal',0);
		}
		
		if ($special_id){
			$t->set_var('special_offer_additional_services_includes_admin',$this->special_offer_additional_services_includes_admin($special_id));
		}
		$t->set_var('additional_service_categories_selected_detail_admin',$this->additional_service_categories_selected_detail_admin($booking_id));
	
	return $t->Parse('mBlock', 'Bvilla_reservation_details_admin', false);	
}


function additional_service_categories_selected_detail_admin($booking_id){
	global $db;
	$addnional_service_list='';
	//echo '<br />'.
	$query=$db->prepare_query("Select * From lumonata_rules 
	Where lgroup=%s 
	And lparent=%d
	Order by lorder",'additional_service',0);
	$result=$db->do_query($query);
	$no=0;
	$nox=0;
	$style_display_tr = '';
	while ($data=$db->fetch_array($result)){
		//echo '<br />&nbsp&nbsp'.
		$rule_id=$data['lrule_id'];
		$pure_spa = false;
		if ($rule_id==13){
			$pure_spa = true;
		}
				
		if ($nox%2==0){
			$style_background='';
			
		}else{
			
			$style_background='background-color: #f0f0f0;';

		}
		
		//echo '<br />'.$rule_id.
		$query_cek = $db->prepare_query("Select * 
		From lumonata_accommodation_booking_additionalservices aba
		Inner Join lumonata_rule_relationship rr On rr.lapp_id =aba.lpackage_id
		Where aba.lbooking_id=%s And rr.lrule_id=%d
		And lstatus=%d",$booking_id,$rule_id,1);
		$result_cek = $db->do_query($query_cek);
		$jum_cek = $db->num_rows($result_cek);
		
		
		$acco_type_id = 0;
		if ($jum_cek > 0){
			//echo '<br />'.
			$categorie_name=$data['lname'];
			$style_display_tr = '';
			$nox++;
		}else{
			$categorie_name='';
			$style_display_tr = 'display:none;';
		}
		
		$addnional_service_list .='
			<tr style="'.$style_display_tr.'">
				<td class="td_line_bottom" colspan="7" style="'.$style_background.'"> 
				</td>
			</tr>';
		$addnional_service_list .='
			<tr style="'.$style_display_tr.'">
				<td colspan="7" class="td_additional_title" style="'.$style_background.'">
					<b>'.$categorie_name.'</b>
				</td>
			</tr>  
		';
		
		$query1=$db->prepare_query("Select * From lumonata_rules 
		Where lgroup=%s 
		And lparent=%d
		Order by lorder
		",'additional_service',$data['lrule_id']);
		$result1=$db->do_query($query1);
		$jum1=$db->num_rows($result1);
		if (!empty($jum1)){
			while ($data1=$db->fetch_array($result1)){
				//echo '<br />&nbsp&nbsp'.
				$categorie_name1=$data1['lname'];
				
				
         		
				$query_rr=$db->prepare_query("Select * 
				From lumonata_rule_relationship rr
				Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
				Where rr.lrule_id=%d
				Order by a.lorder
				",$data1['lrule_id']);
				$result_rr=$db->do_query($query_rr);
				$noo = 0;
				$style_display_tr_2 = '';
				while ($data_rr=$db->fetch_array($result_rr)){
					if ($noo==0){
						$style_border_top = '';
					}else{
						//$style_border_top ='border-top: 1px dashed #444444;';
						$style_border_top = '';	
					}
					$rule_id = $data1['lrule_id'];
					
					//echo '<br />'.$rule_id.
					$query_cek = $db->prepare_query("Select * 
					From lumonata_accommodation_booking_additionalservices aba
					Inner Join lumonata_rule_relationship rr On rr.lapp_id =aba.lpackage_id
					Where aba.lbooking_id=%s And rr.lrule_id=%d
					And lstatus=%d",$booking_id,$rule_id,1);
					$result_cek = $db->do_query($query_cek);
					$jum_cek = $db->num_rows($result_cek);
					
					if ($jum_cek > 0){
						//echo '<br />'.
						$categorie_name=$data1['lname'];
						$style_display_tr_2 = '';
					}else{
						//echo '<br />M';
						$categorie_name='';
						$style_display_tr_2 = 'display:none;';
					}
					
					$article_id = $data_rr['lapp_id'];
					$article_content = $data_rr['larticle_content'];
					//echo '<br />'.
					$query_a=$db->prepare_query("Select a.* From lumonata_articles a
					Inner Join lumonata_additional_fields af On a.larticle_id = af.lapp_id 
					Where a.larticle_id=%d 
					And a.larticle_status=%s
					And af.lvalue<>%s
					Order by lorder",$article_id,'publish','');
					$result_a=$db->do_query($query_a);
					$jum_a=$db->num_rows($result_a);
					
					if (!empty($jum_a)){
						$data_a=$db->fetch_array($result_a);
						//echo '<br />&nbsp&nbsp&nbsp&nbsp&nbsp'.
						$article_id = $data_a['larticle_id'];
						$article_title=$data_a['larticle_title'];
						
						if ($pure_spa){
							//echo '<br />&nbsp&nbsp&nbsp&nbsp&nbsp'.
							$price_temp = get_additional_field($data_a['larticle_id'],'additional_service_pure_spa_price','additional_service');
							$time_temp = get_additional_field($data_a['larticle_id'],'additional_service_pure_spa_time','additional_service');
							
							$exp = explode(';', $price_temp);
							$ext = explode(';', $time_temp);
							$jum_exp = count($exp);
							
							
							$detail_package_spa = '';
							if ($jum_exp > 1){
								$jum_exp = $jum_exp - 1;
								$ii = 0;
								$iii = 0;
								//echo '<br />'.
								$query_cek = $db->prepare_query("Select * 
								From lumonata_accommodation_booking_additionalservices aba
								Where aba.lbooking_id=%s And aba.lpackage_id=%d
								And lstatus=%d",$booking_id,$article_id,1);
								$result_cek = $db->do_query($query_cek);
								$jum_cek = $db->num_rows($result_cek);
								while ($data_cek=$db->fetch_array($result_cek)){
									//echo "M";
									$theAdditionalService = $data_cek['lprice'];
									$the_qty = $data_cek['lqty'];
									$the_price = $data_cek['lprice'];
									$the_duration = $data_cek['lduration'];
									$the_day = $data_cek['llday'];
									$the_tot_price = $the_price * $the_qty;
									
									$display_show = 'display: block;';
									$checked_show = 'checked="checked"';
									if (!empty($the_duration)){
										$price_show = ''.$the_duration.' - '.number_format($the_price,2).'';	
									}else{
										$price_show = number_format($the_price,2).'';
									}
									$total_price_show = ''.number_format($the_tot_price,2).'';
									
									if ($article_id==119){
										$day = '<input type="text" style="'.$display_show.' width: 30px; text-align: center; float: left;" value="'.$the_day.'" name="day_'.$data_rr['lrule_id'].'_'.$article_id.'_'.$ii.'" rel="'.$data_rr['lrule_id'].'_'.$article_id.'_'.$ii.'" class="text day">';
									}
									
									if ($iii%2==0){
										$style_border_top_2 = '';
									}else{
										//$style_border_top_2 ='border-top: 1px dashed #444444;';	
										$style_border_top_2 = '';
									}
									
									$detail_package_spa .='
					         		<tr style="'.$style_display_tr_2.'">
							          	<td class="td_checkbox">
							          	</td>
							          	<td class="td_price" style="'.$style_border_top_2.'">
							          	'.$price_show.'
							          	</td>
							          	<td class="td_qty" style="'.$style_border_top_2.'">
							         		'.$the_qty.'
							          	</td>
							          	<td class="td_day" style="'.$style_border_top_2.'">
							         		'.$day.'
							          	</td>
							          	<td class="td_tot_price" style="'.$style_border_top_2.'">
							          		<span class="Spa" id="total_price_'.$data_rr['lrule_id'].'_'.$article_id.'_'.$ii.'">
							          		'.$total_price_show.'
							          		</span>
							          	</td>
									</tr>
									';
									
									$iii++;
								}
							}
							$detail_package = $detail_package_spa;
							//echo '<br />'.$iii;
							if ($iii >= 1){
								$addnional_service_list .='
									<tr style="'.$style_display_tr_2.'">
										<td colspan="2" class="td_package_title" style="'.$style_background.' '.$style_border_top.'">

											'.$article_title.'

										</td>
										<td colspan="5" class="td_colspan_5" style="'.$style_background.' '.$style_border_top.'">
								         	<table cellpadding="0" cellspacing="0" style="width: 100%;">
								         		'.$detail_package.'
								          	</table>
										</td>
									</tr>
									<tr>
										<td colspan="7" valign="top" style="text-align:left; padding:0px 0 0px 10px; '.$style_background.'">
											<div style="display: none; width: 630px; padding-bottom: 15px;" id="block_description_'.$data1['lrule_id'].'_'.$article_id.'" class="blockDescriptionPackage">
								         	'.$article_content.'
								         	</div>
										</td>
									</tr>
								';
							}
							
						}else{
							$addnional_service_list_tr = false;
							$day = '';
							$additional_service_price = get_additional_field($data_a['larticle_id'],'additional_service_price','additional_service');
							
							//echo '<br />'.
							$query_cek = $db->prepare_query("Select * 
							From lumonata_accommodation_booking_additionalservices aba
							Where aba.lbooking_id=%s And aba.lpackage_id=%d
							And lstatus=%d",$booking_id,$article_id,1);
							$result_cek = $db->do_query($query_cek);
							$jum_cek = $db->num_rows($result_cek);
							if ($jum_cek > 0){
								$data_cek=$db->fetch_array($result_cek);
								//echo "M";
								$theAdditionalService = $data_cek['lprice'];
								$the_qty = $data_cek['lqty'];
								$the_price = $data_cek['lprice'];
								$the_duration = $data_cek['lduration'];
								$the_day = $data_cek['llday'];
								$the_tot_price = $the_price * $the_qty;
								
								$display_show = 'display: block;';
								$checked_show = 'checked="checked"';
								if (!empty($the_duration)){
									$price_show = ''.$the_duration.' - '.number_format($the_price,2).'';	
								}else{
									$price_show = number_format($the_price,2).'';
								}
								$total_price_show = ''.number_format($the_tot_price,2).'';
							
								if ($article_id==119){
									$day = '<input type="text" style="'.$display_show.' width: 30px; text-align: center; float: left;" value="'.$the_day.'" name="day_'.$data_rr['lrule_id'].'_'.$article_id.'" rel="'.$data_rr['lrule_id'].'_'.$article_id.'" class="text day">';
								}
								
								$detail_package='
					         		<tr style="'.$style_display_tr_2.'">
							          	<td class="td_checkbox" >
							         		<input '.$checked_show.' type="checkbox" rel="'.$data_rr['lrule_id'].'_'.$article_id.'" class="checkbox">
							          	</td>
							          	<td class="td_price">
							          		'.$price_show.'
							          	</td>
							          	<td class="td_qty">
							         		<input rel="'.$data_rr['lrule_id'].'_'.$article_id.'" type="text" style="'.$display_show.' width: 30px; text-align: center; float: left;" value="'.$the_qty.'" name="qty_'.$data_rr['lrule_id'].'_'.$article_id.'" class="text quantity">
							          	</td>
							          	<td class="td_day">
							         		'.$day.'
							          	</td>
							          	<td class="td_tot_price">
							          		<span class="Spa" id="total_price_'.$data_rr['lrule_id'].'_'.$article_id.'" style="display: none;">
							          		'.$total_price_show.'
							          		</span>
							          	</td>
									</tr>
									';
								
								$detail_package = $detail_package;
								$addnional_service_list_tr = true;
							}
							
							if ($addnional_service_list_tr){
								$addnional_service_list .='
									<tr style="'.$style_display_tr_2.'">
										<td colspan="2" class="td_package_title" style="'.$style_background.' '.$style_border_top.'">

											'.$article_title.'

										</td>
										<td colspan="5" class="td_colspan_5" style="'.$style_background.' '.$style_border_top.'">
								         	<table cellpadding="0" cellspacing="0" style="width: 100%;">
								         		'.$detail_package.'
								          	</table>
										</td>
									</tr>
									<tr>
										<td colspan="7" valign="top" style="text-align:left; padding:0px 0 0px 10px; '.$style_background.'">
											<div style="display: none; width: 630px; padding-bottom: 15px;" id="block_description_'.$data1['lrule_id'].'_'.$article_id.'" class="blockDescriptionPackage">
								         	'.$article_content.'
								         	</div>
										</td>
									</tr>
								';
							}
						}
						
						
						
					}
					
					$noo++;
				}
				
				
				
			}
		}else{
				//echo "M";
				$query_rr=$db->prepare_query("Select * 
				From lumonata_rule_relationship rr
				Inner Join lumonata_articles a On rr.lapp_id = a.larticle_id
				Where rr.lrule_id=%d
				Order by a.lorder
				",$data['lrule_id']);
				$result_rr=$db->do_query($query_rr);
				$noo=0;
				while ($data_rr=$db->fetch_array($result_rr)){
					//echo '<br />'.$noo;
					if ($noo==0){
						$style_border_top = '';
					}else{
						//$style_border_top ='border-top: 1px dashed #444444;';
						$style_border_top = '';	
					}
					
					$article_id = $data_rr['lapp_id'];
					$article_content = $data_rr['larticle_content'];
					$query_a=$db->prepare_query("Select a.* From lumonata_articles a
					Inner Join lumonata_additional_fields af On a.larticle_id = af.lapp_id 
					Where a.larticle_id=%d 
					And a.larticle_status=%s
					And af.lvalue<>%s
					Order by lorder",$article_id,'publish','');
					$result_a=$db->do_query($query_a);
					$jum_a=$db->num_rows($result_a);
					if (!empty($jum_a)){
						$data_a=$db->fetch_array($result_a);
						//echo '<br />&nbsp&nbsp&nbsp&nbsp&nbsp'.
						$article_title=$data_a['larticle_title'];
						$additional_service_price = get_additional_field($data_a['larticle_id'],'additional_service_price','additional_service');
						$article_title=$data_a['larticle_title'];
						$day = '';
						
						
						//echo '<br />'.
						$query_cek = $db->prepare_query("Select * 
						From lumonata_accommodation_booking_additionalservices aba
						Where aba.lbooking_id=%s And aba.lpackage_id=%d 
						And lstatus=%d",$booking_id,$article_id,1);
						$result_cek = $db->do_query($query_cek);
						$jum_cek = $db->num_rows($result_cek);
						
						if ($jum_cek > 0){
								$data_cek=$db->fetch_array($result_cek);
							
								$theAdditionalService = $data_cek['lprice'];
								$the_qty = $data_cek['lqty'];
								$the_price = $data_cek['lprice'];
								$the_duration = $data_cek['lduration'];
								$the_day = $data_cek['llday'];
								$the_tot_price = $the_price * $the_qty;

								$display_show = 'display: block;';
								$checked_show = 'checked="checked"';
								if (!empty($the_duration)){
									$price_show = ''.$the_duration.' - '.number_format($the_price,2).'';	
								}else{
									$price_show = number_format($the_price,2).'';
								}
								$total_price_show = ''.number_format($the_tot_price,2).'';
								
								
							if ($article_id==119){
								$day = '
								'.$the_day.'
								';
							}
							
							$addnional_service_list .='
								<tr>
									<td colspan="2" class="td_package_title" style="'.$style_background.' '.$style_border_top.'">
										
										'.$article_title.'

									</td>
									<td colspan="5" class="td_colspan_5" style="'.$style_background.' '.$style_border_top.'">
							         	<table cellpadding="0" cellspacing="0" style="width: 100%;">
							         		<tr>
									          	<td class="td_checkbox" >
									         		<!--input '.$checked_show.' type="checkbox" rel="'.$data['lrule_id'].'_'.$article_id.'" class="checkbox"-->
									          	</td>
									          	<td class="td_price">
									          		'.$price_show.'
									          	</td>
									          	<td class="td_qty">
									         		<!--input rel="'.$data['lrule_id'].'_'.$article_id.'" type="text" style="'.$display_show.' width: 30px; text-align: center; float: left;" value="'.$the_qty.'" name="qty_'.$data['lrule_id'].'_'.$article_id.'" class="text quantity"-->
									         		'.$the_qty.'
									          	</td>
									          	<td class="td_day">
									         		'.$day.'
									          	</td>
									          	<td class="td_tot_price">
									          		<!--input type="text" class="text theAdditionalService_'.$data['lrule_id'].'_'.$article_id.' theAdditionalService"-->
									          		<span class="Spa" id="total_price_'.$data['lrule_id'].'_'.$article_id.'" >
									          		'.$total_price_show.'
									          		</span>
									          	</td>
											</tr>
							          	</table>
									</td>
								</tr>
								<tr>
									<td colspan="7" valign="top" style="text-align:left; padding:0px 0 0px 10px; '.$style_background.'">
										<div style="display: none; width: 630px; padding-bottom: 15px" id="block_description_'.$data['lrule_id'].'_'.$article_id.'" class="blockDescriptionPackage">
							         	'.$article_content.'
							         	</div>
									</td>
								</tr>
							';
						}
						
						
					}
					
					$noo++;
				}
			$addnional_service_list .='
			<tr style="'.$style_display_tr.'">
				<td  colspan="7" style="'.$style_background.' padding-top: 10px;"> 
				</td>
			</tr>';
		}

		$no++;
	}
	
	return $addnional_service_list;
	
}


function special_offer_additional_services_includes_admin($special_id){
	global $db;
	$query = $db->prepare_query("Select * From lumonata_special_offer Where lspecial_id=%d",$special_id);
	$result = $db->do_query($query);
	$jum = $db->num_rows($result);
	if (!empty($jum)){
		///*
		$all_rate_include = '';
		
		//*/
		
		$query2 = $db->prepare_query("Select * From lumonata_special_offer_additional_services lsoas
		Inner Join lumonata_articles la On lsoas.lpackage_id = la.larticle_id
		Where lspecial_id=%d",$special_id);
		$result2 = $db->do_query($query2);
		$jum2 = $db->num_rows($result2);
		if(!empty($jum2)){
			$all_rate_include .= '
			<div class="blockReservation" style="float: left;">
				<table cellspacing="0" cellpadding="0" style="width: 100%;">
				<th style="text-align:left; padding-bottom:10px; padding-left:10px; width:150px;padding: 5px 10px;
	    text-align: left;
	    width: 150px;">
					All Rates Include
				</th>
				<tr>
					<td style="background-color: #1F1C14;" colspan="7" class="td_line_bottom"> 
					</td>
				</tr>
				<tr>
					<td class="td_additional_title" colspan="7">
						<ul style="margin-left: 20px; padding:0px;">
			';
	
				while ($data=$db->fetch_array($result2)){
					$all_rate_include .= '<li style="list-style: square;">'.$data['larticle_title'].'</li>';
					
				}
			$all_rate_include .= '
					</ul>
					</td>
				</tr>
				</table>
			</div>';
		}
		///*
		
		//*/
		return $all_rate_include;
	}
	
}



function get_special_offer_detail($special_id){
	//$special_id = $_POST['special_id'];
	$table = "lumonata_special_offer";
    $condition = "Where lspecial_id = $special_id";
    $d = $this->get_field_costum_table($table, $condition);
    
    $table2 = "lumonata_special_offer_categories";
    $condition2 = "Where lcat_id = $d[lcat_id]";
    $d2 = $this->get_field_costum_table($table2, $condition2);
    
    $table3 = "lumonata_accommodation_type";
    $condition3 = "Where lacco_type_id = $d[lacco_id]";
    $d3 = $this->get_field_costum_table($table3, $condition3);
    
    $table4 = "lumonata_accommodation_type_categories";
    $condition4 = "Where lacco_type_cat_id = $d[lacco_id]";
    $d4 = $this->get_field_costum_table($table4, $condition4);
    
    $d["d"] = $d;
	$d["d2"] = $d2;
	$d["d3"] = $d3;
	$d["d4"] = $d4;
	
    return $d;
}

function get_field_costum_table($table,$condition){
	//defined database class to global variable
	global $db;
	//echo "<br />". 
	$sql=$db->prepare_query("SELECT *  
				FROM $table 
				$condition
				");
	$r=$db->do_query($sql);
	$d=$db->fetch_array($r);
	
	return $d;
}


function get_canceltation_fee($booking_id){
	global $db;
	$q=$db->prepare_query("Select * From lumonata_accommodation_booking Where lbooking_id=%s",$booking_id);
	$r=$db->do_query($q);
	$f=$db->fetch_array($r);
	$check_in = strtotime(date('d F Y',$f['lcheck_in']));
	$check_out = strtotime(date('d F Y',$f['lcheck_out']));
	$grand_total = $f['lgrand_total'];
	$night = ($check_out - $check_in)/ 86400;
	$date_now = strtotime(date('d M Y',time()));
	$nightBefore= ($check_in - $date_now)/ 86400;
	
	$query_d=$db->prepare_query("Select * From lumonata_accommodation_booking_detail Where lbooking_id=%s",$booking_id);
	$result_d=$db->do_query($query_d);
	$d=$db->fetch_array($result_d);
	$acco_type_id = $d['lacco_type_id'];
	$acco_type_cat_id = $d['lacco_type_cat_id'];

	$query_a=$db->prepare_query("Select * From lumonata_accommodation_type Where lacco_type_id=%d",$acco_type_id);
	$result_a=$db->do_query($query_a);
	$da=$db->fetch_array($result_a);
	$no_of_bedroom = $da['lno_of_bedroom'];

	
	$tmp = "SELECT * FROM lumonata_accommodation_cancelationpolicy_categories WHERE ldate_from <= %d And ldate_to >%d And lacco_type_cat_id=%d";
	$sql = $db->prepare_query($tmp, $check_in, $check_out ,$acco_type_cat_id);
	$que = $db->do_query($sql);
	$rec = $db->fetch_array($que);
	$lrule1 = $rec['lrule1'];
	$lrule2 = $rec['lrule2'];
	$lrule3 = $rec['lrule3'];
	$canceltation_fee_info['price'] = 0;
	$canceltation_fee_info['unit'] = "";
	$canceltation_fee_info['charge'] = "";
	$canceltation_fee_info['nightBefore'] = $nightBefore;
	$canceltation_fee_info['night'] = $night;
	$canceltation_fee = false;
	$canceltation_fee_next = false;
	
	$qr1=$db->prepare_query("Select * From lumonata_accommodation_cancelationpolicy_master Where lcancelpolicy_master_ID=%d",$lrule1);
	$rr1=$db->do_query($qr1);
	$fr1=$db->fetch_array($rr1);
	$lnightBefore=$fr1['lnightBefore'];
	$lcharge=$fr1['lcharge'];
	$lunit=$fr1['lunit'];
	if ($lnightBefore > $nightBefore){
		$canceltation_fee = true;
		$canceltation_fee_next = false;
		if ($lunit=='%'){
			$canceltation_fee_info['price'] = ($lcharge * $grand_total)/ 100;
			$canceltation_fee_info['unit'] = $lunit;
			$canceltation_fee_info['charge'] = $lcharge;
		}else{
			$canceltation_fee_info['price'] = ($lcharge * $night);
			$canceltation_fee_info['unit'] = $lunit;
			$canceltation_fee_info['charge'] = $lcharge;
		}
	}else{
		$canceltation_fee_next = true;
	}
	 	
	if ($canceltation_fee_next){
		$qr2=$db->prepare_query("Select * From lumonata_accommodation_cancelationpolicy_master Where lcancelpolicy_master_ID=%d",$lrule2);
			$rr2=$db->do_query($qr2);
			$fr2=$db->fetch_array($rr2);
			$lnightBefore=$fr2['lnightBefore'];
			$lcharge=$fr2['lcharge'];
			$lunit=$fr2['lunit'];
			if ($lnightBefore > $nightBefore){
				$canceltation_fee = true;
				$canceltation_fee_next = false;
				if ($lunit=='%'){
					$canceltation_fee_info['price'] = ($lcharge * $grand_total)/ 200;
					$canceltation_fee_info['unit'] = $lunit;
					$canceltation_fee_info['charge'] = $lcharge;
				}else{
					$canceltation_fee_info['price'] = ($lcharge * $night);
					$canceltation_fee_info['unit'] = $lunit;
					$canceltation_fee_info['charge'] = $lcharge;
				}
			}else{
				$canceltation_fee_next = true;
			}
	}
	
	if ($canceltation_fee_next){
		$qr3=$db->prepare_query("Select * From lumonata_accommodation_cancelationpolicy_master Where lcancelpolicy_master_ID=%d",$lrule3);
			$rr3=$db->do_query($qr3);
			$fr3=$db->fetch_array($rr3);
			$lnightBefore=$fr3['lnightBefore'];
			$lcharge=$fr3['lcharge'];
			$lunit=$fr3['lunit'];
			if ($lnightBefore > $nightBefore){
				$canceltation_fee = true;
				$canceltation_fee_next = false;
				if ($lunit=='%'){
					$canceltation_fee_info['price'] = ($lcharge * $grand_total)/ 200;
					$canceltation_fee_info['unit'] = $lunit;
					$canceltation_fee_info['charge'] = $lcharge;
				}else{
					$canceltation_fee_info['price'] = ($lcharge * $night);
					$canceltation_fee_info['unit'] = $lunit;
					$canceltation_fee_info['charge'] = $lcharge;
				}
			}else{
				$canceltation_fee_next = true;
			}
	}
	
	if ($date_now > $check_in){
		$canceltation_fee_info['price'] = $grand_total;
		$canceltation_fee_info['unit'] = "";
		$canceltation_fee_info['charge'] = "";
		$canceltation_fee_info['nightBefore'] = $nightBefore;
		$canceltation_fee_info['night'] = $night;
	}
	
	
	return $canceltation_fee_info;	
}


		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>