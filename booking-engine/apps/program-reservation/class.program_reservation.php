<?php 
class program_reservation extends db{
	function program_reservation($appName=""){
		$this->appName=$appName;
		$this->setMetaTitle("Program Reservation");	
	}
	function load(){
		if(isset($_POST['act']) && $_POST['act']=='use-ajax'){
			echo $this->validate_ajax();	
		}else{
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			//set block
			$t->set_var('category_program',$this->get_list_category_program());
			$data_booking = $this->get_first_data();
			//print_r($data_first);
			$t->set_var('list_reservation_history_all',$data_booking->temp);
			$t->set_var('pagging_list',$data_booking->pagging);
			$t->set_var('grand_total',$data_booking->grand_total);
			
			/*
			//$t->set_var('roomTypeOpt',$this->get_temp_all_villa($t,true,true));
			$t->set_var('site_url',SUPPLIERPANEL_SITE_URL);
			
			$data_villa = $this->get_data('lumonata_articles',"where larticle_type='villas'",'array');
			$id_firsd_villa = $data_villa['larticle_id'];
			
			//View list booking first atau pakai ajax
				*/
				$t->Parse('vC', 'viewContent', true); 
			
			return $t->Parse('mBlock', 'mainBlock', false);	
		}
		
	}	
	
	function get_first_data(){
		$month = date('m');
		$year = date('Y');
		
		$val_1 = mktime(0, 0, 0, $month, 1, $year); 
	    $val_2 = mktime(0, 0, 0, $month+1, 1, $year); 
				
		$page = 1;
		
		$first_data = 	$this->get_all_list_prog_history($val_1,$val_2,'all',$page);	
		
		//get_all_list_accom_history($val_1,$val_2,$show_label_villa=true,$status='all',$page=0)
		//echo 'dd';
		$ob_fd = json_decode($first_data);
		//print_r($ob_fd);
		return  $ob_fd;
		
		//echo $ob_fd->pagging;
		
	}
	
	
	//general function
	function get_list_category_program($selected_parent=0,$index=0,$id_show=0, $not_show_parent = false){
		global $db;
		$str 	=  $db->prepare_query("select * from lumonata_rules where lgroup=%s",'ratesreservations');
		$result = $db->do_query($str);
		$parent = "";
		if($not_show_parent==false) $parent .= "<option value=\"all\">All</option>";
		while($data= $db->fetch_array($result)){
			if($data['lrule_id']!=$id_show){
				if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
				else $select_info='';
				if($data['lname']=='Rejuvenation Programs'){
					$parent .= "<option $select_info value=".$data['lrule_id'].">".$data['lname']."</option>";
					$parent .= $this->get_child($data['lrule_id'],1,$selected_parent);
				} 
			}
		}
		
		return $parent;
	}
	
	function get_child($id,$level=0,$selected_parent){
		global $db;
		$str = $db->prepare_query("select * from lumonata_rules where lparent=%d",$id);
		$result = $db->do_query($str);
		$child = "";
		while($data= $db->fetch_array($result)){
			//echo $data.','.$selected_parent.'#';
			if($data['lrule_id']==$selected_parent) $select_info = 'selected="selected"';
			else $select_info='';
			$nbs = "";
			for($i=1;$i<=$level;$i++){
				$nbs .= "&nbsp;&nbsp;&nbsp;";
			}
			$child .= "<option $select_info value=".$data['lrule_id'].">$nbs".$data['lname']."</option>";
			$child .= $this->get_child($data['lrule_id'],$level+1,$selected_parent); 
		}
		return $child;
		
	}
	
	
	
	function validate_ajax(){
		if($_POST['do_act']=='get-list-booking-history'){
			$type_search = $_POST['type_search'];
			$prog_id 	 = $_POST['prog_id'];
			$year	 	 = $_POST['year'];
			$status		 = $_POST['status'];
			$page		 = $_POST['page'];
			//if($page==1) $page=0;
			
			if($type_search=='by_month') {
				$month	 = $_POST['month'];
				$val_1 = mktime(0, 0, 0, $month, 1, $year); 
				$val_2 = mktime(0, 0, 0, $month+1, 1, $year); 
			}else{
				$val_1 = mktime(0, 0, 0, 1, 1, $year);
				$val_2 = mktime(0, 0, 0, 1, 1, $year+1);
			}						
			
			if($prog_id=='all') return $this->get_all_list_prog_history($val_1,$val_2,$status,$page);
			else return $this->get_list_accom_history_single_program($prog_id,$val_1,$val_2,true,$status,$page);			
		}else if($_POST['do_act']=='get-single-detail-booking-history'){
			$book_id = $_POST['book_id'];
			return $this->single_detail_book_history($book_id);
		}else if($_POST['do_act']=='change-status-reservation'){
			$book_id = $_POST['book_id'];
			$status  = $_POST['status_id'];
			return $this->change_status_reservation($book_id,$status);
		}else if($_POST['do_act']=='change-status-reservation-and-detail'){
			$this->update_bookdet_and_status();
		}
	}
	
	function update_bookdet_and_status(){
		global $db;
		$book_id = $_POST['book_id'];
		$dbk = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
		if(!empty($dbk)){//do update
			$name = $_POST['name'];
			$email = $_POST['email'];
			$country = $_POST['country'];
			$phone = $_POST['phone'];
			$guest = $_POST['guest'];
			$message = $_POST['message'];
			$status = $_POST['status_id'];
			$additional_service = json_encode($_POST['additional_service']);
			$additional_cost = $_POST['additional_cost'];
			$heard_about_us = $_POST['heard_about_us'];
			$return =array();$error=0;			
			$u = $db->prepare_query("update lumonata_booking set 
									lname=%s,lemail=%s,lphone=%s,
									lnote=%s,lcountry=%s,lguest=%s,lstatus=%d where lbook_id=%d",$name,$email,$phone,$message,$country,$guest,$status,$book_id);
			//echo $u;
			$ru = $db->do_query($u);						
			if($ru){
				$dbkd = $this->get_data('lumonata_booking_detail',"where lbook_id=$book_id",'array');
				if(!empty($dbkd)){
					$ud =$db->prepare_query("update lumonata_booking_detail set ladditional_service=%s,ladditional_cost=%d,lheard_about_us=%s where lbook_id=%d",
					$additional_service,$additional_cost,$heard_about_us,$book_id);
				}else{
					$ud = $db->prepare_query("insert into lumonata_booking_detail 
											  (lbook_id,ladditional_service,ladditional_cost,lheard_about_us) values 
											  (%d,%s,%d,%s)",$book_id,$additional_service,$additional_cost,$heard_about_us);
				}
				$rud = $db->do_query($ud);
				if($rud){
					if($status==0 || $status==10){
						require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
						$this->villa_reservation = new villa_reservation();	
						$this->villa_reservation->cancel_book_n_availability($book_id);	
						if($status==10)$this->delete_booking_data($book_id);	
					}
					/*if($status==2) $this->send_email_waiting_down_payment($book_id);tunggu confirm aktif dari pak purwa*/
					/*if($status==3) $this->send_email_waiting_full_payment($book_id);
					if($status==4) {
						require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
						$this->villa_reservation = new villa_reservation();	
						$this->villa_reservation->change_status_availability($book_id);
						ob_start();
						$this->send_email_confirm($book_id);
						ob_end_flush();
					}*/
					/*if($status==5) $this->send_email_unconfirm($book_id);
					if($status==0) $this->send_email_cancel($book_id);*/
					
				}else $error++;//end update book_detail		
			}else $error++;// ebd update booking
			
			if($error==0){
				$return['status']=$status;
				$return['process']='success';
			}else $return['process']='failed';
			echo json_encode($return);
		}
	}
	
	
	
	
	function change_status_reservation($book_id,$status){
		global $db;
		
		$update = $db->prepare_query("update lumonata_booking set lstatus=%d where lbook_id=%d",$status,$book_id);
		$result = $db->do_query($update);
		$data = array();
		if($result){
			if($status==0 || $status==10){
				require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
				$this->villa_reservation = new villa_reservation();	
				$this->villa_reservation->cancel_book_n_availability($book_id);	
				if($status==10)$this->delete_booking_data($book_id);	
			}
			
			/*if($status==2) $this->send_email_waiting_down_payment($book_id);
			
			tunggu confirm aktif dari pak purwa
			*/
			
			
			/*if($status==3) $this->send_email_waiting_full_payment($book_id);
			if($status==4) {
				require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
				$this->villa_reservation = new villa_reservation();	
				$this->villa_reservation->change_status_availability($book_id);
				ob_start();
				$this->send_email_confirm($book_id);
				ob_end_flush();
			}*/
			/*if($status==5) $this->send_email_unconfirm($book_id);
			if($status==0) $this->send_email_cancel($book_id);*/
			
			$data['status']=$status;
			$data['process']='success';
		}else{
			$data['process']='failed';
		}
		
		echo json_encode($data);
		
	}
	
	function delete_booking_data($book_id){
		global $db;
		$qd3 = $db->prepare_query("delete from lumonata_booking_program where lbook_id=%d",$book_id);
		$db->do_query($qd3);
		
		$qd = $db->prepare_query("delete from lumonata_booking where lbook_id=%d",$book_id);	
		$db->do_query($qd);
		
		$qd2 = $db->prepare_query("delete from lumonata_booking_detail where lbook_id=%d",$book_id);
		$db->do_query($qd2);
		
		
	}
	
	function get_contact_us_detail(){
		$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];	
		$result = array();
				
		$result['contact_us_email']= get_additional_field( $post_id, 'contact_us_email', $article_type);
		$result['contact_us_phone'] = get_additional_field( $post_id, 'contact_us_phone', $article_type);
		return $result;
	}
	
	function send_email_waiting_down_payment($book_id){
		$content_email = $this->email_waiting_down_payment($book_id);
		//echo $content_email['content_email'];
		if(!empty($content_email)){			
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			$data_smtp = $this->get_smtp_info();
			$email_smtp = $data_smtp['email'];
			$pass_smtp = $data_smtp['pass'];
			
			require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
			$mail = new PHPMailer(true);
			$web_name = 'Sukhavati.com';
			$SMTP_SERVER = get_meta_data('smtp');
			try {
				$mail->SMTPDebug  = 1;
				$mail->IsSMTP();
				$mail->Host = $SMTP_SERVER;
				$mail->SMTPAuth = true;
				$mail->Port       = 2525; 
				$mail->Username   = $email_smtp;  // GMAIL username
				$mail->Password   = $pass_smtp; // GMAIL password
				$mail->AddReplyTo($sukhavati_email, $web_name);
				$mail->AddAddress($email_to);
				$mail->SetFrom($sukhavati_email, $web_name);
				$mail->AddReplyTo($sukhavati_email, $web_name);
				$mail->Subject = "Sukhavati Booking Invoice: $book_id";
				$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
				$mail->MsgHTML($content);
				$mail->Send();
			}catch (phpmailerException $e) {
				echo $e->errorMessage().$sukhavati_email;
			}catch (Exception $e) {
				echo $e->getMessage();
			}
					
			/*ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Booking Invoice: $book_id";
			$send = mail($email_to,$subject,$content,$headers);*/
		}
	}
	
	function email_waiting_down_payment($book_id){
		global $db;
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
		
		$OUT_TEMPLATE="email-down-payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block 
		//$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'single_history_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);			
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email'] = $dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$t->set_var('arrival_date',$arrival);
			$t->set_var('derpature_date',$derpature);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			$the_subtotal = 0;	
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];
				if($occupancy_type==1) {
					$name_occupancy = 'Single';
					$price = $data_program['lsingle_occupancy_price'];
				}
				else if($occupancy_type==2){
					$name_occupancy = 'Double';
					$price = $data_program['ldouble_occupancy_price'];
				}
				
				$id = $dbp['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				//get discount
				$rt = $this->get_data_additional_villa($id,'room_type');
				$rt  = $db->fetch_array($rt);
				//print_r($rt);
				$room_type_id  = $rt['lrule_id'];
				$date_booking = date('d M Y',$dbp['lbook_id']);
				$time_booking = strtotime($date_booking);					
				$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$date_book,'program');
				
				$class_line_through = '';$price_disc ="";
				$disc_desc = "";
				if(!empty($data_discount)){
					$class_line_through = 'style="text-decoration: line-through;color: #bbb;"';
					$price_disc = '($ '.number_format($data_discount['price_discount'],2).')';
					$disc_desc = "<b>".$data_discount['discount_text']."</b><br/>";
					$result['total'] = $data_discount['price_discount'];
					$sub_total = $data_discount['price_discount'];
				}else {
					$result['total']	= $price;
					$sub_total = $price;
				}
				
				$the_price  ="<li>
								<span style=\"width: 50px;float:left;\">$name_occupancy</span>
								<span $class_line_through >($ $price)</span><br>
								<span style=\"margin-left: 50px;\">$price_disc</span><br>
							  </li>";
	
				$temp_price = "<ul style=\"list-style: none;padding: 0;min-height:120px;\">$the_price</ul>";								
				$the_subtotal = $the_subtotal + $sub_total;
				
				$prog_commance = get_additional_field($data_program['larticle_id'],'prog_commence','ratesreservations');
				$date_prog_commance = $arrival_date + ($prog_commance * 86400);
				$time_prog_commance = get_additional_field($prog_id,'program_commences','program-package');
				$dpc = date('d M Y',$date_prog_commance).' - '.$time_prog_commance;
				$date_prog_end = date('d M Y',$derpature_date).' – 5:00pm';					
				
				$list_prog .= "<li style=\"min-height:120px\">
								  <span>
									  <b>$name_program</b><br>
									  <small>Dates: $arrival - $derpature</small><br>
									  <small>Program commences: $dpc</small><br>
									  <small>Program ends: $date_prog_end</small><br>
									  <small class=\"thesl\">Villa:</small>$villa_name<br>
									  $disc_desc
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .=$temp_price;
				$list_total		 .="<li style=\"min-height:120px\">".number_format($sub_total,2)."</li>";
				$list_status	 .="<li style=\"min-height:120px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding: 0 0 0 10px;margin: 0;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;margin: 0;\">".$list_prog_price."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;margin: 0;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			$dt_dp_presentase = $this->get_data('lumonata_meta_data'," where lmeta_name='dp_presentase'",'array');
			$t->set_var('dp_presentase',$dt_dp_presentase['lmeta_value'].'%');
			
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);
			
			
						
			$t->set_var('sub_total',number_format($the_subtotal,2));
			//find surcharge
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			$surcharge_temp ="";
			if(!empty($data_surcharge)){
				$grand_total = $the_subtotal + $data_surcharge['the_surcharge'];
				$grand_total_text = number_format($grand_total,2);
				$surcharge_text  = number_format($data_surcharge['the_surcharge'],2);
				//$surcharge_temp = td_surcharge_booking_detail($data_surcharge['the_surcharge'],$data_surcharge['temp'],$grand_total);
				$surcharge_temp = $this->td_surcharge_booking_detail($surcharge_text,$data_surcharge['temp'],$grand_total_text);
			}
			
			$downpayment_value = ($grand_total*$dt_dp_presentase['lmeta_value'])/100;
			
			$t->set_var('down_payment',number_format($downpayment_value,2));
			$t->set_var('surcharge_temp',$surcharge_temp);
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
			
			$this->set_detail_booking_new($t,$book_id);
				
			$result['content_email'] = $t->Parse('shb', 'single_history_block', false);	
		}
		
		return $result;
		
	}
	
	
	function send_email_waiting_full_payment($book_id){
		$content_email = $this->email_waiting_full_payment($book_id);
		//echo $content_email['content_email'];
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			//$sukhavati_email = 'adisend@localhost';
			//$email_to = 'adisend2@localhost';
			
			ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
	}
	
	function email_waiting_full_payment($book_id){
		global $db;
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
		
		$OUT_TEMPLATE="email_full_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);			
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email'] = $dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			
			$dlp = $this->get_data('lumonata_booking_program as a',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				$id = $data_program['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				$excess_day = $days - $days_prog;
				
				$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;
				
				
				$list_prog .= "<li style=\"min-height:80px\">
								  <span>
									  <b>$name_program</b>
									  <br>
									  <small>Dates: $arrival - $derpature</small>
									  <br>
									  <small class=\"thesl\">Villa:</small>$villa_name
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .="<li style=\"min-height:80px\">$price</li>";
				$list_excess_day .="<li style=\"min-height:80px\">$excess_day</li>";
				$list_price_villa.="<li style=\"min-height:80px\">$price_villa</li>";
				$list_total		 .="<li style=\"min-height:80px\">$sub_total</li>";
				$list_status	 .="<li style=\"min-height:80px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			$dt_dp_presentase = $this->get_data('lumonata_meta_data'," where lmeta_name='dp_presentase'",'array');
			$t->set_var('dp_presentase',$dt_dp_presentase['lmeta_value'].'%');
			$t->set_var('down_payment',($grand_total*$dt_dp_presentase['lmeta_value'])/100);
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total',$grand_total);
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
			
			$data_due_date_final_payment = $this->get_data('lumonata_meta_data',"where lmeta_name='due_date_final_payment' and lapp_name='global_setting'",'array');
			$due_date_final_payment = $data_due_date_final_payment['lmeta_value'];				
			$the_due_date_final_payment = date('d M Y',$data_booking['lcheck_in']-($due_date_final_payment*86400));
			
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('down_payment_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= $dt_paypal_dp['lgross'];		
			$t->set_var('downpayment',$downpayment);
			$t->set_var('rest_payment',$grand_total-$downpayment);
			$t->set_var('paypal_id',$dt_paypal_dp['ltxn_id']);
			$t->set_var('receiver_email',$dt_paypal_dp['lemail']);
			$t->set_var('payment_date',$dt_paypal_dp['lcreated_date']);
			$t->set_var('due_date',$the_due_date_final_payment);
			$data_days_crojob_final_payment  = $this->get_data('lumonata_meta_data',"where lmeta_name='days_crojob_final_payment' and lapp_name='global_setting'",'array');	
			$t->set_var('cronjob_days',$data_days_crojob_final_payment['lmeta_value']);
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		
		return $result;
			
	}
	
	function generate_pdf_file($content,$book_id){	
		set_include_path(ROOT_PATH."/dompdf/");
		
		require_once "dompdf_config.inc.php";
		 
		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->render();
		 
		$output = $dompdf->output();
		$file_path = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";
		unlink($file_path);
		file_put_contents($file_path, $output);
		
	}
	
	function content_invoice($book_id){
		global $db;
		$OUT_TEMPLATE="invoice_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
			$this->villa_reservation = new villa_reservation();	
		
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			
			$dlp = $this->get_data('lumonata_booking_program as a',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				$id = $data_program['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				$excess_day = $days - $days_prog;
				
				$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;
				
				
				$list_prog .= "<li style=\"height:100px\">
								  <span>
									  <b>$name_program</b>
									  <br>
									  <small>Dates: $arrival - $derpature</small>
									  <br>
									  <small class=\"thesl\">Villa:</small>$villa_name
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .="<li style=\"height:100px\">".number_format($price,2)."</li>";
				$list_excess_day .="<li style=\"height:100px\">$excess_day</li>";
				$list_price_villa.="<li style=\"height:100px\">".number_format($price_villa,2)."</li>";
				$list_total		 .="<li style=\"height:100px\">".number_format($sub_total,2)."</li>";
				$list_status	 .="<li style=\"height:100px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total', number_format($grand_total,2));
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
			
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('dp_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= number_format($dt_paypal_dp['lgross'],2);		
			$t->set_var('down_payment',$downpayment);
			$dt_paypal_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
			$t->set_var('paypal_id',$dt_paypal_fp['ltxn_id']);
			$fullpayment=  number_format($dt_paypal_fp['lgross'],2);		
			$t->set_var('full_payment',$fullpayment);
			//$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		
		return $t->Parse('shb', 'the_email_block', false);	;
	}
	
	function send_email_confirm($book_id){
		$content_email = $this->email_confirm_payment($book_id);
		$message = $content_email['content_email'];
		$email_to = $content_email['email'];
		
		//generate invoice
		$content_invoice =  $this->content_invoice($book_id);
		$this->generate_pdf_file($content_invoice,$book_id);
		
		$path_invoice = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";;
		
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			//print_r($data_contact);
			$sukhavati_email = $data_contact['contact_us_email'];
			
			$data_smtp = $this->get_smtp_info();
			$email_smtp = $data_smtp['email'];
			$pass_smtp = $data_smtp['pass'];
			//print_r($data_smtp);
			//$sukhavati_email = 'adi@lumonatalabs.com';
			require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
			
			$mail = new PHPMailer(true);
			$web_name = 'sukhavati.com';
			//$email_to = 'juli4rth4@yahoo.com';
			$SMTP_SERVER = get_meta_data('smtp');
			//$SMTP_SERVER = 'mail.lumonatalabs.com';
			try {
				$mail->SMTPDebug  = 1;
				$mail->IsSMTP();
				$mail->Host = $SMTP_SERVER;
				$mail->SMTPAuth = true;
				$mail->Port       = 2525; 
				$mail->Username   = $email_smtp;  // GMAIL username
				$mail->Password   = $pass_smtp; // GMAIL password
				$mail->AddReplyTo($sukhavati_email, 'sukhavati.com');
				$mail->AddAddress($email_to,'juliartha');
				$mail->SetFrom($sukhavati_email, $web_name);
				$mail->AddReplyTo($sukhavati_email, $web_name);
				$mail->Subject = "Sukhavati Booking Invoice: $book_id";
				$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
				$mail->MsgHTML($message);
				$mail->AddAttachment($path_invoice);
				$mail->Send();
			}catch (phpmailerException $e) {
				echo $e->errorMessage().$sukhavati_email;
			}catch (Exception $e) {
				echo $e->getMessage();
			}
		}
	}
	
	function get_smtp_info(){
		global $db;
		$result = array();
		$dt_email  = $this->get_data('lumonata_meta_data',"where lmeta_name = 'email_user_smtp'",'array');
		$result['email'] = $dt_email['lmeta_value'];
		
		$dt_pass_email  = $this->get_data('lumonata_meta_data',"where lmeta_name = 'pass_email_user_smtp'",'array');
		$base64_decode_pass = base64_decode($dt_pass_email['lmeta_value']);
		$json_decode_pass = json_decode($base64_decode_pass);
		$result['pass'] = $json_decode_pass->p_e_u_smtp;
		
		return $result;			
	}
	
	
	function send_email_confirm_old($book_id){
		$content_email = $this->email_confirm_payment($book_id);
		$message = $content_email['content_email'];
		
		//generate invoice
		$content_invoice =  $this->content_invoice($book_id);
		//echo $content_invoice;
		$this->generate_pdf_file($content_invoice,$book_id);
		
		$path_invoice = ROOT_PATH."/booking-engine/files/pdf/$book_id.pdf";;
		
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			/*$sukhavati_email = 'adisend@localhost';
			$email_to = 'adisend2@localhost';*/
			
			/*ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$attachment = chunk_split(base64_encode(file_get_contents($path_invoice)));
			$headers .= 'Content-Type: application/zip; name="'.$book_id.'.pdf"' . "\r\n"; 
			$headers .= 'Content-Transfer-Encoding: base64 ' . "\r\n"; 
			$headers .= 'Content-Disposition: '.$attachment."\r\n"; */
			
			
			$file = $path_invoice;
			$file_size = filesize($file);
			$handle = fopen($file, "r");
			$content = fread($handle, $file_size);
			fclose($handle);
			$file_content = chunk_split(base64_encode($content));
			$uid = md5(uniqid(time()));
			$name = basename($file);
			
			
			$header = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$header .= "Reply-To: ".$sukhavati_email."\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $message."\r\n\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
			$header .= $file_content."\r\n\r\n";
			$header .= "--".$uid."--";		
			
			$subject = "Sukhavati Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
	}
	
	function email_confirm_payment($book_id){
		global $db;
		$OUT_TEMPLATE="email_confirm_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
			$this->villa_reservation = new villa_reservation();	
		
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email']=$dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			
			$dlp = $this->get_data('lumonata_booking_program as a',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				$id = $data_program['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				$excess_day = $days - $days_prog;
				
				$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;
				
				
				$list_prog .= "<li style=\"min-height:80px\">
								  <span>
									  <b>$name_program</b>
									  <br>
									  <small>Dates: $arrival - $derpature</small>
									  <br>
									  <small class=\"thesl\">Villa:</small>$villa_name
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .="<li style=\"min-height:80px\">".number_format($price,2)."</li>";
				$list_excess_day .="<li style=\"min-height:80px\">$excess_day</li>";
				$list_price_villa.="<li style=\"min-height:80px\">".number_format($price_villa,2)."</li>";
				$list_total		 .="<li style=\"min-height:80px\">".number_format($sub_total,2)."</li>";
				$list_status	 .="<li style=\"min-height:80px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total', number_format($grand_total,2));
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
			
			
			/*$t->set_var('book_id',$book_id);
			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				print_r($data_booking);
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];	
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);
				$result['email'] =	$dt_book['email'];				
			}
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);
			//down payment
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('down_payment_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= $dt_paypal_dp['lgross'];		
			$t->set_var('downpayment',$downpayment);
			//full payment
			$dt_paypal_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
			$full_payment= $dt_paypal_fp['lgross'];		
			$t->set_var('full_payment',$full_payment);
			
			
			$t->set_var('rest_payment',$grand_total-($downpayment+$full_payment));
			$t->set_var('paypal_id',$dt_paypal_fp['ltxn_id']);
			$t->set_var('receiver_email',$dt_paypal_fp['lemail']);
			$t->set_var('payment_date',$dt_paypal_fp['lcreated_date']);
			$t->set_var('grand_total',$grand_total);*/
			
			$dt_paypal_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
			$t->set_var('dp_presentase',$dt_paypal_dp['lpresentase_dp'].'%');
			$downpayment= number_format($dt_paypal_dp['lgross'],2);		
			$t->set_var('down_payment',$downpayment);
			$dt_paypal_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
			$t->set_var('paypal_id',$dt_paypal_fp['ltxn_id']);
			$fullpayment=  number_format($dt_paypal_fp['lgross'],2);		
			$t->set_var('full_payment',$fullpayment);
			//$rest_payment = $grand_total-($downpayment+$fullpayment);
			//add_variable('rest_payment',$rest_payment);
			
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		
		return $result;
	}
	
	function send_email_unconfirm($book_id){
		//echo $book_id;
		//echo 'dd';
		$content_email = $this->email_uncon_canceled($book_id,'unconfirm');
		//print_r($content_email);
		//echo $content_email['content_email'];
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			//$sukhavati_email = 'adisend@localhost';
			//$email_to = 'adisend2@localhost';
			
			ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Unconfirm Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
		
	}
	
	function email_uncon_canceled($book_id,$type){
		global $db;
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
		if($type=='unconfirm') $OUT_TEMPLATE="email_unconfirm_payment.html";
		else if($type=='canceled')$OUT_TEMPLATE="email_cancel_payment.html";
				
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		//$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email'] = $dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";$the_subtotal = 0;				
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				if($occupancy_type==1) {
					$name_occupancy = 'Single';
					$price = $data_program['lsingle_occupancy_price'];
				}
				else if($occupancy_type==2){
					$name_occupancy = 'Double';
					$price = $data_program['ldouble_occupancy_price'];
				}
				
				$id = $dbp['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;		
				
				//get discount
				$rt = $this->get_data_additional_villa($id,'room_type');
				$rt  = $db->fetch_array($rt);
				//print_r($rt);
				$room_type_id  = $rt['lrule_id'];
				$date_booking = date('d M Y',$dbp['lbook_id']);
				$time_booking = strtotime($date_booking);					
				$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$date_book,'program');
				
				$class_line_through = '';$price_disc ="";
				$disc_desc = "";
				if(!empty($data_discount)){
					$class_line_through = 'style="text-decoration: line-through;color: #bbb;"';
					$price_disc = '($ '.number_format($data_discount['price_discount'],2).')';
					$disc_desc = "<b>".$data_discount['discount_text']."</b><br/>";
					$result['total'] = $data_discount['price_discount'];
					$sub_total = $data_discount['price_discount'];
				}else {
					$result['total']	= $price;
					$sub_total = $price;
				}
				$the_price  ="<li>
								<span style=\"float: left;width: 50px;\">$name_occupancy</span>
								<span $class_line_through >($ ".number_format($price,2).")</span><br>
								<span style=\"margin-left: 50px;\">$price_disc</span><br>
							  </li>";
				$temp_price = "<ul style=\"list-style: none;padding: 0;min-height:120px;\">$the_price</ul>";								
				$the_subtotal = $the_subtotal + $sub_total;	
				
				$list_prog .= "<li>
								  <span>
									  <b>$name_program</b><br>
									  <small>Dates: $arrival - $derpature</small><br>
									  <small class=\"thesl\">Villa:</small>$villa_name<br>
									  $disc_desc
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .=$temp_price;
				$list_total		 .="<li style=\"min-height:120px\">".number_format($sub_total,2)."</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			
			$t->set_var('sub_total',number_format($the_subtotal,2));
			//find surcharge
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			$surcharge_temp ="";
			if(!empty($data_surcharge)){
				$grand_total = $the_subtotal + $data_surcharge['the_surcharge'];
				$surcharge_text = number_format($data_surcharge['the_surcharge'],2);
				$grand_total_text = number_format($grand_total,2);
				$surcharge_temp = $this->td_surcharge_booking_detail($surcharge_text,$data_surcharge['temp'],$grand_total_text);
			}
			$t->set_var('surcharge_temp',$surcharge_temp);

			
			$dt_dp_presentase = $this->get_data('lumonata_meta_data'," where lmeta_name='dp_presentase'",'array');
			$t->set_var('dp_presentase',$dt_dp_presentase['lmeta_value'].'%');
			$downpayment = ($grand_total*$dt_dp_presentase['lmeta_value'])/100;
			$t->set_var('down_payment',number_format($downpayment,2));
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total',$grand_total);
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
				
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		return $result;
	}
	
	
	function email_unconfirm_payment($book_id){
		global $db;
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
			
		$OUT_TEMPLATE="email_unconfirm_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		//$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email'] = $dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";$the_subtotal = 0;				
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				$id = $data_program['lvilla_id'];

				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;				
				
				$list_prog .= "<li style=\"min-height:80px\">
								  <span>
									  <b>$name_program</b>
									  <br>
									  <small>Dates: $arrival - $derpature</small>
									  <br>
									  <small class=\"thesl\">Villa:</small>$villa_name
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .="<li style=\"min-height:80px\">$price</li>";
				$list_excess_day .="<li style=\"min-height:80px\">$excess_day</li>";
				$list_price_villa.="<li style=\"min-height:80px\">$price_villa</li>";
				$list_total		 .="<li style=\"min-height:80px\">$sub_total</li>";
				$list_status	 .="<li style=\"min-height:80px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			$dt_dp_presentase = $this->get_data('lumonata_meta_data'," where lmeta_name='dp_presentase'",'array');
			$t->set_var('dp_presentase',$dt_dp_presentase['lmeta_value'].'%');
			$t->set_var('down_payment',($grand_total*$dt_dp_presentase['lmeta_value'])/100);
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total',$grand_total);
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
				
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		return $result;
	}
	
	
	function send_email_cancel($book_id){
		//echo $book_id;
		$content_email = $this->email_uncon_canceled($book_id,'canceled');
		//print_r($content_email);
		//echo $content_email['content_email'];
		if(!empty($content_email)){
			$data_contact = $this->get_contact_us_detail();
			$sukhavati_email =$data_contact['contact_us_email'];
			
			$content = $content_email['content_email'];
			$email_to = $content_email['email'];
			
			//for local
			//$sukhavati_email = 'adisend@localhost';
			//$email_to = 'adisend2@localhost';
			
			ini_set("SMTP", SMTP_SERVER);
			ini_set("sendmail_from", $sukhavati_email);
				
			$headers  = "From: SUKHAVATI TEAM <$sukhavati_email>\r\n"; 
			$headers .= "Cc: ".$sukhavati_email."\r\n";
			$headers .= "Bcc: ".$sukhavati_email."\r\n";
			$headers .= "Reply-To: $sukhavati_email\r\n"; 
			$headers .= "Return-Path: $sukhavati_email\r\n"; 
			$headers .= "X-Mailer: Drupal\n"; 
			$headers .= 'MIME-Version: 1.0' . "\n"; 
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
			
			$subject = "Sukhavati Cancel Booking Invoice: $book_id";
			
			$send = mail($email_to,$subject,$content,$headers);
		}
	}
	
	function email_cancel_payment($book_id){
		global $db;
		
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
			
		$OUT_TEMPLATE="email_cancel_payment.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		//$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'the_email_block',  'shb');		
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		
		$result = array();
		
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('night',$dt_book['days']);
			$t->set_var('total',$total);
			
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$result['email'] = $dt_book['email'];
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			$status = $dt_book['book_status'];
			$t->set_var('book_status',$status);
			$t->set_var('book_date',$dt_book['book_date']);
			$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
			
			$grand_total = 0;
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			
			$dlp = $this->get_data('lumonata_booking_program as a',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				$id = $data_program['lvilla_id'];

				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				$excess_day = $days - $days_prog;
				
				$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;
				
				
				$list_prog .= "<li style=\"min-height:80px\">
								  <span>
									  <b>$name_program</b>
									  <br>
									  <small>Dates: $arrival - $derpature</small>
									  <br>
									  <small class=\"thesl\">Villa:</small>$villa_name
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .="<li style=\"min-height:80px\">$price</li>";
				$list_excess_day .="<li style=\"min-height:80px\">$excess_day</li>";
				$list_price_villa.="<li style=\"min-height:80px\">$price_villa</li>";
				$list_total		 .="<li style=\"min-height:80px\">$sub_total</li>";
				$list_status	 .="<li style=\"min-height:80px\">$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\" style=\"list-style:none;padding-left: 10px;\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\" style=\"list-style:none;padding-left: 10px;\">".$list_status."</ul>");
			
			$t->Parse('lbb', 'list_book_block', true);
			
			$dt_dp_presentase = $this->get_data('lumonata_meta_data'," where lmeta_name='dp_presentase'",'array');
			$t->set_var('dp_presentase',$dt_dp_presentase['lmeta_value'].'%');
			$t->set_var('down_payment',($grand_total*$dt_dp_presentase['lmeta_value'])/100);
			$t->set_var('book_id',$book_id);
			$t->set_var('link_check_reservation','http://'.SITE_URL.'/check-reservation/?book_id='.$book_id);			
			$t->set_var('grand_total',$grand_total);
			
			$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
			$post_id = $data_contact['larticle_id'];
			$article_type = $data_contact['larticle_type'];			
			$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
			$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
			$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
			$t->set_var('phone_sukhavati',$contact_us_phone);
			$t->set_var('phone_sukhavati_bali',$contact_us_phone_bali);
			$t->set_var('email_sukhavati',$contact_us_email);
				
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
		}
		
		
		/*$result = array();
		if($num_booking>0){
			$t->set_var('book_id',$book_id);
			
			$grand_total = 0; 
			$list_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				$t->set_var('name',$dt_book['name']);
				$t->set_var('email',$dt_book['email']);
				$t->set_var('country',$dt_book['country']);
				$t->set_var('phone',$dt_book['phone']);
				$t->set_var('guest',$dt_book['guest']);
				$t->set_var('message',$dt_book['message']);
				$status = $dt_book['book_status'];
				$t->set_var('book_status',$status);
				$t->set_var('book_date',$dt_book['book_date']);
				$t->set_var('logo_sukhavati','http://'.SITE_URL.'/lumonata-content/themes/sukhavati/images/logo-sukhavati.png');
				
				$data_contact = $this->get_data('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
				$post_id = $data_contact['larticle_id'];
				$article_type = $data_contact['larticle_type'];			
				$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
				$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
				$t->set_var('phone_sukhavati',$contact_us_phone);
				$t->set_var('email_sukhavati',$contact_us_email);
				
				$t->Parse('lbb', 'list_book_block', true);
				$result['email'] =	$dt_book['email'];				
			}
			$t->set_var('book_id',$book_id);			
			
			$t->set_var('grand_total',$grand_total);
			$result['content_email'] = $t->Parse('shb', 'the_email_block', false);	
			
		}*/
		
		return $result;
	}
	
	
	
	function get_all_list_prog_history($val_1,$val_2,$status='all',$page=1){
		global $db;
		$OUT_TEMPLATE="list-program-reservation.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'booking_block', 'bb');
		$t->set_block('home', 'list_block',  'lb');
		
		$view = DEFAULT_SHOW_CONTENT;
		$pages = ($page-1)*$view; 
		$limits = "";		
		//$limits = "limit $pages,$view";	
		
		if($status=='all'){
			$list_booking = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' 
							group by lbook_id order by lcheck_in $limits");
			$num_booking  = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' 
							group by lbook_id order by lcheck_in",'num_row');	
		}else{
			$list_booking = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' and lstatus=$status  
							group by lbook_id order by lcheck_in $limits");
			$num_booking  = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' and lstatus=$status  
							group by lbook_id order by lcheck_in",'num_row');
		}
		
		$new_list_booking = array();
		while($data_booking = $db->fetch_array($list_booking)){
			$id_booking  = $data_booking['lbook_id'];
			$index_booking = count($new_list_booking[$id_booking]);
			$new_list_booking[$id_booking][$index_booking] = $data_booking;	
		}
		
		//validate limit list
		$new_list_booking_2 = array();
		$the_view = $pages+ $view;
		//echo $the_view;
		$n = 0;
		foreach($new_list_booking as $id_book => $data_books){
			if($n >= $pages && $n <$the_view) $new_list_booking_2[$id_book]= $data_books;		
			$n++;
		}
		//print_r($new_list_booking_2);
		$grand_total = 0;
		foreach($new_list_booking_2 as $id_booking=> $list_book_villa){
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$id_booking",'array');
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = ""; $list_status="";
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$id_booking");
			
			$the_subtotal = 0;	
			while($dbp= $db->fetch_array($dlp)){
				//print_r($dbp);
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				
				if($occupancy_type==1) {
					$name_occupancy = 'Single';
					$price = $data_program['lsingle_occupancy_price'];
				}
				else if($occupancy_type==2){
					$name_occupancy = 'Double';
					$price = $data_program['ldouble_occupancy_price'];
				}
				
				$id = $dbp['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				
				//get discount
				$rt  = $this->get_data_additional_villa($id,'room_type');
				$rt  = $db->fetch_array($rt);
				
				$room_type_id  = $rt['lrule_id'];
				$date_booking = date('d M Y',$dbp['lbook_id']);
				$time_booking = strtotime($date_booking);
				$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$date_book,'program');
				
				$class_line_through = '';$price_disc ="";
				$disc_desc = "";
				if(!empty($data_discount)){
					$class_line_through = 'style="text-decoration: line-through;color: #bbb;"';
					$price_disc = '($ '.number_format($data_discount['price_discount'],2).')';
					$disc_desc = "<b>".$data_discount['discount_text']."</b><br/>";
					$result['total'] = $data_discount['price_discount'];
					$sub_total = $data_discount['price_discount'];
				}else {
					$result['total']	= $price;
					$sub_total = $price;
				}
				
				$the_price  ="<li>
								<span style=\"width: 40px;\">$name_occupancy</span>
								<span $class_line_through >($ ".number_format($price,2).")</span><br>
								<span style=\"margin-left: 40px;\">$price_disc</span><br>
							  </li>";
				$temp_price = "<ul style=\"list-style: none;padding: 0;min-height:142px;\">$the_price</ul>";
				$the_subtotal = $the_subtotal + $sub_total;				
				
				
				$prog_commance = get_additional_field($data_program['larticle_id'],'prog_commence','ratesreservations');
				$date_prog_commance = $arrival_date + ($prog_commance * 86400);
				$time_prog_commance = get_additional_field($prog_id,'program_commences','program-package');
				$dpc = date('d M Y',$date_prog_commance).' - '.$time_prog_commance;
				$date_prog_end = date('d M Y',$derpature_date).' – 5:00pm';	
				
				$list_prog .= "<li>
								  <span>
									  <b>$name_program</b><br>
									  <small>Dates: $arrival - $derpature</small><br>
									  <small>Program commences: $dpc</small><br>
									  <small>Program ends: $date_prog_end</small><br>
									  <small class=\"thesl\">Villa:</small>$villa_name<br>
									  $disc_desc
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .=$temp_price;
				$list_total		 .="<li>".number_format($sub_total,2)."</li>";
				$list_status	 .="<li>$status</li>";
				
			}
			//find surcharge
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			if(!empty($data_surcharge)){
				$the_subtotal = $the_subtotal + $data_surcharge['the_surcharge'];
			}
			
			$grand_total = $grand_total + $the_subtotal;	
			
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\">".$list_prog_price."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\">".$list_status."</ul>");
			
			$t->Parse('bb', 'booking_block', true);

		}		
		
		//echo $grand_total;
		$result = array();
		if($grand_total==0) $result['temp'] = "<div class=\"wrapper form table list-reservation-history center old-data\">
													No Booking history found...
												</div>";
		else {
			//if($page==0)$page=1;
			$result['pagging'] = $this->get_pagging($page,$num_booking);	
			$result['temp'] = $t->Parse('lb', 'list_block', false);		
		}
		$result['grand_total'] = number_format($grand_total,2);
		return json_encode($result);
	}
	
	function get_pagging($page=1,$num_booking){
		$view = DEFAULT_SHOW_CONTENT;
		if(empty($num_booking) or $num_booking=='null') $num_booking= 0;
		$num_list_pagging = ceil($num_booking/ $view);	
		//echo $num_list_pagging;
		if($num_list_pagging > 1){
			//$page = $page + 1;
			$list = "";
			for($i=1;$i<=$num_list_pagging;$i++){
				if($page==$i) $class = 'class="selected"';
				else $class = '';
				$list .= "<li $class rel=\"$i\">$i</li>";	
			}
			
			$cont = "<div class=\"wrapper form table list-reservation-history cont-pagging old-data $page\">
						<ul class=\"list_pagging\">
							$list
						</ul>             
					 </div>	";	
		}else {
			$cont = "";	
		}
		
		return $cont;	
			
	}
	
	
	function get_list_accom_history_single_program($prog_id,$val_1,$val_2,$show_label_villa=false,$status,$page){
		global $db;
		$OUT_TEMPLATE="list-program-reservation.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//set block
		$t->set_block('home', 'booking_block', 'bb');
		$t->set_block('home', 'list_block',  'lb');
		$limits ="";
		//$limits = "limit $page,$view";
		
		if($status=='all'){
			$list_booking = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' 
							group by lbook_id order by lcheck_in ");
			/*$num_booking  =	$this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' 
							group by lbook_id order by lcheck_in",'num_row');	*/		
		}else{
			$list_booking = $this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' and lstatus=$status
							group by lbook_id order by lcheck_in ");
			
			/*$num_booking  =	$this->get_data('lumonata_booking',"where lcheck_in >= $val_1 and lcheck_in <$val_2 and lbook_type ='prog_villa' and lstatus=$status
							group by lbook_id order by lcheck_in",'num_row');*/			
		}
		
		//get list id first;		
		/*
		$page = ($page-1)*$view; 
		$limits = "";*/	
		$view = DEFAULT_SHOW_CONTENT;
		//echo $page.'$';
		if($page==1)$page_start = 1;
		else $page_start = ($view * $page) - 1;
		$page_end = ($view * $page);
		//echo $page_start.'-'.$page_end;	
		//echo $page.'-'.$page_end;
		$num_booking = 0;
		$new_list_booking = array();
		while($data_booking = $db->fetch_array($list_booking)){
			//print_r($data_booking);
			$id_book = $data_booking['lbook_id'];
			$list_booking_program = $this->get_data('lumonata_booking_program',"where lbook_id=$id_book");
			
			$is_same_category=0;
			while($booking_program = $db->fetch_array($list_booking_program)){
				$id_program_package = $booking_program['lpp_id'];
				$check_is_same_category = $this->get_data('lumonata_program_package',"where lpp_id=$id_program_package and lprogram_id=$prog_id",'num_row');
				if($check_is_same_category>0) $is_same_category++;
			}
			//echo "where lpp_id=$id_program_package and lprogram_id=$prog_id";
			//echo $is_same_category.'oi';
			$viewed = 0;			
			if($is_same_category > 0) {
				$num_booking++;
				//echo $num_booking.' >= '.$page_start .'&& '.$num_booking.' <= '.$page_end.'###'; 
				
				if($num_booking >=$page_start and $num_booking <=$page_end){//validate for limits
					//echo 'masuk limit';
					$list_book_per_id = $this->get_data('lumonata_booking',"where lbook_id=$id_book");
					while($dt_book = $db->fetch_array($list_book_per_id)){
						$id_booking  = $dt_book['lbook_id'];
						$index_booking = count($new_list_booking[$id_booking]);
						$new_list_booking[$id_booking][$index_booking] = $dt_book;	
					}	
				}
				
			}		
		}
		
		//echo "where lpp_id=$id_program_package and lprogram_id=$prog_id";
		//print_r($new_list_booking);
		$grand_total = 0;
		foreach($new_list_booking as $id_booking=> $list_book_villa){
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$id_booking",'array');
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$id_booking");
			$the_subtotal = 0;	
			while($dbp= $db->fetch_array($dlp)){
				//print_r()
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];				
				$price			= $data_program['lprice'];
				//print_r($data_program);
				//print_r($dbp);
				$name_program 	= $data_program['larticle_title'];
				if($occupancy_type==1) {
					$name_occupancy = 'Single';
					$price = $data_program['lsingle_occupancy_price'];
				}
				else if($occupancy_type==2){
					$name_occupancy = 'Double';
					$price = $data_program['ldouble_occupancy_price'];
				}
				$id = $dbp['lvilla_id'];
				
				//$id = $data_program['lvilla_id'];
				$villa_name = $this->get_villa_name($id);
				
				$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				/*$excess_day = $days - $days_prog;
				$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;*/
				
				//get discount
				$rt = $this->get_data_additional_villa($id,'room_type');
				$rt  = $db->fetch_array($rt);
				//print_r($rt);
				$room_type_id  = $rt['lrule_id'];
				$date_booking = date('d M Y',$dbp['lbook_id']);
				$time_booking = strtotime($date_booking);	
				$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$time_booking,'program');	
				//print_r($data_discount);
				
				$class_line_through = '';$price_disc ="";
				$disc_desc = "";
				if(!empty($data_discount)){
					$class_line_through = 'style="text-decoration: line-through;color: #bbb;"';
					$price_disc = '($ '.number_format($data_discount['price_discount'],2).')';
					$disc_desc = "<b>".$data_discount['discount_text']."</b><br/>";
					$result['total'] = $data_discount['price_discount'];
					$sub_total = $data_discount['price_discount'];
				}else {
					$result['total']	= $price;
					$sub_total = $price;
				}
				
				$the_price  ="<li>
								<span style=\"width: 40px;\">$name_occupancy</span>
								<span $class_line_through >($ ".number_format($price,2).")</span><br>
								<span style=\"margin-left: 40px;\">$price_disc</span><br>
							  </li>";
	
				
				//$the_price = "<li $class_line_through >".number_format($price,2)."</li>";
				$temp_price = "<ul style=\"list-style: none;padding: 0;min-height:142px;\">$the_price</ul>";
				$the_subtotal = $the_subtotal + $sub_total;	
				//$grand_total = $grand_total + $sub_total;	
				
				
				$prog_commance = get_additional_field($data_program['larticle_id'],'prog_commence','ratesreservations');
				$date_prog_commance = $arrival_date + ($prog_commance * 86400);
				$time_prog_commance = get_additional_field($prog_id,'program_commences','program-package');
				$dpc = date('d M Y',$date_prog_commance).' - '.$time_prog_commance;
				$date_prog_end = date('d M Y',$derpature_date).' – 5:00pm';	
				
				$list_prog .= "<li>
								  <span>
									  <b>$name_program</b><br>
									  <small>Dates: $arrival - $derpature</small><br>
									  <small>Program commences: $dpc</small><br>
									  <small>Program ends:: $date_prog_end</small><br>
									  <small class=\"thesl\">Villa:</small>$villa_name<br>
									   $disc_desc
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .=$temp_price;
				$list_total		 .="<li>".number_format($sub_total,2)."</li>";
				$list_status	 .="<li>$status</li>";
			}
			
			//find surcharge
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			if(!empty($data_surcharge)){
				$the_subtotal = $the_subtotal + $data_surcharge['the_surcharge'];
			}			
			$grand_total = $grand_total + $the_subtotal;
			
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\">".$list_status."</ul>");
			
			$t->Parse('bb', 'booking_block', true);
		}
		
		$result = array();
		if($grand_total==0) $result['temp'] = "<div class=\"wrapper form table list-reservation-history center old-data\">
													No Booking history found...
												</div>";
		else{ 
			$result['pagging'] = $this->get_pagging($page,$num_booking);	
			$result['temp'] = $t->Parse('lb', 'list_block', false);	
		}
		$result['grand_total'] = number_format($grand_total,2);
		echo json_encode($result);
	}
	
	
	function single_detail_book_history($book_id){
		global $db;
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();	
		
		$OUT_TEMPLATE="single-history.html";
		$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
		$t->set_file('home', $OUT_TEMPLATE);
		//$t->set_block('home', 'list_book_block', 'lbb');
		$t->set_block('home', 'single_history_block',  'shb');
		
		$result = array();
		$num_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'num_row');		
		if($num_booking>0){
			$grand_total = 0; 
						
			$data_booking  = $this->get_data('lumonata_booking',"where lbook_id=$book_id",'array');
			$arrival	= date('d M Y',$data_booking['lcheck_in']);
			$derpature	= date('d M Y',$data_booking['lcheck_out']);
			$arrival_date	= $data_booking['lcheck_in'];
			$derpature_date	= $data_booking['lcheck_out'];
			$days = ($derpature_date-$arrival_date )/86400;
			$status = $data_booking['lstatus'];
			$dt_book = $this->villa_reservation->get_data_booking($data_booking);
			$t->set_var('name',$dt_book['name']);
			$t->set_var('email',$dt_book['email']);
			$t->set_var('country',$dt_book['country']);
			$t->set_var('phone',$dt_book['phone']);
			$t->set_var('guest',$dt_book['guest']);
			$t->set_var('message',$dt_book['message']);
			//$status = $dt_book['book_status'];
			//$t->set_var('book_status',$status);
			
			//$status = $this->get_status_book_name($data_booking['lstatus']);
			
			$list_prog = ""; $list_prog_price = ""; $list_excess_day = ""; $list_price_villa = ""; $list_total = "";$list_status="";
			$the_subtotal = 0;	
			$dlp = $this->get_data('lumonata_booking_program',"where lbook_id=$book_id");
			while($dbp= $db->fetch_array($dlp)){
				$prog_id= $dbp['lpp_id'];
				$occupancy_type = $dbp['loccupancy_type'];
				$data_program 	= $this->get_data('lumonata_program_package as a, lumonata_articles as b',
								  "where a.lprogram_id=b.larticle_id and a.lpp_id=$prog_id",'array');
				$name_program 	= $data_program['larticle_title'];	
				if($occupancy_type==1) {
					$name_occupancy = 'Single';
					$price = $data_program['lsingle_occupancy_price'];
				}
				else if($occupancy_type==2){
					$name_occupancy = 'Double';
					$price = $data_program['ldouble_occupancy_price'];
				}//echo 'dsdasd';
				//print_r($data_program);
				//print_r($dbp);
				$id = $dbp['lvilla_id'];
				//echo $id.'#';
				$villa_name = $this->get_villa_name($id);
				//echo $villa_name.'#';
				//$days_prog = ($data_program['ldate_end'] - $data_program['ldate_start'])/86400;
				//$excess_day = $days - $days_prog;
				//get discount
				$rt = $this->get_data_additional_villa($id,'room_type');
				$rt  = $db->fetch_array($rt);
				//print_r($rt);
				$room_type_id  = $rt['lrule_id'];
				$date_booking = date('d M Y',$dbp['lbook_id']);
				$time_booking = strtotime($date_booking);					
				$data_discount = $this->single_villa_discount_and_earlybird($room_type_id,$arrival_date,$derpature_date,$price,'array',$date_book,'program');
				
				$class_line_through = '';$price_disc ="";
				$disc_desc = "";
				if(!empty($data_discount)){
					$class_line_through = 'style="text-decoration: line-through;color: #bbb;"';
					$price_disc = '($ '.number_format($data_discount['price_discount'],2).')';
					$disc_desc = "<b>".$data_discount['discount_text']."</b><br/>";
					$result['total'] = $data_discount['price_discount'];
					$sub_total = $data_discount['price_discount'];
				}else {
					$result['total']	= $price;
					$sub_total = $price;
				}
				
				$the_price  ="<li>
								<span style=\"float: left;width: 50px;\">$name_occupancy</span>
								<span $class_line_through >($ $price)</span><br>
								<span style=\"margin-left: 50px;\">$price_disc</span><br>
							  </li>";
	
				$temp_price = "<ul style=\"list-style: none;padding: 0;min-height:120px;\">$the_price</ul>";								
				$the_subtotal = $the_subtotal + $sub_total;	
				//$grand_total = $grand_total + $sub_total;			
				
				
				/*$price_villa = $this->get_prices_excess_days_villa($id,$arrival_date,$derpature_date,$days,$data_program['ldate_start'],$data_program['ldate_end'],$days_prog);
				$sub_total   = $price + $price_villa;
				$grand_total = $grand_total + $sub_total;*/
				
				$prog_commance = get_additional_field($data_program['larticle_id'],'prog_commence','ratesreservations');
				$date_prog_commance = $arrival_date + ($prog_commance * 86400);
				$time_prog_commance = get_additional_field($prog_id,'program_commences','program-package');
				$dpc = date('d M Y',$date_prog_commance).' - '.$time_prog_commance;
				$date_prog_end = date('d M Y',$derpature_date).' – 5:00pm';
				
				$list_prog .= "<li>
								  <span>
									  <b>$name_program</b><br>
									  <small>Dates: $arrival - $derpature</small><br>
									  <small>Program commences: $dpc</small><br>
									  <small>Program ends: $date_prog_end</small><br>
									  <small class=\"thesl\">Villa:</small> <small>$villa_name</small><br>
									  $disc_desc
								  </span><div class=\"clear\"></div>
								</li>";
				$list_prog_price .=$temp_price;
				$list_excess_day .="<li>$excess_day</li>";
				$list_price_villa.="<li>$price_villa</li>";
				$list_total		 .="<li style=\"min-height:110px;\" >$sub_total</li>";
				$list_status	 .="<li>$status</li>";
			}
			$t->set_var('id_booking',$id_booking);
			$t->set_var('check',$id_booking);
			$t->set_var('list_prog',"<ul class=\"ul-prog-his\">".$list_prog."</ul>");
			$t->set_var('list_prog_price',"<ul class=\"same-height\">".$list_prog_price."</ul>");
			$t->set_var('list_excess_day',"<ul class=\"same-height\">".$list_excess_day."</ul>");
			$t->set_var('list_price_villa',"<ul class=\"same-height\">".$list_price_villa."</ul>");
			$t->set_var('list_total',"<ul class=\"same-height\">".$list_total."</ul>");
			$t->set_var('list_status',"<ul class=\"same-height\">".$list_status."</ul>");
			
			
			/*$list_booking = $this->get_data('lumonata_booking',"where lbook_id=$book_id");
			while($data_booking = $db->fetch_array($list_booking)){
				$dt_book = $this->villa_reservation->get_data_booking($data_booking);
				$t->set_var('villa_name',$dt_book['villa_name']);
				$t->set_var('arrival_date',$dt_book['arrival_date']);
				$t->set_var('derpature_date',$dt_book['derpature_date']);
				$t->set_var('night',$dt_book['days']);
				$total  = $dt_book['total'];
				$grand_total = $grand_total+$total;
				$t->set_var('total',$total);
				
				
				
				//$t->Parse('lbb', 'list_book_block', true);						
			}*/
			
			$t->set_var('book_id',$book_id);
			$t->set_var('book_status',$status);
			
			
			$is_paid = 0;
			$rest_paid = $grand_total;
			
			//find surcharge
			/*$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			if(!empty($data_surcharge)){
				
			}			
			$grand_total = $grand_total + $the_subtotal;*/
			
			$t->set_var('sub_total',number_format($the_subtotal,2));
			//find surcharge
			$data_surcharge = $this->single_villa_get_surcharge($arrival_date,$derpature_date,$the_subtotal,'array');
			$surcharge_temp ="";
			if(!empty($data_surcharge)){
				//$the_subtotal = $the_subtotal + $data_surcharge['the_surcharge'];
				$grand_total = $the_subtotal + $data_surcharge['the_surcharge'];
				//$surcharge_temp = td_surcharge_booking_detail($data_surcharge['the_surcharge'],$data_surcharge['temp'],$grand_total);
				$surcharge_text = number_format($data_surcharge['the_surcharge'],2);
				$grand_total_text = number_format($grand_total,2);
				$surcharge_temp = $this->td_surcharge_booking_detail($surcharge_text,$data_surcharge['temp'],$grand_total_text);
			}
			$t->set_var('surcharge_temp',$surcharge_temp);
			//$grand_total = $grand_total + $the_subtotal;
			//$t->set_var('grand_total',number_format($grand_total,2));
			//get_downpayment
			$downpayment = "";
			
			$data_dp = $this->get_downpayment_single_history($book_id);
			if(!empty($data_dp)){
				$downpayment = $data_dp['container'];
				$rest_paid = $rest_paid - $data_dp['dp'];
				$is_paid++;
			}
			$t->set_var("downpayment",number_format($downpayment,2));
			
			$fullpayment = "";
			$data_fp = $this->get_fullpayment_single_history($book_id);
			if(!empty($data_fp)){
				$fullpayment = $data_fp['container'];
				$rest_paid = $rest_paid - $data_fp['fp'];
				$is_paid++;
			}
			$t->set_var("fullpayment",$fullpayment);
			
			$cont_rest_paid = "";
			if($is_paid>0){
				$cont_rest_paid = $this->get_restpayment($rest_paid);
			}
			$t->set_var("restpayment",$cont_rest_paid);
			$this->villa_reservation->set_detail_booking_new($t,$book_id);
			//$this->set_detail_booking_new($t,$book_id);
			
			$result['process'] = 'success';
			$result['status']  = $status;
			$result['data'] = $t->Parse('shb', 'single_history_block', false);	
		}else{
			$result['process'] = 'failed';
		}		
		echo json_encode($result);
	}
	
	
	
	/*function set_detail_booking_new($t,$book_id){
		$detail_booking = $this->get_data('lumonata_booking_detail',"where lbook_id=$book_id",'array');
		$additional_service = json_decode($detail_booking['ladditional_service']);
		$text_additional_service="";
		foreach($additional_service as $as){
			
			if($text_additional_service!="") $text_additional_service .= ', '.$as;
			else $text_additional_service = $as;
		}
		if($text_additional_service=="")$text_additional_service="-";
		$t->set_var('additional_service',$text_additional_service);
		if($detail_booking['ladditional_cost']==1)	$t->set_var('additional_cost','Yes');
		else $t->set_var('additional_cost','No');
		if($detail_booking['lheard_about_us'] !="")$t->set_var('heard_about_us',$detail_booking['lheard_about_us']);
		else $t->set_var('heard_about_us',"-");
	}*/
	
	function td_surcharge_booking_detail($the_surcharge, $temp_surcharge,$grand_total){
		require_once("../booking-engine/apps/villa-reservation/class.villa_reservation.php");
		$this->villa_reservation = new villa_reservation();
		return $this->villa_reservation->td_surcharge_booking_detail($the_surcharge, $temp_surcharge,$grand_total);	
	}
	
	function single_villa_get_surcharge($arrival_date,$departure_date,$price,$return_type='array'){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->single_villa_get_surcharge($arrival_date,$departure_date,$price,$return_type);	
	}	
	
	function get_downpayment_single_history($book_id){
		$result = array();
		$data_dp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=0",'array');
		if(!empty($data_dp)){
			$downpayment = $data_dp['lgross'];
			$presentase  = $data_dp['lpresentase_dp'].'%';
			
			$dp_container = "<tr class=\"desc_gt\">
								<td colspan=\"2\">Down Payment ($presentase)</div>
								<td class=\"center\">$downpayment</div>
							</tr>";
			$result['container'] =  $dp_container;
			$result['dp'] = $downpayment;				
			//$t->set_var("downpayment",$dp_container);
		}
		
		return $result;
		
	}
	
	function get_fullpayment_single_history($book_id){
		$result = array();
		$data_fp = $this->get_data('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid=1",'array');
		if(!empty($data_fp)){
			$fullpayment = $data_fp['lgross'];
			
			$fp_container = "<tr class=\"desc_gt\">
								<td colspan=\"2\">Full Payment</td>
								<td class=\"center\">$fullpayment</td>
							</tr>";
			$result['container'] =  $fp_container;
			$result['fp'] = $fullpayment;				
			//$t->set_var("downpayment",$dp_container);
		}
		
		return $result;
		
	}
	
	
	
	function get_restpayment($rest_payement){
		$dp_container = "<tr class=\"desc_gt\">
							<td colspan=\"2\">Rest Payment</td>
							<td class=\"center\">$rest_payement</td>
						</tr>";
						
		return $dp_container;				
	}
	
	
		
	
	function get_data($t,$w,$r='result_only'){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->data_tabels($t,$w,$r);
	}
	
	function get_villa_name($villa_id){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->villa_room_type_name($villa_id);
	}
	
	function get_data_additional_villa($id,$type){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->get_data_additional_villa($id,$type);	
	}
		
	function single_villa_discount_and_earlybird($roomtype_id,$arrival_date,$departure_date,$price,$return_type,$date_now,$discount_for){
		require_once("../booking-engine/apps/general-func/class.general_func.php");
		$this->general_func = new general_func();
		return $this->general_func->single_villa_discount_and_earlybird($roomtype_id,$arrival_date,$departure_date,$price,$return_type,$date_now,$discount_for);
	}
		
	function price_villas($villa_id,$arrival_date,$departure_date,$days){
		$error = 0;
		$price = 0;
		for($i=0 ;$i<=$days ; $i++){
			$the_date = $arrival_date + ($i*86400);
			if($the_date >= $arrival_date && $the_date < $departure_date) {
				$data = $this->get_data('lumonata_availability',"where ldate=$the_date and lacco_id=$villa_id",'array');
				if(!empty($data)) $price = $price + $data['lrate'];
				else $error++;
			}
		}//end for
		
		if($error==0)return $price;
		else return 0;
	}
	
	function get_status_book_name($id=1){
		if($id==1) return 'Check Availability';
		else if($id==2)return 'Waiting Down Payment';
		else if($id==3)return 'Waiting Full Payment';
		else if($id==4)return 'Confirm';
		else if($id==5)return 'Unconfirm';
		else if($id==0)return 'Canceled';
	}
	
	function get_prices_excess_days_villa($villa_id,$arrival_date,$departure_date,$day_book,$start_prog_date,$end_prog_date,$day_prog){
		$one_day = 86400;
		//echo date('d M Y',$arrival_date).'-'.date('d M Y',$departure_date).'<br />';
		//echo $day_book.'<br/>';
		$price = 0;
		for($i=0; $i<=$day_book; $i++){
			$the_date_book = $arrival_date + ($i * $one_day);
			$match = 0;
			for($p=0; $p<=$day_prog; $p++){
				$the_date_prog = $start_prog_date + ($p * $one_day);
				//echo date('d M Y',$the_date_book).'-'.date('d M Y',$the_date_prog).'<br />';
				if($the_date_book == $the_date_prog){
					//echo date('d M Y',$the_date_prog).'<br />';
					$match++;	
				}
			}
			
			if($match==0){
				$data_availability_villa = $this->get_data('lumonata_availability',"where ldate=$the_date_book and lacco_id=$villa_id",'array');
				$price = $price + $data_availability_villa['lrate'];
			}
		}//end for
		
		return $price;
	}

	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
	
}

?>