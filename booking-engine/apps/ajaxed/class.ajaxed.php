<?php
	require_once '../../../lumonata_config.php';
	//require_once '../../../functions/db.php';
	
	class ajaxed extends db{
		function ajaxed($appName){
			$this->appName=$appName;
			$this->setMetaTitle("ajaxed");
			
			require_once("../../../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		
		function load(){	
			global $db;
			$q = $db->prepare_query("Select * From lumonata_meta_data Where lmeta_name = 'time_zone'");
			$r = $db->do_query($q);
			$d = $db->fetch_array($r);
			putenv("TZ=".$d['lmeta_value']);
			
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			 
			if( !empty($_POST)){
				if($_POST['act']=='facilitiesAdd'){
					$facilities = strtolower($_POST['name']);
					$str = $db->prepare_query("SELECT * FROM lumonata_accommodation_facilities WHERE lfacilities=%s", $facilities );
					$res = $db->do_query($str);
					$nnn = $db->num_rows($res);
					
					$time = time();
					if($nnn<=0){
						$str = $db->prepare_query("INSERT INTO lumonata_accommodation_facilities ( lfacilities, ldlu, lpublish ) VALUES ( %s,%d,%d )", $facilities, $time, 0 );
						if( $res = $db->do_query($str) ){
							$str = $db->prepare_query("SELECT * FROM lumonata_accommodation_facilities WHERE lfacilities=%s AND ldlu=%d", $facilities, $time);
							$res = $db->do_query($str);
							$rec = $db->fetch_array($res);
							
							$add = '';
							if($rec['lpublish']!=1){ $add = '<small style="float:left; margin-left:10px; color:#f00; font-size:0.9em;">waiting for moderation</small>'; }
							
							echo '<div class="one brandNew" id="theFacilities-'.$rec['lfacilities_ID'].'" rel="'.$rec['lfacilities'].'" ><p style="float:left;">'.$rec['lfacilities'].'</p>'.$add.'</div>';
						}else{
							echo 'error';
						}
					}else{
						echo '';
					}
				}
				
				if($_POST['act']=='allotmentEdit' || $_POST['act']=='rateEdit' || $_POST['act']=='availEdit'){
					$prt = explode('-',$_POST['date']);
					$thaDate = mktime(0,0,0,$prt[1],$prt[0],$prt[2]);
					
					$str = "SELECT * from lumonata_availability WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$qry = $db->prepare_query($str, $thaDate, $_POST['acco'], $_POST['type']);
					$res = $db->do_query($qry);
					$rec = $db->fetch_array($res);
					$eStatus = explode(';',$rec['ledit']);
				}
				
				if($_POST['act']=='allotmentEdit'){
					$newEstatus = '1'.';'.$eStatus[1];
					
					$str = "UPDATE lumonata_availability SET lallotment=%d, ledit=%s WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$qry = $db->prepare_query($str, $_POST['val'], $newEstatus, $thaDate, $_POST['acco'], $_POST['type']);
					//echo $qry ;
					$res_allotmentEdit = $db->do_query($qry);
					if($res_allotmentEdit){
						echo 'success';
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"calendar : edit allotment",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						}
					}else{
						echo 'error';
					}
				}
				if($_POST['act']=='rateEdit'){
					$newEstatus = $eStatus[0].';'.'1';
					
					$str = "UPDATE lumonata_availability SET lrate=%s, ledit=%s WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					$qry = $db->prepare_query($str, $_POST['val'], $newEstatus, $thaDate, $_POST['acco'], $_POST['type']);
					//echo $qry ;
					$res_rateEdit = $db->do_query($qry);
					if($res_rateEdit){
						echo 'success';
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"calendar : edit rate",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						}
					}else{
						echo 'error';
					}
				}
				
				
				if($_POST['act']=='availEdit'){
					$key = $_POST['val'];
					$status['available'] 	= 0;
					//$status['half']      	= 1;
					$status['no-in-out']    = 1;
					$status['booked'] 	 	= 2;
					$status['no-in']		= 3;
					$status['no-out']      	= 4;
					
					
					$str = "UPDATE lumonata_availability SET lstatus=%d, lreason=%s WHERE ldate=%d AND lacco_id=%d AND lacco_type_id=%d";
					
					$qry = $db->prepare_query($str, $status[$key], $_POST['rea'], $thaDate, $_POST['acco'], $_POST['type']);
					//echo $qry ;
					$res_availEdit = $db->do_query($qry);
					if($res_availEdit){
						//echo $qry;
						echo 'success';
						
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"calendar : edit availibility",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						}
					}else{
						echo 'error';
					}
				}					
			}
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
	$ajx=new ajaxed("ajaxed");
	$ajx->load();
?>