<?php
	class rate_delete extends db{
		function rate_delete($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Rate Delete");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
		}
		function load(){
			global $db;
			/*print_r($_GET); //, [bool return])
			echo "<br /><br />";
			print_r($_POST); //, [bool return])
			echo "<br /><br />";
			print_r($_SESSION);*/
			$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=$db->do_query($sql);
			$nnn=$db->num_rows($res);
			$user=$db->fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if (isset($_GET['act2'])){
				$rate_id = $_GET['act2'];
			}else{
				$rate_id = $_POST['rate_id'];
			}
			
			$query =  $db->prepare_query("SELECT b.lname as room_category 
										from lumonata_accommodation_rate_categories as a,
										lumonata_rules as b where 
										a.lacco_type_cat_id = b.lrule_id and a.lrate_cat_id=%d",$rate_id);					
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			
			$msg_succsess = "Delete rate for \"".$data['room_category']."\" was successfully processed.";
			$msg_error = "Delete rate for \"".$data['room_category']."\" was unsuccessfully processed.";
			$t->set_var('msg_succsess',$msg_succsess);
			$t->set_var('msg_error',$msg_error);
			
			if(!empty($_POST)){
				$selStrCat = "SELECT * FROM lumonata_accommodation_rate_categories WHERE lrate_cat_id =%d";
				//echo "<br >".
				$sqlCat    = $db->prepare_query($selStrCat, $rate_id);
				$resCat    = $db->do_query($sqlCat);
				$theRatCat = $db->fetch_array($resCat);
				
				$stmtCat 	= $db->prepare_query("DELETE FROM lumonata_accommodation_rate_categories WHERE lrate_cat_id =%d",$rate_id);
				$resultCat = $db->do_query($stmtCat);
				if($resultCat){
					$selStr = "SELECT * FROM lumonata_accommodation_rate WHERE lrate_cat_id =%d";
					$sql    = $db->prepare_query($selStr, $theRatCat['lrate_cat_id']);
					$res    = $db->do_query($sql);
					//$theRat = $db->fetch_array($res);
					
					$stmt = $db->prepare_query("DELETE FROM lumonata_accommodation_rate WHERE lrate_cat_id =%d",$theRatCat['lrate_cat_id']);
					$result = $db->do_query($stmt);
					//$result = 2;
					if($result){
						while ($theRat = $db->fetch_array($res)){
							$str = "DELETE FROM lumonata_availability WHERE lacco_type_id=%d";
							$sql = $db->prepare_query($str, $theRat['lacco_type_id']);
							$re2 = $db->do_query($sql);
							//echo $sql;
							if($re2){
								if($bySupplier){
									$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"rate : delete",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
								}
								
								/*
								$url = "http://".SUPPLIERPANEL_SITE_URL."/rate/";
								$t->set_var('jsAction', "notifBlockSaved(); 
								var val = 'content-typeEdit';
								var url = '$url';
								displayNoneId(val);
								WindowLocation(url);
								");
								*/
								//header("Location:".SUPPLIERPANEL_SITE_URL."/rate/");
							}	
						}				
					}else{
						/*
						$url = "http://".SUPPLIERPANEL_SITE_URL."/rate/";
						$t->set_var('jsAction', "notifFailedSaved();
						var val = 'content-typeEdit';
						var url = '$url';
						displayNoneId(val);
						WindowLocation(url);
						");	
						*/
					}
					$url = "http://".SUPPLIERPANEL_SITE_URL."/rate/";
							$t->set_var('jsAction', "notifBlockSaved(); 
							var val = 'content-typeEdit';
							var url = '$url';
							displayNoneId(val);
							WindowLocation(url);
							");
				}else{
					$url = "http://".SUPPLIERPANEL_SITE_URL."/rate/";
						$t->set_var('jsAction', "notifFailedSaved();
						var val = 'content-typeEdit';
						var url = '$url';
						displayNoneId(val);
						WindowLocation(url);
						");	
				}
				
				
				
				
			}else{
				//if (isset($_POST['pilih'][$i])){$pilih=$_POST['pilih'][$i];}else{$pilih=''}
				//$t->set_var('pilih',$pilih);
				//$t->set_var('i', $i);	
				$t->set_var('rate_id', $data['lrate_cat_id']);
				$t->set_var('name', "Rate for ".$data['room_category']);
				$save = 'save';
					$t->set_var('jsActionButton',"
					var val = 'save';
					displayNoneClass(val);
					");
			}
			
			return $t->Parse('mBlock', 'mainBlock', false);
		}

		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			//return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			//return $this->meta_key;
		}
	};
?>