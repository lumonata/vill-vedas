<?php
	class gallery extends db{
		function gallery($appName){
			$this->appName=$appName;
			$this->setMetaTitle("Gallery");
			
			require_once("../lumonata-admin/functions/globalAdmin.php");
            $this->globalAdmin=new globalAdmin();
			$this->to=$this->globalAdmin->getSettingValue('email');
			$this->cc=$this->globalAdmin->getSettingValue('cc');
			$this->bcc=$this->globalAdmin->getSettingValue('bcc');
			global $globalSetting;
			
			require_once("../lumonata-admin/functions/upload.php");
			$this->upload = new upload();
			$this->upload->upload_constructor(IMAGE_PATH."/Accommodations/");
		}
		function load(){
			/*print_r($_GET); //, [bool return])
			echo "<br /><br />";
			print_r($_POST); //, [bool return])
			echo "<br /><br />";
			print_r($_SESSION);
			*/
			$sql=parent::prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
			$res=parent::query($sql);
			$nnn=parent::num_rows($res);
			$user=parent::fetch_array($res);
			if ($nnn != 0){
				$key=$user['lproduct_id'];
				$_SESSION['product_type']=$user['lproduct_type'];
				$_SESSION['product_id']=$user['lproduct_id'];
				$_SESSION['upd_by']=$user['laccount_id'];
				$bySupplier = true;
			}else{
				$key=$_COOKIE['product_id'];
				$_SESSION['product_type']=$_COOKIE['product_type'];
				$_SESSION['product_id']=$key;
				$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];
				$bySupplier = false;	
			}
			
			if ($_POST['prc'] == "new" || $_POST['prc'] == "save_new"){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				//set block
				$t->set_block('home', 'mainBlock',  'mBlock');
				if($_POST['prc'] == "save_new"){
					if (!empty($_FILES['image']['name'][0])){
						$image_id = $this->genImageId();
						$image_name = $_FILES['image']['name'][0];
						$image_size = $_FILES['image']['size'][0];
						$image_type = $_FILES['image']['type'][0];
						$image_tmp = $_FILES['image']['tmp_name'][0];
						$sef_url = $this->upload->sef_url($image_id);
						$image = $this->upload->rename_file($image_name,$sef_url);
						$stmt = parent::prepare_query("INSERT INTO lumonata_accommodation_images (
							limage_id,lacco_id,ltitle,limage,lorder_id,
							lsef_url,lcreated_by,lcreated_date) VALUES (
							%s,%d,'".htmlentities($_POST['title'][0],ENT_QUOTES)."',%s,%d,
							%s,%s,%d)",
							$image_id,$_SESSION['product_id'],$image,1,
							$sef_url,$_SESSION['upd_by'],time());
						$this->globalAdmin->setOrderID("lumonata_accommodation_images",1);		
						$result = parent::query($stmt);
						$dimensions = $this->globalAdmin->getImageDimensions(212);
						//print_r($dimensions);
						//echo IMAGE_PATH; 
						//upload image
						$this->upload->upload_resize($image_name,$sef_url,$image_tmp,$image_type,$dimensions[0],$dimensions[1],0);
						$this->upload->upload_resize($image_name,$sef_url,$image_tmp,$image_type,$dimensions[2],$dimensions[3],1);
						$this->upload->upload_resize($image_name,$sef_url,$image_tmp,$image_type,$dimensions[4],$dimensions[5],2);
					}
					if($result){
						if($bySupplier){
							$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"gallery : new",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
						} 
						
						$t->set_var('notif_message', 'Data Saved');	
						$t->set_var('jsAction', 'notifBlockSaved();');	
					}else{
						$t->set_var('notif_message', 'Failed to saved');
						$t->set_var('jsAction', 'notifFailedSaved();');	
					}	
					
				}
				$t->set_var('i', 0);
				$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
				$t->set_var('title_h2', "New");
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}if ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit"){
				$OUT_TEMPLATE="main.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'editContent', 'eC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				
				//start edit process
				if($_POST['prc'] == "save_edit"){
					for($i=0;$i<count($_POST['image_id']);$i++){
							$stmt = parent::prepare_query("UPDATE lumonata_accommodation_images SET
								ltitle=%s,
								lusername=%s,
								ldlu=%d
								WHERE limage_id =%s",
								$_POST['title'][$i],
								$_SESSION['upd_by'],
								time(),
								$_POST['image_id'][$i]);
							$result = parent::query($stmt);
							if($result){
								$image_name = $_FILES['image']['name'][$i];
								$image_size = $_FILES['image']['size'][$i];
								$image_type = $_FILES['image']['type'][$i];
								$image_tmp = $_FILES['image']['tmp_name'][$i];
								$sef_img = $this->upload->sef_url($_POST['image_id'][$i]);
								$image = $this->upload->rename_file($image_name,$sef_img);
								if (!empty($_FILES['image']['name'][$i])){ //not empty image
									$stmtImages = parent::prepare_query("UPDATE lumonata_accommodation_images SET limage=%s WHERE limage_id =%s",$image,$_POST['image_id'][$i]);
									$resultImages = parent::query($stmtImages);
									if ($image_name != $_POST['image_old'][$i]){
										$this->upload->delete_file($_POST['image_old'][$i],0);
										$this->upload->delete_file($_POST['image_old'][$i],1);
										$this->upload->delete_file($_POST['image_old'][$i],2);
									}
									$dimensions = $this->globalAdmin->getImageDimensions(212); 
									$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[0],$dimensions[1],0);
									$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[2],$dimensions[3],1);
									$this->upload->upload_resize($image_name,$sef_img,$image_tmp,$image_type,$dimensions[4],$dimensions[5],2);
								}
								//end upadate existing images
								$msg .= "<li>Update \"".$_POST['title'][$i]."\" was successfully processed.</li>";
							}else{
								$error = 1;
								$msg .= "<li>Update \"".$_POST['title'][$i]."\" was unsuccessfully processed.</li>";
							}
					}// end for
						if($error==1){
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifFailedSaved();');	
						}else{
							$t->set_var('notif_message', "<ul>".$msg."</ul>");
							$t->set_var('jsAction', 'notifBlockSaved();');	
							if($bySupplier){
								$this->globalAdmin->sendAlertSupplier($user['lsalutation']." ".$user['lfname']." ".$user['llname'],"gallery",$key,$this->to,$this->cc,$this->bcc,$_SESSION['product_type']);
							} 
						}
				}
				//end edit process
				
				//start display data
				if (sizeof($_POST['pilih']) != 0){
					for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
						$query =  parent::prepare_query("SELECT *
							FROM lumonata_accommodation_images
							WHERE limage_id =%s",$_POST['pilih'][$i]);
						$result = parent::query($query);
						$data = parent::fetch_array($result);
						$t->set_var('pilih',$_POST['pilih'][$i]);
						$t->set_var('i', $i);	
						$t->set_var('image_id', $data['limage_id']);
						$t->set_var('title', $data['ltitle']);	 	
						$img = '{http}{rootsite_url}/images/Accommodations/'.$data['limage'];
						$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=152&h=103&src='.$img;
						$imgs = '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
						$existingImage = "<div class=\"wrapper form\" style=\"border-bottom:solid 1px #ccc;\">
									<label>Existing Image</label>
									<label style=\"width:160px; margin-right:10px;\">".$imgs."
									<input name=\"image_old[".$i."]\" id=\"image_old".$i."\" type=\"hidden\" value=\"".$data['limage']."\" />
									</label>
								</div>";
						$t->set_var('existingImage', $existingImage);
						$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-bottom:solid 1px #ccc; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
						$t->Parse('eC', 'editContent', true); 	
					}//end for
					$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");
				}else{
					$t->set_var('notif_message','<b>Attention</b><br />No any data selected');
					$t->set_var('jsAction', 'notifFailedSaved();');	
				}
				//end display data
				$t->set_var('title_h2', "Edit");
				
				
				return $t->Parse('mBlock', 'mainBlock', false);
				
			}else{
				$OUT_TEMPLATE="template.html";
				$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
				$t->set_file('home', $OUT_TEMPLATE);
				$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
				
				//set block
				$t->set_block('home', 'deleteContent', 'dC');
				$t->set_block('home', 'viewContent', 'vC');
				$t->set_block('home', 'mainBlock',  'mBlock');
				// BEGIN Confimation Delete
				if ($_POST['prc']=="delete"){
					if (sizeof($_POST['pilih']) != 0){
						for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
							$query = parent::prepare_query("SELECT *
							FROM lumonata_accommodation_images
							WHERE limage_id =%s",$_POST['pilih'][$i]);
							$result = parent::query($query);
							$data = parent::fetch_array($result);
							$t->set_var('i', $i);
							$t->set_var('delId', $_POST['pilih'][$i]);
							$n1 = 0;//$this->globalAdmin->getNumRows("lumonata_accommodation_images","lacco_type_id",$data['lacco_type_id']);
							if ($n1 != 0)
							{
								$title = $title."<li>".$data['ltitle']."</li>";
							}
							$alltitle = $alltitle."<li>".$data['ltitle']."</li>";
							$t->Parse('dC', 'deleteContent', true);
						}
						if (!empty($title)){
							$msg = "<div class=\"error\"><b>Sorry, image below using in accommodation rate data:</b><br /><ol>".$title."</ol>".$this->globalAdmin->setButtonBack()."</div>";	
							$t->set_var('notif_message', $msg);
							
						}else{
							$msg = "<div class=\"confirm\"><b>Do you really want to DELETE:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
							$t->set_var('notif_message', $msg);
							
						}
					}else{
						$t->set_var('notif_message', "<div class=\"confirm\"><b>Attention</b><br />No any data selected</div>");
					}
				
				}
				// END Confimation Delete
				// BEGIN Delete Process 
				if ($_POST['action']=="Yes"){
					for ($i=0; $i<sizeof($_POST['delId']); $i++) {
						//$msgTitle = $this->globalAdmin->getValueField("lumonata_accommodation_type","lname","lacco_type_id",$_POST['delId'][$i]); 
						$query = parent::prepare_query("SELECT *
						FROM lumonata_accommodation_images
						WHERE limage_id =%s",$_POST['delId'][$i]);
						$result = parent::query($query);
						$data = parent::fetch_array($result);
						$stmt = parent::prepare_query("DELETE FROM lumonata_accommodation_images WHERE limage_id =%s",$_POST['delId'][$i]);
						$result = parent::query($stmt);
						if($result){
							if (!empty($data['limage'])){
								$this->upload->delete_file($data['limage'],0);
								$this->upload->delete_file($data['limage'],1);
								$this->upload->delete_file($data['limage'],2);
							}
							$msg .= "<li>Delete \"".$data['ltitle']."\" was successfully processed.</li>";
						}else{
							$error = 1;
							$msg .= "<li>Delete \"".$data['ltitle']."\" was unsuccessfully processed.</li>";
						}
					}// end for
					if($error==1){
						$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
					}else{
						$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
					}
				}// END Delete Process 
				$query =  parent::prepare_query("SELECT *
						FROM lumonata_accommodation_images 
					WHERE lacco_id = %d
					ORDER BY lorder_id",$_SESSION['product_id']);
				//BEGIN set var on viewContent Block
				$result = parent::query($query);
				while($data = parent::fetch_array($result)){
					$t->set_var('no', $no);	 	
					$t->set_var('check', $data['limage_id']);	 		
					$t->set_var('title', $data['ltitle']);
					$img    = '{http}{rootsite_url}/images/Accommodations/'.$data['limage'];
					$imgurl = '{http}{rootsite_url}/images/tb/tb.php?w=152&h=103&src='.$img;
					$imgs   = '<a class="fancybox" rel="gallery" href="'.$img.'"><img src="'.$imgurl.'" /></a>';
					$t->set_var('imgs', $imgs);	 		 	
					$t->Parse('vC', 'viewContent', true); 	
				}
			 //END set var on viewContent Block
			return $t->Parse('mBlock', 'mainBlock', false);
			}
		}
		
		function genImageId(){
			$query = parent::prepare_query("SELECT MAX(substring(limage_id,5,5)) 
							FROM lumonata_accommodation_images 
							WHERE substring(limage_id,1,2)=%s and substring(limage_id,3,2)=%s",
							date("y",time()),date("m",time()));
			$result = parent::query($query);
			$data = parent::fetch_array($result);
			
			if($data[0]==NULL){
				$code=1;	
			}else{
				$code=$data[0]+1;
				
			}
			$mixcode=date("y",time()).date("m",time());
			
			if($code < 10){
				$mixcode=$mixcode."0000".$code;
			}elseif($code<100){
				$mixcode=$mixcode."000".$code;
			}elseif($code<1000){
				$mixcode=$mixcode."00".$code;
			}elseif($code<10000){
				$mixcode=$mixcode."0".$code;
			}elseif($code<100000){
				$mixcode=$mixcode.$code;
			}
			return $mixcode;
		}
		
		function setMetaTitle($metaTitle=''){
			$this->meta_title=$metaTitle;
		}
		function getMetaTitle(){
			return $this->meta_title;
		}
		function setMetaDescriptions($metaDesc=''){
			$this->meta_desc=$metaDesc;
		}
		function getMetaDescriptions(){
			return $this->meta_desc;
		}
		function setMetaKeywords($metaKey=''){
			$this->meta_key=$metaKey;
		}
		function getMetaKeywords(){
			return $this->meta_key;
		}
	};
?>