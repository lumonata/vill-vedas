<?php 
class special_offer extends db{
	function special_offer($appName){
		$this->appName=$appName;
		$this->setMetaTitle("Special Offer");
		
		require_once("../lumonata-admin/functions/globalAdmin.php");
		$this->globalAdmin = new globalAdmin();
		$this->to=$this->globalAdmin->getSettingValue('email');
		$this->cc=$this->globalAdmin->getSettingValue('cc');
		$this->bcc=$this->globalAdmin->getSettingValue('bcc');
		global $globalSetting;
		
		require_once("../booking-engine/apps/special-offer-ajax/class.ajax_special_offer.php");
		$this->ajax_special_offer = new ajax_special_offer();
		
		require_once("../booking-engine/apps/special-offer-general/class.special_offer_general.php");
		$this->special_offer_general = new special_offer_general();
	}
	
	function load(){
		if(!empty($_POST['act']) && isset($_POST['do_act'])){
			$do_act  = $_POST['do_act'];
			if($do_act=='get_addtional_service_price') return $this->ajax_special_offer->get_additional_service_price();
		}else{
			return $this->set_template();
		}
	}
	
	function set_template(){
		global $db;
		if(isset($_POST['prc']) && ($_POST['prc'] == "new" || $_POST['prc'] == "save_new")){//new additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'mainBlock',  'mBlock');
			//$this->show_empty_price_time($t);
			$t->set_var('parent', $this->special_offer_general->get_parent(0,0,0));
			$t->set_var('site_url','http://'.SITE_URL);
			$t->set_var('room_category',$this->get_room_type());
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_new\" type=\"submit\">");
			$t->set_var('title', "New");			
			$t->set_var('i', 0);
			$t->set_var('additional_service_list',$this->special_offer_general->get_list_additional_service(0));
			
			if(isset($_POST['acco_type_cat_id'])) $this->save_special_offer($t);		
		}elseif(isset($_POST['prc']) && ($_POST['prc'] == "edit" || $_POST['prc'] == "save_edit")){//edit additional service
			$OUT_TEMPLATE="main.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			//set block
			$t->set_block('home', 'editContent', 'eC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if($_POST['prc'] == "save_edit"){$this->edit_multiple_data($t);}	
			if (sizeof($_POST['pilih']) != 0){$this->show_multiple_edit($t);}//show multiple edit
			
			$t->set_var('title', "Edit");			
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			$t->set_var('button_save', "<input class=\"btn_save\" name=\"prc\" value=\"save_edit\" type=\"submit\">");	
			return $t->Parse('mBlock', 'mainBlock', false);
		}elseif(isset($_POST['prc']) && $_POST['prc'] == "category" ){//edit additional service
			header('Location: http://'.SITE_URL.'/booking-engine/special-offer-category/');
		}else{	
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_APP_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);
			$t->set_var('supplier_site_url',SUPPLIERPANEL_SITE_URL);
			
			//set block
			$t->set_block('home', 'deleteContent', 'dC');
			$t->set_block('home', 'viewContent', 'vC');
			$t->set_block('home', 'mainBlock',  'mBlock');
			
			if (isset($_POST['prc']) && $_POST['prc']=="delete" && sizeof($_POST['pilih']) != 0) $this->show_delete($t);
			if (isset($_POST['action']) && $_POST['action']=="Yes")	$this->do_delete($t);				
			
			
			$theQStr = $db->prepare_query("select a.*,b.lcategory from lumonata_special_offer as a, lumonata_special_offer_categories as b
											where a.lcat_id=b.lcat_id");
			//BEGIN set var on viewContent Block
			$result = $db->do_query($theQStr);
			
			while($data = $db->fetch_array($result)){
				$t->set_var('check', $data['lspecial_id']);
				$t->set_var('category',$data['lcategory']);
				$t->set_var('room_category',$this->get_room_type($data['lacco_type_cat_id'],'get_name'));
				$t->set_var('price',$data['lprice']);
				$t->set_var('start_date',date("d-m-Y",$data['ldate_start']));
				$t->set_var('end_date',date("d-m-Y",$data['ldate_valid']));
				$t->Parse('vC', 'viewContent', true);
			}
			
			//END set var on viewContent Block	
		}//end if validate post
		return $t->Parse('mBlock', 'mainBlock', false);	
	}
	
	/*Maintance Function*/
	function save_special_offer($t){
		global $db;
		
		$cat_id = $_POST['parent'][0];
		$acco_type_cat_id = $_POST['acco_type_cat_id'][0];
		$new_name = "";
		if(isset($_FILES['file_0'])){
			$name_images = explode('.',$_FILES['file_0']['name']);
			$new_name = 'temporary.'.$name_images[1];
			$folder = ROOT_PATH."/lumonata-content/files/special_offer_thumb/";
			if(!is_dir($folder)){
				 mkdir($folder);	
			}
			//move_uploaded_file($_FILES['file_0']['tmp_name'],$folder.$new_name);
		}
		$target		= '_self';
		$price		= $_POST['price'][0]; 
		$days		= $_POST['days'][0]; 
		$nights		= $_POST['nights'][0]; 
		
		$period_day_arrival		= $_POST['period_day_arrival'][0]; 
		$start_date		= strtotime($_POST['start_date'][0]); 
		$date_finish	= strtotime($_POST['date_finish'][0]);
		$desc		= $_POST['description'][0];
		$publish	= 1;
		$start_publish = strtotime($_POST['start_publish'][0]);
		$end_publish   = strtotime($_POST['end_publish'][0]);
		$user 		   = $_COOKIE['username'];
		$link		   = $_POST['link'][0];
		
		$str = $db->prepare_query("insert into lumonata_special_offer 
								   (lcat_id,lacco_type_cat_id,ldescription,limage,ltarget,
								    lwebsite, lprice,lday,lnight,lperiod,
									ldate_start,ldate_valid,lstatus,lsef_url,lorder,
									lpublish,lpublish_up,lpublish_down,lcreated_by,lcreated_date,
									lusername,ldlu) values 
									(%d,%s,%s,%s,%s,
									 %s,%d,%d,%d,%d,
									 %d,%d,%d,%s,%d,
									 %d,%d,%d,%s,%d,
									 %s,%d)",
									 $cat_id,$acco_type_cat_id,$desc,$new_name,$target,
									 '',$price,$days,$nights,$period_day_arrival,
									 $start_date,$date_finish,$publish,'',0,
									 $publish,$start_publish,$end_publish,$user,time(),
									 $user,time());
									 
		$result = $db->do_query($str);							 
		$special_id=mysql_insert_id();
		if($result){
			//upload image
			if(isset($_FILES['file_0'])){
				$name_images = explode('.',$_FILES['file_0']['name']);
				$new_name = $special_id.'.'.$name_images[1];
				$folder = ROOT_PATH."/lumonata-content/files/special_offer_thumb/";
				if(!is_dir($folder)){
					 mkdir($folder);	
				}
				move_uploaded_file($_FILES['file_0']['tmp_name'],$folder.$new_name);
				$this->update_image_special_offer($special_id,$new_name);
			}
			
			if(!empty($_POST['get_price_additional_service'][0])){
				foreach($_POST['get_price_additional_service'][0] as $key =>$additional_service_data){
					$package_id = $additional_service_data;					
					$qty = $_POST['qty_additional_service_'.$package_id][0];
					$days = 0;
					
					if(isset($_POST['days_additional_service_'.$package_id])) $days = $_POST['days_additional_service_'.$package_id][0];
					$price = $this->special_offer_general->get_additional_service_price_server($package_id,$qty,$days);
					$this->save_special_offer_additional_service($special_id,$package_id,$qty,$days,$price);
				}	
			}
			
			$t->set_var('notif_message', 'data saved');	
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}else{
			$t->set_var('notif_message', 'failed to saved');
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}
	}
	
	function update_image_special_offer($id,$name){
		global $db;
		$str = $db->prepare_query("update lumonata_special_offer set limage=%s where lspecial_id=%d",$name,$id);
		$db->do_query($str);
	}
	
	function save_special_offer_additional_service($special_id,$package_id,$qty,$days,$price){
		global $db;
		$str = $db->prepare_query("insert into lumonata_special_offer_additional_services 
								   (lspecial_id,lpackage_id,lprice,lqty,llday,lduration) values
								   (%d,%s,%d,%d,%d,%d)",
									$special_id,$package_id,$price,$qty,$days,0);
									
		
		$db->do_query($str);								
	}
	
	function show_multiple_edit($t){
		global $db;
		if(isset($_POST['pilih'])) $data_pilih = $_POST['pilih'];
		else $data_pilih = array($_GET['act2']);
		//for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
		for ($i=0; $i<sizeof($data_pilih); $i++) { 
			$t->set_var('separate', "<div class=\"wrapper form table\" style=\"margin-top:-1px; border-top:solid 1px #ccc; background:#f5f5f5;\"></div>");
			$t->set_var('i',$i);
			$id = $data_pilih[$i];
			$t->set_var('so_id',$id);
			$t->set_var('pilih',$id);
			$query = $db->prepare_query("select a.*,b.lcategory from lumonata_special_offer as a, lumonata_special_offer_categories as b
											where a.lcat_id=b.lcat_id and a.lspecial_id=%d",$id);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);								
			$t->set_var('parent', $this->special_offer_general->get_parent($data['lcat_id'],$i));
			$t->set_var('room_category',$this->get_room_type($data['lacco_type_cat_id'],'get_list'));
			$t->set_var('site_url','http://'.SITE_URL);	
			$t->set_var('days',$data['lday']);
			$t->set_var('nights',$data['lnight']);	
			$t->set_var('price',$data['lprice']);	
			$t->set_var('period_day_arrival',$data['lperiod']);	
			$t->set_var('start_date',date('m/d/Y',$data['ldate_start']));	
			$t->set_var('date_finish',date('m/d/Y',$data['ldate_valid']));		
			$t->set_var('description', $data['ldescription']);
			if($data['lpublish']==1)$t->set_var('publish', 'checked="checked"');
			$t->set_var('start_publish',date('m/d/Y',$data['lpublish_up']));	
			$t->set_var('end_publish',date('m/d/Y',$data['lpublish_down']));		
			$t->set_var('link', $data['lwebsite']);
			
			$data_selected_additional_service = array();
			//get selected_additional_service
			$datas = $this->data_tabel('lumonata_special_offer_additional_services',"where lspecial_id=$id");
			while($sas = $db->fetch_array($datas)){
				$data_selected_additional_service[$sas['lpackage_id']] = $sas;
			}
			$t->set_var('additional_service_list',$this->special_offer_general->get_list_additional_service($i,$data_selected_additional_service));
			$t->Parse('eC', 'editContent', true); 
		}
	}
	
	function edit_multiple_data($t){
		global $db;
		$msg = "";
		$error = 0;
		
		for($i=0;$i<count($_POST['so_id']);$i++){
			$so_id = $_POST['so_id'][$i];
			$cat_id = $_POST['parent'][$i];
			$acco_type_cat_id = $_POST['acco_type_cat_id'][$i];
			$new_name = "";
			$folder = ROOT_PATH."/lumonata-content/files/special_offer_thumb/";
			if(isset($_FILES['file_'.$i])){
				$name_images = explode('.',$_FILES['file_'.$i]['name']);
				$new_name = $so_id.'.'.$name_images[1];
				
				if(!is_dir($folder)){
					 mkdir($folder);	
				}
				//move_uploaded_file($_FILES['file_0']['tmp_name'],$folder.$new_name);
			}else{//delete image
				$data_so = $this->data_tabel('lumonata_special_offer',"where lspecial_id=$so_id",'array');
				$new_name = $data_so['limage'];
			}
			
			$target		= '_self';
			$price		= $_POST['price'][$i]; 
			$days		= $_POST['days'][$i]; 
			$nights		= $_POST['nights'][$i]; 
			
			$period_day_arrival		= $_POST['period_day_arrival'][$i]; 
			$start_date		= strtotime($_POST['start_date'][$i]); 
			$date_finish	= strtotime($_POST['date_finish'][$i]);
			$desc		= $_POST['description'][$i];
			if(isset($_POST['publish'][$i]))$publish = 1;
			else $publish = 0;
			$start_publish = strtotime($_POST['start_publish'][$i]);
			$end_publish   = strtotime($_POST['end_publish'][$i]);
			$user 		   = $_COOKIE['username'];
			$link		   = $_POST['link'][$i];
			
			$str = $db->prepare_query("update lumonata_special_offer set
									   lspecial_id=%d,lcat_id=%d,lacco_type_cat_id=%s,ldescription=%s,limage=%s,
									   ltarget=%s,lwebsite=%s,lprice=%d,lday=%d,lnight=%d,
									   lperiod=%d,ldate_start=%d,ldate_valid=%d,lstatus=%d,lsef_url=%s,
									   lorder=%d,lpublish=%d,lpublish_up=%d,lpublish_down=%d,lusername=%s,
									   ldlu=%d where lspecial_id=%d",
									   $so_id,$cat_id,$acco_type_cat_id,$desc,$new_name,
									   $target,$link,$price,$days,$nights,
									   $period_day_arrival,$start_date,$date_finish,$publish,'',
									   0,$publish,$start_publish,$end_publish,$user,
									   time(),$so_id);
							   
									   
			$result = $db->do_query($str);						   
			if($result){
				//upload image
				if(isset($_FILES['file_'.$i])){
					$name_images = explode('.',$_FILES['file_'.$i]['name']);
					$new_name = $so_id.'.'.$name_images[1];
					$folder = ROOT_PATH."/lumonata-content/files/special_offer_thumb/";
					move_uploaded_file($_FILES['file_0']['tmp_name'],$folder.$new_name);					
				}
				//delete additional data
				$this->delete_special_offer_additional_service($so_id);
				//save additional data
				if(!empty($_POST['get_price_additional_service'][$i])){
					foreach($_POST['get_price_additional_service'][$i] as $key =>$additional_service_data){
						$package_id = $additional_service_data;					
						$qty = $_POST['qty_additional_service_'.$package_id][$i];
						$days = 0;
						
						if(isset($_POST['days_additional_service_'.$package_id])) $days = $_POST['days_additional_service_'.$package_id][$i];
						$price = $this->special_offer_general->get_additional_service_price_server($package_id,$qty,$days);
						$this->save_special_offer_additional_service($so_id,$package_id,$qty,$days,$price);
					}	
				}
				$msg = "<li>Update was successfully processed.</li>";	
			}else{
				$error=1;
				$msg = "<li>Update was failed processed.</li>";	
			}	
		}
		if($error==1){
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifFailedSaved();');	
		}else{
			$t->set_var('notif_message', "<ul>".$msg."</ul>");
			$t->set_var('jsAction', 'notifBlockSaved();');	
		}
		
		
	}
	
	function show_delete($t){
		global $db;
		$alltitle = '';
		
		for ($i=0; $i<sizeof($_POST['pilih']); $i++) { 
			$query = $db->prepare_query("SELECT * FROM lumonata_special_offer where lspecial_id =%d",$_POST['pilih'][$i]);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			$t->set_var('i', $i);
			$t->set_var('delId', $_POST['pilih'][$i]);
			$n1 = 0;
			
			if ($n1 != 0)
			{
				$title = $title."<li>".date('d-m-Y',$data['ldate_start'])." - ".date('d-m-Y',$data['ldate_valid'])."</li>";
			}
			$alltitle = $alltitle."<li>".date('d-m-Y',$data['ldate_start'])." - ".date('d-m-Y',$data['ldate_valid'])."</li>";
			$t->Parse('dC', 'deleteContent', true);
		}
		$msg = "<div class=\"confirm\"><b>Do you really want to DELETE Special Offer on:</b><br /><ol>".$alltitle."</ol>".$this->globalAdmin->setButtonYes()." ".$this->globalAdmin->setButtonNo()."</div>";
		$t->set_var('notif_message', $msg);
	}
	
	function do_delete($t){
		global $db;
		$msg = '';
		$error = '';
		if(isset($_POST['delId'])) $data_pilih = $_POST['delId'];
		else $data_pilih = array($_POST['so_id']);
		
		for ($i=0; $i<sizeof($data_pilih); $i++) {
			$str = $db->prepare_query("select * from lumonata_special_offer where lspecial_id=%d",$data_pilih[$i]);
			$result = $db->do_query($str);
			$data = $db->fetch_array($result);
			
			$str_delete = $db->prepare_query("delete from lumonata_special_offer where lspecial_id=%d",$data_pilih[$i]);
			$result_delete = $db->do_query($str_delete);
			if($result_delete) {
				$folder = ROOT_PATH."/lumonata-content/files/special_offer_thumb/";
				$image = $data['limage'];
				unlink($folder.$image);
				//delete addtional service
				$this->delete_special_offer_additional_service($data_pilih[$i]);	
				$msg .= "<li>Delete Special Offer on \"".date('d-m-Y',$data['ldate_start'])." - ".date('d-m-Y',$data['ldate_valid'])."\" was successfully processed.</li>";
			}else{
				$error = 1;
				$msg .= "<li>Delete Special Offer on \"".date('d-m-Y',$data['ldate_start'])." - ".date('d-m-Y',$data['ldate_valid'])."\" was unsuccessfully processed.</li>";
			}
		}
		
		if($error==1){
			$t->set_var('notif_message', "<div class=\"error\"><b>Failed!</b><ol>".$msg."</ol></div>");
		}else{
			$t->set_var('notif_message', "<div class=\"success\"><b>Success!</b><ol>".$msg."</ol></div>"); 	
		}
	}
	
	
	/*Ajax Function*/
	
	
	/*General function*/
	
	
	
	function delete_special_offer_additional_service($id){
		global $db;
		$str = $db->prepare_query("delete from lumonata_special_offer_additional_services where lspecial_id=%d",$id);	
		$result = $db->do_query($str);
	}
	
	function get_category_type_data($action,$id=0){
		require_once("../booking-engine/apps/list_category_type_villa/class.list_category_type_villa.php");
		$this->list_category_type_villa = new list_category_type_villa();
		if($action=='get_list') return $this->list_category_type_villa->load($id);	
		else if($action=='get_name') return $this->list_category_type_villa->get_name_category_type_villa($id);
	}
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function get_room_type($id,$action='get_list'){
		require_once("../booking-engine/apps/general-category-villa/class.general_category_villa.php");
		$this->general_category_villa = new general_category_villa();
				
		if($action=='get_list') return $this->general_category_villa->get_category_type_data($id);	
		else if($action=='get_name') return $this->general_category_villa->get_category_name($id);	
	}
	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
}
?>