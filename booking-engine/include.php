<?php

require_once("../lumonata-classes/template.inc");
require_once("../lumonata_config.php");
require_once(ROOT_PATH."/lumonata_settings.php");
require_once(ROOT_PATH."/lumonata-functions/settings.php");



//SUPPPLIER PANEL
define('SITE_URL',get_meta_data('site_url'));
define('SSITE_URL',get_meta_data('site_url'));
define('SUPPLIERPANEL_SITE_URL'		,SSITE_URL.'/booking-engine');
define('SUPPLIERPANEL_ROOT_PATH'	,ROOT_PATH.'/booking-engine');

define('SUPPLIERPANEL_IMAGE_URL',SUPPLIERPANEL_SITE_URL.'/images');
define('SUPPLIERPANEL_THEME_URL',SUPPLIERPANEL_SITE_URL.'/templates');
define('SUPPLIERPANEL_APPS_URL'	,SUPPLIERPANEL_SITE_URL.'/apps');
define('SUPPLIERPANEL_JS_URL'	,SUPPLIERPANEL_SITE_URL.'/js');
define('SUPPLIERPANEL_APP_TEMPLATE_URL'		,SUPPLIERPANEL_ROOT_PATH.'/apps');
define('SUPPLIERPANEL_PLUGIN_TEMPLATE_URL'	,SUPPLIERPANEL_ROOT_PATH.'/plugin');
define('SUPPLIERPANEL_APP_PATH'				,SUPPLIERPANEL_ROOT_PATH.'/apps');
define('SUPPLIERPANEL_TEMPLATE_URL'			,SUPPLIERPANEL_ROOT_PATH.'/templates');
define('SUPPLIERPANEL_PLUGIN_SITE_URL'	,SUPPLIERPANEL_SITE_URL.'/plugin');
define('DEFAULT_PRICE_VILLA',100);
define('DEFAULT_SHOW_CONTENT',100);

define('SMTP_SERVER',get_meta_data('smtp'));

/*echo '<!--'.SMTP_SERVER.'-->';*/

/*SET TIMEZONE*/
set_timezone(get_meta_data('time_zone'));


require_once("../lumonata-functions/functions/navigationMenu.php");	
require_once("../lumonata-functions/functions/searchDir.php");	
require_once("../lumonata-functions/functions/globalSetting.php");
require_once("../lumonata-functions/functions/lumonata_functions.php");
require_once("../lumonata-admin/functions/globalAdmin.php");
require_once("../lumonata-admin/functions/upload.php");
//require_once("functions/paging.php");

//$globalAdmin=new globalAdmin();
//$paging=new paging();
$upload = new upload();
//$imageFolder = $globalAdmin->getImageFolder($mod);
//$upload->upload_constructor(IMAGE_PATH."/File_order");

//$banners=new banners("Banners");
$globalSetting=new globalAdmin();
$navigationMenu=new navigationMenu(SITE_URL);
$searchDir =new searchDir();

//define("COMMENT_STATUS",$globalSetting->getCommentStatus());
//require_once("../apps/Comments/class.comments.php");
/*SET DEFAULT META DATA*/
$globalSetting->setMetaTitle(''); 
$META_TITLE=$globalSetting->getMetaTitle();
$globalSetting->setMetaDescriptions('');
$META_DESC=$globalSetting->getMetaDescriptions();
$globalSetting->setMetaKeywords('');
$META_KEY=$globalSetting->getMetaKeywords();

/*DEFINE LANGUAGE*/
if(!empty($_REQUEST['lang']))
	define('LANGUAGE',$_REQUEST['lang']);
else
	define('LANGUAGE','en');

require("template.php");
?>