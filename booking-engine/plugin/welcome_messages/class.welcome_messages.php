<?php
	class welcome_messages extends db {
		var $appName;
		function welcome_messages($appName){
			$this->appName=$appName;
			$this->template_var="welcome_messages";
		}
		
		function load(){
			global $db;
			$OUT_TEMPLATE="template.html";
			$t=new Template(SUPPLIERPANEL_PLUGIN_TEMPLATE_URL.'/'.$this->appName);
			$t->set_file('home', $OUT_TEMPLATE);

			//set block
			$t->set_block('home', 'mainBlock', 'mBlock');	
			if (isset($_COOKIE['member_logged_ID'])){
				$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
				$res=$db->do_query($sql);
				$nnn=$db->num_rows($res);
				if ($nnn != 0){ //Get Supplier Member
					$memberData=$db->fetch_array($res);
					$t->set_var('name', $memberData['lfname'].' '.$memberData['llname']);
					$t->set_var('time', date('j F Y', $memberData['llast_login']).' at '.date('H:i', $memberData['llast_login']) );
				}else{ //Get Administrator Admin
					$sql=$db->prepare_query("SELECT * FROM lumonata_users WHERE lusername='".$_COOKIE['member_logged_ID']."'");
					$res=$db->do_query($sql);
					$memberData=$db->fetch_array($res);
					$t->set_var('name', $memberData['ldisplay_name']);
					//$t->set_var('time', date('j F Y', $memberData['llastvisit_date']).' at '.date('H:i', $memberData['llastvisit_date']) );
				}
				//parse block
				return $t->Parse('mBlock', 'mainBlock', false);
			}
		}
				
		function getTemplateVar(){
			return $this->template_var;
		}
	};
?>