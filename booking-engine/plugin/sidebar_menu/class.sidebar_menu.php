<?php
	class sidebar_menu extends db {
		var $appName;

		function sidebar_menu($appName=''){
			$this->appName=$appName;
			$this->template_var="sidebar_menu";
		}
		
		function load(){
			if (isset($_COOKIE['member_logged_ID'])){
				global $db;
				$sql=$db->prepare_query("SELECT * FROM lumonata_members WHERE lmember_id='".$_COOKIE['member_logged_ID']."'");
				$res=$db->do_query($sql);
				$nnn=$db->num_rows($res);
				$user=$db->fetch_array($res);
				if ($nnn != 0){
					$key=$user['lproduct_id'];
					$_SESSION['product_type']=$user['lproduct_type'];
					$_SESSION['product_id']=$user['lproduct_id'];
					$_SESSION['upd_by']=$user['laccount_id'];
				}else{
					$key=$_COOKIE['product_id'];
					$_SESSION['product_type']=$_COOKIE['product_type'];
					$_SESSION['product_id']=$key;
					$_SESSION['upd_by']=$_COOKIE['member_logged_ID'];	
				}
				
				if($_SESSION['product_type']==1 || $_SESSION['product_type']==2){
					
					$OUT_TEMPLATE="template.html";
					$t=new Template(SUPPLIERPANEL_PLUGIN_TEMPLATE_URL.'/'.$this->appName);
					$t->set_file('home', $OUT_TEMPLATE);
					//set block
					$t->set_block('home', 'mainBlock', 'mBlock');
					$data_url = explode('/',$_SERVER['REQUEST_URI']);
					if(SITE_URL=='localhost/villa-vedas') $t->set_var('selected_menu',$data_url[3]);
					else $t->set_var('selected_menu',$data_url[3]);
					
					$num_all_check_availability  = $this->data_table('lumonata_accommodation_booking',"where lstatus=1 group by lbook_id",'num_row');
					$num_villa_check_availability  = $this->data_table('lumonata_accommodation_booking',"where lstatus=1 and lbook_type='villa' group by lbook_id",'num_row');
					$num_prog_check_availability  = $this->data_table('lumonata_accommodation_booking',"where lstatus=1 and lbook_type='prog_villa' group by lbook_id",'num_row');
					
					$all_badge = "";$villa_badge="";$prog_badge="";
					if($num_all_check_availability > 0) $t->set_var('all_badge',"<span class=\"badge\">$num_all_check_availability</span>");
					if($num_villa_check_availability > 0) $t->set_var('villa_badge',"<span class=\"badge\">$num_villa_check_availability</span>");
					if($num_prog_check_availability > 0) $t->set_var('prog_badge',"<span class=\"badge\">$num_prog_check_availability</span>");
					
					if($_SESSION['product_type']==1){
						$t->set_var('accoType', 'Hotel');
					}else{
						$t->set_var('accoType', 'Villa');
					}
					
					$t->set_var('url_admin','http://'.SITE_URL.'/lumonata-admin/');
				}else{
					$OUT_TEMPLATE="template.html";
					$t=new Template(SUPPLIERPANEL_PLUGIN_TEMPLATE_URL.'/'.$this->appName);
					$t->set_file('home', $OUT_TEMPLATE);
					
					//set block
					$t->set_block('home', 'mainBlock', 'mBlock');
				}
				
				//parse block
				return $t->Parse('mBlock', 'mainBlock', false);
			
			}
		}
		
		function getTemplateVar(){
			return $this->template_var;
		}
		
		function data_table($table,$where,$return = 'result'){
			global $db;
			$str =  $db->prepare_query("select * from $table $where");
			$result = $db->do_query($str);
			if($return=='result')return $result;				
			else if($return=='array') return $db->fetch_array($result);
			else if($return=='num_row') return $db->num_rows($result);
			
		}
		
	};
?>