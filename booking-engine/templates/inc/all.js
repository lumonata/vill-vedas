jQuery(document).ready(function(){
	
	jQuery('.transOverlay').css({opacity:0.7});
	<!--jQuery('a.fancybox').fancybox();-->
	
	jQuery('.tabsContainer .content .oneContent').css({display:'none'});
	jQuery('#content-rateAndRoomType').css({display:'block'});
	
	jQuery('.tabsContainer .theTabs a').click(function(){
		jQuery('.tabsContainer .theTabs a.active').removeClass('active');
		
		tabMenuID = jQuery(this).attr('id');
		var tabID = tabMenuID.split('-');
		
		jQuery('.tabsContainer .content .oneContent').css({display:'none'});
		jQuery('#content-'+tabID[1]).css({display:'block'});
		
		jQuery(this).addClass('active');
	});
	
	
	//zebraTable
	jQuery('.headsUp .wrapper.bordered:odd').css({background:'#f2f7f6'});
	
	//MY ACCOUNT part ============================================
	jQuery('#cont-bookHistory, #cont-billingHistory, #cont-earningHistory').css({display:'none'});
	
	jQuery('a#tab-yourDetails').click(function(){
		jQuery('#cont-yourDetails').css({display:'block'});
		jQuery('#cont-bookHistory').css({display:'none'});
		jQuery('#cont-billingHistory').css({display:'none'});
		jQuery('#cont-earningHistory').css({display:'none'});
		
		jQuery('a#tab-bookHistory, a#tab-billingHistory, a#tab-earningHistory').removeClass('active');
		jQuery(this).addClass('active');
	});
	jQuery('a#tab-bookHistory').click(function(){
		jQuery('#cont-yourDetails').css({display:'none'});
		jQuery('#cont-bookHistory').css({display:'block'});
		jQuery('#cont-billingHistory').css({display:'none'});
		jQuery('#cont-earningHistory').css({display:'none'});
		
		jQuery('a#tab-yourDetails, a#tab-billingHistory, a#tab-earningHistory').removeClass('active');
		jQuery(this).addClass('active');
	});
	jQuery('a#tab-billingHistory').click(function(){
		jQuery('#cont-yourDetails').css({display:'none'});
		jQuery('#cont-bookHistory').css({display:'none'});
		jQuery('#cont-billingHistory').css({display:'block'});
		jQuery('#cont-earningHistory').css({display:'none'});
		
		jQuery('a#tab-yourDetails, a#tab-bookHistory, a#tab-earningHistory').removeClass('active');
		jQuery(this).addClass('active');
	});
	jQuery('a#tab-earningHistory').click(function(){
		jQuery('#cont-yourDetails').css({display:'none'});
		jQuery('#cont-bookHistory').css({display:'none'});
		jQuery('#cont-billingHistory').css({display:'none'});
		jQuery('#cont-earningHistory').css({display:'block'});
		
		jQuery('a#tab-yourDetails, a#tab-bookHistory, a#tab-billingHistory').removeClass('active');
		jQuery(this).addClass('active');
	});
	
	
	
	
	jQuery('.headsUp .wrapper.bordered .details').css({display:'none'});
	
	jQuery('.headsUp .wrapper.bordered a.showDetails').click(function(){
		if( jQuery(this).parent().parent().find('img.up').css('display')=='inline' ){
			jQuery(this).parent().parent().find('img.up').css({display:'none'});
			jQuery(this).parent().parent().find('img.down').css({display:'inline'});
			
			jQuery(this).parent().parent().find('.details').slideUp(100);
		}else{
			jQuery('.headsUp .wrapper.bordered .details').slideUp(100);
			jQuery('.headsUp .wrapper.bordered .showDetails').find('img.up').css({display:'none'});
			jQuery('.headsUp .wrapper.bordered .showDetails').find('img.down').css({display:'inline'});
			
			jQuery(this).parent().parent().find('.details').slideDown(100);
			jQuery(this).parent().parent().find('img.up').css({display:'inline'});
			jQuery(this).parent().parent().find('img.down').css({display:'none'});
		}
	});
	//MY ACCOUNT part END ============================================	
	// SIDEBAR SUB MENU ==============================================
	jQuery('#sidebar ul li ul').css({display:'none'});
	
	jQuery('#sidebar ul').find('li:has(ul) a').click(function(){
		if( jQuery(this).parent().find('ul:first').css('display')=='none' ){
			jQuery(this).parent().parent().find('li ul').slideUp(100);
			
			jQuery(this).parent().find('ul:first').slideDown(100);
		}else{
			jQuery(this).parent().find('ul').slideUp(100);
		}
	});
	
	
	jQuery('#sidebar ul li ul li a.active').parent().parent().css({display:'block'}).parent().parent().css({display:'block'});
	jQuery('#sidebar ul li a.active').parent().parent().css({display:'block'});
	
	jQuery("#sidebar ul li").each(function() {
		var selected_menu = jQuery("[name=selected-menu]").val();
		var this_href =  jQuery(this).attr('name');
		if(this_href==selected_menu){
			jQuery(this).addClass('active');
			if(jQuery(this).hasClass('sub-menu')){
				jQuery(this).parent().parent().addClass('active');
				jQuery(this).parent().css({display:'block'});
			}
		}		
    });
	
	
	
	// SIDEBAR SUB MENU  END==============================================
	set_year()	

});

function set_year(){
	var firstYear = 2014;
	var lastYear = 2500;
	for(var i =firstYear; i<=lastYear; i++) {
		if((new Date).getFullYear()== i)   jQuery('[name=select_year]').append('<option selected="selected" value="'+i+'">'+i+'</option>')
		else  jQuery('[name=select_year]').append('<option value="'+i+'">'+i+'</option>')
	}
	//jQuery('[name=select_year]').trigger("chosen:updated");	
}

function match_selected_menu(href,selected_menu){
	var data_href = str.replace('')
}

function displayNoneClass(val){
	$('.'+val).css('display','none');
}

function displayNoneId(val){
	$('#'+val).css('display','none');
}

function WindowLocation(url){
	setTimeout(
	    	function(){
	    		window.location = url;
    }, 1500);
}

function notifBlockSaved(){
	//alert("M");
	jQuery('.notifBlockSaved').slideDown().delay(3000).slideUp();
}
function notifFailedSaved(){
	//alert("M");
	jQuery('.notifFailedSaved').slideDown().delay(3000).slideUp();
}

function notifFailedValidation(){
	//alert("M");
	jQuery('.notifFailedSaved').slideDown();
}

function formButtons(){
	jQuery('a.button.submit').click(function(){
		jQuery('#theEditForm').submit();
	});
}

function validateTheEmail(address) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var address;
   if(reg.test(address) == false) {
      return false;
   }else{
      return true;
   }
}
function trim(str) {
	var	str = str.replace(/^\s\s*/, ''),
		ws = /\s/,
		i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i + 1);
}

function ShowEditDelete(){
	//alert("M");
	$('.block_action').mouseover(function(){
		$('.a_action').css('display','none');
		var check = $(this).attr('rel');
		$('.a_action_'+check).css('display','block');
	});

	$('.block_action').mouseleave(function(){
		$('.a_action').css('display','none');
	})
}

/*tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "style",

	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_path_location : "bottom",
	content_css : "example_full.css",
    plugin_insertdate_dateFormat : "%Y-%m-%d",
    plugin_insertdate_timeFormat : "%H:%M:%S",
	extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
	external_link_list_url : "example_link_list.js",
	external_image_list_url : "example_image_list.js",
	flash_external_list_url : "example_flash_list.js",
	file_browser_callback : "fileBrowserCallBack",
	theme_advanced_resize_horizontal : false,
	theme_advanced_resizing : true
});*/