// JavaScript Document
//Function to create an XMLHttp Object.
function getxmlhttp (){
	//Create a boolean variable to check for a valid Microsoft active x instance.
	var xmlhttp = false;
	//Check if we are using internet explorer.
	try {
		//If the javascript version is greater than 5.
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		//If not, then use the older active x object.
		try {
			//If we are using internet explorer.
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			//Else we must be using a non-internet explorer browser.
			xmlhttp = false;
		}
	}
	
	// If not using IE, create a
	// JavaScript instance of the object.
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function submitform(theform, serverPage, objID,timeOut, valfunc){
	var filePage=serverPage;
	var serverPage = serverPage;
	alert(serverPage);
	var str = getformvalues(theform,valfunc);
	//If the validation is ok.
	
	if (aok == true){
		obj = document.getElementById(objID);
		processajax (serverPage, obj, "post", str);
		
	}
}


function getformvalues (fobj, valfunc){
	var str = "";
	aok = true;
	var val;
	//Run through a list of all objects contained within the form.
	for(var i = 0; i < fobj.elements.length; i++){
		if(valfunc) {
			if (aok == true){
				val = valfunc (fobj.elements[i].value,fobj.elements[i].name);
				if (val == false){
					aok = false;
				}
			}
		}
		
		str += fobj.elements[i].name + "=" + escape(fobj.elements[i].value) + "&";
	}
	//Then return the string values.
	return str;
}

function processajax (serverPage, obj, getOrPost, str){
	//Get an XMLHttpRequest object for use.
	xmlhttp = getxmlhttp ();
	if (getOrPost == "get"){
		
		xmlhttp.open("GET", serverPage);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				obj.innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.send(null);
	}else{
		xmlhttp.open("POST", serverPage, true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				obj.innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.send(str);
	}
}

function hit_close_button(alertid,formid,filePage){
	 if(formid=='billing_form'){
	 	document.getElementById(formid).billing_close.onclick();
		get_address(filePage,'b','billing_address');
		 document.getElementById(alertid).innerHTML="";
	 }else if(formid=='shipping_form'){
	 	document.getElementById(formid).shipping_close.onclick();
		get_address(filePage,'s','shipping_address');
        document.getElementById(alertid).innerHTML="";
	 }else if(formid=='upload_file'){
		 document.getElementById(formid).upload_file_close.onclick();	
		 document.getElementById("upload_button").value="Upload";
		 window.frames['uploadframe'].document.getElementById('upload_alert').innerHTML="";
		 window.frames['uploadframe'].document.getElementById('upload_alert').style.display="";
		 window.frames['uploadframe'].document.getElementById('upload_alert').style.visibility="hidden";
    	// document.location.reload()
	 }
	
}

function uploadimg(theform){
	window.frames['uploadframe'].document.getElementById('upload_prs').innerHTML="Proses....";
	theform.submit();
	//document.getElementById("upload_button").value="Upload";
	//setTimeout("refresh_window()",4000);
	
}
function refresh_window(){
	location.reload(true);
}
function uploadfoto(theform,serverPage,obj){
	window.frames['uploadframe'].document.getElementById('upload_prs').innerHTML="Proses....";
	document.getElementById(theform).submit();
	//document.getElementById("pesan").innerHTML="";
	
	/*if(a=="Upload file telah berhasil"){
		setTimeout("hit_close_button('uploadframe','upload_file','')",3000);
		setTimeout("getImage()",2000);
	}*/
}

function close_window(win){
	Boxy.get(win).hide();
	setTimeout("refresh_window()",1000);
	return false;
}
function getImage(){
	refresh_url=document.getElementById("refresh").value;
	window.location=refresh_url;
	
}

function banned_member(serverPage,obj,client_id){
	serverPage=serverPage+"?client_id="+client_id;
	obj=document.getElementById(obj);
	processajax (serverPage, obj,'get','')
	document.getElementById('banned_button').innerHTML="<div class=\"member_status_gray\">Member di blokir</div>";
}

function activate_member(serverPage,obj,client_id){
	serverPage=serverPage+"?client_id="+client_id;
	document.getElementById(obj).innerHTML="<div class=\"alert2\">Prosess.....</div>";
	obj=document.getElementById(obj);
	processajax (serverPage, obj,'get','')
}
function ajaxPaging(serverPage,obj,page,cat_id){
	serverPage=serverPage+"?page="+page+"&act2="+cat_id;
	document.getElementById(obj).innerHTML="<div class=\"alert2\">Prosess.....</div>";
	obj=document.getElementById(obj);
	processajax (serverPage, obj,'get','')
}
function doTestimonial(serverPage,obj,testi_id,action){
	page=serverPage;
	serverPage=serverPage+"?id="+testi_id+"&act_type="+action;
	document.getElementById(obj).innerHTML="<div class=\"alert2\">Prosess.....</div>";
	obj=document.getElementById(obj);
	processajax (serverPage, obj,'get','')
	setTimeout("refreshTestimonial('"+page+"','refresh')",1000);
}

function refreshTestimonial(serverPage,obj){
	serverPage=serverPage+"?act_type="+4;
	document.getElementById(obj).innerHTML="<div class=\"alert2\">Refreshing.....</div>";
	obj=document.getElementById(obj);
	processajax (serverPage, obj,'get','')
}