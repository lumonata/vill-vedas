
var xmlhttp = false;
//Check if we are using IE.
try {
//If the javascript version is greater than 5.
	xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
	//If not, then use the older active x object.
	try {
	//If we are using IE.
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} catch (E) {
		//Else we must be using a non-IE browser.
		xmlhttp = false;
	}
}

//If we are using a non-IE browser, create a JavaScript instance of the object.
if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
	xmlhttp = new XMLHttpRequest();
}



function closetask (){
	
	acObject = document.getElementById("autocompletediv");
	acObject.style.visibility = "hidden";
	acObject.style.height = "0px";
	acObject.style.width = "0px";
}
function findPosX(obj){
	var curleft = 0;
	if (obj.offsetParent){
		while (obj.offsetParent){
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	} else if (obj.x){
		curleft += obj.x;
	}
	return curleft;
}

function findPosY(obj){
	
	var curtop = 0;
	if (obj.offsetParent){
		while (obj.offsetParent){
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}else if (obj.y){
			curtop += obj.y;
	}
	return curtop;
}

function autocomplete (serverPage,thevalue, e){
	if(thevalue=="")closetask();
	
	theObject = document.getElementById("autocompletediv");
	theObject.style.visibility = "visible";
	theObject.style.width = "152px";
	var posx = 0;
	var posy = 0;
	posx = (findPosX (document.getElementById("yourname")) + 1);
	posy = (findPosY (document.getElementById("yourname")) + 20);
	theObject.style.left = posx + "px";
	theObject.style.top = posy + "px";
	var theextrachar = e.which;
	if (theextrachar == 'undefined'){
		theextrachar = e.keyCode;
	}
	
	//The location we are loading the page into.
	var objID = "autocompletediv";

	//Take into account the backspace.
	if (theextrachar == 8){
		if (thevalue.length == 1){
			var serverPage =serverPage;
		}else{
			var serverPage = serverPage + "?sstring=" + thevalue.substr (0, (thevalue.length -1));
		}
	} else {
		var serverPage = serverPage + "?sstring=" +thevalue + String.fromCharCode (theextrachar);
	}
	


	var obj = document.getElementById(objID);
	
	xmlhttp.open("GET", serverPage);
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			obj.innerHTML = xmlhttp.responseText;
			document.getElementById("rs1").focus();
		}
	}
	
	xmlhttp.send(null);
	
}

function setvalue (thevalue){
	acObject = document.getElementById("autocompletediv");
	acObject.style.visibility = "hidden";
	acObject.style.height = "0px";
	acObject.style.width = "0px";
	document.getElementById("yourname").value =thevalue;
}
function setMouseOver(obj){
	obj.style.background='#b5d431';
	obj.style.color='#000000';
}
function setMouseOut(obj){
	obj.style.background='#ffffff';
	obj.style.color='#666666';
}