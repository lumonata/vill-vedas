<?php
	/*BEGIN PLUGIN*/
	$plugin=$searchDir->getDirectory("plugin/");
	for($i=0;$i<=count($plugin)-1;$i++){
		$class_name=$searchDir->getClassName("plugin/".$plugin[$i]);
		require_once("plugin/".$plugin[$i]."/class.".$class_name.".php");
		$object_class = new $class_name($plugin[$i]);
		$template_var=$object_class->getTemplateVar();
		$var=$object_class->load();
		$t->set_var($template_var,$var);	
	}
	/*END PLUGIN*/
?>