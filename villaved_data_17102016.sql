-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 17, 2016 at 02:30 AM
-- Server version: 5.5.40-36.1-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `villaved_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_booking`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_booking` (
  `lbook_id` int(11) NOT NULL DEFAULT '0',
  `lcheck_in` int(11) DEFAULT NULL,
  `lcheck_out` int(11) DEFAULT NULL,
  `lfirst_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `llast_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `lemail` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `lphone` varchar(24) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `ladult` smallint(6) DEFAULT NULL,
  `lchild` smallint(6) DEFAULT NULL,
  `lspecial_note` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `ltotal` decimal(10,2) DEFAULT NULL,
  `ldeposit` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `lbalance` int(11) NOT NULL,
  `lstatus` smallint(6) DEFAULT NULL COMMENT '0:cancel, 1:check-availability;2: dp-waiting,3:full-pay waiting,4:confirm,5:uncofirm,6:receive deposite payment',
  `lbook_type` char(12) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `lbook_by` bigint(20) NOT NULL,
  PRIMARY KEY (`lbook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_booking_detail`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_booking_detail` (
  `lbook_id` int(11) NOT NULL DEFAULT '0',
  `larticle_id` bigint(20) NOT NULL DEFAULT '0',
  `ltotal` decimal(10,2) DEFAULT NULL,
  `ldiscount` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `learly_bird` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `lsurecharge` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`lbook_id`,`larticle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_cancelationpolicy`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_cancelationpolicy` (
  `lcancelpolicy_ID` int(12) NOT NULL AUTO_INCREMENT,
  `lcancelpolicy_cat_ID` int(11) NOT NULL,
  `lacco_type_id` int(12) NOT NULL,
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lrule1` int(12) NOT NULL,
  `lrule2` int(12) NOT NULL,
  `lrule3` int(12) NOT NULL,
  `lcancellation_policy` text NOT NULL,
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lcancelpolicy_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `lumonata_accommodation_cancelationpolicy`
--

INSERT INTO `lumonata_accommodation_cancelationpolicy` (`lcancelpolicy_ID`, `lcancelpolicy_cat_ID`, `lacco_type_id`, `ldate_from`, `ldate_to`, `lrule1`, `lrule2`, `lrule3`, `lcancellation_policy`, `ldlu`) VALUES
(17, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(18, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(19, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(20, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(21, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(22, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(23, 1, 7, 1317398400, 1349020800, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1323408889),
(48, 2, 19, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(47, 2, 18, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(46, 2, 16, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(44, 2, 14, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(45, 2, 15, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(43, 2, 6, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(41, 0, 13, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(42, 2, 5, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1323408927),
(36, 0, 8, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(37, 0, 9, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(38, 0, 10, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(39, 0, 11, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(40, 0, 12, 1317398400, 1349020800, 36, 37, 38, '1', 1323408889),
(24, 0, 8, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(25, 0, 9, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(26, 0, 10, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(27, 0, 11, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(28, 0, 12, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(29, 0, 13, 1317398400, 1343664000, 36, 37, 38, '1', 1323252627),
(30, 0, 8, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(31, 0, 9, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(32, 0, 10, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(33, 0, 11, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(34, 0, 12, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(35, 0, 13, 1317398400, 1343664000, 36, 37, 38, '1', 1323252651),
(49, 3, 2, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1323408952),
(50, 3, 3, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1323408952),
(51, 3, 1, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1323408952),
(52, 3, 4, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1323408952),
(53, 3, 17, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1323408952),
(54, 4, 20, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(55, 4, 21, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(56, 4, 22, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(57, 4, 23, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(58, 4, 24, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(59, 4, 25, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(60, 4, 26, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(61, 4, 27, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(62, 4, 28, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971),
(63, 4, 29, 1317398400, 1349020800, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1323408971);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_cancelationpolicy_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_cancelationpolicy_categories` (
  `lcancelpolicy_cat_ID` int(12) NOT NULL AUTO_INCREMENT,
  `lacco_type_cat_id` bigint(20) NOT NULL,
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lrule1` int(12) NOT NULL,
  `lrule2` int(12) NOT NULL,
  `lrule3` int(12) NOT NULL,
  `lcancellation_policy` text NOT NULL,
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lcancelpolicy_cat_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `lumonata_accommodation_cancelationpolicy_categories`
--

INSERT INTO `lumonata_accommodation_cancelationpolicy_categories` (`lcancelpolicy_cat_ID`, `lacco_type_cat_id`, `ldate_from`, `ldate_to`, `lrule1`, `lrule2`, `lrule3`, `lcancellation_policy`, `ldlu`) VALUES
(1, 59, 1397664000, 1397750400, 36, 37, 38, 'Quisque eu accumsan libero. Cras dapibus adipiscing dui et molestie. Morbi mattis dui eu odio volutpat imperdiet. Aliquam et urna nunc, in iaculis nunc.', 1397702262),
(2, 59, 1397664000, 1397750400, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. ', 1397702262),
(3, 96, 1397664000, 1397750400, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. ', 1397702262),
(4, 97, 1397664000, 1397750400, 36, 37, 38, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ullamcorper mauris id ipsum tempor mollis. In vitae convallis metus. Sed sit amet sollicitudin neque. Curabitur hendrerit, nunc vitae feugiat rutrum, turpis magna scelerisque ante, et mattis justo magna ut ante.', 1397702262),
(5, 98, 1397664000, 1397750400, 36, 0, 0, 'asdasdasd', 1397702262),
(6, 105, 1399996800, 1400169600, 36, 37, 38, 'adsadad', 1400061931);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_cancelationpolicy_master`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_cancelationpolicy_master` (
  `lcancelpolicy_master_ID` int(12) NOT NULL AUTO_INCREMENT,
  `ldescription` text NOT NULL,
  `lnightBefore` int(12) NOT NULL,
  `lcharge` decimal(10,2) NOT NULL,
  `lunit` varchar(12) NOT NULL,
  `lparent` int(12) NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` varchar(100) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`lcancelpolicy_master_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `lumonata_accommodation_cancelationpolicy_master`
--

INSERT INTO `lumonata_accommodation_cancelationpolicy_master` (`lcancelpolicy_master_ID`, `ldescription`, `lnightBefore`, `lcharge`, `lunit`, `lparent`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`) VALUES
(36, 'Rule 1', 5, '20.00', '%', 0, 3, 'Raden Yudistira', '2011-12-05 18:04:30', 'Raden Yudistira', '2011-12-09 13:34:10'),
(37, 'Rule 11', 15, '15.00', '%', 36, 2, 'Raden Yudistira', '2011-12-05 18:04:44', 'Raden Yudistira', '2011-12-09 13:34:10'),
(38, 'Rule 12', 20, '10.00', '%', 37, 1, 'Raden Yudistira', '2011-12-05 18:05:57', 'Raden Yudistira', '2011-12-09 13:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_payment`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_payment` (
  `lpaid` int(11) NOT NULL DEFAULT '0',
  `lbook_id` int(11) DEFAULT NULL,
  `ltxn_id` varchar(25) NOT NULL DEFAULT '',
  `ltype_paid` varchar(1) DEFAULT NULL COMMENT '0:down payment;1:full payment',
  `lgross` decimal(6,2) DEFAULT NULL,
  `lcurrency` varchar(10) DEFAULT NULL,
  `lpresentase_dp` smallint(3) DEFAULT NULL,
  `lemail` varchar(50) DEFAULT NULL,
  `lcreated_date` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`lpaid`,`ltxn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lumonata_accommodation_payment`
--

INSERT INTO `lumonata_accommodation_payment` (`lpaid`, `lbook_id`, `ltxn_id`, `ltype_paid`, `lgross`, `lcurrency`, `lpresentase_dp`, `lemail`, `lcreated_date`) VALUES
(1400815092, 1400476152, '61E67681CH3238416', '0', '1.50', 'USD', 50, 'juli4rth4@yahoo.com', '23 Mey 2014'),
(1401169153, 1401070506, '61E67681CH3238469', '0', '2.03', 'USD', 50, 'juli4rth4@yahoo.com', '27 Mey 2014'),
(1403069366, 1403069125, '61E67681CH3238534', '0', '236.60', 'USD', 50, 'juli4rth4@yahoo.com', '16:00:59 Mey 6, 2014 PST');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_promo`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_promo` (
  `lpromo_ID` int(12) NOT NULL AUTO_INCREMENT,
  `lpromo_for` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `lpromo_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lacco_type_id` int(12) NOT NULL,
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lday_of_week` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `lday_before` int(12) NOT NULL,
  `lstay` int(12) NOT NULL,
  `lstay_to` int(12) NOT NULL,
  `lammount` decimal(10,2) NOT NULL,
  `lammount_unit` varchar(5) CHARACTER SET ucs2 NOT NULL,
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lpromo_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `lumonata_accommodation_promo`
--

INSERT INTO `lumonata_accommodation_promo` (`lpromo_ID`, `lpromo_for`, `lpromo_type`, `lacco_type_id`, `ldate_from`, `ldate_to`, `lday_of_week`, `lday_before`, `lstay`, `lstay_to`, `lammount`, `lammount_unit`, `ldlu`) VALUES
(1, 'villa', 'disc_promo', 116, 1410796800, 1410883200, '["sun","wen"]', 0, 1, 1, '1.00', 'USD', 1410853125),
(2, 'villa', 'early_bird', 116, 1410796800, 1410883200, '', 1, 1, 1, '1.00', 'USD', 1410856371);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_promo_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_promo_categories` (
  `lpromo_cat_ID` int(12) NOT NULL AUTO_INCREMENT,
  `lpromo_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lacco_type_cat_id` varchar(20) NOT NULL,
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lday_before` int(12) NOT NULL,
  `lstay` int(12) NOT NULL,
  `lstay_to` int(12) NOT NULL,
  `lroom` int(12) NOT NULL,
  `lammount` decimal(10,2) NOT NULL,
  `lammount_unit` varchar(5) CHARACTER SET ucs2 NOT NULL,
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lpromo_cat_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `lumonata_accommodation_promo_categories`
--

INSERT INTO `lumonata_accommodation_promo_categories` (`lpromo_cat_ID`, `lpromo_type`, `lacco_type_cat_id`, `ldate_from`, `ldate_to`, `lday_before`, `lstay`, `lstay_to`, `lroom`, `lammount`, `lammount_unit`, `ldlu`) VALUES
(1, 'early_bird', '59_100', 1397664000, 1397750400, 2, 1, 10, 1, '100.00', 'USD', 1397701879),
(2, 'early_bird', '59_101', 1397664000, 1397750400, 1, 1, 10, 1, '123.00', 'USD', 1397701879),
(3, 'disc_promo', '59_100', 1397664000, 1397750400, 0, 1, 10, 1, '200.00', 'USD', 1397701896),
(4, 'disc_promo', '59_101', 1397664000, 1397750400, 0, 1, 1, 1, '242.00', '%', 1397701896),
(5, 'disc_promo', '96_101', 1397664000, 1397750400, 0, 1, 1, 1, '1.00', 'USD', 1397701896);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_rate`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_rate` (
  `lrate_id` int(11) NOT NULL AUTO_INCREMENT,
  `lrate_cat_id` int(11) NOT NULL,
  `lacco_type_id` varchar(20) COLLATE latin1_general_ci NOT NULL COMMENT 'id of Room Type of Accommodation',
  `lcurrency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `llow_season` decimal(10,2) NOT NULL,
  `lhigh_season` decimal(10,2) NOT NULL,
  `lpeak_season` decimal(10,2) NOT NULL,
  `llow_published` decimal(10,2) NOT NULL,
  `lhigh_published` decimal(10,2) NOT NULL,
  `lpeak_published` decimal(10,2) NOT NULL,
  `lbreakfast` int(5) NOT NULL COMMENT '0=not included;1=breakfast included',
  `lallotment` int(11) NOT NULL,
  `lmin_commision` decimal(5,2) NOT NULL COMMENT 'Minimal Commision by Admin',
  `lcommision` decimal(5,2) NOT NULL COMMENT 'commision set by Supplier',
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=242 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_rate_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_rate_categories` (
  `lrate_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `lacco_type_cat_id` bigint(20) NOT NULL COMMENT 'id of Room Category',
  `lcurrency` varchar(50) CHARACTER SET utf8 NOT NULL,
  `llow_season` decimal(10,2) NOT NULL,
  `lhigh_season` decimal(10,2) NOT NULL,
  `lpeak_season` decimal(10,2) NOT NULL,
  `llow_additional` decimal(10,2) NOT NULL,
  `lhigh_additional` decimal(10,2) NOT NULL,
  `lpeak_additional` decimal(10,2) NOT NULL,
  `lbreakfast` int(5) NOT NULL COMMENT '0=not included;1=breakfast included',
  `lallotment` int(11) NOT NULL,
  `lmin_commision` decimal(5,2) NOT NULL COMMENT 'Minimal Commision by Admin',
  `lcommision` decimal(5,2) NOT NULL COMMENT 'commision set by Supplier',
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lrate_cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lumonata_accommodation_rate_categories`
--

INSERT INTO `lumonata_accommodation_rate_categories` (`lrate_cat_id`, `lacco_type_cat_id`, `lcurrency`, `llow_season`, `lhigh_season`, `lpeak_season`, `llow_additional`, `lhigh_additional`, `lpeak_additional`, `lbreakfast`, `lallotment`, `lmin_commision`, `lcommision`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(1, 116, '', '1.15', '3.00', '4.00', '0.65', '1.25', '3.25', 1, 1, '0.00', '0.00', 1, 'admin', 1410838386, 'admin', 1410849486);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_season`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_season` (
  `lseason_id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ldate_start` int(11) NOT NULL,
  `ldate_finish` int(11) NOT NULL,
  `lmin_stay` int(11) NOT NULL COMMENT 'minimal stay',
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lseason_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=140 ;

--
-- Dumping data for table `lumonata_accommodation_season`
--

INSERT INTO `lumonata_accommodation_season` (`lseason_id`, `lname`, `ldate_start`, `ldate_finish`, `lmin_stay`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(139, 'Low Season', 1410969600, 1412006400, 1, 1, 'admin', 1411034217, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_surecharge`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_surecharge` (
  `lsurecharge_ID` int(12) NOT NULL AUTO_INCREMENT,
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lday_of_week` varchar(255) NOT NULL,
  `ldescription` varchar(255) NOT NULL,
  `lcharge` decimal(6,2) NOT NULL,
  `lammount_unit` varchar(10) NOT NULL COMMENT 'unit for surcharge',
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lsurecharge_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=282 ;

--
-- Dumping data for table `lumonata_accommodation_surecharge`
--

INSERT INTO `lumonata_accommodation_surecharge` (`lsurecharge_ID`, `ldate_from`, `ldate_to`, `lday_of_week`, `ldescription`, `lcharge`, `lammount_unit`, `ldlu`) VALUES
(1, 0, 0, '["sun","mon","tue","wen","thu","fri","sat"]', 'Government tax and service charge', '15.00', '%', 1410830720);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_surecharge_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_surecharge_categories` (
  `lsurecharge_cat_ID` int(12) NOT NULL AUTO_INCREMENT,
  `lacco_type_cat_id` varchar(20) NOT NULL COMMENT 'id of Room Category [category_villa] [room_type]',
  `ldate_from` int(12) NOT NULL,
  `ldate_to` int(12) NOT NULL,
  `lday_of_week` varchar(255) NOT NULL,
  `ldescription` int(12) NOT NULL,
  `lcharge` decimal(10,2) NOT NULL,
  `lammount_unit` varchar(10) NOT NULL COMMENT 'unit for surcharge',
  `ldlu` int(12) NOT NULL,
  PRIMARY KEY (`lsurecharge_cat_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lumonata_accommodation_surecharge_categories`
--

INSERT INTO `lumonata_accommodation_surecharge_categories` (`lsurecharge_cat_ID`, `lacco_type_cat_id`, `ldate_from`, `ldate_to`, `lday_of_week`, `ldescription`, `lcharge`, `lammount_unit`, `ldlu`) VALUES
(1, '59_100', 1397664000, 1397750400, '["sun","mon","sat"]', 2, '10.00', 'USD', 1397700768),
(2, '59_100', 1397664000, 1397750400, '["sun"]', 1, '12.00', 'USD', 1397701851);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_surecharge_description`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_surecharge_description` (
  `lsurecharge_desc_ID` int(12) NOT NULL AUTO_INCREMENT,
  `ldescription` text NOT NULL,
  `lperNight` int(1) NOT NULL,
  `lperBooking` int(1) NOT NULL,
  `lperAdult` int(1) NOT NULL,
  `lperChild` int(1) NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` varchar(100) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`lsurecharge_desc_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `lumonata_accommodation_surecharge_description`
--

INSERT INTO `lumonata_accommodation_surecharge_description` (`lsurecharge_desc_ID`, `ldescription`, `lperNight`, `lperBooking`, `lperAdult`, `lperChild`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`) VALUES
(2, 'High season surcharge', 1, 0, 0, 0, 2, 'Raden Yudistira', '2011-07-26 16:27:27', 'Raden Yudistira', '2011-08-16 11:07:59'),
(1, '21% government tax', 0, 1, 0, 0, 1, 'Raden Yudistira', '2011-07-26 16:28:37', 'Raden Yudistira', '2011-08-18 16:36:52'),
(3, 'new surcharge', 0, 0, 1, 0, 3, 'Raden Yudistira', '2011-08-16 11:03:20', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_type`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_type` (
  `lacco_type_id` int(11) NOT NULL,
  `lacco_id` int(11) NOT NULL DEFAULT '1' COMMENT 'FK lumonata_accommodation',
  `lnumber` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lbrief` text CHARACTER SET utf8 NOT NULL,
  `ldescription` text CHARACTER SET utf8 NOT NULL,
  `limage` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lno_of_bedroom` int(11) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lsef_url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lmax_adult` int(15) NOT NULL,
  `lmember_id` varchar(12) COLLATE latin1_general_ci NOT NULL,
  `lpost_by` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`lacco_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_accommodation_type_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_accommodation_type_categories` (
  `lacco_type_cat_id` int(11) NOT NULL,
  `lname` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `ldescription` text COLLATE latin1_general_ci NOT NULL,
  `limage` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lno_of_bedroom` int(11) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lpost_by` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `ldlu` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_additional_fields`
--

CREATE TABLE IF NOT EXISTS `lumonata_additional_fields` (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lvalue` text CHARACTER SET utf8 NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`lapp_id`,`lkey`),
  KEY `key` (`lkey`),
  KEY `app_name` (`lapp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lumonata_additional_fields`
--

INSERT INTO `lumonata_additional_fields` (`lapp_id`, `lkey`, `lvalue`, `lapp_name`) VALUES
(122, 'meta_title', '', 'aboutus'),
(1, 'first_name', 'Villa', 'user'),
(1, 'last_name', 'Vedas', 'user'),
(1, 'website', 'http://', 'user'),
(1, 'bio', '', 'user'),
(122, 'meta_description', '', 'aboutus'),
(0, 'contact_us_email', 'test@test.com', 'contact-us'),
(0, 'contact_us_cc', 'test-cc@test.com', 'contact-us'),
(0, 'contact_us_bcc', 'test-bcc@test.com', 'contact-us'),
(0, 'contact_us_title', '', 'contact-us'),
(8, 'contact_us_email', 'ary@villavedas.com', 'contact-us'),
(8, 'contact_us_cc', '', 'contact-us'),
(8, 'contact_us_bcc', '', 'contact-us'),
(8, 'contact_us_title', '', 'contact-us'),
(122, 'meta_keywords', '', 'aboutus'),
(1, 'one_liner', '', 'user'),
(1, 'location', '', 'user'),
(27, 'meta_title', '', 'pages'),
(27, 'meta_description', '', 'pages'),
(27, 'meta_keywords', '', 'pages'),
(32, 'meta_title', '', 'pages'),
(32, 'meta_description', '', 'pages'),
(32, 'meta_keywords', '', 'pages'),
(33, 'meta_title', '', 'pages'),
(33, 'meta_description', '', 'pages'),
(33, 'meta_keywords', '', 'pages'),
(40, 'meta_title', 'Q-Switch Laser', 'pages'),
(40, 'meta_description', '', 'pages'),
(40, 'meta_keywords', 'Q-Switch Laser', 'pages'),
(42, 'meta_title', '', 'pages'),
(42, 'meta_description', '', 'pages'),
(42, 'meta_keywords', '', 'pages'),
(46, 'meta_title', '', 'pages'),
(46, 'meta_description', '', 'pages'),
(46, 'meta_keywords', '', 'pages'),
(81, 'social_media_link_fb', 'lumonata', 'social-media'),
(81, 'social_media_link_twitter', 'lumonata', 'social-media'),
(81, 'social_media_link_pinterest', '#', 'social-media'),
(24, 'rp_data', '[{"order":0,"type":"pages","label":"Brow Lift","target":"_self","link":"?page_id=55","permalink":"brow-lift"},{"order":1,"type":"articles","label":"After care","target":"_self","link":"?app_name=articles&amp;cat_id=8","permalink":"articles\\/after-care"}]', 'related_page'),
(2, 'invite_limit', '10', 'user'),
(2, 'first_name', 'Dana', 'user'),
(2, 'last_name', 'Asmara', 'user'),
(2, 'website', 'http://', 'user'),
(26, 'rp_data', '[{"order":0,"type":"pages","label":"Botox","target":"_self","link":"?page_id=24","permalink":"botox"},{"order":1,"type":"articles","label":"After care","target":"_self","link":"?app_name=articles&amp;cat_id=8","permalink":"articles\\/after-care"}]', 'related_page'),
(351, 'meta_title', '', 'pages'),
(351, 'meta_description', '', 'pages'),
(350, 'meta_description', '', 'pages'),
(350, 'meta_keywords', '', 'pages'),
(349, 'meta_keywords', '', 'articles'),
(350, 'meta_title', '', 'pages'),
(81, 'social_media_link_skype', 'yana_np', 'social-media'),
(81, 'social_media_link_yahoo', 'asmaradana_nusapenida', 'social-media'),
(81, 'social_media_link_whatsapp', '081 9364 75484', 'social-media'),
(135, 'meta_title', '', 'ratesreservations'),
(135, 'meta_description', '', 'ratesreservations'),
(349, 'prog_commence', '', 'articles'),
(349, 'article_news', '0', 'articles'),
(349, 'meta_title', '', 'articles'),
(349, 'phi_id', '348', 'page_header_image'),
(349, 'phi_data', '{"order":"0000","img":"home-1409305172.jpg","url":"","text1":"Home","text2":"","app_type":"pages","description_left":"","description_right":""}', 'page_header_image'),
(120, 'product_price_range', '{"1-5":"10","6-10":"9"}', 'products'),
(120, 'product_image_thumbnail', '38', 'products'),
(120, 'product_additional', '{"code":"P-0001","price":"10","weight":"0.5","tax":"70","stock_limit":"0","product_buy_out_of_stock":"1","trackstok":"0","set_as_featured":""}', 'products'),
(121, 'product_price_range', '{"1-5":"20","6-10":"9"}', 'products'),
(121, 'product_additional', '{"code":"P-0002","price":"20","weight":"0.5","tax":"71","stock_limit":"100","product_buy_out_of_stock":"1","trackstok":"1","set_as_featured":""}', 'products'),
(121, 'product_image_thumbnail', '39', 'products'),
(121, 'product_variant', '{"child_variant":[{"15":["10"]}],"parent_variant":["14"]}', 'products'),
(135, 'meta_keywords', '', 'ratesreservations'),
(137, 'meta_title', '', 'ratesreservations'),
(137, 'meta_description', '', 'ratesreservations'),
(137, 'meta_keywords', '', 'ratesreservations'),
(139, 'meta_title', '', 'ratesreservations'),
(139, 'meta_description', '', 'ratesreservations'),
(139, 'meta_keywords', '', 'ratesreservations'),
(151, 'meta_title', '', 'ratesreservations'),
(151, 'meta_description', '', 'ratesreservations'),
(151, 'meta_keywords', '', 'ratesreservations'),
(154, 'meta_title', '', 'ratesreservations'),
(154, 'meta_description', '', 'ratesreservations'),
(154, 'meta_keywords', '', 'ratesreservations'),
(161, 'meta_title', '', 'facilities'),
(161, 'meta_description', '', 'facilities'),
(161, 'meta_keywords', '', 'facilities'),
(162, 'meta_title', '', 'facilities'),
(162, 'meta_description', '', 'facilities'),
(162, 'meta_keywords', '', 'facilities'),
(349, 'meta_description', '', 'articles'),
(348, 'meta_keywords', '', 'pages'),
(349, 'phi_search', 'Home', 'page_header_image'),
(349, 'min_stay', '', 'articles'),
(348, 'meta_description', '', 'pages'),
(348, 'meta_title', '', 'pages'),
(8, 'contact_us_video', '', 'contact-us'),
(221, 'tripadvisor_url', 'http://www.tripadvisor.com/Hotel_Review-g297697-d7898667-Reviews-Villa_Vedas_Bali-Kuta_Bali.html', 'social_media'),
(221, 'tripadvisor_title', 'Trip Advisor', 'social_media'),
(221, 'twitter_logo', '', 'social_media'),
(221, 'twitter_url', '', 'social_media'),
(221, 'twitter_title', '', 'social_media'),
(221, 'instagram_logo', '', 'social_media'),
(221, 'instagram_url', 'http://instagram.com/villavedasbali', 'social_media'),
(221, 'instagram_title', 'Instagram', 'social_media'),
(347, 'min_stay', '', 'villas'),
(347, 'prog_commence', '', 'villas'),
(347, 'meta_title', '', 'villas'),
(347, 'meta_description', '', 'villas'),
(347, 'meta_keywords', '', 'villas'),
(351, 'meta_keywords', '', 'pages'),
(352, 'meta_title', '', 'pages'),
(352, 'meta_description', '', 'pages'),
(352, 'meta_keywords', '', 'pages'),
(353, 'meta_title', '', 'pages'),
(353, 'meta_description', '', 'pages'),
(353, 'meta_keywords', '', 'pages'),
(354, 'meta_title', '', 'pages'),
(354, 'meta_description', '', 'pages'),
(354, 'meta_keywords', '', 'pages'),
(355, 'min_stay', '', 'articles'),
(355, 'prog_commence', '', 'articles'),
(355, 'article_news', '0', 'articles'),
(355, 'meta_title', '', 'articles'),
(355, 'meta_description', '', 'articles'),
(355, 'meta_keywords', '', 'articles'),
(356, 'min_stay', '', 'articles'),
(356, 'prog_commence', '', 'articles'),
(356, 'article_news', '0', 'articles'),
(356, 'meta_title', '', 'articles'),
(356, 'meta_description', '', 'articles'),
(356, 'meta_keywords', '', 'articles'),
(357, 'phi_id', '112', 'page_header_image'),
(357, 'phi_data', '{"order":"0000","img":"about-us-1428913343.jpg","url":"","text1":"About Us","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(357, 'phi_search', 'About Us', 'page_header_image'),
(357, 'min_stay', '', 'articles'),
(357, 'prog_commence', '', 'articles'),
(357, 'article_news', '0', 'articles'),
(357, 'meta_title', '', 'articles'),
(357, 'meta_description', '', 'articles'),
(357, 'meta_keywords', '', 'articles'),
(358, 'min_stay', '', 'articles'),
(358, 'prog_commence', '', 'articles'),
(358, 'article_news', '0', 'articles'),
(358, 'meta_title', '', 'articles'),
(358, 'meta_description', '', 'articles'),
(358, 'meta_keywords', '', 'articles'),
(359, 'min_stay', '', 'articles'),
(359, 'prog_commence', '', 'articles'),
(359, 'article_news', '0', 'articles'),
(359, 'meta_title', '', 'articles'),
(359, 'meta_description', '', 'articles'),
(359, 'meta_keywords', '', 'articles'),
(360, 'phi_id', '359', 'page_header_image'),
(360, 'phi_data', '{"order":"0000","img":"dining-room-1410507182.jpg","url":"","text1":"Dining Room","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(360, 'phi_search', 'Dining Room', 'page_header_image'),
(361, 'phi_id', '113', 'page_header_image'),
(361, 'phi_data', '{"order":"0000","img":"facilities-1410507810.jpg","url":"","text1":"Facilities","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(361, 'phi_search', 'Facilities', 'page_header_image'),
(360, 'min_stay', '', 'articles'),
(360, 'prog_commence', '', 'articles'),
(360, 'article_news', '0', 'articles'),
(360, 'meta_title', '', 'articles'),
(360, 'meta_description', '', 'articles'),
(360, 'meta_keywords', '', 'articles'),
(363, 'phi_data', '{"order":"0000","img":"contact-us-1411119767.jpg","url":"","text1":"Contact Us","text2":"","app_type":"pages","description_left":"","description_right":""}', 'page_header_image'),
(363, 'phi_search', 'Contact Us', 'page_header_image'),
(363, 'phi_id', '353', 'page_header_image'),
(87, 'rule_pdf', '/lumonata-content/files/201403/ayurveda-therapies.pdf', 'rule_additional_fields'),
(315, 'meta_title', '', 'wellness'),
(315, 'meta_description', '', 'wellness'),
(315, 'meta_keywords', '', 'wellness'),
(316, 'meta_title', '', 'wellness'),
(316, 'meta_description', '', 'wellness'),
(316, 'meta_keywords', '', 'wellness'),
(317, 'meta_title', '', 'aboutus'),
(317, 'meta_description', '', 'aboutus'),
(317, 'meta_keywords', '', 'aboutus'),
(330, 'additional_service_prices', '80', 'additional_service'),
(330, 'additional_service_times', '60', 'additional_service'),
(331, 'additional_service_times', '60', 'additional_service'),
(331, 'additional_service_prices', '6', 'additional_service'),
(8, 'contact_us_phone', '', 'contact-us'),
(8, 'contact_us_phone_bali', '', 'contact-us'),
(361, 'meta_description', '', 'villas'),
(361, 'meta_keywords', '', 'villas'),
(347, 'num_room', '1', 'villas'),
(361, 'num_room', '1', 'villas'),
(362, 'meta_title', '', 'villas'),
(362, 'meta_description', '', 'villas'),
(362, 'meta_keywords', '', 'villas'),
(361, 'meta_title', '', 'villas'),
(361, 'min_stay', '', 'villas'),
(361, 'prog_commence', '', 'villas'),
(365, 'meta_title', '', 'pages'),
(365, 'meta_description', '', 'pages'),
(365, 'meta_keywords', '', 'pages'),
(366, 'meta_title', '', 'pages'),
(366, 'meta_description', '', 'pages'),
(366, 'meta_keywords', '', 'pages'),
(367, 'meta_title', '', 'pages'),
(367, 'meta_description', '', 'pages'),
(367, 'meta_keywords', '', 'pages'),
(368, 'meta_title', '', 'pages'),
(368, 'meta_description', '', 'pages'),
(368, 'meta_keywords', '', 'pages'),
(369, 'meta_title', '', 'pages'),
(369, 'meta_description', '', 'pages'),
(369, 'meta_keywords', '', 'pages'),
(370, 'phi_id', '365', 'page_header_image'),
(370, 'phi_data', '{"order":"0000","img":"contact-us-1411119767.jpg","url":"","text1":"Reservation","text2":"","app_type":"pages","description_left":"","description_right":""}', 'page_header_image'),
(370, 'phi_search', 'Reservation', 'page_header_image'),
(370, 'min_stay', '', 'articles'),
(370, 'prog_commence', '', 'articles'),
(370, 'num_room', '', 'articles'),
(370, 'article_news', '0', 'articles'),
(370, 'meta_title', '', 'articles'),
(370, 'meta_description', '', 'articles'),
(370, 'meta_keywords', '', 'articles'),
(371, 'phi_id', '118', 'page_header_image'),
(371, 'phi_data', '{"order":"0000","img":"events-1428906606.jpg","url":"","text1":"Events","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(371, 'phi_search', 'Events', 'page_header_image'),
(357, 'num_room', '', 'articles'),
(221, 'tripadvisor_logo', '', 'social_media'),
(221, 'facebook_title', 'Facebook', 'social_media'),
(221, 'facebook_url', 'https://www.facebook.com/VillaVedas', 'social_media'),
(221, 'facebook_logo', '', 'social_media'),
(372, 'phi_id', '114', 'page_header_image'),
(372, 'phi_data', '{"order":"0000","img":"bedrooms-1428913104.jpg","url":"","text1":"Bedrooms","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(372, 'phi_search', 'Bedrooms', 'page_header_image'),
(371, 'min_stay', '', 'articles'),
(371, 'prog_commence', '', 'articles'),
(371, 'num_room', '', 'articles'),
(371, 'article_news', '0', 'articles'),
(371, 'meta_title', '', 'articles'),
(371, 'meta_description', '', 'articles'),
(371, 'meta_keywords', '', 'articles'),
(349, 'num_room', '', 'articles'),
(360, 'num_room', '', 'articles'),
(373, 'phi_id', '115', 'page_header_image'),
(373, 'phi_data', '{"order":"0000","img":"services-1428913795.jpg","url":"","text1":"Services","text2":"","app_type":"articles","description_left":"","description_right":""}', 'page_header_image'),
(373, 'phi_search', 'Services', 'page_header_image'),
(356, 'num_room', '', 'articles'),
(1, 'invite_limit', '-1', 'user'),
(372, 'min_stay', '', 'articles'),
(372, 'prog_commence', '', 'articles'),
(372, 'num_room', '', 'articles'),
(372, 'article_news', '0', 'articles'),
(372, 'meta_title', '', 'articles'),
(372, 'meta_description', '', 'articles'),
(372, 'meta_keywords', '', 'articles');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_articles`
--

CREATE TABLE IF NOT EXISTS `lumonata_articles` (
  `larticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_title` text CHARACTER SET utf8 NOT NULL,
  `larticle_brief` text CHARACTER SET utf8 NOT NULL,
  `larticle_content` longtext CHARACTER SET utf8 NOT NULL,
  `larticle_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `larticle_type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_count` bigint(20) NOT NULL,
  `lcount_like` bigint(20) NOT NULL,
  `lsef` text CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`larticle_id`),
  KEY `article_title` (`larticle_title`(255)),
  KEY `type_status_date_by` (`larticle_type`,`larticle_status`,`lpost_date`,`lpost_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=373 ;

--
-- Dumping data for table `lumonata_articles`
--

INSERT INTO `lumonata_articles` (`larticle_id`, `larticle_title`, `larticle_brief`, `larticle_content`, `larticle_status`, `larticle_type`, `lcomment_status`, `lcomment_count`, `lcount_like`, `lsef`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`, `lshare_to`) VALUES
(8, 'Contact Us', '', '', 'publish', 'contact-us', '', 0, 0, 'contact-us', 376, 1, '2012-05-09 11:51:15', 1, '2015-04-13 14:34:36', 0),
(45, 'Slide 1', '', '', 'publish', 'home_slide_show', '', 0, 0, 'slide-1', 302, 1, '2013-06-03 10:40:46', 0, '0000-00-00 00:00:00', 0),
(81, 'Social Media', '', 'Social Media', 'publish', 'social-media', '', 0, 0, 'social-media', 267, 1, '2013-06-11 14:42:59', 1, '2013-11-20 17:22:23', 0),
(161, 'The Grounds', '', '<p>When staying at Sukhavati you have access to all estate facilities including a salt-water swimming pool, an open air dining and entertainment pavilion, yoga pavilion, meditation pavilion along with the Ayurvedic Treatment &amp; Spa Clinic</p>', 'publish', 'facilities', 'allowed', 0, 0, 'the-grounds', 187, 1, '2014-01-07 10:14:23', 1, '2014-01-07 10:14:23', 0),
(162, 'Spa', '', '<p>Set on the river with a view of the green lush tropical vegetation, our open air spa treatment rooms are private, comfortable and traditional. Our Ayurvedic spa therapists and specialists will greet you warmly and tend to your every need for pampering, from foot massage to head massage to all prescribed treatments.</p>', 'publish', 'facilities', 'allowed', 0, 0, 'spa', 186, 1, '2014-01-07 10:14:39', 1, '2014-02-20 09:36:27', 0),
(348, 'Home', '', '<p>just for home</p>', 'publish', 'pages', 'allowed', 0, 0, 'home', 25, 1, '2014-08-29 16:21:51', 1, '2014-08-29 16:21:51', 0),
(349, 'About Us', '', '<p style="text-align: center">Villa Vedas is situated on a stretch of largely untouched and pristine Bali coastline, providing a truly private and luxurious experience.</p>\r\n<p style="text-align: center">The property is located on 3,600 m2 of land, including 45 metres of absolute beach frontage, and offers sweeping panoramic views in all directions.</p>\r\n<p style="text-align: center">Looking out to sea is Java and the perfect sunset, while ancient rice paddies surround to the north and south, and the stunning Batukaru volcano towers behind.</p>\r\n<p style="text-align: center">The frameless Hafele glass wall panels installed throughout enable the owner and his guests to close the villa up during periods of inclement weather, or just to enjoy fully airconditioned comfort provided by the Daikin central airconditioning system, or alternatively to enjoy the breeze al fresco if preferred. <strong>&nbsp;</strong></p>\r\n<p style="text-align: center">The white sandstone stairs and planter boxes blend seamlessly into the beach and the dune flora. The boundary walls feature more than 2000 m2 of tropical vertical gardens providing a sense of being in harmony with nature: Villa Vedas is a residence where sculpture and conceptual elements are fused into the very essence of the building.<strong>&nbsp;</strong></p>\r\n<p style="text-align: center">Every element of the villa, from the feature walls, built in furniture, the sculptural floating brass and teak staircase, to the chandeliers and wood and stone carvings has been meticulously custom designed.</p>\r\n<p style="text-align: center">Villa Vedas is peerless in its attention to detail while exceeding all international standards for structural integrity.</p>', 'publish', 'articles', 'allowed', 0, 0, 'about-us', 19, 1, '2014-08-29 17:48:45', 1, '2016-10-11 11:01:45', 0),
(350, 'Event', '', '<p>Villa Vedas is an incredible venue for private functions, from small parties to concerts and weddings. The beach and surrounding rice paddies, the tranquillity and privacy, it all makes Villa Vedas an exclusive and unique venue in Bali. Our professional hospitality team can assist with smaller events. For bigger events, we can suggest the best wedding planners from around Australia and Indonesia that specialize in providing everything from catering, flowers, d&eacute;cor, dancers and DJs or anything else that a couple may wish to make their wedding day unforgettable.</p>\r\n<ul>\r\n<li>Minimum nights stay low season: 2 nights</li>\r\n<li>Minimum nights stay high season: 3 nights</li>\r\n<li>Minimum nights stay peak season: 4 nights</li>\r\n<li>US$3,000++ is payable per event, such as a party of a wedding, for up to 150 guests, which includes all banjar and other fees. Above 150 guests there is an additional charge of US$5.00++ per person.</li>\r\n</ul>\r\n<h2 style="text-align: center"><strong>Event Inclusions:</strong></h2>\r\n<ul>\r\n<li>Sound Equipment</li>\r\n<li>Access to event vendors and their equipment</li>\r\n<li>Application and fee payment for license to hold a gathering of more than 20 people (Ijin Keramaian payable to Indonesia Police)</li>\r\n<li>Registry and fee payment to Kepala Desa&rsquo;s office for invited guests</li>\r\n<li>Payment fee for Village Security (Pencalang)</li>\r\n<li>Use of parking space and supervision</li>\r\n<li>Use of generator to supply additional power (if required)</li>\r\n<li>Curfew for amplified music is NOT APPLICABLE AT THIS POINT IN TIME however it may change</li>\r\n<li>Fireworks are not permitted from the villa but can be arranged on the beach in front</li>\r\n<li>Laser show is not permitted</li>\r\n<li>Pyrotechnics, fire dance, sky lanterns are permitted.</li>\r\n</ul>\r\n<p><strong><br /></strong></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'publish', 'pages', 'allowed', 0, 0, 'event', 23, 1, '2014-08-29 18:01:01', 1, '2014-09-12 16:33:16', 0),
(351, 'Location', '', '', 'publish', 'pages', 'allowed', 0, 0, 'location', 22, 1, '2014-08-29 18:01:12', 1, '2014-08-29 18:01:12', 0),
(352, 'Terms &amp; Conditions', '', '<p style="text-align: center"><strong>RATES</strong></p>\r\n<div>\r\n<table id="tbl-rate">\r\n<tbody>\r\n<tr>\r\n<td style="text-align: center;width: 100px"><span>Season</span></td>\r\n<td style="text-align: center;width: 200px"><span>Period</span></td>\r\n<td style="text-align: center;width: 100px"><span>Per Night</span></td>\r\n<td style="text-align: center;width: 100px"><span>Per 7+ Night</span></td>\r\n<td style="text-align: center;width: 120px"><span>Minimum Stay</span></td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n<td style="text-align: center">&nbsp;</td>\r\n<td style="text-align: center">&nbsp;</td>\r\n<td style="text-align: center">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center"><span>Low Season</span></td>\r\n<td style="text-align: center"><span>11 Apr 2015 to 15 Jun 2015</span></td>\r\n<td style="text-align: center"><span>US$2,350++</span></td>\r\n<td style="text-align: center"><span>US$2,050++</span></td>\r\n<td style="text-align: center"><span>2 nights</span></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center"><span>High Season</span></td>\r\n<td style="text-align: center"><span>16 Jun 2015 to 15 Sep 2015</span></td>\r\n<td style="text-align: center"><span>US$2,750++</span></td>\r\n<td style="text-align: center"><span>US$2,400++</span></td>\r\n<td style="text-align: center"><span>3 nights</span></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center"><span>Low Season</span></td>\r\n<td style="text-align: center"><span>16 Sep 2015 to 18 Dec 2015</span></td>\r\n<td style="text-align: center"><span>US$2,350++</span></td>\r\n<td style="text-align: center"><span>US$2,050++</span></td>\r\n<td style="text-align: center"><span>2 nights</span></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center"><span>Peak Season</span></td>\r\n<td style="text-align: center"><span>19 Dec 2015 to 05 Jan 2016</span></td>\r\n<td style="text-align: center"><span>US$3,200++</span></td>\r\n<td style="text-align: center"><span>US$2,800++</span></td>\r\n<td style="text-align: center"><span>4 nights</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<p><br />++ All rates are subject to a 10% government tax and 5% service charge&nbsp;</p>\r\n<p>Special last minute rates are often available within two weeks of your intended arrival. Please enquire if dates in which you are interested appear to be available in the booking calendar. &nbsp;&nbsp;&nbsp;</p>\r\n<p>US$3,000++ is payable per event, such as a party of a wedding, for up to 150 guests, which includes all banjar and other fees. Above 150 guests there is an additional charge of US$5.00++ per person.</p>\r\n<p style="text-align: center"><strong>PAYMENT TERMS</strong></p>\r\n<p>Payment terms are 50% deposit for confirmation (payment must be made within 5 days of the provision of a quotation to be binding on Villa Vedas), and the balance of 50% at least 60 days prior to check in. If bookings are made within 60 days of the date of arrival, the full rental amount is payable. &nbsp;</p>\r\n<p>If payments are not made on or before the specified dates, Villa Vedas reserves the right to cancel the booking. In the event of the guest&rsquo;s failure to make payments as they fall due, cancellation charges as detailed below may apply.</p>\r\n<p style="text-align: center"><strong>CURRENCY OF INVOICE</strong></p>\r\n<p><strong>&nbsp;</strong>Upon request Villa Vedas offers pricing (using prevailing mid market foreign exchange rates for the preferred currency at the time of booking) and invoicing in Indonesian Rupiah, Pounds Sterling, Euros, Swiss Francs, Australian Dollars, New Zealand Dollars, Singapore Dollars, Hong Kong Dollars, Canadian dollars, Thai Baht, Malaysian Ringgit and Japanese Yen.</p>\r\n<p style="text-align: center"><strong>CONFIRMATION OF BOOKING</strong></p>\r\n<p>On receipt of the reservation inquiry form, Villa Vedas will email or fax a confirmation of availability to the guest, together with an invoice setting out the total price (including any applicable taxes), and the deposit required to confirm the booking.</p>\r\n<p>The invoice and bank account details for the transfer of funds will be sent at the same time as the tentative booking confirmation. Invoicing will be in US Dollars unless another currency is requested, in which case the invoice will be in the preferred currency, and the bank account details provided will be capable of receiving payment in the preferred currency.</p>\r\n<p>After confirmation of a booking and payment of the appropriate deposit, Villa Vedas will furnish guests with a reservation voucher setting out the terms and conditions of the rental. Guests are encouraged to read this document carefully, and to revert to us as soon as possible in the event of any disagreement.&nbsp;</p>\r\n<p style="text-align: center"><strong>CANCELLATION POLICY</strong></p>\r\n<p>If the guest wishes to cancel a confirmed booking, written notice of cancellation should be sent to Villa Vedas. All cancellation notices received by Villa Vedas will be acknowledged in writing.</p>\r\n<p>Cancellation of a booking includes, but is not necessarily limited to, any of the following:</p>\r\n<ul>\r\n<li>Cancellation of one or more days of a booking</li>\r\n<li>Changes in the booking so that none of the revised dates fall within the original booking dates</li>\r\n<li>Failure of the guests to provide required documents (passport or other ID) on arrival</li>\r\n<li>Attempt by the guest to hold an event in breach of Villa Vedas&rsquo; terms and conditions</li>\r\n</ul>\r\n<p>Should a guest need to cancel a confirmed booking, the following cancellation fees will apply:</p>\r\n<ul>\r\n<li>10% of the total rental amount in the event of cancellation 90 or more days before arrival</li>\r\n<li>20% of the total rental amount in the event of cancellation 60 to 89 days before arrival</li>\r\n<li>50% of the total rental amount in the event of cancellation 30 to 59 days before arrival</li>\r\n<li>75% of the total rental amount in the event of cancellation 29 or less days before arrival</li>\r\n</ul>\r\n<p>The applicable cancellation fees will be deducted from payments already made by guests, and the remaining balance refunded promptly to the guest by Villa Vedas.</p>\r\n<p>Notwithstanding the foregoing, it is not Villa Vedas&rsquo; intention to benefit from cancellations, which are normally for unexpected reasons. In certain circumstances Villa Vedas may, <span style="text-decoration: underline">at its absolute discretion</span>, waive some or all of the cancellation fees, and / or offer alternative dates to a guest in a value equivalent to some or all of the money they have paid. If an unexpected event has forced you to cancel your booking, please talk to us about your circumstances, and we will do what we can to help. &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n<p style="text-align: center"><strong>ARRIVAL AND DEPARTURE TIMES</strong></p>\r\n<p>Typical check-out and check-in times are 12:00 noon and 3:00pm respectively unless stated otherwise. Villa Vedas will work with the guests to try to accommodate the guest&rsquo;s actual arrival and departure times, subject to availability. Guests are encouraged to advise Villa Vedas of any changes to their schedule so every effort can be made to accommodate them.</p>\r\n<p style="text-align: center"><strong>RENTAL INCLUSIONS</strong></p>\r\n<p>The cost of electricity, water, cleaning and garden supplies, and local taxes are included in rental rates. There are generally no additional costs, surcharges, taxes, staff salaries, or management fees above the price quoted unless otherwise stated in the reservation voucher.</p>\r\n<p>Inclusions in the rental price are as follows: &nbsp;<strong>&nbsp;</strong></p>\r\n<ul>\r\n<li>Luxury accommodation for up to 10 adults</li>\r\n<li>Airport transfers (helicopter transfers can be arranged at cost)</li>\r\n<li>Personal chef and fully-equipped kitchen</li>\r\n<li>Personal villa manager to organize all your needs</li>\r\n<li>24 hour butler service</li>\r\n<li>Complimentary full breakfast at a time of your choosing</li>\r\n<li>Chambermaid and housekeeping services</li>\r\n<li>24 hour uniformed security</li>\r\n<li>Car and chauffeur for 10 hours per day (excluding fuel)</li>\r\n<li>Chauffeur overtime payable at US$10 / hour&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>In villa spa treatments and massage available upon request </li>\r\n<li>Child minding service available upon request</li>\r\n<li>Laundry and dry cleaning services available upon request </li>\r\n<li>Free wifi throughout the property</li>\r\n</ul>\r\n<p>We have the ability to sleep up to 14 adults with the use of extra beds. Charges for additional guests are as follows:</p>\r\n<ul>\r\n<li>0 &ndash; 5 years old: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; free of charge</li>\r\n<li>6+ years old: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; US$50.00++ per person per night</li>\r\n</ul>\r\n<p style="text-align: center"><strong>EVENT INCLUSIONS</strong></p>\r\n<ul>\r\n<li>Access for event vendors and their equipment&nbsp;</li>\r\n<li>Additional maintenance / cleaning requirements </li>\r\n<li>Application and fee payment to Kapolsek (Police) for Ijin Keramaian (license to hold a gathering of more than 20 people) </li>\r\n<li>Registry and fee payment to Kepala Desa&rsquo;s office for accommodated guests </li>\r\n<li>Registry and fee payment to Kepala Desa&rsquo;s office for invited guests </li>\r\n<li>Fee Pencalang (Village Security fee) </li>\r\n<li>Use of parking space and supervision </li>\r\n<li>Use of generator to supply additional power (if required)</li>\r\n</ul>\r\n<p style="text-align: center"><strong>Other Requirements</strong></p>\r\n<ul>\r\n<li>Curfew for amplified music is NOT APPLICAABLE AT THIS POINT IN TIME however it may change </li>\r\n<li>Fireworks are not permitted from the Villa but can be arranged from the beach in front</li>\r\n<li>Laser show not permitted </li>\r\n<li>Pyrotechnics, fire dance, sky lanterns are permitted</li>\r\n</ul>\r\n<p style="text-align: center"><strong>CHANGES TO BOOKINGS</strong></p>\r\n<p>All requests for changes to bookings must be made in writing. While Villa Vedas will try to accommodate requests for booking changes, this can only occur subject to availability.</p>\r\n<p>Please note that a request for a change to revised dates, none of which fall within the original booking dates, can be treated by Villa Vedas as a cancellation of the original booking (see Villa Vedas&rsquo; cancellation policy, details of which are set out above).</p>\r\n<p style="text-align: center"><strong>FUNCTIONS AND EVENTS</strong></p>\r\n<p>Villa Vedas must approve any functions or events you plan to hold during your stay at our property for guest numbers that exceed more than 20 guests. Such approval is at the sole discretion of Villa Vedas, and will be priced according to our published prices.</p>\r\n<p>While Villa Vedas will always listen to the guest&rsquo;s event plans, and will try to accommodate his or her wishes, nevertheless Villa Vedas reserves the right to require the appointment of a professional event organizer before permission to hold a function is granted.</p>\r\n<p style="text-align: center"><strong>GOVERNMENT TAXES AND SERVICE CHARGE</strong></p>\r\n<p>Quoted rates are exclusive of tax and service charges, which total 15%.</p>\r\n<p>Government taxes of 10% are charged in accordance with applicable government regulations. A service charge of 5% is also applied, which is the industry standard.</p>\r\n<p style="text-align: center"><strong>SECURITY DEPOSIT</strong></p>\r\n<p>A security deposit may be payable to Villa Vedas upon the guests&rsquo; arrival at the Property. The standard security deposit is $1,500, and a credit card authorization in such an amount will normally suffice. &nbsp;</p>\r\n<p>However, Villa Vedas reserves the right to request a larger security bond if, in its opinion, a larger bond is required. When a non-standard security deposit applies, Villa Vedas will inform the guest in advance of the amount payable, and any other special terms and conditions.</p>\r\n<p>Guests will also be required to sign a Waiver of Liability form at check in.</p>\r\n<p style="text-align: center"><strong>DAMAGES, BREAKAGES AND LOSSES</strong></p>\r\n<p>Guests are responsible for looking after the property, and leaving it in good order, and in a clean condition. Guests also undertake to pay for damages, breakages or losses for which they are responsible during the period of their stay.</p>\r\n<p>Villa Vedas reserves the right to terminate the contract, and to require guests and visitors to vacate the premises, without any refund or other compensation, if the guest or his or her visitors cause excessive damage or mess during the rental period.</p>\r\n<p style="text-align: center"><strong>INSURANCE RECOMMENDED</strong></p>\r\n<p>We recommend that guests take out comprehensive travel insurance at the time of booking to protect themselves for the full period of their visit against illness (including medical evacuation), injury, death, loss of baggage and personal items, theft, cancellation and other travel contingencies.</p>\r\n<p style="text-align: center"><strong>LIMIT OF LIABILITY</strong></p>\r\n<p>Please be advised that Villa Vedas is not responsible for any loss or damage to personal equipment and property during the rental period. We will not accept responsibility for any delay, additional expense or inconvenience which maybe caused directly or indirectly by events outside of our control, such as late arrival of international flights, civil disturbances, fire, floods, unusually severe weather, acts of God, acts of Government, or the failure of any machinery or equipment.</p>', 'publish', 'pages', 'allowed', 0, 0, 'terms-conditions', 21, 1, '2014-08-29 18:01:48', 1, '2015-04-13 14:40:05', 0),
(353, 'CONTACT US', '', '', 'publish', 'pages', 'allowed', 0, 0, 'contact-us', 20, 1, '2014-08-29 18:02:04', 1, '2015-04-13 16:40:14', 0),
(354, 'Reservation', '', '', 'publish', 'pages', 'allowed', 0, 0, 'reservation', 19, 1, '2014-08-29 18:03:37', 1, '2014-08-29 18:03:37', 0),
(355, 'Staff', '', '<p>The staff at Villa Vedas is trained to provide intimate and discreet service, enhancing guest&rsquo;s privacy and luxury experience. Our team is comprised of a general manager and assistant manager, chefs, butlers, maids and housekeepers, drivers, bar staff, and security.</p>\r\n<p><a title="staff" href="http://localhost/villa-vedas/lumonata-content/files/201409/staff-1410496478.jpg"><img src="http://localhost/villa-vedas/lumonata-content/files/201409/staff-1410496478.jpg" alt="" /></a></p>', 'unpublish', 'articles', 'allowed', 0, 0, 'staff', 17, 1, '2014-09-11 18:08:27', 1, '2015-04-13 16:25:30', 0),
(356, 'Floor Plan', '', '<p style="text-align: center"><a title="layout-villa-vedas-1st-ffl-240315" href="http://villavedas.com/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961-large.jpg"><img style="float: none" src="http://villavedas.com/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961-large.jpg" alt="" /></a></p>\r\n<p style="text-align: center"><a title="layout-villa-vedas-2nd-fl-240315" href="http://villavedas.com/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044-large.jpg"><img style="float: none" src="http://villavedas.com/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044-large.jpg" alt="" /></a></p>', 'publish', 'articles', 'allowed', 0, 0, 'floor-plan', 18, 1, '2014-09-11 18:09:16', 1, '2015-04-13 17:12:07', 0),
(357, 'Facilities', '', '<p style="text-align: center">The building itself is approximately 1,800 m2 of construction, and is a modern architecturally designed home that is unique and unequalled, with many design and layout features.</p>\r\n<p style="text-align: center">The finishes include polished concrete, stone walls and pillars, natural marble floors, teak staircase and extensive paneling.</p>\r\n<p style="text-align: center">The property features a large living area, two dining areas, an art gallery area and a media room (or office / snooker room).</p>\r\n<p style="text-align: center">The living area can be opened to benefit from the regular breeze off the ocean, or alternatively enclosed and air conditioned by deploying the Hafele glass sliding wall system.</p>\r\n<p style="text-align: center">The centre stairs to the upstairs bedrooms are a work of art, and are suspended on long brass rods which link into custom designed brass corner units which hold the stairs together.</p>\r\n<p style="text-align: center">&nbsp;The villa features Hafele rolling glass doors throughout, a high quality home automation system and fabulous sound system. &nbsp; &nbsp;</p>\r\n<p style="text-align: center">Upstairs is a 6 metre x 6 metre glass bottom plunge pool suspended between two of the bedrooms.</p>\r\n<p style="text-align: center">The laminated glass floor features a switching nano glass film that enables the owner of his or her guests to turn the glass from clear to opaque at the flick of a switch for the ultimate in privacy.</p>\r\n<p style="text-align: center">The property features a show kitchen and a commercial kitchen. The show kitchen is finished with glass doors, black granite benches and an extensive array of Gaggenau appliances.</p>\r\n<p style="text-align: center">The commercial kitchen is located behind the show kitchen, and features a wide variety of appliances, walk in cool room and a range of burners and ovens.</p>\r\n<p style="text-align: center">The gardens are a mix of tropical and minimalistic, with green vertical gardens on the property walls, and various roof top gardens.</p>\r\n<p style="text-align: center">There are three swimming pools in total, with two on the ground level and a plunge pool on the second floor. The main pool is 40 metres, and the second pool 20 metres in length.</p>\r\n<p style="text-align: center">The gardens also feature a large gazebo area facing the beach with BBQ facilities including gas, water, and USB connection for DJs into the extensive indoor and outdoor sound system.</p>\r\n<p style="text-align: center">There is also a separate staff quarters, which can be used as additional accommodation for guests if required, featuring four bedrooms, two bathrooms, a large kitchen and a commercial laundry area.</p>', 'publish', 'articles', 'allowed', 0, 0, 'facilities', 16, 1, '2014-09-12 14:34:49', 1, '2016-10-11 11:25:28', 0),
(358, 'Home Theater', '', '<p>The villa uses a reverse osmosis and oxygenating filtration system, which cleanses and UV stabilises our deep spring water. This ensures the enjoyment of the most pure H20 from every tap in the villa, eliminating concerns with the use of water in Bali.</p>', 'unpublish', 'articles', 'allowed', 0, 0, 'home-theater', 15, 1, '2014-09-12 14:59:37', 1, '2015-04-13 14:12:04', 0),
(359, 'Dining Room', '', '<p>The villa uses a reverse osmosis and oxygenating filtration system, which cleanses and UV stabilises our deep spring water. This ensures the enjoyment of the most pure H20 from every tap in the villa, eliminating concerns with the use of water in Bali.</p>', 'unpublish', 'articles', 'allowed', 0, 0, 'dining-room', 14, 1, '2014-09-12 15:00:09', 1, '2015-04-13 14:12:04', 0),
(360, 'Services', '', '<p style="text-align: center"><strong>Kitchens</strong></p>\r\n<p style="text-align: center"><strong>&nbsp;</strong>Show kitchen with Gaggenau appliances including 2 ovens, warming tray, microwave, coffee machine concealed fridge, 2 wine fridges, steamer, teppanyaki, bench grill, electric touch control hotplate and gas wok burner,</p>\r\n<p style="text-align: center">Commercial kitchen, walk-in cool room, 6 burner oven, large griddle, chargrill, combi oven, long fridge and counter, stand up freezer, separate wash up and sink area, commercial dish washer and various stainless steel shelves and benches.</p>\r\n<p style="text-align: center"><strong>Bar Area</strong></p>\r\n<p style="text-align: center">Includes large custom brass top bar, stainless steel benches, fridges, sink, ice makers, glass washers plus various glass shelving and displays and sound system.</p>\r\n<p style="text-align: center"><strong>Smart Home</strong></p>\r\n<p style="text-align: center"><strong>&nbsp;</strong>Philips Dynalite lighting system which allows any light or group of lights to be controlled by any of the lighting controller units throughout the house. There are a total of 12 controller units, with six lighting buttons on each.</p>\r\n<p style="text-align: center">Control 4 Home Automation system which allows connection via WiFi from android, iOS, PC or Mac to control any of the lighting channels specified above, any of the air-conditioning units throughout, and also to stream music from the user&rsquo;s device or a central server to any combination of the four zones of the sound system specified below.</p>\r\n<p style="text-align: center"><strong>Sound System</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Includes a fully built in professional sound system throughout the home and gardens with JBL speakers and more than 10kW of Crown Amplifiers.</p>\r\n<p style="text-align: center">An Audio matrix system splits the system into 4 zones being Garden (8 Speakers and 2 subwoofers), Bar (4 speakers and a subwoofer), Dining Area (12 speakers and 2 subwoofers) and Media Room (2 speakers and a subwoofer).</p>\r\n<p style="text-align: center">The system has 4 audio inputs streamed individually or via the Control 4 system and each input can be routed via the matrix to each or more of the 4 zones, hence each of the 4 zones can be utilized individually or concurrently.</p>\r\n<p style="text-align: center"><strong>Electricity</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The property has a 66 kVA mains power supply plus a 30 kVA diesel generator which is used in the event of a temporary loss of mains power.</p>\r\n<p style="text-align: center"><strong>Wifi and 3G Booster</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The villa has a Unifi WiFi system with 3 antennas throughout the property for full WiFi coverage. This is a wireless-N (high speed) system with gigabit connections throughout.</p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There is also a 3G booster for amplifying mobile, internet and telephone systems.</p>\r\n<p style="text-align: center"><strong>Engineering</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There are concealed pump rooms and water storage areas for both the pools, and potable water is produced by a sophisticated water filtration system.</p>\r\n<p style="text-align: center"><strong>Decorations</strong></p>\r\n<p style="text-align: center">The property has a variety of quality timberwork, and wall paneling is with solid teak timber. In addition there are various other fabric and feature walls to enhance the natural modern feel.</p>\r\n<p style="text-align: center"><strong>Furnishings</strong></p>\r\n<p style="text-align: center">The bedrooms feature in-built beds, linen and curtains etc but the owner is considering sell the villa without soft furnishings so that a buyer can complete the villa to his or her own style and taste.</p>\r\n<p style="text-align: center">However, we are currently in the process of producing a selection of suggested furnishing designs, which can be made available upon request.</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>', 'publish', 'articles', 'allowed', 0, 0, 'services', 13, 1, '2014-09-12 16:55:43', 1, '2016-10-11 12:19:35', 0),
(221, 'social media', '', '', 'publish', 'social_media', '', 0, 0, 'social_media', 153, 1, '2014-01-20 17:38:22', 1, '2014-01-20 17:38:22', 0),
(361, 'South Beach Suite', '', '<p><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. <!-- pagebreak -->Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>', 'publish', 'villas', 'allowed', 0, 0, 'south-beach-suite', 12, 1, '2014-09-15 16:53:13', 1, '2014-09-19 10:02:58', 0),
(347, 'North Beach Suite', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<!-- pagebreak --> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br /></span></p>\r\n<p><span><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></span></p>\r\n<p><span><span><span>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</span><br /></span></span></p>', 'publish', 'villas', 'allowed', 0, 0, 'villa-a', 26, 1, '2014-08-28 16:41:08', 1, '2014-09-19 10:03:45', 0),
(330, 'Extra Bed', '', 'hoho', 'publish', 'additional_service', '', 0, 0, 'extra-bed', 42, 1, '2014-05-14 17:46:36', 1, '2014-05-14 17:47:06', 0),
(331, 'Thai Body Work', '', 'Thai Massage', 'publish', 'additional_service', '', 0, 0, 'thai-body-work', 42, 1, '2014-05-14 17:48:20', 1, '2014-05-14 17:48:28', 0),
(365, 'Reservation', '', '', 'publish', 'pages', 'allowed', 0, 0, 'reservation-form', 8, 1, '2015-04-13 13:43:29', 1, '2015-04-13 13:43:29', 0),
(366, 'Success Inquiry', '', '<p style="text-align: center">Your Booking Inquiry has been Sent.</p>\r\n<p style="text-align: center">The Villa Vedas booking office is located at Pantai Batu Tampih Kangin, Kediri,Tabanan and is open 9am-5pm, Monday-Friday.</p>\r\n<p style="text-align: center">Please allow 24 hours for a response on these days.</p>\r\n<p style="text-align: center">All enquires sent through on weekends and public holidays will be responded to on the next working day.</p>', 'publish', 'pages', 'allowed', 0, 0, 'success-inquiry', 7, 1, '2015-04-13 13:44:38', 1, '2015-04-13 13:44:38', 0),
(367, 'Success Paying Down Payment', '', '<h1 style="text-align: center">Success Payment</h1>\r\n<p style="border-bottom: solid 1px #fff;width: 160px;margin: 0 auto 20px">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">Down Payment has been successfully made. Thank you for using our services.</p>', 'publish', 'pages', 'allowed', 0, 0, 'success-dp', 6, 1, '2015-04-13 13:45:19', 1, '2015-04-13 13:45:19', 0),
(368, 'Success Paying Balance Payment', '', '<h1 style="text-align: center">Success Payment</h1>\r\n<p style="border-bottom: solid 1px #fff;width: 160px;margin: 0 auto 20px">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">Balance Payment has been successfully made. Thank you for using our services.</p>', 'publish', 'pages', 'allowed', 0, 0, 'success-bp', 5, 1, '2015-04-13 13:46:03', 1, '2015-04-13 13:46:03', 0),
(369, 'Success Paying Full Payment', '', '', 'publish', 'pages', 'allowed', 0, 0, 'success-fp', 4, 1, '2015-04-13 13:46:44', 1, '2015-04-13 13:46:44', 0),
(370, 'Events', '', '<p>The villa was designed with weddings in mind, where the whole bridal party can stay and prepare for the big day. We can seat up to 300 guests comfortably throughout our extensive gardens and indoors. Nonetheless, we can accommodate all event sizes, from intimate and exclusive dinners and corporate parties to birthdays and concerts.</p>\r\n<p><a title="shutterstock121949203" href="http://villavedas.com/lumonata-content/files/201504/shutterstock121949203-1428906147-medium.jpg"><img style="float: none" src="http://villavedas.com/lumonata-content/files/201504/shutterstock121949203-1428906147-medium.jpg" alt="" /></a></p>\r\n<p>Our professional hospitality team can assist with smaller events. For bigger events, we can suggest the best party and wedding planners from around Australia and Indonesia that specialize in providing everything from catering, flowers, d&eacute;cor, dancers and DJs and anything else that will help to make the wedding day or special event unforgettable.</p>\r\n<p><a title="shutterstock196507181" href="http://villavedas.com/lumonata-content/files/201504/shutterstock196507181-1428906175-medium.jpg"><img style="float: none" src="http://villavedas.com/lumonata-content/files/201504/shutterstock196507181-1428906175-medium.jpg" alt="" /></a></p>\r\n<p><strong>Event Fees &amp; Conditions</strong><strong>&nbsp;</strong></p>\r\n<ul>\r\n<li>US$3,000++ is payable per event, such as a party of a wedding, for up to 150 guests, which includes all local government and other fees. Above 150 guests there is an additional charge of US$5.00++ per person.</li>\r\n<li>2 nights minimum stay low season</li>\r\n<li>3 nights minimum stay high season</li>\r\n<li>4 nights minimum stay peak season</li>\r\n</ul>\r\n<p><strong>Event Inclusions</strong></p>\r\n<ul>\r\n<li>Sound Equipment</li>\r\n<li>Application and fee payment for event license and security, payable to Indonesia Police and local government agencies</li>\r\n<li>Use of parking space and supervision</li>\r\n<li>Use of generator to supply additional power (if required)</li>\r\n</ul>\r\n<p><strong>Other Conditions</strong></p>\r\n<ul>\r\n<li>Curfew for amplified music is not applicable at this point in time however this regulation may change</li>\r\n<li>Fireworks may not be set off within the villa&rsquo;s grounds but can be arranged on the beachfront</li>\r\n<li>Laser show is not permitted</li>\r\n<li>Pyrotechnics, fire dance, sky lanterns are permitted.</li>\r\n</ul>', 'publish', 'articles', 'allowed', 0, 0, 'events', 3, 1, '2015-04-13 14:25:59', 1, '2015-04-13 14:31:17', 0),
(371, 'Bedrooms', '', '<p style="text-align: center">Villa Vedas features five master bedrooms, all with large ensuites and attractive views.</p>\r\n<p style="text-align: center">The bedrooms are expansive, and have large built in wardrobes and furnishings plus other unique design features, including brass cast lighting and bamboo ceilings.</p>\r\n<p style="text-align: center">Each bedroom also has a king size King Koil quality mattress, full height block out curtains and matching bed linen.</p>\r\n<p style="text-align: center">The staff quarters feature an additional four bedrooms, two bathrooms and a kitchen which can be used by children r additional guests as necessary.&nbsp;</p>', 'publish', 'articles', 'allowed', 0, 0, 'bedrooms', 2, 1, '2015-04-13 16:19:14', 1, '2016-10-15 11:16:32', 0),
(372, 'Technical', '', '<p style="text-align: center"><strong>Kitchens</strong></p>\r\n<p style="text-align: center"><strong>&nbsp;</strong>Show kitchen with Gaggenau appliances including 2 ovens, warming tray, microwave, coffee machine concealed fridge, 2 wine fridges, steamer, teppanyaki, bench grill, electric touch control hotplate and gas wok burner,</p>\r\n<p style="text-align: center">Commercial kitchen, walk-in cool room, 6 burner oven, large griddle, chargrill, combi oven, long fridge and counter, stand up freezer, separate wash up and sink area, commercial dish washer and various stainless steel shelves and benches.</p>\r\n<p style="text-align: center"><strong>Bar Area</strong></p>\r\n<p style="text-align: center">Includes large custom brass top bar, stainless steel benches, fridges, sink, ice makers, glass washers plus various glass shelving and displays and sound system.</p>\r\n<p style="text-align: center"><strong>Smart Home</strong></p>\r\n<p style="text-align: center"><strong>&nbsp;</strong>Philips Dynalite lighting system which allows any light or group of lights to be controlled by any of the lighting controller units throughout the house. There are a total of 12 controller units, with six lighting buttons on each.</p>\r\n<p style="text-align: center">Control 4 Home Automation system which allows connection via WiFi from android, iOS, PC or Mac to control any of the lighting channels specified above, any of the air-conditioning units throughout, and also to stream music from the user&rsquo;s device or a central server to any combination of the four zones of the sound system specified below.</p>\r\n<p style="text-align: center"><strong>Sound System</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Includes a fully built in professional sound system throughout the home and gardens with JBL speakers and more than 10kW of Crown Amplifiers.</p>\r\n<p style="text-align: center">An Audio matrix system splits the system into 4 zones being Garden (8 Speakers and 2 subwoofers), Bar (4 speakers and a subwoofer), Dining Area (12 speakers and 2 subwoofers) and Media Room (2 speakers and a subwoofer).</p>\r\n<p style="text-align: center">The system has 4 audio inputs streamed individually or via the Control 4 system and each input can be routed via the matrix to each or more of the 4 zones, hence each of the 4 zones can be utilized individually or concurrently.</p>\r\n<p style="text-align: center"><strong>Electricity</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The property has a 66 kVA mains power supply plus a 30 kVA diesel generator which is used in the event of a temporary loss of mains power.</p>\r\n<p style="text-align: center"><strong>Wifi and 3G Booster</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The villa has a Unifi WiFi system with 3 antennas throughout the property for full WiFi coverage. This is a wireless-N (high speed) system with gigabit connections throughout.</p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There is also a 3G booster for amplifying mobile, internet and telephone systems.</p>\r\n<p style="text-align: center"><strong>Engineering</strong></p>\r\n<p style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There are concealed pump rooms and water storage areas for both the pools, and potable water is produced by a sophisticated water filtration system.</p>\r\n<p style="text-align: center"><strong>Decorations</strong></p>\r\n<p style="text-align: center">The property has a variety of quality timberwork, and wall paneling is with solid teak timber. In addition there are various other fabric and feature walls to enhance the natural modern feel.</p>\r\n<p style="text-align: center"><strong>Furnishings</strong></p>\r\n<p style="text-align: center">The bedrooms feature in-built beds, linen and curtains etc but the owner is considering selling the villa without soft furnishings so that a buyer can complete the villa to his or her own style and taste.</p>\r\n<p style="text-align: center">However, we have produced a selection of suggested furnishing designs, which are available upon request.</p>\r\n<p style="text-align: center">&nbsp;</p>\r\n<p style="text-align: center">&nbsp;</p>', 'publish', 'articles', 'allowed', 0, 0, 'technical', 1, 1, '2016-10-15 11:46:57', 1, '2016-10-15 11:46:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_attachment`
--

CREATE TABLE IF NOT EXISTS `lumonata_attachment` (
  `lattach_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_thumb` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_medium` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_large` text CHARACTER SET utf8 NOT NULL,
  `ltitle` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lalt_text` text CHARACTER SET utf8 NOT NULL,
  `lcaption` varchar(200) CHARACTER SET utf8 NOT NULL,
  `mime_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '0',
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY (`lattach_id`),
  KEY `article_id` (`larticle_id`),
  KEY `attachment_title` (`ltitle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=282 ;

--
-- Dumping data for table `lumonata_attachment`
--

INSERT INTO `lumonata_attachment` (`lattach_id`, `larticle_id`, `lattach_loc`, `lattach_loc_thumb`, `lattach_loc_medium`, `lattach_loc_large`, `ltitle`, `lalt_text`, `lcaption`, `mime_type`, `lorder`, `upload_date`, `date_last_update`) VALUES
(265, 355, '/lumonata-content/files/201409/staff-1410496478.jpg', '/lumonata-content/files/201409/staff-1410496478-thumbnail.jpg', '/lumonata-content/files/201409/staff-1410496478-medium.jpg', '/lumonata-content/files/201409/staff-1410496478-large.jpg', 'staff', '', '', 'image/jpeg', 16, '2014-09-12 06:34:40', '2014-09-12 06:34:53'),
(266, 112, 'about-us-1410502921.jpg', 'about-us-1410502921-thumbnail.jpg', '', '', 'About Us', '', '', 'image/jpeg', 16, '2014-09-12 14:22:03', '2014-09-12 14:22:03'),
(267, 359, 'dining-room-1410507182.jpg', 'dining-room-1410507182-thumbnail.jpg', '', '', 'Dining Room', '', '', 'image/jpeg', 15, '2014-09-12 15:33:02', '2014-09-12 15:33:02'),
(268, 357, 'facilities-1410507810.jpg', 'facilities-1410507810-thumbnail.jpg', '', '', 'Facilities', '', '', 'image/jpeg', 14, '2014-09-12 15:43:31', '2014-09-12 15:43:31'),
(269, 354, 'reservation-1410757023.jpg', 'reservation-1410757023-thumbnail.jpg', '', '', 'Reservation', '', '', 'image/jpeg', 13, '2014-09-15 12:57:04', '2014-09-15 12:57:04'),
(272, 347, '/lumonata-content/files/201409/bx7440resize-1411035422.jpg', '/lumonata-content/files/201409/bx7440resize-1411035422-thumbnail.jpg', '/lumonata-content/files/201409/bx7440resize-1411035422-medium.jpg', '/lumonata-content/files/201409/bx7440resize-1411035422-large.jpg', 'bx7440resize', '', '', 'image/jpg', 10, '2014-09-18 12:17:07', '2014-09-18 12:17:07'),
(273, 353, 'contact-us-1411119767.jpg', 'contact-us-1411119767-thumbnail.jpg', '', '', 'Contact Us', '', '', 'image/jpeg', 9, '2014-09-19 17:42:47', '2014-09-19 17:42:47'),
(274, 370, '/lumonata-content/files/201504/shutterstock121949203-1428906147.jpg', '/lumonata-content/files/201504/shutterstock121949203-1428906147-thumbnail.jpg', '/lumonata-content/files/201504/shutterstock121949203-1428906147-medium.jpg', '/lumonata-content/files/201504/shutterstock121949203-1428906147-large.jpg', 'shutterstock121949203', '', '', 'image/jpeg', 8, '2015-04-13 01:22:28', '2015-04-13 01:22:28'),
(275, 370, '/lumonata-content/files/201504/shutterstock196507181-1428906175.jpg', '/lumonata-content/files/201504/shutterstock196507181-1428906175-thumbnail.jpg', '/lumonata-content/files/201504/shutterstock196507181-1428906175-medium.jpg', '/lumonata-content/files/201504/shutterstock196507181-1428906175-large.jpg', 'shutterstock196507181', '', '', 'image/jpeg', 7, '2015-04-13 01:22:56', '2015-04-13 01:25:52'),
(276, 118, 'events-1428906606.jpg', 'events-1428906606-thumbnail.jpg', '', '', 'Events', '', '', 'image/jpeg', 6, '2015-04-13 14:30:07', '2015-04-13 14:30:07'),
(277, 114, 'bedrooms-1428913104.jpg', 'bedrooms-1428913104-thumbnail.jpg', '', '', 'Bedrooms', '', '', 'image/jpeg', 5, '2015-04-13 16:18:24', '2015-04-13 16:18:24'),
(278, 112, 'about-us-1428913343.jpg', 'about-us-1428913343-thumbnail.jpg', '', '', 'About Us', '', '', 'image/jpeg', 4, '2015-04-13 16:22:23', '2015-04-13 16:22:23'),
(279, 115, 'services-1428913795.jpg', 'services-1428913795-thumbnail.jpg', '', '', 'Services', '', '', 'image/jpeg', 3, '2015-04-13 16:29:55', '2015-04-13 16:29:55'),
(280, 356, '/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961.jpg', '/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961-thumbnail.jpg', '/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961-medium.jpg', '/lumonata-content/files/201504/layout-villa-vedas-1st-ffl-240315-1428915961-large.jpg', 'layout-villa-vedas-1st-ffl-240315', '', '', 'image/jpeg', 1, '2015-04-13 04:06:02', '2015-04-13 04:10:44'),
(281, 356, '/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044.jpg', '/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044-thumbnail.jpg', '/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044-medium.jpg', '/lumonata-content/files/201504/layout-villa-vedas-2nd-fl-240315-1428916044-large.jpg', 'layout-villa-vedas-2nd-fl-240315', '', '', 'image/jpeg', 2, '2015-04-13 04:07:24', '2015-04-13 04:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_availability`
--

CREATE TABLE IF NOT EXISTS `lumonata_availability` (
  `ldate` int(11) NOT NULL COMMENT 'Date',
  `lacco_id` int(11) NOT NULL COMMENT 'Accommodation ID',
  `lrate` decimal(10,2) NOT NULL,
  `lrate_additional` decimal(10,2) NOT NULL,
  `lseason` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lstatus` int(2) NOT NULL COMMENT '0=booked;1=available;2=no-check-in;3=no-check-out;4=no-check-in-out;5=booking-hold;6=owner;7=maintance;',
  `lreason` text CHARACTER SET utf8 NOT NULL,
  `ledit` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'Rate;Allotment;Commission = ex: 1;0;1',
  `lcreated_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`ldate`,`lacco_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lumonata_availability`
--

INSERT INTO `lumonata_availability` (`ldate`, `lacco_id`, `lrate`, `lrate_additional`, `lseason`, `lstatus`, `lreason`, `ledit`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(1410969600, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411056000, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411142400, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411228800, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411315200, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411401600, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411488000, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411574400, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411660800, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411747200, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411833600, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411920000, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1412006400, 347, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1410969600, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411056000, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411142400, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411228800, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411315200, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411401600, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411488000, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411574400, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411660800, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411747200, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411833600, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1411920000, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1412006400, 361, '1.15', '0.65', 'Low Season', 1, '', 'admin', 'admin', 1411034217, 'admin', 1411034217),
(1428940800, 347, '100.00', '100.00', '', 1, '', 'admin', 'admin', 1429003779, 'admin', 1429003779),
(1429027200, 347, '100.00', '100.00', '', 1, '', 'admin', 'admin', 1429003779, 'admin', 1429003779),
(1429113600, 347, '100.00', '100.00', '', 1, '', 'admin', 'admin', 1429003779, 'admin', 1429003779),
(1429200000, 347, '100.00', '100.00', '', 1, '', 'admin', 'admin', 1429003779, 'admin', 1429003779),
(1429286400, 347, '100.00', '100.00', '', 1, '', 'admin', 'admin', 1429003779, 'admin', 1429003779);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_booking`
--

CREATE TABLE IF NOT EXISTS `lumonata_booking` (
  `lbook_id` int(11) NOT NULL,
  `larticle_id` bigint(20) NOT NULL DEFAULT '0',
  `lcheck_in` int(11) DEFAULT NULL,
  `lcheck_out` int(11) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `lemail` varchar(200) DEFAULT NULL,
  `lphone` varchar(200) DEFAULT NULL,
  `lnote` tinytext,
  `lusername` varchar(200) DEFAULT NULL,
  `lstatus` smallint(2) DEFAULT NULL COMMENT '0:cancel, 1:check-availability;2: dp-waiting,3:full-pay waiting,4:confirm,5:uncofirm',
  `lbook_type` varchar(12) DEFAULT NULL,
  `lcountry` varchar(50) DEFAULT NULL,
  `lguest` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`lbook_id`,`larticle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_booking_detail`
--

CREATE TABLE IF NOT EXISTS `lumonata_booking_detail` (
  `lbook_id` int(11) NOT NULL DEFAULT '0',
  `ladditional_service` varchar(255) DEFAULT NULL,
  `ladditional_cost` tinyint(4) DEFAULT NULL,
  `lheard_about_us` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lbook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_booking_program`
--

CREATE TABLE IF NOT EXISTS `lumonata_booking_program` (
  `lbook_id` int(11) DEFAULT NULL,
  `lpp_id` bigint(20) DEFAULT NULL,
  `loccupancy_type` char(1) DEFAULT NULL,
  `lvilla_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_comments`
--

CREATE TABLE IF NOT EXISTS `lumonata_comments` (
  `lcomment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lcomment_parent` bigint(20) NOT NULL,
  `larticle_id` bigint(20) NOT NULL,
  `lcomentator_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomentator_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_ip` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomment_date` datetime NOT NULL,
  `lcomment` text CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_like` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'like,comment,like_comment',
  PRIMARY KEY (`lcomment_id`),
  KEY `lcomment_status` (`lcomment_status`),
  KEY `lcomment_userid` (`luser_id`),
  KEY `lcomment_type` (`lcomment_type`),
  KEY `larticle_id` (`larticle_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `lumonata_comments`
--

INSERT INTO `lumonata_comments` (`lcomment_id`, `lcomment_parent`, `larticle_id`, `lcomentator_name`, `lcomentator_email`, `lcomentator_url`, `lcomentator_ip`, `lcomment_date`, `lcomment`, `lcomment_status`, `lcomment_like`, `luser_id`, `lcomment_type`) VALUES
(1, 0, 3, 'Wahya Biantara', 'request@arunna.com', 'http://localhost/arunna-repo/?user=admin', '127.0.0.1', '2011-03-18 21:52:25', 'Hello comment....', 'approved', 0, 1, 'comment'),
(2, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://10.10.10.19/aquashine/?user=admin', '10.10.10.19', '2012-05-08 12:49:41', '', 'approved', 0, 1, 'comment'),
(3, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://10.10.10.19/aquashine/?user=admin', '10.10.10.19', '2012-05-08 12:49:43', '', 'approved', 0, 1, 'comment'),
(4, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://10.10.10.19/aquashine/?user=admin', '10.10.10.19', '2012-05-08 12:49:45', '', 'approved', 0, 1, 'comment'),
(7, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://10.10.10.19/aquashine/?user=admin', '10.10.10.19', '2012-05-08 12:52:17', 'like_post_3', 'approved', 0, 1, 'like');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_contact_us`
--

CREATE TABLE IF NOT EXISTS `lumonata_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `message` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `lumonata_contact_us`
--

INSERT INTO `lumonata_contact_us` (`id`, `name`, `email`, `message`) VALUES
(6, 'widia', 'widia@lumonata.com', 'testestestest'),
(5, 'widia', 'widia@lumonata.com', 'testestestest'),
(7, 'Input your name here', 'And email address please', 'Send us a couple of words'),
(8, 'Input your name here', 'And email address please', 'Send us a couple of words');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_country`
--

CREATE TABLE IF NOT EXISTS `lumonata_country` (
  `lcountry_id` int(11) NOT NULL AUTO_INCREMENT,
  `lcountry` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lpublish` enum('0','1') COLLATE latin1_general_ci NOT NULL DEFAULT '1',
  `lshow_group` enum('0','1') COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lcountry_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=248 ;

--
-- Dumping data for table `lumonata_country`
--

INSERT INTO `lumonata_country` (`lcountry_id`, `lcountry`, `lorder_id`, `lpublish`, `lshow_group`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(1, 'Ã…land Islands', 5, '1', '0', 'admin', 1241502068, '', 0),
(2, 'Afghanistan', 5, '1', '0', 'admin', 1241502081, '', 0),
(3, 'Albania', 5, '1', '0', 'admin', 1241502086, '', 0),
(4, 'Algeria', 5, '1', '0', 'admin', 1241502092, '', 0),
(5, 'American Samoa', 5, '1', '0', 'admin', 1241502100, '', 0),
(6, 'Andorra', 5, '1', '0', 'admin', 1241502105, '', 0),
(7, 'Angola', 5, '1', '0', 'admin', 1241502114, '', 0),
(8, 'Anguilla', 5, '1', '0', 'admin', 1241502121, '', 0),
(9, 'Antarctica', 5, '1', '0', 'admin', 1241502127, '', 0),
(10, 'Antigua And Barbuda', 5, '1', '0', 'admin', 1241502135, '', 0),
(11, 'Argentina', 5, '1', '0', 'admin', 1241502142, '', 0),
(12, 'Armenia', 5, '1', '0', 'admin', 1241502149, '', 0),
(13, 'Aruba', 5, '1', '0', 'admin', 1241502162, '', 0),
(14, 'Australia', 3, '1', '1', 'admin', 1241502166, '', 0),
(15, 'Austria', 5, '1', '0', 'admin', 1241502174, '', 0),
(16, 'Azerbaijan', 5, '1', '0', 'admin', 1241502182, '', 0),
(17, 'Bahamas', 5, '1', '0', 'admin', 1241502191, '', 0),
(18, 'Bahrain', 5, '1', '0', 'admin', 1241502197, '', 0),
(19, 'Bangladesh', 5, '1', '0', 'admin', 1241502203, '', 0),
(20, 'Barbados', 5, '1', '0', 'admin', 1241502209, '', 0),
(21, 'Belarus', 5, '1', '0', 'admin', 1241502216, '', 0),
(22, 'Belgium', 5, '1', '0', 'admin', 1241502222, '', 0),
(23, 'Belize', 5, '1', '0', 'admin', 1241502228, '', 0),
(24, 'Benin', 5, '1', '0', 'admin', 1241502233, '', 0),
(25, 'Bermuda', 5, '1', '0', 'admin', 1241502239, '', 0),
(26, 'Bhutan', 5, '1', '0', 'admin', 1241502247, '', 0),
(27, 'Bolivia', 5, '1', '0', 'admin', 1241502253, '', 0),
(28, 'Bosnia and Herzegovina', 5, '1', '0', 'admin', 1241502263, '', 0),
(29, 'Botswana', 5, '1', '0', 'admin', 1241502272, '', 0),
(30, 'Bouvet Island', 5, '1', '0', 'admin', 1241502281, '', 0),
(31, 'Brazil', 5, '1', '0', 'admin', 1241502289, '', 0),
(32, 'British Indian Ocean Territory', 5, '1', '0', 'admin', 1241502300, '', 0),
(33, 'Brunei', 5, '1', '0', 'admin', 1241502306, '', 0),
(34, 'Bulgaria', 5, '1', '0', 'admin', 1241502313, '', 0),
(35, 'Burkina Faso', 5, '1', '0', 'admin', 1241502321, '', 0),
(36, 'Burundi', 5, '1', '0', 'admin', 1241502329, '', 0),
(37, 'Cambodia', 5, '1', '0', 'admin', 1241502335, '', 0),
(38, 'Cameroon', 5, '1', '0', 'admin', 1241502341, '', 0),
(39, 'Canada', 2, '1', '1', 'admin', 1241502347, '', 0),
(40, 'Cape Verde', 5, '1', '0', 'admin', 1241502355, '', 0),
(41, 'Cayman Islands', 5, '1', '0', 'admin', 1241502363, '', 0),
(42, 'Central African Republic', 5, '1', '0', 'admin', 1241502371, '', 0),
(43, 'Chad', 5, '1', '0', 'admin', 1241502377, '', 0),
(44, 'Chile', 5, '1', '0', 'admin', 1241502384, '', 0),
(45, 'China', 5, '1', '0', 'admin', 1241502390, '', 0),
(46, 'Christmas Island', 5, '1', '0', 'admin', 1241502398, '', 0),
(47, 'Cocos (Keeling) Islands', 5, '1', '0', 'admin', 1241502411, '', 0),
(48, 'Colombia', 5, '1', '0', 'admin', 1241502465, '', 0),
(49, 'Comoros', 5, '1', '0', 'admin', 1241502499, '', 0),
(50, 'Congo', 5, '1', '0', 'admin', 1241502505, '', 0),
(51, 'Congo, Democractic Republic', 5, '1', '0', 'admin', 1241502513, '', 0),
(52, 'Cook Islands', 5, '1', '0', 'admin', 1241502521, '', 0),
(53, 'Costa Rica', 5, '1', '0', 'admin', 1241502531, '', 0),
(54, 'Cote D\\''Ivoire (Ivory Coast)', 5, '1', '0', 'admin', 1241502550, '', 0),
(55, 'Croatia (Hrvatska)', 5, '1', '0', 'admin', 1241502560, '', 0),
(56, 'Cuba', 5, '1', '0', 'admin', 1241502567, '', 0),
(57, 'Cyprus', 5, '1', '0', 'admin', 1241502573, '', 0),
(58, 'Czech Republic', 5, '1', '0', 'admin', 1241502581, '', 0),
(59, 'Denmark', 5, '1', '0', 'admin', 1241502590, '', 0),
(60, 'Djibouti', 5, '1', '0', 'admin', 1241502596, '', 0),
(61, 'Dominica', 5, '1', '0', 'admin', 1241502602, '', 0),
(62, 'Dominican Republic', 5, '1', '0', 'admin', 1241502610, '', 0),
(63, 'East Timor', 5, '1', '0', 'admin', 1241502619, '', 0),
(64, 'Ecuador', 5, '1', '0', 'admin', 1241502625, '', 0),
(65, 'Egypt', 5, '1', '0', 'admin', 1241502635, '', 0),
(66, 'El Salvador', 5, '1', '0', 'admin', 1241502644, '', 0),
(67, 'Equatorial Guinea', 5, '1', '0', 'admin', 1241502653, '', 0),
(68, 'Eritrea', 5, '1', '0', 'admin', 1241502659, '', 0),
(69, 'Estonia', 5, '1', '0', 'admin', 1241502665, '', 0),
(70, 'Ethiopia', 5, '1', '0', 'admin', 1241502673, '', 0),
(71, 'Falkland Islands (Islas Malvinas)', 5, '1', '0', 'admin', 1241502687, '', 0),
(72, 'Faroe Islands', 5, '1', '0', 'admin', 1241502696, '', 0),
(73, 'Fiji Islands', 5, '1', '0', 'admin', 1241502704, '', 0),
(74, 'Finland', 5, '1', '0', 'admin', 1241502710, '', 0),
(75, 'France', 5, '1', '0', 'admin', 1241502716, '', 0),
(76, 'France, Metropolitan', 5, '1', '0', 'admin', 1241502741, '', 0),
(77, 'French Guiana', 5, '1', '0', 'admin', 1241502745, '', 0),
(78, 'French Polynesia', 5, '1', '0', 'admin', 1241502754, '', 0),
(79, 'French Southern Territories', 5, '1', '0', 'admin', 1241502762, '', 0),
(80, 'Gabon', 5, '1', '0', 'admin', 1241502768, '', 0),
(81, 'Gambia, The', 5, '1', '0', 'admin', 1241502776, '', 0),
(82, 'Georgia', 5, '1', '0', 'admin', 1241502782, '', 0),
(83, 'Germany', 5, '1', '0', 'admin', 1241502789, '', 0),
(84, 'Ghana', 5, '1', '0', 'admin', 1241502799, '', 0),
(85, 'Gibraltar', 5, '1', '0', 'admin', 1241502812, '', 0),
(86, 'Greece', 5, '1', '0', 'admin', 1241502819, '', 0),
(87, 'Greenland', 5, '1', '0', 'admin', 1241502915, '', 0),
(88, 'Grenada', 5, '1', '0', 'admin', 1241502920, '', 0),
(89, 'Guadeloupe', 5, '1', '0', 'admin', 1241502926, '', 0),
(90, 'Guam', 5, '1', '0', 'admin', 1241502931, '', 0),
(91, 'Guatemala', 5, '1', '0', 'admin', 1241502937, '', 0),
(92, 'Guernsey', 5, '1', '0', 'admin', 1241502943, '', 0),
(93, 'Guinea', 5, '1', '0', 'admin', 1241502950, '', 0),
(94, 'Guinea-Bissau', 5, '1', '0', 'admin', 1241502958, '', 0),
(95, 'Guyana', 5, '1', '0', 'admin', 1241502965, '', 0),
(96, 'Haiti', 5, '1', '0', 'admin', 1241502972, '', 0),
(97, 'Heard and McDonald Islands', 5, '1', '0', 'admin', 1241503013, '', 0),
(98, 'Honduras', 5, '1', '0', 'admin', 1241503022, '', 0),
(99, 'Hong Kong S.A.R.', 5, '1', '0', 'admin', 1241503033, '', 0),
(100, 'Hungary', 5, '1', '0', 'admin', 1241503038, '', 0),
(101, 'Iceland', 5, '1', '0', 'admin', 1241503043, '', 0),
(102, 'India', 5, '1', '0', 'admin', 1241503048, '', 0),
(103, 'Indonesia', 5, '1', '0', 'admin', 1241503054, '', 0),
(104, 'Iran', 5, '1', '0', 'admin', 1241503060, '', 0),
(105, 'Iraq', 5, '1', '0', 'admin', 1241503065, '', 0),
(106, 'Ireland', 5, '1', '0', 'admin', 1241503071, '', 0),
(107, 'Isle of Man', 5, '1', '0', 'admin', 1241503078, '', 0),
(108, 'Israel', 5, '1', '0', 'admin', 1241503084, '', 0),
(109, 'Italy', 5, '1', '0', 'admin', 1241503090, '', 0),
(110, 'Jamaica', 5, '1', '0', 'admin', 1241503097, '', 0),
(111, 'Japan', 5, '1', '0', 'admin', 1241503106, '', 0),
(112, 'Jersey', 5, '1', '0', 'admin', 1241503114, '', 0),
(113, 'Jordan', 5, '1', '0', 'admin', 1241503128, '', 0),
(114, 'Kazakhstan', 5, '1', '0', 'admin', 1241503132, '', 0),
(115, 'Kenya', 5, '1', '0', 'admin', 1241503144, '', 0),
(116, 'Kiribati', 5, '1', '0', 'admin', 1241503151, '', 0),
(117, 'Korea', 5, '1', '0', 'admin', 1241503159, '', 0),
(118, 'Korea, North', 5, '1', '0', 'admin', 1241503166, '', 0),
(119, 'Kuwait', 5, '1', '0', 'admin', 1241503172, '', 0),
(120, 'Kyrgyzstan', 5, '1', '0', 'admin', 1241503178, '', 0),
(121, 'Laos', 5, '1', '0', 'admin', 1241503183, '', 0),
(122, 'Latvia', 5, '1', '0', 'admin', 1241503192, '', 0),
(123, 'Lebanon', 5, '1', '0', 'admin', 1241503197, '', 0),
(124, 'Lesotho', 5, '1', '0', 'admin', 1241503203, '', 0),
(125, 'Liberia', 5, '1', '0', 'admin', 1241503210, '', 0),
(126, 'Libya', 5, '1', '0', 'admin', 1241503217, '', 0),
(127, 'Liechtenstein', 5, '1', '0', 'admin', 1241503224, '', 0),
(128, 'Lithuania', 5, '1', '0', 'admin', 1241503229, '', 0),
(129, 'Luxembourg', 5, '1', '0', 'admin', 1241503235, '', 0),
(130, 'Macau S.A.R.', 5, '1', '0', 'admin', 1241503243, '', 0),
(131, 'Macedonia', 5, '1', '0', 'admin', 1241503251, '', 0),
(132, 'Madagascar', 5, '1', '0', 'admin', 1241503256, '', 0),
(133, 'Malawi', 5, '1', '0', 'admin', 1241503263, '', 0),
(134, 'Malaysia', 5, '1', '0', 'admin', 1241503274, '', 0),
(135, 'Maldives', 5, '1', '0', 'admin', 1241503280, '', 0),
(136, 'Mali', 5, '1', '0', 'admin', 1241503286, '', 0),
(137, 'Malta', 5, '1', '0', 'admin', 1241503292, '', 0),
(138, 'Marshall Islands', 5, '1', '0', 'admin', 1241503303, '', 0),
(139, 'Martinique', 5, '1', '0', 'admin', 1241503314, '', 0),
(140, 'Mauritania', 5, '1', '0', 'admin', 1241503321, '', 0),
(141, 'Mauritius', 5, '1', '0', 'admin', 1241503330, '', 0),
(142, 'Mayotte', 5, '1', '0', 'admin', 1241503337, '', 0),
(143, 'Mexico', 5, '1', '0', 'admin', 1241503343, '', 0),
(144, 'Micronesia', 5, '1', '0', 'admin', 1241503350, '', 0),
(145, 'Moldova', 5, '1', '0', 'admin', 1241503359, '', 0),
(146, 'Monaco', 5, '1', '0', 'admin', 1241503367, '', 0),
(147, 'Mongolia', 5, '1', '0', 'admin', 1241503374, '', 0),
(148, 'Montenegro', 5, '1', '0', 'admin', 1241503387, '', 0),
(149, 'Montserrat', 5, '1', '0', 'admin', 1241503395, '', 0),
(150, 'Morocco', 5, '1', '0', 'admin', 1241503419, '', 0),
(151, 'Mozambique', 5, '1', '0', 'admin', 1241503429, '', 0),
(152, 'Myanmar', 5, '1', '0', 'admin', 1241503437, '', 0),
(153, 'Namibia', 5, '1', '0', 'admin', 1241503444, '', 0),
(154, 'Nauru', 5, '1', '0', 'admin', 1241503451, '', 0),
(155, 'Nepal', 5, '1', '0', 'admin', 1241503459, '', 0),
(156, 'Netherlands', 5, '1', '0', 'admin', 1241503466, '', 0),
(157, 'Netherlands Antilles', 5, '1', '0', 'admin', 1241503477, '', 0),
(158, 'New Caledonia', 5, '1', '0', 'admin', 1241503486, '', 0),
(159, 'New Zealand', 5, '1', '0', 'admin', 1241503503, '', 0),
(160, 'Nicaragua', 5, '1', '0', 'admin', 1241503508, '', 0),
(161, 'Niger', 5, '1', '0', 'admin', 1241503517, '', 0),
(162, 'Nigeria', 5, '1', '0', 'admin', 1241503530, '', 0),
(163, 'Niue', 5, '1', '0', 'admin', 1241503543, '', 0),
(164, 'Norfolk Island', 5, '1', '0', 'admin', 1241503552, '', 0),
(165, 'Northern Mariana Islands', 5, '1', '0', 'admin', 1241503561, '', 0),
(166, 'Norway', 5, '1', '0', 'admin', 1241503566, '', 0),
(167, 'Oman', 5, '1', '0', 'admin', 1241503572, '', 0),
(168, 'Pakistan', 5, '1', '0', 'admin', 1241503625, '', 0),
(169, 'Palau', 5, '1', '0', 'admin', 1241503630, '', 0),
(170, 'Palestinian Territory, Occupied', 5, '1', '0', 'admin', 1241503639, '', 0),
(171, 'Panama', 5, '1', '0', 'admin', 1241503647, '', 0),
(172, 'Papua new Guinea', 5, '1', '0', 'admin', 1241503655, '', 0),
(173, 'Paraguay', 5, '1', '0', 'admin', 1241503661, '', 0),
(174, 'Peru', 5, '1', '0', 'admin', 1241503695, '', 0),
(175, 'Philippines', 5, '1', '0', 'admin', 1241503702, '', 0),
(176, 'Pitcairn Island', 5, '1', '0', 'admin', 1241503712, '', 0),
(177, 'Poland', 5, '1', '0', 'admin', 1241503717, '', 0),
(178, 'Portugal', 5, '1', '0', 'admin', 1241503725, '', 0),
(179, 'Puerto Rico', 5, '1', '0', 'admin', 1241503758, '', 0),
(180, 'Qatar', 5, '1', '0', 'admin', 1241503765, '', 0),
(181, 'Reunion', 5, '1', '0', 'admin', 1241503775, '', 0),
(182, 'Romania', 5, '1', '0', 'admin', 1241503780, '', 0),
(183, 'Russia', 5, '1', '0', 'admin', 1241503786, '', 0),
(184, 'Rwanda', 5, '1', '0', 'admin', 1241503792, '', 0),
(185, 'Saint Helena', 5, '1', '0', 'admin', 1241503799, '', 0),
(186, 'Saint Kitts And Nevis', 5, '1', '0', 'admin', 1241503807, '', 0),
(187, 'Saint Lucia', 5, '1', '0', 'admin', 1241503814, '', 0),
(188, 'Saint Pierre and Miquelon', 5, '1', '0', 'admin', 1241503822, '', 0),
(189, 'Saint Vincent And The Grenadines', 5, '1', '0', 'admin', 1241503830, '', 0),
(190, 'Samoa', 5, '1', '0', 'admin', 1241503835, '', 0),
(191, 'San Marino', 5, '1', '0', 'admin', 1241503844, '', 0),
(192, 'Sao Tome and Principe', 5, '1', '0', 'admin', 1241503851, '', 0),
(193, 'Saudi Arabia', 5, '1', '0', 'admin', 1241503858, '', 0),
(194, 'Senegal', 5, '1', '0', 'admin', 1241503864, '', 0),
(195, 'Serbia', 5, '1', '0', 'admin', 1241503868, '', 0),
(196, 'Seychelles', 5, '1', '0', 'admin', 1241503873, '', 0),
(197, 'Sierra Leone', 5, '1', '0', 'admin', 1241503881, '', 0),
(198, 'Singapore', 5, '1', '0', 'admin', 1241503886, '', 0),
(199, 'Slovakia', 5, '1', '0', 'admin', 1241503892, '', 0),
(200, 'Slovenia', 5, '1', '0', 'admin', 1241503898, '', 0),
(201, 'Solomon Islands', 5, '1', '0', 'admin', 1241503905, '', 0),
(202, 'Somalia', 5, '1', '0', 'admin', 1241503914, '', 0),
(203, 'South Africa', 5, '1', '0', 'admin', 1241503924, '', 0),
(204, 'South Georgia And The South Sandwich Islands', 5, '1', '0', 'admin', 1241503932, '', 0),
(205, 'Spain', 5, '1', '0', 'admin', 1241503939, '', 0),
(206, 'Sri Lanka', 5, '1', '0', 'admin', 1241503946, '', 0),
(207, 'Sudan', 5, '1', '0', 'admin', 1241503951, '', 0),
(208, 'Suriname', 5, '1', '0', 'admin', 1241503957, '', 0),
(209, 'Svalbard And Jan Mayen Islands', 5, '1', '0', 'admin', 1241503965, '', 0),
(210, 'Swaziland', 5, '1', '0', 'admin', 1241503972, '', 0),
(211, 'Sweden', 5, '1', '0', 'admin', 1241503977, '', 0),
(212, 'Switzerland', 5, '1', '0', 'admin', 1241503985, '', 0),
(213, 'Syria', 5, '1', '0', 'admin', 1241504000, '', 0),
(214, 'Taiwan', 5, '1', '0', 'admin', 1241504006, '', 0),
(215, 'Tajikistan', 5, '1', '0', 'admin', 1241504011, '', 0),
(216, 'Tanzania', 5, '1', '0', 'admin', 1241504018, '', 0),
(217, 'Thailand', 5, '1', '0', 'admin', 1241504026, '', 0),
(218, 'Timor-Leste', 5, '1', '0', 'admin', 1241504036, '', 0),
(219, 'Togo', 5, '1', '0', 'admin', 1241504042, '', 0),
(220, 'Tokelau', 5, '1', '0', 'admin', 1241504056, '', 0),
(221, 'Tonga', 5, '1', '0', 'admin', 1241504143, '', 0),
(222, 'Trinidad And Tobago', 5, '1', '0', 'admin', 1241504153, '', 0),
(223, 'Tunisia', 5, '1', '0', 'admin', 1241504158, '', 0),
(224, 'Turkey', 5, '1', '0', 'admin', 1241504163, '', 0),
(225, 'Turkmenistan', 5, '1', '0', 'admin', 1241504169, '', 0),
(226, 'Turks And Caicos Islands', 5, '1', '0', 'admin', 1241504177, '', 0),
(227, 'Tuvalu', 5, '1', '0', 'admin', 1241504183, '', 0),
(228, 'Uganda', 5, '1', '0', 'admin', 1241504189, '', 0),
(229, 'Ukraine', 5, '1', '0', 'admin', 1241504200, '', 0),
(230, 'United Arab Emirates', 5, '1', '0', 'admin', 1241504208, '', 0),
(231, 'United Kingdom', 4, '1', '1', 'admin', 1241504217, '', 0),
(232, 'United States', 1, '1', '1', 'admin', 1241504224, '', 0),
(233, 'United States Minor Outlying Islands', 5, '1', '0', 'admin', 1241504231, '', 0),
(234, 'Uruguay', 5, '1', '0', 'admin', 1241504237, '', 0),
(235, 'Uzbekistan', 5, '1', '0', 'admin', 1241504244, '', 0),
(236, 'Vanuatu', 5, '1', '0', 'admin', 1241504249, '', 0),
(237, 'Vatican City State (Holy See)', 5, '1', '0', 'admin', 1241504257, '', 0),
(238, 'Venezuela', 5, '1', '0', 'admin', 1241504262, '', 0),
(239, 'Vietnam', 5, '1', '0', 'admin', 1241504269, '', 0),
(240, 'Virgin Islands (British)', 5, '1', '0', 'admin', 1241504278, '', 0),
(241, 'Virgin Islands (US)', 5, '1', '0', 'admin', 1241504286, '', 0),
(242, 'WESTERN SAHARA', 5, '1', '0', 'admin', 1241504294, '', 0),
(243, 'Wallis And Futuna Islands', 5, '1', '0', 'admin', 1241504303, '', 0),
(244, 'Yemen', 5, '1', '0', 'admin', 1241504310, '', 0),
(245, 'Zambia', 5, '1', '0', 'admin', 1241504317, '', 0),
(246, 'Zimbabwe', 5, '1', '0', 'admin', 1241504323, '', 0),
(247, 'Rest of the world', 5, '1', '0', 'admin', 1241502080, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_currency`
--

CREATE TABLE IF NOT EXISTS `lumonata_currency` (
  `lcurrency_id` int(11) NOT NULL AUTO_INCREMENT,
  `lcode` varchar(25) NOT NULL,
  `lsymbol` varchar(50) NOT NULL,
  `lcurrency` varchar(255) NOT NULL,
  `lamount` decimal(10,5) NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lcurrency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `lumonata_currency`
--

INSERT INTO `lumonata_currency` (`lcurrency_id`, `lcode`, `lsymbol`, `lcurrency`, `lamount`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(8, 'AUD', '$', 'Australia Dollars', '0.00000', 56, 'admin', 1289791160, 'admin', 1305882382),
(7, 'EUR', 'â‚¬', 'Euro', '0.00000', 5, 'admin', 1289791092, 'admin', 1305882229),
(6, 'USD', '$', 'United States Dollar', '0.00000', 51, 'admin', 1289791060, 'admin', 1307754273),
(9, 'IDR', 'Rp', 'Indonesian Rupiah', '0.00000', 10, 'admin', 1289791212, 'admin', 1305882274),
(11, 'GBP', 'Â£', 'Pound Sterling', '0.00000', 1, 'admin', 1301300965, '', 0),
(12, 'CZK', 'KÄ', 'Koruna ÄeskÃ¡', '0.00000', 15, 'admin', 1305882631, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_friendship`
--

CREATE TABLE IF NOT EXISTS `lumonata_friendship` (
  `lfriendship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY (`lfriendship_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_friends_list`
--

CREATE TABLE IF NOT EXISTS `lumonata_friends_list` (
  `lfriends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriends_list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lumonata_friends_list`
--

INSERT INTO `lumonata_friends_list` (`lfriends_list_id`, `luser_id`, `llist_name`, `lorder`) VALUES
(1, 1, 'Work', 1),
(2, 1, 'School', 2),
(3, 1, 'Familiy', 3);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_friends_list_rel`
--

CREATE TABLE IF NOT EXISTS `lumonata_friends_list_rel` (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriendship_id`,`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_languages`
--

CREATE TABLE IF NOT EXISTS `lumonata_languages` (
  `llanguage_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `llanguage_title` text COLLATE latin1_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` datetime NOT NULL,
  PRIMARY KEY (`llanguage_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `lumonata_languages`
--

INSERT INTO `lumonata_languages` (`llanguage_id`, `llanguage_title`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`) VALUES
(1, 'English', 1, 0, '2011-10-14 14:41:06', '0000-00-00 00:00:00'),
(2, 'Dutch', 2, 0, '2011-10-14 14:41:22', '0000-00-00 00:00:00'),
(3, 'Spanish', 3, 0, '2011-10-14 14:42:21', '0000-00-00 00:00:00'),
(4, 'Franc', 4, 0, '2011-10-14 14:42:28', '0000-00-00 00:00:00'),
(5, 'Italian', 5, 0, '2011-10-14 14:43:01', '0000-00-00 00:00:00'),
(6, 'Turkish', 6, 0, '2011-10-14 14:43:31', '0000-00-00 00:00:00'),
(7, 'Swedish', 7, 0, '2011-10-14 14:44:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_members`
--

CREATE TABLE IF NOT EXISTS `lumonata_members` (
  `lmember_id` varchar(25) COLLATE latin1_general_ci NOT NULL COMMENT '201=Clients Mongkiki, 202=Client Lumonata,203=Other',
  `lfname` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `llname` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lemail` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `lphone` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lphone2` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcountry_id` int(11) NOT NULL,
  `lregion` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `lcity` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `laddress` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `laddress2` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `laddress3` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lpostal_code` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `lpassword` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `lpass` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lactv_code` text COLLATE latin1_general_ci NOT NULL,
  `laccount_status` int(1) NOT NULL DEFAULT '0' COMMENT '1=active;2=awaiting verification;',
  `llast_login` int(11) NOT NULL,
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`lmember_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lumonata_members`
--

INSERT INTO `lumonata_members` (`lmember_id`, `lfname`, `llname`, `lemail`, `lphone`, `lphone2`, `lcountry_id`, `lregion`, `lcity`, `laddress`, `laddress2`, `laddress3`, `lpostal_code`, `lpassword`, `lpass`, `lactv_code`, `laccount_status`, `llast_login`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES
('MI121000008', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '7kmezzvb', '504918dbe4c36d99a9a33ea2e39fcd9b', '64e80c0274e528320b9bfc155a1bac2e', 2, 0, 'MI121000008', '2012-10-30 18:05:23', 'MI121000008', '2012-10-30 18:05:23', 0),
('MI121000007', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'e6qh5e2u', 'dfd7bdb955096369d8e1ac29a4462e8e', '0f2ab693428ca71ee83721082459b6cf', 2, 0, 'MI121000007', '2012-10-30 18:05:23', 'MI121000007', '2012-10-30 18:05:23', 0),
('MI121000006', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'zevs653r', '26a8495e8a34c9648892b239dfaea374', '102f47a2c453be32d454512aa60e5826', 2, 0, 'MI121000006', '2012-10-30 18:05:21', 'MI121000006', '2012-10-30 18:05:21', 0),
('MI121000005', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'qaidpedp', '66906e85da4af0239933f5ad98361583', '43d265c63fc26b32eeead2f92ae22125', 2, 0, 'MI121000005', '2012-10-30 18:05:21', 'MI121000005', '2012-10-30 18:05:21', 0),
('MI121000004', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '7qunmec5', '8942ff1083e64b63047f3116f90508e5', 'a694d475a87e9e2054c843478558d6ee', 2, 0, 'MI121000004', '2012-10-30 18:05:19', 'MI121000004', '2012-10-30 18:05:19', 0),
('MI121000003', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'yr8b8ftd', 'ba3ccc81c87806a9d8098433916b2525', '7aea8a8a39b146c253366b4446925a0e', 2, 0, 'MI121000003', '2012-10-30 18:05:13', 'MI121000003', '2012-10-30 18:05:13', 0),
('MI121000002', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'ofrzypxq', 'd87e6af41f1746f666653adc4a988fb2', 'b71498b0674aed0f96e7322171275ae0', 2, 0, 'MI121000002', '2012-10-30 18:05:04', 'MI121000002', '2012-10-30 18:05:04', 0),
('MI121000001', 'Dana', 'Asmara', 'dana@lumonata.com', '081916248002', '', 103, 'Bali', 'Denpasar', 'Jl. Sedap malam', '', '', '80361', '499a7dc58a4fbcafdbc4a9bcd93363ab', '499a7dc58a4fbcafdbc4a9bcd93363ab', '4b3f3ee4cacf368c39f1801cd0434c24', 1, 0, 'MI121000001', '2012-10-30 17:39:35', 'MI121000001', '2012-10-30 17:39:35', 0),
('MI121000009', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 't5gg2xna', '029ce8af1536aac8f874685b337609b3', '90f525a94e86305f60ee1e1602b2d840', 2, 0, 'MI121000009', '2012-10-30 18:05:45', 'MI121000009', '2012-10-30 18:05:45', 0),
('MI121000010', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'tsxzbbgk', '282bdd99ab17325e7531a56221b5c4bb', '6d70aa8b186133a1793162eacb0c2d45', 2, 0, 'MI121000010', '2012-10-30 18:05:47', 'MI121000010', '2012-10-30 18:05:47', 0),
('MI121000011', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'eziij3iw', 'dca74afdf2d4922d2a6ae953c9728dac', 'f87ac75ff2c1067c81b2105faf8cd8f9', 2, 0, 'MI121000011', '2012-10-30 18:05:50', 'MI121000011', '2012-10-30 18:05:50', 0),
('MI121000012', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '3u3ep8t6', 'da3cc4c12906e8e51706347529c617ef', '220b9c1167b9e5847818a797345f2b3c', 2, 0, 'MI121000012', '2012-10-30 18:05:51', 'MI121000012', '2012-10-30 18:05:51', 0),
('MI121000013', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'rx5g6m0q', '7b872c02652d39ecd01d774ad01d4f2e', '876c7718930c07a006a0571542fc7905', 2, 0, 'MI121000013', '2012-10-30 18:05:53', 'MI121000013', '2012-10-30 18:05:53', 0),
('MI121000014', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '4dsxfa6x', '4c024b940282bbcff78e14123e4cb7e0', '319d3e3c172c6aa7d53056b26dd0e38d', 2, 0, 'MI121000014', '2012-10-30 18:05:53', 'MI121000014', '2012-10-30 18:05:53', 0),
('MI121000015', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'zdkyvozo', 'af16991d3c01cef756c7ce334a319c9e', '57f7972723a1eed84d126f6bafdc0923', 2, 0, 'MI121000015', '2012-10-30 18:05:53', 'MI121000015', '2012-10-30 18:05:53', 0),
('MI121000016', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'x22bc5fh', 'c0d739725b5b5ff12873507e94ddbb0b', '01ac3d7b2b1436917a6e1368484e9ac1', 2, 0, 'MI121000016', '2012-10-30 18:05:54', 'MI121000016', '2012-10-30 18:05:54', 0),
('MI121000017', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '2hipppjz', '10049913297057cb3dc8adb315b6c307', '58b04ce60bacaf937714cccd195d6693', 2, 0, 'MI121000017', '2012-10-30 18:05:54', 'MI121000017', '2012-10-30 18:05:54', 0),
('MI121000018', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '52x28a6z', '3806ffbff914fed3cf17d2b951748b15', 'bf8b6a2eda7f0e8ec120a532b9db3197', 2, 0, 'MI121000018', '2012-10-30 18:05:55', 'MI121000018', '2012-10-30 18:05:55', 0),
('MI121000019', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'kmxnaof3', '0e3cac9940244eac79faaab7fa03cb81', 'be3d153cb8646a27f950809f89108fe1', 2, 0, 'MI121000019', '2012-10-30 18:05:55', 'MI121000019', '2012-10-30 18:05:55', 0),
('MI121000020', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'tfe0q0m8', '8ac0687b7fb19e0ed622b7d3671d0f81', '988c2fc3b85b25885dc4b3b31af4304d', 2, 0, 'MI121000020', '2012-10-30 18:05:56', 'MI121000020', '2012-10-30 18:05:56', 0),
('MI121000021', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'svn5y7vx', '52137fd55ede61496fc63edf20d6a907', '528fc199bc66c359ee215367f99a2bfe', 2, 0, 'MI121000021', '2012-10-30 18:05:56', 'MI121000021', '2012-10-30 18:05:56', 0),
('MI121000022', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'g0885t6w', '0065e2ec0e0188494b60023cba8cb9ce', 'ca64fa21e3c621b2a9bfcdd78b81bde8', 2, 0, 'MI121000022', '2012-10-30 18:05:56', 'MI121000022', '2012-10-30 18:05:56', 0),
('MI121000023', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '2v5xoxao', '1589e4e28764153d51554f3cbf61e369', '1daa4688274247d6fecb697b30301758', 2, 0, 'MI121000023', '2012-10-30 18:05:57', 'MI121000023', '2012-10-30 18:05:57', 0),
('MI121000024', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'oh65dc8c', 'b2e50b8ce03456181dd8f66e9b933ae6', '6bf01035c2ecf52bdc77d3288657174a', 2, 0, 'MI121000024', '2012-10-30 18:05:57', 'MI121000024', '2012-10-30 18:05:57', 0),
('MI121000025', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'g6d3svrh', 'e132e114b0ab0ef6f5de947d6bf8f366', '850128530e997e15681137dc88a85255', 2, 0, 'MI121000025', '2012-10-30 18:05:57', 'MI121000025', '2012-10-30 18:05:57', 0),
('MI121000026', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'kau3bmpw', 'a59ffd80a028eb7dea18f4b2b01b576c', '8b51d33e7d433dc0bf301e87ae1bca0c', 2, 0, 'MI121000026', '2012-10-30 18:09:11', 'MI121000026', '2012-10-30 18:09:11', 0),
('MI121000027', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'kboajie3', 'e0d87cd34c32f319a274de31d1626b92', 'a8310636fcb8d7a0ce571e16d5eb554e', 2, 0, 'MI121000027', '2012-10-30 18:09:14', 'MI121000027', '2012-10-30 18:09:14', 0),
('MI121000028', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'vbjsen7s', '88dc6d2b7765b4c5aa981cc32fbc84ff', '4eeccf5622570403248782ec3b18b264', 2, 0, 'MI121000028', '2012-10-30 18:09:16', 'MI121000028', '2012-10-30 18:09:16', 0),
('MI121000029', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '3uowgsxk', '92418b33e9415b12223250e2d0a117ea', 'adecc28409a836e75d1b284dabdb9aa0', 2, 0, 'MI121000029', '2012-10-30 18:09:16', 'MI121000029', '2012-10-30 18:09:16', 0),
('MI121000030', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'yq3wgbjp', 'fc6a902f432ad3be09beb5c309326b47', '2c491a0f97992f5dccd195ec5ef68dbf', 2, 0, 'MI121000030', '2012-10-30 18:09:17', 'MI121000030', '2012-10-30 18:09:17', 0),
('MI121000031', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '43urjqsx', '8ae97317ef67ea4acb58180b5e8ab2f0', '03776904896e529b3072c72b4f11650a', 2, 0, 'MI121000031', '2012-10-30 18:09:18', 'MI121000031', '2012-10-30 18:09:18', 0),
('MI121000032', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'ok6daqje', '31b4ce5b1eeae57366c2e85ecf7d070a', 'c4a09364cfd009cf2f6554138ccf9653', 2, 0, 'MI121000032', '2012-10-30 18:09:19', 'MI121000032', '2012-10-30 18:09:19', 0),
('MI121000033', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'yjkvweee', '76b3ebf87991d75dc4fd5d0185ae80dd', 'd3db428f68a3d8d946031ffec22377ba', 2, 0, 'MI121000033', '2012-10-30 18:09:24', 'MI121000033', '2012-10-30 18:09:24', 0),
('MI121000034', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'ujcf2tti', 'c531508fc32e624ae7d59bb965756612', '3a67398fe2f7c5d232b87f3a6dd72392', 2, 0, 'MI121000034', '2012-10-30 18:09:24', 'MI121000034', '2012-10-30 18:09:24', 0),
('MI121000035', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '0etzi6rc', '39f68269a32c4be6bc5340aab6173fdc', '73878e2ea532a1921aa94d07346ff199', 2, 0, 'MI121000035', '2012-10-30 18:09:24', 'MI121000035', '2012-10-30 18:09:24', 0),
('MI121000036', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'iziosds3', '0abcf9f0e5873cbf4d34acc871ec01cf', '3c26f569fcd267967b285ea95fc8076f', 2, 0, 'MI121000036', '2012-10-30 18:09:24', 'MI121000036', '2012-10-30 18:09:24', 0),
('MI121000037', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '2aaov27d', 'c9b27a045b57d3d91a66ff9a485db689', '742a4c5f116bc8e9177b428445390e97', 2, 0, 'MI121000037', '2012-10-30 18:09:34', 'MI121000037', '2012-10-30 18:09:34', 0),
('MI121000038', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'd00dyo3a', 'e3836e9002af9653a50047149a51a6dd', '3c81abdd9e7faa06e2726d407e63d128', 2, 0, 'MI121000038', '2012-10-30 18:09:35', 'MI121000038', '2012-10-30 18:09:35', 0),
('MI121000039', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'o4o450vf', '04ad24c9a279d7a5c1d663d0390690db', '437afc18b34797412192efb2494663f7', 2, 0, 'MI121000039', '2012-10-30 18:09:35', 'MI121000039', '2012-10-30 18:09:35', 0),
('MI121000040', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'ta8k20p0', 'eebaf8ece2d32e8ff6045111055ecc6e', 'c488b1938af0bf119591f8a0ac8e135a', 2, 0, 'MI121000040', '2012-10-30 18:09:35', 'MI121000040', '2012-10-30 18:09:35', 0),
('MI121000041', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '3zv55gma', 'dc755f95726bc156be5af166f396eb44', 'bb49e2eaa07c939a7bff06c904431a7d', 2, 0, 'MI121000041', '2012-10-30 18:09:36', 'MI121000041', '2012-10-30 18:09:36', 0),
('MI121000042', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'ugvjn3rf', 'a80df781948e97cc4da0c2048a74fae0', '5b9754eab1d9c0ee2437fd2b8cea6928', 2, 0, 'MI121000042', '2012-10-30 18:09:36', 'MI121000042', '2012-10-30 18:09:36', 0),
('MI121000043', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'x2am55g7', 'daf35b16fb48c1d8733e5b25231fb602', '79a181bf39992c160d5143ea2fe844b4', 2, 0, 'MI121000043', '2012-10-30 18:09:36', 'MI121000043', '2012-10-30 18:09:36', 0),
('MI121000044', 'Wahya', 'Biantara', 'gwahya@gmail.com', '754926', '', 103, 'Bali', 'Bali', 'Jl Raya Kuta 127', '', '', '80361', 'sty0fxnu', '2148a0dbac1371c01e77fdce600151f3', 'ed33e8db85eb5e5c4b2f24022d510346', 2, 0, 'MI121000044', '2012-10-30 18:12:33', 'MI121000044', '2012-10-30 18:12:33', 0),
('MI121000045', 'Yana', 'Nice', 'wdanaasmara474@gmail.com', '081916248002', '', 103, 'Bali', 'Dps', 'Jl. jalanan', '', '', '80361', '2eg67yim', '6c9b908dbc8d8b3eb531a4f0b015ebdb', '495e698719656613ad77f847c887e49e', 2, 0, 'MI121000045', '2012-10-30 18:15:04', 'MI121000045', '2012-10-30 18:15:04', 0),
('MI121000046', 'Yana', 'Nice', 'wdanaasmara474@gmail.com', '081916248002', '', 103, 'Bali', 'Dps', 'Jl. Sedap malam', '', '', '80361', 's0agraqz', '39f82d5eb32c964e582af0f7a61fe6bf', 'e8eb7d464986969aced270f56b889a54', 2, 0, 'MI121000046', '2012-10-30 18:21:22', 'MI121000046', '2012-10-30 18:21:22', 0),
('MI121000047', 'Yana', 'Lumonata', 'wdanaasmara474@gmail.com', '081234567', '', 103, 'Bali', 'Dps', 'Lumonata', '', '', '80361', '4237d0of', '42c4d5dcc49f1a74e276c9f51907b057', 'f7ffe9b38dbbb49a3ec7fd1bcd46a85f', 2, 0, 'MI121000047', '2012-10-30 18:33:37', 'MI121000047', '2012-10-30 18:33:37', 0),
('MI121000048', 'Wahya', 'Biantara', 'gwahya@gmail.com', '754926', '', 103, 'Bali', 'Bali', 'Jl Raya Kuta 127', '', '', '80361', '78z7vyrn', '21491a40f5844ba3d1e517a0be2bd17c', 'c67d215c04be2bd85cd333f67245751d', 2, 0, 'MI121000048', '2012-10-30 18:56:17', 'MI121000048', '2012-10-30 18:56:17', 0),
('MI121000049', 'Wahya', 'Biantara', 'wahya@lumonata.com', '754926', '', 103, 'Bali', 'Kuta', 'Jl Raya Kuta 127', '', '', '80361', 'kqhqdvr6', '3c524e33e9cffa8fc35aca570fdd5965', '4d93321f98a4465b763dc3c34f142c18', 2, 0, 'MI121000049', '2012-10-30 18:57:34', 'MI121000049', '2012-10-30 18:57:34', 0),
('MI121000050', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'rd6z03z8', 'd760b09fd427ade6c55cefaf9af9049c', '0777e3906d4fe666c07e3107617478cb', 2, 0, 'MI121000050', '2012-10-30 21:08:27', 'MI121000050', '2012-10-30 21:08:27', 0),
('MI121000051', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361123456', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'bsy2rn8u', '7c136909d32164e9a94d217f8d79c0a8', '7354f832266a979042d93f3a54b01894', 1, 0, 'MI121000051', '2012-10-30 21:21:33', 'MI121000051', '2012-10-30 21:21:33', 0),
('MI121000052', 'Putu', 'Lastari', 'putulastari@gmail.com', '0823432534', '', 103, 'Bali', 'Denpasar', 'MU FC', '', '', '80361', 'uinc550u', 'ca2c2a1cfe17a228a75197c051415c7a', 'f36e5aaa83467cd41c99b9a4040e6483', 2, 0, 'MI121000052', '2012-10-31 11:00:56', 'MI121000052', '2012-10-31 11:00:56', 0),
('MI121000053', 'Agus', 'Setiawan', 'agus@lumonata.com', '0361754926', '03619144343', 103, 'Bali', 'Gianyar', 'Jl. I. B. Mantra Gg. Telaga Sari 1', '', 'Jl. Kubur Beach 27', '80582', 'txeaf5q5', '10c2e3a6dbe89006c82791d8d45c8d8f', '06c38702c45d523f81425597826e27f6', 2, 0, 'MI121000053', '2012-10-31 11:13:06', 'MI121000053', '2012-10-31 11:13:06', 0),
('MI121000054', 'Putu', 'Lastari', 'putulastari@gmail.com', '081827236376', '', 103, 'Bali', 'Denpasar', 'Kasih Ibu Hospital', '', '', '80361', '25d55ad283aa400af464c76d713c07ad', '25d55ad283aa400af464c76d713c07ad', 'fd19b2dcc362c7d61e48890618f54b96', 1, 0, 'MI121000054', '2012-10-31 12:12:12', 'MI121000054', '2012-10-31 12:44:24', 0),
('MI121100001', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '036112345', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', '37ddcc8396001f769e8ec3f20ca90f9c', '37ddcc8396001f769e8ec3f20ca90f9c', '71db2c89887480d138564cd727af9d42', 1, 0, 'MI121100001', '2012-11-01 11:55:11', 'MI121100001', '2012-11-01 12:07:32', 0),
('MI121100002', 'Tes', 'Tester', 'purwa@lumonata.com', '1234567', '', 103, 'Bali', 'Kuta', 'Jl Raya Kuta ', '', '', '80361', 'meoiafs2', '6cfc900816f0eacbd05136116b72e20d', 'fbcaab945eaf4845f5d614fcbedec23f', 1, 0, 'MI121100002', '2012-11-06 14:58:16', 'MI121100002', '2012-11-06 14:58:16', 0),
('MI121100003', 'Test', 'Tester', 'purwa@lumonata.com', '1234567', '', 103, 'Bali', 'Kuta', 'Jalan Raya Kuta', '', '', '80361', '5uqzw3yk', '73cca0c56b1f5464bd2587b04a9377b4', 'a4c051facc90ead088c02c3727f0ba92', 2, 0, 'MI121100003', '2012-11-06 15:29:02', 'MI121100003', '2012-11-06 15:29:02', 0),
('MI121100004', 'Wdana', 'Asmara', 'wdanaasmara474@gmail.com', '0812234245', '', 103, 'Bali', 'Dps', 'Jl. Sedap malam', '', '', '80361', '0xpmz0uu', 'dc48fa712b0893be7f179d6bb307917c', 'c8f451aaad49f10ddd1f49a3c25b811b', 2, 0, 'MI121100004', '2012-11-06 17:25:42', 'MI121100004', '2012-11-06 17:25:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_menu`
--

CREATE TABLE IF NOT EXISTS `lumonata_menu` (
  `lmenu_id` int(11) NOT NULL,
  `lmenu` varchar(255) NOT NULL DEFAULT '',
  `lparent_id` int(11) NOT NULL DEFAULT '0',
  `lcat_id` int(11) NOT NULL DEFAULT '0',
  `llink_type` char(1) NOT NULL DEFAULT '' COMMENT 'A=Apps, U=URL, P=Parent',
  `ltarget` varchar(20) NOT NULL DEFAULT '_self' COMMENT '_self,_blank,_top,_parent',
  `lpos` char(1) NOT NULL DEFAULT '' COMMENT 'L=Left, R=Right, T=Top, B=Bottom, M=Main Menu',
  `lapps` varchar(100) NOT NULL DEFAULT 'parent' COMMENT 'Folder name if applications',
  `lorder_id` int(11) NOT NULL DEFAULT '0',
  `lsef_url` varchar(100) NOT NULL DEFAULT '',
  `lpublish` smallint(6) NOT NULL DEFAULT '0',
  `lpublish_up` int(11) NOT NULL DEFAULT '0',
  `lpublish_down` int(11) NOT NULL DEFAULT '0',
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL DEFAULT '',
  `ldlu` int(11) NOT NULL DEFAULT '0',
  `llang_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lumonata_menu`
--

INSERT INTO `lumonata_menu` (`lmenu_id`, `lmenu`, `lparent_id`, `lcat_id`, `llink_type`, `ltarget`, `lpos`, `lapps`, `lorder_id`, `lsef_url`, `lpublish`, `lpublish_up`, `lpublish_down`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES
(7, 'Home', 0, 0, 'U', '_self', 'B', 'localhost/macadipan/', 94, 'home', 1, 1290419880, 0, 'admin', 1290419949, 'admin', 1301411262, 6),
(7, 'Startseite', 0, 0, 'U', '_self', 'B', 'localhost/macadipan/', 94, 'startseite', 1, 1290419880, 0, 'admin', 1290419949, 'admin', 1301411262, 5),
(7, 'Accueil', 0, 0, 'U', '_self', 'B', 'localhost/macadipan/', 94, 'accueil', 1, 1290419880, 0, 'admin', 1290419949, 'admin', 1301411262, 3),
(7, 'Inicio', 0, 0, 'U', '_self', 'B', 'localhost/macadipan/', 94, 'inicio', 1, 1290419880, 0, 'admin', 1290419949, 'admin', 1301411262, 2),
(7, 'Home', 0, 0, 'U', '_self', 'B', 'localhost/macadipan/', 22, 'home', 1, 1290419880, 0, 'admin', 1290419949, 'admin', 1301411262, 1),
(6, 'Kontakt', 0, 0, 'A', '_self', 'T', 'Contact_Form', 77, 'kontakt', 1, 1290419220, 0, 'admin', 1290419402, 'admin', 1302717419, 5),
(6, 'Contatto', 0, 0, 'A', '_self', 'T', 'Contact_Form', 77, 'contatto-6', 1, 1290419220, 0, 'admin', 1290419402, 'admin', 1302717419, 6),
(6, 'Contactez-nous', 0, 0, 'A', '_self', 'T', 'Contact_Form', 77, 'contactez-nous', 1, 1290419220, 0, 'admin', 1290419402, 'admin', 1302717419, 3),
(6, 'Contacto', 0, 0, 'A', '_self', 'T', 'Contact_Form', 77, 'contacto-6', 1, 1290419220, 0, 'admin', 1290419402, 'admin', 1302717419, 2),
(6, 'Contact Us', 0, 0, 'A', '_self', 'T', 'Contact_Form', 23, 'contact-us', 1, 1290419220, 0, 'admin', 1290419402, 'admin', 1302717419, 1),
(9, 'OUR VILLAS', 0, 0, 'A', '_self', 'M', 'Accommodations', 33, 'our-villas', 1, 1290482640, 0, 'admin', 1290482684, 'admin', 1302628386, 1),
(9, 'Destinos', 0, 0, 'A', '_self', 'M', 'Accommodations', 93, 'destinos', 1, 1290482640, 0, 'admin', 1290482684, 'admin', 1302628386, 2),
(9, 'Destinations', 0, 0, 'A', '_self', 'M', 'Accommodations', 93, 'destinations', 1, 1290482640, 0, 'admin', 1290482684, 'admin', 1302628386, 3),
(9, 'Ziele', 0, 0, 'A', '_self', 'M', 'Accommodations', 93, 'ziele', 1, 1290482640, 0, 'admin', 1290482684, 'admin', 1302628386, 5),
(9, 'Destinazioni', 0, 0, 'A', '_self', 'M', 'Accommodations', 93, 'destinazioni', 1, 1290482640, 0, 'admin', 1290482684, 'admin', 1302628386, 6),
(12, 'SPA', 0, 0, 'A', '_self', 'M', 'Spa', 31, 'spa', 1, 1290482760, 0, 'admin', 1290482793, 'admin', 1302714039, 1),
(2, 'Restaurant', 0, 0, 'P', '_self', 'B', 'Static_Pages', 92, 'restaurant-2', 1, 1290482760, 0, 'admin', 1290482793, 'admin', 1301410538, 2),
(2, 'Restaurant', 0, 0, 'P', '_self', 'B', 'Static_Pages', 92, 'restaurant-2', 1, 1290482760, 0, 'admin', 1290482793, 'admin', 1301410538, 3),
(2, 'Restaurant', 0, 0, 'P', '_self', 'B', 'Static_Pages', 92, 'restaurant-2', 1, 1290482760, 0, 'admin', 1290482793, 'admin', 1301410538, 5),
(2, 'Restaurant', 0, 0, 'P', '_self', 'B', 'Static_Pages', 92, 'restaurant-2', 1, 1290482760, 0, 'admin', 1290482793, 'admin', 1301410538, 6),
(21, 'SPECIAL OFFER', 0, 0, 'A', '_self', 'M', 'Special', 30, 'special-offer', 1, 1290496320, 0, 'admin', 1290496371, 'admin', 1302739132, 1),
(21, 'Galerias', 0, 0, 'A', '_self', 'M', 'Special', 91, 'galerias', 1, 1290496320, 0, 'admin', 1290496371, 'admin', 1302739132, 2),
(21, 'Galeries', 0, 0, 'A', '_self', 'M', 'Special', 91, 'galeries', 1, 1290496320, 0, 'admin', 1290496371, 'admin', 1302739132, 3),
(21, 'Galerie', 0, 0, 'A', '_self', 'M', 'Special', 91, 'galerie', 1, 1290496320, 0, 'admin', 1290496371, 'admin', 1302739132, 5),
(21, 'Gallerie', 0, 0, 'A', '_self', 'M', 'Special', 91, 'gallerie', 1, 1290496320, 0, 'admin', 1290496371, 'admin', 1302739132, 6),
(23, 'Contact Us', 0, 0, 'A', '_self', 'B', 'Contact_Form', 12, 'contact-us-23', 1, 1290496380, 0, 'admin', 1290496394, 'admin', 1301411261, 1),
(23, 'Contacto', 0, 0, 'A', '_self', 'B', 'Contact_Form', 89, 'contacto', 1, 1290496380, 0, 'admin', 1290496394, 'admin', 1301411261, 2),
(23, 'Contactez-nous', 0, 0, 'A', '_self', 'B', 'Contact_Form', 89, 'contactez-nous-23', 1, 1290496380, 0, 'admin', 1290496394, 'admin', 1301411261, 3),
(23, 'Kontakt', 0, 0, 'A', '_self', 'B', 'Contact_Form', 89, 'kontakt-23', 1, 1290496380, 0, 'admin', 1290496394, 'admin', 1301411262, 5),
(23, 'Contatto', 0, 0, 'A', '_self', 'B', 'Contact_Form', 89, 'contatto', 1, 1290496380, 0, 'admin', 1290496394, 'admin', 1301411262, 6),
(31, 'Reservation', 0, 0, 'A', '_self', 'T', 'Reservation_Form', 29, 'reservation', 0, 1290752880, 0, 'admin', 1290752940, 'admin', 1302706798, 1),
(31, 'Reservation', 0, 0, 'A', '_self', 'T', 'Reservation_Form', 69, 'reservation', 0, 1290752880, 0, 'admin', 1290752940, 'admin', 1302706798, 2),
(31, 'Reservation', 0, 0, 'A', '_self', 'T', 'Reservation_Form', 69, 'reservation', 0, 1290752880, 0, 'admin', 1290752940, 'admin', 1302706798, 3),
(31, 'Reservation', 0, 0, 'A', '_self', 'T', 'Reservation_Form', 69, 'reservation', 0, 1290752880, 0, 'admin', 1290752940, 'admin', 1302706798, 5),
(31, 'Reservation', 0, 0, 'A', '_self', 'T', 'Reservation_Form', 69, 'reservation', 0, 1290752880, 0, 'admin', 1290752940, 'admin', 1302706798, 6),
(32, 'ajaxex', 0, 0, 'A', '_self', 'M', 'ajaxex', 68, 'ajaxex', 0, 1290993300, 0, 'admin', 1290993325, 'admin', 1300359197, 1),
(32, 'ajaxex', 0, 0, 'A', '_self', 'M', 'ajaxex', 68, 'ajaxex', 0, 1290993300, 0, 'admin', 1290993325, 'admin', 1300359197, 2),
(32, 'ajaxex', 0, 0, 'A', '_self', 'M', 'ajaxex', 68, 'ajaxex', 0, 1290993300, 0, 'admin', 1290993325, 'admin', 1300359197, 3),
(32, 'ajaxex', 0, 0, 'A', '_self', 'M', 'ajaxex', 68, 'ajaxex', 0, 1290993300, 0, 'admin', 1290993325, 'admin', 1300359197, 5),
(32, 'ajaxex', 0, 0, 'A', '_self', 'M', 'ajaxex', 68, 'ajaxex', 0, 1290993300, 0, 'admin', 1290993325, 'admin', 1300359197, 6),
(33, 'Facilities', 0, 7, 'A', '_self', 'T', 'Static_Pages', 25, 'facilities', 1, 1301388120, 0, 'admin', 1301388149, 'admin', 1301499134, 3),
(33, 'Facilities', 0, 7, 'A', '_self', 'T', 'Static_Pages', 25, 'facilities', 1, 1301388120, 0, 'admin', 1301388149, 'admin', 1301499134, 5),
(33, 'Facilities', 0, 7, 'A', '_self', 'T', 'Static_Pages', 25, 'facilities', 1, 1301388120, 0, 'admin', 1301388149, 'admin', 1301499134, 6),
(34, 'Gallery', 0, 0, 'A', '_self', 'T', 'Photos', 26, 'gallery', 1, 1301388180, 0, 'admin', 1301388271, 'admin', 1311322377, 1),
(34, 'Gallery', 0, 0, 'A', '_self', 'T', 'Photos', 24, 'gallery', 1, 1301388180, 0, 'admin', 1301388271, 'admin', 1311322378, 2),
(34, 'Gallery', 0, 0, 'A', '_self', 'T', 'Photos', 24, 'gallery', 1, 1301388180, 0, 'admin', 1301388271, 'admin', 1311322378, 3),
(34, 'Gallery', 0, 0, 'A', '_self', 'T', 'Photos', 24, 'gallery', 1, 1301388180, 0, 'admin', 1301388271, 'admin', 1311322378, 5),
(34, 'Gallery', 0, 0, 'A', '_self', 'T', 'Photos', 24, 'gallery', 1, 1301388180, 0, 'admin', 1301388271, 'admin', 1311322378, 6),
(35, 'Location', 0, 4, 'A', '_self', 'T', 'Static_Pages', 25, 'location', 1, 1301388240, 0, 'admin', 1301388295, 'admin', 1301499963, 1),
(35, 'Location', 0, 4, 'A', '_self', 'T', 'Static_Pages', 23, 'location', 1, 1301388240, 0, 'admin', 1301388295, 'admin', 1301499963, 2),
(35, 'Location', 0, 4, 'A', '_self', 'T', 'Static_Pages', 23, 'location', 1, 1301388240, 0, 'admin', 1301388295, 'admin', 1301499963, 3),
(35, 'Location', 0, 4, 'A', '_self', 'T', 'Static_Pages', 23, 'location', 1, 1301388240, 0, 'admin', 1301388295, 'admin', 1301499963, 5),
(35, 'Location', 0, 4, 'A', '_self', 'T', 'Static_Pages', 23, 'location', 1, 1301388240, 0, 'admin', 1301388295, 'admin', 1301499963, 6),
(36, 'Career', 0, 0, 'A', '_self', 'T', 'Career', 24, 'career', 1, 1301388240, 0, 'admin', 1301388315, 'admin', 1302633137, 1),
(36, 'Career', 0, 0, 'A', '_self', 'T', 'Career', 22, 'career', 1, 1301388240, 0, 'admin', 1301388315, 'admin', 1302633137, 2),
(36, 'Career', 0, 0, 'A', '_self', 'T', 'Career', 22, 'career', 1, 1301388240, 0, 'admin', 1301388315, 'admin', 1302633137, 3),
(36, 'Career', 0, 0, 'A', '_self', 'T', 'Career', 22, 'career', 1, 1301388240, 0, 'admin', 1301388315, 'admin', 1302633137, 5),
(36, 'Career', 0, 0, 'A', '_self', 'T', 'Career', 22, 'career', 1, 1301388240, 0, 'admin', 1301388315, 'admin', 1302633137, 6),
(4, 'Our Villas', 0, 0, 'A', '_self', 'B', 'Accommodations', 21, 'our-villas-4', 1, 1301388360, 0, 'admin', 1301388436, 'admin', 1302706697, 1),
(4, 'Our Villas', 0, 0, 'A', '_self', 'B', 'Accommodations', 21, 'our-villas-4', 1, 1301388360, 0, 'admin', 1301388436, 'admin', 1302706697, 2),
(4, 'Our Villas', 0, 0, 'A', '_self', 'B', 'Accommodations', 21, 'our-villas-4', 1, 1301388360, 0, 'admin', 1301388436, 'admin', 1302706697, 3),
(4, 'Our Villas', 0, 0, 'A', '_self', 'B', 'Accommodations', 21, 'our-villas-4', 1, 1301388360, 0, 'admin', 1301388436, 'admin', 1302706697, 5),
(4, 'Our Villas', 0, 0, 'A', '_self', 'B', 'Accommodations', 21, 'our-villas-4', 1, 1301388360, 0, 'admin', 1301388436, 'admin', 1302706697, 6),
(38, 'RESTAURANT', 0, 3, 'A', '_self', 'M', 'Static_Pages', 32, 'restaurant', 1, 1301388420, 0, 'admin', 1301388453, 'admin', 1311061413, 1),
(38, 'RESTAURANT', 0, 3, 'A', '_self', 'M', 'Static_Pages', 20, 'restaurant', 1, 1301388420, 0, 'admin', 1301388453, 'admin', 1311061413, 2),
(38, 'RESTAU', 0, 3, 'A', '_self', 'M', 'Static_Pages', 20, 'restau', 1, 1301388420, 0, 'admin', 1301388453, 'admin', 1311061413, 3),
(38, 'RESTAURANT', 0, 3, 'A', '_self', 'M', 'Static_Pages', 20, 'restaurant', 1, 1301388420, 0, 'admin', 1301388453, 'admin', 1311061413, 5),
(38, 'RESTAURANT', 0, 3, 'A', '_self', 'M', 'Static_Pages', 20, 'restaurant', 1, 1301388420, 0, 'admin', 1301388453, 'admin', 1311061413, 6),
(39, 'Spa', 0, 0, 'A', '_self', 'B', 'Spa', 19, 'spa-39', 1, 1301388420, 0, 'admin', 1301388472, 'admin', 1302737145, 1),
(39, 'Spa', 0, 0, 'A', '_self', 'B', 'Spa', 19, 'spa-39', 1, 1301388420, 0, 'admin', 1301388472, 'admin', 1302737145, 2),
(39, 'Spa', 0, 0, 'A', '_self', 'B', 'Spa', 19, 'spa-39', 1, 1301388420, 0, 'admin', 1301388472, 'admin', 1302737145, 3),
(39, 'Spa', 0, 0, 'A', '_self', 'B', 'Spa', 19, 'spa-39', 1, 1301388420, 0, 'admin', 1301388472, 'admin', 1302737145, 5),
(39, 'Spa', 0, 0, 'A', '_self', 'B', 'Spa', 19, 'spa-39', 1, 1301388420, 0, 'admin', 1301388472, 'admin', 1302737145, 6),
(40, 'About The Villas', 0, 2, 'A', '_self', 'T', 'Static_Pages', 28, 'about-the-villas', 1, 1301388420, 0, 'admin', 1301388489, 'admin', 1301499014, 1),
(40, 'About The Villas', 0, 2, 'A', '_self', 'T', 'Static_Pages', 18, 'about-the-villas', 1, 1301388420, 0, 'admin', 1301388489, 'admin', 1301499014, 2),
(40, 'About The Villas', 0, 2, 'A', '_self', 'T', 'Static_Pages', 18, 'about-the-villas', 1, 1301388420, 0, 'admin', 1301388489, 'admin', 1301499014, 3),
(40, 'About The Villas', 0, 2, 'A', '_self', 'T', 'Static_Pages', 18, 'about-the-villas', 1, 1301388420, 0, 'admin', 1301388489, 'admin', 1301499014, 5),
(40, 'About The Villas', 0, 2, 'A', '_self', 'T', 'Static_Pages', 18, 'about-the-villas', 1, 1301388420, 0, 'admin', 1301388489, 'admin', 1301499014, 6),
(41, 'The Facilities', 0, 7, 'A', '_self', 'B', 'Static_Pages', 17, 'the-facilities', 1, 1301388480, 0, 'admin', 1301388503, 'admin', 1301499925, 1),
(41, 'The Facilities', 0, 7, 'A', '_self', 'B', 'Static_Pages', 17, 'the-facilities', 1, 1301388480, 0, 'admin', 1301388503, 'admin', 1301499925, 2),
(41, 'The Facilities', 0, 7, 'A', '_self', 'B', 'Static_Pages', 17, 'the-facilities', 1, 1301388480, 0, 'admin', 1301388503, 'admin', 1301499925, 3),
(41, 'The Facilities', 0, 7, 'A', '_self', 'B', 'Static_Pages', 17, 'the-facilities', 1, 1301388480, 0, 'admin', 1301388503, 'admin', 1301499925, 5),
(41, 'The Facilities', 0, 7, 'A', '_self', 'B', 'Static_Pages', 17, 'the-facilities', 1, 1301388480, 0, 'admin', 1301388503, 'admin', 1301499925, 6),
(5, 'Gallery', 0, 0, 'A', '_self', 'B', 'Photos', 16, 'gallery-5', 1, 1301388480, 0, 'admin', 1301388519, 'admin', 1302706624, 1),
(5, 'Gallery', 0, 0, 'A', '_self', 'B', 'Photos', 16, 'gallery-5', 1, 1301388480, 0, 'admin', 1301388519, 'admin', 1302706624, 2),
(5, 'Gallery', 0, 0, 'A', '_self', 'B', 'Photos', 16, 'gallery-5', 1, 1301388480, 0, 'admin', 1301388519, 'admin', 1302706624, 3),
(5, 'Gallery', 0, 0, 'A', '_self', 'B', 'Photos', 16, 'gallery-5', 1, 1301388480, 0, 'admin', 1301388519, 'admin', 1302706624, 5),
(5, 'Gallery', 0, 0, 'A', '_self', 'B', 'Photos', 16, 'gallery-5', 1, 1301388480, 0, 'admin', 1301388519, 'admin', 1302706624, 6),
(1, 'Location', 0, 4, 'A', '_self', 'B', 'Static_Pages', 15, 'location-1', 1, 1301388480, 0, 'admin', 1301388536, 'admin', 1301499882, 1),
(1, 'Location', 0, 4, 'A', '_self', 'B', 'Static_Pages', 15, 'location-1', 1, 1301388480, 0, 'admin', 1301388536, 'admin', 1301499882, 2),
(1, 'Location', 0, 4, 'A', '_self', 'B', 'Static_Pages', 15, 'location-1', 1, 1301388480, 0, 'admin', 1301388536, 'admin', 1301499882, 3),
(1, 'Location', 0, 4, 'A', '_self', 'B', 'Static_Pages', 15, 'location-1', 1, 1301388480, 0, 'admin', 1301388536, 'admin', 1301499882, 5),
(1, 'Location', 0, 4, 'A', '_self', 'B', 'Static_Pages', 15, 'location-1', 1, 1301388480, 0, 'admin', 1301388536, 'admin', 1301499882, 6),
(3, 'Career', 0, 0, 'A', '_self', 'B', 'Career', 13, 'career-3', 1, 1301388480, 0, 'admin', 1301388551, 'admin', 1302633107, 1),
(3, 'Career', 0, 0, 'A', '_self', 'B', 'Career', 14, 'career-3', 1, 1301388480, 0, 'admin', 1301388551, 'admin', 1302633107, 2),
(3, 'Career', 0, 0, 'A', '_self', 'B', 'Career', 14, 'career-3', 1, 1301388480, 0, 'admin', 1301388551, 'admin', 1302633107, 3),
(3, 'Career', 0, 0, 'A', '_self', 'B', 'Career', 14, 'career-3', 1, 1301388480, 0, 'admin', 1301388551, 'admin', 1302633107, 5),
(3, 'Career', 0, 0, 'A', '_self', 'B', 'Career', 14, 'career-3', 1, 1301388480, 0, 'admin', 1301388551, 'admin', 1302633107, 6),
(45, 'About The Villas', 0, 2, 'A', '_self', 'B', 'Static_Pages', 18, 'about-the-villas-45', 1, 1301388720, 0, 'admin', 1301388787, 'admin', 1301499925, 1),
(45, 'About The Villas', 0, 2, 'A', '_self', 'B', 'Static_Pages', 13, 'about-the-villas-45', 1, 1301388720, 0, 'admin', 1301388787, 'admin', 1301499925, 2),
(45, 'About The Villas', 0, 2, 'A', '_self', 'B', 'Static_Pages', 13, 'about-the-villas-45', 1, 1301388720, 0, 'admin', 1301388787, 'admin', 1301499925, 3),
(45, 'About The Villas', 0, 2, 'A', '_self', 'B', 'Static_Pages', 13, 'about-the-villas-45', 1, 1301388720, 0, 'admin', 1301388787, 'admin', 1301499925, 5),
(45, 'About The Villas', 0, 2, 'A', '_self', 'B', 'Static_Pages', 13, 'about-the-villas-45', 1, 1301388720, 0, 'admin', 1301388787, 'admin', 1301499925, 6),
(2, 'Restaurant', 0, 3, 'A', '_self', 'B', 'Static_Pages', 20, 'restaurant-2', 1, 1301388780, 0, 'admin', 1301388839, 'admin', 1301499883, 1),
(46, 'Reservation', 0, 3, 'A', '_self', 'B', 'Static_Pages', 12, 'reservation-46', 1, 1301388780, 0, 'admin', 1301388839, 'admin', 1301499883, 2),
(46, 'Reservation', 0, 3, 'A', '_self', 'B', 'Static_Pages', 12, 'reservation-46', 1, 1301388780, 0, 'admin', 1301388839, 'admin', 1301499883, 3),
(46, 'Reservation', 0, 3, 'A', '_self', 'B', 'Static_Pages', 12, 'reservation-46', 1, 1301388780, 0, 'admin', 1301388839, 'admin', 1301499883, 5),
(46, 'Reservation', 0, 3, 'A', '_self', 'B', 'Static_Pages', 12, 'reservation-46', 1, 1301388780, 0, 'admin', 1301388839, 'admin', 1301499883, 6),
(33, 'Facilities', 0, 7, 'A', '_self', 'T', 'Static_Pages', 27, 'facilities', 1, 1301388120, 0, 'admin', 1301388149, 'admin', 1301499134, 1),
(33, 'Facilities', 0, 7, 'A', '_self', 'T', 'Static_Pages', 25, 'facilities', 1, 1301388120, 0, 'admin', 1301388149, 'admin', 1301499134, 2),
(47, 'About The Location', 0, 6, 'A', '_self', 'T', 'Static_Pages', 11, 'about-the-location', 0, 1302680820, 0, 'admin', 1302680926, '', 0, 1),
(47, '', 0, 6, 'A', '_self', 'T', 'Static_Pages', 11, 'about-the-location', 0, 1302680820, 0, 'admin', 1302680926, '', 0, 2),
(47, '', 0, 6, 'A', '_self', 'T', 'Static_Pages', 11, 'about-the-location', 0, 1302680820, 0, 'admin', 1302680926, '', 0, 3),
(47, '', 0, 6, 'A', '_self', 'T', 'Static_Pages', 11, 'about-the-location', 0, 1302680820, 0, 'admin', 1302680926, '', 0, 5),
(47, '', 0, 6, 'A', '_self', 'T', 'Static_Pages', 11, 'about-the-location', 0, 1302680820, 0, 'admin', 1302680926, '', 0, 6),
(12, 'SPA', 0, 0, 'A', '_self', 'M', 'Spa', 10, 'spa', 1, 1290482760, 0, 'admin', 1302714039, '', 0, 2),
(12, 'SPA', 0, 0, 'A', '_self', 'M', 'Spa', 10, 'spa', 1, 1290482760, 0, 'admin', 1302714039, '', 0, 3),
(12, 'SPA', 0, 0, 'A', '_self', 'M', 'Spa', 10, 'spa', 1, 1290482760, 0, 'admin', 1302714039, '', 0, 5),
(12, 'SPA', 0, 0, 'A', '_self', 'M', 'Spa', 10, 'spa', 1, 1290482760, 0, 'admin', 1302714039, '', 0, 6),
(48, 'Availability', 0, 0, 'A', '_self', 'M', 'Availability', 10, 'availability', 0, 1302753600, 0, 'admin', 1302753702, '', 0, 1),
(48, '', 0, 0, 'A', '_self', 'M', 'Availability', 10, 'availability', 0, 1302753600, 0, 'admin', 1302753702, '', 0, 2),
(48, '', 0, 0, 'A', '_self', 'M', 'Availability', 10, 'availability', 0, 1302753600, 0, 'admin', 1302753702, '', 0, 3),
(48, '', 0, 0, 'A', '_self', 'M', 'Availability', 10, 'availability', 0, 1302753600, 0, 'admin', 1302753702, '', 0, 5),
(48, '', 0, 0, 'A', '_self', 'M', 'Availability', 10, 'availability', 0, 1302753600, 0, 'admin', 1302753702, '', 0, 6),
(49, 'Villa Reservation', 0, 0, 'A', '_self', 'M', 'Villa_Reservation', 9, 'villa-reservation', 0, 1302756240, 0, 'admin', 1302756322, '', 0, 1),
(49, '', 0, 0, 'A', '_self', 'M', 'Villa_Reservation', 9, 'villa-reservation', 0, 1302756240, 0, 'admin', 1302756322, '', 0, 2),
(49, '', 0, 0, 'A', '_self', 'M', 'Villa_Reservation', 9, 'villa-reservation', 0, 1302756240, 0, 'admin', 1302756322, '', 0, 3),
(49, '', 0, 0, 'A', '_self', 'M', 'Villa_Reservation', 9, 'villa-reservation', 0, 1302756240, 0, 'admin', 1302756322, '', 0, 5),
(49, '', 0, 0, 'A', '_self', 'M', 'Villa_Reservation', 9, 'villa-reservation', 0, 1302756240, 0, 'admin', 1302756322, '', 0, 6),
(50, 'Villa Form', 0, 0, 'A', '_self', 'M', 'Villa_Form', 8, 'villa-form', 0, 1302756300, 0, 'admin', 1302756740, '', 0, 1),
(50, '', 0, 0, 'A', '_self', 'M', 'Villa_Form', 8, 'villa-form', 0, 1302756300, 0, 'admin', 1302756740, '', 0, 2),
(50, '', 0, 0, 'A', '_self', 'M', 'Villa_Form', 8, 'villa-form', 0, 1302756300, 0, 'admin', 1302756740, '', 0, 3),
(50, '', 0, 0, 'A', '_self', 'M', 'Villa_Form', 8, 'villa-form', 0, 1302756300, 0, 'admin', 1302756740, '', 0, 5),
(50, '', 0, 0, 'A', '_self', 'M', 'Villa_Form', 8, 'villa-form', 0, 1302756300, 0, 'admin', 1302756740, '', 0, 6),
(51, 'Villa Credit', 0, 0, 'A', '_self', 'M', 'Villa_Credit', 7, 'villa-credit', 0, 1302756720, 0, 'admin', 1302756907, 'admin', 1308814372, 1),
(51, 'Credit Card Confirmation', 0, 0, 'A', '_self', 'M', 'Villa_Credit', 7, 'credit-card-confirmation', 0, 1302756720, 0, 'admin', 1302756907, 'admin', 1308814372, 2),
(51, 'Credit Card Confirmation', 0, 0, 'A', '_self', 'M', 'Villa_Credit', 7, 'credit-card-confirmation', 0, 1302756720, 0, 'admin', 1302756907, 'admin', 1308814372, 3),
(51, 'Credit Card Confirmation', 0, 0, 'A', '_self', 'M', 'Villa_Credit', 7, 'credit-card-confirmation', 0, 1302756720, 0, 'admin', 1302756907, 'admin', 1308814372, 5),
(51, 'Credit Card Confirmation', 0, 0, 'A', '_self', 'M', 'Villa_Credit', 7, 'credit-card-confirmation', 0, 1302756720, 0, 'admin', 1302756907, 'admin', 1308814372, 6),
(52, 'Spa Booking', 0, 0, 'A', '_self', 'M', 'Spa_Booking', 6, 'spa-booking', 0, 1302757740, 0, 'admin', 1302757820, '', 0, 1),
(52, '', 0, 0, 'A', '_self', 'M', 'Spa_Booking', 6, 'spa-booking', 0, 1302757740, 0, 'admin', 1302757820, '', 0, 2),
(52, '', 0, 0, 'A', '_self', 'M', 'Spa_Booking', 6, 'spa-booking', 0, 1302757740, 0, 'admin', 1302757820, '', 0, 3),
(52, '', 0, 0, 'A', '_self', 'M', 'Spa_Booking', 6, 'spa-booking', 0, 1302757740, 0, 'admin', 1302757820, '', 0, 5),
(52, '', 0, 0, 'A', '_self', 'M', 'Spa_Booking', 6, 'spa-booking', 0, 1302757740, 0, 'admin', 1302757820, '', 0, 6),
(53, 'Google Map', 35, 5, 'A', '_self', 'M', 'Static_Pages', 5, 'google-map', 0, 1304499600, 0, 'admin', 1304499787, '', 0, 1),
(53, '', 35, 5, 'A', '_self', 'M', 'Static_Pages', 5, 'google-map', 0, 1304499600, 0, 'admin', 1304499787, '', 0, 2),
(53, '', 35, 5, 'A', '_self', 'M', 'Static_Pages', 5, 'google-map', 0, 1304499600, 0, 'admin', 1304499787, '', 0, 3),
(53, '', 35, 5, 'A', '_self', 'M', 'Static_Pages', 5, 'google-map', 0, 1304499600, 0, 'admin', 1304499787, '', 0, 5),
(53, '', 35, 5, 'A', '_self', 'M', 'Static_Pages', 5, 'google-map', 0, 1304499600, 0, 'admin', 1304499787, '', 0, 6),
(54, 'Terms and Conditions', 0, 9, 'A', '_self', 'B', 'Static_Pages', 4, 'terms-and-conditions', 0, 1308724260, 0, 'admin', 1308724331, '', 0, 1),
(54, '', 0, 9, 'A', '_self', 'B', 'Static_Pages', 4, 'terms-and-conditions', 0, 1308724260, 0, 'admin', 1308724331, '', 0, 2),
(54, '', 0, 9, 'A', '_self', 'B', 'Static_Pages', 4, 'terms-and-conditions', 0, 1308724260, 0, 'admin', 1308724331, '', 0, 3),
(54, '', 0, 9, 'A', '_self', 'B', 'Static_Pages', 4, 'terms-and-conditions', 0, 1308724260, 0, 'admin', 1308724331, '', 0, 5),
(54, '', 0, 9, 'A', '_self', 'B', 'Static_Pages', 4, 'terms-and-conditions', 0, 1308724260, 0, 'admin', 1308724331, '', 0, 6),
(55, 'Reservation Details', 0, 0, 'A', '_self', 'M', 'Reservation_Details', 3, 'reservation-details', 0, 1308816180, 0, 'admin', 1308816247, '', 0, 1),
(55, '', 0, 0, 'A', '_self', 'M', 'Reservation_Details', 3, 'reservation-details', 0, 1308816180, 0, 'admin', 1308816247, '', 0, 2),
(55, '', 0, 0, 'A', '_self', 'M', 'Reservation_Details', 3, 'reservation-details', 0, 1308816180, 0, 'admin', 1308816247, '', 0, 3),
(55, '', 0, 0, 'A', '_self', 'M', 'Reservation_Details', 3, 'reservation-details', 0, 1308816180, 0, 'admin', 1308816247, '', 0, 5),
(55, '', 0, 0, 'A', '_self', 'M', 'Reservation_Details', 3, 'reservation-details', 0, 1308816180, 0, 'admin', 1308816247, '', 0, 6),
(56, 'Last Minutes Deal', 0, 0, 'A', '_self', 'T', 'Last_Minutes_Deal', 2, 'last-minutes-deal', 0, 1310544480, 0, 'admin', 1310544561, '', 0, 1),
(56, '', 0, 0, 'A', '_self', 'T', 'Last_Minutes_Deal', 2, 'last-minutes-deal', 0, 1310544480, 0, 'admin', 1310544561, '', 0, 2),
(56, '', 0, 0, 'A', '_self', 'T', 'Last_Minutes_Deal', 2, 'last-minutes-deal', 0, 1310544480, 0, 'admin', 1310544561, '', 0, 3),
(56, '', 0, 0, 'A', '_self', 'T', 'Last_Minutes_Deal', 2, 'last-minutes-deal', 0, 1310544480, 0, 'admin', 1310544561, '', 0, 5),
(56, '', 0, 0, 'A', '_self', 'T', 'Last_Minutes_Deal', 2, 'last-minutes-deal', 0, 1310544480, 0, 'admin', 1310544561, '', 0, 6),
(57, 'Rates', 0, 1, 'A', '_self', 'T', 'Static_Pages', 1, 'rates', 0, 1311664200, 0, 'admin', 1311664244, '', 0, 1),
(57, '', 0, 1, 'A', '_self', 'T', 'Static_Pages', 1, 'rates', 0, 1311664200, 0, 'admin', 1311664244, '', 0, 2),
(57, '', 0, 1, 'A', '_self', 'T', 'Static_Pages', 1, 'rates', 0, 1311664200, 0, 'admin', 1311664244, '', 0, 3),
(57, '', 0, 1, 'A', '_self', 'T', 'Static_Pages', 1, 'rates', 0, 1311664200, 0, 'admin', 1311664244, '', 0, 5),
(57, '', 0, 1, 'A', '_self', 'T', 'Static_Pages', 1, 'rates', 0, 1311664200, 0, 'admin', 1311664244, '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_meta_data`
--

CREATE TABLE IF NOT EXISTS `lumonata_meta_data` (
  `lmeta_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmeta_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lmeta_value` longtext CHARACTER SET utf8 NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lapp_id` int(11) NOT NULL,
  PRIMARY KEY (`lmeta_id`),
  KEY `meta_name` (`lmeta_name`),
  KEY `app_name` (`lapp_name`),
  KEY `app_id` (`lapp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=130 ;

--
-- Dumping data for table `lumonata_meta_data`
--

INSERT INTO `lumonata_meta_data` (`lmeta_id`, `lmeta_name`, `lmeta_value`, `lapp_name`, `lapp_id`) VALUES
(1, 'front_theme', 'villa-vedas', 'themes', 0),
(2, 'time_zone', 'Asia/Singapore', 'global_setting', 0),
(3, 'site_url', 'villavedas.com', 'global_setting', 0),
(4, 'web_title', 'Villa Vedas                                                    ', 'global_setting', 0),
(5, 'smtp_server', '10.10.10.19', 'global_setting', 0),
(6, 'admin_theme', 'default', 'themes', 0),
(7, 'smtp', 'mail.villavedas.com', 'global_setting', 0),
(8, 'email', 'ary@villavedas.com', 'global_setting', 0),
(9, 'web_tagline', '', 'global_setting', 0),
(10, 'invitation_limit', '10', 'global_setting', 0),
(11, 'date_format', 'F j, Y', 'global_setting', 0),
(12, 'time_format', 'H:i', 'global_setting', 0),
(13, 'post_viewed', '10', 'global_setting', 0),
(14, 'rss_viewed', '15', 'global_setting', 0),
(15, 'rss_view_format', 'full_text', 'global_setting', 0),
(16, 'list_viewed', '50', 'global_setting', 0),
(17, 'email_format', 'html', 'global_setting', 0),
(18, 'text_editor', 'tiny_mce', 'global_setting', 0),
(19, 'thumbnail_image_size', '400:300', 'global_setting', 0),
(20, 'large_image_size', '1024:1024', 'global_setting', 0),
(21, 'medium_image_size', '900:900', 'global_setting', 0),
(22, 'is_allow_comment', '1', 'global_setting', 0),
(23, 'is_login_to_comment', '1', 'global_setting', 0),
(24, 'is_auto_close_comment', '0', 'global_setting', 0),
(25, 'days_auto_close_comment', '15', 'global_setting', 0),
(26, 'is_break_comment', '1', 'global_setting', 0),
(27, 'comment_page_displayed', 'last', 'global_setting', 0),
(28, 'comment_per_page', '3', 'global_setting', 0),
(29, 'active_plugins', '{"lumonata-meta-data":"\\/metadata\\/metadata.php","contact-us":"\\/contact_us\\/contact_us.php","article-to-blog":"\\/article_to_blog\\/article_to_blog.php","paypal-ipn":"\\/paypal_ipn\\/ipn_1.0.php","gallery":"\\/gallery\\/gallery.php","social-media":"\\/social_media\\/social_media.php","testimonail-front":"\\/testimonial_front\\/testimonial_front.php","manage-villa-program":"\\/manage_villa_program\\/index.php","header-image":"\\/header_image\\/header_image.php","booking-engine":"\\/booking_engine\\/booking_engine.php","reservation":"\\/reservation\\/index.php","simple-query":"\\/simple_query\\/index.php","reservation-form":"\\/reservation-form\\/index.php"}', 'plugins', 0),
(30, 'save_changes', 'Save Changes', 'global_setting', 0),
(31, 'is_rewrite', 'yes', 'global_setting', 0),
(32, 'is_allow_post_like', '1', 'global_setting', 0),
(33, 'is_allow_comment_like', '1', 'global_setting', 0),
(34, 'alert_on_register', '1', 'global_setting', 0),
(35, 'alert_on_comment', '1', 'global_setting', 0),
(36, 'alert_on_comment_reply', '1', 'global_setting', 0),
(37, 'alert_on_liked_post', '1', 'global_setting', 0),
(38, 'alert_on_liked_comment', '1', 'global_setting', 0),
(39, 'web_name', 'Villa Vedas                                  ', 'global_setting', 0),
(40, 'meta_description', '', 'global_setting', 0),
(41, 'meta_keywords', 'Villa Vedas', 'global_setting', 0),
(42, 'meta_title', 'Villa Vedas - Bali', 'global_setting', 0),
(43, 'custome_bg_color', 'FFF', 'themes', 0),
(44, 'status_viewed', '30', 'global_setting', 0),
(45, 'update', 'true', 'global_setting', 0),
(46, 'the_date_format', 'F j, Y', 'global_setting', 0),
(47, 'the_time_format', 'H:i', 'global_setting', 0),
(48, 'thumbnail_image_width', '400', 'global_setting', 0),
(49, 'thumbnail_image_height', '300', 'global_setting', 0),
(50, 'medium_image_width', '900', 'global_setting', 0),
(51, 'medium_image_height', '900', 'global_setting', 0),
(52, 'large_image_width', '1024', 'global_setting', 0),
(53, 'large_image_height', '1024', 'global_setting', 0),
(54, 'menu_set', '{"menu-top":"Menu Top","menu-mobile":"Menu Mobile","menu-bottom":"Menu Bottom","menu-without-reservation":"Menu Without Reservation"}', 'menus', 0),
(120, 'menu_items_menu-top', '[{"id":1,"label":"Bedrooms","target":"_self","link":"\\/?app_name=articles&cat_id=114","permalink":"articles\\/bedrooms\\/"},{"id":2,"label":"Facilities","target":"_self","link":"\\/?app_name=articles&cat_id=113","permalink":"articles\\/facilities\\/"},{"id":5,"label":"Location","target":"_self","link":"\\/?page_id=351","permalink":"location\\/"},{"id":6,"label":"About Us","target":"_self","link":"\\/?app_name=articles&cat_id=112","permalink":"articles\\/about-us\\/"},{"id":7,"label":"Technical","target":"_self","link":"\\/?page_id=372","permalink":"http:\\/\\/villavedas.com\\/articles\\/technical\\/technical.html"}]', 'menus', 0),
(89, 'language', '1', 'product_setting', 0),
(90, 'currency', '6', 'product_setting', 0),
(91, 'unit_system', 'metric', 'product_setting', 0),
(92, 'minimum_price', '100', 'product_setting', 0),
(93, 'payments', '{"parent_payment":["payments_paypal_standard"],"child_payment":[{"name":"PayPal","text":"dana_1350365534_biz@lumonata.com","mode":"0"}]}', 'product_setting', 0),
(121, 'menu_order_menu-top', '[{"id":"6"},{"id":"1"},{"id":"2"},{"id":"7"},{"id":"5"}]', 'menus', 0),
(126, 'menu_items_menu-bottom', '[{"id":0,"label":"Terms & Conditions","target":"_self","link":"\\/?page_id=352","permalink":"terms-conditions\\/"},{"id":1,"label":"Contact Us","target":"_self","link":"\\/?page_id=353","permalink":"contact-us\\/"}]', 'menus', 0),
(127, 'menu_order_menu-bottom', '[{"id":0},{"id":1}]', 'menus', 0),
(124, 'menu_items_menu-mobile', '{"0":{"id":0,"label":"About Us","target":"_self","link":"\\/?app_name=articles&cat_id=112","permalink":"articles\\/about-us\\/"},"1":{"id":1,"label":"Bedrooms","target":"_self","link":"\\/?app_name=articles&cat_id=114","permalink":"articles\\/bedrooms\\/"},"2":{"id":5,"label":"Location","target":"_self","link":"\\/?page_id=351","permalink":"location\\/"},"4":{"id":11,"label":"Technical","target":"_self","link":"\\/?page_id=372","permalink":"http:\\/\\/villavedas.com\\/articles\\/technical\\/technical.html"},"5":{"id":12,"label":"Facilities","target":"_self","link":"\\/?app_name=articles&cat_id=113","permalink":"articles\\/facilities\\/"}}', 'menus', 0),
(125, 'menu_order_menu-mobile', '{"0":{"id":"0"},"1":{"id":"1"},"3":{"id":"11"},"4":{"id":"12"},"5":{"id":"5"}}', 'menus', 0),
(128, 'menu_items_menu-without-reservation', '{"0":{"id":0,"label":"About Us","target":"_self","link":"\\/?page_id=349","permalink":"http:\\/\\/villavedas.com\\/articles\\/about-us\\/about-us.html"},"1":{"id":1,"label":"Bedrooms","target":"_self","link":"\\/?app_name=articles&cat_id=114","permalink":"articles\\/bedrooms\\/"},"2":{"id":2,"label":"Facilities","target":"_self","link":"\\/?app_name=articles&cat_id=113","permalink":"articles\\/facilities\\/"},"3":{"id":3,"label":"Services","target":"_self","link":"\\/?app_name=articles&cat_id=115","permalink":"articles\\/services\\/"},"4":{"id":5,"label":"Location","target":"_self","link":"\\/?page_id=351","permalink":"location\\/"},"5":{"id":7,"label":"Terms & Conditions","target":"_self","link":"\\/?page_id=352","permalink":"terms-conditions\\/"},"6":{"id":8,"label":"Contact Us","target":"_self","link":"\\/?page_id=353","permalink":"contact-us\\/"},"7":{"id":9,"label":"Events","target":"_self","link":"\\/?app_name=articles&cat_id=118","permalink":"articles\\/events\\/"},"9":{"id":11,"label":"Reservation","target":"_self","link":"\\/?page_id=365","permalink":"reservation-form\\/"}}', 'menus', 0),
(129, 'menu_order_menu-without-reservation', '{"0":{"id":"0"},"1":{"id":"1"},"2":{"id":"2"},"3":{"id":"3"},"4":{"id":"9"},"5":{"id":"5"},"7":{"id":"11"},"8":{"id":"7"},"9":{"id":"8"}}', 'menus', 0),
(113, 'dp_presentase', '50', 'global_setting', 0),
(114, 'date_before_booking', '1', 'global_setting', 0),
(115, 'due_date_final_payment', '1', 'global_setting', 0),
(116, 'days_crojob_final_payment', '1', 'global_setting', 0),
(117, 'email_paypal', 'adi@lumonata.com', 'global_setting', 0),
(118, 'email_user_smtp', 'sender@villavedas.com', 'global_setting', 0),
(119, 'pass_email_user_smtp', 'eyJwX2VfdV9zbXRwIjoiczNuZDNyMTIzIn0=', 'global_setting', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_module`
--

CREATE TABLE IF NOT EXISTS `lumonata_module` (
  `lmodule_id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `lparent_id` int(11) NOT NULL DEFAULT '0',
  `lfolder` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ltitle` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `llink_type` char(2) COLLATE latin1_general_ci NOT NULL COMMENT 'A=Apps, U=URL, P=Parent',
  `lapps` varchar(225) COLLATE latin1_general_ci NOT NULL COMMENT 'apps nam if llink_type = A; Parent if llink_type = Parent, Link URL if llink_type = U',
  `ltarget` varchar(20) COLLATE latin1_general_ci NOT NULL COMMENT '_self,_blank,_top,_parent',
  `limage_folder` varchar(100) COLLATE latin1_general_ci NOT NULL COMMENT 'folder name of apps images ; should be located on public_html/images/"image_folder"',
  `lnum_image` int(4) NOT NULL COMMENT 'number of images',
  `ldimensions` varchar(100) COLLATE latin1_general_ci NOT NULL COMMENT 'dimension of each images, separated by ; example : 800;600;400;300;200;100',
  `lview` smallint(6) NOT NULL DEFAULT '0',
  `linsert` smallint(6) NOT NULL DEFAULT '0',
  `ledit` smallint(6) NOT NULL DEFAULT '0',
  `ldelete` smallint(6) NOT NULL DEFAULT '0',
  `lorder_id` int(11) NOT NULL DEFAULT '1',
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `ldlu` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lmodule_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=271 ;

--
-- Dumping data for table `lumonata_module`
--

INSERT INTO `lumonata_module` (`lmodule_id`, `lname`, `lparent_id`, `lfolder`, `ltitle`, `llink_type`, `lapps`, `ltarget`, `limage_folder`, `lnum_image`, `ldimensions`, `lview`, `linsert`, `ledit`, `ldelete`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(6, 'Home', 0, '', 'Home', 'U', 'http://localhost/macadipan/lumonata-admin/home.php', '_self', '', 0, '', 1, 0, 0, 0, 266, 'admin', 1275548086, 'admin', 1224237969),
(7, 'Site', 0, '', 'Site', 'P', 'Parent', '_self', '', 0, '', 1, 0, 0, 0, 265, '', 0, 'admin', 1216973424),
(8, 'Global Setting', 7, '', 'Global Setting', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=8', '_self', '', 0, '', 1, 0, 1, 0, 264, 'admin', 1275548086, 'admin', 1218431866),
(9, 'Menu', 7, 'Default', 'Menu', 'A', 'menu', '_self', '', 0, '', 1, 1, 1, 1, 263, 'admin', 1299827335, 'admin', 1218432594),
(10, 'User Management', 7, 'Default', 'User Management', 'P', 'Parent', '_self', '', 0, '', 1, 0, 0, 0, 262, '', 0, 'admin', 1218432511),
(11, 'User Type', 10, 'Default', 'User Type', 'A', 'usertype', '_self', '', 0, '', 1, 1, 1, 1, 261, '', 0, 'admin', 1218432511),
(12, 'User Privilege', 10, 'Default', 'User Privilege', 'A', 'userPrivilege', '_self', '', 0, '', 1, 1, 1, 1, 260, '', 0, 'admin', 1218432511),
(13, 'Login Manager', 10, 'Default', 'Login Manager', 'A', 'loginManager', '_self', '', 0, '', 1, 1, 1, 1, 259, '', 0, 'admin', 1218432594),
(14, 'Preview Website', 7, '', 'Preview Website', 'U', '../', '_blank', '', 0, '', 1, 0, 0, 0, 258, 'admin', 1275548086, 'admin', 1216973424),
(16, 'Dynamic Pages', 61, '', 'Dynamic Pages', 'P', 'Parent', '_self', 'Dynamic_Pages', 2, '300;225;175;130', 0, 0, 0, 1, 205, 'admin', 1303564195, 'admin', 1224495926),
(2, 'File Manager', 0, 'Default', 'File Manager', 'A', 'fileManager', '_self', '', 0, '', 0, 1, 1, 1, 224, '', 0, 'admin', 1224134404),
(5, 'Logout', 0, '', 'Logout', 'U', 'http://localhost/macadipan/lumonata-admin/functions/logout.php', '_self', '', 0, '', 1, 1, 1, 1, 220, 'admin', 1275548086, 'admin', 1224237969),
(1, 'Module', 0, 'Default', 'Module', 'A', 'module', '_self', '', 0, '', 0, 1, 1, 1, 224, '', 0, 'admin', 0),
(4, 'Apps Settings', 0, 'Default', 'Apps Settings', 'A', 'appsSettings', '_self', '', 0, '', 0, 1, 1, 1, 200, 'admin', 1231473004, '', 0),
(61, 'Applications', 0, '', 'Applications', 'P', 'Parent', '_self', '', 0, '', 1, 1, 1, 1, 227, 'admin', 1224494198, 'admin', 1224503869),
(62, 'Static Pages', 61, '', 'Static Pages', 'P', 'Parent', '_self', '', 0, '', 1, 1, 1, 1, 204, 'admin', 1231473164, 'admin', 1224495926),
(63, 'Settings', 62, '', 'Static Pages Settings', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=62', '_self', 'Static_Pages', 2, '618;395;175;130', 1, 1, 1, 1, 205, 'admin', 1275548086, 'admin', 1224500168),
(64, 'Categories', 16, 'Dynamic_Pages', 'Dynamic Pages Categories', 'A', 'dynamicPagesCategories', '_self', '', 0, '', 1, 1, 1, 1, 205, 'admin', 1224494602, 'admin', 1224577987),
(65, 'Details', 16, 'Dynamic_Pages', 'Dynamic Pages Details', 'A', 'dynamicPages', '_self', 'Dynamic_Pages', 2, '300;225;175;130', 1, 1, 1, 1, 204, 'admin', 1231769200, 'admin', 1224577809),
(66, 'Settings', 16, '', 'Dynamic Pages Settings', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=16', '_self', '', 0, '', 1, 1, 1, 1, 206, 'admin', 1275548086, 'admin', 1224500129),
(3, 'Settings', 0, 'Default', 'Settings', 'A', 'settings', '_self', '', 0, '', 0, 1, 1, 1, 203, 'admin', 1231473004, 'admin', 0),
(68, 'Details', 62, 'Static_Pages', 'Static Pages Details', 'A', 'staticPages', '_self', 'Static_Pages', 2, '618;395;175;130', 1, 1, 1, 1, 204, 'admin', 1236413934, 'admin', 1224500284),
(235, 'Activity', 61, 'Accommodations', 'Activity', 'A', 'accommodationActivity', '_self', '', 0, '', 0, 0, 0, 1, 86, 'admin', 1303564194, '', 0),
(236, 'Destination', 185, 'Accommodations', 'Destination', 'A', 'accommodationCategories', '_self', 'Destination', 1, '340;300', 0, 0, 0, 1, 34, 'admin', 1303564194, '', 0),
(237, 'Room Type', 185, 'Accommodations', 'Accomodation Room Type', 'A', 'accommodationType', '_self', 'Accommodations', 2, '190;142;764;405', 1, 1, 1, 1, 33, 'admin', 1303561945, '', 0),
(238, 'Season Period', 185, '', 'Season Period', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=season&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 31, 'admin', 1303562098, '', 0),
(159, 'Images', 0, 'Galleries', 'Images', 'A', 'images', '_self', '', 2, '600;450;210;316', 0, 1, 1, 1, 112, 'admin', 1254453205, '', 0),
(161, 'Videos', 0, 'Galleries', 'Videos', 'A', 'videos', '_self', '', 2, '530;350;175;130', 0, 1, 1, 1, 110, 'admin', 1231873921, '', 0),
(185, 'Accommodations', 61, 'Accommodations', 'Accommodations', 'A', 'accommodation', '_self', 'Accommodations', 2, '595;446;245;165', 1, 1, 1, 1, 86, 'admin', 1303564308, '', 0),
(186, 'Setting', 185, '', 'Accommodations Setting', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=185', '_self', '', 0, '', 1, 1, 1, 1, 85, 'admin', 1289453588, '', 0),
(191, 'Reservations', 185, 'Accommodations', 'Accommodations Reservations', 'A', 'accommodationBooking', '_self', '', 0, '', 0, 0, 0, 1, 78, 'admin', 1303564194, '', 0),
(210, 'Destination', 61, 'Accommodations', 'Destination', 'A', 'accommodationCategories', '_self', 'Destination', 4, '595;446;400;300;200;100;100;50', 0, 0, 0, 1, 87, 'admin', 1303564195, '', 0),
(239, 'Gallery', 61, '', 'Gallery', 'P', 'Parent', '_self', '', 0, '', 1, 1, 1, 1, 31, 'admin', 1289540488, '', 0),
(240, 'Settings', 239, '', 'Apps Settings', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=239', '_self', '', 0, '', 1, 1, 1, 1, 30, 'admin', 1289541516, '', 0),
(241, 'Category', 239, 'Photos', 'Gallery Category', 'A', 'photosCategories', '_self', 'Gallery', 2, '190;142;190;142', 1, 1, 1, 1, 29, 'admin', 1299059332, '', 0),
(242, 'Currency', 61, 'Currency', 'Currency', 'A', 'currency', '_self', '', 0, '', 0, 0, 0, 1, 1037, 'admin', 1303563807, '', 0),
(243, 'Management', 61, '', 'Management', 'P', 'Parent', '_self', '', 0, '', 0, 0, 0, 1, 27, 'admin', 1303564194, '', 0),
(244, 'Alert & Label', 7, 'Management', 'Alert & Label Management', 'A', 'alert', '_self', 'Alert', 1, '1024;768;', 1, 1, 1, 1, 260, 'admin', 1311057533, '', 0),
(245, 'Photo', 239, 'Photos', 'Photo Gallery', 'A', 'photos', '_self', 'Gallery', 2, ' 550;412;190;142', 1, 1, 1, 1, 25, 'admin', 1299059175, '', 0),
(246, 'Testimonials', 61, 'Testimonials', 'Testimonials', 'A', 'testimonial', '_self', 'Testimonials', 2, '220;139;66;67', 0, 0, 0, 1, 24, 'admin', 1303564194, '', 0),
(247, 'Rate', 185, '', 'Rate', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=rate&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 23, 'admin', 1303562029, '', 0),
(248, 'Booking', 61, 'Accommodations', 'Booking', 'A', 'accommodationBooking', '_self', '', 0, '', 1, 1, 1, 1, 22, 'admin', 1308961219, '', 0),
(227, 'Country', 7, 'Default', 'Country', 'A', 'country', '_self', '', 0, '', 1, 1, 1, 1, 261, 'admin', 1303563753, '', 0),
(231, 'Special Offer', 61, 'Special_Offer', 'Special Offer', 'A', 'specialOffer', '_self', 'Special_Offer', 1, '220;139', 1, 1, 1, 1, 39, 'admin', 1310982085, '', 0),
(234, 'Language', 7, 'Language', 'Language', 'A', 'language', '_self', 'Language', 1, '20;20', 1, 0, 1, 0, 259, 'admin', 1303563656, '', 0),
(249, 'Images', 185, 'Accommodations', 'Accommodation Images', 'A', 'accommodationImages', '_self', 'Accommodations', 2, '800;600;220;150;', 1, 1, 1, 1, 32, 'admin', 1302194977, '', 0),
(250, 'Details', 253, 'Accommodations', 'Additional Service Detail', 'A', 'accommodationAdditionalService', '_self', '', 0, '', 1, 1, 1, 1, 20, 'admin', 1302591707, '', 0),
(251, 'Packages', 253, 'Accommodations', 'Additional Service Packages', 'A', 'accommodationAdditionalServicePackage', '_self', '', 0, '', 1, 1, 1, 1, 18, 'admin', 1302591820, '', 0),
(252, 'Images', 253, 'Accommodations', 'Additional Service Images', 'A', 'accommodationAdditionalServiceImages', '_self', 'Additional_Services', 2, '800;600;220;150;', 1, 1, 1, 1, 19, 'admin', 1302595424, '', 0),
(253, 'Additional Services', 61, '', 'Aditional Services', 'P', 'Parent', '_self', '', 0, '', 1, 1, 1, 1, 82, 'admin', 1302591651, '', 0),
(254, 'Settings', 253, '', 'Additional Service Settings', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=253', '_self', '', 0, '', 1, 1, 1, 1, 21, 'admin', 1302595017, '', 0),
(255, 'Career', 61, 'Career', 'Career', 'A', 'career', '_self', '', 0, '', 1, 1, 1, 1, 16, 'admin', 1302626058, '', 0),
(256, 'Surcharge Description', 185, 'Accommodations', 'Surcharge Description', 'A', 'accommodationSurechargeDesc', '_self', '', 0, '', 1, 1, 1, 1, 15, 'admin', 1303192859, '', 0),
(257, 'Surcharge', 185, '', 'Surcharge', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=surcharge&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 14, 'admin', 1303562602, '', 0),
(258, 'Discount Promo', 185, '', 'Discount Promo', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=discount-promo&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 13, 'admin', 1303562713, '', 0),
(259, 'Early Bird Promo', 185, '', 'Early Bird Promo', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=early-bird&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 12, 'admin', 1303562783, '', 0),
(260, 'Availability Calendar', 185, '', 'Availability Calendar', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=allotment&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 11, 'admin', 1303603336, '', 0),
(261, 'Cancellation Policy', 185, '', 'Cancellation Policy', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=cancellation-policy&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 9, 'admin', 1303603336, '', 0),
(262, 'Cancellation', 185, '', 'Cancellation', 'U', 'http://localhost/macadipan/lumonata-admin/functions/direct.php?type=supplier&apps=cancellation&pro_id=1&pro_type=2&mod=237', '_self', '', 0, '', 1, 1, 1, 1, 8, 'admin', 1303603336, '', 0),
(263, 'Cancellation Policy Description', 185, 'Accommodations', 'Cancellation Policy Description', 'A', 'accommodationCancelationPolicyMaster', '_self', '', 0, '', 1, 1, 1, 1, 10, 'admin', 1303603336, '', 0),
(264, 'Newsletter', 61, '', 'Newsletter', 'P', 'Parent', '_self', '', 0, '', 1, 1, 1, 1, 7, 'admin', 1305535784, '', 0),
(265, 'Settings', 264, '', 'Newsletter Settings', 'U', 'http://localhost/macadipan/lumonata-admin/home.php?mod=4&prc=view&d=264', '_self', '', 0, '', 1, 1, 1, 1, 6, 'admin', 1309325288, '', 0),
(266, 'Member Categories', 264, 'Newsletter', 'Newsletter Member Categories', 'A', 'membersCategories', '_self', '', 0, '', 1, 1, 1, 1, 5, 'admin', 1305536883, '', 0),
(267, 'Member Details', 264, 'Newsletter', 'Newsletter Member Details', 'A', 'members', '_self', '', 0, '', 1, 1, 1, 1, 4, 'admin', 1305537151, '', 0),
(268, 'Sender', 264, 'Newsletter', 'Newsletter Sender', 'A', 'newsSender', '_self', '', 0, '', 1, 1, 1, 1, 3, 'admin', 1305537203, '', 0),
(269, 'News', 264, 'Newsletter', 'Newsletter News', 'A', 'news', '_self', '', 0, '', 1, 1, 1, 1, 2, 'admin', 1305537258, '', 0),
(270, 'Banners', 7, 'Banners', 'Banners', 'A', 'banners', '_self', 'Banners', 1, '1024;768;', 1, 1, 1, 1, 261, 'admin', 1311314467, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_notifications`
--

CREATE TABLE IF NOT EXISTS `lumonata_notifications` (
  `lnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`lnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_order`
--

CREATE TABLE IF NOT EXISTS `lumonata_order` (
  `lorder_id` varchar(50) NOT NULL,
  `lmember_id` varchar(50) NOT NULL,
  `lorder_shipping_id` varchar(50) NOT NULL,
  `lorder_shipping_status` int(5) NOT NULL COMMENT '1=difference;0=same;',
  `lorder_date` datetime NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '1=Pending; 2=Paid, 3=Cancelled, 4=Partially Shipped,5=Shipped',
  `lstatus_archive` int(11) NOT NULL,
  `litem_total` int(10) NOT NULL,
  `lquantity_total` int(10) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lshipping_option` varchar(255) NOT NULL,
  `lshipping_price` decimal(10,2) NOT NULL,
  `tax_product` decimal(10,2) NOT NULL,
  `tax_shipping` decimal(10,2) NOT NULL,
  `lprice_total` decimal(10,2) NOT NULL,
  `lpayment_method` varchar(60) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`lorder_id`,`llang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_order_detail`
--

CREATE TABLE IF NOT EXISTS `lumonata_order_detail` (
  `ldetail_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `lorder_id` varchar(50) NOT NULL,
  `lproduct_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lqty` int(25) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(100) NOT NULL,
  `ldlu` int(50) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`ldetail_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=682 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_order_detail_material`
--

CREATE TABLE IF NOT EXISTS `lumonata_order_detail_material` (
  `ldetail_material_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `ldetail_id` varchar(50) NOT NULL,
  `lstatus` smallint(5) NOT NULL COMMENT '1=material;2=product;3=Reorder as Print ;4=Reorder as Other Product ;5=Reorder ;',
  `lproduct_id` int(11) NOT NULL,
  `product_variant` text NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lqty` int(25) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(100) NOT NULL,
  `ldlu` int(50) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`ldetail_material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=682 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_order_payment`
--

CREATE TABLE IF NOT EXISTS `lumonata_order_payment` (
  `lpayment_id` int(11) NOT NULL AUTO_INCREMENT,
  `lorder_id` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lstatus` tinyint(10) NOT NULL,
  `lpayment_method` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lpaid` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lname_paid` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldestination_bank` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `laccount_number` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldate_trans` int(11) NOT NULL,
  `lipn` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `lfile` text COLLATE latin1_general_ci NOT NULL,
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `payer_email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`lpayment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=338 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_order_shipping`
--

CREATE TABLE IF NOT EXISTS `lumonata_order_shipping` (
  `lorder_shipping_id` varchar(50) NOT NULL,
  `lfname` varchar(300) NOT NULL,
  `llname` varchar(200) NOT NULL,
  `lemail` varchar(300) NOT NULL,
  `lphone` varchar(50) NOT NULL,
  `lphone2` varchar(50) NOT NULL,
  `lcountry_id` int(10) NOT NULL,
  `lshipping_price` decimal(10,2) NOT NULL,
  `lregion` varchar(255) NOT NULL,
  `lcity` varchar(255) NOT NULL,
  `laddress` varchar(255) NOT NULL,
  `laddress2` varchar(255) NOT NULL,
  `laddress3` varchar(255) NOT NULL,
  `lpostal_code` varchar(100) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`lorder_shipping_id`,`llang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lumonata_order_shipping`
--

INSERT INTO `lumonata_order_shipping` (`lorder_shipping_id`, `lfname`, `llname`, `lemail`, `lphone`, `lphone2`, `lcountry_id`, `lshipping_price`, `lregion`, `lcity`, `laddress`, `laddress2`, `laddress3`, `lpostal_code`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES
('OS121000003', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000003', '2012-10-30 18:05:13', 'MI121000003', '2012-10-30 18:05:13', 0),
('OS121000004', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000004', '2012-10-30 18:05:19', 'MI121000004', '2012-10-30 18:05:19', 0),
('OS121000005', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000005', '2012-10-30 18:05:21', 'MI121000005', '2012-10-30 18:05:21', 0),
('OS121000006', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000006', '2012-10-30 18:05:21', 'MI121000006', '2012-10-30 18:05:21', 0),
('OS121000007', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000007', '2012-10-30 18:05:23', 'MI121000007', '2012-10-30 18:05:23', 0),
('OS121000008', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000008', '2012-10-30 18:05:23', 'MI121000008', '2012-10-30 18:05:23', 0),
('OS121000009', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000009', '2012-10-30 18:05:45', 'MI121000009', '2012-10-30 18:05:45', 0),
('OS121000010', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000010', '2012-10-30 18:05:47', 'MI121000010', '2012-10-30 18:05:47', 0),
('OS121000011', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000011', '2012-10-30 18:05:50', 'MI121000011', '2012-10-30 18:05:50', 0),
('OS121000012', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000012', '2012-10-30 18:05:51', 'MI121000012', '2012-10-30 18:05:51', 0),
('OS121000013', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000013', '2012-10-30 18:05:53', 'MI121000013', '2012-10-30 18:05:53', 0),
('OS121000014', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000014', '2012-10-30 18:05:53', 'MI121000014', '2012-10-30 18:05:53', 0),
('OS121000015', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000015', '2012-10-30 18:05:53', 'MI121000015', '2012-10-30 18:05:53', 0),
('OS121000016', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000016', '2012-10-30 18:05:54', 'MI121000016', '2012-10-30 18:05:54', 0),
('OS121000017', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000017', '2012-10-30 18:05:54', 'MI121000017', '2012-10-30 18:05:54', 0),
('OS121000018', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000018', '2012-10-30 18:05:55', 'MI121000018', '2012-10-30 18:05:55', 0),
('OS121000019', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000019', '2012-10-30 18:05:55', 'MI121000019', '2012-10-30 18:05:55', 0),
('OS121000020', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000020', '2012-10-30 18:05:56', 'MI121000020', '2012-10-30 18:05:56', 0),
('OS121000021', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000021', '2012-10-30 18:05:56', 'MI121000021', '2012-10-30 18:05:56', 0),
('OS121000022', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000022', '2012-10-30 18:05:56', 'MI121000022', '2012-10-30 18:05:56', 0),
('OS121000023', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000023', '2012-10-30 18:05:57', 'MI121000023', '2012-10-30 18:05:57', 0),
('OS121000024', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000024', '2012-10-30 18:05:57', 'MI121000024', '2012-10-30 18:05:57', 0),
('OS121000025', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000025', '2012-10-30 18:05:57', 'MI121000025', '2012-10-30 18:05:57', 0),
('OS121000027', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000027', '2012-10-30 18:09:14', 'MI121000027', '2012-10-30 18:09:14', 0),
('OS121000028', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000028', '2012-10-30 18:09:16', 'MI121000028', '2012-10-30 18:09:16', 0),
('OS121000029', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000029', '2012-10-30 18:09:16', 'MI121000029', '2012-10-30 18:09:16', 0),
('OS121000030', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000030', '2012-10-30 18:09:17', 'MI121000030', '2012-10-30 18:09:17', 0),
('OS121000031', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000031', '2012-10-30 18:09:18', 'MI121000031', '2012-10-30 18:09:18', 0),
('OS121000032', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000032', '2012-10-30 18:09:19', 'MI121000032', '2012-10-30 18:09:19', 0),
('OS121000033', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000033', '2012-10-30 18:09:24', 'MI121000033', '2012-10-30 18:09:24', 0),
('OS121000034', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000034', '2012-10-30 18:09:24', 'MI121000034', '2012-10-30 18:09:24', 0),
('OS121000035', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000035', '2012-10-30 18:09:24', 'MI121000035', '2012-10-30 18:09:24', 0),
('OS121000036', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000036', '2012-10-30 18:09:24', 'MI121000036', '2012-10-30 18:09:24', 0),
('OS121000037', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000037', '2012-10-30 18:09:35', 'MI121000037', '2012-10-30 18:09:35', 0),
('OS121000038', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000038', '2012-10-30 18:09:35', 'MI121000038', '2012-10-30 18:09:35', 0),
('OS121000039', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000039', '2012-10-30 18:09:35', 'MI121000039', '2012-10-30 18:09:35', 0),
('OS121000040', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000040', '2012-10-30 18:09:35', 'MI121000040', '2012-10-30 18:09:35', 0),
('OS121000041', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000041', '2012-10-30 18:09:36', 'MI121000041', '2012-10-30 18:09:36', 0),
('OS121000042', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000042', '2012-10-30 18:09:36', 'MI121000042', '2012-10-30 18:09:36', 0),
('OS121000043', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361121221', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000043', '2012-10-30 18:09:36', 'MI121000043', '2012-10-30 18:09:36', 0),
('OS121000048', 'Wahya', 'Biantara', 'gwahya@gmail.com', '754926', '', 103, '8.00', 'Bali', 'Bali', 'Jl Raya Kuta 127', '', '', '80361', 'MI121000048', '2012-10-30 18:56:17', 'MI121000048', '2012-10-30 18:56:17', 0),
('OS121000050', 'Purwa', 'Suajaya', 'purwa@lumonata.com', '0361987654', '', 103, '8.00', 'Bali', 'Kuta', 'Jalan Raya Kuta 127', '', '', '80361', 'MI121000050', '2012-10-30 21:08:28', 'MI121000050', '2012-10-30 21:08:28', 0),
('OS121100004', 'Putu', 'Lastari', 'putulastari@gmail.com', '081827236376', '', 103, '8.00', 'Bali', 'Denpasar', 'Kasih Ibu Hospital', '', '', '80361', 'MI121000054', '2012-11-01 10:37:26', 'MI121000054', '2012-11-01 10:37:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_program_package`
--

CREATE TABLE IF NOT EXISTS `lumonata_program_package` (
  `lpp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lrule_id` bigint(20) DEFAULT NULL,
  `lprogram_id` bigint(20) DEFAULT NULL,
  `lacco_type_id` bigint(20) DEFAULT NULL,
  `lsingle_occupancy_price` decimal(6,2) DEFAULT NULL,
  `ldouble_occupancy_price` decimal(6,2) DEFAULT NULL,
  `lcreated_by` varchar(50) DEFAULT NULL,
  `lcreated_date` int(11) DEFAULT NULL,
  `lusername` varchar(50) DEFAULT NULL,
  `ldlu` int(11) DEFAULT NULL,
  PRIMARY KEY (`lpp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_relationship_villa_category_room_type`
--

CREATE TABLE IF NOT EXISTS `lumonata_relationship_villa_category_room_type` (
  `larticle_id` bigint(20) NOT NULL DEFAULT '0',
  `lacco_type_cat_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`larticle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lumonata_relationship_villa_category_room_type`
--

INSERT INTO `lumonata_relationship_villa_category_room_type` (`larticle_id`, `lacco_type_cat_id`) VALUES
(347, '1_116'),
(361, '1_116');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_rules`
--

CREATE TABLE IF NOT EXISTS `lumonata_rules` (
  `lrule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lparent` bigint(20) NOT NULL,
  `lname` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsef` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldescription` text CHARACTER SET utf8 NOT NULL,
  `lrule` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lgroup` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcount` bigint(20) NOT NULL DEFAULT '0',
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lsubsite` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'arunna',
  PRIMARY KEY (`lrule_id`),
  KEY `rules_name` (`lname`),
  KEY `sef` (`lsef`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=120 ;

--
-- Dumping data for table `lumonata_rules`
--

INSERT INTO `lumonata_rules` (`lrule_id`, `lparent`, `lname`, `lsef`, `ldescription`, `lrule`, `lgroup`, `lcount`, `lorder`, `lsubsite`) VALUES
(1, 0, 'Uncategorized', 'uncategorized', '', 'categories', 'default', 3, 224, 'arunna'),
(2, 0, 'Designer', 'designer', '', 'categories', 'global_settings', 0, 117, 'arunna'),
(3, 0, 'Entepreneurs', 'entepreneurs', '', 'categories', 'global_settings', 1, 116, 'arunna'),
(4, 0, 'Photographer', 'photographer', '', 'categories', 'global_settings', 0, 115, 'arunna'),
(5, 0, 'Programmer', 'programmer', '', 'categories', 'global_settings', 1, 114, 'arunna'),
(6, 0, 'CEO', 'ceo', '', 'tags', 'profile', 1, 113, 'arunna'),
(7, 0, 'Cooking', 'cooking', '', 'skills', 'profile', 1, 112, 'arunna'),
(13, 0, 'Sample', 'sample', '', 'categories', 'products', 2, 106, 'arunna'),
(14, 0, 'Color', 'color', '', 'variant', 'product', 0, 105, 'arunna'),
(15, 14, 'Red', 'red', '', 'variant', 'product', 0, 105, 'arunna'),
(27, 0, 'VISION', 'vision-2', '', 'categories', 'aboutus', 0, 93, 'arunna'),
(28, 0, 'Wellness Experience', 'wellness-experience', '', 'categories', 'wellness', 0, 19, 'arunna'),
(84, 28, 'Rejuvenation Programs', 'rejuvenation-programs', '<p>Are you after a program that isn&rsquo;t listed here? We can tailor a program to your specific needs, whether it&rsquo;s a short 3 day program or a comprehensive 21 day program we will endeavor to satisfy every request. Simply enquiry here and a Sukhavati representative will contact you as soon as possible.</p>', 'categories', 'wellness', 0, 29, 'arunna'),
(85, 28, 'Our Trained Wellness Specialists', 'our-trained-wellness-specialists', '', 'categories', 'wellness', 0, 30, 'arunna'),
(86, 83, 'Ayurveda Panchakarma', 'ayurveda-panchakarma', '<p>Ayurveda is the ancient system of healing originating in India thousands of years ago. In Sanskrit, &ldquo;Ayur&rdquo; means &ldquo;Life&rdquo; and &ldquo;Veda&rdquo; means &ldquo;Knowledge&rdquo; so Ayurveda is the &ldquo;Science of Life&rdquo; which is based on the profound insights and understanding of the laws of nature cognised by the sages and rishis of ancient times.<br /><br />Ayurveda has spread beyond India and is recognised and practiced worldwide as a unique, holistic and practical system of health.</p>', 'categories', 'wellness', 0, 20, 'arunna'),
(116, 0, 'Default Room Type', 'default-room-type', '', 'room_type', 'villas', 2, 4, 'arunna'),
(117, 0, 'Test Room Type', 'test-room-type', '', 'room_type', 'villas', 0, 3, 'arunna'),
(60, 0, 'Facilities', 'facilities', '', 'categories', 'facilities', 2, 60, 'arunna'),
(61, 0, 'Cuisine', 'cuisine', '<p>Our gourmet chefs offer a unique experience of beautiful fresh vegetarian food and a range of specialised Ayurvedic dishes. The freshest ingredients are sourced from local growers and markets and also seasonally grown on our own property using organic farming methods. The sattvic quality of food will promote and support the effectiveness of the purifying treatments of Ayurveda. It is important that the digestive fire is strong for food to be well metabolized. Our chefs are vigilant to the healing properties of the food they prepare and to the healing needs of the guests at each stage of the program throughout your stay with us. Good simple food is an essentail part of the rejuvenation programs at Sukhavati.</p>', 'categories', 'cuisine', 0, 59, 'arunna'),
(62, 0, 'Gallery ', 'gallery', '', 'categories', 'gallery', 0, 56, 'arunna'),
(63, 62, 'The Villas', 'the-villas', '', 'categories', 'gallery', 0, 53, 'arunna'),
(64, 62, 'The Estate', 'the-estate', '', 'categories', 'gallery', 0, 54, 'arunna'),
(65, 62, 'The Ayurvedic Spa', 'the-ayurvedic-spa', '', 'categories', 'gallery', 0, 55, 'arunna'),
(100, 28, 'Wellness Experience', 'wellness-experience-2', '<p class="Body">At Sukhavati our goal is to provide the most comprehensive program of healing and wellness.</p>\r\n<p>On arrival to the Sukhavati Estate you will receive a comprehensive personal consultation with our Ayurvedic Doctor that will assess all aspects of your health. A specialised treatment program will be designed in conjunction with your needs and the length of your stay.</p>\r\n<p>Our vision of wellness includes comprehensive spa treatments, Ayurveda, daily programs of yoga, specialised healing diets and techniques of meditation along with daily consultations and education programs.</p>\r\n<p>The estate is a place of healing with art, sculpture, music and aroma therapy all coming together to create an experience of wholeness.</p>', 'categories', 'wellness', 0, 18, 'arunna'),
(83, 28, 'Wellness Treatments', 'wellness-treatments', '', 'categories', 'wellness', 0, 28, 'arunna'),
(88, 83, 'Balinese healing modalities', 'balinese-healing-modalities-2', '<p><span style="text-decoration: underline;">Yoga Massage</span></p>\r\n<p>Our spa Manager Wayan is gifted in the art of Yoga massage. Originating from Thailand, this massage and muscle manipulation technique is influenced by Ayurveda and Yoga.</p>\r\n<p>The massage recipient lies on a mat or firm mattress on the floor and then is positioned into a variety of yoga-like positions in deep, static and rhythmic pressures during the course of the massage. A light pressure is applied during the deep stretching of the massage.</p>\r\n<p>Yoga massage is a deeply healing experience as energy blockages are released. Yoga massage puts the receiver in a deep state of relaxation as it frees the flow of vital energy in the body. It is wonderful for improving posture, breathing, flexibility, digestion and circulation. It is beneficial for relieving arthritic conditions, muscles issues, joint problems and nervous tension. It also symptomatically relives headaches, back pain, digestive disorders, neck and shoulder tension, menstrual problems and acute stress.</p>\r\n<p>&nbsp;</p>\r\n<p><span style="text-decoration: underline;">Acupuncture and Acupressure</span></p>\r\n<p>Local Balinese healer Joko is a talented acupuncturist.</p>\r\n<p>An initial consultation will involve a conversation about your condition, how it started and your overall health. A physical examination including checking parts of your body, feeling your pulse and looking at your tongue is completed to discover energetic blockages in your system that are the root cause of your health concern. Treatment involves a firm full body massage to release any tension, then the application of pressure or dry needling along specific energy channels to release the stagnation, promote healing and the free flow of Qi (energy).</p>\r\n<p>Acupuncture and acupressure work beautifully with Ayurveda and Yoga at restoring balance to the deepest level of your being. Beneficial for muscle tension, pain, menstrual issues, digestive issue, stiff joints, anxiety and arthritis.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style="text-decoration: underline;">Balinese Massage</span></p>\r\n<p>Balinese massage is a massage technique developed in Bali, with influence from Ayurveda and other traditional medicines of China and Southeast Asia.</p>\r\n<p>The technique involves acupressure, skin rolling and flicking, firm and gentle stroking, percussion and application of essential oils. The practitioner may also incorporate stone massage. The intention is to loosen muscle tightness, stimulate the lymphatic system to enhance detoxification and increase the flow of blood and energy. &nbsp;</p>\r\n<p>The massage leaves your feeling relax, supple and restored.</p>\r\n<p>&nbsp;</p>\r\n<p><span style="text-decoration: underline;">Ibu Jero</span></p>\r\n<p>Fifth generation Balinese priestess.</p>\r\n<p>She is a priestess with a pure heart&nbsp;and mind that offers traditional Balinese healing therapies using an integrated Western approach. Ibu Jero understands how negative energies can debilitate when held within a person. These feelings lead to mental, emotional and physical imbalances that can be difficult to let go and shake off.</p>\r\n<p>These are the types of &lsquo;healings&rsquo; you can receive:</p>\r\n<p>Energy Cleansing Session</p>\r\n<p>Negative thoughts and energy can become lodged, stuck and carried in a person after they have come into contact with them unwittingly or through some trauma in this or a past life.&nbsp; Ibu Jero locates and clears these energies in the Cleansing Session, leaving you free to experience life as you, not as the host of negative energies which limit your experience of your life.</p>\r\n<p>Balinese Shamanic Massage</p>\r\n<p>The Balinese Shamanic Massage Ibu Jero performs releases cellular programming responsible for the&nbsp;harm caused by energies from the past that are affecting a person in the present. This energy takes hold,&nbsp;leaving the recipient or carrier not feeling like themselves. Symptoms can include erratic behaviour, mood swings, personality changes, exhaustion and open negativity toward others. All of&nbsp;these symptoms operate to destroy the natural harmony within that person, and need to be cleared to restore balance and health.</p>', 'categories', 'wellness', 0, 22, 'arunna'),
(89, 83, 'Detox ', 'detox-2', '<p>Ayurveda recommends that in order to maintain optimal levels of vitality, clarity and youthfulness, it is important to internally purify the mind and body.</p>\r\n<p>Detoxing or removing toxic build up from the body is beneficial in a number of ways. It creates balance in the body which helps it to function efficiency, it boosts the immune system, improves digestion, improves clarity of mind, reduces stress, regulates weight, increases energy, and creates beautiful glowing skin, but most importantly, it is removing the toxic material that causes us to get unwell, so is a great preventative health routine.&nbsp;</p>\r\n<p>Detox is a natural part of the body&rsquo;s process. The body has its own mechanisms to get rid of waste, neutralize toxins and excrete them from the system via the eliminatory channels (skin, bowels, liver, kidneys and lungs) to ensure healthy tissues and that everything runs smoothly. Sometimes when we burden our body with poor food, high sugar, alcohol and late nights, these mechanisms slow down and don&rsquo;t remove the toxins efficiently.&nbsp;</p>\r\n<p>We are now more than ever also exposed to toxins everywhere in our environment. From the additives and preservatives in the food we eat, the air we breathe, the water we drink, to the lotions we put on our skin. Even the negative thoughts, emotional disturbances and stress we have all create free radicals and toxins in our body. All of these start to build up, disturb our mind, body and eventually create systemic toxicity which is the breeding ground for all disease.&nbsp;</p>\r\n<p>According to Ayurveda, good health is dependent on our capability to fully process and metabolise our nutritional, emotional and sensory information that we take in from the external world. This is dependent on our internal fire, known as Agni. When we have robust, strong Agni, we create healthy tissues, eliminate waste efficiently, and have sound sleep and a peaceful mind. These are the basic pillars of good health. If our Agni is weakened, our digestive system is compromised and the food we eat is turned into undigested food waste or &lsquo;toxins&rsquo;. This undigested food is called Ama in Ayurveda. Ama is a cold, thick, sticky, mucous type substance that accumulates in our digestive tract, sticks to the walls then dampens the digestive fire even further. Eventually this Ama creates more toxins and absorbs into our bloodstream. There is circulates throughout the body, eventually accumulating in the weaker areas and causing blockages, stagnation in our channel, tissues and organs.</p>\r\n<p>The Ama starts to coat all of our cells which interrupts cellular communication, disturbs the immune function and blocks the flow of energy, nutrition and information. Ama is the basis for all disease and imbalance in the body.</p>\r\n<p>Symptoms and signs of toxicity:</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Recurrent headaches</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Skin rashes/ irritation - eczema, dermatitis, psoriasis</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Muscles aching, fibromyalgia</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nerve pain or numbness</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Recurrent infections or cold and flus/ decrease immune function</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Poor short term memory and concentration</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sensitivity to environmental chemicals,</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fatigue, malaise</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fear, worry, anxiety, insomnia, mood swings, dull mind</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IBS, constipation, sluggish digestion, bad breath</p>\r\n<p>&nbsp;</p>\r\n<p>Detoxifying or purifying the body through Sukhavati&rsquo;s Panchakarma is a way to create optimal health and vitality by gently and effectively clearing accumulated Ama, toxins, and deep rooted stress from the body.</p>\r\n<p>Treatment would involve removing all toxic or aggravating foods from your diet and including cleansing Ayurvedic dishes such as wholesome, vegetarian Dahl, curries and vegetables. You will receive daily Ayurvedic treatments that help liquefy and break up the accumulated Ama and toxins, while gently encouraging their removal through the digestive tract. Practicing twice daily Yoga helps to balance the energies of the body, while stimulating the lymphatic, circulatory and digestive system to facilitate toxic elimination. Meditation helps to calm and purify the nervous system to eliminate stress and worry to guide your body into a deep state of relaxation so healing can occur. &nbsp;</p>\r\n<p>For an effective, in-depth detox a 10 day - 5 week program is recommended.</p>', 'categories', 'wellness', 0, 23, 'arunna'),
(90, 83, 'Beauty treatments', 'beauty-treatments-2', '<p><span style="text-decoration: underline;">Facials</span></p>\r\n<p>Our luxurious facials are designed to purify, thoroughly cleanse, exfoliate, decongest and renew the skin. Prepared fresh for every treatment they incorporates healing spices, purifying clay and freshly blended herbs. Combined with marma point head, shoulder, feet and hand massage to create balance to the body&rsquo;s energy system, the experience will leave you feeling relaxed and rejuvenated and all levels of being.</p>\r\n<p>We tailor make our facials for specific skin conditions leaving troubled complexions soothed, balanced and refreshed.</p>\r\n<p>Combined with the in-depth purification and rejuvenation Panchakarma program overcome acne, congested skin, dry skin, eczema, psoriasis, rosacea, scarring and create beautiful, glowing youthful skin.</p>\r\n<p>&nbsp;</p>\r\n<p><span style="text-decoration: underline;">Ubtan</span></p>\r\n<p>Ubtan is a luxurious full body exfoliator that has been used as part of a beauty regime in India for thousands of years. It is used traditional in pre-wedding rituals to create youthful, glowing skin for the bride.&nbsp;</p>\r\n<p>Ubtan is a mix of different powders from grains and spices, combined together with oils to form a paste that is then rubbed over the whole body in a synchronized manner by 2 technicians.</p>\r\n<p>An Ubtan is a traditional natural version to the modern day micro-dermabrasion. Its acts by sloughing off the dead skins cells, stimulating elastin and collagen and increasing blood circulation so new cells can begin to form. This decreases the effects of aging, fine lines, pigmentation, scars and enhances skin texture. But unlike a microdermabrasion, the ingredients used in the Ubtan not only enhance the beauty of the skin, but also contain medicinal properties and are free of chemicals.</p>\r\n<p>Used in Panchakarma an Ubtan is a powerful lymphatic and circulatory stimulant, which aids in the detoxification process.&nbsp; The ingredients used are dry and rough in nature and hot in potency and when rubbed all over the body they induce the same qualities in the deeper tissues and remove excess congestion, water, stiffness and blockages from the system.</p>\r\n<p>The specific action of the massage creates friction to open up the pores, allowing the medicinal spices to penetrate and have a liquefying and scrapping action on fat tissue and stimulate the metabolism.&nbsp; Ubtan is beneficial for promoting weight loss, reducing subcutaneous fat tissues, toning the muscles and reducing cellulite. By reducing the blockages and congestion in the system (Kapha in Ayurvedic terms), stimulating lymphatic drainage and sluggish circulation, it clears the Nadis (energy channels), which allows the flow of Prana (energy) more efficiently throughout the body. This invigorates the nervous system, relieves fatigue and revitalises the body.</p>\r\n<p>The combination of the oil with the powder ensures the superficial layers do not become to dry and rough in the process and leaves the skin feeling subtle, soft and lustrous.</p>\r\n<p>&nbsp;</p>\r\n<p><span style="text-decoration: underline;">Steam/ Swedana</span></p>\r\n<p>Swedana which is also known as steaming is performed in our specially designed steam cabin. The Swedana treatment opens the pores, flushing out impurities through the sweat glands and increasing the penetration of oils and herbs used in therapy. Swedana can be performed by itself or in conjunction with other treatments that are offered at Sukhavati Ayurvedic Spa and Retreat such as Abhyanga or Pizzicchil.</p>\r\n<p>Benefits include helping with body detoxification, relieving an imbalance of doshas which may be causing health issues, releasing muscle tension, aches and pains, relieving heaviness and coldness of the body, promoting weight loss, helping keep skin clear and healthy.</p>', 'categories', 'wellness', 0, 24, 'arunna'),
(91, 83, 'Specialised weight loss programs', 'specialised-weight-loss-programs-2', '<p>Weight loss at Sukhavati is an easy and enjoyable process that incorporates mind, body and spirit. By combining the modalities of Ayurvedic therapies, Ayurvedic diet, Yoga and Meditation, weight loss is achievable, sustainable, and accompanied with increased energy, more confidence, better mobility, and a deeper appreciation of your body.</p>\r\n<p>Weight loss at Sukhavati is a holistic approach that includes discovering your unique body constitution and foods that support its natural balance, lifestyle recommendations, internal herbal medicines, and external therapies including massage and detoxifying procedures. We teach people how to eat delicious, wholesome food in a mindful way that supports digestion, metabolism and consciously nourishes the body.</p>\r\n<p>Weight loss is achieved by creating balance in your unique body type. Something that is unique to Ayurveda is that it does not have a &lsquo;one-size-fits-all&rsquo; approach to health. We know that different people respond differently to foods, exercises, daily routines, weather, and environments.</p>\r\n<p>We all have bodily humours in the body, called Doshas, but we each have a different proportion of them that gives us our own unique make up. This unique combination is known as our constitution or mind/body type, and is like our DNA. This is called our <em>Prakriti</em> in Sanskrit, and is responsible for everything from our habits, emotions, likes and dislikes to our bodily structure, reaction to foods or propensity to certain illnesses or conditions. Knowing your constitution is like having a blue print for life - each Dosha has its own &lsquo;personality&rsquo; with its respective strengths and areas of weaknesses. We are mainly a Vata (Air) type, Pitta (Fire) type or Kapha (Earth) type.</p>\r\n<p>This is quite a profound idea to realise: what&rsquo;s right for one constitution isn&rsquo;t necessarily right for the other. We don&rsquo;t all have to fit into the same box of how we should look, think and feel. This means that there is no one diet that is right for everyone. Awareness of your Dosha or constitution is the key to health and sustainable weight loss in Ayurveda as it helps you to make better choices, particularly in regard to diet and lifestyle. Upon arrival at Sukhavati you will have a consultation with our resident Doctor who will tailor make a program specific to your body type and weight loss aims, and guide and support you through this process.</p>\r\n<p>Ayurveda believes our digestive fire called &lsquo;Agni&rsquo;, is responsible for our metabolism. It is the metabolic fire that takes energy and nourishment from food and is also part of the immune system as it destroys harmful toxins and organisms. An imbalance in Agni in the tissues creates Ama (a toxic residue) and congestion in the body, contributing to decrease metabolic function, excess weight, fluid retention and the formation of excess fat tissue. The therapies of Panchakarma at Sukhavati remove Ama and other impurities from the system. They carry toxic waste from the external tissues to the digestive tract so it can be eliminated and expelled from the body.</p>\r\n<p>By cleansing and detoxifying the body this process helps to re-build the Agni, which stimulates our metabolism to burn up fat tissue and toxins at a cellular level, preventing it from re-forming. This process rebalances the body on a deeper lever so sustainable and healthy weight loss is achieved. For best and sustainable results a minimum of a 14 day program is recommended.</p>\r\n<p>&nbsp;</p>\r\n<p>Weight loss treatments:</p>\r\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ubtan:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Ubtan is a luxurious full body exfoliator that has been used as part of a beauty regime in India for thousands of years. It is used traditional in pre-wedding rituals to create youthful, glowing skin for the bride.&nbsp; Ubtan is a mix of different powders from grains and spices, combined together with oils to form a paste that is then rubbed over the whole body in a synchronized manner by 2 technicians. Used in Panchakarma an Ubtan is a powerful lymphatic and circulatory stimulant, which aids in the detoxification process.&nbsp; </li>\r\n</ul>\r\n<p>The ingredients used are dry and rough in nature and hot in potency and when rubbed all over the body they induce the same qualities in the deeper tissues and remove excess congestion, water, stiffness and blockages from the system.</p>\r\n<p>Massaged all over the body this creates feeling of lightness and creates a cutting and liquefying action on the fat tissue. The method in which it is rubbed creates friction that opens the pores, removed blockages in vessels and increases the heat in the tissues to stimulate fat metabolism.</p>\r\n<p>Benefits include reducing cellulite, decreasing subcutaneous fat tissue, creating a glowing skin complexion, cleansing the skin, creating better skin tone and decreasing body stiffness. &nbsp;</p>', 'categories', 'wellness', 0, 25, 'arunna'),
(92, 83, 'Meditation', 'meditation', '<p>Meditation is an important component to complement our Ayurvedic   program, since meditation refines the consciousness while the treatments   refine the physiology.</p>\r\n<p>We need to allow the mind to take a dive beyond the surface level of   thinking to experience the silent unbounded source of thought, the   source of all creativity and energy deep within the mind itself. When   the mind learns to transcend then finer states of a thought are   experienced, this has the natural effect of settling the mind, and the   mind comes to know the level of being without a thought.</p>\r\n<p>The Success without Stress technique is an effortless mental   technique that is easy to learn and enjoyable to practice. Every time   you meditate the body gains a profound level of rest and deep   relaxation, while the mind remains effortlessly alert. The learning   process is conducted step by step in just four sessions. In the first   session you will receive individual instruction from a qualified,   experienced teacher. The following three sessions build a deeper   understanding of the mind from the perspective of ancient Vedic   knowledge and modern science. You will learn how and why the technique   works and its practical benefits in daily life.</p>\r\n<p>Learning the Success Without Stress meditation technique is available   on specific programs when our visiting specialist is in residence.</p>\r\n<p>The benefits of Success Without Stress Meditation:</p>\r\n<ul>\r\n<li>Reduced stress, anxiety and tension</li>\r\n<li>Clear, creative, positive thinking</li>\r\n<li>Better sleep</li>\r\n<li>Increased energy and motivation</li>\r\n<li>Lowered cholesterol levels that decrease the risk of cardiovascular disease</li>\r\n<li>Reduced use of alcohol and cigarettes</li>\r\n</ul>\r\n<p>Learning the Success Without Stress meditation technique is available  on specific programs when our visiting specialist is in residence.</p>', 'categories', 'wellness', 0, 26, 'arunna'),
(93, 83, 'Yoga', 'yoga', '<p>Sukhavati incorporates daily yoga sessions to assist you in becoming   more aware of your body&rsquo;s posture, alignment and patterns of movement.</p>\r\n<p>Yoga relaxes the body and gives more suppleness. The practice of yoga   can help you recognise your hidden physical and mental potential by   creating more ease in the body and leaving the mind clear and settled.</p>\r\n<p>Through the regular daily practice of yoga postures or asanas, you   will gain flexibility and strength. This serves well in developing an   inner resilience and resourcefulness so that you can maintain poise and   inner peace even amidst the stresses and demands of your daily life  when  you return home.</p>\r\n<p>The benefits of Yoga:</p>\r\n<ul>\r\n<li>To release tension from the body</li>\r\n<li>To relieve stress</li>\r\n<li>To improve joint mobility</li>\r\n<li>To relax the mind and bring about mental clarity</li>\r\n<li>To recognise the full healing and spiritual potential of the body</li>\r\n<li>To stimulate blood flow and lymphatic fluids that increase the flow of toxins out of the body</li>\r\n<li>To improve flexibility and strength</li>\r\n<li>Pranayama (breathing technique) increases oxygen to the body, enriches the blood and calms the central nervous system</li>\r\n</ul>', 'categories', 'wellness', 0, 27, 'arunna'),
(107, 0, 'Extra', 'extra', 'Extra service', 'categories', 'additional_service', 0, 11, 'arunna'),
(108, 0, 'Pure Spa', 'pure-spa', 'spa service', 'categories', 'additional_service', 0, 11, 'arunna'),
(109, 27, 'Estate Life', 'estate-life', '<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.dsadasdasda fsadfsdfsdf.fsdfsdfsdf</span></p>', 'categories', 'aboutus', 0, 11, 'arunna'),
(112, 0, 'About Us', 'about-us', '<p>Welcome to&nbsp;<strong>Villa Vedas</strong>, a unique residence on 3,600 m2 of pristine beachfront land, located on the southwest coast of Bali, a leisurely 30 minutes drive from the nightlife and restaurants of Seminyak, and a few minutes from the world famous Tanah Lot temple.</p>', 'categories', 'articles', 3, 8, 'arunna'),
(113, 0, 'Facilities', 'facilities', '<p style="text-align: center;">The beachfront gardens feature a 40 metre infinity lap pool with separate spa cascading into a 20 metre infinity pool, a commercially fitted Art Deco styled bar and a Bale BBQ area, where guests can enjoy formal and informal meals, drinks, or simply watch the magnificent sunsets. .</p>', 'categories', 'articles', 3, 7, 'arunna'),
(114, 0, 'Bedrooms', 'bedrooms', '<p>Villa Vedas features five master bedrooms, all with ensuite bathrooms featuring carved stone bathtubs, central airconditioning, custom designed king size beds, full home automation and JBL surround sound systems.</p>', 'categories', 'articles', 1, 6, 'arunna'),
(115, 0, 'Services', 'services', '<p>The staff at Villa Vedas is trained to maintain the property in excellent condition, and to ensure a seamless experience for guests. Our team is comprised of a manager, housekeepers and groundskeepers.</p>', 'categories', 'articles', 1, 5, 'arunna'),
(119, 0, 'Technical', 'technical', '<p style="text-align: center;">Villa Vedas features many technical features that make living easier.</p>', 'categories', 'articles', 1, 1, 'arunna'),
(118, 0, 'Events', 'events', '<p>Villa Vedas is a stunning venue for events and functions. The absolute beachfront and surrounding rice paddies, coupled with the tranquility and privacy makes the villa one of the most exclusive and unique venues in Bali.</p>', 'categories', 'articles', 1, 2, 'arunna');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_rule_relationship`
--

CREATE TABLE IF NOT EXISTS `lumonata_rule_relationship` (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lapp_id`,`lrule_id`),
  KEY `taxonomy_id` (`lrule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lumonata_rule_relationship`
--

INSERT INTO `lumonata_rule_relationship` (`lapp_id`, `lrule_id`, `lorder_id`) VALUES
(1, 3, 0),
(120, 1, 0),
(121, 13, 0),
(120, 13, 0),
(359, 113, 0),
(358, 113, 0),
(357, 113, 0),
(356, 112, 0),
(161, 60, 0),
(162, 60, 0),
(349, 112, 0),
(355, 112, 0),
(347, 116, 0),
(347, 1, 0),
(360, 115, 0),
(361, 116, 0),
(361, 1, 0),
(330, 107, 0),
(331, 108, 0),
(370, 118, 0),
(371, 114, 0),
(372, 119, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_send_email_history`
--

CREATE TABLE IF NOT EXISTS `lumonata_send_email_history` (
  `lsend_id` int(11) NOT NULL DEFAULT '0',
  `lbook_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`lsend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lumonata_send_email_history`
--

INSERT INTO `lumonata_send_email_history` (`lsend_id`, `lbook_id`) VALUES
(1399344515, 1398671011);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_settings`
--

CREATE TABLE IF NOT EXISTS `lumonata_settings` (
  `lsetting_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmodule_id` int(11) NOT NULL,
  `loption_name` varchar(225) COLLATE latin1_general_ci NOT NULL,
  `loption_value` text COLLATE latin1_general_ci NOT NULL,
  `loption_title` varchar(225) COLLATE latin1_general_ci NOT NULL,
  `loption_type` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'text',
  `loption_list_value` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '0;1',
  `loption_list_label` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lvalue_type` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lvalue_min` int(3) NOT NULL DEFAULT '0',
  `lvalue_max` int(3) NOT NULL DEFAULT '225',
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lsetting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=113 ;

--
-- Dumping data for table `lumonata_settings`
--

INSERT INTO `lumonata_settings` (`lsetting_id`, `lmodule_id`, `loption_name`, `loption_value`, `loption_title`, `loption_type`, `loption_list_value`, `loption_list_label`, `lvalue_type`, `lvalue_min`, `lvalue_max`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(4, 8, 'sys_code', '1', 'Code System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 225, 102, 'admin', 1227674482, 'admin', 1311059758),
(5, 8, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 225, 103, 'admin', 1227674529, 'admin', 1311059758),
(6, 8, 'comments', '1', 'Comments', 'Combo Box', '0;1', 'Disable;Enable', '', 0, 225, 104, 'admin', 1227674615, 'admin', 1311059758),
(7, 8, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 225, 105, 'admin', 1227674757, 'admin', 1311059758),
(8, 8, 'list_front', '10', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 106, 'admin', 1227675211, 'admin', 1311059758),
(9, 8, 'list_admin', '50', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 107, 'admin', 1227675242, 'admin', 1311059758),
(10, 8, 'latest_upload', '5', 'Latest Upload ', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 108, 'admin', 1227675290, 'admin', 1311059758),
(11, 62, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 225, 96, 'admin', 1227675716, 'admin', 1231573482),
(12, 62, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 225, 97, 'admin', 1227675763, 'admin', 1231573482),
(30, 16, 'list_admin', '50', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 81, 'admin', 1231587369, 'admin', 1266315889),
(15, 8, 'email', 'info@macavillas.com;info@macavillas.com', 'Email (separate emails with commas)', 'Text', '', '', 'jsvalidate_email', 0, 1000, 112, 'admin', 1227761154, 'admin', 1311059758),
(16, 8, 'cc', '', 'CC (separate emails with commas)', 'Text', '', '', 'jsvalidate_email', 0, 1000, 113, 'admin', 1227761226, 'admin', 1311059758),
(17, 8, 'bcc', '', 'BCC <br />*(separate emails with commas)', 'Text', '', '', 'jsvalidate_email', 0, 1000, 114, 'admin', 1227761264, 'admin', 1311059758),
(19, 16, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 92, 'admin', 1227858241, 'admin', 1266315889),
(20, 16, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 91, 'admin', 1227858473, 'admin', 1266315889),
(31, 16, 'latest_upload', '10', 'Latest Upload ', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 80, 'admin', 1231587462, 'admin', 1266315889),
(28, 16, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 255, 83, 'admin', 1231587256, 'admin', 1266315889),
(29, 16, 'list_front', '2', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 82, 'admin', 1231587312, 'admin', 1266315889),
(25, 62, 'comments', '0', 'Comments', 'Combo Box', '0;1', 'Disable;Enable', '', 0, 0, 101, 'admin', 1229526458, 'admin', 1231573482),
(26, 62, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 0, 100, 'admin', 1229526636, 'admin', 1231573482),
(27, 16, 'comments', '0', 'Comments', 'Combo Box', '0;1', 'Disable;Enable', 'jsvalidate_number', 0, 0, 84, 'admin', 1231571559, 'admin', 1266315889),
(32, 137, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 79, 'admin', 1231587668, 'admin', 1231908730),
(39, 216, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 72, 'admin', 1231769974, 'admin', 1277787290),
(40, 216, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 71, 'admin', 1231770041, 'admin', 1277787290),
(35, 137, 'list_front', '10', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 76, 'admin', 1231587817, 'admin', 1231908730),
(36, 137, 'list_admin', '50', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 75, 'admin', 1231587856, 'admin', 1231908730),
(37, 62, 'sys_code', '1', 'Code System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 74, 'admin', 1231589496, '', 0),
(41, 216, 'list_admin', '15', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 70, 'admin', 1231770206, 'admin', 1277787290),
(38, 137, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 73, 'admin', 1231752891, 'admin', 1231908730),
(42, 216, 'list_front', '10', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 69, 'admin', 1231770357, 'admin', 1277787290),
(43, 159, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 68, 'admin', 1231812262, '', 0),
(44, 159, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 67, 'admin', 1231812346, '', 0),
(45, 161, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 66, 'admin', 1231845722, '', 0),
(49, 270, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 100, 62, 'admin', 1231918390, 'admin', 1311314562),
(50, 216, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 0, 72, 'admin', 1231918511, 'admin', 1277787290),
(48, 270, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 100, 63, 'admin', 1231918353, 'admin', 1311314562),
(51, 185, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 60, 'admin', 1231926114, 'admin', 1298962604),
(52, 185, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 59, 'admin', 1231926175, 'admin', 1298962604),
(53, 185, 'list_admin', '50', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 1, 1000000, 58, 'admin', 1231926227, 'admin', 1298962604),
(54, 185, 'list_front', '10', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 57, 'admin', 1231926265, 'admin', 1298962604),
(55, 211, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 0, 56, 'admin', 1231929330, 'admin', 1277785776),
(56, 211, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 55, 'admin', 1231934575, 'admin', 1277785776),
(57, 211, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 54, 'admin', 1231934619, 'admin', 1277785776),
(58, 211, 'list_admin', '10', 'List View Admin Pages', 'Text', '', '', '', 0, 1000000, 53, 'admin', 1231934693, 'admin', 1277785776),
(59, 211, 'list_front', '10', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 52, 'admin', 1231934734, 'admin', 1277785776),
(60, 210, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 51, 'admin', 1231940289, 'admin', 1289198500),
(61, 210, 'sys_thumb', '0', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 50, 'admin', 1231940324, 'admin', 1289198500),
(62, 253, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 100, 49, 'admin', 1232019009, 'admin', 1302594821),
(63, 253, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 100, 48, 'admin', 1232019205, 'admin', 1302594821),
(64, 253, 'list_admin', '5', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 5, 1000000, 47, 'admin', 1232027699, 'admin', 1302594821),
(65, 253, 'list_front', '2', 'List View Front Pages', 'Text', '', '', '', 0, 1000000, 46, 'admin', 1232027726, 'admin', 1302594821),
(66, 222, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 10, 45, 'admin', 1232082822, 'admin', 1275292673),
(67, 264, 'smtp', 'mail.macavillas.com', 'SMTP', 'Text', '', '', '', 0, 100, 44, 'admin', 1232082986, 'admin', 1309325356),
(68, 264, 'email_sum', '50', 'Sum of Emails per Send', 'Text', '', '', 'jsvalidate_digits', 5, 100, 43, 'admin', 1232083114, 'admin', 1309325151),
(69, 176, 'email_alert', 'wahya@lumonata.com', 'Email Alert when Guest join Newsletter', 'Text', '', '', 'jsvalidate_email', 0, 0, 42, 'admin', 1232083176, 'admin', 1232083249),
(82, 237, 'sys_thumb', '0', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 31, 'admin', 1289209250, 'admin', 1289453517),
(74, 231, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 100, 37, 'admin', 1276923504, '', 0),
(75, 231, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 100, 36, 'admin', 1276923550, '', 0),
(76, 231, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', 'jsvalidate_digits', 0, 100, 35, 'admin', 1276923631, '', 0),
(81, 8, 'llanguage', '1', 'Default Language', 'Combo Box', '1;2;3;5;6', 'English;Japanese;French;German;Italian', '', 0, 1000, 32, 'admin', 1288922859, 'admin', 1311059758),
(83, 237, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 30, 'admin', 1289209306, 'admin', 1289453517),
(84, 237, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 225, 29, 'admin', 1289266685, 'admin', 1289453517),
(85, 237, 'list_front', '2', 'List View Front Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 28, 'admin', 1289453374, 'admin', 1289453517),
(86, 237, 'list_admin', '1', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 27, 'admin', 1289453401, 'admin', 1289453517),
(87, 185, 'max_image_slide', '5', 'Maximum Image Slide Show', 'Text', '', '', 'jsvalidate_digits', 0, 10, 26, 'admin', 1289454000, 'admin', 1298962604),
(88, 239, 'list_admin', '50', 'List View Admin Pages', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 22, 'admin', 1289541182, 'admin', 1301505023),
(89, 239, 'list_front_album', '10', 'List View Front Pages Album', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 21, 'admin', 1289541207, 'admin', 1301505023),
(90, 239, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 1000000, 20, 'admin', 1289541279, 'admin', 1301505023),
(91, 239, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', 'jsvalidate_digits', 0, 1000000, 19, 'admin', 1289541328, 'admin', 1301505023),
(92, 234, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 1000000, 21, 'admin', 1289792657, '', 0),
(93, 244, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 1000000, 20, 'admin', 1289798978, '', 0),
(94, 245, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 1000000, 19, 'admin', 1289810998, '', 0),
(95, 245, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', 'jsvalidate_digits', 0, 1000000, 18, 'admin', 1289811061, '', 0),
(96, 239, 'list_front_photos', '1000', 'List View Front Pages Photos', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 21, 'admin', 1289811231, 'admin', 1301505023),
(97, 239, 'max_upload', '5', 'Maximum Upload', 'Text', '', '', 'jsvalidate_digits', 0, 1000000, 16, 'admin', 1289812445, 'admin', 1301505023),
(98, 246, 'publish', '1', 'Publish', 'Combo Box', '0;1', 'Uncheck publish on add new data;Auto check publish on add new data', '', 0, 0, 15, 'admin', 1289877951, '', 0),
(99, 246, 'sys_thumb', '1', 'Thumbnails System', 'Combo Box', '0;1', 'Manual;Automatic', '', 0, 0, 14, 'admin', 1289878072, '', 0),
(100, 246, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', '', 0, 0, 13, 'admin', 1289878110, '', 0),
(110, 8, 'web_name', 'Maca Villas & Spa Seminyak', 'Website Name', 'Text', '', '', '', 0, 255, 3, 'admin', 1304503097, 'admin', 1311059758),
(106, 8, 'facebook_url', 'http://www.facebook.com', 'Facebook URL', 'Text', '', '', 'jsvalidate_number', 0, 1000, 7, 'admin', 1293086903, 'admin', 1311059758),
(107, 8, 'twitter_url', 'http://www.twitter.com', 'Twitter URL', 'Text', '', '', 'jsvalidate_number', 0, 1000, 6, 'admin', 1293086973, 'admin', 1311059758),
(111, 8, 'laud', '0.929221', '1.00 USD =  ... AUD', 'Text', '', '', 'jsvalidate_number', 0, 100, 115, 'admin', 1309503491, 'admin', 1311059758),
(112, 244, 'image', '1', 'Using Image?', 'Combo Box', '0;1', 'No;Yes', 'jsvalidate_digits', 0, 100, 1, 'admin', 1311057137, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_shipping`
--

CREATE TABLE IF NOT EXISTS `lumonata_shipping` (
  `lshipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `lcountry_id` int(11) NOT NULL,
  `lshipping_option_id` int(11) NOT NULL,
  `lshipping_type` varchar(60) NOT NULL,
  `lformula` decimal(10,0) NOT NULL,
  `unit_symbol` varchar(50) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lshipping_tax` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`lshipping_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `lumonata_shipping`
--

INSERT INTO `lumonata_shipping` (`lshipping_id`, `lcountry_id`, `lshipping_option_id`, `lshipping_type`, `lformula`, `unit_symbol`, `lcurrency_id`, `lprice`, `lshipping_tax`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES
(75, 103, 1, 'weight', '4', 'Kilograms\n\n\n\n', 6, '2.00', '0.00', 12, '1', '2012-10-29 17:55:52', '1', '2012-10-29 17:55:52', 1),
(74, 103, 1, 'weight', '0', 'Kilograms\n\n\n\n', 6, '1.00', '0.00', 13, '1', '2012-10-29 17:55:26', '1', '2012-10-29 17:55:26', 1),
(72, 247, 2, 'price', '20', 'USD', 6, '5.00', '3.00', 15, '1', '2012-10-29 17:48:21', '1', '2012-10-29 17:48:21', 1),
(73, 247, 2, 'price', '60', 'USD', 6, '11.00', '5.00', 14, '1', '2012-10-29 17:49:06', '1', '2012-10-29 17:49:06', 1),
(51, 247, 1, 'price', '0', 'USD\n\n', 6, '1.00', '1.00', 36, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(52, 247, 1, 'price', '10', 'USD\n\n', 6, '2.00', '2.00', 35, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(53, 247, 1, 'price', '20', 'USD\n\n', 6, '4.00', '3.00', 34, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(55, 247, 1, 'price', '60', 'USD', 6, '9.00', '5.00', 32, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(71, 247, 2, 'price', '10', 'USD', 6, '3.00', '2.00', 16, '1', '2012-10-29 17:44:33', '1', '2012-10-29 17:44:33', 1),
(70, 247, 2, 'price', '0', 'USD', 6, '2.00', '1.00', 17, '1', '2012-10-29 17:44:11', '1', '2012-10-29 17:44:11', 1),
(83, 1, 2, 'price', '0', 'USD', 6, '2.00', '0.00', 4, '1', '2012-10-30 11:26:27', '1', '2012-10-30 11:26:27', 1),
(82, 1, 1, 'price', '10', 'USD', 6, '5.00', '0.50', 5, '1', '2012-10-30 11:26:01', '1', '2012-10-30 11:26:01', 1),
(81, 1, 1, 'price', '5', 'USD', 6, '2.00', '0.10', 6, '1', '2012-10-30 11:25:45', '1', '2012-10-30 11:25:45', 1),
(80, 1, 1, 'price', '0', 'USD', 6, '1.00', '0.00', 7, '1', '2012-10-30 11:25:30', '1', '2012-10-30 11:25:30', 1),
(79, 103, 2, 'weight', '4', 'Kilograms\n\n\n\n', 6, '3.00', '0.00', 8, '1', '2012-10-30 09:43:36', '1', '2012-10-30 09:43:36', 1),
(78, 103, 2, 'weight', '0', 'Kilograms\n\n\n\n', 6, '1.50', '0.00', 9, '1', '2012-10-30 09:43:18', '1', '2012-10-30 09:43:18', 1),
(84, 1, 2, 'price', '5', 'USD', 6, '4.00', '0.20', 3, '1', '2012-10-30 11:26:42', '1', '2012-10-30 11:26:42', 1),
(85, 1, 2, 'price', '10', 'USD', 6, '8.00', '0.50', 2, '1', '2012-10-30 11:27:01', '1', '2012-10-30 11:27:01', 1),
(86, 232, 1, 'price', '100', 'USD', 6, '0.00', '0.00', 1, '1', '2012-11-14 10:07:43', '1', '2012-11-14 10:07:43', 1),
(87, 232, 2, 'price', '100', 'USD', 6, '0.00', '0.00', 0, '1', '2012-11-14 10:08:05', '1', '2012-11-14 10:08:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_shipping_option`
--

CREATE TABLE IF NOT EXISTS `lumonata_shipping_option` (
  `lshipping_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `loption` varchar(255) NOT NULL,
  `ldesc` text NOT NULL,
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY (`lshipping_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lumonata_shipping_option`
--

INSERT INTO `lumonata_shipping_option` (`lshipping_option_id`, `loption`, `ldesc`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES
(1, 'Regular (3-5 business day)', '', 5, '', '0000-00-00 00:00:00', '', '2012-10-30 16:00:23', 0),
(2, 'Express (1-2 business day)', '', 6, '', '2012-10-29 00:00:00', '', '2012-10-29 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_special_offer`
--

CREATE TABLE IF NOT EXISTS `lumonata_special_offer` (
  `lspecial_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lcat_id` int(11) NOT NULL,
  `lacco_type_cat_id` bigint(20) NOT NULL,
  `ldescription` text NOT NULL,
  `limage` varchar(200) NOT NULL,
  `ltarget` varchar(20) NOT NULL DEFAULT '_self' COMMENT '_self,_blank,_top,_parent',
  `lwebsite` varchar(200) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lday` int(11) NOT NULL,
  `lnight` int(11) NOT NULL,
  `lperiod` int(11) NOT NULL,
  `ldate_start` int(11) NOT NULL,
  `ldate_valid` int(11) NOT NULL,
  `lstatus` int(2) NOT NULL DEFAULT '0',
  `lsef_url` varchar(100) CHARACTER SET ucs2 NOT NULL,
  `lorder` int(11) NOT NULL,
  `lpublish` smallint(6) NOT NULL DEFAULT '0',
  `lpublish_up` int(11) NOT NULL DEFAULT '0',
  `lpublish_down` int(11) NOT NULL DEFAULT '0',
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY (`lspecial_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `lumonata_special_offer`
--

INSERT INTO `lumonata_special_offer` (`lspecial_id`, `lcat_id`, `lacco_type_cat_id`, `ldescription`, `limage`, `ltarget`, `lwebsite`, `lprice`, `lday`, `lnight`, `lperiod`, `ldate_start`, `ldate_valid`, `lstatus`, `lsef_url`, `lorder`, `lpublish`, `lpublish_up`, `lpublish_down`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(84, 2, 105, 'dsd', '84.', '_self', '', '3.00', 2, 2, 1, 1397232000, 1397577600, 1, '', 0, 1, 1397836800, 1398009600, 'admin', 1397180871, 'admin', 1400061446),
(89, 2, 105, 'ds', '89.', '_self', '', '34.00', 1, 2, 12, 1398268800, 1398009600, 1, '', 0, 1, 1397664000, 1397750400, 'admin', 1397200169, 'admin', 1400061446),
(91, 2, 104, 'dasdad', '91.', '_self', '', '3.00', 1, 2, 4, 1399996800, 1400169600, 1, '', 0, 1, 1400083200, 1400256000, 'admin', 1400061282, 'admin', 1400061446);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_special_offer_additional_services`
--

CREATE TABLE IF NOT EXISTS `lumonata_special_offer_additional_services` (
  `lspecial_id` int(11) NOT NULL,
  `lpackage_id` varchar(20) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lqty` int(11) NOT NULL,
  `llday` int(11) NOT NULL,
  `lduration` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lumonata_special_offer_additional_services`
--

INSERT INTO `lumonata_special_offer_additional_services` (`lspecial_id`, `lpackage_id`, `lprice`, `lqty`, `llday`, `lduration`) VALUES
(91, '107_330_0', '80.00', 1, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_special_offer_categories`
--

CREATE TABLE IF NOT EXISTS `lumonata_special_offer_categories` (
  `lcat_id` int(11) NOT NULL,
  `lcategory` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `ldescription` text COLLATE latin1_general_ci,
  `lsef_url` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `ldlu` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lcat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lumonata_special_offer_categories`
--

INSERT INTO `lumonata_special_offer_categories` (`lcat_id`, `lcategory`, `ldescription`, `lsef_url`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(3, 'dasda', 'sdasdasd', 'dasda', 0, 'admin', 1396933513, 'admin', 1396933513),
(2, 'Middle', 'lorem12', 'middle', 0, 'admin', 1396929640, 'admin', 1396929640);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_tax`
--

CREATE TABLE IF NOT EXISTS `lumonata_tax` (
  `ltax_id` int(11) NOT NULL AUTO_INCREMENT,
  `taxes_on_shipping` tinyint(4) NOT NULL COMMENT '1=true,0=false',
  `price_include_taxes` tinyint(4) NOT NULL COMMENT '1=true,0=false',
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`ltax_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `lumonata_tax`
--

INSERT INTO `lumonata_tax` (`ltax_id`, `taxes_on_shipping`, `price_include_taxes`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES
(1, 1, 0, 18, '1', '2011-10-24 14:40:51', '1', '2012-10-17 16:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_tax_detail`
--

CREATE TABLE IF NOT EXISTS `lumonata_tax_detail` (
  `ltax_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `ltax_id` int(11) NOT NULL,
  `lcountry_id` int(11) NOT NULL,
  `ltax_groups_ID` int(11) NOT NULL,
  `ltax_charge` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  PRIMARY KEY (`ltax_detail_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=423 ;

--
-- Dumping data for table `lumonata_tax_detail`
--

INSERT INTO `lumonata_tax_detail` (`ltax_detail_id`, `ltax_id`, `lcountry_id`, `ltax_groups_ID`, `ltax_charge`, `lorder`) VALUES
(415, 1, 1, 70, '1.00', 7),
(416, 1, 1, 71, '2.00', 6),
(417, 1, 2, 70, '1.00', 5),
(418, 1, 2, 71, '2.00', 4),
(419, 1, 3, 70, '4.00', 3),
(420, 1, 3, 71, '2.00', 2),
(421, 1, 247, 70, '1.00', 1),
(422, 1, 247, 71, '2.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_tax_groups`
--

CREATE TABLE IF NOT EXISTS `lumonata_tax_groups` (
  `ltax_groups_ID` int(12) NOT NULL AUTO_INCREMENT,
  `ldescription` text NOT NULL,
  `ldefault` int(1) NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` varchar(100) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`ltax_groups_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `lumonata_tax_groups`
--

INSERT INTO `lumonata_tax_groups` (`ltax_groups_ID`, `ldescription`, `ldefault`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`) VALUES
(71, 'Goverment Rate', 0, 21, '1', '2011-10-21 15:54:23', '1', '2011-10-21 15:54:23'),
(70, 'default', 1, 22, '1', '2011-10-21 14:46:43', '1', '2011-10-21 14:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `lumonata_users`
--

CREATE TABLE IF NOT EXISTS `lumonata_users` (
  `luser_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lusername` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldisplay_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lpassword` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lemail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lregistration_date` datetime NOT NULL,
  `luser_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lactivation_key` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lavatar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsex` int(11) NOT NULL COMMENT '1=male,2=female',
  `lbirthday` date NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`luser_id`),
  KEY `username` (`lusername`),
  KEY `display_name` (`ldisplay_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lumonata_users`
--

INSERT INTO `lumonata_users` (`luser_id`, `lusername`, `ldisplay_name`, `lpassword`, `lemail`, `lregistration_date`, `luser_type`, `lactivation_key`, `lavatar`, `lsex`, `lbirthday`, `lstatus`, `ldlu`) VALUES
(1, 'admin', 'Villa Vedas', 'b3fc20b1b6ee8007647a68910e100580', 'request@arunna.com', '0000-00-00 00:00:00', 'administrator', '', 'admin-1.jpg|admin-2.jpg|admin-3.jpg', 1, '2011-03-19', 1, '2016-07-19 07:09:55');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
