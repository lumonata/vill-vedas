<? 
class globalAdmin{
		
		//======BEGIN SETTING ===============//
		
		// function ALL SETING VALUE
		function getSettingValue($option_name,$app_name="Global Setting"){
			global $db;
		    $query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='".$option_name."' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return $data['loption_value'];
		}
		

		
		
		// function get using image or not?
		function getUsingImage($app_name){
			global $db;
			$query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='image' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['loption_value']==1) ? TRUE : FALSE;
		}
		
		// function get auto code or not?
		function getAutoCode($app_name){
			global $db;
			$query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='sys_code' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['loption_value']==1) ? TRUE : FALSE;
		}
		
		// function get auto thumbnails or not?
		function getAutoThumb($app_name){
			global $db;
			$query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='sys_thumb' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['loption_value']==1) ? TRUE : FALSE;
		}
		
		// function get enable/disable comments?
		function getCommentsStatus($app_name){
			global $db;
			$query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='comments' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['loption_value']==1) ? TRUE : FALSE;
		}
		
		// function get auto check publish?
		function getAutoCheckPublish($app_name){
			global $db;
			$query = $db->prepare_query("SELECT s.loption_value FROM lumonata_settings s,lumonata_module m WHERE s.lmodule_id=m.lmodule_id AND s.loption_name='publish' AND m.lname=%s",$app_name);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['loption_value']==1) ? TRUE : FALSE;
		}
		
		function getAllRows($query_paging){
			$result_paging=$db->do_query($query_paging);
			return $num_all_rows=$db->num_rows($result_paging);
		}	
		
		function getMetaTitle(){
			return $this->metaTitle;
		}
		function getMetaKeywords(){
			return $this->metaKeywords;
		}
		function getMetaDescriptions(){
			return $this->metaDescriptions;
		}
		
		function setMetaTitle($metaTitle=''){
			if(!empty($metaTitle)){
				//$this->metaTitle=$metaTitle." - ".$this->getSettingValue("meta_title","Global Setting");
				$this->metaTitle=$metaTitle;
			}else{
				$this->metaTitle=$this->getSettingValue("meta_title","Global Setting");
				
			}
			
		}
		function setMetaKeywords($metaKey=''){
			if(!empty($metaKey))
				$this->metaKeywords=$metaKey;
			else
				$this->metaKeywords='';
		}
		function setMetaDescriptions($metaDesc=''){
			if(!empty($metaDesc))
				$this->metaDescriptions=$metaDesc;
			else
				$this->metaDescriptions='';
		}
		
		
		function getMetaTitleAlert(){
			return $this->metaTitleAlert;
		}
		function getMetaKeywordsAlert(){
			return $this->metaKeywordsAlert;
		}
		function getMetaDescriptionsAlert(){
			return $this->metaDescriptionsAlert;
		}
		//function getValueField2($table,$field_get,$field_cond,$id,$field_cond2,$id2){
		function setMetaTitleAlert($metaTitleAlert=''){
			if(!empty($metaTitleAlert))
				//$this->metaTitleAlert=$metaTitleAlert." - ".$this->getValueField2("lumonata_alert","llabel","lname","GlobalMetaTitle","llang_id",LANGUAGE_ID);
				$this->metaTitleAlert=$metaTitleAlert;
			else
				$this->metaTitleAlert= strip_tags($this->getValueField2("lumonata_alert","llabel","lname","GlobalMetaTitle","llang_id",LANGUAGE_ID));
			
		}
		function setMetaKeywordsAlert($metaKeyAlert=''){
			if(!empty($metaKeyAlert))
				$this->metaKeywordsAlert=$metaKeyAlert;
			else
				$this->metaKeywordsAlert='';
		}
		function setMetaDescriptionsAlert($metaDescAlert=''){
			if(!empty($metaDescAlert))
				$this->metaDescriptionsAlert=$metaDescAlert;
			else
				$this->metaDescriptionsAlert='';
		}
		//======END SETTING =================//
		
		function getRequired(){
			return "<img src=\"http://".IMAGE_DIR."/required.gif\" >";
		}
		
		function getRequiredMsg(){
			return "<div class=\"requiredMsg\"><img src=\"http://".IMAGE_DIR."/required.gif\">  Represents compulsory fields</div>";
		}	
		
		function setFlag($id){
			return "<img src=\"http://".IMAGE_DIR."/flags/".$id.".gif\" >";
		}
		
		function setButtonYes(){
			return "<input name=\"action\" type=\"Submit\" class=\"button_yesno\" value=\"Yes\">";
		}
		
		function setButtonNo(){
			return "<input type=\"button\" name=\"Button\" class=\"button_yesno\" value=\"No\" onClick=\"javascript:history.back();\">";
		}
		
		function setButtonBack(){
			return "<input type=\"button\" name=\"Button\" class=\"button_yesno\" value=\"Back\" onClick=\"javascript:history.back();\">";
		}
		
		/* === Setting Toolbar === */ 
		function setToolbar($out_template="toolbar.html",$sts=1,$new=0,$save=0,$edit=0,$delete=0,$cancel=0){
		global $db;
		/*DEFINE LANGUAGE*/
		//define('LANGUAGE',$_REQUEST['lang']);
		//Set Templates
		$OUT_TEMPLATE=$out_template;
		$t=new Template(TEMPLATE_DIR);
		$t->set_file('toolbar', $OUT_TEMPLATE);
				
		//set block variable
		$t->set_var('image_dir',IMAGE_DIR);
		$t->set_var('admin_url',ADMIN_URL);
		if (!empty($_REQUEST['d'])){
			$t->set_var('d',"&d=".$_REQUEST['d']);
		}
		if ($sts == 1 ){
			$t->set_var('img_bar_left',"t".$_REQUEST['prc'].".jpg");
			$mod = $_REQUEST['mod'];
			$src = $_REQUEST['src'];
			$order = $_REQUEST['order'];
			$p = $_REQUEST['p'];
			$d = $_GET['d'];
			//mod=212&prc=view&d=2
				if($mod == 212 && $d == 2){
						$title_bar = "Villa Details";
				}elseif($mod == 213 && $d == 2){
						$title_bar = "Villa Room Type";
				}elseif($mod == 214 && $d == 2){
						$title_bar = "Villa Season Period";
				}elseif($mod == 215 && $d == 2){
						$title_bar = "Villa Rate";
				}elseif($mod == 182){
					if ($d == 1)
						$title_bar = "Testimonial";
					elseif ($d==2)
						$title_bar = "Our Team";
				}else{
					$query = $db->prepare_query("select * from lumonata_module where lmodule_id =%d",$mod);
					$result=$db->do_query($query);
					$data=$db->fetch_array($result);
					$title_bar = $data['ltitle'];
				}
			$t->set_var('title_bar',$title_bar);
			$t->set_var('mod',$mod);
			$t->set_var('src',$src);
			$t->set_var('order',$order);
			$t->set_var('p',$p);
			$t->set_block('toolbar', 'bLbar', 'bL');
			$vBar = $t->Parse('bL', 'bLbar', false);
			if ($cancel == 1){
				$t->set_block('toolbar', 'cancelT', 'cT');
				$vBar .= $t->Parse('cT', 'cancelT', false);
			}else{
				$t->set_block('toolbar', 'cancelF', 'nF');
				$vBar .= $t->Parse('cF', 'cancelF', false);
			}
		
			if ($delete == 1){
				$t->set_block('toolbar', 'deleteT', 'dT');
				$vBar .= $t->Parse('dT', 'deleteT', false);
			}else{
				$t->set_block('toolbar', 'deleteF', 'dF');
				$vBar .= $t->Parse('dF', 'deleteF', false);
			}
			
			if ($edit == 1){
				$t->set_block('toolbar', 'editT', 'eT');
				$vBar .= $t->Parse('eT', 'editT', false);
			}else{
				$t->set_block('toolbar', 'editF', 'eF');
				$vBar .= $t->Parse('eF', 'editF', false);
			}
			//set block
			if ($save == 1){
				$t->set_block('toolbar', 'saveT', 'sT');
				$vBar .= $t->Parse('sT', 'saveT', false);
			}else{
				$t->set_block('toolbar', 'saveF', 'sF');
				$vBar .= $t->Parse('sF', 'saveF', false);
			}
		
			if ($new == 1){
				$t->set_block('toolbar', 'newT', 'nT');
				$vBar .= $t->Parse('nT', 'newT', false);	
			}else{
				$t->set_block('toolbar', 'newF', 'nF');
				$vBar .= $t->Parse('nF', 'newF', false);
			}
		}else{
			$t->set_var('img_bar_left',"control-panel.jpg");
			$t->set_var('title_bar',"Control Panel");
			//$t->set_var('app',$_REQUEST['app']);
			$t->set_block('toolbar', 'bLbar', 'bL');
			$vBar = $t->Parse('bL', 'bLbar', false);
		}
		
		return $vBar;
		}
		
		function setHome($usr){
			global $db;
			$query = $db->prepare_query("SELECT u.lusername as lusername,
									u.lusertype_id as lusertype_id,
									t.lname as lusertype_name,
									u.lpassword as lpassword,
									u.lname as lname,
									u.lemail as lemail,
									u.lblock as lblock,
									u.llastvisit_date as llastvisit_date,
									u.lorder_id as lorder_id,
									u.lupd_by as lupd_by,
									u.ldlu as ldlu
					FROM lumonata_user u
					LEFT JOIN lumonata_usertype t ON u.lusertype_id = t.lusertype_id
					where u.lusername =%s",$usr);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			$whois .= "Name: ".$data[lname]."<br />";
			$whois .= "Usertype: ".$data[lusertype_name]."<br />";
			$whois .= "Email: ".$data[lemail]."<br />";
			
			$OUT_TEMPLATE="main_body.html";
			$t=new Template(TEMPLATE_DIR);
			$t->set_file('main_body', $OUT_TEMPLATE);
			$t->set_var('whois',$whois);
			$t->set_block('main_body', 'bMainBody', 'bMB');
			return $t->Parse('bMB', 'bMainBody', false);
		}
		
		function setError($msg=NO_AUTHO){
			return "<div class=\"error\">".$msg."<br />".$this->setButtonBack()."</div>";	
		}
		
		function isApps($app){
			global $db;
			if ($app != "images"){	
				$query = $db->prepare_query("select * from lumonata_module where lapps = %d",$app);
				$result=$db->do_query($query);
				$num_rows=$db->num_rows($result);
				return ($num_rows != 0) ? TRUE : FALSE;
			}else{
				return TRUE;
			}
		}
		
		function getModule($mod){		
			global $db;
			$query = $db->prepare_query("select * from lumonata_module where lmodule_id =%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			$module = $data['lfolder']."/".$data['lapps'];
			return $module;
		}
		
		function getApps($mod){		
			global $db;
			$query = $db->prepare_query("select * from lumonata_module where lmodule_id =%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return $data['lapps'];
		}
		
		function getModuleTitle($mod){		
			global $db;
			$query = $db->prepare_query("select * from lumonata_module where lmodule_id =%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return $data['ltitle'];
		}
		//Select Option NOT NULL
		function setSelectOption($qry,$select_name,$opt,$selected,$view,$item_id="",$index=0){
			global $db;
			$rs = $db->do_query($qry);
			$options = "<select name=\"".$select_name."[".$index."]\" id=\"$select_name$index\" class=\"jsrequired\">
							<option value=\"\">Select $opt</option>";
			while($dt = $db->fetch_array($rs)){
				if ($item_id == $dt[$selected]){
					$options .= "<option value=\"$dt[$selected]\" selected=\"selected\" >$dt[$view]</option>";			
				}else{
					$options .= "<option value=\"$dt[$selected]\">$dt[$view]</option>";	


			 }
			}
			$options .= "</select>";
			return $options;
		}
		
		
		//Select Option NULL
		function setSelectOptionNull($qry,$select_name,$opt,$selected,$view,$item_id="",$index=0){
			global $db;
			$rs = $db->do_query($qry);
			$options = "<select name=\"".$select_name."[".$index."]\" id=\"$select_name$index\">
							<option value=\"\">Select $opt</option>";
			while($dt = $db->fetch_array($rs)){
				if ($item_id == $dt[$selected]){
					$options .= "<option value=\"$dt[$selected]\" selected=\"selected\" >$dt[$view]</option>";			
				}else{
					$options .= "<option value=\"$dt[$selected]\">$dt[$view]</option>";	


			 }
			}
			$options .= "</select>";
			return $options;
		}
		
		//Select Option NOT NULL
		function setSelectOptionClients($qry,$select_name,$opt,$selected,$view,$item_id="",$index=0){
			global $db;
			$rs = $db->do_query($qry);
			$options = "<select name=\"".$select_name."[".$index."]\" id=\"$select_name$index\" class=\"jsrequired\" onchange=\"getClients(this,$index)\" >
							<option value=\"\">Select $opt</option>
							<option value=\"Addnew\">Add New Client</option>";
			while($dt = $db->fetch_array($rs)){
				if ($item_id == $dt[$selected]){
					$options .= "<option value=\"$dt[$selected]\" selected=\"selected\" >$dt[$view] - $dt[lclient_id]</option>";			
				}else{
					$options .= "<option value=\"$dt[$selected]\">$dt[$view] - $dt[lclient_id]</option>";	


			 }
			}
			$options .= "</select>";
			return $options;
		}
		
		//Select Option NOT NULL special for reservation
		function setSelectOptionReserv($qry,$select_name,$opt,$selected,$view,$item_id="",$index=0){
			global $db;
			$rs = $db->do_query($qry);
			$options = "<select name=\"".$select_name."[".$index."]\" id=\"$select_name$index\" class=\"jsrequired\" onchange=\"getRoomType(this,$index)\">>
							<option value=\"\">Select $opt</option>";
			while($dt = $db->fetch_array($rs)){
				if ($item_id == $dt[$selected]){
					$options .= "<option value=\"$dt[$selected]\" selected=\"selected\" >$dt[$view]</option>";			
				}else{
					$options .= "<option value=\"$dt[$selected]\">$dt[$view]</option>";	


			 }
			}
			$options .= "</select>";
			return $options;
		}
		
		//special for rate
		function setSelectOptionSeason($qry,$select_name,$opt,$selected,$view,$item_id="",$index=0){
			global $db;
			$rs = $db->do_query($qry);
			$options = "<select name=\"".$select_name."[".$index."]\" id=\"$select_name$index\" class=\"jsrequired\">
							<option value=\"\">Select $opt</option>";
			while($dt = $db->fetch_array($rs)){
				if ($item_id == $dt[$selected]){
					$options .= "<option value=\"$dt[$selected]\" selected=\"selected\" >$dt[$view] ( ".date("d M Y",$dt[ldate_start])." - ".date("d M Y",$dt[ldate_finish])." )</option>";			
				}else{
					$options .= "<option value=\"$dt[$selected]\">$dt[$view] ( ".date("d M Y",$dt[ldate_start])." - ".date("d M Y",$dt[ldate_finish])." )</option>";	


			 }
			}
			$options .= "</select>";
			return $options;
		}
		function setCommentsTrue($i,$check){
			return "<tr>
	  				<td >Comments</td>
	  				<td ><input name=\"comments[".$i."] id=\"comments".$i."\" value=\"1\" ".$check." type=\"checkbox\"> Allow Comments</td>
	   			</tr>";
		}
		
		function setPublishTrue($i,$check){
			return "<tr>
	  					<td >Published</td>
	  					<td ><input name=\"publish[".$i."]\" id=\"publish".$i."\" value=\"1\" ".$check." type=\"checkbox\"> Yes</td>
  					</tr>";
		}
		
		
		function setStartPublishTrueOld($i,$value,$label){
  			return	"<tr>
						<td >Start Publishing</td>
						<td ><input name=\"start_publish[".$i."]\" id=\"start_publish".$i."\" type=\"text\" value=\"".$value."\" onmousedown=\"displayDatePicker('start_publish[".$i."]');\" style=\"width:70px;\"/>
							<input name=\"button\" type=\"button\" class=\"datebutton\" id=\"start_publish_button".$i."\"  onclick=\"displayDatePicker('start_publish[".$i."]');\" value=\"       \"> 
        * MM/DD/YYYY	
						</td>
					</tr>";
		}
		

		
		function checkIn($i,$value){
  			return	"<tr>
						<td >Check In</td>
						<td ><input name=\"checkIn[".$i."]\" id=\"checkIn".$i."\" type=\"text\" value=\"".$value."\" onmousedown=\"displayDatePicker('checkIn[".$i."]');\" style=\"width:70px;\"/>
						     <input name=\"button\" type=\"button\" class=\"datebutton\" id=\"start_publish_button".$i."\"  onclick=\"displayDatePicker('checkIn[".$i."]');\" value=\"       \"> 
        * DD/MM/YYYY	
						</td>
					</tr>";
		}
		
		function checkOut($i,$value){
  			return	"<tr>
						<td >Check Out</td>
						<td ><input name=\"checkOut[".$i."]\" id=\"checkOut".$i."\" type=\"text\" value=\"".$value."\" onmousedown=\"displayDatePicker('checkOut[".$i."]');\" style=\"width:70px;\"/>
							<input name=\"button\" type=\"button\" class=\"datebutton\" id=\"start_publish_button".$i."\"  onclick=\"displayDatePicker('checkOut[".$i."]');\" value=\"       \"> 
        * DD/MM/YYYY	
						</td>
					</tr>";
		}
		
		function datetrans($i,$value){
  			return	"<tr>
						<td >Date of Transfer</td>
						<td ><input name=\"date_trans[".$i."]\" id=\"date_trans".$i."\" type=\"text\" value=\"".$value."\" onmousedown=\"displayDatePicker('date_trans[".$i."]');\" style=\"width:70px;\"/>
							<input name=\"button\" type=\"button\" class=\"datebutton\" id=\"start_publish_button".$i."\"  onclick=\"displayDatePicker('date_trans[".$i."]');\" value=\"       \"> 
        * DD/MM/YYYY	
						</td>
					</tr>";
		}
		
		function datearrival($i,$value){
  			return	"<tr>
						<td >Date of Arrival</td>
						<td ><input name=\"datearrival[".$i."]\" id=\"datearrival".$i."\" type=\"text\" value=\"".$value."\" onmousedown=\"displayDatePicker('datearrival[".$i."]');\" style=\"width:70px;\"/>
							<input name=\"button\" type=\"button\" class=\"datebutton\" id=\"start_publish_button".$i."\"  onclick=\"displayDatePicker('datearrival[".$i."]');\" value=\"       \"> 
        * DD/MM/YYYY	
						</td>
					</tr>";
		}
		
		
		
		function setStartPublishTrue($i,$value){
  			return	"<tr>
						<td >Start Publishing</td>
						<td ><input type=\"text\" id=\"start_publish".$i."\" name=\"start_publish[".$i."]\" value=\"".$value."\" style=\"width:150px;\"/>
    <button id=\"strigger".$i."\" class=\"datebutton\">...</button>
    <script type=\"text/javascript\">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 1,
        showsTime         : true,
        step              : 1,
        electric          : false,
        inputField        : \"start_publish".$i."\",
        button            : \"strigger".$i."\",
        ifFormat          : \"%a, %b %d, %Y %k:%M \",
        daFormat          : \"%Y/%m/%d\"
      });
    //]]></script>
	<script type=\"text/javascript\">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 1,
        showsTime         : true,
        step              : 1,
        electric          : false,
        inputField        : \"start_publish".$i."\",
        button            : \"start_publish".$i."\",
        ifFormat          : \"%a, %b %d, %Y %k:%M \",
        daFormat          : \"%Y/%m/%d\"
      });
    //]]></script>
        * <i>eg: Mon, Jan 01, 2008 23:01</i>
						</td>
					</tr>";
		}
		
		function setFinishPublishTrue($i,$value){
  			return	"<tr>
						<td >Finish Publishing</td>
						<td ><input type=\"text\" id=\"finish_publish".$i."\" name=\"finish_publish[".$i."]\" value=\"".$value."\" style=\"width:150px;\"/>
    <button id=\"ftrigger".$i."\" class=\"datebutton\">...</button>
    <script type=\"text/javascript\">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 1,
        showsTime         : true,
        step              : 1,
        electric          : false,
        inputField        : \"finish_publish".$i."\",
        button            : \"ftrigger".$i."\",
        ifFormat          : \"%a, %b %d, %Y %k:%M \",
        daFormat          : \"%Y/%m/%d\"
      });
    //]]></script>
	<script type=\"text/javascript\">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 1,
        showsTime         : true,
        step              : 1,
        electric          : false,
        inputField        : \"finish_publish".$i."\",
        button            : \"finish_publish".$i."\",
        ifFormat          : \"%a, %b %d, %Y %k:%M \",
        daFormat          : \"%Y/%m/%d\"
      });
    //]]></script>
        * <i>eg: Mon, Jan 01, 2008 23:01</i>
						</td>
					</tr>";
		}
		
		function setCodeField($i=0,$value=""){
			return "<tr >
					<td>Code ".$this->getRequired()."</td>
					<td><input name=\"id[".$i."]\" id=\"id".$i."\" type=\"text\" class=\"jsrequired jsvalidate_alphanum\" value=\"".$value."\" style=\"width:100px;\"/></td>
	</tr>";	
		}
		
		function setImageFieldView($mod,$i=0,$value="",$imgLoc="",$imgW="",$imgH="",$imgLocT="",$imgWT="",$imgHT="",$imgSts=""){
			return "
			<tr>
				<td >Existing Image</td>
				<td >".$this->setImagePopupView($imgLoc,$imgW,$imgH,$imgLocT,$imgWT,$imgHT)."</td>
			</tr>
			<tr>
				<td >New Image</td>
				<td >
					<input name=\"imageold[".$i."]\" id=\"imageold".$i."\" type=\"hidden\" value=\"{imageold}\" />
					<input name=\"image[".$i."]\" id=\"image".$i."\" type=\"file\" />".$this->setImageRecommended($mod)."
				</td>
			</tr>
				";
		}
		
		function setImageField($mod,$i=0,$label="Image",$value="",$imgLoc="",$imgW="",$imgH="",$imgLocT="",$imgWT="",$imgHT="",$imgSts="",$banner=0){
			//echo $banner;
			if($banner==1){
				return "
				<tr>
				<td >".$label."</td>
				<td >
				<input name=\"imageold[".$i."]\" id=\"imageold".$i."\" type=\"hidden\" value=\"{imageold}\" />
				<input name=\"image[".$i."]\" id=\"image".$i."\" type=\"file\" />
				<span class=\"recommended\">
				<ul>
					<li>Recommended Dimensions: width:960px; height:300px; </li>
					<li>Recommended Size : Less than 200 KB</li>
				</ul></span>" 
						.$this->setImagePopup($imgLoc,$imgW,$imgH,$imgSts)."
				</td>
				</tr>
				";
			}else if ($banner==2){
				return "
				<tr>
				<td >".$label."</td>
				<td >
				<input name=\"imageold[".$i."]\" id=\"imageold".$i."\" type=\"hidden\" value=\"{imageold}\" />
				<input name=\"image[".$i."]\" id=\"image".$i."\" type=\"file\" />
				<span class=\"recommended\">Recommended : Dimensions : width:220 px, height:auto</span> " 
				.$this->setImagePopup($imgLoc,$imgW,$imgH,$imgLocT,$imgWT,$imgHT)."
				</td>
				</tr>
				";	
			}else{
					return "
				<tr>
			<td >".$label."</td>
			<td >
			<input name=\"imageold[".$i."]\" id=\"imageold".$i."\" type=\"hidden\" value=\"$value\" />
			<input name=\"image[".$i."]\" id=\"image".$i."\" type=\"file\" />".$this->setImageRecommended($mod).$this->setImagePopup($imgLoc,$imgW,$imgH,$imgLocT,$imgWT,$imgHT)."
			</td>
		</tr>
				";
			}
		}
		
		function setImageField2($mod,$i=0,$l=0,$label="Image",$value="",$imgLoc="",$imgW="",$imgH="",$imgLocT="",$imgWT="",$imgHT="",$imgSts="",$banner=0){
			
			if($banner==1){
				return "
				<tr>
			<td >".$label."</td>
			<td >
			<input name=\"imageold[".$l."][".$i."]\" id=\"imageold".$l.$i."\" type=\"hidden\" value=\"$value\" />
			<input name=\"image[".$l."][".$i."]\" id=\"image".$l.$i."\" type=\"file\" />
			<span class=\"recommended\">
			<ul>
			<li>Recommended Dimensions: width:960px; height:300px; </li>
			<li>Recommended Size : Less than 200 KB</li>
	 		</ul></span>" 
			.$this->setImagePopup($imgLoc,$imgW,$imgH,$imgSts)."
			</td>
			</tr>
				";
			}else{
					return "
				<tr>
			<td >".$label."</td>
			<td >
			<input name=\"imageold[".$l."][".$i."]\" id=\"imageold".$l.$i."\" type=\"hidden\" value=\"$value\" />
			<input name=\"image[".$l."][".$i."]\" id=\"image".$l.$i."\" type=\"file\" />".$this->setImageRecommended($mod).$this->setImagePopup($imgLoc,$imgW,$imgH,$imgLocT,$imgWT,$imgHT)."
			</td>
		</tr>
				";
			}
		}
		
		
		function setVideoField($mod,$i=0,$value="",$title="",$videoSts="",$type="",$videoLoc="",$imgLoc=""){
			return "
			<tr>
		<td >Video</td>
		<td >
		<input name=\"videoold[".$i."]\" id=\"videoold".$i."\" type=\"hidden\" value=\"{videoold}\" />
		<input name=\"video[".$i."]\" id=\"video".$i."\" type=\"file\" />".$this->setVideoPopup($title,$videoSts,$type,$videoLoc,$imgLoc)."
		</td>
	</tr>
			";
		}
		
		function setThumbField($mod,$thumb,$i=0,$value="",$imgLoc="",$imgW="",$imgH="",$imgSts=""){
			($thumb==0)?$thumb="":$thumb=$thumb;
			return "
			<tr>
		<td >Thumbnail ".$thumb."</td>
		<td >
		<input name=\"thumb".$thumb."[".$i."]\" id=\"thumb".$thumb.$i."\" type=\"file\" />".$this->setThumbRecommended($mod,$thumb).$this->setImagePopup($imgLoc,$imgW,$imgH,$imgSts)."
		</td>
	</tr>
			";
		}
		
		
		
		//get number of thumbnails on module
		function getNumThumb($mod){
			global $db;
			$query  =  $db->prepare_query("SELECT lnum_image FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			$numThumb = $data['lnum_image'] - 1;
			return $numThumb;
		}
		
		//get number of thumbnails on module
		function getNumImages($mod){
			global $db;
			$query  =  $db->prepare_query("SELECT lnum_image FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			return $data['lnum_image'];
		} 
		
		//get image folder on module
		function getImageFolder($mod){
			global $db;
			$query  =  $db->prepare_query("SELECT limage_folder FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			return $data['limage_folder'];
		}
		
		function getImageDimensions($mod){
			global $db;
			$query = $db->prepare_query("SELECT ldimensions FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return $pre=explode(";",$data['ldimensions']);
		}
		
		function setImageRecommended($mod){
			global $db;
			$query = $db->prepare_query("SELECT * FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			$pre=explode(";",$data['ldimensions']);
			return "<span class=\"recommended\">Recommended : Dimensions : ".$pre[0]." x ".$pre[1]." px , Size : Less than 200 KB</span> ";
		}
		
		function setThumbRecommended($mod,$thumb){
			global $db;
			$query = $db->prepare_query("SELECT * FROM lumonata_module WHERE lmodule_id=%d",$mod);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			$pre=explode(";",$data['ldimensions']);
			$w = $thumb + $thumb;
			$h = $w + 1;
			return "<span class=\"recommended\">Recommended : Dimensions : ".$pre[$w]." x ".$pre[$h]." px , Size : Less than 100 KB</span> ";
		}
		
		function setImagePopup($imgLoc="",$imgW="",$imgH="",$imgSts="",$i=""){
		return "<span id=\"imgSts".$i."\"><a href=\"javascript:;\" title=\"Click to Enlarge\" onclick=\"javascript:window.open ('".$imgLoc."','','width=".$imgW.",height=".$imgH.",resizeable=no,scrollbars=no,toolbar=no,status=no')\">".$imgSts."</a></span>";
		}
		
		function setImagePopupView($imgLoc="",$imgW="",$imgH="",$imgLocT="",$imgWT="",$imgHT=""){
		return "<a href=\"javascript:;\" title=\"Click to Enlarge\" onclick=\"javascript:window.open ('".$imgLoc."','','width=".$imgW.",height=".$imgH.",resizeable=no,scrollbars=no,toolbar=no,status=no')\"><img src=\"".$imgLocT."\" width=\"".$imgWT."\" vspace=\"1\" height=\"".$imgHT."\" border=\"0\"></a>";
		}
		
		function setVideoPopup($title="",$videoSts="",$type="",$videoLoc="",$imgLoc=""){
		return "<a href=\"javascript:;\"  title=\"Click to view Video\"  onclick=\"javascript:window.open ('http://".APPS_DIR."/Galleries/videos/popup.php?title=$title&type=$type&videoLoc=$videoLoc&imgLoc=$imgLoc','','left=200,top=200,width=420,height=270,resizeable=no,scrollbars=no,toolbar=no,status=no')\">$videoSts</a>";
		}
		//============== BEGIN GET PRIVILEGE =======================//		
		function getPView($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT lview FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['lview']==1) ? TRUE : FALSE;
		}
		
		function getPInsert($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT linsert FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['linsert']==1) ? TRUE : FALSE;
		}
		
		function getPEdit($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT ledit FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['ledit']==1) ? TRUE : FALSE;
		}
		
		function getPDelete($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT ldelete FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['ldelete']==1) ? TRUE : FALSE;
		}
		
		function getPPublish($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT lpublish FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['lpublish']==1) ? TRUE : FALSE;
		}
		
		function getPViewOwn($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT lview_own FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['lview_own']==1) ? TRUE : FALSE;
		}
		
		function getPInsertOwn($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT linsert_own FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['linsert_own']==1) ? TRUE : FALSE;
		}
		
		function getPEditOwn($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT ledit_own FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['ledit_own']==1) ? TRUE : FALSE;
		}
		
		function getPDeleteOwn($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT ldelete_own FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['ldelete_own']==1) ? TRUE : FALSE;
		}
		
		function getPPublishOwn($mod,$usertype_id){
			global $db;
			$query = $db->prepare_query("SELECT lpublish_own FROM lumonata_user_privilege WHERE lmodule_id=%d AND lusertype_id=%d",$mod,$usertype_id);
			$result=$db->do_query($query);
			$data=$db->fetch_array($result);
			return ($data['lpublish_own']==1) ? TRUE : FALSE;
		}
		
		function getPViewButton($mod,$usertype_id){
			return ($this->getPView($mod,$usertype_id) || $this->getPViewOwn($mod,$usertype_id)) ? 1 : 0;
		}
		
		function getPInsertButton($mod,$usertype_id){
			return ($this->getPInsert($mod,$usertype_id) || $this->getPInsertOwn($mod,$usertype_id)) ? 1 : 0;
		}
		
		function getPEditButton($mod,$usertype_id){
			return ($this->getPEdit($mod,$usertype_id) || $this->getPEditOwn($mod,$usertype_id)) ? 1 : 0;
		}
		
		function getPDeleteButton($mod,$usertype_id){
			return ($this->getPDelete($mod,$usertype_id) || $this->getPDeleteOwn($mod,$usertype_id)) ? 1 : 0;
		}
		
		function getPPublishButton($mod,$usertype_id){
			return ($this->getPPublish($mod,$usertype_id) || $this->getPPublishOwn($mod,$usertype_id)) ? 1 : 0;
		}
		
		function getPViewBoth($mod,$usertype_id){
			return ($this->getPView($mod,$usertype_id) || $this->getPViewOwn($mod,$usertype_id)) ? TRUE : FALSE;
		}
		
		function getPInsertBoth($mod,$usertype_id){
			return ($this->getPInsert($mod,$usertype_id) || $this->getPInsertOwn($mod,$usertype_id)) ? TRUE : FALSE;
		}
		
		function getPEditBoth($mod,$usertype_id){
			return ($this->getPEdit($mod,$usertype_id) || $this->getPEditOwn($mod,$usertype_id)) ? TRUE : FALSE;
		}
		
		function getPDeleteBoth($mod,$usertype_id){
			return ($this->getPDelete($mod,$usertype_id) || $this->getPDeleteOwn($mod,$usertype_id)) ? TRUE : FALSE;
		}
		
		function getPPublishBoth($mod,$usertype_id){
			return ($this->getPPublish($mod,$usertype_id) || $this->getPPublishOwn($mod,$usertype_id)) ? TRUE : FALSE;
		}
		
		//============== END GET PRIVILEGE =========================//
		
		//Update Order ID each table 
		function setOrderID($table,$order_id){
			global $db;
			$query = $db->prepare_query("UPDATE ".$table." SET lorder_id = lorder_id + 1 WHERE lorder_id >= %d",$order_id);
			$result = $db->do_query($query); 
			//return ($result == DB_OK) ? TRUE : FALSE;
			return true;
		}
		
		function setOrderID2($table,$order_id,$field,$id){
			global $db;
			$query = $db->prepare_query("UPDATE ".$table." SET lorder_id = lorder_id + 1 WHERE lorder_id >= %d And $field <> %d",$order_id,$id);
			$result = $db->do_query($query); 
			return ($result == DB_OK) ? TRUE : FALSE;
		}
		
		//get 1 value of field on 1 table
		function getValueField($table,$field_get,$field_cond,$id){
			global $db;
			$query  =  $db->prepare_query("SELECT ".$field_get." FROM ".$table." WHERE ".$field_cond."=%s",$id);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			return $data[$field_get];
		}
		
		//get 1 value of field on 1 table wit 2 condition
		function getValueField2($table,$field_get,$field_cond,$id,$field_cond2,$id2){
			global $db;
		//echo "<br>".
		        $query  =  $db->prepare_query("SELECT ".$field_get." FROM ".$table." WHERE ".$field_cond."=%s AND ".$field_cond2."=%s",$id,$id2);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			return $data[$field_get];
		}
		
		//get number of row on 1 table with 1 condition
		
		function getNumRows($table,$field,$id){
			global $db;
			$query  = $db->prepare_query("SELECT * FROM ".$table." WHERE ".$field."=%s",$id);
			$result = $db->do_query($query);
			return $db->num_rows($result);
		}
		
		
		//get num of rows with 2 condition
		function getNumRows2($table,$field,$id,$field2,$id2){
			global $db;
			$query  = $db->prepare_query("SELECT * FROM ".$table." WHERE ".$field."=%s AND ".$field2."=%s",$id,$id2);
			$result = $db->do_query($query);
			return $db->num_rows($result);
		}
		
		// INFO - TOOLTIP
		function getName($usr){		
			global $db;
			$query = $db->prepare_query("SELECT lname FROM lumonata_user WHERE lusername =%s",$usr);
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			return $data['lname'];
		}
		
		function setInfo($type,$created_by,$created_date,$update_by,$dlu,$publish="",$publish_up="",$publish_down=""){
			if ($type =="CU"){
				//creator
				$created = "Created on ".date("D, M d, Y H:i",$created_date)." by ".$this->getName($created_by);
				
				//update
				if ($dlu == 0){
					$dlu = "Last Modified : Not Modified";
				}else{
					$dlu = "Last Modified on ".date("D, M d, Y H:i",$dlu)." by ".$this->getName($update_by);	
				}
				$info = $created."<br />".$dlu;
			}elseif($type=="CUP"){
				//creator
				$created = "Created on ".date("D, M d, Y H:i",$created_date)." by ".$this->getName($created_by);
				
				//update
				if ($dlu == 0){
					$dlu = "Last Modified : Not Modified";
				}else{
					$dlu = "Last Modified on ".date("D, M d, Y H:i",$dlu)." by ".$this->getName($update_by);	
				}
				//publish start
				if ($publish == 0 && $publish_up == 0 ){
					$publish_start = "Start Publish is awaiting moderation";
				}else{
					$publish_start = "Start Publish on ".date("D, M d, Y H:i",$publish_up);
				}
				//publish down
				if ($publish_down == ""){
					$publish_down = "";
				}else{
					$publish_down = "<br />Finish Publish on ".date("D, M d, Y H:i",$publish_down);
				}
				
				$info = $created."<br />".$dlu."<br />".$publish_start.$publish_down;
			}elseif($type=="U"){
				//update
				if ($dlu == 0){
					$dlu = "Last Modified : Not Modified";
				}else{
					$dlu = "Last Modified on ".date("D, M d, Y H:i",$dlu)." by ".$this->getName($update_by);	
				}
				$info = $dlu;
			}
			
			return $info;
		}
		
		function setCode($table,$field){
			global $db;
			$query = $db->prepare_query("SELECT MAX(".$field.") from ".$table."");
			$result = $db->do_query($query);
			$data = $db->fetch_array($result);
			if($data[0]==NULL){
				return "1";	
			}else{
				$cnt=$data[0]+1;
				return $cnt;
			}
		}
		
		//Create Folder using on Module apps
		function createFolder($folder,$num_image){
			$dir = IMAGE_PATH."/".$folder;
			if (!is_dir($dir)){
				if (mkdir($dir,755)){
					for($i=1;$i<$num_image;$i++){
						$dirThumb = "";
						if($i==1){
							$dirThumb = $dir."/thumbs";
						}else{
							$dirThumb = $dir."/thumbs".$i;
						}
						if (!is_dir($dirThumb)) mkdir($dirThumb,755);
					}
				}else{
					return "Directory can't created";
				}
			}else{
				$path = opendir($dir);
				 while ($dirList = readdir($path)){
					if($dirList !='..' && $dirList !='.' && $dirList!='' && $dirList!='Thumbs.db' && substr_count($dirList,".")==0){
						$dirlist[] = $dirList ;
					}	
				 }
				$num_dir = sizeof($dirlist);
				for($i=1;$i<$num_image;$i++){
						$dirThumb = "";
						if($i==1){
							$dirThumb = $dir."/thumbs";
						}else{
							$dirThumb = $dir."/thumbs".$i;
						}
						if (!is_dir($dirThumb)) mkdir($dirThumb,755);
				}
				//closedir($dir);
				//echo "num image :".$num_image." num dir : ".$num_dir;
				for($j=$num_image;$j<=$num_dir;$j++){
					$dirThumb = "";
					if($j==1){
						$dirThumb = $dir."/thumbs";
					}else{
						$dirThumb = $dir."/thumbs".$j;
					}
						
						if (is_dir($dirThumb)){
							$dir2 = opendir($dirThumb);
							while ($dirList = readdir($dir2)){
								if($dirList !='..' && $dirList !='.'){ // && $dirList!='' && $dirList!='Thumbs.db'){
					 			if(file_exists($dirThumb."/".$dirList)) unlink($dirThumb."/".$dirList);
								}
							}
							closedir($dir2);
						 	rmdir($dirThumb);
						}
				}
				
			}
			//closedir($dir);
		}
		
		//BEGIN VALIDATION FUNCTION
		// function to validate an integer
		function isInteger($str) {
			return preg_match("/^-?([0-9])+$/", $str);
		}
	
		// function to validate a float
		function isFloat($str) {
			return preg_match("/^-?([0-9])+([\.|,]([0-9])*)?$/", $str);
		}
		
		// function to validate a alphabetic strings
		function isAlpha($str) {
			return preg_match("/^[a-z]+$/i", $str);
		}
		
		// function to validate a alphanumeric strings
		function isAlphaNum($str) {
			return preg_match("/^[a-z0-9]*$/i", $str);
		}
		
		// function to validate an e-mail address
		function isEmailAddress($str) {
			if(eregi("^([a-z0-9_-])+([\.a-z0-9_-])*@([a-z0-9-])+(\.[a-z0-9-]+)*\.([a-z]{2,6})$", $str)){
				list($username, $domain) = split("@",$str);			
				if(function_exists("getmxrr") && !getmxrr($domain, $MXHost)) {
				  return FALSE;
				}else return true;
			}
		}
		
		// function to validate a URL
		function isUrl($str) {
			return preg_match("/^(http|https|ftp):\/\/([a-z0-9]([a-z0-9_-]*[a-z0-9])?\.)+[a-z]{2,6}\/?([a-z0-9\?\._-~&#=+%]*)?/", $str);
			
		}
		
		// funtion to checking available a data
		function isAvailable($table,$field,$id,$int=0){
			global $db;
			if($int==0){
				$query  = $db->prepare_query("SELECT * FROM ".$table." WHERE ".$field."=%s",$id);		
			}else{
				$query  = $db->prepare_query("SELECT * FROM ".$table." WHERE ".$field."=%d",$id);		
			}
			
			$result = $db->do_query($query);
			$num_rows=$db->num_rows($result);	
			return ($num_rows == 0)? TRUE : FALSE;
		}
		
		//END VALIDATION FUNCTION
		
		//function unzip
		/*function unzip($zipfile,$dest)
		{
			$zip = zip_open($zipfile);
			while ($zip_entry = zip_read($zip))    {
				zip_entry_open($zip, $zip_entry);
				if (substr(zip_entry_name($zip_entry), -1) == '/') {
					$zdir = substr(zip_entry_name($zip_entry), 0, -1);
					$zdir = $dest.$zdir;
					if (file_exists($zdir)) {
						trigger_error('Directory "<b>' . $zdir . '</b>" exists', E_USER_ERROR);
						return false;
					}
					mkdir($zdir);
				}
				else {
					$name = zip_entry_name($zip_entry);
					$name = $dest.name;
					if (file_exists($name)) {
						trigger_error('File "<b>' . $name . '</b>" exists', E_USER_ERROR);
						return false;
					}
					$fopen = fopen($name, "w");
					fwrite($fopen, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)), zip_entry_filesize($zip_entry));
				}
				zip_entry_close($zip_entry);
			}
			zip_close($zip);
			return true;
		}*/
		
		//
		function unzip($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true)
		{
		  if(function_exists("zip_open"))
		  {  
			  if(!is_resource(zip_open($src_file)))
			  {
				  $src_file=dirname($_SERVER['SCRIPT_FILENAME'])."/".$src_file;
			  }
			 
			  if (is_resource($zip = zip_open($src_file)))
			  {         
				  $splitter = ($create_zip_name_dir === true) ? "." : "/";
				  if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";
				
				  // Create the directories to the destination dir if they don't already exist
				  $this->create_dirs($dest_dir);
		
				  // For every file in the zip-packet
				  while ($zip_entry = zip_read($zip))
				  {
					// Now we're going to create the directories in the destination directories
				  
					// If the file is not in the root dir
					$pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
					if ($pos_last_slash !== false)
					{
					  // Create the directory where the zip-entry should be saved (with a "/" at the end)
					  $this->create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
					}
		
					// Open the entry
					if (zip_entry_open($zip,$zip_entry,"r"))
					{
					
					  // The name of the file to save on the disk
					  $file_name = $dest_dir.zip_entry_name($zip_entry);
					
					  // Check if the files should be overwritten or not
					  if ($overwrite === true || $overwrite === false && !is_file($file_name))
					  {
						// Get the content of the zip entry
						if (zip_entry_filesize($zip_entry)!=0){
							$fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));          
						   
							if(!is_dir($file_name)){    
								if (!function_exists('file_put_contents')) {
									$this->my_file_put_contents($file_name, $fstream );      
								}else{
									file_put_contents($file_name, $fstream );
								}
							}
							
							// Set the rights
							if(file_exists($file_name))
							{
								chmod($file_name, 0644);
								//echo "<span style=\"color:#1da319;\">file saved: </span>".$file_name."<br />";
							}
							else
							{
								echo "<span style=\"color:red;\">file not found: </span>".$file_name."<br />";
							}
						}
					  }
					
					  // Close the entry
					  zip_entry_close($zip_entry);
					}     
				  }
				  // Close the zip-file
				  zip_close($zip);
			  }
			  else
			  {
				echo "No Zip Archive Found.";
				return false;
			  }
			
			  return true;
		  }
		  else
		  {
			  if(version_compare(phpversion(), "5.2.0", "<"))
			  $infoVersion="(use PHP 5.2.0 or later)";
			 
			  echo "You need to install/enable the php_zip.dll extension $infoVersion";
		  }
		}
		
		function create_dirs($path)
		{
		  if (!is_dir($path))
		  {
			$directory_path = "";
			$directories = explode("/",$path);
			array_pop($directories);
		  
			foreach($directories as $directory)
			{
			  $directory_path .= $directory."/";
			  if (!is_dir($directory_path))
			  {
				mkdir($directory_path);
				chmod($directory_path, 0755);
			  }
			}
		  }
		}
		
		
		function my_file_put_contents($filename, $data) {
			$f = @fopen($filename, 'w');
			if (!$f) {
				return false;
			} else {
				$bytes = fwrite($f, $data);
				fclose($f);
				return $bytes;
			}
		}
	
		//function set % to code
		function setPrc2Code($string){
			return str_replace("%","c1r8p",$string);
		}
		
		//function set code to %
		function setCode2Prc($string){
			return str_replace("c1r8p","%",$string);
		}
		 //function send alert @ supplier
		function sendAlertSupplier($supplier_name,$section,$key,$email_to,$cc,$bcc){
			global $db;

			$query = $db->prepare_query("SELECT lname FROM lumonata_accommodation WHERE lacco_id=%s",$key);
			$result=$db->do_query($query);
	$data=$db->fetch_array($result);
			$acco_name = $data['lname'];
	$email_content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
						<html xmlns=\"http://www.w3.org/1999/xhtml\">
						<head>
						<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
						<title>hotelsarchipelago.com Supplier Panel Alert</title>
						</head>
						<body>
						<p>Dear hotelsarchipelago.com Admin,</p><br />
						<p>".$supplier_name." just updated ".$section." of ".$acco_name."</p>
						</body></html>";
	ini_set("SMTP",SMTP_SERVER);
	ini_set("sendmail_from","info@hotelsarchipelago.com");
			$email_subject = $acco_name." updated by ".$supplier_name;
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: hotelsarchipelago.com Supplier Panel<noreply@hotelsarchipelago.com>\r\nReply-To :noreply@hotelsarchipelago.com\r\n";
			$headers .= "Cc: ".$cc."\r\n";
	$headers .= "Bcc: ".$bcc."\r\n";
			$send=mail($email_to,$email_subject,$email_content,$headers);
			if ($send){
				return true;
			}else{
				return false;
			}
	
		}
};

?>