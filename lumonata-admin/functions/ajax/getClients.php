<?  
require_once("../../../config.php");
require_once("../../../functions/db.php");
require_once("../../functions/globalAdmin.php");
require_once("../../apps/Clients/clientsCategories/clientsCategories.php");
$clients_categories = new clients_categories();		
$globalAdmin = new globalAdmin();
$id = $_GET['obj_val'];
$i = $_GET['i'];
if (empty($id)){
	echo  "<div class=\"error\"><b>Please select personal detail clients</b></div>";
}else{
$query = $db->prepare_query("select * from lumonata_clients where lclient_id =%d",$id);
$result=$db->query($query);
$data=$db->fetch_array($result);
$required = "<img src=\"http://".IMAGE_DIR."/required.gif\">";
$cat_id = $clients_categories->select_option($data['lcat_id'],$i);	
if ($data['lstatus'] == 0){
	$check1="";
	$check2="checked=\"checked\"";
}elseif ($data['lstatus'] == 1){
	$check1="checked=\"checked\"";
	$check2="";
}
$salutation = $globalAdmin->setSelectOption("Select * from lumonata_salutation order by lorder_id","salutation_id","Salutation","lsalutation_id","lsalutation",$data['lsalutation_id'],$i); 
$nationality =  $globalAdmin->setSelectOption("Select * from lumonata_negara order by lorder_id","nationality_id","Nationality","lnegara_id","lnegara",$data['lnationality_id'],$i); 
$country =  $globalAdmin->setSelectOption("Select * from lumonata_negara order by lorder_id","country_id","Country","lnegara_id","lnegara",$data['lnegara_id'],$i); 
$age =  $globalAdmin->setSelectOptionNull("Select * from lumonata_age order by lorder_id","age_id","Age","lage_id","lage",$data['lage_id'],$i); 
$occupation = $globalAdmin->setSelectOptionNull("Select * from lumonata_occupation order by lorder_id","occupation_id","Occupation","loccupation_id","loccupation",$data['loccupation_id'],$i); 
$lifestyle =  $globalAdmin->setSelectOptionNull("Select * from lumonata_lifestyle order by lorder_id","lifestyle_id","Lifestyle","llifestyle_id","llifestyle",$data['llifestyle_id'],$i); 
$source =  $globalAdmin->setSelectOptionNull("Select * from lumonata_source order by lorder_id","source_id","Source","lsource_id","lsource",$data['lsource_id'],$i); 	
echo"
<table width=\"100%\" cellpadding=\"2\" cellspacing=\"1\" style=\"background:#EFEFEF;\">
<tr >
		<td width=\"15%\">Category $required</td>
		<td width=\"85%\">$cat_id</td>
	</tr>
	<tr>
		  <td >Newsletter Status $required</td>
		  <td ><input name=\"status[$i]\" id=\"status$i\" value=\"1\" $check1 class=\"inputbox\" size=\"1\" type=\"radio\">
		<label for=\"block1\">Subscribcre</label>
	<input name=\"status[$i]\" id=\"status$i\" value=\"0\" $check2 class=\"inputbox\" size=\"1\" type=\"radio\">
	<label for=\"block2\">Unsubscribe</label>
		</td>
	</tr>
<tr >
		<td width=\"15%\">Salutation $required</td>
		<td width=\"85%\">$salutation</td>
	</tr>
	<tr >
		<td>Full Name $required</td>
		<td ><input name=\"name[$i]\" id=\"name$i\" type=\"text\" value=\"".$data['lname']."\" class=\"jsrequired\" style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Nationality $required</td>
		<td>$nationality</td>
	</tr>
	<tr >
		<td>Address </td>
		<td ><input name=\"address[$i]\" id=\"address$i\" type=\"text\" value=\"".$data['laddress']."\" style=\"width:350px;\"/></td>
	</tr>
	<tr >
		<td>City </td>
		<td ><input name=\"city[$i]\" id=\"city$i\" type=\"text\" value=\"".$data['lcity']."\" style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Country $required</td>
		<td>$country</td>
	</tr>
	<tr >
		<td>Zip Code </td>
		<td ><input name=\"zip[$i]\" id=\"zip$i\" type=\"text\" value=\"".$data['lzip']."\"  style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Home/Work Phone </td>
		<td ><input name=\"phone[$i]\" id=\"phone$i\" type=\"text\" value=\"".$data['lphone']."\"  style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Mobile Phone </td>
		<td ><input name=\"mobile[$i]\" id=\"mobile$i\" type=\"text\" value=\"".$data['lmobile']."\"  style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Fax </td>
		<td ><input name=\"fax[$i]\" id=\"fax$i\" type=\"text\" value=\"".$data['lfax']."\"  style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Age </td>
		<td>$age</td>
	</tr>
	<tr >
		<td>Occupation </td>
		<td>$occupation</td>
	</tr>
	<tr >
		<td>Lifestyle </td>
		<td>$lifestyle</td>
	</tr>
	<tr >
		<td>Email Address $required</td>
		<td ><input name=\"email[$i]\" id=\"email$i\" type=\"text\" value=\"".$data['lemail']."\" class=\"jsrequired jsvalidate_email\" style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Alternate Address </td>
		<td ><input name=\"email_alternate[$i]\" id=\"email_alternate$i\" type=\"text\" value=\"".$data['lemail_alternate']."\" class=\"jsvalidate_email\" style=\"width:150px;\"/></td>
	</tr>
	<tr >
		<td>Source </td>
		<td>$source</td>
	</tr>
</table>";
}
?>