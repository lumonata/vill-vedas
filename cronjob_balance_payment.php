<?php 
require_once('lumonata_config.php');
require_once('lumonata_settings.php');
require_once('lumonata-functions/settings.php');
require_once('lumonata-plugins/reservation/class.email_reservation.php');
global $db;

$email_reservation = new email_reservation();
$limit = 20;$no_send=0;
$dsb = get_meta_data('days_crojob_final_payment') * 86400;
if(!defined('SITE_URL')) define('SITE_URL',get_meta_data('site_url'));
$timeNow = time();
//echo $dsb.'#';
$q = $db->prepare_query("select * from lumonata_accommodation_booking lab
						where lab.lstatus=%d and 
						not exists(select * from lumonata_send_email_history lseh 
						where lab.lbook_id=lseh.lbook_id and lseh.lapp_name=%s)",6,'balance_email');
$r = $db->do_query($q);
while($dt = $db->fetch_array($r)){
	$cronjob_send = $dt['lbalance'] - $dsb;
	if($cronjob_send <= $timeNow && $no_send <=$limit){
		$message = get_content_balance_email($dt);
		send_balance_email($dt,$message);
		$email_reservation->save_send_email_history($dt['lbook_id'],time(),'balance_email');
		update_status_booking($dt['lbook_id'],3);
		$no_send++;
	}//end if send now
}

function update_status_booking($book_id,$status){
	global $db;
	$qu = $db->prepare_query("update lumonata_accommodation_booking set lstatus=%d where lbook_id=%s",$status,$book_id);				
	$db->do_query($qu);
}

function send_balance_email($dbk,$message){
	global $db;	
	require_once('lumonata-plugins/reservation/class.email_reservation.php');
	$email_reservation = new email_reservation();
	
	$dtContact = $email_reservation->contact_admin();
	$contact_us_email = $dtContact['cu_email'];
	$contact_us_email_cc = $dtContact['cu_email_cc'];
	//echo 'dsd';
	$data_smtp = $email_reservation->smtp_info();
	$email_smtp = $data_smtp['email'];
	$pass_smtp = $data_smtp['pass'];
	$web_name = get_meta_data('web_title');
	$SMTP_SERVER = get_meta_data('smtp');
	$to = $dbk['lemail'];
	$guest_name = $dbk['lfirst_name'].' '.$dbk['llast_name'];
	//echo $message;exit;
	//echo "$contact_us_email,$contact_us_email_cc,$email_smtp,$pass_smtp,$web_name,$SMTP_SERVER,$to,$guest_name";exit;	
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
	$mail = new PHPMailer(true);	
	try {
		$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Port       = 2525; 
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($contact_us_email, $web_name);
		$mail->AddAddress($to,$guest_name);
		$mail->SetFrom($contact_us_email, $web_name);
		$mail->Subject = $subject;
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$mail->AddAttachment($url_pdf);
		$mail->Send();
	}catch (phpmailerException $e) {
		echo $e->errorMessage().$web_name;
	}catch (Exception $e) {
		echo $e->getMessage();
	}
}

function get_content_balance_email($dt){
	require_once('lumonata-plugins/reservation/class.email_reservation.php');
	require_once('lumonata-functions/functions/template.inc');
	$email_reservation = new email_reservation();
	
	$OUT_TEMPLATE="temp-email-waiting-final-payment.html";
	$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html");
	$t->set_file('template_ecpa', $OUT_TEMPLATE);
	$t->set_block('template_ecpa', 'ewfp_Block',  'mB');
	
	$dac =$email_reservation->get_detail_accommodation($dt['lbook_id']);
	$t->set_var('name',$dac['first_name'].' '.$dac['last_name']);
	$t->set_var('villa_name',$dac['larticle_title']);
	$t->set_var('check_in',date('d M Y', $dac['lcheck_in']));
	$t->set_var('check_out',date('d M Y', $dac['lcheck_out']));
	
	$url_array  = array();
	$url_array['val_date'] 	= time()+ (86400 * 3);
	$url_array['book_id'] 	= $dt['lbook_id'];
	$url_array['type'] 		= 'fp';
	
	$link = base64_encode(json_encode($url_array));
	$t->set_var('link','http://'.SITE_URL.'/payment/'.$link.'/');
	
	$booking_detail = $email_reservation->just_detail_booking($dt['lbook_id'],'fp_before');
	$t->set_var('booking_detail',$booking_detail);
	
	$t->set_var('best_regrads',$email_reservation->best_regrads_email());	
	$content = $t->Parse('mB', 'ewfp_Block', false);
	$content_email = $email_reservation->set_content_to_main_email_template('',$content);
	
	//print_r($dac);
	return $content_email;
}


?>