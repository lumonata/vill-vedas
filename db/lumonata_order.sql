-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Dec 02, 2011 at 10:04 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `baliworking4u`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order`
-- 

CREATE TABLE `lumonata_order` (
  `lorder_id` varchar(50) NOT NULL,
  `lmember_id` varchar(50) NOT NULL,
  `lorder_shipping_id` varchar(50) NOT NULL,
  `lorder_shipping_status` int(5) NOT NULL COMMENT '1=difference;0=same;',
  `lorder_date` datetime NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '1=Pending; 2=Paid, 3=Cancelled, 4=Partially Shipped,5=Shipped',
  `lstatus_archive` int(11) NOT NULL,
  `litem_total` int(10) NOT NULL,
  `lquantity_total` int(10) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lshipping_price` decimal(10,2) NOT NULL,
  `tax_product` decimal(10,2) NOT NULL,
  `tax_shipping` decimal(10,2) NOT NULL,
  `lprice_total` decimal(10,2) NOT NULL,
  `lpayment_method` varchar(60) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lorder_id`,`llang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `lumonata_order`
-- 

INSERT INTO `lumonata_order` (`lorder_id`, `lmember_id`, `lorder_shipping_id`, `lorder_shipping_status`, `lorder_date`, `lstatus`, `lstatus_archive`, `litem_total`, `lquantity_total`, `lcurrency_id`, `lshipping_price`, `tax_product`, `tax_shipping`, `lprice_total`, `lpayment_method`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
('OI111200002', 'MI111200002', 'OS111200002', 0, '2011-12-02 11:10:54', 1, 0, 3, 4, 6, 10.00, 0.73, 1.00, 73.00, 'payments_cheque', 'MI111200002', '2011-12-02 11:10:54', 'MI111200002', '2011-12-02 11:10:54', 0),
('OI111200001', 'MI111200001', 'OS111200001', 0, '2011-12-02 10:42:00', 2, 0, 1, 1, 6, 2.00, 0.02, 0.04, 2.00, 'payments_bank_transfer', 'MI111200001', '2011-12-02 10:42:00', 'MI111200001', '2011-12-02 10:42:00', 0);
