-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Dec 16, 2011 at 10:03 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `baliworking4u`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_additional_fields`
-- 

CREATE TABLE `lumonata_additional_fields` (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) character set utf8 NOT NULL,
  `lvalue` text character set utf8 NOT NULL,
  `lapp_name` varchar(200) character set utf8 NOT NULL,
  PRIMARY KEY  (`lapp_id`,`lkey`),
  KEY `key` (`lkey`),
  KEY `app_name` (`lapp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_additional_fields`
-- 

INSERT INTO `lumonata_additional_fields` (`lapp_id`, `lkey`, `lvalue`, `lapp_name`) VALUES 
(83, 'meta_keywords', 'product', 'pages'),
(84, 'meta_title', 'How To Order', 'pages'),
(3, 'meta_title', '', 'articles'),
(3, 'meta_description', '', 'articles'),
(3, 'meta_keywords', '', 'articles'),
(1, 'first_name', 'angelina', 'user'),
(1, 'last_name', 'jolie', 'user'),
(1, 'website', 'http://', 'user'),
(1, 'bio', '', 'user'),
(83, 'meta_title', 'product', 'pages'),
(83, 'meta_description', 'product', 'pages'),
(2, 'invite_limit', '10', 'user'),
(2, 'first_name', 'contributor', 'user'),
(2, 'last_name', 'user', 'user'),
(2, 'website', 'http://nolink.com', 'user'),
(1, 'one_liner', '', 'user'),
(1, 'location', '', 'user'),
(1, 'invite_limit', '-1', 'user'),
(3, 'invite_limit', '10', 'user'),
(3, 'first_name', 'userklien', 'user'),
(3, 'last_name', 're', 'user'),
(7, 'meta_title', '', 'articles'),
(7, 'meta_description', '', 'articles'),
(7, 'meta_keywords', '', 'articles'),
(11, 'meta_title', '', 'pages'),
(11, 'meta_description', '', 'pages'),
(11, 'meta_keywords', '', 'pages'),
(15, 'meta_title', '', 'pages'),
(15, 'meta_description', '', 'pages'),
(15, 'meta_keywords', '', 'pages'),
(16, 'meta_title', '', 'pages'),
(16, 'meta_description', '', 'pages'),
(16, 'meta_keywords', '', 'pages'),
(95, 'product_image_thumbnail', '295', 'products'),
(95, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"573":["65"]}]}', 'products'),
(94, 'product_image_thumbnail', '296', 'products'),
(94, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"566":["105"]}]}', 'products'),
(85, 'meta_description', 'About Us', 'pages'),
(84, 'meta_description', 'How To Order', 'pages'),
(84, 'meta_keywords', 'How To Order', 'pages'),
(85, 'meta_title', 'About Us', 'pages'),
(97, 'product_additional', '{"code":"0057","price":"200","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"0"}', 'products'),
(502, 'categories_image_thumbnail', '260', 'categories'),
(92, 'product_additional', '{"code":"0053","price":"90","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"1"}', 'products'),
(92, 'product_image_thumbnail', '294', 'products'),
(92, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"567":["90"]}]}', 'products'),
(505, 'categories_image_thumbnail', '300', 'categories'),
(93, 'product_image_thumbnail', '297', 'products'),
(93, 'product_variant', '{"child_variant":[{"572":["175"],"571":["175"],"570":["175"]}],"parent_variant":["493"]}', 'products'),
(97, 'product_image_thumbnail', '293', 'products'),
(97, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"565":["200"]}]}', 'products'),
(86, 'meta_title', 'Contact Us', 'pages'),
(86, 'meta_description', 'Contact Us', 'pages'),
(86, 'meta_keywords', 'Contact Us', 'pages'),
(85, 'meta_keywords', 'About Us', 'pages'),
(593, 'minimum_price', '2', 'categories_price'),
(102, 'product_image_thumbnail', '307', 'products'),
(102, 'product_additional', '{"code":"0050","price":"200","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":""}', 'products'),
(101, 'product_image_thumbnail', '306', 'products'),
(101, 'product_additional', '{"code":"0049","price":"140","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"0"}', 'products'),
(94, 'product_additional', '{"code":"0055","price":"105","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"1"}', 'products'),
(506, 'categories_image_thumbnail', '301', 'categories'),
(98, 'product_image_thumbnail', '302', 'products'),
(98, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"574":["90"]}]}', 'products'),
(98, 'product_additional', '{"code":"0061","price":"90","weight":"0","tax":"70","stock_limit":"2","product_buy_out_of_stock":"1"}', 'products'),
(99, 'product_image_thumbnail', '304', 'products'),
(99, 'product_additional', '{"code":"0062","price":"90","weight":"0","tax":"70","stock_limit":"0","product_buy_out_of_stock":"1"}', 'products'),
(99, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"574":["90"]}]}', 'products'),
(100, 'product_image_thumbnail', '305', 'products'),
(100, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"565":["0.00"]}]}', 'products'),
(100, 'product_additional', '{"code":"0048","price":"280","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"1"}', 'products'),
(93, 'product_additional', '{"code":"0054","price":"175","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":"1"}', 'products'),
(95, 'product_additional', '{"code":"0056","price":"65","weight":"0","tax":"70","stock_limit":"2","product_buy_out_of_stock":"1"}', 'products'),
(102, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"589":["200"]}]}', 'products'),
(103, 'product_image_thumbnail', '309', 'products'),
(103, 'product_additional', '{"code":"0051","price":"320","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":""}', 'products'),
(103, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"590":["320"]}]}', 'products'),
(104, 'product_image_thumbnail', '311', 'products'),
(104, 'product_additional', '{"code":"0052","price":"600","weight":"0","tax":"70","stock_limit":"","product_buy_out_of_stock":""}', 'products'),
(104, 'product_variant', '{"parent_variant":["493"],"child_variant":[{"591":["600"]}]}', 'products'),
(504, 'categories_image_thumbnail', '313', 'categories'),
(105, 'meta_title', '', 'pages'),
(105, 'meta_description', '', 'pages'),
(105, 'meta_keywords', '', 'pages'),
(594, 'minimum_price', '12', 'categories_price'),
(595, 'minimum_price', '0', 'categories_price'),
(86, 'contact_us_email', 'widia@lumonata.com', 'contact-us'),
(86, 'contact_us_cc', '', 'contact-us'),
(86, 'contact_us_bcc', '', 'contact-us'),
(126, 'product_image_thumbnail', '316', 'products'),
(126, 'product_additional', '{"code":"12312","price":"12","weight":"0","tax":"70","stock_limit":"2","product_buy_out_of_stock":"1"}', 'products'),
(506, 'minimum_price', '100', 'categories_price'),
(596, 'minimum_price', '0', 'categories_price'),
(597, 'minimum_price', '0', 'categories_price'),
(505, 'minimum_price', '50', 'categories_price'),
(504, 'minimum_price', '600', 'categories_price'),
(155, 'product_image_thumbnail', '333', 'products'),
(155, 'product_variant', '{"child_variant":[{"570":"2","567":"2","566":"1"}],"parent_variant":["493"]}', 'products'),
(155, 'product_additional', '{"code":"123","price":"1","weight":"2","tax":"70","stock_limit":"2","product_buy_out_of_stock":"1"}', 'products');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_articles`
-- 

CREATE TABLE `lumonata_articles` (
  `larticle_id` bigint(20) NOT NULL auto_increment,
  `larticle_title` text character set utf8 NOT NULL,
  `larticle_brief` text character set utf8 NOT NULL,
  `larticle_content` longtext character set utf8 NOT NULL,
  `larticle_status` varchar(20) character set utf8 NOT NULL,
  `larticle_type` varchar(20) character set utf8 NOT NULL,
  `lcomment_status` varchar(20) character set utf8 NOT NULL,
  `lcomment_count` bigint(20) NOT NULL,
  `lcount_like` bigint(20) NOT NULL,
  `lsef` text character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL default '1',
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY  (`larticle_id`),
  KEY `article_title` (`larticle_title`(255)),
  KEY `type_status_date_by` (`larticle_type`,`larticle_status`,`lpost_date`,`lpost_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=158 ;

-- 
-- Dumping data for table `lumonata_articles`
-- 

INSERT INTO `lumonata_articles` (`larticle_id`, `larticle_title`, `larticle_brief`, `larticle_content`, `larticle_status`, `larticle_type`, `lcomment_status`, `lcomment_count`, `lcount_like`, `lsef`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`, `lshare_to`) VALUES 
(1, 'My First Status', '', '', 'publish', 'status', 'allowed', 0, 0, 'my-first-status', 157, 1, '2011-03-18 21:46:22', 1, '2011-03-18 21:46:22', 0),
(85, 'About Us', '', '<p>welcome to baliworking4u.com.au&nbsp; Thankyou for viewing our web site. We are two friends and mum''s that now live in Bali who have combined our knowledge and passion to provide you (our valued customer) the best product''s, quality and service. We have both prevoiusly owned and operated business''s in furniture and homewares for the past 15 years, that involved travelling the world sourcing unique products to please our clients. As Bali is now our home we can both souce any products you desire. We have photo''s on line of our best sellers and once again if you cant find what you want just send us an email of what products you require and we will do our best to source for you. We have a close working relationship with our bali suppliers so we can offer you the best product''s at the best prices.</p>', 'publish', 'pages', 'allowed', 0, 0, 'about-us', 74, 1, '2011-11-03 11:33:04', 1, '2011-12-08 09:52:28', 0),
(86, 'Contact Us', '', '', 'publish', 'contact-us', 'allowed', 0, 0, 'contact-us', 75, 1, '2011-11-03 11:33:47', 1, '2011-12-09 14:11:25', 0),
(103, 'wagon wheel', '', '<p>authentic</p>', 'publish', 'products', 'not_allowed', 0, 0, 'wagon-wheel', 55, 1, '2011-12-06 17:30:37', 1, '2011-12-06 17:30:37', 0),
(104, 'large day bed', '', '<p>antique white wash</p>', 'publish', 'products', 'not_allowed', 0, 0, 'large-day-bed', 54, 1, '2011-12-06 17:34:03', 1, '2011-12-06 17:34:03', 0),
(3, 'Welcome to Arunna', '', '<p>Hello Friends,</p>\r\n<p>Let''s share to make a better world</p>', 'publish', 'articles', 'allowed', 2, 1, 'welcome-arunna-training', 155, 1, '2011-03-18 21:51:45', 1, '2011-09-01 10:14:14', 0),
(4, 'Halloo...', '', '', 'publish', 'status', 'allowed', 0, 0, 'halloo', 154, 1, '2011-09-01 10:07:16', 1, '2011-09-01 10:07:16', 1),
(83, 'Product', '', '', 'publish', 'pages', 'allowed', 0, 0, 'product', 72, 1, '2011-11-03 11:31:03', 1, '2011-11-03 03:33:59', 0),
(84, 'How to Order', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna  arcu, eleifend in fermentum vitae, commodo nec nulla. Sed commodo,  sapien sit amet tincidunt feugiat, nibh nisl vehicula odio, at luctus  nisi eros sed risus. Mauris tellus mauris, aliquet sit amet pretium  vitae, suscipit in massa. Nam suscipit mollis dui, quis consequat tellus  tincidunt nec. Nulla suscipit justo vel mauris lobortis quis posuere  tellus lobortis. Donec pharetra interdum enim. Vestibulum aliquam  pretium ipsum non dictum. Phasellus in malesuada odio. Vestibulum a sem  erat. Nam ac velit lorem, sit amet fermentum dui. Phasellus varius risus  ullamcorper quam gravida vel pretium lorem hendrerit. Suspendisse  venenatis consequat imperdiet. Vivamus ut felis aliquet dolor convallis  aliquam luctus et eros.</p>\r\n<p>Praesent ullamcorper molestie eros ac eleifend. Nunc nibh urna, tempor a  pharetra quis, consequat sit amet ligula. Vestibulum a commodo odio.  Aliquam sed nulla non diam bibendum adipiscing. Integer posuere dui non  purus suscipit facilisis. Pellentesque sit amet tempus erat. Sed sit  amet ullamcorper lorem. Morbi ut arcu ut sapien suscipit congue. Nunc ut  sagittis dui. Maecenas tristique dapibus lectus eu porta. Fusce sed  auctor mi. In in cursus odio. Aenean auctor sodales dui et tincidunt.</p>', 'publish', 'pages', 'allowed', 0, 0, 'how-to-order', 73, 1, '2011-11-03 11:31:36', 1, '2011-12-08 14:05:24', 0),
(99, 'painting', '', '<p>painting</p>', 'publish', 'products', 'not_allowed', 0, 0, 'painting', 59, 1, '2011-12-06 15:11:56', 1, '2011-12-06 15:11:56', 0),
(98, 'painting', '', '<p>painting</p>', 'publish', 'products', '', 0, 0, 'painting', 60, 1, '2011-12-06 15:08:13', 1, '2011-12-06 18:03:02', 0),
(94, 'vase', '', '<p>black gold/black silver</p>', 'publish', 'products', '', 0, 0, 'vase', 64, 1, '2011-12-06 11:04:21', 1, '2011-12-06 17:05:32', 0),
(95, 'Lamp', '', '<p>black gold</p>', 'publish', 'products', '', 0, 0, 'lamp', 63, 1, '2011-12-06 11:06:21', 1, '2011-12-06 17:04:28', 0),
(97, 'buddha head hanging', '', '<p>antique finishing red finishing</p>', 'publish', 'products', '', 0, 0, 'buddha-head-hanging', 61, 1, '2011-12-06 11:37:40', 1, '2011-12-06 17:03:12', 0),
(105, 'Sourcing &amp; Buying Agent', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed adipiscing,  nibh sit amet dignissim mollis, orci est cursus nunc, sit amet egestas  dui diam hendrerit ante. Sed luctus commodo dignissim. Phasellus  pulvinar convallis molestie. Donec hendrerit metus id magna tincidunt eu  pulvinar nisi fringilla. Ut porttitor ligula sit amet ligula  consectetur ut iaculis urna vestibulum. Curabitur vel lacus dolor, nec  convallis sem. Duis egestas, enim vitae pellentesque pharetra, velit dui  gravida tellus, non convallis nunc urna euismod tellus. Aenean lacinia  libero nec nunc consequat dictum. Donec elit nunc, tempor vel semper  non, porta non mi. Vivamus varius felis a augue faucibus eget  condimentum justo volutpat. Duis ante purus, facilisis sit amet  ultricies cursus, scelerisque sit amet magna. In hac habitasse platea  dictumst. Nam porta, risus vel ultrices iaculis, lorem elit porttitor  velit, vel auctor leo felis ut neque. Praesent fringilla elementum  vehicula. Nulla sodales convallis dictum.</p>', 'publish', 'pages', 'allowed', 0, 0, 'sourcing-buying-agent', 53, 1, '2011-12-07 18:38:01', 1, '2011-12-08 09:39:32', 0),
(102, 'hall table', '', '<p>antique white wash</p>', 'publish', 'products', 'not_allowed', 0, 0, 'hall-table', 56, 1, '2011-12-06 17:26:44', 1, '2011-12-06 17:26:44', 0),
(101, 'antique wood bird', '', '<p>as per photo</p>\r\n<p>Set of 3 Fern Vases<br />White with Gold or Black with Gold<br />L H:120cm x 30cm<br />M H:100cm x 28cm<br />S H: 80cm x 25cm</p>', 'publish', 'products', '', 0, 0, 'antique-wood-bird', 57, 1, '2011-12-06 17:20:57', 1, '2011-12-15 12:46:01', 0),
(93, 'vase set of 3', '', '<p>white gold</p>\r\n<p><span class="additional-fontsize">Set of 3 Fern Vases<br />White with Gold or Black with Gold<br />L H:120cm x 30cm<br />M H:100cm x 28cm<br />S H: 80cm x 25cm</span></p>', 'publish', 'products', '', 0, 0, 'vase-set-of-3', 65, 1, '2011-12-06 10:48:24', 1, '2011-12-15 13:26:35', 0),
(100, 'day bed', '', '<p>antique white wash</p>', 'publish', 'products', '', 0, 0, 'day-bed', 58, 1, '2011-12-06 17:15:24', 1, '2011-12-12 13:10:03', 0),
(92, 'buddha head', '', '<p>white wash antique brown</p>', 'publish', 'products', '', 0, 0, 'buddha-head', 66, 1, '2011-12-06 10:39:14', 1, '2011-12-06 17:07:19', 0),
(126, 'large day bed', '', '<p>asd</p>', 'publish', 'products', '', 0, 0, 'large-day-bed-2', 32, 1, '2011-12-12 14:39:19', 1, '2011-12-15 13:17:39', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_attachment`
-- 

CREATE TABLE `lumonata_attachment` (
  `lattach_id` bigint(20) NOT NULL auto_increment,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text character set utf8 NOT NULL,
  `lattach_loc_thumb` text character set utf8 NOT NULL,
  `lattach_loc_medium` text character set utf8 NOT NULL,
  `lattach_loc_large` text character set utf8 NOT NULL,
  `ltitle` varchar(200) character set utf8 NOT NULL,
  `lalt_text` text character set utf8 NOT NULL,
  `lcaption` varchar(200) character set utf8 NOT NULL,
  `mime_type` varchar(50) character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL default '0',
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY  (`lattach_id`),
  KEY `article_id` (`larticle_id`),
  KEY `attachment_title` (`ltitle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=337 ;

-- 
-- Dumping data for table `lumonata_attachment`
-- 

INSERT INTO `lumonata_attachment` (`lattach_id`, `larticle_id`, `lattach_loc`, `lattach_loc_thumb`, `lattach_loc_medium`, `lattach_loc_large`, `ltitle`, `lalt_text`, `lcaption`, `mime_type`, `lorder`, `upload_date`, `date_last_update`) VALUES 
(4, 1316070102, '/lumonata-content/files/201109/nobita4.jpg', '/lumonata-content/files/201109/nobita4-thumbnail.jpg', '/lumonata-content/files/201109/nobita4-medium.jpg', '/lumonata-content/files/201109/nobita4-large.jpg', 'billabong', '', '', 'image/jpeg', 3, '2011-09-15 07:04:03', '2011-09-15 07:04:23'),
(3, 6, '/lumonata-content/files/201109/detail07.jpg', '/lumonata-content/files/201109/detail07-thumbnail.jpg', '/lumonata-content/files/201109/detail07-medium.jpg', '/lumonata-content/files/201109/detail07-large.jpg', 'detail07', '', '', 'image/jpeg', 2, '2011-09-01 05:59:40', '2011-09-07 07:37:33'),
(140, 1318556130, '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '', '', 'image/jpeg', 1, '2011-10-14 09:49:10', '0000-00-00 00:00:00'),
(303, 1323155293, '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155410.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0062-1323155410.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155410-1323155410-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155410-1323155410-large.jpg', 'code0062-1323155410.jpg', '', '', 'image/jpeg', 1, '2011-12-06 15:10:10', '0000-00-00 00:00:00'),
(131, 59, '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:14:25', '0000-00-00 00:00:00'),
(130, 59, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:14:22', '0000-00-00 00:00:00'),
(129, 58, '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '766519b74a562c473afa9fce860443c6-1318493421.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:10:21', '0000-00-00 00:00:00'),
(137, 1318555717, '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', 'ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '', '', 'image/jpeg', 1, '2011-10-14 09:29:09', '0000-00-00 00:00:00'),
(194, 1320380814, '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320380833.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/0c4304dc08e12046253238703be407ec-1320380833.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320380833.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320380833.jpg', '0c4304dc08e12046253238703be407ec-1320380833.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:27:13', '0000-00-00 00:00:00'),
(260, 502, '/lumonata-plugins/shopping_cart/uploads/categories/code-0005-1323137131.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/code-0005-1323137131.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code-0005-1323137131.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code-0005-1323137131.jpg', 'code-0005-1323137131.jpg', '', '', 'image/jpeg', 1, '2011-12-06 10:05:32', '0000-00-00 00:00:00'),
(252, 1320725735, '/lumonata-plugins/shopping_cart/uploads/categories/4animitatedstonestuddednecklaceset-designer-indian-wear-com-1320725747.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/4animitatedstonestuddednecklaceset-designer-indian-wear-com-1320725747.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/4animitatedstonestuddednecklaceset-designer-indian-wear-com-1320725747.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/4animitatedstonestuddednecklaceset-designer-indian-wear-com-1320725747.jpg', '4animitatedstonestuddednecklaceset-designer-indian-wear-com-1320725747.jpg', '', '', 'image/jpeg', 1, '2011-11-08 12:15:47', '0000-00-00 00:00:00'),
(175, 1319593685, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '', '', 'image/jpeg', 1, '2011-10-26 09:48:11', '0000-00-00 00:00:00'),
(178, 1319594171, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '', '', 'image/jpeg', 1, '2011-10-26 09:56:22', '0000-00-00 00:00:00'),
(302, 98, '/lumonata-plugins/shopping_cart/uploads/product/code0061-1323155288.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0061-1323155288.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0061-1323155288-1323155288-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0061-1323155288-1323155288-large.jpg', 'code0061-1323155288.jpg', '', '', 'image/jpeg', 1, '2011-12-06 15:08:09', '0000-00-00 00:00:00'),
(294, 92, '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150910.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0053-1323150910.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150910-1323150910-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150910-1323150910-large.jpg', 'code0053-1323150910.jpg', '', '', 'image/jpeg', 1, '2011-12-06 13:55:14', '0000-00-00 00:00:00'),
(235, 487, '/lumonata-plugins/shopping_cart/uploads/categories/9bddd549f241fb9fd3f64a0d7a0a7869-1320719389.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/9bddd549f241fb9fd3f64a0d7a0a7869-1320719389.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/9bddd549f241fb9fd3f64a0d7a0a7869-1320719389.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/9bddd549f241fb9fd3f64a0d7a0a7869-1320719389.jpg', '9bddd549f241fb9fd3f64a0d7a0a7869-1320719389.jpg', '', '', 'image/jpeg', 1, '2011-11-08 10:29:49', '0000-00-00 00:00:00'),
(304, 99, '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155512.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0062-1323155512.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155512-1323155512-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0062-1323155512-1323155512-large.jpg', 'code0062-1323155512.jpg', '', '', 'image/jpeg', 1, '2011-12-06 15:11:53', '0000-00-00 00:00:00'),
(195, 1320381034, '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320381041.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/0c4304dc08e12046253238703be407ec-1320381041.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320381041.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1320381041.jpg', '0c4304dc08e12046253238703be407ec-1320381041.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:30:41', '0000-00-00 00:00:00'),
(196, 1320381091, '/lumonata-plugins/shopping_cart/uploads/product/m147-1320381097.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/m147-1320381097.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/m147-1320381097.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/m147-1320381097.gif.png', 'm147-1320381097.gif.png', '', '', 'image/png', 1, '2011-11-04 12:31:37', '0000-00-00 00:00:00'),
(197, 1320381102, '/lumonata-plugins/shopping_cart/uploads/product/m055-1320381114.gif (1).png', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/m055-1320381114.gif (1).png', '/lumonata-plugins/shopping_cart/uploads/product/m055-1320381114.gif (1).png', '/lumonata-plugins/shopping_cart/uploads/product/m055-1320381114.gif (1).png', 'm055-1320381114.gif (1).png', '', '', 'image/png', 1, '2011-11-04 12:31:54', '0000-00-00 00:00:00'),
(198, 1320381400, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1320381406.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1320381406.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1320381406.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1320381406.jpg', '58a1e51b1cc281df09785bfe179389b6-1320381406.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:36:46', '0000-00-00 00:00:00'),
(199, 1320381427, '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1320381432.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/85c4c19f9abefbc553bec3e0d9ee34c9-1320381432.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1320381432.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1320381432.jpg', '85c4c19f9abefbc553bec3e0d9ee34c9-1320381432.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:37:12', '0000-00-00 00:00:00'),
(200, 1320381788, '/lumonata-plugins/shopping_cart/uploads/product/images-1320381799.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/images-1320381799.jpg', '/lumonata-plugins/shopping_cart/uploads/product/images-1320381799.jpg', '/lumonata-plugins/shopping_cart/uploads/product/images-1320381799.jpg', 'images-1320381799.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:43:19', '0000-00-00 00:00:00'),
(201, 1320381817, '/lumonata-plugins/shopping_cart/uploads/product/m035-1320381824.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/m035-1320381824.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/m035-1320381824.gif.png', '/lumonata-plugins/shopping_cart/uploads/product/m035-1320381824.gif.png', 'm035-1320381824.gif.png', '', '', 'image/png', 1, '2011-11-04 12:43:44', '0000-00-00 00:00:00'),
(202, 1320381890, '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381895.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381895.jpg', '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381895.jpg', '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381895.jpg', '42cb37ab1ab3f9727c3bfd7cb19549b6-1320381895.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:44:55', '0000-00-00 00:00:00'),
(203, 1320381890, '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381939.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381939.jpg', '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381939.jpg', '/lumonata-plugins/shopping_cart/uploads/product/42cb37ab1ab3f9727c3bfd7cb19549b6-1320381939.jpg', '42cb37ab1ab3f9727c3bfd7cb19549b6-1320381939.jpg', '', '', 'image/jpeg', 1, '2011-11-04 12:45:39', '0000-00-00 00:00:00'),
(308, 1323163604, '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163761.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0051-1323163761.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163761-1323163761-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163761-1323163761-large.jpg', 'code0051-1323163761.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:29:22', '0000-00-00 00:00:00'),
(309, 103, '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163832.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0051-1323163832.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163832-1323163832-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0051-1323163832-1323163832-large.jpg', 'code0051-1323163832.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:30:33', '0000-00-00 00:00:00'),
(293, 97, '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150871.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0053-1323150871.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150871-1323150871-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323150871-1323150871-large.jpg', 'code0053-1323150871.jpg', '', '', 'image/jpeg', 1, '2011-12-06 13:54:34', '0000-00-00 00:00:00'),
(305, 100, '/lumonata-plugins/shopping_cart/uploads/product/code0048-1323162913.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0048-1323162913.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0048-1323162913-1323162913-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0048-1323162913-1323162913-large.jpg', 'code0048-1323162913.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:15:14', '0000-00-00 00:00:00'),
(306, 101, '/lumonata-plugins/shopping_cart/uploads/product/code0049-1323163249.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0049-1323163249.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0049-1323163249-1323163249-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0049-1323163249-1323163249-large.jpg', 'code0049-1323163249.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:20:50', '0000-00-00 00:00:00'),
(307, 102, '/lumonata-plugins/shopping_cart/uploads/product/code0050-1323163598.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0050-1323163598.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0050-1323163598-1323163598-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0050-1323163598-1323163598-large.jpg', 'code0050-1323163598.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:26:39', '0000-00-00 00:00:00'),
(301, 506, '/lumonata-plugins/shopping_cart/uploads/categories/code0061-1323154873.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/code0061-1323154873.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0061-1323154873-1323154873-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0061-1323154873-1323154873-large.jpg', 'code0061-1323154873.jpg', '', '', 'image/jpeg', 1, '2011-12-06 15:01:14', '0000-00-00 00:00:00'),
(297, 93, '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151184.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0054-1323151184.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151184-1323151184-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151184-1323151184-large.jpg', 'code0054-1323151184.jpg', '', '', 'image/jpeg', 1, '2011-12-06 13:59:48', '0000-00-00 00:00:00'),
(296, 94, '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151135.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0054-1323151135.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151135-1323151135-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0054-1323151135-1323151135-large.jpg', 'code0054-1323151135.jpg', '', '', 'image/jpeg', 1, '2011-12-06 13:58:59', '0000-00-00 00:00:00'),
(276, 1323142416, '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323142548.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0053-1323142548.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323142548.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0053-1323142548.jpg', 'code0053-1323142548.jpg', '', '', 'image/jpeg', 1, '2011-12-06 11:35:49', '0000-00-00 00:00:00'),
(295, 95, '/lumonata-plugins/shopping_cart/uploads/product/code0056-1323151077.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0056-1323151077.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0056-1323151077-1323151077-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0056-1323151077-1323151077-large.jpg', 'code0056-1323151077.jpg', '', '', 'image/jpeg', 1, '2011-12-06 13:58:02', '0000-00-00 00:00:00'),
(300, 505, '/lumonata-plugins/shopping_cart/uploads/categories/code0054-1323154477.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/code0054-1323154477.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0054-1323154477-1323154477-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0054-1323154477-1323154477-large.jpg', 'code0054-1323154477.jpg', '', '', 'image/jpeg', 1, '2011-12-06 14:54:41', '0000-00-00 00:00:00'),
(310, 1323163837, '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323163938.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0052-1323163938.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323163938-1323163938-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323163938-1323163938-large.jpg', 'code0052-1323163938.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:32:20', '0000-00-00 00:00:00'),
(311, 104, '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323164035.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0052-1323164035.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323164035-1323164035-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323164035-1323164035-large.jpg', 'code0052-1323164035.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:33:56', '0000-00-00 00:00:00'),
(313, 504, '/lumonata-plugins/shopping_cart/uploads/categories/code0051-1323165188.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/code0051-1323165188.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0051-1323165188-1323165188-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/categories/code0051-1323165188-1323165188-large.jpg', 'code0051-1323165188.jpg', '', '', 'image/jpeg', 1, '2011-12-06 17:53:09', '0000-00-00 00:00:00'),
(316, 126, '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323671953.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/code0052-1323671953.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323671953-1323671953-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/code0052-1323671953-1323671953-large.jpg', 'code0052-1323671953.jpg', '', '', 'image/jpeg', 1, '2011-12-12 14:39:14', '0000-00-00 00:00:00'),
(334, 1323755738, '/lumonata-plugins/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1323755792.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/3158613157106989521416251363309301581033102n-1323755792.jpg', '/lumonata-plugins/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1323755792-1323755792-medium.jpg', '/lumonata-plugins/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1323755792-1323755792-large.jpg', '3158613157106989521416251363309301581033102n-1323755792.jpg', '', '', 'image/jpeg', 1, '2011-12-13 13:56:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_comments`
-- 

CREATE TABLE `lumonata_comments` (
  `lcomment_id` bigint(20) NOT NULL auto_increment,
  `lcomment_parent` bigint(20) NOT NULL,
  `larticle_id` bigint(20) NOT NULL,
  `lcomentator_name` varchar(200) character set utf8 NOT NULL,
  `lcomentator_email` varchar(100) character set utf8 NOT NULL,
  `lcomentator_url` varchar(200) character set utf8 NOT NULL,
  `lcomentator_ip` varchar(100) character set utf8 NOT NULL,
  `lcomment_date` datetime NOT NULL,
  `lcomment` text character set utf8 NOT NULL,
  `lcomment_status` varchar(20) character set utf8 NOT NULL,
  `lcomment_like` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) character set utf8 NOT NULL COMMENT 'like,comment,like_comment',
  PRIMARY KEY  (`lcomment_id`),
  KEY `lcomment_status` (`lcomment_status`),
  KEY `lcomment_userid` (`luser_id`),
  KEY `lcomment_type` (`lcomment_type`),
  KEY `larticle_id` (`larticle_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `lumonata_comments`
-- 

INSERT INTO `lumonata_comments` (`lcomment_id`, `lcomment_parent`, `larticle_id`, `lcomentator_name`, `lcomentator_email`, `lcomentator_url`, `lcomentator_ip`, `lcomment_date`, `lcomment`, `lcomment_status`, `lcomment_like`, `luser_id`, `lcomment_type`) VALUES 
(1, 0, 3, 'Wahya Biantara', 'request@arunna.com', 'http://localhost/arunna-repo/?user=admin', '127.0.0.1', '2011-03-18 21:52:25', 'Hello comment....', 'approved', 0, 1, 'comment'),
(2, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://localhost/arunna_training/user/admin/', '127.0.0.1', '2011-09-01 11:36:28', 'hyh', 'approved', 0, 1, 'comment'),
(3, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://localhost/arunna_training/user/admin/', '127.0.0.1', '2011-09-01 11:38:51', 'like_post_3', 'approved', 0, 1, 'like');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_contact_us`
-- 

CREATE TABLE `lumonata_contact_us` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) collate latin1_general_ci NOT NULL,
  `email` varchar(60) collate latin1_general_ci NOT NULL,
  `message` text collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `lumonata_contact_us`
-- 

INSERT INTO `lumonata_contact_us` (`id`, `name`, `email`, `message`) VALUES 
(6, 'widia', 'widia@lumonata.com', 'testestestest'),
(5, 'widia', 'widia@lumonata.com', 'testestestest'),
(7, 'Input your name here', 'And email address please', 'Send us a couple of words'),
(8, 'Input your name here', 'And email address please', 'Send us a couple of words');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_country`
-- 

CREATE TABLE `lumonata_country` (
  `lcountry_id` int(11) NOT NULL auto_increment,
  `lcountry` varchar(255) collate latin1_general_ci NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) collate latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) collate latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY  (`lcountry_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=248 ;

-- 
-- Dumping data for table `lumonata_country`
-- 

INSERT INTO `lumonata_country` (`lcountry_id`, `lcountry`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(1, 'Ã…land Islands', 246, 'admin', 1241502068, '', 0),
(2, 'Afghanistan', 245, 'admin', 1241502081, '', 0),
(3, 'Albania', 244, 'admin', 1241502086, '', 0),
(4, 'Algeria', 243, 'admin', 1241502092, '', 0),
(5, 'American Samoa', 242, 'admin', 1241502100, '', 0),
(6, 'Andorra', 241, 'admin', 1241502105, '', 0),
(7, 'Angola', 240, 'admin', 1241502114, '', 0),
(8, 'Anguilla', 239, 'admin', 1241502121, '', 0),
(9, 'Antarctica', 238, 'admin', 1241502127, '', 0),
(10, 'Antigua And Barbuda', 237, 'admin', 1241502135, '', 0),
(11, 'Argentina', 236, 'admin', 1241502142, '', 0),
(12, 'Armenia', 235, 'admin', 1241502149, '', 0),
(13, 'Aruba', 234, 'admin', 1241502162, '', 0),
(14, 'Australia', 233, 'admin', 1241502166, '', 0),
(15, 'Austria', 232, 'admin', 1241502174, '', 0),
(16, 'Azerbaijan', 231, 'admin', 1241502182, '', 0),
(17, 'Bahamas', 230, 'admin', 1241502191, '', 0),
(18, 'Bahrain', 229, 'admin', 1241502197, '', 0),
(19, 'Bangladesh', 228, 'admin', 1241502203, '', 0),
(20, 'Barbados', 227, 'admin', 1241502209, '', 0),
(21, 'Belarus', 226, 'admin', 1241502216, '', 0),
(22, 'Belgium', 225, 'admin', 1241502222, '', 0),
(23, 'Belize', 224, 'admin', 1241502228, '', 0),
(24, 'Benin', 223, 'admin', 1241502233, '', 0),
(25, 'Bermuda', 222, 'admin', 1241502239, '', 0),
(26, 'Bhutan', 221, 'admin', 1241502247, '', 0),
(27, 'Bolivia', 220, 'admin', 1241502253, '', 0),
(28, 'Bosnia and Herzegovina', 219, 'admin', 1241502263, '', 0),
(29, 'Botswana', 218, 'admin', 1241502272, '', 0),
(30, 'Bouvet Island', 217, 'admin', 1241502281, '', 0),
(31, 'Brazil', 216, 'admin', 1241502289, '', 0),
(32, 'British Indian Ocean Territory', 215, 'admin', 1241502300, '', 0),
(33, 'Brunei', 214, 'admin', 1241502306, '', 0),
(34, 'Bulgaria', 213, 'admin', 1241502313, '', 0),
(35, 'Burkina Faso', 212, 'admin', 1241502321, '', 0),
(36, 'Burundi', 211, 'admin', 1241502329, '', 0),
(37, 'Cambodia', 210, 'admin', 1241502335, '', 0),
(38, 'Cameroon', 209, 'admin', 1241502341, '', 0),
(39, 'Canada', 208, 'admin', 1241502347, '', 0),
(40, 'Cape Verde', 207, 'admin', 1241502355, '', 0),
(41, 'Cayman Islands', 206, 'admin', 1241502363, '', 0),
(42, 'Central African Republic', 205, 'admin', 1241502371, '', 0),
(43, 'Chad', 204, 'admin', 1241502377, '', 0),
(44, 'Chile', 203, 'admin', 1241502384, '', 0),
(45, 'China', 202, 'admin', 1241502390, '', 0),
(46, 'Christmas Island', 201, 'admin', 1241502398, '', 0),
(47, 'Cocos (Keeling) Islands', 200, 'admin', 1241502411, '', 0),
(48, 'Colombia', 199, 'admin', 1241502465, '', 0),
(49, 'Comoros', 198, 'admin', 1241502499, '', 0),
(50, 'Congo', 197, 'admin', 1241502505, '', 0),
(51, 'Congo, Democractic Republic', 196, 'admin', 1241502513, '', 0),
(52, 'Cook Islands', 195, 'admin', 1241502521, '', 0),
(53, 'Costa Rica', 194, 'admin', 1241502531, '', 0),
(54, 'Cote D\\''Ivoire (Ivory Coast)', 193, 'admin', 1241502550, '', 0),
(55, 'Croatia (Hrvatska)', 192, 'admin', 1241502560, '', 0),
(56, 'Cuba', 191, 'admin', 1241502567, '', 0),
(57, 'Cyprus', 190, 'admin', 1241502573, '', 0),
(58, 'Czech Republic', 189, 'admin', 1241502581, '', 0),
(59, 'Denmark', 188, 'admin', 1241502590, '', 0),
(60, 'Djibouti', 187, 'admin', 1241502596, '', 0),
(61, 'Dominica', 186, 'admin', 1241502602, '', 0),
(62, 'Dominican Republic', 185, 'admin', 1241502610, '', 0),
(63, 'East Timor', 184, 'admin', 1241502619, '', 0),
(64, 'Ecuador', 183, 'admin', 1241502625, '', 0),
(65, 'Egypt', 182, 'admin', 1241502635, '', 0),
(66, 'El Salvador', 181, 'admin', 1241502644, '', 0),
(67, 'Equatorial Guinea', 180, 'admin', 1241502653, '', 0),
(68, 'Eritrea', 179, 'admin', 1241502659, '', 0),
(69, 'Estonia', 178, 'admin', 1241502665, '', 0),
(70, 'Ethiopia', 177, 'admin', 1241502673, '', 0),
(71, 'Falkland Islands (Islas Malvinas)', 176, 'admin', 1241502687, '', 0),
(72, 'Faroe Islands', 175, 'admin', 1241502696, '', 0),
(73, 'Fiji Islands', 174, 'admin', 1241502704, '', 0),
(74, 'Finland', 173, 'admin', 1241502710, '', 0),
(75, 'France', 172, 'admin', 1241502716, '', 0),
(76, 'France, Metropolitan', 171, 'admin', 1241502741, '', 0),
(77, 'French Guiana', 170, 'admin', 1241502745, '', 0),
(78, 'French Polynesia', 169, 'admin', 1241502754, '', 0),
(79, 'French Southern Territories', 168, 'admin', 1241502762, '', 0),
(80, 'Gabon', 167, 'admin', 1241502768, '', 0),
(81, 'Gambia, The', 166, 'admin', 1241502776, '', 0),
(82, 'Georgia', 165, 'admin', 1241502782, '', 0),
(83, 'Germany', 164, 'admin', 1241502789, '', 0),
(84, 'Ghana', 163, 'admin', 1241502799, '', 0),
(85, 'Gibraltar', 162, 'admin', 1241502812, '', 0),
(86, 'Greece', 161, 'admin', 1241502819, '', 0),
(87, 'Greenland', 160, 'admin', 1241502915, '', 0),
(88, 'Grenada', 159, 'admin', 1241502920, '', 0),
(89, 'Guadeloupe', 158, 'admin', 1241502926, '', 0),
(90, 'Guam', 157, 'admin', 1241502931, '', 0),
(91, 'Guatemala', 156, 'admin', 1241502937, '', 0),
(92, 'Guernsey', 155, 'admin', 1241502943, '', 0),
(93, 'Guinea', 154, 'admin', 1241502950, '', 0),
(94, 'Guinea-Bissau', 153, 'admin', 1241502958, '', 0),
(95, 'Guyana', 152, 'admin', 1241502965, '', 0),
(96, 'Haiti', 151, 'admin', 1241502972, '', 0),
(97, 'Heard and McDonald Islands', 150, 'admin', 1241503013, '', 0),
(98, 'Honduras', 149, 'admin', 1241503022, '', 0),
(99, 'Hong Kong S.A.R.', 148, 'admin', 1241503033, '', 0),
(100, 'Hungary', 147, 'admin', 1241503038, '', 0),
(101, 'Iceland', 146, 'admin', 1241503043, '', 0),
(102, 'India', 145, 'admin', 1241503048, '', 0),
(103, 'Indonesia', 144, 'admin', 1241503054, '', 0),
(104, 'Iran', 143, 'admin', 1241503060, '', 0),
(105, 'Iraq', 142, 'admin', 1241503065, '', 0),
(106, 'Ireland', 141, 'admin', 1241503071, '', 0),
(107, 'Isle of Man', 140, 'admin', 1241503078, '', 0),
(108, 'Israel', 139, 'admin', 1241503084, '', 0),
(109, 'Italy', 138, 'admin', 1241503090, '', 0),
(110, 'Jamaica', 137, 'admin', 1241503097, '', 0),
(111, 'Japan', 136, 'admin', 1241503106, '', 0),
(112, 'Jersey', 135, 'admin', 1241503114, '', 0),
(113, 'Jordan', 134, 'admin', 1241503128, '', 0),
(114, 'Kazakhstan', 133, 'admin', 1241503132, '', 0),
(115, 'Kenya', 132, 'admin', 1241503144, '', 0),
(116, 'Kiribati', 131, 'admin', 1241503151, '', 0),
(117, 'Korea', 130, 'admin', 1241503159, '', 0),
(118, 'Korea, North', 129, 'admin', 1241503166, '', 0),
(119, 'Kuwait', 128, 'admin', 1241503172, '', 0),
(120, 'Kyrgyzstan', 127, 'admin', 1241503178, '', 0),
(121, 'Laos', 126, 'admin', 1241503183, '', 0),
(122, 'Latvia', 125, 'admin', 1241503192, '', 0),
(123, 'Lebanon', 124, 'admin', 1241503197, '', 0),
(124, 'Lesotho', 123, 'admin', 1241503203, '', 0),
(125, 'Liberia', 122, 'admin', 1241503210, '', 0),
(126, 'Libya', 121, 'admin', 1241503217, '', 0),
(127, 'Liechtenstein', 120, 'admin', 1241503224, '', 0),
(128, 'Lithuania', 119, 'admin', 1241503229, '', 0),
(129, 'Luxembourg', 118, 'admin', 1241503235, '', 0),
(130, 'Macau S.A.R.', 117, 'admin', 1241503243, '', 0),
(131, 'Macedonia', 116, 'admin', 1241503251, '', 0),
(132, 'Madagascar', 115, 'admin', 1241503256, '', 0),
(133, 'Malawi', 114, 'admin', 1241503263, '', 0),
(134, 'Malaysia', 113, 'admin', 1241503274, '', 0),
(135, 'Maldives', 112, 'admin', 1241503280, '', 0),
(136, 'Mali', 111, 'admin', 1241503286, '', 0),
(137, 'Malta', 110, 'admin', 1241503292, '', 0),
(138, 'Marshall Islands', 109, 'admin', 1241503303, '', 0),
(139, 'Martinique', 108, 'admin', 1241503314, '', 0),
(140, 'Mauritania', 107, 'admin', 1241503321, '', 0),
(141, 'Mauritius', 106, 'admin', 1241503330, '', 0),
(142, 'Mayotte', 105, 'admin', 1241503337, '', 0),
(143, 'Mexico', 104, 'admin', 1241503343, '', 0),
(144, 'Micronesia', 103, 'admin', 1241503350, '', 0),
(145, 'Moldova', 102, 'admin', 1241503359, '', 0),
(146, 'Monaco', 101, 'admin', 1241503367, '', 0),
(147, 'Mongolia', 100, 'admin', 1241503374, '', 0),
(148, 'Montenegro', 99, 'admin', 1241503387, '', 0),
(149, 'Montserrat', 98, 'admin', 1241503395, '', 0),
(150, 'Morocco', 97, 'admin', 1241503419, '', 0),
(151, 'Mozambique', 96, 'admin', 1241503429, '', 0),
(152, 'Myanmar', 95, 'admin', 1241503437, '', 0),
(153, 'Namibia', 94, 'admin', 1241503444, '', 0),
(154, 'Nauru', 93, 'admin', 1241503451, '', 0),
(155, 'Nepal', 92, 'admin', 1241503459, '', 0),
(156, 'Netherlands', 91, 'admin', 1241503466, '', 0),
(157, 'Netherlands Antilles', 90, 'admin', 1241503477, '', 0),
(158, 'New Caledonia', 89, 'admin', 1241503486, '', 0),
(159, 'New Zealand', 88, 'admin', 1241503503, '', 0),
(160, 'Nicaragua', 87, 'admin', 1241503508, '', 0),
(161, 'Niger', 86, 'admin', 1241503517, '', 0),
(162, 'Nigeria', 85, 'admin', 1241503530, '', 0),
(163, 'Niue', 84, 'admin', 1241503543, '', 0),
(164, 'Norfolk Island', 83, 'admin', 1241503552, '', 0),
(165, 'Northern Mariana Islands', 82, 'admin', 1241503561, '', 0),
(166, 'Norway', 81, 'admin', 1241503566, '', 0),
(167, 'Oman', 80, 'admin', 1241503572, '', 0),
(168, 'Pakistan', 79, 'admin', 1241503625, '', 0),
(169, 'Palau', 78, 'admin', 1241503630, '', 0),
(170, 'Palestinian Territory, Occupied', 77, 'admin', 1241503639, '', 0),
(171, 'Panama', 76, 'admin', 1241503647, '', 0),
(172, 'Papua new Guinea', 75, 'admin', 1241503655, '', 0),
(173, 'Paraguay', 74, 'admin', 1241503661, '', 0),
(174, 'Peru', 73, 'admin', 1241503695, '', 0),
(175, 'Philippines', 72, 'admin', 1241503702, '', 0),
(176, 'Pitcairn Island', 71, 'admin', 1241503712, '', 0),
(177, 'Poland', 70, 'admin', 1241503717, '', 0),
(178, 'Portugal', 69, 'admin', 1241503725, '', 0),
(179, 'Puerto Rico', 68, 'admin', 1241503758, '', 0),
(180, 'Qatar', 67, 'admin', 1241503765, '', 0),
(181, 'Reunion', 66, 'admin', 1241503775, '', 0),
(182, 'Romania', 65, 'admin', 1241503780, '', 0),
(183, 'Russia', 64, 'admin', 1241503786, '', 0),
(184, 'Rwanda', 63, 'admin', 1241503792, '', 0),
(185, 'Saint Helena', 62, 'admin', 1241503799, '', 0),
(186, 'Saint Kitts And Nevis', 61, 'admin', 1241503807, '', 0),
(187, 'Saint Lucia', 60, 'admin', 1241503814, '', 0),
(188, 'Saint Pierre and Miquelon', 59, 'admin', 1241503822, '', 0),
(189, 'Saint Vincent And The Grenadines', 58, 'admin', 1241503830, '', 0),
(190, 'Samoa', 57, 'admin', 1241503835, '', 0),
(191, 'San Marino', 56, 'admin', 1241503844, '', 0),
(192, 'Sao Tome and Principe', 55, 'admin', 1241503851, '', 0),
(193, 'Saudi Arabia', 54, 'admin', 1241503858, '', 0),
(194, 'Senegal', 53, 'admin', 1241503864, '', 0),
(195, 'Serbia', 52, 'admin', 1241503868, '', 0),
(196, 'Seychelles', 51, 'admin', 1241503873, '', 0),
(197, 'Sierra Leone', 50, 'admin', 1241503881, '', 0),
(198, 'Singapore', 49, 'admin', 1241503886, '', 0),
(199, 'Slovakia', 48, 'admin', 1241503892, '', 0),
(200, 'Slovenia', 47, 'admin', 1241503898, '', 0),
(201, 'Solomon Islands', 46, 'admin', 1241503905, '', 0),
(202, 'Somalia', 45, 'admin', 1241503914, '', 0),
(203, 'South Africa', 44, 'admin', 1241503924, '', 0),
(204, 'South Georgia And The South Sandwich Islands', 43, 'admin', 1241503932, '', 0),
(205, 'Spain', 42, 'admin', 1241503939, '', 0),
(206, 'Sri Lanka', 41, 'admin', 1241503946, '', 0),
(207, 'Sudan', 40, 'admin', 1241503951, '', 0),
(208, 'Suriname', 39, 'admin', 1241503957, '', 0),
(209, 'Svalbard And Jan Mayen Islands', 38, 'admin', 1241503965, '', 0),
(210, 'Swaziland', 37, 'admin', 1241503972, '', 0),
(211, 'Sweden', 36, 'admin', 1241503977, '', 0),
(212, 'Switzerland', 35, 'admin', 1241503985, '', 0),
(213, 'Syria', 34, 'admin', 1241504000, '', 0),
(214, 'Taiwan', 33, 'admin', 1241504006, '', 0),
(215, 'Tajikistan', 32, 'admin', 1241504011, '', 0),
(216, 'Tanzania', 31, 'admin', 1241504018, '', 0),
(217, 'Thailand', 30, 'admin', 1241504026, '', 0),
(218, 'Timor-Leste', 29, 'admin', 1241504036, '', 0),
(219, 'Togo', 28, 'admin', 1241504042, '', 0),
(220, 'Tokelau', 27, 'admin', 1241504056, '', 0),
(221, 'Tonga', 26, 'admin', 1241504143, '', 0),
(222, 'Trinidad And Tobago', 25, 'admin', 1241504153, '', 0),
(223, 'Tunisia', 24, 'admin', 1241504158, '', 0),
(224, 'Turkey', 23, 'admin', 1241504163, '', 0),
(225, 'Turkmenistan', 22, 'admin', 1241504169, '', 0),
(226, 'Turks And Caicos Islands', 21, 'admin', 1241504177, '', 0),
(227, 'Tuvalu', 20, 'admin', 1241504183, '', 0),
(228, 'Uganda', 19, 'admin', 1241504189, '', 0),
(229, 'Ukraine', 18, 'admin', 1241504200, '', 0),
(230, 'United Arab Emirates', 17, 'admin', 1241504208, '', 0),
(231, 'United Kingdom', 16, 'admin', 1241504217, '', 0),
(232, 'United States', 15, 'admin', 1241504224, '', 0),
(233, 'United States Minor Outlying Islands', 14, 'admin', 1241504231, '', 0),
(234, 'Uruguay', 13, 'admin', 1241504237, '', 0),
(235, 'Uzbekistan', 12, 'admin', 1241504244, '', 0),
(236, 'Vanuatu', 11, 'admin', 1241504249, '', 0),
(237, 'Vatican City State (Holy See)', 10, 'admin', 1241504257, '', 0),
(238, 'Venezuela', 9, 'admin', 1241504262, '', 0),
(239, 'Vietnam', 8, 'admin', 1241504269, '', 0),
(240, 'Virgin Islands (British)', 7, 'admin', 1241504278, '', 0),
(241, 'Virgin Islands (US)', 6, 'admin', 1241504286, '', 0),
(242, 'WESTERN SAHARA', 5, 'admin', 1241504294, '', 0),
(243, 'Wallis And Futuna Islands', 4, 'admin', 1241504303, '', 0),
(244, 'Yemen', 3, 'admin', 1241504310, '', 0),
(245, 'Zambia', 2, 'admin', 1241504317, '', 0),
(246, 'Zimbabwe', 1, 'admin', 1241504323, '', 0),
(247, 'Rest of the world', 0, 'admin', 1241502080, '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_currency`
-- 

CREATE TABLE `lumonata_currency` (
  `lcurrency_id` int(11) NOT NULL auto_increment,
  `lcode` varchar(25) NOT NULL,
  `lsymbol` varchar(50) NOT NULL,
  `lcurrency` varchar(255) NOT NULL,
  `lamount` decimal(10,5) NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY  (`lcurrency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `lumonata_currency`
-- 

INSERT INTO `lumonata_currency` (`lcurrency_id`, `lcode`, `lsymbol`, `lcurrency`, `lamount`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(8, 'AUD', '$', 'Australia Dollars', 0.00000, 56, 'admin', 1289791160, 'admin', 1305882382),
(7, 'EUR', 'â‚¬', 'Euro', 0.00000, 5, 'admin', 1289791092, 'admin', 1305882229),
(6, 'USD', '$', 'United States Dollar', 0.00000, 51, 'admin', 1289791060, 'admin', 1307754273),
(9, 'IDR', 'Rp', 'Indonesian Rupiah', 0.00000, 10, 'admin', 1289791212, 'admin', 1305882274),
(11, 'GBP', 'Â£', 'Pound Sterling', 0.00000, 1, 'admin', 1301300965, '', 0),
(12, 'CZK', 'KÄ', 'Koruna ÄeskÃ¡', 0.00000, 15, 'admin', 1305882631, '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friendship`
-- 

CREATE TABLE `lumonata_friendship` (
  `lfriendship_id` bigint(20) NOT NULL auto_increment,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY  (`lfriendship_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `lumonata_friendship`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friends_list`
-- 

CREATE TABLE `lumonata_friends_list` (
  `lfriends_list_id` bigint(20) NOT NULL auto_increment,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL,
  PRIMARY KEY  (`lfriends_list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `lumonata_friends_list`
-- 

INSERT INTO `lumonata_friends_list` (`lfriends_list_id`, `luser_id`, `llist_name`, `lorder`) VALUES 
(1, 1, 'Work', 1),
(2, 1, 'School', 2),
(3, 1, 'Familiy', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friends_list_rel`
-- 

CREATE TABLE `lumonata_friends_list_rel` (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`lfriendship_id`,`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `lumonata_friends_list_rel`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_languages`
-- 

CREATE TABLE `lumonata_languages` (
  `llanguage_id` bigint(20) NOT NULL auto_increment,
  `llanguage_title` text collate latin1_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` datetime NOT NULL,
  PRIMARY KEY  (`llanguage_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `lumonata_languages`
-- 

INSERT INTO `lumonata_languages` (`llanguage_id`, `llanguage_title`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`) VALUES 
(1, 'English', 1, 0, '2011-10-14 14:41:06', '0000-00-00 00:00:00'),
(2, 'Dutch', 2, 0, '2011-10-14 14:41:22', '0000-00-00 00:00:00'),
(3, 'Spanish', 3, 0, '2011-10-14 14:42:21', '0000-00-00 00:00:00'),
(4, 'Franc', 4, 0, '2011-10-14 14:42:28', '0000-00-00 00:00:00'),
(5, 'Italian', 5, 0, '2011-10-14 14:43:01', '0000-00-00 00:00:00'),
(6, 'Turkish', 6, 0, '2011-10-14 14:43:31', '0000-00-00 00:00:00'),
(7, 'Swedish', 7, 0, '2011-10-14 14:44:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_members`
-- 

CREATE TABLE `lumonata_members` (
  `lmember_id` varchar(25) collate latin1_general_ci NOT NULL COMMENT '201=Clients Mongkiki, 202=Client Lumonata,203=Other',
  `lfname` varchar(200) collate latin1_general_ci NOT NULL,
  `llname` varchar(50) collate latin1_general_ci NOT NULL,
  `lemail` varchar(100) collate latin1_general_ci NOT NULL,
  `lphone` varchar(50) collate latin1_general_ci NOT NULL,
  `lphone2` varchar(50) collate latin1_general_ci NOT NULL,
  `lcountry_id` int(11) NOT NULL,
  `lregion` varchar(255) collate latin1_general_ci default NULL,
  `lcity` varchar(255) collate latin1_general_ci default NULL,
  `laddress` varchar(255) collate latin1_general_ci NOT NULL,
  `laddress2` varchar(255) collate latin1_general_ci NOT NULL,
  `laddress3` varchar(255) collate latin1_general_ci NOT NULL,
  `lpostal_code` varchar(100) collate latin1_general_ci NOT NULL,
  `lpassword` varchar(100) collate latin1_general_ci NOT NULL,
  `lpass` varchar(255) collate latin1_general_ci NOT NULL,
  `lactv_code` text collate latin1_general_ci NOT NULL,
  `laccount_status` int(1) NOT NULL default '0' COMMENT '1=active;2=awaiting verification;',
  `llast_login` int(11) NOT NULL,
  `lcreated_by` varchar(50) collate latin1_general_ci NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) collate latin1_general_ci NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lmember_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_members`
-- 

INSERT INTO `lumonata_members` (`lmember_id`, `lfname`, `llname`, `lemail`, `lphone`, `lphone2`, `lcountry_id`, `lregion`, `lcity`, `laddress`, `laddress2`, `laddress3`, `lpostal_code`, `lpassword`, `lpass`, `lactv_code`, `laccount_status`, `llast_login`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
('MI111100001', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'd5ayjqq6', '0bcab54afcb696c65d2e163f2cf7b129', 'aefcec72df9707894ee6e6177d26481e', 2, 0, 'MI111100001', '2011-11-25 16:09:44', 'MI111100001', '2011-11-25 16:09:44', 0),
('MI111100002', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'ecdvur0j', '207b3e15899ca5aa3c03281ab449f82e', '299900c19e11b623ded4948af96a259a', 2, 0, 'MI111100002', '2011-11-25 17:40:43', 'MI111100002', '2011-11-25 17:40:43', 0),
('MI111100003', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', '7ocjmu57', 'f8bf775de52bd34b4aab0751089d5176', '180587e9c4cb5c6f84ee35a7a89bad93', 2, 0, 'MI111100003', '2011-11-25 17:49:20', 'MI111100003', '2011-11-25 17:49:20', 0),
('MI111100004', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 've83tz68', '1024a1b2c4f720ecb9d7e8c90352fa19', 'ac8eb9d8e18fbe0a0542f582e545d93e', 2, 0, 'MI111100004', '2011-11-25 17:51:56', 'MI111100004', '2011-11-25 17:51:56', 0),
('MI111100005', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'isvch2tc', '6cf285d7bf95c8b10d6f2deb4fe63908', '9e264ad4dd0d0799df8e7dffcb04631f', 2, 0, 'MI111100005', '2011-11-25 17:54:38', 'MI111100005', '2011-11-25 17:54:38', 0),
('MI111100006', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'cdzppwqd', 'f942828107dc309ff0834104dafb9391', '7132c57521058817e14688da20d4e305', 2, 0, 'MI111100006', '2011-11-25 17:55:20', 'MI111100006', '2011-11-25 17:55:20', 0),
('MI111100007', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'm80bycce', 'd56cebc27281fe3b85df6e8b61239dc4', 'd514bf0bff2f5445ec9c98ecf0cdc1a3', 2, 0, 'MI111100007', '2011-11-25 18:01:56', 'MI111100007', '2011-11-25 18:01:56', 0),
('MI111100008', 'Agus', 'GEDE', 'widia_xp@yahoo.com', '23423', '234234', 15, '2342', 'rewr', 'dwew', 'wer', 'wrw', '23423', 'at65pvmw', '11b8c25f3d1d8f64105adc87d8f5c570', '8143ced36b062dcf56785e3106d158ab', 2, 0, 'MI111100008', '2011-11-25 18:04:10', 'MI111100008', '2011-11-25 18:04:10', 0),
('MI111100009', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'asd', '80852', 'vfnaq0xt', '9286f835c5f28d4d7a100f7a1ad639ae', '9c264d0202a919dc9608be5e5e0f0f52', 2, 0, 'MI111100009', '2011-11-28 09:47:54', 'MI111100009', '2011-11-28 09:47:54', 0),
('MI111100010', '1', '2', 'admin@mail.com', '08523655545', '', 10, 'Gianyar1', 'a', 'ketewel', 'ketewel2', 'asd', '80852', 'mbrcyz6i', 'd9433eb00fd1b25d3496f96f40f056c3', 'bd3c08771b2cd6c818a7b88e00c38429', 2, 0, 'MI111100010', '2011-11-28 09:58:19', 'MI111100010', '2011-11-28 09:58:19', 0),
('MI111100011', '1', '2', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'asd', '80852', 'cg82ti0p', '27f94bfd1cb46c7e402ee4b6ea916ffe', '2796fbfbdac17792a9053c66f2d6a2e4', 2, 0, 'MI111100011', '2011-11-28 10:03:38', 'MI111100011', '2011-11-28 10:03:38', 0),
('MI111100012', '1', '2', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'asd', '80852', 'ebtuzu2h', '2e1aaee8a81536620d7664fbceb87bc4', 'f5c9cea15028ee19da4834b51766ccdd', 2, 0, 'MI111100012', '2011-11-28 10:11:57', 'MI111100012', '2011-11-28 10:11:57', 0),
('MI111100013', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'dtbudcyz', '265bb187f95fb3946d089dc6d33114f5', 'a03007855c8e7a1d08f66207bde732bf', 2, 0, 'MI111100013', '2011-11-28 12:13:27', 'MI111100013', '2011-11-28 12:13:27', 0),
('MI111100014', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'fjcu0646', '9dde46884641f459e2dde7e58e41be26', '2fb9c53955fe96f23354ed5f7188241f', 2, 0, 'MI111100014', '2011-11-28 12:16:25', 'MI111100014', '2011-11-28 12:16:25', 0),
('MI111100015', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '45njapp4', 'dc767eb03820b5fcf07f3317f90ff8f4', '3fcac20a0ec00608655338c293a878e9', 2, 0, 'MI111100015', '2011-11-28 12:17:53', 'MI111100015', '2011-11-28 12:17:53', 0),
('MI111100016', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'mzcz02dz', '002a891df3cab6dbd7c0990721c9b595', '52eccbde57a00ec95ae169b24f41cba0', 2, 0, 'MI111100016', '2011-11-28 15:34:25', 'MI111100016', '2011-11-28 15:34:25', 0),
('MI111100017', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'y73jaib8', '46ad05968bc0e351823a83c0f922cbad', '5513954199ac59286f013cd7407889b9', 2, 0, 'MI111100017', '2011-11-28 15:38:05', 'MI111100017', '2011-11-28 15:38:05', 0),
('MI111100018', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'asd', 'asd', 'ketewel3', '80852', 's4azrc67', 'd6fc0b5c575803e205ff827d48c36cf2', 'e4c124b08747396914ac9ac2ae853e30', 2, 0, 'MI111100018', '2011-11-28 16:08:12', 'MI111100018', '2011-11-28 16:08:12', 0),
('MI111100019', 'widia', 'astina', 'admin@mail.com', '08523655545', '', 10, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'mqe8sh00', '4f6f8894e67837d3572a338235ed3733', '46794fa0949424292dfe7f27273472fb', 2, 0, 'MI111100019', '2011-11-28 17:22:00', 'MI111100019', '2011-11-28 17:22:00', 0),
('MI111100020', 'widia', 'astina', 'admin@mail.com', '08523655545', '', 10, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'rkhs3j8b', '4ccaee52b913fbdca063193ce381f009', '1a8314c786d2833a521f8e4437608e62', 2, 0, 'MI111100020', '2011-11-28 17:24:33', 'MI111100020', '2011-11-28 17:24:33', 0),
('MI111100021', 'widia', 'astina', 'admin@mail.com', '08523655545', '', 10, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'tfe5ugvk', 'c4be770f43c87386ea515d6869ad227b', '0610b553fd09f52542858ae14a73a508', 2, 0, 'MI111100021', '2011-11-28 17:25:40', 'MI111100021', '2011-11-28 17:25:40', 0),
('MI111100022', 'widia', 'astina', 'admin@mail.com', '08523655545', '', 10, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'yeoi5dnp', '89ca003ed4778d1d0449685c5b723a24', '3c0b4aca52e9f80c2805230a069fb22e', 2, 0, 'MI111100022', '2011-11-28 17:26:38', 'MI111100022', '2011-11-28 17:26:38', 0),
('MI111100023', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'itwtnkbs', '1ec170e8b4ec1286130c425d1261f5c7', '4d1453faf17a785fdcebad19ff8b92e9', 2, 0, 'MI111100023', '2011-11-28 17:27:24', 'MI111100023', '2011-11-28 17:27:24', 0),
('MI111100024', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'qsq64sra', '3c1cb58c3233b331cb1e7126e3075f64', 'f241d1e80359729e55dc246d25a038ed', 2, 0, 'MI111100024', '2011-11-28 17:28:09', 'MI111100024', '2011-11-28 17:28:09', 0),
('MI111100025', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '0ztdckb6', 'ec456d41856516648688c9e0ef379c0f', '64fdfcd8624b4693ee5e8eafaf011fe6', 2, 0, 'MI111100025', '2011-11-28 17:28:41', 'MI111100025', '2011-11-28 17:28:41', 0),
('MI111100026', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'td52ax0b', '565aff3977e6bd2bffbceb157ec824aa', 'c3fc1a0a6d284dea266cb8aa04bbd995', 2, 0, 'MI111100026', '2011-11-28 17:29:07', 'MI111100026', '2011-11-28 17:29:07', 0),
('MI111100027', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'dv4ve7hj', 'fbc1f989e331ee1c9aa4229d952a7bec', '1f1b99c64cf02089b8caf93848c0e96e', 2, 0, 'MI111100027', '2011-11-28 17:29:34', 'MI111100027', '2011-11-28 17:29:34', 0),
('MI111100028', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '8aey23oz', '69ef8841fce1548ab3adb90baa7c779f', '900d103f2ea34f9e95da56d528d088f8', 2, 0, 'MI111100028', '2011-11-28 17:30:03', 'MI111100028', '2011-11-28 17:30:03', 0),
('MI111100029', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'uv6nntme', '0a4732debefd2c15477c3816006ddff0', 'ea761f612ca55b7f01c752484ace423e', 2, 0, 'MI111100029', '2011-11-28 17:30:16', 'MI111100029', '2011-11-28 17:30:16', 0),
('MI111100030', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '5jbn00cb', 'cb2c6496b11e8d0c0c1bdcc47ad52886', 'f49425d610e949176ee6ab3823040aca', 2, 0, 'MI111100030', '2011-11-28 17:31:22', 'MI111100030', '2011-11-28 17:31:22', 0),
('MI111100031', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'f3hcrpuv', '033af5bb0abf4b79bb205d93a57b6ec0', 'c1b4ef358361adf4c8d582b6dd0ba51c', 2, 0, 'MI111100031', '2011-11-28 17:31:30', 'MI111100031', '2011-11-28 17:31:30', 0),
('MI111100032', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 10, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'nz47ymyt', '3d4c66f30cb93ba2de7e7def06bfc50e', 'bab1d5cacdfa4d39a38ea5774b3dd4d9', 2, 0, 'MI111100032', '2011-11-28 17:33:43', 'MI111100032', '2011-11-28 17:33:43', 0),
('MI111100033', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ddang5bp', 'f3a8bce82bb046856ca57c33054a5571', '94c346fe83500ba46e4a3ab2279f7b5e', 2, 0, 'MI111100033', '2011-11-28 17:40:41', 'MI111100033', '2011-11-28 17:40:41', 0),
('MI111100034', 'widia', 'astina', 'admin@mail.com', '08523655545', '08523655546', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'v5ibr7qd', 'cc56935b90f9d2d19a8a8f5f128abc1e', 'a18a31a5fe9c9d33ee8efe8466f22d9e', 2, 0, 'MI111100034', '2011-11-30 11:37:24', 'MI111100034', '2011-11-30 11:37:24', 0),
('MI111200001', 'I Ketut Widia', 'Astina Putra', 'widia_xp@lumonata.com', '08523655545', '08523655546', 103, 'Sukawati', 'Gianyar', 'Ketewel1', 'ketewel2', 'ketewel3', '80582', '07co52dh', 'a79d9571e3d4c9a4be5150377483af3f', '7a4decbb416d8c82c02372fddee732fd', 2, 0, 'MI111200001', '2011-12-02 10:42:00', 'MI111200001', '2011-12-02 10:42:00', 0),
('MI111200002', 'widia', 'as', 'admin@mail.com', 'a', '08523655546', 2, '22', 'a', 'ketewel', 'asd', 'a', '1', 'ya62f8g4', '16be3732840cbfcffffd86d3d086bf30', '51a618495aa541a57c63d1b0d1d9800d', 2, 0, 'MI111200002', '2011-12-02 11:10:54', 'MI111200002', '2011-12-02 11:10:54', 0),
('MI111200003', 'Alfred', 'Riddle', 'widia_xp@lumonata.com', '08523655545', '', 103, '', 'Jakarta', 'Jl. Merdeka Timur', '', '', '8056855', 'xvt8pfyc', '109ceb7a542c4268b0b27fac6e36a510', 'bdac6d790fc0af5406196f1bfa374b9b', 2, 0, 'MI111200003', '2011-12-05 12:11:49', 'MI111200003', '2011-12-05 12:11:49', 0),
('MI111200004', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'ha2hrvoa', '4f31156754dde3563ba858dc3b8831f9', 'd2a99b48587eecdbccc5f03a56ede5bc', 2, 0, 'MI111200004', '2011-12-05 12:24:40', 'MI111200004', '2011-12-05 12:24:40', 0),
('MI111200005', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'qtqudnqo', '50e6d23a5b8127dd97a9ee22887d01e0', '3cadfe5bae533ebe78d3f913846bfbde', 2, 0, 'MI111200005', '2011-12-05 12:24:48', 'MI111200005', '2011-12-05 12:24:48', 0),
('MI111200006', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'auheic5s', 'a69630ad7705b377c36fb0717945d01b', 'f9694a04ba05fd226049b0c5d9585bed', 2, 0, 'MI111200006', '2011-12-05 12:24:49', 'MI111200006', '2011-12-05 12:24:49', 0),
('MI111200007', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'uwcdezrn', '35bf828e63db2d89920373d8cff80328', '15239708e4447f4fb0356e006b511633', 2, 0, 'MI111200007', '2011-12-05 12:24:49', 'MI111200007', '2011-12-05 12:24:49', 0),
('MI111200008', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', '7zbwepxt', 'f9d24dab2656fbff24f4d689b75756b4', '9cdd9edecc7ff00764d58f7b778a6e7c', 2, 0, 'MI111200008', '2011-12-05 12:24:50', 'MI111200008', '2011-12-05 12:24:50', 0),
('MI111200009', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'z86kbpyn', '90936f7d7778c19db51c9fe26a4b3d74', 'd4702682b957c3387ab2119eb5434b26', 2, 0, 'MI111200009', '2011-12-05 12:24:50', 'MI111200009', '2011-12-05 12:24:50', 0),
('MI111200010', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'd52ax0b6', 'a227c5517a70229eb233b1767c011ded', '8457c2e2fe96fd82823789c4876b9e46', 2, 0, 'MI111200010', '2011-12-05 12:24:50', 'MI111200010', '2011-12-05 12:24:50', 0),
('MI111200011', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'zsxdaqf8', 'cce0401575501ae976c5ddd24f169e3f', '8ba54dbe997516535b2f22bc324d7f3a', 2, 0, 'MI111200011', '2011-12-05 12:24:51', 'MI111200011', '2011-12-05 12:24:51', 0),
('MI111200012', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'uj44n256', '8bba8e8e325aa34d648d220d27020772', 'c5f34fb69e08cde568cd0f4f6dd42ed5', 2, 0, 'MI111200012', '2011-12-05 12:24:51', 'MI111200012', '2011-12-05 12:24:51', 0),
('MI111200013', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'omkqw3az', '8740b839e8bf8cfe75e1d8b8afbf76e8', '85e3bf8218ca7fc93d491bd8525da56c', 2, 0, 'MI111200013', '2011-12-05 12:24:51', 'MI111200013', '2011-12-05 12:24:51', 0),
('MI111200014', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'vt2pa6gr', '13975d386ad6fc4a4ea0f3e0dc6b3ded', 'da556bee6df45b24c3d125c27021236c', 2, 0, 'MI111200014', '2011-12-05 12:24:51', 'MI111200014', '2011-12-05 12:24:51', 0),
('MI111200015', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'p8z2vrsi', '23039321b22d49157ae79cb3c890c34a', 'ce25c77d37f094a28b6c3b74678159ef', 2, 0, 'MI111200015', '2011-12-05 12:24:52', 'MI111200015', '2011-12-05 12:24:52', 0),
('MI111200016', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'gfgvov4e', '9e2396564f2ba70b033570f44013f040', 'd782fb7e0df705f684e40a221e8f9d45', 2, 0, 'MI111200016', '2011-12-05 12:24:52', 'MI111200016', '2011-12-05 12:24:52', 0),
('MI111200017', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', '4iixyyaz', 'ff4d4159b1fcb886d37799d0515d68c6', 'a38da2f7fde88a73fb5c7d5b50a3c695', 2, 0, 'MI111200017', '2011-12-05 12:24:52', 'MI111200017', '2011-12-05 12:24:52', 0),
('MI111200018', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'fyc80pae', '08eaf5b99ed5ed2b2740f19c57177217', 'bc4af254785976198c56db3b1dfc0780', 2, 0, 'MI111200018', '2011-12-05 12:24:52', 'MI111200018', '2011-12-05 12:24:52', 0),
('MI111200019', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'edgwwxpx', 'bb034bc50cd9349c7b4a99a5fdd8f4f8', 'c74921e42749abd4727b9f3651c11e79', 2, 0, 'MI111200019', '2011-12-05 12:25:54', 'MI111200019', '2011-12-05 12:25:54', 0),
('MI111200020', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', '0aaergno', 'e6d95f690708c417c08528b541d08461', '6467078acd2290bd16f6f2d41fba6c4c', 2, 0, 'MI111200020', '2011-12-05 12:28:38', 'MI111200020', '2011-12-05 12:28:38', 0),
('MI111200021', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '', 111, '', 'Leaf Village', 'konoha', '', '', '789456', 'hikgs7px', '340983271b1feeb1a988e08357105e16', '4ab35cc2d3102e28a46b0f10bfc13002', 2, 0, 'MI111200021', '2011-12-05 12:28:48', 'MI111200021', '2011-12-05 12:28:48', 0),
('MI111200022', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'isauo340', 'eee2dbe2f1794b923a640a03e929d655', 'bc6113ca1ff50c2b2d71fc8064b8e5db', 2, 0, 'MI111200022', '2011-12-05 12:30:33', 'MI111200022', '2011-12-05 12:30:33', 0),
('MI111200023', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'deh7w5ph', '81d4de19651bbc1a256959c9f6f5bb65', '416ad964889fa0f0b814cf6a3053c4b0', 2, 0, 'MI111200023', '2011-12-05 12:32:00', 'MI111200023', '2011-12-05 12:32:00', 0),
('MI111200024', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'y5u65wzw', '0f3411832d84b270e64fd4eaa0897538', '2b8cbdc5f62ab4979b4fc918c00481d4', 2, 0, 'MI111200024', '2011-12-05 12:32:38', 'MI111200024', '2011-12-05 12:32:38', 0),
('MI111200025', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', '7zzct4cc', '2af865b92d5dede20fc52d26e1bbfb90', '095e1f0cc20e4e8f5f59e958f680c246', 2, 0, 'MI111200025', '2011-12-05 12:32:46', 'MI111200025', '2011-12-05 12:32:46', 0),
('MI111200026', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', '4j5qbzfd', 'e0eac145d0cfa3bb4f835606c6059f3e', '509260e10ceec9fb79bca0d6b77901cc', 2, 0, 'MI111200026', '2011-12-05 12:35:15', 'MI111200026', '2011-12-05 12:35:15', 0),
('MI111200027', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'i6oa7any', 'a992d70f7bdb47b963e83f2d7ef9f5c1', '3f498744d2f3d14e8bd8dce86059cd99', 2, 0, 'MI111200027', '2011-12-05 12:35:53', 'MI111200027', '2011-12-05 12:35:53', 0),
('MI111200028', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'i806qpop', 'bf809d9b2693f9984199647f6fa95809', 'dc1906755c95d439aa02a27b3786518f', 2, 0, 'MI111200028', '2011-12-05 12:36:53', 'MI111200028', '2011-12-05 12:36:53', 0),
('MI111200029', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', '5e0bod20', 'f68f101bac6beb06b85b8782cd085c24', 'fd136d29c80d01e3468a65ee04637214', 2, 0, 'MI111200029', '2011-12-05 12:38:26', 'MI111200029', '2011-12-05 12:38:26', 0),
('MI111200030', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'kwebnrod', '1e80d0db128799ed44886586ffd78da7', '5b3d5955d84199065c09e03ab0b9f698', 2, 0, 'MI111200030', '2011-12-05 12:42:18', 'MI111200030', '2011-12-05 12:42:18', 0),
('MI111200031', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'eu75st7d', 'fab65123875c683565cf8be3189d27fa', '810ae8082dd9f06b61268e7bd2a85d3b', 2, 0, 'MI111200031', '2011-12-05 12:43:22', 'MI111200031', '2011-12-05 12:43:22', 0),
('MI111200032', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'cmivcprg', 'afa2b3b18a140f7743b973ef88491ead', '654491e3e1dabe7f0749cc91be2c3871', 2, 0, 'MI111200032', '2011-12-05 12:47:42', 'MI111200032', '2011-12-05 12:47:42', 0),
('MI111200033', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'n7bx7hgn', '6ed10455c3d236eccc73ff8333f3f248', 'cb1fb95185e6851b9d5bbf161d239c06', 2, 0, 'MI111200033', '2011-12-05 13:03:29', 'MI111200033', '2011-12-05 13:03:29', 0),
('MI111200034', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'rp2hgc3o', '47aa8b74dc530c94a3b1820957bd9e71', '2c4f05ae0771a2bd45eaf5b762ca4e57', 2, 0, 'MI111200034', '2011-12-05 13:04:42', 'MI111200034', '2011-12-05 13:04:42', 0),
('MI111200035', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'sh46u5qp', '6f53770f1b147d64fedca122af96a896', '544b0ea7676a41baa8f2fbed08768d61', 2, 0, 'MI111200035', '2011-12-05 13:16:20', 'MI111200035', '2011-12-05 13:16:20', 0),
('MI111200036', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', '7z3pnh0c', 'b6695628299087549c3b62d7c6e71011', 'cbb41e2e1d420e7f466081d00ca8af5a', 2, 0, 'MI111200036', '2011-12-05 13:17:25', 'MI111200036', '2011-12-05 13:17:25', 0),
('MI111200037', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'kcwyskv3', 'dac572d03de07f5143c68c9666c3fae5', '4772b762f02fcf584b4d7f97f996b033', 2, 0, 'MI111200037', '2011-12-05 13:18:15', 'MI111200037', '2011-12-05 13:18:15', 0),
('MI111200038', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'u3i8tmao', '896c51fee8eaee112ff20b9cd3ef71f4', 'f0235cd279528fb5e04bcb45ded1cfbe', 2, 0, 'MI111200038', '2011-12-05 13:19:09', 'MI111200038', '2011-12-05 13:19:09', 0),
('MI111200039', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'xi27ti4g', 'ab65970cb763604ee12a6f02522bc90c', '38cd0b5ffb876a606a4bde84171aa3be', 2, 0, 'MI111200039', '2011-12-05 13:26:00', 'MI111200039', '2011-12-05 13:26:00', 0),
('MI111200040', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', '8pnsebq3', '1e99d06c0c6b897b05491b41243010c7', 'f22bf15fdf078a7ed8b38df83fec999d', 2, 0, 'MI111200040', '2011-12-05 13:27:56', 'MI111200040', '2011-12-05 13:27:56', 0),
('MI111200041', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'rzwqr88p', '8b936b7e067270b743744623bb4ad003', '1be63f54cd5f44b818dd7b5b68c0119a', 2, 0, 'MI111200041', '2011-12-05 13:31:45', 'MI111200041', '2011-12-05 13:31:45', 0),
('MI111200042', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'mg8nigbw', 'b08f11a5116b1522392ce6a135598cdd', 'a3c65389cc7c97da915dd644964efcc1', 2, 0, 'MI111200042', '2011-12-05 13:32:26', 'MI111200042', '2011-12-05 13:32:26', 0),
('MI111200043', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'jveuzdqd', '45f30d8e101133e032e4a7f6a8417a73', '94489bd68ff1f69d873bc6ccb7abcafb', 2, 0, 'MI111200043', '2011-12-05 13:33:30', 'MI111200043', '2011-12-05 13:33:30', 0),
('MI111200044', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'asd', '', '', '80852', 'g6qhbu6u', '7d0a3b021aa095b3ddb70db87c500255', '5862d4b3a3ec724d7099734f07b3e88e', 2, 0, 'MI111200044', '2011-12-05 13:35:00', 'MI111200044', '2011-12-05 13:35:00', 0),
('MI111200045', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'cpyg6jya', '442ecd758ca5cdec11305bb1c8152152', '5b56499e3c77e21c699302eab4c9d6e2', 2, 0, 'MI111200045', '2011-12-05 13:40:35', 'MI111200045', '2011-12-05 13:40:35', 0),
('MI111200046', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'j0s7f7o0', '9e42ccc1c124fe7c3bd44d20723cd3e8', '555253a35d472e1563ddc2aab632a8ee', 2, 0, 'MI111200046', '2011-12-05 13:41:23', 'MI111200046', '2011-12-05 13:41:23', 0),
('MI111200047', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'ins4gvhd', '85730fd0789c7255e54f090efede1d33', '0d5b8929e0029ce3e3d76a11be794856', 2, 0, 'MI111200047', '2011-12-05 13:51:20', 'MI111200047', '2011-12-05 13:51:20', 0),
('MI111200048', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'sjvfgak8', '1a613fb777ff7221729c2ffc7cdfd48a', 'ce1018d3976fd15a917d9f913fa4d660', 2, 0, 'MI111200048', '2011-12-05 13:52:43', 'MI111200048', '2011-12-05 13:52:43', 0),
('MI111200049', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'pi0mngcs', 'c5c11879dcff0ad50ff809adf450af72', 'f88df4b303a7cac90dad5e73f9da29ef', 2, 0, 'MI111200049', '2011-12-05 14:00:36', 'MI111200049', '2011-12-05 14:00:36', 0),
('MI111200050', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'suxwg02g', '60d3f0c8736cebf8953ff4db1f83e494', 'b3f2b291de101e5b9c76afe1e3b232bb', 2, 0, 'MI111200050', '2011-12-05 14:03:31', 'MI111200050', '2011-12-05 14:03:31', 0),
('MI111200051', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'mje2zmxz', '18a5739a018667651c7b39f7ac84de29', '5568b23eab128f8ea15d23f00c49ec60', 2, 0, 'MI111200051', '2011-12-05 14:10:09', 'MI111200051', '2011-12-05 14:10:09', 0),
('MI111200052', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', '6mjsk6n8', 'ae30d41167f71611dac5850887ce04be', 'c3f1d49fba93192e456b2773701a78fa', 2, 0, 'MI111200052', '2011-12-05 14:10:32', 'MI111200052', '2011-12-05 14:10:32', 0),
('MI111200053', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'u475ei5o', '6343206907ba9365f207c42f29c10bf5', '1f388b1891cc9469c6f37f8a24677cd1', 2, 0, 'MI111200053', '2011-12-05 14:12:08', 'MI111200053', '2011-12-05 14:12:08', 0),
('MI111200054', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'ty830e0s', 'aebaf0750e900650887530faad18bbbb', '779e118b9d3eff1de292bd9e3cda99cf', 2, 0, 'MI111200054', '2011-12-05 14:12:44', 'MI111200054', '2011-12-05 14:12:44', 0),
('MI111200055', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', '', '', '80852', 'bdne4cf0', '7033267eeb58e3b4d8082a030cbd2b1d', '5b113fba7a07868f6bdde480d861bfa0', 2, 0, 'MI111200055', '2011-12-05 14:14:37', 'MI111200055', '2011-12-05 14:14:37', 0),
('MI111200056', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'hpokoozo', 'ed74e2a370ebcf686c8f6ff7759e4749', '98e6d40fac8e3400e239f338f1246dd9', 2, 0, 'MI111200056', '2011-12-05 14:21:56', 'MI111200056', '2011-12-05 14:21:56', 0),
('MI111200057', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'rweeeatk', 'f700b2db627d58fe1a99e2fdf45fe6df', 'acc7e3af29be931e359df2442eb7bd7e', 2, 0, 'MI111200057', '2011-12-05 14:33:45', 'MI111200057', '2011-12-05 14:33:45', 0),
('MI111200058', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '2szhzhme', 'c20b1d117ad30290d78c700fdf574e84', '0d88398427efd586ebbc2d9ed1d65dd9', 2, 0, 'MI111200058', '2011-12-05 14:43:52', 'MI111200058', '2011-12-05 14:43:52', 0),
('MI111200059', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'g36s748e', '7f1f4dc49f9b5c8e4a90a12417e4fa58', '7769d59049ae7032d04162d3dd06b015', 2, 0, 'MI111200059', '2011-12-05 14:47:20', 'MI111200059', '2011-12-05 14:47:20', 0),
('MI111200060', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'bb3hf4i7', '50dace1c3ffb3b665b9f6f6e19646729', '40a7b11b2ebef6c6113ecd183df9adc2', 2, 0, 'MI111200060', '2011-12-05 15:07:44', 'MI111200060', '2011-12-05 15:07:44', 0),
('MI111200061', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'dinbo5ay', '5f718a5eaee9e1127955426fd3d5402b', '75986fc4d2245fc4308f351b666f4853', 2, 0, 'MI111200061', '2011-12-05 15:08:34', 'MI111200061', '2011-12-05 15:08:34', 0),
('MI111200062', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'xykj86ak', 'b2984ae3979b808a0677e4faa9306a8d', '6f36090cb815a4cb6a3555c78a91406d', 2, 0, 'MI111200062', '2011-12-05 15:13:06', 'MI111200062', '2011-12-05 15:13:06', 0),
('MI111200063', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'io4c8c28', 'd8167c6a2f33c890e14c8006d07b2476', 'f47a7178d032ce9e2ae4d0dccd7ce61e', 2, 0, 'MI111200063', '2011-12-05 15:15:21', 'MI111200063', '2011-12-05 15:15:21', 0),
('MI111200064', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'qibqz4wi', '32849b2740810c986ecd03bb4a09658c', '4b1684c800fb726cd4a0c0a3c6627a80', 2, 0, 'MI111200064', '2011-12-05 15:15:48', 'MI111200064', '2011-12-05 15:15:48', 0),
('MI111200065', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'a5cf8sag', '260437ead7c1499bee2c25201c3e9090', '2870c734118e72666b43fe586901b977', 2, 0, 'MI111200065', '2011-12-05 15:16:05', 'MI111200065', '2011-12-05 15:16:05', 0),
('MI111200066', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'dyot3fed', '292f5aa341fc5a133c26a1d0387c31f6', '4254c8a718651b60f3d42d5befd118b9', 2, 0, 'MI111200066', '2011-12-05 15:16:51', 'MI111200066', '2011-12-05 15:16:51', 0),
('MI111200067', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'h4a7y3f3', '3b328b3bf048613773bfd98d0018ee36', '42434c808c69a4894356127a8b795ce8', 2, 0, 'MI111200067', '2011-12-05 15:17:16', 'MI111200067', '2011-12-05 15:17:16', 0),
('MI111200068', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ypp4bt55', 'dec4953fd394b02d144b717b50033921', 'e3ee2fbf0c1ead3034d2b15fb057755b', 2, 0, 'MI111200068', '2011-12-05 15:20:14', 'MI111200068', '2011-12-05 15:20:14', 0),
('MI111200069', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'mc3do48h', 'b07d548a6e8e25109c75571b2242b914', '96e8a585f2410d029d0285267ec4934c', 2, 0, 'MI111200069', '2011-12-05 15:20:17', 'MI111200069', '2011-12-05 15:20:17', 0),
('MI111200070', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '4wmtxj5g', 'afecfa7225ef42fb4264519ba874b323', '034c970b084b38ecac6a32c03dc2a904', 2, 0, 'MI111200070', '2011-12-05 15:33:39', 'MI111200070', '2011-12-05 15:33:39', 0),
('MI111200071', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'u7j3wx6e', 'b125685319ef0c772ab63f7c5148fd47', '3a2dad27ec67277862dea6bccface2d7', 2, 0, 'MI111200071', '2011-12-05 15:35:44', 'MI111200071', '2011-12-05 15:35:44', 0),
('MI111200072', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'grb6xep4', 'bc4e88e407a150920bd4d0ad7a68a465', '7ad9889dafc3d75cf57a9315b7757b19', 2, 0, 'MI111200072', '2011-12-05 15:37:22', 'MI111200072', '2011-12-05 15:37:22', 0),
('MI111200073', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '2yzyjjq0', 'bddfbc30fa1b2a8d31807de513afc4c6', '5fa1cc7f1e5dae2cb6ba78a72aef4f54', 2, 0, 'MI111200073', '2011-12-05 15:39:21', 'MI111200073', '2011-12-05 15:39:21', 0),
('MI111200074', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'igzcikrq', '16a9ede8a0ef5a528bee44c17e56d4f6', 'a255586066933d2026a381ff255a3585', 2, 0, 'MI111200074', '2011-12-05 15:42:22', 'MI111200074', '2011-12-05 15:42:22', 0),
('MI111200075', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ec43sbnd', '52bda8d56bcc04efe6bc09c14b4b2beb', '10cdba8f8c5aa6b18c16c544a948a772', 2, 0, 'MI111200075', '2011-12-05 15:43:55', 'MI111200075', '2011-12-05 15:43:55', 0),
('MI111200076', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ntbtr0rh', 'ae1534859f60ab4c60b6531cbf218670', 'decf2c58929042b0b917fb9405dab9cb', 2, 0, 'MI111200076', '2011-12-05 15:47:15', 'MI111200076', '2011-12-05 15:47:15', 0),
('MI111200077', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'gadf08r2', '753d05dd22eb71f92ee9e90bb14f9d85', '86940063bec1f370f4b1875d47bdbf67', 2, 0, 'MI111200077', '2011-12-05 15:48:06', 'MI111200077', '2011-12-05 15:48:06', 0),
('MI111200078', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'bfqczt27', '03cc64c2d98c137d5903cfb491f6abad', '4660f1b289e1c611d665eb46abc50a03', 2, 0, 'MI111200078', '2011-12-05 15:51:54', 'MI111200078', '2011-12-05 15:51:54', 0),
('MI111200079', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '60xhnfma', '5c18c9ceedcbe0656377aab14cf5dc75', '53bcecbf72e9bc571db962d27c52d512', 2, 0, 'MI111200079', '2011-12-05 15:52:09', 'MI111200079', '2011-12-05 15:52:09', 0),
('MI111200080', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ythqvsfw', '2729242c2763224fb3b0e036f0584cc6', '654131dc5fc1ac6486afddb4f519833f', 2, 0, 'MI111200080', '2011-12-05 15:52:46', 'MI111200080', '2011-12-05 15:52:46', 0),
('MI111200081', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'q6cktq5b', '6a138d6c1eac1e370785596afe92a8c2', 'a26babc0853a48089f22403b95750cfd', 2, 0, 'MI111200081', '2011-12-05 15:56:10', 'MI111200081', '2011-12-05 15:56:10', 0),
('MI111200082', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'dc84cnt7', '86e62c29df82e989f83ea749cdf56166', '6f34897dafb54bd79873ce13f9ccfb39', 2, 0, 'MI111200082', '2011-12-05 15:56:37', 'MI111200082', '2011-12-05 15:56:37', 0),
('MI111200083', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'scysk3p4', '146b5760b9705d1426502a594b4c849c', 'a1cebdf804ab9fd42978cd7fc8f932e0', 2, 0, 'MI111200083', '2011-12-05 15:56:54', 'MI111200083', '2011-12-05 15:56:54', 0),
('MI111200084', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'm3jd5opc', '59c80b8bba7ca202db2ff40ebdcd7f43', '88fdb6389be6a5930ac050ba7f9bf923', 2, 0, 'MI111200084', '2011-12-05 15:57:02', 'MI111200084', '2011-12-05 15:57:02', 0),
('MI111200085', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'pwieyyjr', 'ce30500b03966eec8a5306ab329e2b7f', 'ac332934f3d92e45a7418568a95767c4', 2, 0, 'MI111200085', '2011-12-05 15:58:13', 'MI111200085', '2011-12-05 15:58:13', 0),
('MI111200086', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'hatppfa3', 'd273202959b33ffc4b2031180a148d63', '00a068a75f2c733201133f329facae79', 2, 0, 'MI111200086', '2011-12-05 15:58:51', 'MI111200086', '2011-12-05 15:58:51', 0),
('MI111200087', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ir324r8c', '7672c144e274891954beb7aa05384c41', '957fdc736958fda039d9bf2ac8b9e5b8', 2, 0, 'MI111200087', '2011-12-05 16:01:23', 'MI111200087', '2011-12-05 16:01:23', 0),
('MI111200088', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'cxd6k7d6', 'be5b3323367274d8c81cc814bdbbda1d', 'b881752ea25b6a2a8b2db98c96676d65', 2, 0, 'MI111200088', '2011-12-05 16:01:42', 'MI111200088', '2011-12-05 16:01:42', 0),
('MI111200089', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'yhmmqkc4', '73022438e66c6d3f8ee458f0f6bd58cc', '8d01ca7989ed93a9c4ccd22f2f84cef1', 2, 0, 'MI111200089', '2011-12-05 16:06:06', 'MI111200089', '2011-12-05 16:06:06', 0),
('MI111200090', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'uf600ugs', '2c5cd65fd61c26590713bc14bd2f8477', '493b49f58cc7742d5a0f833e8d18623f', 2, 0, 'MI111200090', '2011-12-05 16:06:44', 'MI111200090', '2011-12-05 16:06:44', 0),
('MI111200091', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ad5akuy8', '94a24999c186d4d16cdf3a8d0d278a70', '49673f86d1221610a4e16215885011a8', 2, 0, 'MI111200091', '2011-12-05 16:08:06', 'MI111200091', '2011-12-05 16:08:06', 0),
('MI111200092', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'knr3xryw', 'f56957d58ec549ef8fb729ed26363f13', '229771271495167027f3bcbded91936f', 2, 0, 'MI111200092', '2011-12-05 16:08:33', 'MI111200092', '2011-12-05 16:08:33', 0),
('MI111200093', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'x2043zhe', 'ccfa90e1443ac3ae9a0de2996aa88cad', '7624b9c6bcc8b0c469cf28ef49e794c8', 2, 0, 'MI111200093', '2011-12-05 16:09:34', 'MI111200093', '2011-12-05 16:09:34', 0),
('MI111200094', 'widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'oh845uav', '106b585d59d57ae8be9832c0437dddd3', '8de132cedcf63911e697f08dc415a918', 2, 0, 'MI111200094', '2011-12-05 16:13:54', 'MI111200094', '2011-12-05 16:13:54', 0),
('MI111200095', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'bhkb3342', '01e2c68cd9d85b6eb875f9c4244ad95b', '75c4e502015e6bb0584db9926a29f67a', 2, 0, 'MI111200095', '2011-12-05 16:18:56', 'MI111200095', '2011-12-05 16:18:56', 0),
('MI111200096', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'siuqqb3e', '7e113fef7c039de3263f7c699e671427', '03546ae63a9b99dddc98c10b7a21a803', 2, 0, 'MI111200096', '2011-12-05 16:22:39', 'MI111200096', '2011-12-05 16:22:39', 0),
('MI111200097', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 111, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '7h0q3pfh', '8454bb87ca80fdce4dc5641123ec7ad4', '8677ccbd082e47fc5ea9473dab57d8f3', 2, 0, 'MI111200097', '2011-12-05 16:23:09', 'MI111200097', '2011-12-05 16:23:09', 0),
('MI111200098', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'eimwg887', '0e418861f35d373e00e2d0ed49c57d91', 'b2b44b6fbaa2f2617f0b06f743285981', 2, 0, 'MI111200098', '2011-12-05 16:32:46', 'MI111200098', '2011-12-05 16:32:46', 0),
('MI111200099', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'fj83i2wv', '6e5526e673b35ab80c2b44d7f4bacc80', '87c6c8df0052bd0c0fd6cfc448ec230a', 2, 0, 'MI111200099', '2011-12-05 16:33:11', 'MI111200099', '2011-12-05 16:33:11', 0),
('MI111200100', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '47dkaej8', '2e4e4da552cb695f85c17b6dce8d073c', 'b8b5c5736be5491279d828913d211a6f', 2, 0, 'MI111200100', '2011-12-07 09:44:15', 'MI111200100', '2011-12-07 09:44:15', 0),
('MI111200101', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 102, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'iepifwp6', '45e0a52b4794ef83751c2fddadee85f6', 'ec76b6de7a1b7b250ac3a80f23338b90', 2, 0, 'MI111200101', '2011-12-07 11:10:01', 'MI111200101', '2011-12-07 11:10:01', 0),
('MI111200102', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 102, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'grb6xep4', 'bc4e88e407a150920bd4d0ad7a68a465', '30da5f34a22cbdf1ba685874f291e1b4', 2, 0, 'MI111200102', '2011-12-07 11:10:14', 'MI111200102', '2011-12-07 11:10:14', 0),
('MI111200103', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'cyzk8g08', 'b49cbff322e8ac94533d022dc547973e', '4cf9117329eef9d56c8f9ffb4fed8598', 2, 0, 'MI111200103', '2011-12-07 11:26:47', 'MI111200103', '2011-12-07 11:26:47', 0),
('MI111200104', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'cajiiuzu', '272cba50dbe3e8e50c4dba3fac8e1707', '0d4e03b540bcc5cbd9eb59a35af3039d', 2, 0, 'MI111200104', '2011-12-07 11:30:43', 'MI111200104', '2011-12-07 11:30:43', 0),
('MI111200105', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ofzr08xj', '624d6b9602cdfb105be32f01bb30656f', 'b0600bada34e4d8553ca821130580375', 2, 0, 'MI111200105', '2011-12-07 17:33:51', 'MI111200105', '2011-12-07 17:33:51', 0),
('MI111200106', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '3wqvgvdb', 'b791d53cf7bf737abbd5f28ceceeaae9', '8f2aaa7a7cf36a0c2686d21009fff6c9', 2, 0, 'MI111200106', '2011-12-07 17:34:05', 'MI111200106', '2011-12-07 17:34:05', 0),
('MI111200107', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'q3ysg05s', '30643aa38cf00b71564ef35c82d4fffb', '96889ca1848c8c666e3861cba04cc7a0', 2, 0, 'MI111200107', '2011-12-07 17:34:10', 'MI111200107', '2011-12-07 17:34:10', 0),
('MI111200108', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'm5txuqz2', '89fbefa73ee2287050fbf8916bfbba34', 'ceb910136a447f1a13affeb872dc9b40', 2, 0, 'MI111200108', '2011-12-07 17:34:16', 'MI111200108', '2011-12-07 17:34:16', 0),
('MI111200109', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'bavpeo40', '33de805575d626bc0dd55542b5ccb5a7', '066e977cc3988a56767d3f747cd62810', 2, 0, 'MI111200109', '2011-12-07 17:35:19', 'MI111200109', '2011-12-07 17:35:19', 0),
('MI111200110', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'f2vjfopi', '39c78c8f5a499a758c0c0f0fc0f28acc', '016fd16a279aea8c126e7c5a432da142', 2, 0, 'MI111200110', '2011-12-07 17:35:24', 'MI111200110', '2011-12-07 17:35:24', 0),
('MI111200111', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 't8pnksuq', 'de638c8a3aa3db5ee6ed903cba271a20', '3518f8dd2fdba8e696e16298dbcd6e9d', 2, 0, 'MI111200111', '2011-12-07 17:35:53', 'MI111200111', '2011-12-07 17:35:53', 0),
('MI111200112', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'pmdez5cb', '99cee303e9d93c3ff651daa1a9166c13', '788822dd053cbbe811fa6ec2482906c5', 2, 0, 'MI111200112', '2011-12-07 17:35:58', 'MI111200112', '2011-12-07 17:35:58', 0),
('MI111200113', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'q64srar4', 'ee82ed4b31b8ac6d5f50d30eb511a5e5', '5d1ffaa6211b479af97d012579d77715', 2, 0, 'MI111200113', '2011-12-07 17:36:08', 'MI111200113', '2011-12-07 17:36:08', 0),
('MI111200114', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'gu6jzh67', '8baaf9145df22a0d67cd9fa855457ea3', '0358afc7c4514e02d75e72c498305657', 2, 0, 'MI111200114', '2011-12-07 17:36:12', 'MI111200114', '2011-12-07 17:36:12', 0),
('MI111200115', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'zae2mzc7', 'ad1f08c063e7206bf201d0ad015edbab', 'e956b91b90aaa202ccb840a0a839de6a', 2, 0, 'MI111200115', '2011-12-07 17:36:17', 'MI111200115', '2011-12-07 17:36:17', 0),
('MI111200116', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'ff54zy0z', 'c665a766f3990bdf2f88d1ef9b546e9b', '92f1863e84a46a04f01ce6c540b4bc71', 2, 0, 'MI111200116', '2011-12-07 17:36:21', 'MI111200116', '2011-12-07 17:36:21', 0),
('MI111200117', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'yvihjddg', '127aae32e0e7779c665965e3d3bc8217', '96e18519771472405d35a203290e2602', 2, 0, 'MI111200117', '2011-12-07 17:36:26', 'MI111200117', '2011-12-07 17:36:26', 0),
('MI111200118', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'auenz7xh', '4f0fda58966d3398d207b862e2140f55', '51fca4abc7df775c636ed7d90a30c1c0', 2, 0, 'MI111200118', '2011-12-07 17:37:15', 'MI111200118', '2011-12-07 17:37:15', 0),
('MI111200119', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'ct4cc3jd', '8abaa66c8ea93e807a2447496b129633', 'f250653faa34874e9db1093720faf43f', 2, 0, 'MI111200119', '2011-12-07 17:37:19', 'MI111200119', '2011-12-07 17:37:19', 0),
('MI111200120', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', '6fi7meif', '08738b8d8a8cdda513d47e45d2484d89', 'b67057d2102d76db5aa9b8793656fe81', 2, 0, 'MI111200120', '2011-12-07 17:37:24', 'MI111200120', '2011-12-07 17:37:24', 0),
('MI111200121', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'no2pyuhm', '116b8788aee13783b82b7e69503338c2', 'd80be52f8c898c590e44a2d8b6b05677', 2, 0, 'MI111200121', '2011-12-07 17:37:28', 'MI111200121', '2011-12-07 17:37:28', 0),
('MI111200122', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'oyy5fasx', '1defd9040d717bf08a0c1dfc2b7ff273', '3df282662446b6a58ee5250877ffcfca', 2, 0, 'MI111200122', '2011-12-07 17:37:33', 'MI111200122', '2011-12-07 17:37:33', 0),
('MI111200123', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 17, 'Gianyar1', 'Gianyar', 'asd', 'ketewel2', 'ketewel3', '80852', 'mcst3jnz', '5617fedeb41dd07507a3d499242011a9', 'b9309f96ef1dbe2576f91863bf26380d', 2, 0, 'MI111200123', '2011-12-08 11:57:22', 'MI111200123', '2011-12-08 11:57:22', 0),
('MI111200124', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '', 4, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'qxz6qayx', '6f534b5595c21b72241d6f124a5bb004', 'b1a3d56a321e480bdcfbfaac173fc896', 2, 0, 'MI111200124', '2011-12-08 13:53:18', 'MI111200124', '2011-12-08 13:53:18', 0),
('MI111200125', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'asd', 'ketewel3', '80852', '2bkfyf65', '928f7391321b34ef0d9ff78eade6b18b', '315a7393cefd3c127a4378ee600e8a4d', 2, 0, 'MI111200125', '2011-12-08 13:58:23', 'MI111200125', '2011-12-08 13:58:23', 0),
('MI111200126', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', 'a', '', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '1', '4n6z44te', 'fd5f56b62d42f4436293fd469ac027d1', '8702999b987b684be64b0e9c5ec98791', 2, 0, 'MI111200126', '2011-12-08 15:00:16', 'MI111200126', '2011-12-08 15:00:16', 0);
INSERT INTO `lumonata_members` (`lmember_id`, `lfname`, `llname`, `lemail`, `lphone`, `lphone2`, `lcountry_id`, `lregion`, `lcity`, `laddress`, `laddress2`, `laddress3`, `lpostal_code`, `lpassword`, `lpass`, `lactv_code`, `laccount_status`, `llast_login`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
('MI111200127', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 15, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'os6s7pjn', '6c8533fdb3f46c8cbc33a09f6c1f4794', '62729cbbb5cb4af5cb27fa52f0eb4cd6', 2, 0, 'MI111200127', '2011-12-08 15:46:26', 'MI111200127', '2011-12-08 15:46:26', 0),
('MI111200128', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'pwtqx45u', '56c381ab53d0c49ee4cf5afd52969576', '3f034802d763a41e499771549e5fe855', 2, 0, 'MI111200128', '2011-12-08 16:25:20', 'MI111200128', '2011-12-08 16:25:20', 0),
('MI111200129', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'njydqy4f', '0063913c2c4c518ceada17fdd2099a98', 'b9bc31a4e0552e9ab785eb81d1074de0', 2, 0, 'MI111200129', '2011-12-08 16:27:02', 'MI111200129', '2011-12-08 16:27:02', 0),
('MI111200130', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'idvqw3oq', '0390341823cfe351d39fd6e51d06366a', 'e9df9a75f02939810ca57fcf8365e03d', 2, 0, 'MI111200130', '2011-12-08 16:27:46', 'MI111200130', '2011-12-08 16:27:46', 0),
('MI111200131', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '8526984755', '08523655546', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'hz7uq70w', 'd60e77e49922850a9589017b50d02a7e', '547ad3b400daa2fabde25987ab47c742', 2, 0, 'MI111200131', '2011-12-08 16:28:05', 'MI111200131', '2011-12-08 16:28:05', 0),
('MI111200132', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'b8r57xy6', '734518e1866aa8df4216be357e9357a6', 'd688ef6e4917ef73e2f37f637d843abb', 2, 0, 'MI111200132', '2011-12-08 16:34:48', 'MI111200132', '2011-12-08 16:34:48', 0),
('MI111200133', 'naruto', 'Astina Putra', 'widia_xp@lumonata.com', '08523655545', '', 4, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'evizvhax', 'bd21052e496c996ff2ecf01c8767e529', 'beddf20475e245698170751274d9a200', 2, 0, 'MI111200133', '2011-12-08 16:39:17', 'MI111200133', '2011-12-08 16:39:17', 0),
('MI111200134', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 16, 'Gianyar1', 'Gianyar', 'ketewel', '', 'ketewel3', '1', 'fbwv22qv', '6dc9c9a71981da126df3cd6298556c9f', '1a682eecf4d2a2e22becdfc91c0b7328', 2, 0, 'MI111200134', '2011-12-08 16:55:48', 'MI111200134', '2011-12-08 16:55:48', 0),
('MI111200135', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 2, '', 'Gianyar', 'ketewel', '', '', '80852', 'jky4cnfi', 'addc104250bb243edd3f8b78f6f4dfb0', '12e55f281ed456bbb6115b812d03e7a8', 2, 0, 'MI111200135', '2011-12-08 16:58:45', 'MI111200135', '2011-12-08 16:58:45', 0),
('MI111200136', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 2, '', 'Gianyar', 'ketewel', '', '', '80852', 'syd5jq7n', 'a506d78968fecf20d400ab92e9908792', '5dc4e30c72d7c733d59f8bcb4eed5f5b', 2, 0, 'MI111200136', '2011-12-08 16:59:53', 'MI111200136', '2011-12-08 16:59:53', 0),
('MI111200137', 'I Ketut Widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 15, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', '78fe8zos', '54602b523f3199e6e8d23ad036167368', '29fb0edabe358b38b5a0f6a3726e8839', 2, 0, 'MI111200137', '2011-12-08 17:00:46', 'MI111200137', '2011-12-08 17:00:46', 0),
('MI111200138', 'I Ketut Widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 15, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', '0a8sg85y', '723478928deaef95a014d41fc070790b', 'fa4a7cf168ea22abf69a18462344d88a', 2, 0, 'MI111200138', '2011-12-08 17:01:15', 'MI111200138', '2011-12-08 17:01:15', 0),
('MI111200139', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 15, 'Gianyar1', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'rhgjbfua', '8807d82a06cab47f2254230f30dd7be8', '181e028b2d7293bd4c2ba31ddf21033d', 2, 0, 'MI111200139', '2011-12-08 17:07:12', 'MI111200139', '2011-12-08 17:07:12', 0),
('MI111200140', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '', 2, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'zm28i0zm', '00e95e0b00e74f050a5deacb1355d5c8', '7f01a0fd2c57cd6d0ecd4b8a8149cc17', 2, 0, 'MI111200140', '2011-12-08 17:08:26', 'MI111200140', '2011-12-08 17:08:26', 0),
('MI111200141', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 16, '', 'Gianyar', 'asd', '', 'a', '80852', 'zdq57rvu', 'b491d5df8eea994f61d5a29359f4c33c', '54399049df7e0ebd0ce9f71a593d05de', 2, 0, 'MI111200141', '2011-12-08 17:14:24', 'MI111200141', '2011-12-08 17:14:24', 0),
('MI111200142', 'naruto', 'uzumaki', 'am_too_wed@yahoo.com', '08523655545', '08523655546', 3, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '1', '0xzpzgay', '8867f7b95844837c864694b793b1e5b2', '02fdc427a13f2720c2e8ffdc02a7fa12', 2, 0, 'MI111200142', '2011-12-08 17:16:13', 'MI111200142', '2011-12-08 17:16:13', 0),
('MI111200143', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 2, '', 'Gianyar', 'ketewel', '', '', '80852', 'rh2uiwj6', '6ce486957ee21f559f32991ab368de40', '2b5b114b4b0617645c6001a34bc94238', 2, 0, 'MI111200143', '2011-12-08 17:21:27', 'MI111200143', '2011-12-08 17:21:27', 0),
('MI111200144', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 2, 'Gianyar1', 'a', 'asd', 'asd', 'a', '80852', 'h0iptv4h', '4745cef2866c6b6311d2b94a5d42b022', '108dfa279b6993a18b1991d6e7d4fc74', 2, 0, 'MI111200144', '2011-12-08 17:24:51', 'MI111200144', '2011-12-08 17:24:51', 0),
('MI111200145', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'oj2bdfto', 'fa0373c0d0639517bb2f9e2629f36931', 'd0d629709acf9a88997bed74b2614917', 2, 0, 'MI111200145', '2011-12-08 17:29:29', 'MI111200145', '2011-12-08 17:29:29', 0),
('MI111200146', 'naruto', 'Astina Putra', 'widia@lumonata.com', '08523655545', '', 16, '', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', '3utgmn5s', 'e994cd49d02a911f090552e9ca10a88d', '3c96d5f673808014feda8e1f8c055cec', 2, 0, 'MI111200146', '2011-12-08 17:54:06', 'MI111200146', '2011-12-08 17:54:06', 0),
('MI111200147', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'uyxjgsfb', '5b2b8ee2d516382632814bb3497bf2de', '89ba41f4d46d03970689faec5537ef07', 2, 0, 'MI111200147', '2011-12-15 14:37:38', 'MI111200147', '2011-12-15 14:37:38', 0),
('MI111200148', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'm3ddyvs8', 'dce16725ddb35a22b8009008f9b3a545', '5723e267c914300bbf0eb630a88d0de0', 2, 0, 'MI111200148', '2011-12-15 14:38:00', 'MI111200148', '2011-12-15 14:38:00', 0),
('MI111200149', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 103, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', '2ngfxx8q', '398d204d29120408f909eb74690c5273', '8056f4efe19e8ec74c8871bc867cef9a', 2, 0, 'MI111200149', '2011-12-15 14:38:07', 'MI111200149', '2011-12-15 14:38:07', 0),
('MI111200150', 'naruto', 'uzumaki', 'widia.xp@gmail.com', '08523655545', '', 15, '', 'Gianyar', 'ketewel', 'asd', 'ketewel3', '80852', 'jfhywir2', 'd634e80c5566050390e65ddbddb90053', 'eef6211ebd5a9a763fb76a6cb9fa6246', 2, 0, 'MI111200150', '2011-12-15 14:41:36', 'MI111200150', '2011-12-15 14:41:36', 0),
('MI111200151', 'naruto', 'uzumaki', 'widia.xp@gmail.com', '08523655545', '', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'nr3j26eb', 'fd8c0245a7cc13bc0c8dd2073111e88c', 'f0c2988e121273d13c44cc7201f4ddc5', 2, 0, 'MI111200151', '2011-12-15 14:48:16', 'MI111200151', '2011-12-15 14:48:16', 0),
('MI111200152', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '08523655546', 14, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'czw38eaz', '2845d7a53f84866ade3eda1e8029b8f8', '7f96d369504b67379313e9d9ef482c97', 2, 0, 'MI111200152', '2011-12-15 15:00:58', 'MI111200152', '2011-12-15 15:00:58', 0),
('MI111200153', 'naruto', 'uzumaki', 'widia@lumonata.com', '8526984755', '08523655546', 5, '22', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'bcwonp2y', 'd7f05978f1abe4c1353628df087e63f0', '65caa1ebb3a92e3b4e99676b1ae737ce', 2, 0, 'MI111200153', '2011-12-15 15:35:10', 'MI111200153', '2011-12-15 15:35:10', 0),
('MI111200154', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 2, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'i77prx3c', '0fa245c4ed827877dc2e1937dd852fcb', 'd9792bcd9dd4411016092c0ca28764d6', 2, 0, 'MI111200154', '2011-12-15 15:37:43', 'MI111200154', '2011-12-15 15:37:43', 0),
('MI111200155', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 11, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'm5mq3h4k', '3012deccdffb5afef69c102d49933878', '295b3a9fb2cea8127223f9b423148e1b', 2, 0, 'MI111200155', '2011-12-15 15:45:56', 'MI111200155', '2011-12-15 15:45:56', 0),
('MI111200156', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 18, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'epxte6y3', '660cc62770d830aaf9c285a46c0599ac', '0a445c30ed48214c041fcedb5348495b', 2, 0, 'MI111200156', '2011-12-15 16:42:17', 'MI111200156', '2011-12-15 16:42:17', 0),
('MI111200157', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 2, '', 'Gianyar', 'ketewel', 'asd', 'ketewel3', '80852', 'qwmm4hp2', '923053f03326851e4143484ba0f3feff', 'dd14d5e063f90056d4b0ad3cf4acd970', 2, 0, 'MI111200157', '2011-12-15 17:13:16', 'MI111200157', '2011-12-15 17:13:16', 0),
('MI111200158', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'tkuv3oi8', '309472775d4af68116e3470474d95b63', '28fc5656bf80504c377c2a9e2ee1c473', 2, 0, 'MI111200158', '2011-12-15 17:16:33', 'MI111200158', '2011-12-15 17:16:33', 0),
('MI111200159', 'I Ketut Widia', 'uzumaki', 'widia@lumonata.com', '8526984755', '', 15, '', 'Gianyar', 'ketewel', 'asd', 'ketewel3', '80852', 'vwv5zugk', '85075e6698c61adc89ee55ea6f3c0584', 'a3b009088aa7d93c2dbdc08990a9aff3', 2, 0, 'MI111200159', '2011-12-16 16:42:10', 'MI111200159', '2011-12-16 16:42:10', 0),
('MI111200160', 'I Ketut Widia', 'Astina Putra', 'widia@lumonata.com', '08523655545', '08523655546', 16, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '1', 'm4aib2jp', '6d35a2afec8b0fd21874a97638ee0e63', '54066caef20c1814ba6adb16a582c565', 2, 0, 'MI111200160', '2011-12-16 16:43:16', 'MI111200160', '2011-12-16 16:43:16', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_meta_data`
-- 

CREATE TABLE `lumonata_meta_data` (
  `lmeta_id` int(11) NOT NULL auto_increment,
  `lmeta_name` varchar(200) character set utf8 NOT NULL,
  `lmeta_value` longtext character set utf8 NOT NULL,
  `lapp_name` varchar(200) character set utf8 NOT NULL,
  `lapp_id` int(11) NOT NULL,
  PRIMARY KEY  (`lmeta_id`),
  KEY `meta_name` (`lmeta_name`),
  KEY `app_name` (`lapp_name`),
  KEY `app_id` (`lapp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=119 ;

-- 
-- Dumping data for table `lumonata_meta_data`
-- 

INSERT INTO `lumonata_meta_data` (`lmeta_id`, `lmeta_name`, `lmeta_value`, `lapp_name`, `lapp_id`) VALUES 
(1, 'front_theme', 'baliworking4u', 'themes', 0),
(2, 'time_zone', 'Asia/Singapore', 'global_setting', 0),
(3, 'site_url', '10.10.10.18/baliworking4u', 'global_setting', 0),
(4, 'web_title', 'Baliworking4u', 'global_setting', 0),
(5, 'smtp_server', '10.10.10.43', 'global_setting', 0),
(6, 'admin_theme', 'default', 'themes', 0),
(7, 'smtp', 'mail.email.com', 'global_setting', 0),
(8, 'email', 'widia@lumonata.com', 'global_setting', 0),
(9, 'web_tagline', '', 'global_setting', 0),
(10, 'invitation_limit', '10', 'global_setting', 0),
(11, 'date_format', 'F j, Y', 'global_setting', 0),
(12, 'time_format', 'H:i', 'global_setting', 0),
(13, 'post_viewed', '10', 'global_setting', 0),
(14, 'rss_viewed', '15', 'global_setting', 0),
(15, 'rss_view_format', 'full_text', 'global_setting', 0),
(16, 'list_viewed', '30', 'global_setting', 0),
(17, 'email_format', 'plain_text', 'global_setting', 0),
(18, 'text_editor', 'tiny_mce', 'global_setting', 0),
(19, 'thumbnail_image_size', '135:200', 'global_setting', 0),
(20, 'large_image_size', '800:600', 'global_setting', 0),
(21, 'medium_image_size', '340:400', 'global_setting', 0),
(22, 'is_allow_comment', '1', 'global_setting', 0),
(23, 'is_login_to_comment', '1', 'global_setting', 0),
(24, 'is_auto_close_comment', '0', 'global_setting', 0),
(25, 'days_auto_close_comment', '15', 'global_setting', 0),
(26, 'is_break_comment', '1', 'global_setting', 0),
(27, 'comment_page_displayed', 'last', 'global_setting', 0),
(28, 'comment_per_page', '3', 'global_setting', 0),
(29, 'active_plugins', '{"shopping-cart":"\\/shopping_cart\\/shoping.php","lumonata-meta-data":"\\/metadata\\/metadata.php","contact-us":"\\/contact_us\\/contact_us.php"}', 'plugins', 0),
(30, 'save_changes', 'Save Changes', 'global_setting', 0),
(31, 'is_rewrite', 'yes', 'global_setting', 0),
(32, 'is_allow_post_like', '1', 'global_setting', 0),
(33, 'is_allow_comment_like', '1', 'global_setting', 0),
(34, 'alert_on_register', '1', 'global_setting', 0),
(35, 'alert_on_comment', '1', 'global_setting', 0),
(36, 'alert_on_comment_reply', '1', 'global_setting', 0),
(37, 'alert_on_liked_post', '1', 'global_setting', 0),
(38, 'alert_on_liked_comment', '1', 'global_setting', 0),
(39, 'web_name', 'Baliworking4u                                  ', 'global_setting', 0),
(40, 'meta_description', '', 'global_setting', 0),
(41, 'meta_keywords', '', 'global_setting', 0),
(42, 'meta_title', 'Baliworking4u', 'global_setting', 0),
(43, 'custome_bg_color', 'c4c4c4', 'themes', 0),
(44, 'status_viewed', '30', 'global_setting', 0),
(45, 'update', 'true', 'global_setting', 0),
(46, 'the_date_format', 'F j, Y', 'global_setting', 0),
(47, 'the_time_format', 'H:i', 'global_setting', 0),
(48, 'thumbnail_image_width', '135', 'global_setting', 0),
(49, 'thumbnail_image_height', '200', 'global_setting', 0),
(50, 'medium_image_width', '340', 'global_setting', 0),
(51, 'medium_image_height', '400', 'global_setting', 0),
(52, 'large_image_width', '800', 'global_setting', 0),
(53, 'large_image_height', '600', 'global_setting', 0),
(54, 'menu_set', '{"footermenu":"footer_menu","mainmenu":"main_menu"}', 'menus', 0),
(113, 'payments', '{"parent_payment":["payments_bank_transfer","payments_paypal_standard","payments_cheque"],"child_payment":[{"name":"Bank transfer","text":"000-00000000-00","mode":"1"},{"name":"PayPal","text":"widia._1318827318_biz@gmail.com","mode":"0"},{"name":"Cheque","text":"BCA","mode":"1"}]}', 'product_setting', 0),
(57, 'language', '1', 'product_setting', 0),
(58, 'currency', '6', 'product_setting', 0),
(59, 'unit_system', 'imperial', 'product_setting', 0),
(114, 'menu_items_footermenu', '[{"id":0,"label":"Products","target":"_self","link":"\\/?page_id=83","permalink":"products\\/"},{"id":1,"label":"How to Order","target":"_self","link":"\\/?page_id=84","permalink":"how-to-order\\/"},{"id":2,"label":"About Us","target":"_self","link":"\\/?page_id=85","permalink":"about-us\\/"},{"id":3,"label":"Contact Us","target":"_self","link":"\\/?page_id=86","permalink":"contact-us\\/"},{"id":4,"label":"Home","target":"_self","link":"http:\\/\\/10.10.10.18\\/baliworking4u\\/","permalink":"http:\\/\\/10.10.10.18\\/baliworking4u\\/"}]', 'menus', 0),
(115, 'menu_order_footermenu', '[{"id":"4"},{"id":"0"},{"id":"1"},{"id":"2"},{"id":"3"}]', 'menus', 0),
(116, 'menu_items_mainmenu', '[{"id":0,"label":"Products","target":"_self","link":"\\/?page_id=83","permalink":"products\\/"},{"id":1,"label":"How to Order","target":"_self","link":"\\/?page_id=84","permalink":"how-to-order\\/"},{"id":2,"label":"About Us","target":"_self","link":"\\/?page_id=85","permalink":"about-us\\/"},{"id":3,"label":"Contact Us","target":"_self","link":"\\/?page_id=86","permalink":"contact-us\\/"},{"id":4,"label":"Home","target":"_self","link":"http:\\/\\/10.10.10.18\\/baliworking4u\\/","permalink":"http:\\/\\/10.10.10.18\\/baliworking4u\\/"}]', 'menus', 0),
(117, 'menu_order_mainmenu', '[{"id":"4"},{"id":"0"},{"id":"1"},{"id":"2"},{"id":"3"}]', 'menus', 0),
(118, 'minimum_price', '0.00', 'product_setting', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_notifications`
-- 

CREATE TABLE `lumonata_notifications` (
  `lnotification_id` bigint(20) NOT NULL auto_increment,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY  (`lnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `lumonata_notifications`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order`
-- 

CREATE TABLE `lumonata_order` (
  `lorder_id` varchar(50) NOT NULL,
  `lmember_id` varchar(50) NOT NULL,
  `lorder_shipping_id` varchar(50) NOT NULL,
  `lorder_shipping_status` int(5) NOT NULL COMMENT '1=difference;0=same;',
  `lorder_date` datetime NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '1=Pending; 2=Paid, 3=Cancelled, 4=Partially Shipped,5=Shipped',
  `lstatus_archive` int(11) NOT NULL,
  `litem_total` int(10) NOT NULL,
  `lquantity_total` int(10) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lshipping_price` decimal(10,2) NOT NULL,
  `tax_product` decimal(10,2) NOT NULL,
  `tax_shipping` decimal(10,2) NOT NULL,
  `lprice_total` decimal(10,2) NOT NULL,
  `lpayment_method` varchar(60) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lorder_id`,`llang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `lumonata_order`
-- 

INSERT INTO `lumonata_order` (`lorder_id`, `lmember_id`, `lorder_shipping_id`, `lorder_shipping_status`, `lorder_date`, `lstatus`, `lstatus_archive`, `litem_total`, `lquantity_total`, `lcurrency_id`, `lshipping_price`, `tax_product`, `tax_shipping`, `lprice_total`, `lpayment_method`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
('OI111200001', 'MI111200158', 'OS111200048', 0, '2011-12-15 17:16:33', 2, 0, 1, 1, 6, 9.00, 0.00, 0.81, 90.00, 'payments_paypal_standard', 'MI111200158', '2011-12-15 17:16:33', 'MI111200158', '2011-12-15 17:16:33', 0),
('OI111200002', 'MI111200159', 'OS111200049', 0, '2011-12-16 16:42:10', 1, 0, 2, 2, 6, 9.00, 0.00, 0.81, 690.00, 'payments_bank_transfer', 'MI111200159', '2011-12-16 16:42:10', 'MI111200159', '2011-12-16 16:42:10', 0),
('OI111200003', 'MI111200160', 'OS111200050', 0, '2011-12-16 16:43:16', 1, 0, 1, 1, 6, 9.00, 0.00, 0.81, 65.00, 'payments_bank_transfer', 'MI111200160', '2011-12-16 16:43:16', 'MI111200160', '2011-12-16 16:43:16', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order_detail`
-- 

CREATE TABLE `lumonata_order_detail` (
  `ldetail_id` bigint(50) NOT NULL auto_increment,
  `lorder_id` varchar(50) NOT NULL,
  `lproduct_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lqty` int(25) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(100) NOT NULL,
  `ldlu` int(50) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`ldetail_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=318 ;

-- 
-- Dumping data for table `lumonata_order_detail`
-- 

INSERT INTO `lumonata_order_detail` (`ldetail_id`, `lorder_id`, `lproduct_id`, `lprice`, `lqty`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
(317, 'OI111200003', 95, 65.00, 1, 'MI111200160', 2011, 'MI111200160', 2011, 0),
(314, 'OI111200001', 92, 90.00, 1, 'MI111200158', 2011, 'MI111200158', 2011, 0),
(315, 'OI111200002', 92, 90.00, 1, 'MI111200159', 2011, 'MI111200159', 2011, 0),
(316, 'OI111200002', 104, 600.00, 1, 'MI111200159', 2011, 'MI111200159', 2011, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order_detail_material`
-- 

CREATE TABLE `lumonata_order_detail_material` (
  `ldetail_material_id` bigint(50) NOT NULL auto_increment,
  `ldetail_id` varchar(50) NOT NULL,
  `lstatus` smallint(5) NOT NULL COMMENT '1=material;2=product;3=Reorder as Print ;4=Reorder as Other Product ;5=Reorder ;',
  `lproduct_id` int(11) NOT NULL,
  `product_variant` text NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lqty` int(25) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(100) NOT NULL,
  `ldlu` int(50) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`ldetail_material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=318 ;

-- 
-- Dumping data for table `lumonata_order_detail_material`
-- 

INSERT INTO `lumonata_order_detail_material` (`ldetail_material_id`, `ldetail_id`, `lstatus`, `lproduct_id`, `product_variant`, `lprice`, `lqty`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
(314, '314', 0, 92, '{"parent_variant":"493","child_variant":"567"}', 90.00, 1, 'MI111200158', 2011, 'MI111200158', 2011, 0),
(315, '315', 0, 92, '{"parent_variant":"493","child_variant":"567"}', 90.00, 1, 'MI111200159', 2011, 'MI111200159', 2011, 0),
(316, '316', 0, 104, '{"parent_variant":"493","child_variant":"591"}', 600.00, 1, 'MI111200159', 2011, 'MI111200159', 2011, 0),
(317, '317', 0, 95, '{"parent_variant":"493","child_variant":"573"}', 65.00, 1, 'MI111200160', 2011, 'MI111200160', 2011, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order_payment`
-- 

CREATE TABLE `lumonata_order_payment` (
  `lpayment_id` int(11) NOT NULL auto_increment,
  `lorder_id` varchar(50) collate latin1_general_ci NOT NULL,
  `lstatus` tinyint(10) NOT NULL,
  `lpayment_method` varchar(50) collate latin1_general_ci NOT NULL,
  `lpaid` varchar(50) collate latin1_general_ci NOT NULL,
  `lname_paid` varchar(50) collate latin1_general_ci NOT NULL,
  `ldestination_bank` varchar(50) collate latin1_general_ci NOT NULL,
  `ldestination_account` varchar(200) collate latin1_general_ci NOT NULL,
  `laccount_number` varchar(50) collate latin1_general_ci NOT NULL,
  `ldate_trans` int(11) NOT NULL,
  `lipn` varchar(60) collate latin1_general_ci NOT NULL,
  `lfile` text collate latin1_general_ci NOT NULL,
  `lcreated_by` varchar(50) collate latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) collate latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  `first_name` varchar(50) collate latin1_general_ci NOT NULL,
  `payer_email` varchar(50) collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`lpayment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=138 ;

-- 
-- Dumping data for table `lumonata_order_payment`
-- 

INSERT INTO `lumonata_order_payment` (`lpayment_id`, `lorder_id`, `lstatus`, `lpayment_method`, `lpaid`, `lname_paid`, `ldestination_bank`, `ldestination_account`, `laccount_number`, `ldate_trans`, `lipn`, `lfile`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `first_name`, `payer_email`) VALUES 
(135, 'OI111200001', 2, 'payments_paypal_standard', '99.81', 'naruto', '', '', '', 1323940594, '', '', 'MI111200158', 1323940594, 'MI111200158', 1323940594, 'naruto', 'widia._1318824593_per@gmail.com'),
(136, 'OI111200002', 0, 'payments_bank_transfer', '699.81', 'I Ketut Widia', '', '', '', 1324024930, '', '', 'MI111200159', 1324024930, 'MI111200159', 1324024930, '', ''),
(137, 'OI111200003', 0, 'payments_bank_transfer', '74.81', 'I Ketut Widia', '', '', '', 1324024996, '', '', 'MI111200160', 1324024996, 'MI111200160', 1324024996, '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_order_shipping`
-- 

CREATE TABLE `lumonata_order_shipping` (
  `lorder_shipping_id` varchar(50) NOT NULL,
  `lfname` varchar(300) NOT NULL,
  `llname` varchar(200) NOT NULL,
  `lemail` varchar(300) NOT NULL,
  `lphone` varchar(50) NOT NULL,
  `lphone2` varchar(50) NOT NULL,
  `lcountry_id` int(10) NOT NULL,
  `lshipping_price` decimal(10,2) NOT NULL,
  `lregion` varchar(255) NOT NULL,
  `lcity` varchar(255) NOT NULL,
  `laddress` varchar(255) NOT NULL,
  `laddress2` varchar(255) NOT NULL,
  `laddress3` varchar(255) NOT NULL,
  `lpostal_code` varchar(100) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lorder_shipping_id`,`llang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `lumonata_order_shipping`
-- 

INSERT INTO `lumonata_order_shipping` (`lorder_shipping_id`, `lfname`, `llname`, `lemail`, `lphone`, `lphone2`, `lcountry_id`, `lshipping_price`, `lregion`, `lcity`, `laddress`, `laddress2`, `laddress3`, `lpostal_code`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
('OS111200035', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200129', '2011-12-08 16:27:02', 'MI111200129', '2011-12-08 16:27:02', 0),
('OS111200009', 'naruto', 'astina', 'widia_xp@lumonata.com', '08523655545', '', 102, 1.00, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'MI111200102', '2011-12-07 11:10:14', 'MI111200102', '2011-12-07 11:10:14', 0),
('OS111200012', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200106', '2011-12-07 17:34:05', 'MI111200106', '2011-12-07 17:34:05', 0),
('OS111200013', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200107', '2011-12-07 17:34:10', 'MI111200107', '2011-12-07 17:34:10', 0),
('OS111200014', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 3, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200108', '2011-12-07 17:34:16', 'MI111200108', '2011-12-07 17:34:16', 0),
('OS111200023', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200117', '2011-12-07 17:36:26', 'MI111200117', '2011-12-07 17:36:26', 0),
('OS111200016', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200110', '2011-12-07 17:35:24', 'MI111200110', '2011-12-07 17:35:24', 0),
('OS111200017', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200111', '2011-12-07 17:35:53', 'MI111200111', '2011-12-07 17:35:53', 0),
('OS111200018', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200112', '2011-12-07 17:35:58', 'MI111200112', '2011-12-07 17:35:58', 0),
('OS111200019', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200113', '2011-12-07 17:36:08', 'MI111200113', '2011-12-07 17:36:08', 0),
('OS111200020', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200114', '2011-12-07 17:36:12', 'MI111200114', '2011-12-07 17:36:12', 0),
('OS111200021', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200115', '2011-12-07 17:36:17', 'MI111200115', '2011-12-07 17:36:17', 0),
('OS111200022', 'naruto', 'uzumaki', 'widia_xp@yahoo.com', '08523655545', '08523655546', 2, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200116', '2011-12-07 17:36:21', 'MI111200116', '2011-12-07 17:36:21', 0),
('OS111200006', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 111, 1.00, '', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200099', '2011-12-05 16:33:11', 'MI111200099', '2011-12-05 16:33:11', 0),
('OS111200028', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'MI111200122', '2011-12-07 17:37:33', 'MI111200122', '2011-12-07 17:37:33', 0),
('OS111200027', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'MI111200121', '2011-12-07 17:37:28', 'MI111200121', '2011-12-07 17:37:28', 0),
('OS111200025', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'MI111200119', '2011-12-07 17:37:19', 'MI111200119', '2011-12-07 17:37:19', 0),
('OS111200026', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '2', '08523655546', 16, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'a', '80852', 'MI111200120', '2011-12-07 17:37:24', 'MI111200120', '2011-12-07 17:37:24', 0),
('OS111200042', 'naruto', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 2, 1.00, '', 'Gianyar', 'ketewel', '', '', '80852', 'MI111200136', '2011-12-08 16:59:53', 'MI111200136', '2011-12-08 16:59:53', 0),
('OS111200044', 'I Ketut Widia', 'uzumaki', 'widia_xp@lumonata.com', '08523655545', '', 15, 1.00, '', 'Gianyar', 'ketewel', '', 'ketewel3', '80852', 'MI111200138', '2011-12-08 17:01:15', 'MI111200138', '2011-12-08 17:01:15', 0),
('OS111200048', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '08523655546', 16, 9.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200158', '2011-12-15 17:16:33', 'MI111200158', '2011-12-15 17:16:33', 0),
('OS111200049', 'I Ketut Widia', 'uzumaki', 'widia@lumonata.com', '8526984755', '', 15, 9.00, '', 'Gianyar', 'ketewel', 'asd', 'ketewel3', '80852', 'MI111200159', '2011-12-16 16:42:10', 'MI111200159', '2011-12-16 16:42:10', 0),
('OS111200050', 'I Ketut Widia', 'Astina Putra', 'widia@lumonata.com', '08523655545', '08523655546', 16, 9.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '1', 'MI111200160', '2011-12-16 16:43:16', 'MI111200160', '2011-12-16 16:43:16', 0),
('OS111200047', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 103, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200149', '2011-12-15 14:38:07', 'MI111200149', '2011-12-15 14:38:07', 0),
('OS111200046', 'naruto', 'uzumaki', 'widia@lumonata.com', '08523655545', '', 103, 1.00, 'Gianyar1', 'Gianyar', 'ketewel', 'ketewel2', 'ketewel3', '80852', 'MI111200148', '2011-12-15 14:38:00', 'MI111200148', '2011-12-15 14:38:00', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_rules`
-- 

CREATE TABLE `lumonata_rules` (
  `lrule_id` bigint(20) NOT NULL auto_increment,
  `lparent` bigint(20) NOT NULL,
  `lname` varchar(200) character set utf8 NOT NULL,
  `lsef` varchar(200) character set utf8 NOT NULL,
  `ldescription` text character set utf8 NOT NULL,
  `lrule` varchar(200) character set utf8 NOT NULL,
  `lgroup` varchar(200) character set utf8 NOT NULL,
  `lcount` bigint(20) NOT NULL default '0',
  `lorder` bigint(20) NOT NULL default '1',
  `lsubsite` varchar(100) collate latin1_general_ci NOT NULL default 'arunna',
  PRIMARY KEY  (`lrule_id`),
  KEY `rules_name` (`lname`),
  KEY `sef` (`lsef`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=598 ;

-- 
-- Dumping data for table `lumonata_rules`
-- 

INSERT INTO `lumonata_rules` (`lrule_id`, `lparent`, `lname`, `lsef`, `ldescription`, `lrule`, `lgroup`, `lcount`, `lorder`, `lsubsite`) VALUES 
(1, 0, 'Uncategorized', 'uncategorized', '', 'categories', 'default', 6, 306, 'arunna'),
(2, 0, 'Designer', 'designer', '', 'categories', 'global_settings', 0, 199, 'arunna'),
(3, 0, 'Entepreneurs', 'entepreneurs', '', 'categories', 'global_settings', 1, 198, 'arunna'),
(4, 0, 'Photographer', 'photographer', '', 'categories', 'global_settings', 0, 197, 'arunna'),
(5, 0, 'Programmer', 'programmer', '', 'categories', 'global_settings', 1, 196, 'arunna'),
(6, 0, 'CEO', 'ceo', '', 'tags', 'profile', 1, 195, 'arunna'),
(7, 0, 'Cooking', 'cooking', '', 'skills', 'profile', 1, 194, 'arunna'),
(8, 0, 'User', 'user', 'User Dumper', 'categories', 'global_settings', 0, 193, 'arunna'),
(10, 0, 'September 2011', 'september-2011', '', 'categories', 'articles', 0, 191, 'arunna'),
(11, 1, 'Sub Catagories', 'sub-catagories', 'test catagories', 'categories', 'articles', 0, 190, 'arunna'),
(573, 493, 'h80 x 40 x 40l', 'h80-x-40-x-40l', '', 'variant', 'product', 0, 7, 'arunna'),
(16, 0, 'shoes', 'shoes', '', 'catagories', 'products', 0, 185, 'arunna'),
(493, 0, 'Size', 'size', '', 'variant', 'product', 0, 15, 'arunna'),
(572, 493, 'h80 x 25s', 'h80-x-25s', '', 'variant', 'product', 0, 7, 'arunna'),
(502, 0, 'Glassess', 'glassess', 'Glassess', 'categories', 'products', 0, 11, 'arunna'),
(499, 498, 'Silver', 'silver', '', 'variant', 'product', 0, 13, 'arunna'),
(489, 0, 'jewelry', 'jewelry', 'jewelry', 'tags', 'products', 0, 17, 'arunna'),
(445, 0, 'Sport', 'sport', '', 'variant', 'product', 0, 37, 'arunna'),
(446, 445, 'sport1', 'sport1', '', 'variant', 'product', 0, 37, 'arunna'),
(447, 445, 'sport2', 'sport2', '', 'variant', 'product', 0, 37, 'arunna'),
(411, 0, 'Color', 'color', '', 'variant', 'product', 0, 48, 'arunna'),
(438, 411, 'Black', 'black', '', 'variant', 'product', 0, 40, 'arunna'),
(439, 411, 'Blue', 'blue', '', 'variant', 'product', 0, 40, 'arunna'),
(437, 411, 'Red', 'red', '', 'variant', 'product', 0, 40, 'arunna'),
(574, 493, '80 x 80', '80-x-80', '', 'variant', 'product', 0, 7, 'arunna'),
(500, 498, 'Silver and Black lip shell', 'silver-and-black-lip-shell', '', 'variant', 'product', 0, 13, 'arunna'),
(498, 0, 'Material', 'material', '', 'variant', 'product', 0, 13, 'arunna'),
(504, 0, 'Funiture', 'funiture-2', 'Funiture', 'categories', 'products', 5, 8, 'arunna'),
(505, 0, 'Ceramics', 'ceramics-2', 'Ceramics', 'categories', 'products', 7, 7, 'arunna'),
(506, 0, 'Paintings', 'paintings', 'Paintings', 'categories', 'products', 2, 9, 'arunna'),
(571, 493, 'h100 x 28m', 'h100-x-28m', '', 'variant', 'product', 0, 7, 'arunna'),
(570, 493, 'h120 x 03l', 'h120-x-03l', '', 'variant', 'product', 0, 7, 'arunna'),
(587, 493, 'h40', '', '', 'variant', 'product', 0, 7, 'arunna'),
(588, 493, '150 x 50', '150-x-50', '', 'variant', 'product', 0, 7, 'arunna'),
(567, 493, '100 x 35 x 30', '100-x-35-x-30', '', 'variant', 'product', 0, 7, 'arunna'),
(566, 493, 'h150 x 40', 'h150-x-40', '', 'variant', 'product', 0, 7, 'arunna'),
(565, 493, 'h120 x 80', 'h120-x-80', '', 'variant', 'product', 0, 7, 'arunna'),
(590, 493, '125 x 125', '125-x-125', '', 'variant', 'product', 0, 7, 'arunna'),
(591, 493, '2MTR x 60', '2mtr-x-60', '', 'variant', 'product', 0, 7, 'arunna'),
(589, 493, 'H80 W120 D40', 'h80-w120-d40', '', 'variant', 'product', 0, 7, 'arunna');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_rule_relationship`
-- 

CREATE TABLE `lumonata_rule_relationship` (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`lapp_id`,`lrule_id`),
  KEY `taxonomy_id` (`lrule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_rule_relationship`
-- 

INSERT INTO `lumonata_rule_relationship` (`lapp_id`, `lrule_id`, `lorder_id`) VALUES 
(3, 1, 0),
(106, 1, 0),
(1, 5, 0),
(93, 505, 0),
(107, 1, 0),
(20, 1, 0),
(92, 505, 0),
(103, 504, 0),
(102, 504, 0),
(108, 1, 0),
(95, 505, 0),
(104, 504, 0),
(94, 505, 0),
(97, 505, 0),
(98, 506, 0),
(99, 506, 0),
(126, 505, 0),
(100, 504, 0),
(101, 504, 0),
(155, 505, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_shipping`
-- 

CREATE TABLE `lumonata_shipping` (
  `lshipping_id` int(11) NOT NULL auto_increment,
  `lcountry_id` int(11) NOT NULL,
  `lshipping_type` varchar(60) NOT NULL,
  `lformula` decimal(10,0) NOT NULL,
  `unit_symbol` varchar(50) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lshipping_tax` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lshipping_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

-- 
-- Dumping data for table `lumonata_shipping`
-- 

INSERT INTO `lumonata_shipping` (`lshipping_id`, `lcountry_id`, `lshipping_type`, `lformula`, `unit_symbol`, `lcurrency_id`, `lprice`, `lshipping_tax`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
(44, 1, 'weight', 2, 'Kilograms\n\n', 6, 3.00, 0.00, 18, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(43, 1, 'weight', 1, 'Pounds\n\n', 6, 3.00, 0.00, 19, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(45, 2, 'price', 0, 'USD', 6, 3.00, 0.00, 17, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(46, 2, 'price', 12, 'USD', 6, 4.00, 0.00, 16, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(47, 1, 'weight', 10, 'Kilograms\n\n', 6, 50.00, 0.00, 15, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(48, 7, 'weight', 10, 'Kilograms\n\n', 6, 50.00, 2.00, 14, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(51, 247, 'price', 0, 'USD\n\n', 6, 1.00, 1.00, 11, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(52, 247, 'price', 10, 'USD\n\n', 6, 2.00, 2.00, 10, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(53, 247, 'price', 20, 'USD\n\n', 6, 4.00, 3.00, 9, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(54, 2, 'price', 20, 'USD', 6, 10.00, 0.00, 8, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(55, 247, 'price', 60, 'USD', 6, 9.00, 5.00, 7, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(59, 103, 'price', 10, 'USD', 6, 1.00, 2.00, 3, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1),
(62, 103, 'price', 11, 'USD', 6, 2.00, 3.00, 0, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_shipping_detail`
-- 

CREATE TABLE `lumonata_shipping_detail` (
  `lshipping_detail_id` int(11) NOT NULL auto_increment,
  `lshipping_id` int(11) NOT NULL,
  `lshipping_detail_variant_id` int(11) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lshipping_detail_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=333 ;

-- 
-- Dumping data for table `lumonata_shipping_detail`
-- 

INSERT INTO `lumonata_shipping_detail` (`lshipping_detail_id`, `lshipping_id`, `lshipping_detail_variant_id`, `lcurrency_id`, `lprice`, `lorder_id`, `llang_id`) VALUES 
(302, 0, 1, 7, 50.00, 34, 15),
(301, 0, 2, 7, 100.00, 35, 15),
(326, 5, 1, 11, 15.00, 10, 1),
(287, 0, 1, 11, 50.00, 49, 1),
(329, 2, 1, 11, 15.00, 7, 1),
(332, 26, 1, 11, 50.00, 2, 1),
(300, 0, 3, 0, 0.00, 36, 15),
(296, 0, 1, 6, 50.00, 40, 3),
(295, 0, 2, 6, 100.00, 41, 3),
(294, 0, 3, 0, 0.00, 42, 3),
(299, 0, 1, 7, 50.00, 37, 5),
(298, 0, 2, 7, 100.00, 38, 5),
(297, 0, 3, 7, 0.00, 39, 5),
(286, 0, 2, 11, 100.00, 50, 1),
(285, 0, 3, 11, 0.00, 51, 1),
(327, 4, 1, 11, 15.00, 9, 1),
(328, 3, 1, 11, 15.00, 8, 1),
(290, 0, 1, 9, 50.00, 46, 17),
(289, 0, 2, 9, 100.00, 47, 17),
(288, 0, 3, 0, 0.00, 48, 17),
(293, 0, 1, 12, 50.00, 43, 16),
(292, 0, 2, 12, 100.00, 44, 16),
(291, 0, 3, 0, 0.00, 45, 16),
(323, 8, 1, 11, 40.00, 13, 1),
(324, 7, 1, 11, 15.00, 12, 1),
(325, 6, 1, 11, 30.00, 11, 1),
(322, 9, 1, 11, 15.00, 14, 1),
(321, 10, 1, 11, 15.00, 15, 1),
(320, 11, 1, 11, 15.00, 16, 1),
(319, 12, 1, 11, 30.00, 17, 1),
(318, 13, 1, 11, 40.00, 18, 1),
(317, 14, 1, 11, 15.00, 19, 1),
(316, 15, 1, 11, 15.00, 20, 1),
(315, 16, 1, 11, 15.00, 21, 1),
(314, 17, 1, 11, 15.00, 22, 1),
(313, 18, 1, 11, 45.00, 23, 1),
(312, 19, 1, 11, 45.00, 24, 1),
(311, 20, 1, 11, 45.00, 25, 1),
(310, 21, 1, 11, 15.00, 26, 1),
(309, 22, 1, 11, 60.00, 27, 1),
(308, 23, 1, 11, 25.00, 28, 1),
(307, 24, 1, 11, 15.00, 29, 1),
(306, 25, 1, 11, 15.00, 30, 1),
(331, 26, 2, 11, 100.00, 3, 1),
(330, 26, 3, 11, 0.00, 4, 1),
(326, 5, 1, 11, 15.00, 10, 3),
(287, 0, 1, 11, 50.00, 49, 3),
(329, 2, 1, 11, 15.00, 7, 3),
(332, 26, 1, 11, 50.00, 2, 3),
(286, 0, 2, 11, 100.00, 50, 3),
(285, 0, 3, 11, 0.00, 51, 3),
(327, 4, 1, 11, 15.00, 9, 3),
(328, 3, 1, 11, 15.00, 8, 3),
(323, 8, 1, 11, 40.00, 13, 3),
(324, 7, 1, 11, 15.00, 12, 3),
(325, 6, 1, 11, 30.00, 11, 3),
(322, 9, 1, 11, 15.00, 14, 3),
(321, 10, 1, 11, 15.00, 15, 3),
(320, 11, 1, 11, 15.00, 16, 3),
(319, 12, 1, 11, 30.00, 17, 3),
(318, 13, 1, 11, 40.00, 18, 3),
(317, 14, 1, 11, 15.00, 19, 3),
(316, 15, 1, 11, 15.00, 20, 3),
(315, 16, 1, 11, 15.00, 21, 3),
(314, 17, 1, 11, 15.00, 22, 3),
(313, 18, 1, 11, 45.00, 23, 3),
(312, 19, 1, 11, 45.00, 24, 3),
(311, 20, 1, 11, 45.00, 25, 3),
(310, 21, 1, 11, 15.00, 26, 3),
(309, 22, 1, 11, 60.00, 27, 3),
(308, 23, 1, 11, 25.00, 28, 3),
(307, 24, 1, 11, 15.00, 29, 3),
(306, 25, 1, 11, 15.00, 30, 3),
(331, 26, 2, 11, 100.00, 3, 3),
(330, 26, 3, 11, 0.00, 4, 3),
(326, 5, 1, 7, 15.00, 10, 5),
(287, 0, 1, 7, 50.00, 49, 5),
(329, 2, 1, 7, 15.00, 7, 5),
(332, 26, 1, 7, 50.00, 2, 5),
(286, 0, 2, 7, 100.00, 50, 5),
(285, 0, 3, 7, 0.00, 51, 5),
(327, 4, 1, 7, 15.00, 9, 5),
(328, 3, 1, 7, 15.00, 8, 5),
(323, 8, 1, 7, 40.00, 13, 5),
(324, 7, 1, 7, 15.00, 12, 5),
(325, 6, 1, 7, 30.00, 11, 5),
(322, 9, 1, 7, 15.00, 14, 5),
(321, 10, 1, 7, 15.00, 15, 5),
(320, 11, 1, 7, 15.00, 16, 5),
(319, 12, 1, 7, 30.00, 17, 5),
(318, 13, 1, 7, 40.00, 18, 5),
(317, 14, 1, 7, 15.00, 19, 5),
(316, 15, 1, 7, 15.00, 20, 5),
(315, 16, 1, 7, 15.00, 21, 5),
(314, 17, 1, 7, 15.00, 22, 5),
(313, 18, 1, 7, 45.00, 23, 5),
(312, 19, 1, 7, 45.00, 24, 5),
(311, 20, 1, 7, 45.00, 25, 5),
(310, 21, 1, 7, 15.00, 26, 5),
(309, 22, 1, 7, 60.00, 27, 5),
(308, 23, 1, 7, 25.00, 28, 5),
(307, 24, 1, 7, 15.00, 29, 5),
(306, 25, 1, 7, 15.00, 30, 5),
(331, 26, 2, 7, 100.00, 3, 5),
(330, 26, 3, 7, 0.00, 4, 5),
(326, 5, 1, 11, 15.00, 10, 15),
(287, 0, 1, 11, 50.00, 49, 15),
(329, 2, 1, 11, 15.00, 7, 15),
(332, 26, 1, 11, 50.00, 2, 15),
(286, 0, 2, 11, 100.00, 50, 15),
(285, 0, 3, 11, 0.00, 51, 15),
(327, 4, 1, 11, 15.00, 9, 15),
(328, 3, 1, 11, 15.00, 8, 15),
(323, 8, 1, 11, 40.00, 13, 15),
(324, 7, 1, 11, 15.00, 12, 15),
(325, 6, 1, 11, 30.00, 11, 15),
(322, 9, 1, 11, 15.00, 14, 15),
(321, 10, 1, 11, 15.00, 15, 15),
(320, 11, 1, 11, 15.00, 16, 15),
(319, 12, 1, 11, 30.00, 17, 15),
(318, 13, 1, 11, 40.00, 18, 15),
(317, 14, 1, 11, 15.00, 19, 15),
(316, 15, 1, 11, 15.00, 20, 15),
(315, 16, 1, 11, 15.00, 21, 15),
(314, 17, 1, 11, 15.00, 22, 15),
(313, 18, 1, 11, 45.00, 23, 15),
(312, 19, 1, 11, 45.00, 24, 15),
(311, 20, 1, 11, 45.00, 25, 15),
(310, 21, 1, 11, 15.00, 26, 15),
(309, 22, 1, 11, 60.00, 27, 15),
(308, 23, 1, 11, 25.00, 28, 15),
(307, 24, 1, 11, 15.00, 29, 15),
(306, 25, 1, 11, 15.00, 30, 15),
(331, 26, 2, 11, 100.00, 3, 15),
(330, 26, 3, 11, 0.00, 4, 15),
(326, 5, 1, 11, 15.00, 10, 16),
(287, 0, 1, 11, 50.00, 49, 16),
(329, 2, 1, 11, 15.00, 7, 16),
(332, 26, 1, 11, 50.00, 2, 16),
(286, 0, 2, 11, 100.00, 50, 16),
(285, 0, 3, 11, 0.00, 51, 16),
(327, 4, 1, 11, 15.00, 9, 16),
(328, 3, 1, 11, 15.00, 8, 16),
(323, 8, 1, 11, 40.00, 13, 16),
(324, 7, 1, 11, 15.00, 12, 16),
(325, 6, 1, 11, 30.00, 11, 16),
(322, 9, 1, 11, 15.00, 14, 16),
(321, 10, 1, 11, 15.00, 15, 16),
(320, 11, 1, 11, 15.00, 16, 16),
(319, 12, 1, 11, 30.00, 17, 16),
(318, 13, 1, 11, 40.00, 18, 16),
(317, 14, 1, 11, 15.00, 19, 16),
(316, 15, 1, 11, 15.00, 20, 16),
(315, 16, 1, 11, 15.00, 21, 16),
(314, 17, 1, 11, 15.00, 22, 16),
(313, 18, 1, 11, 45.00, 23, 16),
(312, 19, 1, 11, 45.00, 24, 16),
(311, 20, 1, 11, 45.00, 25, 16),
(310, 21, 1, 11, 15.00, 26, 16),
(309, 22, 1, 11, 60.00, 27, 16),
(308, 23, 1, 11, 25.00, 28, 16),
(307, 24, 1, 11, 15.00, 29, 16),
(306, 25, 1, 11, 15.00, 30, 16),
(331, 26, 2, 11, 100.00, 3, 16),
(330, 26, 3, 11, 0.00, 4, 16),
(326, 5, 1, 11, 15.00, 10, 17),
(287, 0, 1, 11, 50.00, 49, 17),
(329, 2, 1, 11, 15.00, 7, 17),
(332, 26, 1, 11, 50.00, 2, 17),
(286, 0, 2, 11, 100.00, 50, 17),
(285, 0, 3, 11, 0.00, 51, 17),
(327, 4, 1, 11, 15.00, 9, 17),
(328, 3, 1, 11, 15.00, 8, 17),
(323, 8, 1, 11, 40.00, 13, 17),
(324, 7, 1, 11, 15.00, 12, 17),
(325, 6, 1, 11, 30.00, 11, 17),
(322, 9, 1, 11, 15.00, 14, 17),
(321, 10, 1, 11, 15.00, 15, 17),
(320, 11, 1, 11, 15.00, 16, 17),
(319, 12, 1, 11, 30.00, 17, 17),
(318, 13, 1, 11, 40.00, 18, 17),
(317, 14, 1, 11, 15.00, 19, 17),
(316, 15, 1, 11, 15.00, 20, 17),
(315, 16, 1, 11, 15.00, 21, 17),
(314, 17, 1, 11, 15.00, 22, 17),
(313, 18, 1, 11, 45.00, 23, 17),
(312, 19, 1, 11, 45.00, 24, 17),
(311, 20, 1, 11, 45.00, 25, 17),
(310, 21, 1, 11, 15.00, 26, 17),
(309, 22, 1, 11, 60.00, 27, 17),
(308, 23, 1, 11, 25.00, 28, 17),
(307, 24, 1, 11, 15.00, 29, 17),
(306, 25, 1, 11, 15.00, 30, 17),
(331, 26, 2, 11, 100.00, 3, 17),
(330, 26, 3, 11, 0.00, 4, 17);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax`
-- 

CREATE TABLE `lumonata_tax` (
  `ltax_id` int(11) NOT NULL auto_increment,
  `taxes_on_shipping` tinyint(4) NOT NULL COMMENT '1=true,0=false',
  `price_include_taxes` tinyint(4) NOT NULL COMMENT '1=true,0=false',
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`ltax_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `lumonata_tax`
-- 

INSERT INTO `lumonata_tax` (`ltax_id`, `taxes_on_shipping`, `price_include_taxes`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(1, 1, 1, 18, '1', '2011-10-24 14:40:51', '1', '2011-12-09 16:03:57');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax_detail`
-- 

CREATE TABLE `lumonata_tax_detail` (
  `ltax_detail_id` int(11) NOT NULL auto_increment,
  `ltax_id` int(11) NOT NULL,
  `lcountry_id` int(11) NOT NULL,
  `ltax_groups_ID` int(11) NOT NULL,
  `ltax_charge` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  PRIMARY KEY  (`ltax_detail_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=415 ;

-- 
-- Dumping data for table `lumonata_tax_detail`
-- 

INSERT INTO `lumonata_tax_detail` (`ltax_detail_id`, `ltax_id`, `lcountry_id`, `ltax_groups_ID`, `ltax_charge`, `lorder`) VALUES 
(407, 1, 1, 70, 1.00, 7),
(408, 1, 1, 71, 2.00, 6),
(409, 1, 2, 70, 1.00, 5),
(410, 1, 2, 71, 2.00, 4),
(411, 1, 3, 70, 4.00, 3),
(412, 1, 3, 71, 2.00, 2),
(413, 1, 247, 70, 1.00, 1),
(414, 1, 247, 71, 2.00, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax_groups`
-- 

CREATE TABLE `lumonata_tax_groups` (
  `ltax_groups_ID` int(12) NOT NULL auto_increment,
  `ldescription` text NOT NULL,
  `ldefault` int(1) NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` varchar(100) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`ltax_groups_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

-- 
-- Dumping data for table `lumonata_tax_groups`
-- 

INSERT INTO `lumonata_tax_groups` (`ltax_groups_ID`, `ldescription`, `ldefault`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`) VALUES 
(71, 'Goverment Rate', 0, 21, '1', '2011-10-21 15:54:23', '1', '2011-10-21 15:54:23'),
(70, 'default', 1, 22, '1', '2011-10-21 14:46:43', '1', '2011-10-21 14:46:43');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_users`
-- 

CREATE TABLE `lumonata_users` (
  `luser_id` bigint(20) NOT NULL auto_increment,
  `lusername` varchar(200) character set utf8 NOT NULL,
  `ldisplay_name` varchar(200) character set utf8 NOT NULL,
  `lpassword` varchar(200) character set utf8 NOT NULL,
  `lemail` varchar(200) character set utf8 NOT NULL,
  `lregistration_date` datetime NOT NULL,
  `luser_type` varchar(50) character set utf8 NOT NULL,
  `lactivation_key` varchar(200) character set utf8 NOT NULL,
  `lavatar` varchar(200) character set utf8 NOT NULL,
  `lsex` int(11) NOT NULL COMMENT '1=male,2=female',
  `lbirthday` date NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`luser_id`),
  KEY `username` (`lusername`),
  KEY `display_name` (`ldisplay_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `lumonata_users`
-- 

INSERT INTO `lumonata_users` (`luser_id`, `lusername`, `ldisplay_name`, `lpassword`, `lemail`, `lregistration_date`, `luser_type`, `lactivation_key`, `lavatar`, `lsex`, `lbirthday`, `lstatus`, `ldlu`) VALUES 
(1, 'admin', 'angelina', 'fcea920f7412b5da7be0cf42b8c93759', 'request@arunna.com', '0000-00-00 00:00:00', 'administrator', '', 'admin-1.jpg|admin-2.jpg|admin-3.jpg', 2, '2011-03-19', 1, '2011-09-01 15:14:27');
