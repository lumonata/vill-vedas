-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Oct 26, 2011 at 05:50 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `arunna_training`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_additional_fields`
-- 

CREATE TABLE `lumonata_additional_fields` (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) character set utf8 NOT NULL,
  `lvalue` text character set utf8 NOT NULL,
  `lapp_name` varchar(200) character set utf8 NOT NULL,
  PRIMARY KEY  (`lapp_id`,`lkey`),
  KEY `key` (`lkey`),
  KEY `app_name` (`lapp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_additional_fields`
-- 

INSERT INTO `lumonata_additional_fields` (`lapp_id`, `lkey`, `lvalue`, `lapp_name`) VALUES 
(2, 'meta_title', '', 'pages'),
(2, 'meta_description', '', 'pages'),
(2, 'meta_keywords', '', 'pages'),
(3, 'meta_title', '', 'articles'),
(3, 'meta_description', '', 'articles'),
(3, 'meta_keywords', '', 'articles'),
(1, 'first_name', 'angelina', 'user'),
(1, 'last_name', 'jolie', 'user'),
(1, 'website', 'http://', 'user'),
(1, 'bio', '', 'user'),
(5, 'meta_title', 'Contact', 'pages'),
(5, 'meta_description', 'description here', 'pages'),
(5, 'meta_keywords', 'contact', 'pages'),
(6, 'meta_title', '', 'articles'),
(6, 'meta_description', '', 'articles'),
(6, 'meta_keywords', '', 'articles'),
(2, 'invite_limit', '10', 'user'),
(2, 'first_name', 'contributor', 'user'),
(2, 'last_name', 'user', 'user'),
(2, 'website', 'http://nolink.com', 'user'),
(1, 'one_liner', '', 'user'),
(1, 'location', '', 'user'),
(1, 'invite_limit', '-1', 'user'),
(3, 'invite_limit', '10', 'user'),
(3, 'first_name', 'userklien', 'user'),
(3, 'last_name', 're', 'user'),
(7, 'meta_title', '', 'articles'),
(7, 'meta_description', '', 'articles'),
(7, 'meta_keywords', '', 'articles'),
(10, 'meta_title', '', 'articles'),
(10, 'meta_description', '', 'articles'),
(10, 'meta_keywords', '', 'articles'),
(11, 'meta_title', '', 'pages'),
(11, 'meta_description', '', 'pages'),
(11, 'meta_keywords', '', 'pages'),
(15, 'meta_title', '', 'pages'),
(15, 'meta_description', '', 'pages'),
(15, 'meta_keywords', '', 'pages'),
(16, 'meta_title', '', 'pages'),
(16, 'meta_description', '', 'pages'),
(16, 'meta_keywords', '', 'pages'),
(76, 'product_image_thumbnail', '184', 'products'),
(76, 'product_variant', '{"parent_variant":["445"],"child_variant":[{"447":"1","446":"2"}]}', 'products'),
(76, 'product_additional', '{"price":"1","weight":"2","tax":"71","stock_limit":"2","product_buy_out_of_stock":"0"}', 'products'),
(74, 'product_image_thumbnail', '154', 'products'),
(74, 'product_variant', '{"parent_variant":["445"],"child_variant":[{"447":[""]},{"446":["0.00"]}]}', 'products'),
(74, 'product_additional', '{"price":"12","weight":"4","tax":"70","stock_limit":"10","product_buy_out_of_stock":"0"}', 'products'),
(52, 'product_image_thumbnail', '127', 'products'),
(52, 'product_variant', '{"parent_variant":["445"],"child_variant":[{"446":["0.00"],"447":[""]}]}', 'products'),
(52, 'product_additional', '{"price":"10","weight":"5","tax":"71","stock_limit":"5","product_buy_out_of_stock":"0"}', 'products'),
(73, 'product_additional', '{"price":"12","weight":"4","tax":"71","stock_limit":"2","product_buy_out_of_stock":"1"}', 'products'),
(75, 'product_additional', '{"price":"3","weight":"4","tax":"70","stock_limit":"","product_buy_out_of_stock":"0"}', 'products'),
(37, 'meta_title', '', 'articles'),
(37, 'meta_description', '', 'articles'),
(37, 'meta_keywords', '', 'articles'),
(73, 'product_image_thumbnail', '153', 'products'),
(73, 'product_variant', '{"parent_variant":["43","445"],"child_variant":{"0":{"442":{"1":""},"447":{"1":"0.00"}},"1":{"441":{"1":"0.00"},"446":{"1":"0.00"}},"2":{"440":{"1":"0.00"}},"5":{"447":{"1":"0.00"}},"6":{"446":{"1":"0.00"}}}}', 'products'),
(75, 'product_image_thumbnail', '155', 'products'),
(75, 'product_variant', '{"parent_variant":["411","445"],"child_variant":[{"439":"3","438":"4","437":"5"},{"447":"4"}]}', 'products');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_articles`
-- 

CREATE TABLE `lumonata_articles` (
  `larticle_id` bigint(20) NOT NULL auto_increment,
  `larticle_title` text character set utf8 NOT NULL,
  `larticle_brief` text character set utf8 NOT NULL,
  `larticle_content` longtext character set utf8 NOT NULL,
  `larticle_status` varchar(20) character set utf8 NOT NULL,
  `larticle_type` varchar(20) character set utf8 NOT NULL,
  `lcomment_status` varchar(20) character set utf8 NOT NULL,
  `lcomment_count` bigint(20) NOT NULL,
  `lcount_like` bigint(20) NOT NULL,
  `lsef` text character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL default '1',
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY  (`larticle_id`),
  KEY `article_title` (`larticle_title`(255)),
  KEY `type_status_date_by` (`larticle_type`,`larticle_status`,`lpost_date`,`lpost_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=77 ;

-- 
-- Dumping data for table `lumonata_articles`
-- 

INSERT INTO `lumonata_articles` (`larticle_id`, `larticle_title`, `larticle_brief`, `larticle_content`, `larticle_status`, `larticle_type`, `lcomment_status`, `lcomment_count`, `lcount_like`, `lsef`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`, `lshare_to`) VALUES 
(1, 'My First Status', '', '', 'publish', 'status', 'allowed', 0, 0, 'my-first-status', 76, 1, '2011-03-18 21:46:22', 1, '2011-03-18 21:46:22', 0),
(2, 'Register', '', '<p>Arunna is a solution that creates a place where social media, networking  and community collaborate. You and your customers can have the option  to connect and share content online. Our platform gives the opportunity  for both hosted community and white label self hosted sites to take the  advantage to express themselves. With arunna, small organization to  large enterprises can build brand awareness, engage their community and  much more.</p>\r\n<p>That''s what we call connecting the clouds...</p>', 'publish', 'pages', 'allowed', 0, 0, 'register-user', 63, 1, '2011-03-18 21:50:33', 1, '2011-10-03 03:32:57', 0),
(3, 'Welcome to Arunna', '', '<p>Hello Friends,</p>\r\n<p>Let''s share to make a better world</p>', 'publish', 'articles', 'allowed', 2, 1, 'welcome-arunna-training', 74, 1, '2011-03-18 21:51:45', 1, '2011-09-01 10:14:14', 0),
(4, 'Halloo...', '', '', 'publish', 'status', 'allowed', 0, 0, 'halloo', 73, 1, '2011-09-01 10:07:16', 1, '2011-09-01 10:07:16', 1),
(5, 'Contact Us', '', '<p>If you have any problem please contact us Below!</p>\r\n<p>Phone: xxxxxxxxxxxxx</p>\r\n<p>address: xxxxxxxxxxxxx</p>', 'publish', 'pages', 'allowed', 0, 0, 'contact-us', 73, 1, '2011-09-01 10:11:09', 1, '2011-09-01 02:11:37', 0),
(6, 'New Article', '', '<p>This is new article just for testing.. :)</p>\r\n<p>thx for visiting..hhhh</p>', 'publish', 'articles', 'allowed', 0, 0, 'new-article', 71, 1, '2011-09-01 10:17:31', 1, '2011-10-11 11:34:39', 0),
(10, 'this is it', '', '', 'draft', 'articles', 'allowed', 0, 0, 'qqq', 67, 1, '2011-09-06 12:06:54', 1, '2011-10-11 11:20:20', 0),
(52, 'CPLQXA - SPACE WAVE POLO - WHITE', '', '<p>CPLQXA - SPACE WAVE POLO - WHITE DESCRIBE</p>', 'publish', 'products', '', 0, 0, 'cplqxa-space-wave-polo-white', 25, 1, '2011-10-13 14:05:52', 1, '2011-10-26 08:48:40', 0),
(37, 'Untitled', '', '', 'draft', 'articles', 'allowed', 0, 0, 'untitled', 40, 1, '2011-10-10 15:19:01', 1, '2011-10-11 11:20:20', 0),
(73, 'CSHQXB - MEZEL SHIRT - DIRTY WHITE', '', '<p>CSHQXB - MEZEL SHIRT - DIRTY WHITE Description</p>', 'draft', 'products', '', 0, 0, 'cshqxb-mezel-shirt-dirty-white', 4, 1, '2011-10-25 11:55:47', 1, '2011-10-26 09:59:42', 0),
(74, 'CSHQXB - MEZEL SHIRT - BLACK', '', '<p>CSHQXB - MEZEL SHIRT - BLACK DESCRIPTION</p>', 'draft', 'products', '', 0, 0, 'cshqxb-mezel-shirt-black', 3, 1, '2011-10-25 11:58:47', 1, '2011-10-26 09:59:42', 0),
(75, 'CPAQAC - DERICK PANT - FATIGUE', '', '<p>CPAQAC - DERICK PANT - FATIGUE DESCRIPTION</p>', 'draft', 'products', '', 0, 0, 'cpaqac-derick-pant-fatigue', 2, 1, '2011-10-25 12:00:51', 1, '2011-10-26 13:16:37', 0),
(76, 'BTRQAB - GLOBAL F-LIGHT TRAVEL - BLACK', '', '<p>BTRQAB - GLOBAL F-LIGHT TRAVEL - BLACK DESC</p>', 'publish', 'products', '', 0, 0, 'btrqab-global-f-light-travel-black', 1, 1, '2011-10-26 10:10:59', 1, '2011-10-26 13:16:47', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_attachment`
-- 

CREATE TABLE `lumonata_attachment` (
  `lattach_id` bigint(20) NOT NULL auto_increment,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text character set utf8 NOT NULL,
  `lattach_loc_thumb` text character set utf8 NOT NULL,
  `lattach_loc_medium` text character set utf8 NOT NULL,
  `lattach_loc_large` text character set utf8 NOT NULL,
  `ltitle` varchar(200) character set utf8 NOT NULL,
  `lalt_text` text character set utf8 NOT NULL,
  `lcaption` varchar(200) character set utf8 NOT NULL,
  `mime_type` varchar(50) character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL default '0',
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY  (`lattach_id`),
  KEY `article_id` (`larticle_id`),
  KEY `attachment_title` (`ltitle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=185 ;

-- 
-- Dumping data for table `lumonata_attachment`
-- 

INSERT INTO `lumonata_attachment` (`lattach_id`, `larticle_id`, `lattach_loc`, `lattach_loc_thumb`, `lattach_loc_medium`, `lattach_loc_large`, `ltitle`, `lalt_text`, `lcaption`, `mime_type`, `lorder`, `upload_date`, `date_last_update`) VALUES 
(4, 1316070102, '/lumonata-content/files/201109/nobita4.jpg', '/lumonata-content/files/201109/nobita4-thumbnail.jpg', '/lumonata-content/files/201109/nobita4-medium.jpg', '/lumonata-content/files/201109/nobita4-large.jpg', 'billabong', '', '', 'image/jpeg', 3, '2011-09-15 07:04:03', '2011-09-15 07:04:23'),
(3, 6, '/lumonata-content/files/201109/detail07.jpg', '/lumonata-content/files/201109/detail07-thumbnail.jpg', '/lumonata-content/files/201109/detail07-medium.jpg', '/lumonata-content/files/201109/detail07-large.jpg', 'detail07', '', '', 'image/jpeg', 2, '2011-09-01 05:59:40', '2011-09-07 07:37:33'),
(140, 1318556130, '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '69097b08d263d98b6ad183403910ddaf-1318556950.jpg', '', '', 'image/jpeg', 1, '2011-10-14 09:49:10', '0000-00-00 00:00:00'),
(154, 74, '/lumonata-plugins/shopping_cart/uploads/product/f7550b2d929d3e18d2980c1a4e2ab3c4-1319515113.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/f7550b2d929d3e18d2980c1a4e2ab3c4-1319515113.jpg', '/lumonata-plugins/shopping_cart/uploads/product/f7550b2d929d3e18d2980c1a4e2ab3c4-1319515113.jpg', '/lumonata-plugins/shopping_cart/uploads/product/f7550b2d929d3e18d2980c1a4e2ab3c4-1319515113.jpg', 'f7550b2d929d3e18d2980c1a4e2ab3c4-1319515113.jpg', '', '', 'image/jpeg', 1, '2011-10-25 11:58:33', '0000-00-00 00:00:00'),
(155, 75, '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1319515248.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/69097b08d263d98b6ad183403910ddaf-1319515248.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1319515248.jpg', '/lumonata-plugins/shopping_cart/uploads/product/69097b08d263d98b6ad183403910ddaf-1319515248.jpg', '69097b08d263d98b6ad183403910ddaf-1319515248.jpg', '', '', 'image/jpeg', 1, '2011-10-25 12:00:48', '0000-00-00 00:00:00'),
(131, 59, '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '/lumonata-plugins/shopping_cart/uploads/product/85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '85c4c19f9abefbc553bec3e0d9ee34c9-1318493665.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:14:25', '0000-00-00 00:00:00'),
(130, 59, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '58a1e51b1cc281df09785bfe179389b6-1318493662.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:14:22', '0000-00-00 00:00:00'),
(129, 58, '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '/lumonata-plugins/shopping_cart/uploads/product/766519b74a562c473afa9fce860443c6-1318493421.jpg', '766519b74a562c473afa9fce860443c6-1318493421.jpg', '', '', 'image/jpeg', 1, '2011-10-13 16:10:21', '0000-00-00 00:00:00'),
(137, 1318555717, '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '/lumonata-plugins/shopping_cart/uploads/product/ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', 'ba55e3cc551a9dfa22d4030e9141927a-1318555749.jpg', '', '', 'image/jpeg', 1, '2011-10-14 09:29:09', '0000-00-00 00:00:00'),
(127, 52, '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1318485702.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/9bddd549f241fb9fd3f64a0d7a0a7869-1318485702.jpg', '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1318485702.jpg', '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1318485702.jpg', '9bddd549f241fb9fd3f64a0d7a0a7869-1318485702.jpg', '', '', 'image/jpeg', 1, '2011-10-13 14:01:42', '0000-00-00 00:00:00'),
(153, 73, '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1319514932.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/0c4304dc08e12046253238703be407ec-1319514932.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1319514932.jpg', '/lumonata-plugins/shopping_cart/uploads/product/0c4304dc08e12046253238703be407ec-1319514932.jpg', '0c4304dc08e12046253238703be407ec-1319514932.jpg', '', '', 'image/jpeg', 1, '2011-10-25 11:55:33', '0000-00-00 00:00:00'),
(175, 1319593685, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '58a1e51b1cc281df09785bfe179389b6-1319593691.jpg', '', '', 'image/jpeg', 1, '2011-10-26 09:48:11', '0000-00-00 00:00:00'),
(178, 1319594171, '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '/lumonata-plugins/shopping_cart/uploads/product/58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '58a1e51b1cc281df09785bfe179389b6-1319594182.jpg', '', '', 'image/jpeg', 1, '2011-10-26 09:56:22', '0000-00-00 00:00:00'),
(183, 73, '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1319594379.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/9bddd549f241fb9fd3f64a0d7a0a7869-1319594379.jpg', '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1319594379.jpg', '/lumonata-plugins/shopping_cart/uploads/product/9bddd549f241fb9fd3f64a0d7a0a7869-1319594379.jpg', '9bddd549f241fb9fd3f64a0d7a0a7869-1319594379.jpg', '', '', 'image/jpeg', 1, '2011-10-26 09:59:39', '0000-00-00 00:00:00'),
(184, 76, '/lumonata-plugins/shopping_cart/uploads/product/f0f7c88168f0289ab8f0686a7dde66d6-1319595057.jpg', '/lumonata-plugins/shopping_cart/uploads/product/thumbnail/f0f7c88168f0289ab8f0686a7dde66d6-1319595057.jpg', '/lumonata-plugins/shopping_cart/uploads/product/f0f7c88168f0289ab8f0686a7dde66d6-1319595057.jpg', '/lumonata-plugins/shopping_cart/uploads/product/f0f7c88168f0289ab8f0686a7dde66d6-1319595057.jpg', 'f0f7c88168f0289ab8f0686a7dde66d6-1319595057.jpg', '', '', 'image/jpeg', 1, '2011-10-26 10:10:57', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_comments`
-- 

CREATE TABLE `lumonata_comments` (
  `lcomment_id` bigint(20) NOT NULL auto_increment,
  `lcomment_parent` bigint(20) NOT NULL,
  `larticle_id` bigint(20) NOT NULL,
  `lcomentator_name` varchar(200) character set utf8 NOT NULL,
  `lcomentator_email` varchar(100) character set utf8 NOT NULL,
  `lcomentator_url` varchar(200) character set utf8 NOT NULL,
  `lcomentator_ip` varchar(100) character set utf8 NOT NULL,
  `lcomment_date` datetime NOT NULL,
  `lcomment` text character set utf8 NOT NULL,
  `lcomment_status` varchar(20) character set utf8 NOT NULL,
  `lcomment_like` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) character set utf8 NOT NULL COMMENT 'like,comment,like_comment',
  PRIMARY KEY  (`lcomment_id`),
  KEY `lcomment_status` (`lcomment_status`),
  KEY `lcomment_userid` (`luser_id`),
  KEY `lcomment_type` (`lcomment_type`),
  KEY `larticle_id` (`larticle_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `lumonata_comments`
-- 

INSERT INTO `lumonata_comments` (`lcomment_id`, `lcomment_parent`, `larticle_id`, `lcomentator_name`, `lcomentator_email`, `lcomentator_url`, `lcomentator_ip`, `lcomment_date`, `lcomment`, `lcomment_status`, `lcomment_like`, `luser_id`, `lcomment_type`) VALUES 
(1, 0, 3, 'Wahya Biantara', 'request@arunna.com', 'http://localhost/arunna-repo/?user=admin', '127.0.0.1', '2011-03-18 21:52:25', 'Hello comment....', 'approved', 0, 1, 'comment'),
(2, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://localhost/arunna_training/user/admin/', '127.0.0.1', '2011-09-01 11:36:28', 'hyh', 'approved', 0, 1, 'comment'),
(3, 0, 3, 'Raden Yudistira', 'request@arunna.com', 'http://localhost/arunna_training/user/admin/', '127.0.0.1', '2011-09-01 11:38:51', 'like_post_3', 'approved', 0, 1, 'like');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_contact_us`
-- 

CREATE TABLE `lumonata_contact_us` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) collate latin1_general_ci NOT NULL,
  `email` varchar(60) collate latin1_general_ci NOT NULL,
  `message` text collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `lumonata_contact_us`
-- 

INSERT INTO `lumonata_contact_us` (`id`, `name`, `email`, `message`) VALUES 
(6, 'widia', 'widia@lumonata.com', 'testestestest'),
(5, 'widia', 'widia@lumonata.com', 'testestestest'),
(7, 'Input your name here', 'And email address please', 'Send us a couple of words'),
(8, 'Input your name here', 'And email address please', 'Send us a couple of words');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_country`
-- 

CREATE TABLE `lumonata_country` (
  `lcountry_id` int(11) NOT NULL auto_increment,
  `lcountry` varchar(255) collate latin1_general_ci NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) collate latin1_general_ci NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) collate latin1_general_ci NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY  (`lcountry_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=248 ;

-- 
-- Dumping data for table `lumonata_country`
-- 

INSERT INTO `lumonata_country` (`lcountry_id`, `lcountry`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(1, 'Ã…land Islands', 246, 'admin', 1241502068, '', 0),
(2, 'Afghanistan', 245, 'admin', 1241502081, '', 0),
(3, 'Albania', 244, 'admin', 1241502086, '', 0),
(4, 'Algeria', 243, 'admin', 1241502092, '', 0),
(5, 'American Samoa', 242, 'admin', 1241502100, '', 0),
(6, 'Andorra', 241, 'admin', 1241502105, '', 0),
(7, 'Angola', 240, 'admin', 1241502114, '', 0),
(8, 'Anguilla', 239, 'admin', 1241502121, '', 0),
(9, 'Antarctica', 238, 'admin', 1241502127, '', 0),
(10, 'Antigua And Barbuda', 237, 'admin', 1241502135, '', 0),
(11, 'Argentina', 236, 'admin', 1241502142, '', 0),
(12, 'Armenia', 235, 'admin', 1241502149, '', 0),
(13, 'Aruba', 234, 'admin', 1241502162, '', 0),
(14, 'Australia', 233, 'admin', 1241502166, '', 0),
(15, 'Austria', 232, 'admin', 1241502174, '', 0),
(16, 'Azerbaijan', 231, 'admin', 1241502182, '', 0),
(17, 'Bahamas', 230, 'admin', 1241502191, '', 0),
(18, 'Bahrain', 229, 'admin', 1241502197, '', 0),
(19, 'Bangladesh', 228, 'admin', 1241502203, '', 0),
(20, 'Barbados', 227, 'admin', 1241502209, '', 0),
(21, 'Belarus', 226, 'admin', 1241502216, '', 0),
(22, 'Belgium', 225, 'admin', 1241502222, '', 0),
(23, 'Belize', 224, 'admin', 1241502228, '', 0),
(24, 'Benin', 223, 'admin', 1241502233, '', 0),
(25, 'Bermuda', 222, 'admin', 1241502239, '', 0),
(26, 'Bhutan', 221, 'admin', 1241502247, '', 0),
(27, 'Bolivia', 220, 'admin', 1241502253, '', 0),
(28, 'Bosnia and Herzegovina', 219, 'admin', 1241502263, '', 0),
(29, 'Botswana', 218, 'admin', 1241502272, '', 0),
(30, 'Bouvet Island', 217, 'admin', 1241502281, '', 0),
(31, 'Brazil', 216, 'admin', 1241502289, '', 0),
(32, 'British Indian Ocean Territory', 215, 'admin', 1241502300, '', 0),
(33, 'Brunei', 214, 'admin', 1241502306, '', 0),
(34, 'Bulgaria', 213, 'admin', 1241502313, '', 0),
(35, 'Burkina Faso', 212, 'admin', 1241502321, '', 0),
(36, 'Burundi', 211, 'admin', 1241502329, '', 0),
(37, 'Cambodia', 210, 'admin', 1241502335, '', 0),
(38, 'Cameroon', 209, 'admin', 1241502341, '', 0),
(39, 'Canada', 208, 'admin', 1241502347, '', 0),
(40, 'Cape Verde', 207, 'admin', 1241502355, '', 0),
(41, 'Cayman Islands', 206, 'admin', 1241502363, '', 0),
(42, 'Central African Republic', 205, 'admin', 1241502371, '', 0),
(43, 'Chad', 204, 'admin', 1241502377, '', 0),
(44, 'Chile', 203, 'admin', 1241502384, '', 0),
(45, 'China', 202, 'admin', 1241502390, '', 0),
(46, 'Christmas Island', 201, 'admin', 1241502398, '', 0),
(47, 'Cocos (Keeling) Islands', 200, 'admin', 1241502411, '', 0),
(48, 'Colombia', 199, 'admin', 1241502465, '', 0),
(49, 'Comoros', 198, 'admin', 1241502499, '', 0),
(50, 'Congo', 197, 'admin', 1241502505, '', 0),
(51, 'Congo, Democractic Republic', 196, 'admin', 1241502513, '', 0),
(52, 'Cook Islands', 195, 'admin', 1241502521, '', 0),
(53, 'Costa Rica', 194, 'admin', 1241502531, '', 0),
(54, 'Cote D\\''Ivoire (Ivory Coast)', 193, 'admin', 1241502550, '', 0),
(55, 'Croatia (Hrvatska)', 192, 'admin', 1241502560, '', 0),
(56, 'Cuba', 191, 'admin', 1241502567, '', 0),
(57, 'Cyprus', 190, 'admin', 1241502573, '', 0),
(58, 'Czech Republic', 189, 'admin', 1241502581, '', 0),
(59, 'Denmark', 188, 'admin', 1241502590, '', 0),
(60, 'Djibouti', 187, 'admin', 1241502596, '', 0),
(61, 'Dominica', 186, 'admin', 1241502602, '', 0),
(62, 'Dominican Republic', 185, 'admin', 1241502610, '', 0),
(63, 'East Timor', 184, 'admin', 1241502619, '', 0),
(64, 'Ecuador', 183, 'admin', 1241502625, '', 0),
(65, 'Egypt', 182, 'admin', 1241502635, '', 0),
(66, 'El Salvador', 181, 'admin', 1241502644, '', 0),
(67, 'Equatorial Guinea', 180, 'admin', 1241502653, '', 0),
(68, 'Eritrea', 179, 'admin', 1241502659, '', 0),
(69, 'Estonia', 178, 'admin', 1241502665, '', 0),
(70, 'Ethiopia', 177, 'admin', 1241502673, '', 0),
(71, 'Falkland Islands (Islas Malvinas)', 176, 'admin', 1241502687, '', 0),
(72, 'Faroe Islands', 175, 'admin', 1241502696, '', 0),
(73, 'Fiji Islands', 174, 'admin', 1241502704, '', 0),
(74, 'Finland', 173, 'admin', 1241502710, '', 0),
(75, 'France', 172, 'admin', 1241502716, '', 0),
(76, 'France, Metropolitan', 171, 'admin', 1241502741, '', 0),
(77, 'French Guiana', 170, 'admin', 1241502745, '', 0),
(78, 'French Polynesia', 169, 'admin', 1241502754, '', 0),
(79, 'French Southern Territories', 168, 'admin', 1241502762, '', 0),
(80, 'Gabon', 167, 'admin', 1241502768, '', 0),
(81, 'Gambia, The', 166, 'admin', 1241502776, '', 0),
(82, 'Georgia', 165, 'admin', 1241502782, '', 0),
(83, 'Germany', 164, 'admin', 1241502789, '', 0),
(84, 'Ghana', 163, 'admin', 1241502799, '', 0),
(85, 'Gibraltar', 162, 'admin', 1241502812, '', 0),
(86, 'Greece', 161, 'admin', 1241502819, '', 0),
(87, 'Greenland', 160, 'admin', 1241502915, '', 0),
(88, 'Grenada', 159, 'admin', 1241502920, '', 0),
(89, 'Guadeloupe', 158, 'admin', 1241502926, '', 0),
(90, 'Guam', 157, 'admin', 1241502931, '', 0),
(91, 'Guatemala', 156, 'admin', 1241502937, '', 0),
(92, 'Guernsey', 155, 'admin', 1241502943, '', 0),
(93, 'Guinea', 154, 'admin', 1241502950, '', 0),
(94, 'Guinea-Bissau', 153, 'admin', 1241502958, '', 0),
(95, 'Guyana', 152, 'admin', 1241502965, '', 0),
(96, 'Haiti', 151, 'admin', 1241502972, '', 0),
(97, 'Heard and McDonald Islands', 150, 'admin', 1241503013, '', 0),
(98, 'Honduras', 149, 'admin', 1241503022, '', 0),
(99, 'Hong Kong S.A.R.', 148, 'admin', 1241503033, '', 0),
(100, 'Hungary', 147, 'admin', 1241503038, '', 0),
(101, 'Iceland', 146, 'admin', 1241503043, '', 0),
(102, 'India', 145, 'admin', 1241503048, '', 0),
(103, 'Indonesia', 144, 'admin', 1241503054, '', 0),
(104, 'Iran', 143, 'admin', 1241503060, '', 0),
(105, 'Iraq', 142, 'admin', 1241503065, '', 0),
(106, 'Ireland', 141, 'admin', 1241503071, '', 0),
(107, 'Isle of Man', 140, 'admin', 1241503078, '', 0),
(108, 'Israel', 139, 'admin', 1241503084, '', 0),
(109, 'Italy', 138, 'admin', 1241503090, '', 0),
(110, 'Jamaica', 137, 'admin', 1241503097, '', 0),
(111, 'Japan', 136, 'admin', 1241503106, '', 0),
(112, 'Jersey', 135, 'admin', 1241503114, '', 0),
(113, 'Jordan', 134, 'admin', 1241503128, '', 0),
(114, 'Kazakhstan', 133, 'admin', 1241503132, '', 0),
(115, 'Kenya', 132, 'admin', 1241503144, '', 0),
(116, 'Kiribati', 131, 'admin', 1241503151, '', 0),
(117, 'Korea', 130, 'admin', 1241503159, '', 0),
(118, 'Korea, North', 129, 'admin', 1241503166, '', 0),
(119, 'Kuwait', 128, 'admin', 1241503172, '', 0),
(120, 'Kyrgyzstan', 127, 'admin', 1241503178, '', 0),
(121, 'Laos', 126, 'admin', 1241503183, '', 0),
(122, 'Latvia', 125, 'admin', 1241503192, '', 0),
(123, 'Lebanon', 124, 'admin', 1241503197, '', 0),
(124, 'Lesotho', 123, 'admin', 1241503203, '', 0),
(125, 'Liberia', 122, 'admin', 1241503210, '', 0),
(126, 'Libya', 121, 'admin', 1241503217, '', 0),
(127, 'Liechtenstein', 120, 'admin', 1241503224, '', 0),
(128, 'Lithuania', 119, 'admin', 1241503229, '', 0),
(129, 'Luxembourg', 118, 'admin', 1241503235, '', 0),
(130, 'Macau S.A.R.', 117, 'admin', 1241503243, '', 0),
(131, 'Macedonia', 116, 'admin', 1241503251, '', 0),
(132, 'Madagascar', 115, 'admin', 1241503256, '', 0),
(133, 'Malawi', 114, 'admin', 1241503263, '', 0),
(134, 'Malaysia', 113, 'admin', 1241503274, '', 0),
(135, 'Maldives', 112, 'admin', 1241503280, '', 0),
(136, 'Mali', 111, 'admin', 1241503286, '', 0),
(137, 'Malta', 110, 'admin', 1241503292, '', 0),
(138, 'Marshall Islands', 109, 'admin', 1241503303, '', 0),
(139, 'Martinique', 108, 'admin', 1241503314, '', 0),
(140, 'Mauritania', 107, 'admin', 1241503321, '', 0),
(141, 'Mauritius', 106, 'admin', 1241503330, '', 0),
(142, 'Mayotte', 105, 'admin', 1241503337, '', 0),
(143, 'Mexico', 104, 'admin', 1241503343, '', 0),
(144, 'Micronesia', 103, 'admin', 1241503350, '', 0),
(145, 'Moldova', 102, 'admin', 1241503359, '', 0),
(146, 'Monaco', 101, 'admin', 1241503367, '', 0),
(147, 'Mongolia', 100, 'admin', 1241503374, '', 0),
(148, 'Montenegro', 99, 'admin', 1241503387, '', 0),
(149, 'Montserrat', 98, 'admin', 1241503395, '', 0),
(150, 'Morocco', 97, 'admin', 1241503419, '', 0),
(151, 'Mozambique', 96, 'admin', 1241503429, '', 0),
(152, 'Myanmar', 95, 'admin', 1241503437, '', 0),
(153, 'Namibia', 94, 'admin', 1241503444, '', 0),
(154, 'Nauru', 93, 'admin', 1241503451, '', 0),
(155, 'Nepal', 92, 'admin', 1241503459, '', 0),
(156, 'Netherlands', 91, 'admin', 1241503466, '', 0),
(157, 'Netherlands Antilles', 90, 'admin', 1241503477, '', 0),
(158, 'New Caledonia', 89, 'admin', 1241503486, '', 0),
(159, 'New Zealand', 88, 'admin', 1241503503, '', 0),
(160, 'Nicaragua', 87, 'admin', 1241503508, '', 0),
(161, 'Niger', 86, 'admin', 1241503517, '', 0),
(162, 'Nigeria', 85, 'admin', 1241503530, '', 0),
(163, 'Niue', 84, 'admin', 1241503543, '', 0),
(164, 'Norfolk Island', 83, 'admin', 1241503552, '', 0),
(165, 'Northern Mariana Islands', 82, 'admin', 1241503561, '', 0),
(166, 'Norway', 81, 'admin', 1241503566, '', 0),
(167, 'Oman', 80, 'admin', 1241503572, '', 0),
(168, 'Pakistan', 79, 'admin', 1241503625, '', 0),
(169, 'Palau', 78, 'admin', 1241503630, '', 0),
(170, 'Palestinian Territory, Occupied', 77, 'admin', 1241503639, '', 0),
(171, 'Panama', 76, 'admin', 1241503647, '', 0),
(172, 'Papua new Guinea', 75, 'admin', 1241503655, '', 0),
(173, 'Paraguay', 74, 'admin', 1241503661, '', 0),
(174, 'Peru', 73, 'admin', 1241503695, '', 0),
(175, 'Philippines', 72, 'admin', 1241503702, '', 0),
(176, 'Pitcairn Island', 71, 'admin', 1241503712, '', 0),
(177, 'Poland', 70, 'admin', 1241503717, '', 0),
(178, 'Portugal', 69, 'admin', 1241503725, '', 0),
(179, 'Puerto Rico', 68, 'admin', 1241503758, '', 0),
(180, 'Qatar', 67, 'admin', 1241503765, '', 0),
(181, 'Reunion', 66, 'admin', 1241503775, '', 0),
(182, 'Romania', 65, 'admin', 1241503780, '', 0),
(183, 'Russia', 64, 'admin', 1241503786, '', 0),
(184, 'Rwanda', 63, 'admin', 1241503792, '', 0),
(185, 'Saint Helena', 62, 'admin', 1241503799, '', 0),
(186, 'Saint Kitts And Nevis', 61, 'admin', 1241503807, '', 0),
(187, 'Saint Lucia', 60, 'admin', 1241503814, '', 0),
(188, 'Saint Pierre and Miquelon', 59, 'admin', 1241503822, '', 0),
(189, 'Saint Vincent And The Grenadines', 58, 'admin', 1241503830, '', 0),
(190, 'Samoa', 57, 'admin', 1241503835, '', 0),
(191, 'San Marino', 56, 'admin', 1241503844, '', 0),
(192, 'Sao Tome and Principe', 55, 'admin', 1241503851, '', 0),
(193, 'Saudi Arabia', 54, 'admin', 1241503858, '', 0),
(194, 'Senegal', 53, 'admin', 1241503864, '', 0),
(195, 'Serbia', 52, 'admin', 1241503868, '', 0),
(196, 'Seychelles', 51, 'admin', 1241503873, '', 0),
(197, 'Sierra Leone', 50, 'admin', 1241503881, '', 0),
(198, 'Singapore', 49, 'admin', 1241503886, '', 0),
(199, 'Slovakia', 48, 'admin', 1241503892, '', 0),
(200, 'Slovenia', 47, 'admin', 1241503898, '', 0),
(201, 'Solomon Islands', 46, 'admin', 1241503905, '', 0),
(202, 'Somalia', 45, 'admin', 1241503914, '', 0),
(203, 'South Africa', 44, 'admin', 1241503924, '', 0),
(204, 'South Georgia And The South Sandwich Islands', 43, 'admin', 1241503932, '', 0),
(205, 'Spain', 42, 'admin', 1241503939, '', 0),
(206, 'Sri Lanka', 41, 'admin', 1241503946, '', 0),
(207, 'Sudan', 40, 'admin', 1241503951, '', 0),
(208, 'Suriname', 39, 'admin', 1241503957, '', 0),
(209, 'Svalbard And Jan Mayen Islands', 38, 'admin', 1241503965, '', 0),
(210, 'Swaziland', 37, 'admin', 1241503972, '', 0),
(211, 'Sweden', 36, 'admin', 1241503977, '', 0),
(212, 'Switzerland', 35, 'admin', 1241503985, '', 0),
(213, 'Syria', 34, 'admin', 1241504000, '', 0),
(214, 'Taiwan', 33, 'admin', 1241504006, '', 0),
(215, 'Tajikistan', 32, 'admin', 1241504011, '', 0),
(216, 'Tanzania', 31, 'admin', 1241504018, '', 0),
(217, 'Thailand', 30, 'admin', 1241504026, '', 0),
(218, 'Timor-Leste', 29, 'admin', 1241504036, '', 0),
(219, 'Togo', 28, 'admin', 1241504042, '', 0),
(220, 'Tokelau', 27, 'admin', 1241504056, '', 0),
(221, 'Tonga', 26, 'admin', 1241504143, '', 0),
(222, 'Trinidad And Tobago', 25, 'admin', 1241504153, '', 0),
(223, 'Tunisia', 24, 'admin', 1241504158, '', 0),
(224, 'Turkey', 23, 'admin', 1241504163, '', 0),
(225, 'Turkmenistan', 22, 'admin', 1241504169, '', 0),
(226, 'Turks And Caicos Islands', 21, 'admin', 1241504177, '', 0),
(227, 'Tuvalu', 20, 'admin', 1241504183, '', 0),
(228, 'Uganda', 19, 'admin', 1241504189, '', 0),
(229, 'Ukraine', 18, 'admin', 1241504200, '', 0),
(230, 'United Arab Emirates', 17, 'admin', 1241504208, '', 0),
(231, 'United Kingdom', 16, 'admin', 1241504217, '', 0),
(232, 'United States', 15, 'admin', 1241504224, '', 0),
(233, 'United States Minor Outlying Islands', 14, 'admin', 1241504231, '', 0),
(234, 'Uruguay', 13, 'admin', 1241504237, '', 0),
(235, 'Uzbekistan', 12, 'admin', 1241504244, '', 0),
(236, 'Vanuatu', 11, 'admin', 1241504249, '', 0),
(237, 'Vatican City State (Holy See)', 10, 'admin', 1241504257, '', 0),
(238, 'Venezuela', 9, 'admin', 1241504262, '', 0),
(239, 'Vietnam', 8, 'admin', 1241504269, '', 0),
(240, 'Virgin Islands (British)', 7, 'admin', 1241504278, '', 0),
(241, 'Virgin Islands (US)', 6, 'admin', 1241504286, '', 0),
(242, 'WESTERN SAHARA', 5, 'admin', 1241504294, '', 0),
(243, 'Wallis And Futuna Islands', 4, 'admin', 1241504303, '', 0),
(244, 'Yemen', 3, 'admin', 1241504310, '', 0),
(245, 'Zambia', 2, 'admin', 1241504317, '', 0),
(246, 'Zimbabwe', 1, 'admin', 1241504323, '', 0),
(247, 'Rest of the world', 0, 'admin', 1241502080, '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_currency`
-- 

CREATE TABLE `lumonata_currency` (
  `lcurrency_id` int(11) NOT NULL auto_increment,
  `lcode` varchar(25) NOT NULL,
  `lsymbol` varchar(50) NOT NULL,
  `lcurrency` varchar(255) NOT NULL,
  `lamount` decimal(10,5) NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` int(11) NOT NULL,
  PRIMARY KEY  (`lcurrency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `lumonata_currency`
-- 

INSERT INTO `lumonata_currency` (`lcurrency_id`, `lcode`, `lsymbol`, `lcurrency`, `lamount`, `lorder_id`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(8, 'AUD', '$', 'Australia Dollars', 0.00000, 56, 'admin', 1289791160, 'admin', 1305882382),
(7, 'EUR', 'â‚¬', 'Euro', 0.00000, 5, 'admin', 1289791092, 'admin', 1305882229),
(6, 'USD', '$', 'United States Dollar', 0.00000, 51, 'admin', 1289791060, 'admin', 1307754273),
(9, 'IDR', 'Rp', 'Indonesian Rupiah', 0.00000, 10, 'admin', 1289791212, 'admin', 1305882274),
(11, 'GBP', 'Â£', 'Pound Sterling', 0.00000, 1, 'admin', 1301300965, '', 0),
(12, 'CZK', 'KÄ', 'Koruna ÄeskÃ¡', 0.00000, 15, 'admin', 1305882631, '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friendship`
-- 

CREATE TABLE `lumonata_friendship` (
  `lfriendship_id` bigint(20) NOT NULL auto_increment,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY  (`lfriendship_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `lumonata_friendship`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friends_list`
-- 

CREATE TABLE `lumonata_friends_list` (
  `lfriends_list_id` bigint(20) NOT NULL auto_increment,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) character set utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL,
  PRIMARY KEY  (`lfriends_list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `lumonata_friends_list`
-- 

INSERT INTO `lumonata_friends_list` (`lfriends_list_id`, `luser_id`, `llist_name`, `lorder`) VALUES 
(1, 1, 'Work', 1),
(2, 1, 'School', 2),
(3, 1, 'Familiy', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_friends_list_rel`
-- 

CREATE TABLE `lumonata_friends_list_rel` (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`lfriendship_id`,`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `lumonata_friends_list_rel`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_languages`
-- 

CREATE TABLE `lumonata_languages` (
  `llanguage_id` bigint(20) NOT NULL auto_increment,
  `llanguage_title` text collate latin1_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` datetime NOT NULL,
  PRIMARY KEY  (`llanguage_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `lumonata_languages`
-- 

INSERT INTO `lumonata_languages` (`llanguage_id`, `llanguage_title`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`) VALUES 
(1, 'English', 1, 0, '2011-10-14 14:41:06', '0000-00-00 00:00:00'),
(2, 'Dutch', 2, 0, '2011-10-14 14:41:22', '0000-00-00 00:00:00'),
(3, 'Spanish', 3, 0, '2011-10-14 14:42:21', '0000-00-00 00:00:00'),
(4, 'Franc', 4, 0, '2011-10-14 14:42:28', '0000-00-00 00:00:00'),
(5, 'Italian', 5, 0, '2011-10-14 14:43:01', '0000-00-00 00:00:00'),
(6, 'Turkish', 6, 0, '2011-10-14 14:43:31', '0000-00-00 00:00:00'),
(7, 'Swedish', 7, 0, '2011-10-14 14:44:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_meta_data`
-- 

CREATE TABLE `lumonata_meta_data` (
  `lmeta_id` int(11) NOT NULL auto_increment,
  `lmeta_name` varchar(200) character set utf8 NOT NULL,
  `lmeta_value` longtext character set utf8 NOT NULL,
  `lapp_name` varchar(200) character set utf8 NOT NULL,
  `lapp_id` int(11) NOT NULL,
  PRIMARY KEY  (`lmeta_id`),
  KEY `meta_name` (`lmeta_name`),
  KEY `app_name` (`lapp_name`),
  KEY `app_id` (`lapp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=114 ;

-- 
-- Dumping data for table `lumonata_meta_data`
-- 

INSERT INTO `lumonata_meta_data` (`lmeta_id`, `lmeta_name`, `lmeta_value`, `lapp_name`, `lapp_id`) VALUES 
(1, 'front_theme', 'newtemplate', 'themes', 0),
(2, 'time_zone', 'Asia/Singapore', 'global_setting', 0),
(3, 'site_url', '10.10.10.18/arunna_training', 'global_setting', 0),
(4, 'web_title', 'Project Q  ', 'global_setting', 0),
(5, 'smtp_server', '10.10.10.43', 'global_setting', 0),
(6, 'admin_theme', 'default', 'themes', 0),
(7, 'smtp', 'mail.email.com', 'global_setting', 0),
(8, 'email', 'admin@mail.com', 'global_setting', 0),
(9, 'web_tagline', 'New Project For New Progrmammer', 'global_setting', 0),
(10, 'invitation_limit', '10', 'global_setting', 0),
(11, 'date_format', 'F j, Y', 'global_setting', 0),
(12, 'time_format', 'H:i', 'global_setting', 0),
(13, 'post_viewed', '10', 'global_setting', 0),
(14, 'rss_viewed', '15', 'global_setting', 0),
(15, 'rss_view_format', 'full_text', 'global_setting', 0),
(16, 'list_viewed', '30', 'global_setting', 0),
(17, 'email_format', 'plain_text', 'global_setting', 0),
(18, 'text_editor', 'tiny_mce', 'global_setting', 0),
(19, 'thumbnail_image_size', '150:150', 'global_setting', 0),
(20, 'large_image_size', '1024:1024', 'global_setting', 0),
(21, 'medium_image_size', '300:300', 'global_setting', 0),
(22, 'is_allow_comment', '1', 'global_setting', 0),
(23, 'is_login_to_comment', '1', 'global_setting', 0),
(24, 'is_auto_close_comment', '0', 'global_setting', 0),
(25, 'days_auto_close_comment', '15', 'global_setting', 0),
(26, 'is_break_comment', '1', 'global_setting', 0),
(27, 'comment_page_displayed', 'last', 'global_setting', 0),
(28, 'comment_per_page', '3', 'global_setting', 0),
(29, 'active_plugins', '{"contact-us":"\\/contact_us\\/contact_us.php","shopping-cart":"\\/shopping_cart\\/shoping.php","lumonata-meta-data":"\\/metadata\\/metadata.php"}', 'plugins', 0),
(30, 'save_changes', 'Save Changes', 'global_setting', 0),
(31, 'is_rewrite', 'yes', 'global_setting', 0),
(32, 'is_allow_post_like', '1', 'global_setting', 0),
(33, 'is_allow_comment_like', '1', 'global_setting', 0),
(34, 'alert_on_register', '1', 'global_setting', 0),
(35, 'alert_on_comment', '1', 'global_setting', 0),
(36, 'alert_on_comment_reply', '1', 'global_setting', 0),
(37, 'alert_on_liked_post', '1', 'global_setting', 0),
(38, 'alert_on_liked_comment', '1', 'global_setting', 0),
(39, 'web_name', 'Lumonata Corp.                                ', 'global_setting', 0),
(40, 'meta_description', 'learning', 'global_setting', 0),
(41, 'meta_keywords', 'learning', 'global_setting', 0),
(42, 'meta_title', 'Arunna - For learning', 'global_setting', 0),
(43, 'custome_bg_color', 'c4c4c4', 'themes', 0),
(44, 'status_viewed', '30', 'global_setting', 0),
(45, 'update', 'true', 'global_setting', 0),
(46, 'the_date_format', 'F j, Y', 'global_setting', 0),
(47, 'the_time_format', 'H:i', 'global_setting', 0),
(48, 'thumbnail_image_width', '150', 'global_setting', 0),
(49, 'thumbnail_image_height', '150', 'global_setting', 0),
(50, 'medium_image_width', '300', 'global_setting', 0),
(51, 'medium_image_height', '300', 'global_setting', 0),
(52, 'large_image_width', '1024', 'global_setting', 0),
(53, 'large_image_height', '1024', 'global_setting', 0),
(54, 'menu_set', '[]', 'menus', 0),
(113, 'payments', '{"parent_payment":["payments_bank_transfer"],"child_payment":[{"name":"Bank transfer","text":"Please use the following bank account number to pay for your items: BCC 000-00000000-00. Mention your order reference ID in your bank transfer so that we can find your payment and order quickly.","mode":"1"}]}', 'product_setting', 0),
(57, 'language', '1', 'product_setting', 0),
(58, 'currency', '6', 'product_setting', 0),
(59, 'unit_system', 'imperial', 'product_setting', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_notifications`
-- 

CREATE TABLE `lumonata_notifications` (
  `lnotification_id` bigint(20) NOT NULL auto_increment,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY  (`lnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `lumonata_notifications`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_product`
-- 

CREATE TABLE `lumonata_product` (
  `lproduct_id` bigint(11) NOT NULL auto_increment,
  `lproduct_name` varchar(60) collate latin1_general_ci NOT NULL,
  `lproduct_describ` text collate latin1_general_ci NOT NULL,
  `lproduct_category` varchar(60) collate latin1_general_ci NOT NULL,
  `lproduct_price` double NOT NULL,
  `lproduct_weight` int(11) NOT NULL,
  `lproduct_tax` int(11) NOT NULL,
  `lproduct_stock` int(11) NOT NULL,
  `lproduct_images` varchar(60) collate latin1_general_ci NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lproduct_status` varchar(60) collate latin1_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL default '1',
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `product_buy_out_of_stock` varchar(20) collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`lproduct_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=45 ;

-- 
-- Dumping data for table `lumonata_product`
-- 

INSERT INTO `lumonata_product` (`lproduct_id`, `lproduct_name`, `lproduct_describ`, `lproduct_category`, `lproduct_price`, `lproduct_weight`, `lproduct_tax`, `lproduct_stock`, `lproduct_images`, `lpost_by`, `lpost_date`, `lproduct_status`, `lorder`, `lupdated_by`, `ldlu`, `product_buy_out_of_stock`) VALUES 
(2, 'Test', '<p>Aloha T-shirt</p>', '', 20, 10, 0, 0, '', 1, '2011-09-13 10:22:49', 'draft', 46, 0, '0000-00-00 00:00:00', ''),
(3, 'billabong', '<p>this is the description of our product</p>', '', 10, 25, 0, 1, '', 1, '2011-09-15 15:23:30', 'draft', 44, 0, '0000-00-00 00:00:00', ''),
(4, 'adidas', '<p>blablablabla</p>', '', 20, 25, 0, 1, '', 1, '2011-09-15 16:14:55', 'draft', 43, 0, '0000-00-00 00:00:00', ''),
(12, 'Product_3', '<p>description for product 3</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 11:29:22', 'draft', 33, 0, '0000-00-00 00:00:00', '0'),
(13, 'asda', '<p>asd</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 12:04:19', 'draft', 32, 0, '0000-00-00 00:00:00', '1'),
(14, 'product_4', '', '', 10, 20, 0, 30, '', 1, '2011-10-03 14:42:25', 'draft', 31, 0, '0000-00-00 00:00:00', '0'),
(39, 'Product_7', '<p>this is the desscription of no 7 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:40:08', 'draft', 6, 0, '0000-00-00 00:00:00', '1'),
(38, 'Product_7', '<p>this is the desscription of no 7 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:39:33', 'draft', 7, 0, '0000-00-00 00:00:00', '1'),
(37, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:15:40', 'draft', 8, 0, '0000-00-00 00:00:00', '1'),
(36, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:11:52', 'draft', 9, 0, '0000-00-00 00:00:00', '1'),
(35, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:11:29', 'draft', 10, 0, '0000-00-00 00:00:00', '1'),
(33, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 15:57:21', 'draft', 12, 0, '0000-00-00 00:00:00', '1'),
(32, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 15:51:28', 'draft', 13, 0, '0000-00-00 00:00:00', '1'),
(34, 'Product_6', '<p>this is the desscription of no 6 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:10:50', 'draft', 11, 0, '0000-00-00 00:00:00', '1'),
(40, 'Product_7', '<p>this is the desscription of no 7 product</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 16:41:27', 'draft', 5, 0, '0000-00-00 00:00:00', '1'),
(41, 'Product_8', '<p>fgf</p>', '', 0, 0, 0, 0, '', 1, '2011-10-03 16:52:40', 'draft', 4, 0, '0000-00-00 00:00:00', '0'),
(42, 'Product_8', '<p>fgf</p>', '', 0, 0, 0, 0, '', 1, '2011-10-03 16:58:56', 'draft', 3, 0, '0000-00-00 00:00:00', '0'),
(43, 'Product_8', '<p>fgf</p>', '', 0, 0, 0, 0, '', 1, '2011-10-03 17:10:18', 'draft', 2, 0, '0000-00-00 00:00:00', '0'),
(44, 'Product_9', '<p>Deskription</p>', '', 10, 11, 0, 12, '', 1, '2011-10-03 17:15:19', 'draft', 1, 0, '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_product_attachmen`
-- 

CREATE TABLE `lumonata_product_attachmen` (
  `lattach_id` bigint(20) NOT NULL auto_increment,
  `lproduct_id` bigint(11) NOT NULL,
  `lattach_loc` text collate latin1_general_ci NOT NULL,
  `lattach_loc_thumb` text collate latin1_general_ci NOT NULL,
  `lattach_loc_medium` text collate latin1_general_ci NOT NULL,
  `lattach_loc_large` text collate latin1_general_ci NOT NULL,
  `ltitle` varchar(200) collate latin1_general_ci NOT NULL,
  `lalt_text` text collate latin1_general_ci NOT NULL,
  `lcaption` varchar(200) collate latin1_general_ci NOT NULL,
  `mime_type` varchar(50) collate latin1_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY  (`lattach_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=390 ;

-- 
-- Dumping data for table `lumonata_product_attachmen`
-- 

INSERT INTO `lumonata_product_attachmen` (`lattach_id`, `lproduct_id`, `lattach_loc`, `lattach_loc_thumb`, `lattach_loc_medium`, `lattach_loc_large`, `ltitle`, `lalt_text`, `lcaption`, `mime_type`, `lorder`, `upload_date`, `date_last_update`) VALUES 
(382, 38, '/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1317631157.jpg', '/shopping_cart/uploads/product/thumbnail/3158613157106989521416251363309301581033102n-1317631157.jpg', '', '', '3158613157106989521416251363309301581033102n-1317631157.jpg', '', '', 'image/jpeg', 1, '2011-10-03 16:39:17', '0000-00-00 00:00:00'),
(383, 44, '/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1317633311.jpg', '/shopping_cart/uploads/product/thumbnail/3158613157106989521416251363309301581033102n-1317633311.jpg', '', '', '3158613157106989521416251363309301581033102n-1317633311.jpg', '', '', 'image/jpeg', 1, '2011-10-03 17:15:11', '0000-00-00 00:00:00'),
(380, 15, '/shopping_cart/uploads/product/detail07-1317625259.jpg', '/shopping_cart/uploads/product/thumbnail/detail07-1317625259.jpg', '', '', 'detail07-1317625259.jpg', '', '', 'image/jpeg', 1, '2011-10-03 15:00:59', '0000-00-00 00:00:00'),
(381, 32, '/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1317628281.jpg', '/shopping_cart/uploads/product/thumbnail/3158613157106989521416251363309301581033102n-1317628281.jpg', '', '', '3158613157106989521416251363309301581033102n-1317628281.jpg', '', '', 'image/jpeg', 1, '2011-10-03 15:51:21', '0000-00-00 00:00:00'),
(376, 14, '/shopping_cart/uploads/product/detail07-1317624133.jpg', '/shopping_cart/uploads/product/thumbnail/detail07-1317624133.jpg', '', '', 'detail07-1317624133.jpg', '', '', 'image/jpeg', 1, '2011-10-03 14:42:14', '0000-00-00 00:00:00'),
(374, 5, '/shopping_cart/uploads/product/detail07-1317610489.jpg', '/shopping_cart/uploads/product/thumbnail/detail07-1317610489.jpg', '', '', 'detail07-1317610489.jpg', '', '', 'image/jpeg', 1, '2011-10-03 10:54:49', '0000-00-00 00:00:00'),
(375, 13, '/shopping_cart/uploads/product/3158613157106989521416251363309301581033102n-1317614648.jpg', '/shopping_cart/uploads/product/thumbnail/3158613157106989521416251363309301581033102n-1317614648.jpg', '', '', '3158613157106989521416251363309301581033102n-1317614648.jpg', '', '', 'image/jpeg', 1, '2011-10-03 12:04:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_product_relationship`
-- 

CREATE TABLE `lumonata_product_relationship` (
  `lrule_id` bigint(20) NOT NULL,
  `lproduct_id` varchar(11) collate latin1_general_ci NOT NULL,
  `lorder_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_product_relationship`
-- 

INSERT INTO `lumonata_product_relationship` (`lrule_id`, `lproduct_id`, `lorder_id`) VALUES 
(24, '26', 0),
(436, '29', 0),
(20, '32', 0),
(19, '32', 0),
(444, '32', 0),
(1, '41', 0),
(21, '44', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_rules`
-- 

CREATE TABLE `lumonata_rules` (
  `lrule_id` bigint(20) NOT NULL auto_increment,
  `lparent` bigint(20) NOT NULL,
  `lname` varchar(200) character set utf8 NOT NULL,
  `lsef` varchar(200) character set utf8 NOT NULL,
  `ldescription` text character set utf8 NOT NULL,
  `lrule` varchar(200) character set utf8 NOT NULL,
  `lgroup` varchar(200) character set utf8 NOT NULL,
  `lcount` bigint(20) NOT NULL default '0',
  `lorder` bigint(20) NOT NULL default '1',
  `lsubsite` varchar(100) collate latin1_general_ci NOT NULL default 'arunna',
  PRIMARY KEY  (`lrule_id`),
  KEY `rules_name` (`lname`),
  KEY `sef` (`lsef`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=478 ;

-- 
-- Dumping data for table `lumonata_rules`
-- 

INSERT INTO `lumonata_rules` (`lrule_id`, `lparent`, `lname`, `lsef`, `ldescription`, `lrule`, `lgroup`, `lcount`, `lorder`, `lsubsite`) VALUES 
(1, 0, 'Uncategorized', 'uncategorized', '', 'categories', 'default', 4, 283, 'arunna'),
(2, 0, 'Designer', 'designer', '', 'categories', 'global_settings', 0, 176, 'arunna'),
(3, 0, 'Entepreneurs', 'entepreneurs', '', 'categories', 'global_settings', 1, 175, 'arunna'),
(4, 0, 'Photographer', 'photographer', '', 'categories', 'global_settings', 0, 174, 'arunna'),
(5, 0, 'Programmer', 'programmer', '', 'categories', 'global_settings', 1, 173, 'arunna'),
(6, 0, 'CEO', 'ceo', '', 'tags', 'profile', 1, 172, 'arunna'),
(7, 0, 'Cooking', 'cooking', '', 'skills', 'profile', 1, 171, 'arunna'),
(8, 0, 'User', 'user', 'User Dumper', 'categories', 'global_settings', 0, 170, 'arunna'),
(10, 0, 'September 2011', 'september-2011', '', 'categories', 'articles', 1, 168, 'arunna'),
(11, 1, 'Sub Catagories', 'sub-catagories', 'test catagories', 'categories', 'articles', 0, 167, 'arunna'),
(476, 0, 'Clothing', 'clothing', '', 'tags', 'products', 4, 2, 'arunna'),
(24, 0, 'Shoes', 'shoes-2', 'shoes collection', 'categories', 'products', 0, 154, 'arunna'),
(16, 0, 'shoes', 'shoes', '', 'catagories', 'products', 0, 162, 'arunna'),
(19, 0, 'Hat', 'hat', 'All About Hat', 'categories', 'products', 0, 159, 'arunna'),
(20, 0, 'T-shirt', 't-shirt-2', 'All About T-shirt', 'categories', 'products', 1, 158, 'arunna'),
(43, 0, 'Size', 'size', '', 'variant', 'product', 0, 138, 'arunna'),
(475, 0, 'Pant', 'pant', '', 'categories', 'products', 1, 3, 'arunna'),
(449, 0, 'Ripcurl', 'ripcurl', '', 'tags', 'products', 2, 12, 'arunna'),
(440, 43, 'Small', 'small', '', 'variant', 'product', 0, 17, 'arunna'),
(441, 43, 'medium', 'medium', '', 'variant', 'product', 0, 17, 'arunna'),
(442, 43, 'large', 'large', '', 'variant', 'product', 0, 17, 'arunna'),
(448, 0, 'Adidas', 'adidas', '', 'tags', 'products', 0, 13, 'arunna'),
(477, 0, 'Bag', 'bag', '', 'categories', 'products', 1, 1, 'arunna'),
(444, 0, 'clothes', 'clothes', '', 'tags', 'products', 0, 15, 'arunna'),
(445, 0, 'Sport', 'sport', '', 'variant', 'product', 0, 14, 'arunna'),
(446, 445, 'sport1', 'sport1', '', 'variant', 'product', 0, 14, 'arunna'),
(447, 445, 'sport2', 'sport2', '', 'variant', 'product', 0, 14, 'arunna'),
(411, 0, 'Color', 'color', '', 'variant', 'product', 0, 25, 'arunna'),
(438, 411, 'Black', 'black', '', 'variant', 'product', 0, 17, 'arunna'),
(439, 411, 'Blue', 'blue', '', 'variant', 'product', 0, 17, 'arunna'),
(455, 0, 'Shirt', 'shirt', '', 'categories', 'products', 2, 6, 'arunna'),
(437, 411, 'Red', 'red', '', 'variant', 'product', 0, 17, 'arunna');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_rule_relationship`
-- 

CREATE TABLE `lumonata_rule_relationship` (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`lapp_id`,`lrule_id`),
  KEY `taxonomy_id` (`lrule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Dumping data for table `lumonata_rule_relationship`
-- 

INSERT INTO `lumonata_rule_relationship` (`lapp_id`, `lrule_id`, `lorder_id`) VALUES 
(3, 1, 0),
(6, 10, 0),
(1, 5, 0),
(74, 476, 0),
(10, 1, 0),
(20, 1, 0),
(73, 476, 0),
(52, 449, 0),
(73, 455, 0),
(52, 476, 0),
(52, 455, 0),
(76, 449, 0),
(37, 1, 0),
(75, 476, 0),
(76, 477, 0),
(74, 20, 0),
(75, 475, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_shipping`
-- 

CREATE TABLE `lumonata_shipping` (
  `lshipping_id` int(11) NOT NULL auto_increment,
  `lcountry_id` int(11) NOT NULL,
  `lshipping_type` varchar(60) NOT NULL,
  `lformula` decimal(10,0) NOT NULL,
  `unit_symbol` varchar(50) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` int(11) NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` int(11) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lshipping_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- 
-- Dumping data for table `lumonata_shipping`
-- 

INSERT INTO `lumonata_shipping` (`lshipping_id`, `lcountry_id`, `lshipping_type`, `lformula`, `unit_symbol`, `lcurrency_id`, `lprice`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`, `llang_id`) VALUES 
(44, 1, 'weight', 2, 'Kilograms\n\n', 6, 3.00, 2, '1', 2011, '1', 2011, 1),
(43, 1, 'weight', 1, 'Pounds\n\n', 6, 3.00, 3, '1', 2011, '1', 2011, 1),
(45, 2, 'price', 0, 'USD', 6, 3.00, 1, '1', 2011, '1', 2011, 1),
(46, 2, 'price', 12, 'USD', 6, 4.00, 0, '1', 2011, '1', 2011, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_shipping_detail`
-- 

CREATE TABLE `lumonata_shipping_detail` (
  `lshipping_detail_id` int(11) NOT NULL auto_increment,
  `lshipping_id` int(11) NOT NULL,
  `lshipping_type_id` int(11) NOT NULL,
  `lcurrency_id` int(11) NOT NULL,
  `lprice` decimal(10,2) NOT NULL,
  `lorder_id` int(11) NOT NULL,
  `llang_id` int(11) NOT NULL,
  PRIMARY KEY  (`lshipping_detail_id`,`llang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=333 ;

-- 
-- Dumping data for table `lumonata_shipping_detail`
-- 

INSERT INTO `lumonata_shipping_detail` (`lshipping_detail_id`, `lshipping_id`, `lshipping_type_id`, `lcurrency_id`, `lprice`, `lorder_id`, `llang_id`) VALUES 
(302, 0, 1, 7, 50.00, 34, 15),
(301, 0, 2, 7, 100.00, 35, 15),
(326, 5, 1, 11, 15.00, 10, 1),
(287, 0, 1, 11, 50.00, 49, 1),
(329, 2, 1, 11, 15.00, 7, 1),
(332, 26, 1, 11, 50.00, 2, 1),
(300, 0, 3, 0, 0.00, 36, 15),
(296, 0, 1, 6, 50.00, 40, 3),
(295, 0, 2, 6, 100.00, 41, 3),
(294, 0, 3, 0, 0.00, 42, 3),
(299, 0, 1, 7, 50.00, 37, 5),
(298, 0, 2, 7, 100.00, 38, 5),
(297, 0, 3, 7, 0.00, 39, 5),
(286, 0, 2, 11, 100.00, 50, 1),
(285, 0, 3, 11, 0.00, 51, 1),
(327, 4, 1, 11, 15.00, 9, 1),
(328, 3, 1, 11, 15.00, 8, 1),
(290, 0, 1, 9, 50.00, 46, 17),
(289, 0, 2, 9, 100.00, 47, 17),
(288, 0, 3, 0, 0.00, 48, 17),
(293, 0, 1, 12, 50.00, 43, 16),
(292, 0, 2, 12, 100.00, 44, 16),
(291, 0, 3, 0, 0.00, 45, 16),
(323, 8, 1, 11, 40.00, 13, 1),
(324, 7, 1, 11, 15.00, 12, 1),
(325, 6, 1, 11, 30.00, 11, 1),
(322, 9, 1, 11, 15.00, 14, 1),
(321, 10, 1, 11, 15.00, 15, 1),
(320, 11, 1, 11, 15.00, 16, 1),
(319, 12, 1, 11, 30.00, 17, 1),
(318, 13, 1, 11, 40.00, 18, 1),
(317, 14, 1, 11, 15.00, 19, 1),
(316, 15, 1, 11, 15.00, 20, 1),
(315, 16, 1, 11, 15.00, 21, 1),
(314, 17, 1, 11, 15.00, 22, 1),
(313, 18, 1, 11, 45.00, 23, 1),
(312, 19, 1, 11, 45.00, 24, 1),
(311, 20, 1, 11, 45.00, 25, 1),
(310, 21, 1, 11, 15.00, 26, 1),
(309, 22, 1, 11, 60.00, 27, 1),
(308, 23, 1, 11, 25.00, 28, 1),
(307, 24, 1, 11, 15.00, 29, 1),
(306, 25, 1, 11, 15.00, 30, 1),
(331, 26, 2, 11, 100.00, 3, 1),
(330, 26, 3, 11, 0.00, 4, 1),
(326, 5, 1, 11, 15.00, 10, 3),
(287, 0, 1, 11, 50.00, 49, 3),
(329, 2, 1, 11, 15.00, 7, 3),
(332, 26, 1, 11, 50.00, 2, 3),
(286, 0, 2, 11, 100.00, 50, 3),
(285, 0, 3, 11, 0.00, 51, 3),
(327, 4, 1, 11, 15.00, 9, 3),
(328, 3, 1, 11, 15.00, 8, 3),
(323, 8, 1, 11, 40.00, 13, 3),
(324, 7, 1, 11, 15.00, 12, 3),
(325, 6, 1, 11, 30.00, 11, 3),
(322, 9, 1, 11, 15.00, 14, 3),
(321, 10, 1, 11, 15.00, 15, 3),
(320, 11, 1, 11, 15.00, 16, 3),
(319, 12, 1, 11, 30.00, 17, 3),
(318, 13, 1, 11, 40.00, 18, 3),
(317, 14, 1, 11, 15.00, 19, 3),
(316, 15, 1, 11, 15.00, 20, 3),
(315, 16, 1, 11, 15.00, 21, 3),
(314, 17, 1, 11, 15.00, 22, 3),
(313, 18, 1, 11, 45.00, 23, 3),
(312, 19, 1, 11, 45.00, 24, 3),
(311, 20, 1, 11, 45.00, 25, 3),
(310, 21, 1, 11, 15.00, 26, 3),
(309, 22, 1, 11, 60.00, 27, 3),
(308, 23, 1, 11, 25.00, 28, 3),
(307, 24, 1, 11, 15.00, 29, 3),
(306, 25, 1, 11, 15.00, 30, 3),
(331, 26, 2, 11, 100.00, 3, 3),
(330, 26, 3, 11, 0.00, 4, 3),
(326, 5, 1, 7, 15.00, 10, 5),
(287, 0, 1, 7, 50.00, 49, 5),
(329, 2, 1, 7, 15.00, 7, 5),
(332, 26, 1, 7, 50.00, 2, 5),
(286, 0, 2, 7, 100.00, 50, 5),
(285, 0, 3, 7, 0.00, 51, 5),
(327, 4, 1, 7, 15.00, 9, 5),
(328, 3, 1, 7, 15.00, 8, 5),
(323, 8, 1, 7, 40.00, 13, 5),
(324, 7, 1, 7, 15.00, 12, 5),
(325, 6, 1, 7, 30.00, 11, 5),
(322, 9, 1, 7, 15.00, 14, 5),
(321, 10, 1, 7, 15.00, 15, 5),
(320, 11, 1, 7, 15.00, 16, 5),
(319, 12, 1, 7, 30.00, 17, 5),
(318, 13, 1, 7, 40.00, 18, 5),
(317, 14, 1, 7, 15.00, 19, 5),
(316, 15, 1, 7, 15.00, 20, 5),
(315, 16, 1, 7, 15.00, 21, 5),
(314, 17, 1, 7, 15.00, 22, 5),
(313, 18, 1, 7, 45.00, 23, 5),
(312, 19, 1, 7, 45.00, 24, 5),
(311, 20, 1, 7, 45.00, 25, 5),
(310, 21, 1, 7, 15.00, 26, 5),
(309, 22, 1, 7, 60.00, 27, 5),
(308, 23, 1, 7, 25.00, 28, 5),
(307, 24, 1, 7, 15.00, 29, 5),
(306, 25, 1, 7, 15.00, 30, 5),
(331, 26, 2, 7, 100.00, 3, 5),
(330, 26, 3, 7, 0.00, 4, 5),
(326, 5, 1, 11, 15.00, 10, 15),
(287, 0, 1, 11, 50.00, 49, 15),
(329, 2, 1, 11, 15.00, 7, 15),
(332, 26, 1, 11, 50.00, 2, 15),
(286, 0, 2, 11, 100.00, 50, 15),
(285, 0, 3, 11, 0.00, 51, 15),
(327, 4, 1, 11, 15.00, 9, 15),
(328, 3, 1, 11, 15.00, 8, 15),
(323, 8, 1, 11, 40.00, 13, 15),
(324, 7, 1, 11, 15.00, 12, 15),
(325, 6, 1, 11, 30.00, 11, 15),
(322, 9, 1, 11, 15.00, 14, 15),
(321, 10, 1, 11, 15.00, 15, 15),
(320, 11, 1, 11, 15.00, 16, 15),
(319, 12, 1, 11, 30.00, 17, 15),
(318, 13, 1, 11, 40.00, 18, 15),
(317, 14, 1, 11, 15.00, 19, 15),
(316, 15, 1, 11, 15.00, 20, 15),
(315, 16, 1, 11, 15.00, 21, 15),
(314, 17, 1, 11, 15.00, 22, 15),
(313, 18, 1, 11, 45.00, 23, 15),
(312, 19, 1, 11, 45.00, 24, 15),
(311, 20, 1, 11, 45.00, 25, 15),
(310, 21, 1, 11, 15.00, 26, 15),
(309, 22, 1, 11, 60.00, 27, 15),
(308, 23, 1, 11, 25.00, 28, 15),
(307, 24, 1, 11, 15.00, 29, 15),
(306, 25, 1, 11, 15.00, 30, 15),
(331, 26, 2, 11, 100.00, 3, 15),
(330, 26, 3, 11, 0.00, 4, 15),
(326, 5, 1, 11, 15.00, 10, 16),
(287, 0, 1, 11, 50.00, 49, 16),
(329, 2, 1, 11, 15.00, 7, 16),
(332, 26, 1, 11, 50.00, 2, 16),
(286, 0, 2, 11, 100.00, 50, 16),
(285, 0, 3, 11, 0.00, 51, 16),
(327, 4, 1, 11, 15.00, 9, 16),
(328, 3, 1, 11, 15.00, 8, 16),
(323, 8, 1, 11, 40.00, 13, 16),
(324, 7, 1, 11, 15.00, 12, 16),
(325, 6, 1, 11, 30.00, 11, 16),
(322, 9, 1, 11, 15.00, 14, 16),
(321, 10, 1, 11, 15.00, 15, 16),
(320, 11, 1, 11, 15.00, 16, 16),
(319, 12, 1, 11, 30.00, 17, 16),
(318, 13, 1, 11, 40.00, 18, 16),
(317, 14, 1, 11, 15.00, 19, 16),
(316, 15, 1, 11, 15.00, 20, 16),
(315, 16, 1, 11, 15.00, 21, 16),
(314, 17, 1, 11, 15.00, 22, 16),
(313, 18, 1, 11, 45.00, 23, 16),
(312, 19, 1, 11, 45.00, 24, 16),
(311, 20, 1, 11, 45.00, 25, 16),
(310, 21, 1, 11, 15.00, 26, 16),
(309, 22, 1, 11, 60.00, 27, 16),
(308, 23, 1, 11, 25.00, 28, 16),
(307, 24, 1, 11, 15.00, 29, 16),
(306, 25, 1, 11, 15.00, 30, 16),
(331, 26, 2, 11, 100.00, 3, 16),
(330, 26, 3, 11, 0.00, 4, 16),
(326, 5, 1, 11, 15.00, 10, 17),
(287, 0, 1, 11, 50.00, 49, 17),
(329, 2, 1, 11, 15.00, 7, 17),
(332, 26, 1, 11, 50.00, 2, 17),
(286, 0, 2, 11, 100.00, 50, 17),
(285, 0, 3, 11, 0.00, 51, 17),
(327, 4, 1, 11, 15.00, 9, 17),
(328, 3, 1, 11, 15.00, 8, 17),
(323, 8, 1, 11, 40.00, 13, 17),
(324, 7, 1, 11, 15.00, 12, 17),
(325, 6, 1, 11, 30.00, 11, 17),
(322, 9, 1, 11, 15.00, 14, 17),
(321, 10, 1, 11, 15.00, 15, 17),
(320, 11, 1, 11, 15.00, 16, 17),
(319, 12, 1, 11, 30.00, 17, 17),
(318, 13, 1, 11, 40.00, 18, 17),
(317, 14, 1, 11, 15.00, 19, 17),
(316, 15, 1, 11, 15.00, 20, 17),
(315, 16, 1, 11, 15.00, 21, 17),
(314, 17, 1, 11, 15.00, 22, 17),
(313, 18, 1, 11, 45.00, 23, 17),
(312, 19, 1, 11, 45.00, 24, 17),
(311, 20, 1, 11, 45.00, 25, 17),
(310, 21, 1, 11, 15.00, 26, 17),
(309, 22, 1, 11, 60.00, 27, 17),
(308, 23, 1, 11, 25.00, 28, 17),
(307, 24, 1, 11, 15.00, 29, 17),
(306, 25, 1, 11, 15.00, 30, 17),
(331, 26, 2, 11, 100.00, 3, 17),
(330, 26, 3, 11, 0.00, 4, 17);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax`
-- 

CREATE TABLE `lumonata_tax` (
  `ltax_id` int(11) NOT NULL auto_increment,
  `taxes_on_shipping` tinyint(4) NOT NULL,
  `price_include_taxes` tinyint(4) NOT NULL,
  `lorder` int(11) NOT NULL,
  `lcreated_by` varchar(50) NOT NULL,
  `lcreated_date` datetime NOT NULL,
  `lusername` varchar(50) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`ltax_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `lumonata_tax`
-- 

INSERT INTO `lumonata_tax` (`ltax_id`, `taxes_on_shipping`, `price_include_taxes`, `lorder`, `lcreated_by`, `lcreated_date`, `lusername`, `ldlu`) VALUES 
(1, 1, 0, 18, '1', '2011-10-24 14:40:51', '1', '2011-10-24 18:03:02');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax_detail`
-- 

CREATE TABLE `lumonata_tax_detail` (
  `ltax_detail_id` int(11) NOT NULL auto_increment,
  `ltax_id` int(11) NOT NULL,
  `lcountry_id` int(11) NOT NULL,
  `ltax_groups_ID` int(11) NOT NULL,
  `ltax_charge` decimal(10,2) NOT NULL,
  `lorder` int(11) NOT NULL,
  PRIMARY KEY  (`ltax_detail_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=295 ;

-- 
-- Dumping data for table `lumonata_tax_detail`
-- 

INSERT INTO `lumonata_tax_detail` (`ltax_detail_id`, `ltax_id`, `lcountry_id`, `ltax_groups_ID`, `ltax_charge`, `lorder`) VALUES 
(289, 1, 1, 70, 1.00, 5),
(290, 1, 1, 71, 2.00, 4),
(291, 1, 2, 70, 1.00, 3),
(292, 1, 2, 71, 2.00, 2),
(293, 1, 3, 70, 4.00, 1),
(294, 1, 3, 71, 2.00, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_tax_groups`
-- 

CREATE TABLE `lumonata_tax_groups` (
  `ltax_groups_ID` int(12) NOT NULL auto_increment,
  `ldescription` text NOT NULL,
  `ldefault` int(1) NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` varchar(100) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` varchar(100) NOT NULL,
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`ltax_groups_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

-- 
-- Dumping data for table `lumonata_tax_groups`
-- 

INSERT INTO `lumonata_tax_groups` (`ltax_groups_ID`, `ldescription`, `ldefault`, `lorder`, `lpost_by`, `lpost_date`, `lupdated_by`, `ldlu`) VALUES 
(71, 'Goverment Rate', 0, 1, '1', '2011-10-21 15:54:23', '1', '2011-10-21 15:54:23'),
(70, 'default', 1, 2, '1', '2011-10-21 14:46:43', '1', '2011-10-21 14:46:43');

-- --------------------------------------------------------

-- 
-- Table structure for table `lumonata_users`
-- 

CREATE TABLE `lumonata_users` (
  `luser_id` bigint(20) NOT NULL auto_increment,
  `lusername` varchar(200) character set utf8 NOT NULL,
  `ldisplay_name` varchar(200) character set utf8 NOT NULL,
  `lpassword` varchar(200) character set utf8 NOT NULL,
  `lemail` varchar(200) character set utf8 NOT NULL,
  `lregistration_date` datetime NOT NULL,
  `luser_type` varchar(50) character set utf8 NOT NULL,
  `lactivation_key` varchar(200) character set utf8 NOT NULL,
  `lavatar` varchar(200) character set utf8 NOT NULL,
  `lsex` int(11) NOT NULL COMMENT '1=male,2=female',
  `lbirthday` date NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime NOT NULL,
  PRIMARY KEY  (`luser_id`),
  KEY `username` (`lusername`),
  KEY `display_name` (`ldisplay_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `lumonata_users`
-- 

INSERT INTO `lumonata_users` (`luser_id`, `lusername`, `ldisplay_name`, `lpassword`, `lemail`, `lregistration_date`, `luser_type`, `lactivation_key`, `lavatar`, `lsex`, `lbirthday`, `lstatus`, `ldlu`) VALUES 
(1, 'admin', 'angelina', 'fcea920f7412b5da7be0cf42b8c93759', 'request@arunna.com', '0000-00-00 00:00:00', 'administrator', '', 'admin-1.jpg|admin-2.jpg|admin-3.jpg', 2, '2011-03-19', 1, '2011-09-01 15:14:27');
