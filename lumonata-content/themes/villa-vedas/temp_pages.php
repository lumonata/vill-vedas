<?php 
//echo is_page().'#'.is_exist_page().'##';
if(is_page() && is_exist_page()){	
	$id =  post_to_id();
	if($cek_url[0]=='location') $thecontent = content_page_location();
	else if($cek_url[0]=='reservation') {
		$full_background = full_background($id,'fixbg');
		$thecontent = reservation_page();	
	}else if($cek_url[0]=='contact-us'){
		$full_background = full_background($id,'fixbg');
		$thecontent = simple_contact_us();
	}else if($cek_url[0]=='success-inquiry'){
		$full_background = full_background($id,'fixbg');
		$thecontent = page_like_category_article($cek_url[0]);
	}else if($cek_url[0]=='reservation-form'){
		$full_background = full_background($id,'fixbg');
		$thecontent = simple_reservation_form();
	}else if($cek_url[0]=='gallery'){
		$thecontent = front_gallery();
	}else $thecontent = content_page();
}

function is_exist_page(){
	$id =  post_to_id();
	if(!empty($id)){
		$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_type='pages' and larticle_status='publish'",'array');	
		if(empty($dt)) return false;
		else return true;	
	}else return false;
}

function content_page(){
	$id =  post_to_id();
	$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_type='pages' and larticle_status='publish'",'array');	
	add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
	return content_detail_article($id);	
}

function page_like_category_article($sef){
	$id =  post_to_id();
	$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_type='pages' and larticle_status='publish'",'array');	
	if(!empty($dt)){
		add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
		set_template("page-like-category-article.html",'tmp_category');		
		add_block('category_article','Cat_artc','tmp_category');		
		add_variable('title_category',strtoupper($dt['larticle_title']));
		add_variable('detail_category',$dt['larticle_content']);
		parse_template('category_article','Cat_artc',false);		
		return return_template('tmp_category');	
	}	
}

function content_page_location(){
	$id =  post_to_id();
	$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_type='pages' and larticle_status='publish'",'array');	
	add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
	
	set_template("location.html",'tmp_loc');
	add_block('location','loc','tmp_loc');
	
	parse_template('location','loc',false);	
	return return_template('tmp_loc');
}

?>