<?php 
global $actions;
//print_r($cek_url);
if(!empty($cek_url[0]) && $cek_url[0]=='payment' && !empty($cek_url[1]) ){
	$dtUrl = json_decode(base64_decode($cek_url[1]));
	//exist booking
	$dbk = data_tabel("lumonata_accommodation_booking", "where lbook_id=".$dtUrl->book_id."",'array');	
	//print_r($dbk);
	if(!empty($dbk)){
		$time_now = time();	
		//echo $dtUrl->val_date .">". $time_now;
		if($dtUrl->val_date > $time_now){//valid date
			if($dtUrl->type=='dp')$thecontent = dp_payment_form($dtUrl);
			if($dtUrl->type=='fp')$thecontent = fp_payment_form($dtUrl);
			
		}else {
			$thecontent = expired_payment_page();
		}		
	}	
}

if(!empty($cek_url[0]) && $cek_url[0]=='success-payment' && !empty($cek_url[1]) ){
	$dtUrl = json_decode(base64_decode($cek_url[1]));
	$dbk = data_tabel("lumonata_accommodation_booking", "where lbook_id=".$dtUrl->book_id."",'array');	
	if(!empty($dbk)){
		//print_r($dtUrl);
		$type_payment = $dtUrl->type;
		$dtDepos = json_decode($dbk['ldeposit']);
		//echo "$type_payment##".$dtDepos->deposit;
		if($dtDepos->deposit==100 && $type_payment=='fp') $sef = 'success-fp';
		if($dtDepos->deposit<100 && $type_payment=='dp') $sef = 'success-dp';
		if($dtDepos->deposit<100 && $type_payment=='fp') $sef = 'success-bp';
		//$sef = ($dtUrl->type =='dp'? 'success-dp':'success-fp');
		$dt = data_tabel("lumonata_articles", "where lsef='$sef' and larticle_type='pages' and larticle_status='publish'",'array');
		add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  	
		$full_background = full_background($dt['larticle_id'],'fixbg');
		$thecontent = custom_detail_page_content($dt);		
	}	
}


if(!empty($cek_url[0]) && $cek_url[0]=='payment-notify' && !empty($cek_url[1]) ){
	$thecontent = paypal_notify();
}


function expired_payment_page(){
	$contact_us = 'http://'.SITE_URL.'/contact-us/';
	add_variable('meta_title', 'Expired Link - '.trim(web_title())); 
	return "<h1>Expired Link</h1>
			<div class=\"border\"></div>	
			<div class=\"content\">
				<p>Your link has expired. Please contact <a href=\"$contact_us\">us</a> for get new link payment.</p>			
			</div>";
}


function dp_payment_form($dt){
	global $db;
	$code = 'USD';
	set_template(ROOT_PATH."/lumonata-plugins/reservation/dp-fp-payment.html",'tmp_payment');
	add_block('dpBlock','dpb','tmp_payment');
	
	$q = $db->prepare_query("select c.larticle_title , b.ltotal sub_total,
							a.lbook_id, a.lcheck_in, a.lcheck_out, a.ladult, a.lchild, a.lspecial_note, a.ltotal grand_total, a.ldeposit  
							from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b on a.lbook_id=b.lbook_id
							inner join lumonata_articles c on b.larticle_id=c.larticle_id
							where a.lbook_id=%d and c.larticle_type=%s",$dt->book_id,'villas');
	$r = $db->do_query($q);
	$book_date = 0;$villa_name='';$price_per_day=0;$adult = 0;
	
	while($dbk = $db->fetch_array($r)){
		$book_date = date('d M Y',$dbk['lbook_id']);
		$days = ($dbk['lcheck_out'] - $dbk['lcheck_in'])/86400;
		$sub_total = $dbk['sub_total'];
		$price_per_day = $sub_total/$days;
		
		$villa_name = ($villa_name!=''? ', ': '').$dbk['larticle_title'];
		add_variable('adult',$dbk['ladult']);
		add_variable('child',$dbk['lchild']);
		add_variable('special_note',nl2br($dbk['lspecial_note']));
		add_variable('check_in', date('d M Y',$dbk['lcheck_in']));
		add_variable('check_out', date('d M Y',$dbk['lcheck_out']));
		add_variable('night',$days.' night'.($days>1?'s':''));
		add_variable('total',$code.' '.number_format($dbk['grand_total'],2));
		$dps = json_decode($dbk['ldeposit']);
		add_variable('deposite',$code.' '.number_format($dps->rate,2));
		add_variable('term_url','http://'.SITE_URL.'/terms-conditions/');
		add_variable('book_id',$dt->book_id);
		add_variable('amount',$dps->rate);
		
		add_variable('title','VILLA IS AVAILABLE');
		add_variable('desc_top',desc_top_dp($villa_name));
		add_variable('deposite_label',($dps->deposit<100? 'Required Deposit':'Full Required'));
		
		$url_array  = array();
		$url_array['book_id'] 	= $dt->book_id;
		$url_array['type'] 		= ($dps->deposit==100? 'fp':'dp');
		$special_url = base64_encode(json_encode($url_array));
		add_variable('special_url',$special_url);
		
	}
	add_variable('book_date',$book_date);
	add_variable('villa_name',$villa_name);
	add_variable('price_per_day',$code.' '.number_format($price_per_day,2));
	add_variable('meta_title', ($dps->deposit<100? 'Deposit Payment':'Full Payment').' - '.trim(web_title()));  
	
	
	$lap = data_tabel("lumonata_meta_data", "where lmeta_name='link_action_paypal' and lapp_name='global_setting'",'array');
	add_variable('action_paypal',$lap['lmeta_value']);
	$ep = data_tabel("lumonata_meta_data", "where lmeta_name='email_paypal' and lapp_name='global_setting'",'array');	
	add_variable('email_paypal',$ep['lmeta_value']);	
	
	parse_template('dpBlock','dpb',false);	
	return return_template('tmp_payment');	
}


function desc_top_dp($villa_name){
	$desc = "<p class=\"center\"><strong>$villa_name</strong> that you are booking on <strong>{book_date}</strong> is available.&nbsp;To confirm your booking please continue with the required payment below.&nbsp;</p>
     <p class=\"center\">Payment are securely processed via Paypal. Should you need assistance in other method of payment, please contact us to <a class=\"underline\" href=\"mailto:reservation@villavedas.com\">reservation@villavedas.com</a></p>";	
	 return $desc;
}

/*function success_dp_page($sef){
	$dt = data_tabel("lumonata_articles", "where lsef='$sef' and larticle_type='pages' and larticle_status='publish'",'array');
	//print_r($dt);
	//add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
	return content_detail_custom_page($dt['larticle_id']);
}*/

function custom_detail_page_content($dt, $with_layer=false){
	set_template("just-text-image.html",'tmp_jti');
	add_block('just_text_image','jti','tmp_jti');
	
	
	add_variable('with_layer',($with_layer? '':'style="display:none;"'));
	
	add_variable('have_menu_top','no-top-menu'); 
	add_variable('title',strtoupper($dt['larticle_title']));
	add_variable('class_page',$dt['lsef'].'-page');
	add_variable('detail_content',$dt['larticle_content']);
	parse_template('just_text_image','jti',true);
	return return_template('tmp_jti');	
}



/*function content_detail_custom_page($id){
	$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_status='publish'",'array');
	if(!empty($dt)){
		add_variable('meta_title', trim(web_title()));  
		
		
		set_template("detail-article.html",'tmp_detail');
		add_block('detail_article','det_artc','tmp_detail');echo 'dasd';
		if(is_have_menu_right_top()) add_variable('have_menu_top','have-menu-top');
		else add_variable('have_menu_top','no-top-menu'); 
		//$menu_top = menu_top_right($dt['larticle_title']);
		
		add_variable('title',strtoupper($dt['larticle_title']));
		add_variable('class_page',$dt['lsef'].'-page');
		
		add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
		add_variable('detail_content',$dt['larticle_content']);
		
		if(is_show_thumb_article()){
			add_variable('thumb_article',set_thumb_article());
			//set_thumb_article();
		}	
		
		parse_template('detail_article','det_artc',false);	
		return return_template('tmp_detail');		
	}
	
}*/


function fp_payment_form($dt){
	global $db;
	$code = 'USD';
	set_template(ROOT_PATH."/lumonata-plugins/reservation/dp-fp-payment.html",'tmp_payment');
	add_block('dpBlock','fpb','tmp_payment');
	
	$q = $db->prepare_query("select c.larticle_title , b.ltotal sub_total,
							a.lbook_id, a.lcheck_in, a.lcheck_out, a.ladult, a.lchild, a.lspecial_note, a.ltotal grand_total, a.ldeposit  
							from lumonata_accommodation_booking a inner join lumonata_accommodation_booking_detail b on a.lbook_id=b.lbook_id
							inner join lumonata_articles c on b.larticle_id=c.larticle_id
							where a.lbook_id=%d and c.larticle_type=%s",$dt->book_id,'villas');
	$r = $db->do_query($q);
	$book_date = 0;$villa_name='';$price_per_day=0;$adult = 0;
	
	$balance = 0;
	while($dbk = $db->fetch_array($r)){
		$book_date = date('d M Y',$dbk['lbook_id']);
		$days = ($dbk['lcheck_out'] - $dbk['lcheck_in'])/86400;
		$sub_total = $dbk['sub_total'];
		$price_per_day = $sub_total/$days;
		
		$villa_name = ($villa_name!=''? ', ': '').$dbk['larticle_title'];
		add_variable('adult',$dbk['ladult']);
		add_variable('child',$dbk['lchild']);
		add_variable('special_note',nl2br($dbk['lspecial_note']));
		add_variable('check_in', date('d M Y',$dbk['lcheck_in']));
		add_variable('check_out', date('d M Y',$dbk['lcheck_out']));
		add_variable('night',$days.' night'.($days>1?'s':''));
		add_variable('total',$code.' '.number_format($dbk['grand_total'],2));
		$dps = json_decode($dbk['ldeposit']);
		add_variable('deposite',$code.' '.number_format($dps->rate,2));
		add_variable('term_url','http://'.SITE_URL.'/terms-conditions/');
		add_variable('book_id',$dt->book_id);
		
		
		$url_array  = array();
		$url_array['book_id'] 	= $dt->book_id;
		$url_array['type'] 		= 'fp';
		$special_url = base64_encode(json_encode($url_array));
		add_variable('special_url',$special_url);
		
		$balance = $dbk['grand_total']-$dps->rate;
		add_variable('amount',$balance);
	}
	add_variable('book_date',$book_date);
	add_variable('villa_name',$villa_name);
	add_variable('price_per_day',$code.' '.number_format($price_per_day,2));
	add_variable('meta_title', 'Balance Payment - '.trim(web_title()));  
	
	add_variable('title','BALANCE PAYMENT');
	//add_variable('desc_top',desc_top_dp($villa_name));
	add_variable('deposite_label','Deposit');
	$balance_txt = $code.' '.number_format($balance,2);
	add_variable('balance_rate',temp_balance_cont($balance_txt));
	
	$lap = data_tabel("lumonata_meta_data", "where lmeta_name='link_action_paypal' and lapp_name='global_setting'",'array');
	add_variable('action_paypal',$lap['lmeta_value']);
	$ep = data_tabel("lumonata_meta_data", "where lmeta_name='email_paypal' and lapp_name='global_setting'",'array');	
	add_variable('email_paypal',$ep['lmeta_value']);	
	
	parse_template('dpBlock','fpb',false);	
	return return_template('tmp_payment');
}

function temp_balance_cont($balance){
	$temp= "<div class=\"dbf gray\">
        	<div class=\"label\">Balance Payment</div>
            <div class=\"value\">$balance</div>
            <div class=\"clear\"></div>
        </div>";	
	return $temp;	
}

?>