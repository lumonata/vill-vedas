<?php 
	global $actions;
	if(!empty($cek_url[0]) && $cek_url[0]=='articles'){//set articles content
		if(is_details()){
			$thecontent = detail_article();
		}else if(is_category()){//for category article
			$thecontent = category_article($cek_url[1]);
			$rule_id = get_category_article_id($cek_url[1]);
			$full_background = full_background($rule_id);
		}
	}
	
	
	function category_article($category='uncategorized'){
		//detail category
		$url = cek_url();
		$data_category = data_tabel("lumonata_rules","where lsef='".$url[1]."' and lgroup ='articles'",'array');
		if(!empty($data_category)){
			set_template("category-article.html",'tmp_category');
			add_block('category_article','Cat_artc','tmp_category');
			
			if(is_have_menu_right_top()){
				$menu_top = menu_top_right();
				add_variable('menu_top_right',$menu_top);
				add_variable('have_top_menu','have-menu-top');
			}
			
			$image = get_page_header_images($data_category['lrule_id']);
			if(!empty($image))add_variable('category_background',$image[0]);
			
			add_variable('title_category',strtoupper($data_category['lname']));
			add_variable('detail_category',$data_category['ldescription']);
			
			$data_article_category = article_category();
			if(!empty($data_article_category)){
				$detail_article = content_detail_article($data_article_category['larticle_id']);	
				add_variable('detail_article',$detail_article);	
			}
			
			parse_template('category_article','Cat_artc',false);		
			return return_template('tmp_category');	
		}		
	}
	
	function get_category_article_id($sef){
		$data_category = data_tabel("lumonata_rules","where lsef='$sef' and lgroup ='articles'",'array');
		if(!empty($data_category)) return $data_category['lrule_id'];
	}
	
	function menu_top_right($selected_article_sef=''){
		global $db;
		$url = cek_url();
		$data_currcat = data_tabel("lumonata_rules","where lsef='".$url[1]."' and lgroup ='".$url[0]."'",'array');
		$title_category = strtoupper($data_currcat['lname']);
		$type = $url[0];
		$group = $url[0];
		$rule_sef = $url[1];
		
		$data_article = data_list_article_category($type,$rule_sef,$group);
		$list_menu="";
		while($d = $db->fetch_array($data_article)){
			$id_article = $d['larticle_id'];
			$name_article = strtoupper($d['larticle_title']);
			$sef_article = $d['lsef'];
			if($name_article !=$title_category){
				$url_article = permalink($id_article);
				if($selected_article_sef == $sef_article){$select_mn = 'selected';}
				else $select_mn = '';
				$list_menu .= "<li  class=\"$select_mn\" ><a href=\"$url_article\">$name_article</a></li>";
			}
		}
		
		if(empty($selected_article_sef))$select_cat = 'selected';
		else $select_cat = '';
		
		$url_default  = 'http://'.SITE_URL.'/'.$type.'/'.$rule_sef.'/';
		$temp = "<div id=\"menu-top-right\">
					<ul>
						<li class=\"$select_cat\"><a href=\"$url_default\">$title_category</a></li>
						$list_menu
					</ul>
				</div> ";
		return $temp;		
		
	}
	
	
	function data_list_article_category($type,$rule_sef,$group){
		global $db;
		$q = $db->prepare_query("SELECT a.* FROM lumonata_articles a, lumonata_rule_relationship b , lumonata_rules c
								WHERE a.larticle_id = b.lapp_id and b.lrule_id = c.lrule_id and
								c.lsef = %s and lgroup = %s and a.larticle_type=%s and a.larticle_status=%s order by lorder",
								$rule_sef,$group,$type,'publish');				
		$r = $db->do_query($q);							
		return $r;
	}
	
	function detail_article(){
		if(is_have_menu_right_top()){
			$id =  post_to_id();
			$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_status='publish'",'array');
			$menu_top = menu_top_right($dt['lsef']);
			add_variable('menu_top_right',$menu_top);	
		} 
		$id =  post_to_id();
		return content_detail_article($id);		
	}
	
	
	function content_detail_article($id){
		$dt = data_tabel("lumonata_articles", "where larticle_id=$id and larticle_status='publish'",'array');
		if(!empty($dt)){
			add_variable('meta_title', trim(web_title()));  
			set_template("detail-article.html",'tmp_detail');
			add_block('detail_article','det_artc','tmp_detail');
			if(is_have_menu_right_top()) add_variable('have_menu_top','have-menu-top');
			else add_variable('have_menu_top','no-top-menu'); 
			//$menu_top = menu_top_right($dt['larticle_title']);
			
			add_variable('title',strtoupper($dt['larticle_title']));
			/*if($dt['larticle_title']=='Floor Plan'){
				add_variable('margin_top','style="padding-top:120px;"');
				add_variable('hidden','style="display:none;"');
			} */
			add_variable('class_page',$dt['lsef'].'-page');
			
			add_variable('meta_title', $dt['larticle_title'].' - '.trim(web_title()));  
			add_variable('detail_content',$dt['larticle_content']);
			
			if(is_show_thumb_article()){
				add_variable('thumb_article',set_thumb_article());
				//set_thumb_article();
			}	
			
			parse_template('detail_article','det_artc',false);	
			return return_template('tmp_detail');		
		}
		
	}
	
	function is_have_menu_right_top(){
		$url = cek_url();
		$rule_sef = (isset($url[1])? $url[1]:'');
		if($rule_sef=='about-us')return true;
		else return false;
	}
	
	function is_show_thumb_article(){
		$url = cek_url();
		$rule_sef = (isset($url[1])? $url[1]:'');
		if($rule_sef=='facilities')return true;
		else return false;
	}
	
	function article_category(){
		global $db;
		$url = cek_url();
		$type = $url[0];
		$group = $url[0];
		$rule_sef = $url[1];
		
		global $db;
		$q = $db->prepare_query("SELECT a.* FROM lumonata_articles a, lumonata_rule_relationship b , lumonata_rules c
								WHERE a.larticle_id = b.lapp_id and b.lrule_id = c.lrule_id and
								c.lsef = %s and lgroup = %s and a.larticle_type=%s and a.larticle_status=%s and a.lsef = %s order by lorder",
								$rule_sef,$group,$type,'publish',$rule_sef);								
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);
		return $d;
	}
	
	function set_thumb_article(){
		global $db;
		$url = cek_url();
		$type = $url[0];
		$group = $url[0];
		$rule_sef = $url[1];
		
		set_template("thumb-article.html",'tmp_thumb_detail');
		add_block('thumb_article_block','thmb_block_artc','tmp_thumb_detail');
		add_block('thumb_article','thmb_det_artc','tmp_thumb_detail');
		
		$data_article = data_list_article_category($type,$rule_sef,$group);
		$image = array();
		while($d = $db->fetch_array($data_article)){
			$sef = $d['lsef'];
			if($sef!=$rule_sef){
				$title = $d['larticle_title'];
				$desc = $d['larticle_content'];
				//echo $d['larticle_id'].'#';
				$image = get_page_header_images($d['larticle_id']);
				if(!empty($image))add_variable('image_thumb',$image[0]);
				else add_variable('image_thumb','');
				add_variable('thumb_title',$title);
				add_variable('thumb_desc',$desc);
				
				parse_template('thumb_article_block','thmb_block_artc',true);
			}
		}
			
		$thumb = return_template('thumb_article','thmb_det_artc',false);
		return $thumb;
	}
	
?>