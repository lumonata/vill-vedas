jQuery(document).ready(function(e) {
	set_menu_mobile();
	set_left_content();
	set_available_scroll() ;

   	jQuery(window).load(function(){
	   jQuery('#full-background, .layer-top, .layer-bottom, #layout-top.contact-us').animate({'opacity':1},1000);
	   set_right_content();
	});
	
	jQuery(window).resize(function(){
		if(jQuery('#location').length > 0) initialize_map();	
		set_left_content();
		//set_right_content();
	});
	
	jQuery(window).bind("load resize", function() {
		set_right_content();
	});
	
	if(jQuery('#location').length > 0) initialize_map();

	gallery_event()
});

function gallery_event(){
	if(jQuery('#list-gallery').length==0) return;

	var hlg = jQuery('#list-gallery').height();
	var hlgn = jQuery('#list-gallery-nav').height();

	var ng = jQuery('#list-gallery .item-gallery').length;
	jQuery('#all-num-slide').text(ng)

	//jQuery('.item-gallery').css('height',hlg+'px');
	//jQuery('.item-gallery-nav').css('height',hlgn+'px');


	jQuery('#list-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: '#list-gallery-nav',
		
	});
	jQuery('#list-gallery-nav').slick({
		slidesToShow: 9,
		slidesToScroll: 1,
		asNavFor: '#list-gallery',
		dots: false,
		centerMode: false,
		focusOnSelect: true,
		lazyLoad: 'ondemand',
		arrows: false,
	});
}

var iScrollPos = 0;
var haveScroll = 0;
//function event_scroll_window(){
	//console.log('masini');
var timeoutId;
	
jQuery(window).scroll(function () {
	if(timeoutId ){
         clearTimeout(timeoutId );  
	}
	timeoutId = setTimeout(function(){
			var w = jQuery(window).width();	
			var h = jQuery(window).height();
			
			if(jQuery('.bottom-read-more').length>0 && jQuery('#layout-bottom').length>0 && w>480){
				var iCurScrollPos = jQuery(this).scrollTop();
				var media_screen = jQuery(window).height();
				var right_pos = jQuery('#right').position();
				var top_bottom_right = right_pos.top * 2;
				var true_right_pos = media_screen - top_bottom_right;
				
				console.log(iCurScrollPos+' > '+iScrollPos+' && '+iCurScrollPos+' < '+true_right_pos);
				if(iCurScrollPos > iScrollPos &&  iCurScrollPos < true_right_pos) {
					//Scrolling Down
					console.log('scrolling down');
					if(haveScroll==0)jQuery('html, body').animate({ scrollTop: true_right_pos}, 700);
					haveScroll = 1;		
				}else if(iCurScrollPos < iScrollPos &&  iCurScrollPos < true_right_pos){
					haveScroll = 0;
				}else {
				   //Scrolling Up
				}
				iScrollPos = iCurScrollPos;
			}
			
			else if(jQuery('.bottom-read-more').length>0 && jQuery('#layout-bottom').length>0 && w<480){
				var iCurScrollPos = jQuery(this).scrollTop();
				var media_screen = jQuery(window).height();
				var right_pos = jQuery('#right').position();
				var top_bottom_right = right_pos.top * 2;
				var true_right_pos = media_screen - top_bottom_right;
				
				console.log(iCurScrollPos+' > '+iScrollPos+' && '+iCurScrollPos+' < '+true_right_pos);
				if(iCurScrollPos > iScrollPos &&  iCurScrollPos < true_right_pos) {/*Scrolling Down*/
					if(haveScroll==0)jQuery('a.bottom-read-more').click();console.log('scrolling down');
					haveScroll = 1;	
				}else if(iCurScrollPos < iScrollPos &&  iCurScrollPos < true_right_pos){
					haveScroll = 0;
				}else { }
				iScrollPos = iCurScrollPos;
			}
	}, 100);
	
	
});
//}

function set_menu_mobile(){
	jQuery('#horizontal-header .top-menu').click(function(){
		jQuery(this).parent().find('ul').slideToggle('slow');
	});	
	jQuery('#side-bar .top-menu').click(function(){
		if(jQuery(this).hasClass('show-menu')){
			jQuery(this).removeClass('show-menu');
			jQuery("#left").css('z-index',1);
		}else{
			jQuery(this).addClass('show-menu');
			jQuery("#left").css('z-index',2);
		}
		jQuery(this).parent().find('ul').toggle('slow');
	});	
	/*var nav = responsiveNav(".nav-collapse", {animate: true, transition: 284,label: "MENU", insert: "before",customToggle: "", closeOnNavClick: false,		  openPos: "relative", navClass: "nav-collapse", navActiveClass: "js-nav-active"});*/
}

function set_left_content(){
	var w = jQuery(window).width();	
	var h = jQuery(window).height();
	
	if(w>640 && h<=680){
		jQuery('#menu-top, #container-bottom-sb').addClass('mmh-mode');
		jQuery('#mobile-menu-height, #mobile-social-height').fadeIn(80);
		jQuery('form#fcontact-us .field-cont .full textarea#message').css('height','100px');
	}else{
		jQuery('#menu-top, #container-bottom-sb').removeClass('mmh-mode');
		jQuery('#mobile-menu-height, #mobile-social-height').fadeOut(80);
		jQuery('form#fcontact-us .field-cont .full textarea#message').removeAttr('style');
	} 
}

function set_right_content(){
	var w = jQuery(window).width();	
	if(jQuery('#layout-top').length>0 && jQuery('#layout-bottom').length>0){
		var h = jQuery(window).height();
		
		if(w>640) var bf = 40;	
		else var bf = 20;
		
		var th = h-bf;
		
		var ObjCss = {'margin-top':th+'px'};
		if(w<=480){
			th = jQuery('#right').height();
			ObjCss = {'margin-top':th+'px'};	
		}
		jQuery('#layout-bottom').css(ObjCss);	
		//set min height content detail
		var content_detail = jQuery('#layout-bottom').height();
		
		
		if(content_detail < th) jQuery('#layout-bottom').css('height',th+'px');			
		
		
		
		
		
		//set event click & scroll
		jQuery('.bottom-read-more').unbind('click');
		jQuery('.bottom-read-more').click(function(e){
			jQuery("html,body").animate({scrollTop:th}, 700);
			e.preventDefault();
		});
	}
	
	if(jQuery('.no-layout-top').length>0 || jQuery('#layout-top').length==0){
		
		var h = jQuery(window).height();
		if(w>640) var bf = 40;	
		else var bf = 20;
		
		var th = h-bf;
		
		var content_detail = jQuery('#layout-bottom').height();
		if(content_detail < th) jQuery('#layout-bottom').css('height',th+'px');		
	}
	//console.log(w);
	//console.log(jQuery('#your-stay').width());
	var h_desc_stay = jQuery('#your-stay .desc-stay').height();
	var h_block_info = jQuery('#your-stay .block-info').height();
	if(w>398 && w<=480){
		//console.log('w>415 && w<=480');
		var h_your_stay = (h_desc_stay>h_block_info? h_desc_stay: h_block_info)+20;			
	}else if(w<398){
		//console.log('w<415');
		var h_your_stay = 	 h_desc_stay+h_block_info+20;	
	}
	//console.log(h_your_stay)	;
	if(jQuery('#sec-book-info').length>0 && w <=480 ){	
		jQuery('#your-stay').css('height',h_your_stay+'px');
	}else jQuery('#your-stay').removeAttr('style');
	if(jQuery('#result-availability').length>0 && w<=480){
		jQuery('#result-availability').css('top',h_your_stay+'px');
	}else jQuery('#result-availability').removeAttr('style');
	
}

function set_available_scroll(){
	if(jQuery('#list-available-villa').length>0){
		var w = jQuery(window).width();	
		var h = jQuery(window).height();
	}
}

function initialize_map() {
	jQuery("#location").html('').removeAttr('style');
	var mapOptions = {
	  center: new google.maps.LatLng(-8.597959, 115.074180),
	  zoom: 14
	};
	var map = new google.maps.Map(document.getElementById("location"),mapOptions);
	
   
	var myLatLng = new google.maps.LatLng(-8.597959, 115.074180);
	var beachMarker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title:'Villa Vedas'
	});
  
}

