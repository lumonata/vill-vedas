jQuery(document).ready(function () {
    jQuery(".icon").click(function () {
		var w = jQuery(window).width();
		jQuery('body').removeClass('no-scroll');
		jQuery(".full-menu-cont").removeClass('full-menu-cont-menuH');
		if(jQuery(this).hasClass('mobMenH')) jQuery(".full-menu-cont").addClass('full-menu-cont-menuH');
		if(jQuery('.top-animate').length==0)jQuery('body').addClass('no-scroll');
		
        jQuery(".full-menu-cont").fadeToggle(500);
        jQuery(".top-menu").toggleClass("top-animate");
        jQuery(".mid-menu").toggleClass("mid-animate");
        jQuery(".bottom-menu").toggleClass("bottom-animate");
    });
});
