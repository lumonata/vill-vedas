<?php
/*
    Plugin Name: Simple Contact Us
   	Plugin URL: http://lumonata.com/
    Description: This plugin is use to send a message.
    Author: Yana
    Author URL: http://lumonata.com/
    Version: 1.0.1
    
*/

add_privileges('administrator', 'contact_us', 'insert');
add_privileges('administrator', 'contact_us', 'update');
add_privileges('administrator', 'contact_us', 'delete');

add_apps_menu(array('contact_us'=>'Contact Us'));

add_actions("contact_us","plugin_contact_us_admin");

function plugin_contact_us_admin(){
	global $db;
	add_actions("header_elements","contact_us_admin_CSS");
	set_template(PLUGINS_PATH."/contact_us/template_admin.html",'the_contact_us_admin');
	add_block('Bcontact_us_admin','bcua','the_contact_us_admin');
	add_actions('section_title','Contact Us');
	
	add_variable('app_title','Contact Us');
	add_variable('save_changes_botton',save_changes_botton());
	//echo
	$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
	$r=$db->do_query($sql);
	$f=$db->fetch_array($r);
	$n=$db->num_rows($r);
	//echo
	$post_id = $f['larticle_id'];
	$article_type = $f['larticle_type'];
	$article_content =$f['larticle_content'];
	$article_brief =$f['larticle_brief'];  
	
	if (isset($_POST['save_changes'])){
		
		$alert_email = "<span class=\"validate_box validate_no\"><span>Invalid email address</span></span>";
		$alert_email2 = "<span class=\"validate_box validate_no\"><span>Email address required</span></span>";
		
		$reE = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
		
		
		$contact_us_email = $_POST['contact_us_email'];
		$contact_us_cc = $_POST['contact_us_cc'];
		$contact_us_bcc = $_POST['contact_us_bcc'];
		$contact_us_title = $_POST['contact_us_title'];
		$contact_us_video = $_POST['contact_us_video'];
		$contact_us_phone = $_POST['contact_us_phone'];
		$contact_us_phone_bali  =  $_POST['contact_us_phone_bali'];
		
		$error = 0;
		if (empty($contact_us_email)){
			add_variable('alert_contact_us_email',$alert_email2);
			$error ++;
		}else if(!eregi($reE, $contact_us_email)) {
			add_variable('alert_contact_us_email',$alert_email);
			$error ++;
		}
		
		if (!empty($contact_us_cc) and !eregi($reE, $contact_us_cc)){
			add_variable('alert_contact_us_cc',$alert_email);
			$error ++;
		}
		
		if (!empty($contact_us_bcc) and !eregi($reE, $contact_us_bcc)){
			add_variable('alert_contact_us_bcc',$alert_email);
			$error ++;
		}
		
		
		if ($error==0){
			$post_by = $_COOKIE['user_id'];
			$article_title = $_POST['article_title'];
			$article_content = $_POST['article_content'];
			$article_brief = $_POST['article_brief'];
			
			$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
			$r=$db->do_query($sql);
			$n=$db->num_rows($r);
			//$n = 0;
			if (empty($n)){
				$article_type = 'contact-us';
				$article_id = setCode("lumonata_articles","larticle_id");
				
				//echo 'M'.
				$str = "Insert Into lumonata_articles
				(larticle_id,larticle_title,larticle_content,larticle_status,larticle_type,
				lsef,lpost_by,lpost_date,larticle_brief) values
				(%d,%s,%s,%s,%s,
				%s,%s,%s,%s)";
				$query=$db->prepare_query($str,
				$article_id,$article_title,$article_content,'publish',$article_type,
				$article_type,$post_by,date("Y-m-d H:i:s"),$article_brief);
				$result=$db->do_query($query);
				
				if ($result){
					add_additional_field($article_id,'contact_us_email',$contact_us_email,$article_type);
					add_additional_field($article_id,'contact_us_cc',$contact_us_cc,$article_type);
					add_additional_field($article_id,'contact_us_bcc',$contact_us_bcc,$article_type);
					add_additional_field($article_id,'contact_us_title',$contact_us_title,$article_type);
					add_additional_field($article_id,'contact_us_phone',$contact_us_phone,$article_type);
					add_additional_field($article_id,'contact_us_phone_bali',$contact_us_phone_bali,$article_type);
					//add_additional_field($article_id,'contact_us_video',$contact_us_video,$article_type);
					
					$sql=$db->prepare_query("INSERT INTO
					lumonata_additional_fields
					VALUES(%d,%s,%s,%s)",
					$article_id,'contact_us_video',$contact_us_video,$article_type);
					$r=$db->do_query($sql);
					
					$alert="<div class=\"alert_green_form\">Contact us has been insert.</div>";
				}else{
					$alert="<div class=\"alert_green_form\">Contact us data failed to insert.</div>";
				}
			}else{
				$article_id = $post_id;
				$article_type = 'contact-us';
				//echo 'M'. 
				$query=$db->prepare_query("Update lumonata_articles Set
				larticle_content = %s,
				lupdated_by = %s,
				ldlu = %s,
				larticle_brief = %s
				Where larticle_id =%d",
				$article_content,$post_by,date("Y-m-d H:i:s"),$article_brief,$article_id);
				$result=$db->do_query($query);
				
				if ($result){
					edit_additional_field($article_id,'contact_us_email',$contact_us_email,$article_type);
					edit_additional_field($article_id,'contact_us_cc',$contact_us_cc,$article_type);
					edit_additional_field($article_id,'contact_us_bcc',$contact_us_bcc,$article_type);
					edit_additional_field($article_id,'contact_us_title',$contact_us_title,$article_type);
					edit_additional_field($article_id,'contact_us_phone',$contact_us_phone,$article_type);
					edit_additional_field($article_id,'contact_us_phone_bali',$contact_us_phone_bali,$article_type);
					//edit_additional_field($article_id,'contact_us_video',$contact_us_video,$article_type);
					
					//add_additional_field($article_id,'contact_us_video',$contact_us_video,$article_type);
					
					if(count_additional_field($article_id,"contact_us_video",$article_type)==0){
						$sql=$db->prepare_query("INSERT INTO
						lumonata_additional_fields
						VALUES(%d,%s,%s,%s)",
						$article_id,'contact_us_video',$contact_us_video,$article_type);
						$r=$db->do_query($sql);
					}else{
						$sql=$db->prepare_query("UPDATE
									lumonata_additional_fields
									SET lvalue=%s
									WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s",
									$contact_us_video,$article_id,"contact_us_video",$article_type);
						
						$r=$db->do_query($sql);
					}
					
					$alert="<div class=\"alert_green_form\">Contact us has been updated.</div>";
				}else{
					$alert="<div class=\"alert_green_form\">Contact us data failed to update.</div>";
				}
				
			}
		}
		
		add_variable('alert',$alert);
		add_variable('val_contact_us_email',$contact_us_email);
		add_variable('val_contact_us_cc',$contact_us_cc);
		add_variable('val_contact_us_bcc',$contact_us_bcc);
		add_variable('val_contact_title',$contact_us_title);
		add_variable('val_contact_us_video',$contact_us_video);
		add_variable('val_contact_us_phone',$contact_us_phone);
		add_variable('val_contact_us_phone_bali',$contact_us_phone_bali);
		add_variable('textarea',textarea('article_content',0,rem_slashes($article_content)));
		add_variable('textarea2',textarea('article_brief','article_brief-0',rem_slashes($article_brief)));
	}else{	
		$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
		$contact_us_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
		$contact_us_bcc = get_additional_field( $post_id, 'contact_us_bcc', $article_type);
		$contact_us_title = get_additional_field( $post_id, 'contact_us_title', $article_type);
		$contact_us_video = get_additional_field( $post_id, 'contact_us_video', $article_type);
		$contact_us_phone = get_additional_field( $post_id, 'contact_us_phone', $article_type);
		$contact_us_phone_bali = get_additional_field( $post_id, 'contact_us_phone_bali', $article_type);
		
		
		add_variable('val_contact_us_email',$contact_us_email);
		add_variable('val_contact_us_cc',$contact_us_cc);
		add_variable('val_contact_us_bcc',$contact_us_bcc);
		add_variable('val_contact_title',$contact_us_title);
		add_variable('val_contact_us_video',$contact_us_video);
		add_variable('val_contact_us_phone',$contact_us_phone);
		add_variable('val_contact_us_phone_bali',$contact_us_phone_bali);
		add_variable('textarea',textarea('article_content',0,rem_slashes($article_content)));
		add_variable('textarea2',textarea('article_brief','article_brief-0',rem_slashes($article_brief)));
		
	}
	
	
	parse_template('Bcontact_us_admin','bcua',true);
	return return_template('the_contact_us_admin');
		
}

function contact_us_admin_CSS(){
	$text = '<style>
			.textarea_button { display:none;}
			.list_edit {border: none;}
			.validate_box span {color: #333333;}
			</style>
			';
	return $text;
}

function simple_contact_us(){
	session_start();
	add_variable('script_contact',"<script src=\"http://".SITE_URL."/lumonata-plugins/contact_us/js/script.js\" type=\"text/javascript\"></script>");
	set_template(PLUGINS_PATH."/contact_us/template_simple_contact_us.html",'template_contact');
	add_block('contact_block','cont_block','template_contact');
	$id =  post_to_id();
	$title = get_article_title($id);
	add_variable('title',$title);
	add_variable('meta_title', $title.' - '.trim(web_title()));  
	return return_template('contact_block','cont_block',false);	
}


function simple_contact_us_send(){
	global $db;
	session_start();
	$name 		= $_POST['name'];
	$email 		= $_POST['email'];
	$subject 	= $_POST['subject'];
	$phone 		= $_POST['phone'];
	$message 	= $_POST['message'];
	$verCode 	= $_POST['verCode'];
	$return = array();
	//print_r($_SESSION);
	if($verCode == $_SESSION['rand_code']){
		$data_contact = data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];
		$contact_us_email = get_additional_field($post_id, 'contact_us_email', $article_type);
		$contact_us_email_cc = get_additional_field($post_id, 'contact_us_cc', $article_type);
		
		$data_smtp = get_smtp_info();
		$email_smtp = $data_smtp['email'];
		$pass_smtp = $data_smtp['pass'];
		$return['process'] = 'success';
		$message = 	content_message();
		require_once(ROOT_PATH.'/lumonata-plugins/reservation/class.email_reservation.php');
		$email_reservation = new email_reservation();
		$content_email = $email_reservation->set_content_to_main_email_template($subject,$message);
		
		//echo $content_email; exit;
		$web_name = get_meta_data('web_title');
		$SMTP_SERVER = get_meta_data('smtp');
		require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
		require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
		$mail = new PHPMailer(true);
		
		try {
			$mail->SMTPDebug  = 1;
			$mail->IsSMTP();
			$mail->Host = $SMTP_SERVER;
			$mail->SMTPAuth = true;
			$mail->Port       = 2525; 
			$mail->Username   = $email_smtp;
			$mail->Password   = $pass_smtp;
			$mail->AddReplyTo($email, $name);
			$mail->AddAddress($contact_us_email,$web_name);
			if($contact_us_email_cc!=''){
				$mail->AddAddress($contact_us_email_cc,$web_name);
			}
			$mail->SetFrom($email_smtp);
			$mail->Subject = $subject;
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($content_email);
			$mail->Send();
			$return['process'] = 'success';
			$return['data'] = 'Message sent successfully. We will reply shortly.';
		}catch (phpmailerException $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
		}catch (Exception $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
		}
	}else{
		$return['process'] = 'failed';
		$return['data'] = 'Capcha code is not match';
	}
	
	echo json_encode($return);	
}
add_actions('simple-contact-us-send-ajax_page', 'simple_contact_us_send');

function content_message(){
	set_template(PLUGINS_PATH."/contact_us/mail.html",'the_mail');
	add_block('Bmail','bm','the_mail');
	
	add_variable('name',$_POST['name']);
	add_variable('email',$_POST['email']);
	add_variable('phone',$_POST['phone']);
	add_variable('message',$_POST['message']);
	
	
	parse_template('Bmail','bm',true);
	$theMailContent = return_template('the_mail');
	return $theMailContent;
}


function get_smtp_info(){	
	global $db;
	$result = array();
	$dt_email  = data_tabel('lumonata_meta_data',"where lmeta_name = 'email_user_smtp'",'array');
	$result['email'] = $dt_email['lmeta_value'];
	
	$dt_pass_email  = data_tabel('lumonata_meta_data',"where lmeta_name = 'pass_email_user_smtp'",'array');
	$base64_decode_pass = base64_decode($dt_pass_email['lmeta_value']);
	
	$json_decode_pass = json_decode($base64_decode_pass);
	$result['pass'] = $json_decode_pass->p_e_u_smtp;
	return $result;			
}



function plugin_contact_us(){
	session_start();
	global $actions;
	global $db;
	
	//print_r($_SESSION['rand_code']);
	//echo post_to_id();
	set_template(PLUGINS_PATH."/contact_us/template.html",'the_contact_us');
	add_block('Bcontact_us','bcu','the_contact_us');
	$article_type = 'contact-us';
	$actions->action['meta_title']['func_name'][0] = 'Contact Us - '.web_title(); 
    $actions->action['meta_title']['args'][0] = 'Contact Us - '.web_title(); 
    //print_r($actions);
    
	//tambahan untuk plugin contact-us
	add_variable('js_costume',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/contact_us/js/all.js\" ></script>");	
	
	//
	$thaUrl= 'http://'.SITE_URL.'/check-chapta';
	add_variable('thaUrl_cekChapta',$thaUrl);
	
	$thaUrl= 'http://'.SITE_URL.'/contact-us-ajax';
	add_variable('thaUrl_sendMail',$thaUrl);
	
	$sql		= $db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	$r			= $db->do_query($sql);
	$data		= $db->fetch_array($r);
    $the_post 	= $data['larticle_content'];
	
    //add_variable('the_title',$data['larticle_title']);
    $contact_us_title = get_additional_field($data['larticle_id'], 'contact_us_title', $article_type);
    $contact_us_video = get_additional_field($data['larticle_id'], 'contact_us_video', $article_type);
    
    add_variable('contact_us_title',strtoupper($contact_us_title));
    add_variable('article_brief',$data['larticle_brief']);
    add_variable('article_content',stripslashes($data['larticle_content']));
    
    add_variable('the_post',stripslashes($the_post)); 
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
    add_variable('form_url','http://'.SITE_URL.'/contact-us#contact-us');
    //$_SESSION['rand_code'] = 0;
    
    if(!empty($_POST)){
	    if ($_POST['ssm']){
	    	
	    	//$_SESSION['rand_code'] = 1;
	    	$error = 0;
	    	if (empty($_POST['text_full_name'])){
	    		$error++;
	    		add_variable('text_full_name_input_Error','input_Error');
	    		
	    	}
	    	
	    	if (empty($_POST['text_email'])){
	    		$error++;
	    		add_variable('text_email_input_Error','input_Error');
	    		
	    	}
	    	
	    	if (empty($_POST['text_phone'])){
	    		$error++;
	    		add_variable('text_phone_input_Error','input_Error');
	    		
	    	}
	    	
	    	if (empty($_POST['textarea_message'])){
	    		$error++;
	    		add_variable('textarea_message_textarea_Error','textarea_Error');
	    		
	    	}
	    	
	    	add_variable('text_full_name',$_POST['text_full_name']);
	    	add_variable('text_email',$_POST['text_email']);
	    	add_variable('text_phone',$_POST['text_phone']);
	    	add_variable('textarea_message',$_POST['textarea_message']);
	    	
	    	
	    	if ($error==0){
	    		if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['text_email'])) {
	    			if($_POST['text_validation']==$_SESSION['rand_code']){
	    				//echo $sendToMail = get_meta_data("email");
	    				if (fam_contact_form_send()){
	    					add_variable('block_success','<div class="block_success" style="width:400px;">
			    			Thank you, your message was sent successfully.
			    			</div>');
		    				
		    				add_variable('text_full_name',"");
					    	add_variable('text_email',"");
					    	add_variable('text_phone',"");
					    	add_variable('textarea_message',"");
	    				///*
	    				}else{
	    					add_variable('block_Error','<div class="block_Error" style="width:400px;">
			    			Somethings wrong, please try again!
			    			</div>');
	    				}
	    				//*/
	    			}else{
	    				add_variable('code_Error','input_Error');
	    				add_variable('block_Error','<div class="block_Error" style="width:400px;">
		    			Code not match, please try again!
		    			</div>');
	    			}
	    		}else{
	    			add_variable('block_Error','<div class="block_Error" style="width:400px;">
	    			Invalid email address, please try again!
	    			</div>');
	    		}
	    	}else{
	    		add_variable('code_Error','input_Error');
	    		add_variable('block_Error','<div class="block_Error" style="width:400px;">
	    		Please input fields with * are mandatory!
	    		</div>');
	    	}
	    }	
    }
    
	add_variable('ulr_images',get_theme_img()); 
	parse_template('Bcontact_us','bcu',true);
	return return_template('the_contact_us');
}


function fam_contact_form_send_2nd($fullname,$email,$phone,$cIn,$cOut,$message){
	global $db;
	//if(!empty($_POST)){	
		set_template(PLUGINS_PATH."/contact_us/mail.html",'the_mail');
		add_block('Bmail','bm','the_mail');
		
		$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
		$r=$db->do_query($sql);
		$f=$db->fetch_array($r);
		$n=$db->num_rows($r);
		//echo
		$post_id = $f['larticle_id'];
		$article_type = $f['larticle_type'];
		
		$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
		$contact_us_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
		$contact_us_bcc = get_additional_field( $post_id, 'contact_us_bcc', $article_type);
		
		/*
		add_variable('full_name', 	$_POST['text_full_name']);
		add_variable('email', 	$_POST['text_email']);
		add_variable('phone', $_POST['text_phone']);
		add_variable('message', $_POST['textarea_message']);
		*/
		
		add_variable('full_name', 	$fullname);
		add_variable('email', 	$email);
		add_variable('phone', $phone);
		add_variable('cIn', $cIn);
		add_variable('cOut', $cOut);
		add_variable('message', $message);
		
		parse_template('Bmail','bm',true);
		$theMailContent = return_template('the_mail');
		
		//$sendToMail = get_meta_data("email");
		$sendToMail = $contact_us_email;
		$cc = $contact_us_cc;
		$bcc = $contact_us_bcc;
		
		$web_title = get_meta_data("web_title");
		
		ini_set("SMTP", SMTP_SERVER);
		ini_set("sendmail_from", $sendToMail);
		
		$subject = "[$web_title] Contact from ".$_POST['text_full_name'];
				
		//$headers  = "From: Dipan Villa Rental<$sendToMail>\r\n"; 
		$headers  = "From: $fullname <$email>\r\n"; 
		$headers .= "Cc: ".$cc."\r\n";
		$headers .= "Bcc: ".$bcc."\r\n";
	    $headers .= "Reply-To: $email\r\n"; 
	    $headers .= "Return-Path: $sendToMail\r\n"; 
	    $headers .= "X-Mailer: Drupal\n"; 
	    $headers .= 'MIME-Version: 1.0' . "\n"; 
	    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 
		
		$send = mail($sendToMail,$subject,$theMailContent,$headers);
		
		if($send){
			return true;
		}else{
			return false;
		}
	//}else{
		//empty
	//}
}


function fam_contact_form_send($fullname,$email,$tlp,$message,$subject=''){
		global $db;	
		
		if(empty($fullname) || empty($email) || empty($message)){
			return false;
		}
		
		set_template(PLUGINS_PATH."/contact_us/mail.html",'the_mail');
		add_block('Bmail','bm','the_mail');
		
		$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
		$r=$db->do_query($sql);
		$f=$db->fetch_array($r);
		$n=$db->num_rows($r);
		//echo
		$post_id = $f['larticle_id'];
		$article_type = $f['larticle_type'];
		
		$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
		$contact_us_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
		$contact_us_bcc = get_additional_field( $post_id, 'contact_us_bcc', $article_type);
		
		add_variable('full_name', $fullname);
		add_variable('email', $email);	
		add_variable('phone', $tlp);		
		add_variable('message', nl2br($message));
		
		parse_template('Bmail','bm',true);
		$theMailContent = return_template('the_mail');
		

		$sendToMail = $contact_us_email;
		$cc = $contact_us_cc;
		$bcc = $contact_us_bcc;
		
		$web_title = get_meta_data("web_title");
		
		///ini_set("SMTP", SMTP_SERVER);
		///ini_set("sendmail_from", $sendToMail);
		
		$subject = "[$web_title] Contact from ".$fullname;
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";							
		$headers .= "From:  $fullname <$email>\r\nReply-to :$email\r\n";
		if(!empty($cc))
			$headers .= "Cc: ".$cc."\r\n";
		if(!empty($bcc))	
			$headers .= "Bcc: ".$bcc."\r\n";
		$send = mail($sendToMail,$subject,$theMailContent,$headers);
		if($send){
			return true;
		}else{
			return false;
		}
}


function fam_newsletter_send($fullname,$email,$country){
		global $db;	
		
		if(empty($fullname) || empty($email) || empty($country)){
			return false;
		}
		
		set_template(PLUGINS_PATH."/contact_us/mail_newsletter.html",'the_mail');
		add_block('Bmail','bm','the_mail');
		
		$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
		$r=$db->do_query($sql);
		$f=$db->fetch_array($r);
		$n=$db->num_rows($r);
		//echo
		$post_id = $f['larticle_id'];
		$article_type = $f['larticle_type'];
		
		$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
		$contact_us_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
		$contact_us_bcc = get_additional_field( $post_id, 'contact_us_bcc', $article_type);
		
		add_variable('full_name', $fullname);
		add_variable('email', $email);	
		add_variable('country', $country);	
		
		parse_template('Bmail','bm',true);
		$theMailContent = return_template('the_mail');
		

		$sendToMail = $contact_us_email;
		$cc = $contact_us_cc;
		$bcc = $contact_us_bcc;
		
		$web_title = get_meta_data("web_title");
		
		///ini_set("SMTP", SMTP_SERVER);
		///ini_set("sendmail_from", $sendToMail);
		
		$subject = "[$web_title] Newsletter Member";
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";							
		$headers .= "From:  $fullname <$email>\r\nReply-to :$email\r\n";
		if(!empty($cc))
			$headers .= "Cc: ".$cc."\r\n";
		if(!empty($bcc))	
			$headers .= "Bcc: ".$bcc."\r\n";
		$send = mail($sendToMail,$subject,$theMailContent,$headers);
		if($send){
			return true;
		}else{
			return false;
		}
}

function contact_us_ajax(){
	session_start();
	add_actions('is_use_ajax', true);		
	global $db;
	if(!empty($_POST['act']) && $_POST['act']=='submit'){
		//if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['tlp']) && !empty($_POST['message'])){
		if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message'])){
			$name 		= $_POST['name'];			
			$email 		= $_POST['email'];
			$tlp		= '';//$_POST['tlp'];
			$message 	= $_POST['message'];			
			
			if(isset($_POST['code']) && $_POST['code']==$_SESSION['rand_code']){
				if (fam_contact_form_send($name,$email,$tlp,$message)){
						echo 'success';
				}else{
						echo 'error';
				}
			}else{
				echo 'error';
			}
		}else{
			echo 'error';
		}
	}else if(!empty($_POST['act']) && $_POST['act']=='submit_newsletter'){
		if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['country'])){
			$name 		= $_POST['name'];			
			$email 		= $_POST['email'];
			$country 	= $_POST['country'];			
			
			if (fam_newsletter_send($name,$email,$country)){
					echo 'success';
			}else{
					echo 'error';
			}
		}else{
			echo 'error';
		}
	}else{
		echo 'error';
	}	
}

function check_chapta(){
	session_start();	
	add_actions('is_use_ajax', true);
	if(!empty($_POST['act']) && $_POST['act']=='checkCode'){
		if($_POST['code']==$_SESSION['rand_code']){
			$value=array('status'=>'match','content'=>'');
    		echo json_encode($value);
		}else{
			$content='<img src="http://'.SITE_URL.'/val-image.php" class="chapta">';
			$value=array('status'=>'no macth : '.$_SESSION['rand_code'],'content'=>$content);
    		echo json_encode($value);
		}
	}
}

function check_chapta2(){
	session_start();	
	add_actions('is_use_ajax', true);
	if(!empty($_POST['act']) && $_POST['act']=='checkCode'){
		if($_POST['code']==$_SESSION['rand_code2']){
			$value=array('status'=>'match','content'=>'');
    		echo json_encode($value);
		}else{
			$content='<img src="http://'.SITE_URL.'/val-image2.php" class="chapta">';
			$value=array('status'=>'no macth : '.$_SESSION['rand_code2'],'content'=>$content);
    		echo json_encode($value);
		}
	}
}

function check_chapta3(){
	session_start();	
	add_actions('is_use_ajax', true);
	if(!empty($_POST['act']) && $_POST['act']=='checkCode'){
		if($_POST['code']==$_SESSION['rand_code3']){
			$value=array('status'=>'match','content'=>'');
    		echo json_encode($value);
		}else{
			$content='<img src="http://'.SITE_URL.'/val-image3.php" class="chapta">';
			$value=array('status'=>'no macth : '.$_SESSION['rand_code3'],'content'=>$content);
    		echo json_encode($value);
		}
	}
}

add_actions('contact-us-ajax_page', 'contact_us_ajax');
add_actions('check-chapta_page', 'check_chapta'); //virtual page, nama function
add_actions('check-chapta2_page', 'check_chapta2'); //virtual page, nama function
add_actions('check-chapta3_page', 'check_chapta3'); //virtual page, nama function


function is_contactus(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='contact-us'){
		$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    	$actions->action['meta_title']['args'][0] = '';
		return true;
	}else{
		return false;
	}
}


function front_contactus(){
	global $actions;
	global $db;
	$article_type = 'contact-us';
	$cek_url = cek_url();
	$type='contactus';
	//set template
	set_template(PLUGINS_PATH."/contact_us/template_front.html",'template_contactus');
	//set block
       
	//print_r($_POST);
    add_block('Bcontactus_block','b_contactus','template_contactus');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/contact_us');
	
	$actions->action['meta_title']['func_name'][0] = "Contact Us - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', 'Contact Us');
	
	$dashed="&nbsp; &gt; &nbsp;";
	$navigation = '<ul class="navigation">';
 	$navigation .= '<li><a href="'.$href.'" title="Home">HOME</a></li> '.$dashed;
	$navigation .= '</ul>';
	add_variable('navigation', $navigation);
	
	$thaUrl= 'http://'.SITE_URL.'/check-chapta';
	add_variable('thaUrl_cekChapta',$thaUrl);
	$thaUrl= 'http://'.SITE_URL.'/contact-us-ajax';
	add_variable('thaUrl_sendMail',$thaUrl);
	
	$sql		= $db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	$r			= $db->do_query($sql);
	$data		= $db->fetch_array($r);
    $the_post 	= $data['larticle_content'];
	
    //add_variable('the_title',$data['larticle_title']);
    $contact_us_title = get_additional_field($data['larticle_id'], 'contact_us_title', $article_type);
    add_variable('contact_us_title',strtoupper($contact_us_title));
    add_variable('article_brief',$data['larticle_brief']);
    add_variable('article_content',stripslashes($data['larticle_content']));
    
    add_variable('the_post',stripslashes($the_post)); 
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
    add_variable('form_url','http://'.SITE_URL.'/contact-us#contact-us');
    
    $contentright =json_decode(get_front_right_image($data['larticle_id']),true);
	add_variable('contentright', $contentright['gallery_list']);
    
    parse_template('Bcontactus_block','b_contactus',false);
         
	if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_contactus'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_contactus');	
    }
}


function front_newsletter(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	//set template
	set_template(PLUGINS_PATH."/contact_us/template_newsletter.html",'template_newsletter');
	//set block
	
	//print_r($_POST);
    add_block('Bnewsletter_block','b_newsletter','template_newsletter');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/contact_us');

	$thaUrl= 'http://'.SITE_URL.'/check-chapta3';
	add_variable('thaUrl_cekChapta',$thaUrl);
	$thaUrl= 'http://'.SITE_URL.'/contact-us-ajax';
	add_variable('thaUrl_sendMail',$thaUrl);
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
    parse_template('Bnewsletter_block','b_newsletter',false);
    return return_template('template_newsletter');	
    
}

function front_video(){
	global $actions;
	global $db;
	$article_type = 'contact-us';
	$cek_url = cek_url();
	//set template
	set_template(PLUGINS_PATH."/contact_us/template_video.html",'template_video');
	//set block
	
	//print_r($_POST);
    add_block('Bvideo_block','b_video','template_video');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/contact_us');
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
    $sql		= $db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	$r			= $db->do_query($sql);
	$data		= $db->fetch_array($r);
    $the_post 	= $data['larticle_content'];
	
    //add_variable('the_title',$data['larticle_title']);
    $contact_us_video = get_additional_field($data['larticle_id'], 'contact_us_video', $article_type);
    if(!empty($contact_us_video)){
    	$url_player = get_url_embed($contact_us_video,1);
		$url_thumb = get_url_embed($contact_us_video,2);
    }else{
    	$url_player = "#";
    	$url_thumb = 'http://'.get_theme().'/images/video.png';
    }
    add_variable('url_player',$url_player);
    add_variable('url_thumb',$url_thumb);
    
    parse_template('Bvideo_block','b_video',false);
    return return_template('template_video');	
    
}
function front_news(){
	global $actions;
	global $db;
	$article_type = 'contact-us';
	$cek_url = cek_url();
	//set template
	set_template(PLUGINS_PATH."/contact_us/template_news.html",'template_news');
	//set block
	
	//print_r($_POST);
	add_block('BList_news','ln','template_news');
    add_block('Bnews_block','b_news','template_news');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/contact_us');
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
	$data_category 	= data_tabel('lumonata_rules',"where lname='Special Offer' and lrule='categories' and lgroup='articles'",'array');
	$category_id 	= $data_category['lrule_id']; 
	
    $str = $db->prepare_query("SELECT c.larticle_id, c.larticle_title, c.larticle_content, c.lsef
							  FROM lumonata_rule_relationship as a,lumonata_rules as b,lumonata_articles as c
							  where  a.lrule_id=b.lrule_id and b.lrule = %s and b.lgroup = %s and 
							  a.lapp_id = c.larticle_id and a.lrule_id=%d
							  order by rand() limit 3",'categories','articles',$category_id);
							  
	$result = $db->do_query($str);						  
	$num = $db->num_rows($result);
	$temp = "";
	if($num>0){
		$list_num= 0;
		$list_pagging = "";
		
		while($data_special_offer = $db->fetch_array($result)){
			$article_id = $data_special_offer['larticle_id'];
			$sef=$data_special_offer['lsef'];
			$frr=fetch_rule_relationship('app_id='.$article_id);
			if(isset($frr[0])) $rule_id=$frr[0];
			//	echo 'hh';
			$d_r=fetch_rule('rule_id='.$rule_id);
			$sef_r=$d_r['lsef'];
			$dt_pdf = data_tabel('lumonata_attachment',"where larticle_id=$article_id and mime_type='application/pdf'",'array');
			$file_pdf = 'http://'.SITE_URL.$dt_pdf['lattach_loc'];
			
			$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$sef_r.'/'.$sef.'.html';
			add_variable('url_detail',$url_detail);
			//add_variable('url_detail',$file_pdf);
			//echo $article_id;
			
			//get image
			$dt_image = data_tabel('lumonata_attachment',"where larticle_id=$article_id and ltitle='thumb_pdf'",'array');
			$thumb_pdf = 'http://'.SITE_URL.$dt_image['lattach_loc'];
			
			add_variable('thumb_pdf',$thumb_pdf);
			
			if($list_num==0){
				$list_pagging .="<li class=\"visited\"></li>";	
				add_variable('active','active');
			}else{
				 $list_pagging .="<li></li>";
				add_variable('active','');
			}
			
			parse_template('BList_news','ln',true);
			$list_num++;
			//print_r($data_special_offer);
		}
		
		if($list_num==1) add_variable('hidden_pagging','style="display:hidden;"');
		else if($list_num>1)add_variable('list_pagging',$list_pagging);
		$temp = return_template('Bnews_block','b_news',false);				
	}
	
	return $temp;
    
	
}




function front_news_old(){
	global $actions;
	global $db;
	$article_type = 'contact-us';
	$cek_url = cek_url();
	//set template
	set_template(PLUGINS_PATH."/contact_us/template_news.html",'template_news');
	//set block
	
	//print_r($_POST);
    add_block('Bnews_block','b_news','template_news');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/contact_us');
    add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
    
    $sql		= $db->prepare_query("SELECT *
			     FROM lumonata_additional_fields
			     WHERE lkey=%s and lvalue=%s AND lapp_name=%s"
			     ,'article_news','1','articles');
	$r			= $db->do_query($sql);
	$data		= $db->fetch_array($r);
	if(!empty($data)){
		$article_id	= $data['lapp_id'];
    
		$sql=$db->prepare_query("select * from lumonata_articles where larticle_id=%d AND larticle_type=%s",$article_id,'articles');
		
		//echo $sql;
		$r=$db->do_query($sql);
		$d=$db->fetch_array($r);
		
		$article_id=$d['larticle_id'];
		$the_title=$d['larticle_title'];
		$the_post=$d['larticle_content'];
		$the_brief=filter_content($d['larticle_content'],true);
		$the_post=filter_content($the_post);
		$the_post=str_replace("<p>[album_set]</p>", "<div class=\"album_set\">".the_attachment($d['larticle_id'],0)."<br clear=\"both\" /></div>", $the_post,$count_replace);
		$the_post=str_replace("[album_set]", "<div class=\"album_set\">".the_attachment($d['larticle_id'],0)."<br clear=\"both\" /></div>", $the_post,$count_replace2);
			
		$post_date=date(get_date_format(),strtotime($d['lpost_date']));
		//$the_categories=the_categories($d['larticle_id'],$d['larticle_type']);
		
		$the_selected_categories=find_selected_rules($d['larticle_id'],'categories',$d['larticle_type']);
		$the_categories=recursive_taxonomy_custom(0,'categories',$d['larticle_type'],'category',$the_selected_categories,'',68);
		$the_categories=trim($the_categories,', ');
			
		$sef=$d['lsef'];
		$frr=fetch_rule_relationship('app_id='.$article_id);
		if(isset($frr[1])){
		$rule_id=$frr[1];
			
		$d_r=fetch_rule('rule_id='.$rule_id);
		$sef_r=$d_r['lsef'];
		$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$sef_r.'/'.$sef.'.html';
		
		add_variable('thumb_pdf','http://'.SITE_URL.'/lumonata-plugins/contact_us/thumb-sample.jpg');
		add_variable('thumb_pdf_2','http://'.SITE_URL.'/lumonata-plugins/contact_us/thumb-sample-2.jpg');
		add_variable('thumb_pdf_3','http://'.SITE_URL.'/lumonata-plugins/contact_us/thumb-sample-3.jpg');
		
		add_variable('the_title',$the_title);
		add_variable('url_detail',$url_detail);
		add_variable('post_date',$post_date);
		$the_brief=substr($the_brief, 0, 50).' ...';
		add_variable('the_brief',$the_brief);
		
		
		parse_template('Bnews_block','b_news',false);
		return return_template('template_news');	
		}
	}
    
	
}

require_once('just_test_email.php');
?>