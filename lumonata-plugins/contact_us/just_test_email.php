<?php 

function just_test_send_email(){
	set_template(PLUGINS_PATH."/contact_us/index.html",'the_mail');
	add_variable('plugin_url','http://'.SITE_URL.'/lumonata-plugins/contact_us');
	$content_email = return_template('the_mail');
	
	$data_smtp = get_smtp_info();
	$email_smtp = $data_smtp['email'];
	$pass_smtp = $data_smtp['pass'];
		
	$web_name = get_meta_data('web_title');
	$SMTP_SERVER = get_meta_data('smtp');
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
	$mail = new PHPMailer(true);
	
	try {
		$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Port       = 2525; 
		$mail->Username   = $email_smtp;
		$mail->Password   = $pass_smtp;
		$mail->AddReplyTo('adijuliartha@gmail.com', 'Adi Juliartha');
		$mail->AddAddress('adi@lumonata.com',$web_name);
		$mail->SetFrom('adijuliartha@gmail.com');
		$mail->Subject = 'subject saja';
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($content_email);
		$mail->Send();
		$return['process'] = 'success';
		$return['data'] = 'Send email successfully done, we will immediately respond later';
	}catch (phpmailerException $e) {
		$return['process'] = 'failed';
		$return['data'] = 'Send email failed please try again later';
	}catch (Exception $e) {
		$return['process'] = 'failed';
		$return['data'] = 'Send email failed please try again later';
	}
}

add_actions('just-test-send-email-ajax_page', 'just_test_send_email');
?>