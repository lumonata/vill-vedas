jQuery(document).ready(function(e) {
    jQuery("#btn_contactUs").click(function(){
		send_email();
	});
});

function send_email(){
	jQuery('.error').removeClass('error');jQuery(".notif-span").text('').removeClass('success-info error-info');
	var name = jQuery("[name=name]").val();	
	var email = jQuery("[name=email]").val();
	var phone = jQuery("[name=phone]").val();
	var subject = jQuery("[name=subject]").val();
	var message = jQuery("[name=message]").val();
	var verCode = jQuery("[name=verCode]").val();
	var error = 0;
	var txt_error = '';
	if(name==''){
		error++;
		txt_error = 'Name';
		jQuery("[name=name]").addClass('error');
	}
	
	if(email==''){
		error++;
		if(txt_error!='')txt_error +=', ';
		txt_error += 'Email';
		jQuery("[name=email]").addClass('error');
	}
	
	if(subject==''){
		error++;
		if(txt_error!='')txt_error +=', ';
		txt_error += 'Subject';
		jQuery("[name=subject]").addClass('error');
	}
	
	if(message==''){
		error++;
		if(txt_error!='')txt_error +=', ';
		txt_error += 'Message';
		jQuery("[name=message]").addClass('error');
	}
	
	
	
	if(verCode==''){
		error++;
		if(txt_error!='')txt_error +=', ';
		txt_error += 'Capcha';
		jQuery("[name=verCode]").addClass('error');
	}
	var txt_email_error = '';
	if(email!=''){
		if(validateTheEmail(email)==false)	{
			error++;
			if(txt_error!=''){
				txt_email_error += ' and email format is false';
			}else{
				txt_email_error += 'Email format is false';
			}
			jQuery("[name=email]").addClass('error');
		}
	}
	
	
	if(error==0){
		jQuery("#btn_contactUs").val("PLEASE WAIT...");
		var url = 'http://'+jQuery("[name=site_url]").val()+'/simple-contact-us-send-ajax';
		var data = new Object();
			data.pKEY = 'is_use_ajax';
			data.name 		= name;
			data.email		= email;
			data.phone		= phone;
			data.subject	= subject;
			data.message	= message;
			data.verCode	= verCode;
		jQuery.post(url,data,function(response){
			var result = jQuery.parseJSON(response);
			if(result.process=='success'){
				jQuery("[name=name],[name=email],[name=phone],[name=subject],[name=message],[name=verCode]").val('');
				jQuery(".notif-span").text(result.data).addClass('success-info');	
			}else if(result.process=='failed'){
				jQuery(".notif-span").text(result.data).addClass('error-info');	
			}
			jQuery("#btn_contactUs").val("SEND");
			jQuery(".chapta").attr('src','http://'+jQuery("[name=site_url]").val()+'/val-image.php');
			
		});
		//do send
		
	}else{
		if(txt_error!='')	jQuery(".notif-span").text(txt_error+' is required'+txt_email_error).addClass('error-info');	
		else jQuery(".notif-span").text(txt_email_error).addClass('error-info');	
	}
	
}

function trim(str) {
    var	str = str.replace(/^\s\s*/, ''),
            ws = /\s/,
            i = str.length;
    while (ws.test(str.charAt(--i)));
    return str.slice(0, i + 1);
}

function validateTheEmail(address) {
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var address;
    if(reg.test(address) == false) {
       return false;
    }else{
       return true;
    }
}