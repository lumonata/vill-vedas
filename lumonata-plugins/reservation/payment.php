<?php
function paypal_notify(){
	global $db;
	$cek_url = cek_url();
	$dtUrl = json_decode(base64_decode($cek_url[1]));
	//exist booking
	$dbk = data_tabel("lumonata_accommodation_booking", "where lbook_id=".$dtUrl->book_id."",'array');	
	//print_r($dtUrl);
	//print_r($_POST);exit;
	//print_r($dbk);exit;
	if(!empty($dbk)){
		if($_POST['payment_status']=='Completed'){
			$dps = json_decode($dbk['ldeposit']);
			if(($dps->deposit==100 && $dtUrl->type=='dp') || $dtUrl->type=='fp' ) $status_txt = 'fp';
			else $status_txt = 'dp';
			
			$q = $db->prepare_query("insert into lumonata_accommodation_payment 
									(lpaid,lbook_id,ltxn_id,lpayment_type,ltype_paid,
									 lgross,lcurrency,lpayer_email,lpayer_id) values 
									 (%d,%d,%s,%s,%s,
									  %s,%s,%s,%s)",
									  time(),$dtUrl->book_id,$_POST['txn_id'],$_POST['payment_type'],$status_txt,
									  $_POST['payment_gross'],$_POST['mc_currency'],$_POST['payer_email'],$_POST['payer_id']);
				
			$result = $db->do_query($q);					  
			//$result = true;
			if($result){
				$status = ($status_txt=='dp'? 6:4);
				$qu = $db->prepare_query("update lumonata_accommodation_booking set lstatus=%d where lbook_id=%s",$status,$dtUrl->book_id);				
				$update = $db->do_query($qu);
				//$update = true;
				if($update){
					send_email_success_payment_guest($dtUrl->type,$dbk);
					send_email_success_payment_admin($dtUrl->type,$dbk);	
					syncronize_villa_availability($dtUrl->book_id);
				}//end if update status success	
			}//end if success save payment process
		}//end if payment status complete
	}//end if exist booking
}


function syncronize_villa_availability($book_id){
	global $db;
	$q = $db->prepare_query("select lab.lbook_id, lab.lcheck_in, lab.lcheck_out, labd.larticle_id from lumonata_accommodation_booking lab 
						inner join lumonata_accommodation_booking_detail labd on lab.lbook_id=labd.lbook_id
						where lab.lbook_id=%d",$book_id);
						
	$r = $db->do_query($q);
	while($dt = $db->fetch_array($r)){
		//print_r($dt);
		$days = ($dt['lcheck_out']- $dt['lcheck_in']) / 86400;
		change_status_availability_villa($dt['larticle_id'],$dt['lcheck_in'],$dt['lcheck_out'],$days,0);
	}						
}


function get_detail_accommodation($book_id){
	global $db;
	$q = $db->prepare_query("select la.* from lumonata_accommodation_booking lab 
								inner join lumonata_accommodation_booking_detail labd on lab.lbook_id=labd.lbook_id
								inner join lumonata_articles la on labd.larticle_id=la.larticle_id 
								where lab.lbook_id=%d",$book_id);				
	$r = $db->do_query($q);							
	return $db->fetch_array($r);
}


function test_invoice_pdf(){
	require_once(ROOT_PATH."/lumonata-plugins/reservation/class.email_reservation.php");
	$email_reservation =  new email_reservation();
	if($_POST['type_show']=='dp') return $email_reservation->generate_pdf_receipt($_POST['book_id']);
	else if($_POST['type_show']=='fp') return $email_reservation->generate_pdf_invoice($_POST['book_id']);
	//print_r($_POST);
}

add_actions('test-invoice-pdf-ajax_page', 'test_invoice_pdf');


?>