<?php 
/*
    Plugin Name: Reservation
    Plugin URL: 
    Description: It's plugin  is use for Reservation
    Author: Adi
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 15 September 2014
*/
require_once('ajax.php');

function reservation_page(){
	session_start();
	add_variable('script_reservation',"<script src=\"http://".SITE_URL."/lumonata-plugins/reservation/script.js\" type=\"text/javascript\"></script>");
	

	set_template(PLUGINS_PATH."/reservation/template.html",'template_reservation');
	add_block('reservation_block','reser_block','template_reservation');
	
	$id =  post_to_id();
	$image = get_page_header_images($id);
	
	add_variable('image_bg',$image[0]);
	//unset($_SESSION['reservation_ss']['step_2']);
	//print_r($_SESSION);
	//unset($_SESSION['reservation_ss']);
	if(empty($_SESSION['reservation_ss']) || empty($_SESSION['reservation_ss']['step_1'])){
		add_variable('the_step',step_one());	
	}
	
	if(!empty($_SESSION['reservation_ss']['step_1']) && empty($_SESSION['reservation_ss']['step_2'])){
		add_variable('the_step',step_two());
	}
	
	if(!empty($_SESSION['reservation_ss']['step_1']) && !empty($_SESSION['reservation_ss']['step_2']) && empty($_SESSION['reservation_ss']['step_3'])){
		add_variable('the_step',step_three());
	}
	
	if(!empty($_SESSION['reservation_ss']['step_1']) && !empty($_SESSION['reservation_ss']['step_2']) && !empty($_SESSION['reservation_ss']['step_3'])){
		
	}
	add_variable('meta_title', get_article_title($id).' - '.trim(web_title()));  
	return return_template('reservation_block','reser_block',false);	
}



function list_num_of_bedroom(){
	global $db;
	$list = data_tabel('lumonata_additional_fields',"where lapp_name='villas' and lkey='num_room' group by lvalue order by lvalue" );	
	$option = "";
	while($dt = $db->fetch_array($list)){
		$option .= "<option value=".$dt['lvalue'].">".$dt['lvalue']."</option>";
	}
	return $option;
}

function test_just_detail_booking(){
	require_once(ROOT_PATH."/lumonata-plugins/reservation/class.email_reservation.php");
	$email_reservation = new email_reservation(true);
	echo $email_reservation->just_detail_booking($_POST['book_id'],$_POST['type_show']);
}
add_actions('test-just-detail-booking-ajax_page','test_just_detail_booking');


?>