<?php
require_once('ajax-2.php');
require_once('ajax-3.php');
require_once('payment.php');
require_once('email-event/email-check-availability.php');
require_once('email-event/email-success-payment-guest.php');
require_once('email-event/email-success-payment-admin.php');


function step_one(){
	//session_start();
	//print_r($_SESSION);
	set_template(PLUGINS_PATH."/reservation/template.html",'template_step_1');
	add_block('step_1_block','b_step_1','template_step_1');
	add_variable('list_num_of_bedroom',list_num_of_bedroom());		
	return return_template('step_1_block','b_step_1_',false);
}

function step_two(){
	global $db;
	step_one_set_session();	
	//session_start();
	set_template(PLUGINS_PATH."/reservation/template.html",'template_step_2');
	add_block('step_2_block','b_step_2','template_step_2');
	
	$step_1 = $_SESSION['reservation_ss']['step_1'];
	add_variable('cIn', $step_1['cIn']);
	add_variable('cOut', $step_1['cOut']);
	add_variable('adult', $step_1['adult']);
	add_variable('child', $step_1['child']);
	add_variable('no_of_bedroom', $step_1['no_of_bedroom']);
	$cIn				= strtotime($step_1['cIn']);
	$cOut				= strtotime($step_1['cOut']);
	$no_of_bedrroom		= $step_1['no_of_bedroom'];	
	$days = ($cOut-$cIn)/86400;
	$night = $days;
	$min_stay_data = min_stay_data($cIn,$cOut,$night);
	//print_r($min_stay_data);
	if($min_stay_data['valid']=='valid'){
		$list_villa	= list_availabel_villa($cIn,$cOut,$no_of_bedrroom);		
		if($list_villa=="")add_variable('not-available','NOT ');	
	}else{
		//print_r($min_stay_data);
		$list_villa = '<p class="min-stay-info">'.$min_stay_data['txt-not-valid'].'<p>';
		add_variable('not-available','NOT ');	
	}

	add_variable('list_villa',$list_villa);
	
	if(isset($_POST['cIn'])) echo return_template('step_2_block','b_step_2_',false);
	else return return_template('step_2_block','b_step_2_',false);	
}

function min_stay_data($cIn,$cOut,$night){
	$seasons = array();
	for($i=0 ;$i<=$night ; $i++){
		$the_date = $cIn + ($i*86400);
		//if($the_date<$cOut){//karena checkout tidak dihitung
			$data = data_tabel('lumonata_accommodation_season',"where $the_date >= ldate_start and $the_date <= ldate_finish",'array');
			//print_r($data);
			//echo "where $the_date >= ldate_start and $the_date <= ldate_finish#";
			if(!empty($data)){
				$id_season = $data['lseason_id'];
				$min_stay = $data['lmin_stay'];
				$seasons[$id_season]['data'] = $data;
				if(!isset($seasons[$id_season]['season_hit'])) $seasons[$id_season]['season_hit']='';
				$hit = ($seasons[$id_season]['season_hit'] == '' ? 1: $hit=$hit+1 );
				$seasons[$id_season]['season_hit'] = $hit;
				$seasons[$id_season]['valid_season'] = ($hit > $min_stay ? 'valid' : 'not-valid');	
			}
			
		//}
	}//end for
	//print_r($seasons);
	//get not valid season
	$not_valid_season = array();
	foreach($seasons as $id=>$season){
		if($season['valid_season']=='not-valid'){
			$not_valid_season[$id]= $season['data'];
		}
	}
	
	$num_not_valid_season = count($not_valid_season);
	$txt_season_error = "";
	$i = 1;
	foreach($not_valid_season as $id=>$season){
		$name_season = $season['lname'];
		$date_start = date('d M Y',$season['ldate_start']);
		$date_end	= date('d M Y',$season['ldate_finish']);
		$min_stay	= $season['lmin_stay'];
		if($txt_season_error!="" && $i!=1) $txt_season_error = ($i==$num_not_valid_season ? $txt_season_error.= " and " : ", ");		
		$txt_season_error .= "$name_season on $date_start - $date_end with a minimum stay $min_stay day".($min_stay>1? 's':'')."";
		$i++;
	}
	$return = array();
	$return['valid'] = ($num_not_valid_season==0 ? 'valid':'not-valid');
	$return['txt-not-valid'] = "Reservation must be on a minimum stay. In your reservation there is ".$txt_season_error.".";
	return $return;
}

function min_stay_booking($cIn,$cOut,$night){
	$min_stay = 0;
	for($i=0 ;$i<=$night ; $i++){
		$the_date = $cIn + ($i*86400);
		if($the_date<$cOut){//karena checkout tidak dihitung
			$data = data_tabel('lumonata_accommodation_season',"where $the_date >= ldate_start and $the_date <= ldate_finish",'array');
			if(!empty($data)){
				if($min_stay==0) $min_stay = $data['lmin_stay'];	
				else if($min_stay !=0 && $min_stay < $data['lmin_stay']) $min_stay = $data['lmin_stay'];	
			}	
		}
	}//end for	
	return $min_stay;
	
}

/*function valid_min_stay($cIn,$cOut){
	$days = ($cOut-$cIn)/86400;
	echo "$cIn,$cOut,$days";
}
*/
function list_availabel_villa($cIn,$cOut,$no_of_bedrroom){
	global $db;
	$days 	  = ($cOut-$cIn)/86400;
	$list 	  = "";
	$q = $db->prepare_query("select a.* from lumonata_articles a, lumonata_additional_fields b where a.larticle_id=b.lapp_id 
							and a.larticle_type=%s and b.lkey=%s and b.lvalue=%d",'villas','num_room',$no_of_bedrroom);				
	$r = $db->do_query($q);			
	while($d = $db->fetch_array($r)){
		$acco_id = $d['larticle_id'];
		$dt_available = check_availability_villa($cIn,$cOut,$days,$acco_id);
		$dt_price = data_prices_villa($cIn,$cOut,$days,$acco_id);
		if($dt_available['not_valid']==0){
			$price = $dt_price['min_price_per_night'];
			$thumb = get_thumb_villa($acco_id);
			if($thumb!='')	$thumb = 'http://'.SITE_URL.$thumb;
			else $thumb = '';
			$villa_name = $d['larticle_title'];
			$desc = $d['larticle_content'];
			$dt_desc = explode('<!-- pagebreak -->',$desc);
			$brief = $dt_desc[0];
			$txt_discount = all_discount_span_thumb($dt_price);
			$list .= temp_available_villa($acco_id,$thumb,$villa_name,$desc,$brief,$price,$txt_discount);
			//break;
		}//end not valid
	}//end while
	
	return $list;
}

function all_discount_span_thumb($dt_price){
	$txt_discount 	= '';
	$txt_discount 	= discount_or_surcharge_txt('discount',$dt_price);
	$span_discount 	= ($txt_discount!='' ? "<span class=\"yellow\">$txt_discount</span>" : "" );
	
	$txt_early_bird = '';
	$txt_early_bird =discount_or_surcharge_txt('early_bird',$dt_price);
	$span_early_bird = ($txt_early_bird!='' ?  "<span class=\"green\">$txt_early_bird</span>" : "");

	$temp = "";
	if($span_discount!="" || $span_early_bird!=""){
		$plus = ($span_discount!="" && $span_early_bird!="" ? " + " : "");
		$temp = "<div class=\"discount-thumb\">$span_discount $plus $span_early_bird</div>";	
	}
	return $temp;	
}

function discount_or_surcharge_txt($type,$dt_price){
	if($type=='discount'){
		$txt_discount 	= '';
		$txt_discount 	= ($dt_price['discount_txt']!='' ? $dt_price['discount_txt'] : '');	
		$txt_discount 	= ($dt_price['discount_txt']!='' && $dt_price['discount_alltime_txt']!='' ? $txt_discount.' + ':$txt_discount);	
		$txt_discount 	= ($dt_price['discount_alltime_txt']!='' ? $txt_discount.$dt_price['discount_alltime_txt'] : $txt_discount);
		return $txt_discount;
	}
	
	if($type=='early_bird'){
		$txt_early_bird = '';
		$txt_early_bird = ($dt_price['early_bird_txt']!='' ? $dt_price['early_bird_txt'] : '');
		$txt_early_bird = ($dt_price['early_bird_txt']!='' && $dt_price['early_bird_alltime_txt']!='' ? $txt_early_bird.' + ' : $txt_early_bird);
		$txt_early_bird = ($dt_price['early_bird_alltime_txt']!='' ? $txt_early_bird.$dt_price['early_bird_alltime_txt'] : $txt_early_bird);
		return $txt_early_bird;
	}
	
	if($type=='surecharge'){
		
	}
}

function is_image_mime($field){
	$valid = "$field = 'image/jpg' or $field = 'image/jpeg' or $field = 'image/png'";
	return $valid;
}

function get_thumb_villa($acco_id){
	$is_image = is_image_mime('mime_type');
	$thumb = data_tabel("lumonata_attachment"," where larticle_id=$acco_id and ($is_image) order by lorder",'array');
	if(!empty($thumb)) return $thumb['lattach_loc_thumb'];
	else return '';
}

function check_availability_villa($cIn,$cOut,$days,$acco_id){
	//0=booked;1=available;2=no-check-in;3=no-check-out;4=no-check-in-out;5=booking-hold;6=owner;7=maintance;
	$price = 0;$not_valid=0;$min_price_per_night=0;
	for($i=0 ;$i<=$days ; $i++){
		$the_date = $cIn + ($i*86400);
		//echo date('d M Y',$the_date).'#';
		//if($the_date >= $cIn && $the_date < $cOut) {
		if($the_date >= $cIn && $the_date <= $cOut) {//for if cOut not set on season						
			$data = data_tabel('lumonata_availability',"where ldate=$the_date and lacco_id=$acco_id",'array');
			if(isset($data) && ($data['lstatus']=='0' || $data['lstatus']=='2' || $data['lstatus']=='3' || $data['lstatus']=='4' || $data['lstatus']=='5'  || $data['lstatus']=='6' || $data['lstatus']=='7' ) || empty($data)){
				$not_valid++;
			}else{
				$price = $price + $data['lrate'];
			}
			if(empty($data))$not_valid++;
		}
	}//end for	
	
	$return = array();
	$return['not_valid'] 	= $not_valid;
	$return['price']		= $price;
	return $return;
}



function get_roomtype_acco($acco_id){
	global $db;
	$q = $db->prepare_query("select c.lrule_id from lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
							where a.larticle_id=b.lapp_id and b.lrule_id=c.lrule_id and
							c.lrule=%s and c.lgroup=%s and
							a.larticle_type=%s and a.larticle_id=%d",'room_type','villas','villas',$acco_id);
	$r = $db->do_query($q);						
	$d = $db->fetch_array($r);
	if(!empty($d)) return $d['lrule_id'];	
}



function temp_available_villa($acco_id,$image,$villa_name,$desc,$brief,$price,$discount=''){
	//$popup_detail =popup_detail_villa($villa_name,$desc,$acco_id);
	if($image!='') $bg = "style=\"background-image:url($image); background-repeat: no-repeat;background-size: cover;background-position: center center;\"";
	else $bg="";
	$temp = "<div class=\"villa\">
				<div class=\"thumb-villa\" $bg >
					$discount
				</div>
				<div class=\"desc-villa\">
					<div class=\"title\"><label>$villa_name</label></div>
					<div class=\"dvc\">$brief</div>
					<div class=\"dvc-other\">
						<span class=\"price\">From USD ".number_format($price,0)."/night</span>
						<a class=\"view-detail\" href=\"#\" rel=\"$acco_id\">View detail</a>
						<a class=\"btv\" href=\"#\" name=\"book-this-villa\" rel=\"$acco_id\">Book this villa &raquo;</a>
						<div class=\"clear\"></div>
					</div>
				</div>
				<div class=\"clear\"></div>
			</div>";
	return $temp;		
}

function popup_detail_villa($villa_name,$desc,$acco_id){
	$images = set_image_villa_popup($acco_id);	
	$return =  "<div class=\"floating-content\">
					<div class=\"cont-mid\">
						<div class=\"for-floating default-skin \">
							$images
							<div class=\"title\">$villa_name</div>
							<div class=\"content-detail\">$desc</div>							
						</div>
					</div>
					<div class=\"close-floating-content\"></div>
				</div>";
	return $return;				
}

function step_one_set_session(){
	if(isset($_POST['cIn'])){
		$step_1 = array();
		$step_1['type_reservation'] = $_POST['type_reservation'];
		$step_1['cIn']				= $_POST['cIn'];
		$step_1['cOut']				= $_POST['cOut'];		
		$step_1['adult']			= $_POST['adult'];		
		$step_1['child']			= $_POST['child'];		
		$step_1['no_of_bedroom']	= $_POST['no_of_bedroom'];	
		session_start();
		$_SESSION['reservation_ss']['step_1'] = $step_1;			
	}	
}

function get_popup_detail_villa(){
	global $db;
	$acco_id = $_POST['villa_id'];
	$dt = data_tabel('lumonata_articles',"where larticle_type='villas' and larticle_id=$acco_id and larticle_status='publish'",'array');
	if(!empty($dt)){
		$title = $dt['larticle_title'];
		$desc = $dt['larticle_content'];
		$temp = temp_popup_detail_villa($title,$desc,$acco_id);
		echo $temp;
	}
}

function set_image_villa_popup($acco_id){
	global $db;
	$is_image = is_image_mime('mime_type');
	$list_image = data_tabel("lumonata_attachment"," where larticle_id=$acco_id and ($is_image) order by lorder");
	$list = '';$first_image = '';
	$num = 0;
	while($dt = $db->fetch_array($list_image)){
		$medium_size = 'http://'.SITE_URL.'/'.$dt['lattach_loc_medium'];
		if($first_image=='')$first_image=$medium_size;
		$list .="<li>$medium_size</li>";
		$num++;
	}
	$ul = "<ul class=\"list-image-detail\" style=\"display:none\">$list</ul>";
	$return = array();
	$return['ul'] = $ul;
	$return['first_image'] = $first_image;
	$return['num']=$num;
	return $return;
	
}

function temp_popup_detail_villa($title,$desc,$acco_id){
	set_template(PLUGINS_PATH."/reservation/template-popup.html",'template_villa_detail');
	add_block('popup_villa_detail_block','pvdb','template_villa_detail');
	$images_data = set_image_villa_popup($acco_id);
	$images = $images_data['ul'];
	$first_image = $images_data['first_image']; 
	$num_image = $images_data['num'];
	$plugin_url = 'http://'.SITE_URL.'/lumonata-plugins/reservation/';
	if($first_image!=''){
		$nav = "";
		if($num_image>1) $nav = "<div class=\"next\" style=\"background: url(".$plugin_url."img/prevnext.png) no-repeat;\"></div>
								 <div class=\"prev\" style=\"background: url(".$plugin_url."img/prevnext.png) no-repeat;\"></div>";
		$temp_first = "<div class=\"container-image\" style=\"background:url($first_image) no-repeat;\" >
							$nav
						</div>";
	}else $temp_first='';
	
	add_variable('title',$title);
	add_variable('desc',$desc);
	add_variable('images',$images);
	add_variable('temp_first',$temp_first);
	add_variable('plugin_url','http://'.SITE_URL.'/lumonata-plugins/reservation/');
	return return_template('popup_villa_detail_block','pvdb',false);
}


function step_three(){
	global $db;	
	//session_start();
	step_two_set_session();
	set_template(PLUGINS_PATH."/reservation/template.html",'template_step_3');
	add_block('step_3_block','b_step_3','template_step_3');
	
	$step_1 = $_SESSION['reservation_ss']['step_1'];
	add_variable('cIn', $step_1['cIn']);
	add_variable('cOut', $step_1['cOut']);
	add_variable('adult', $step_1['adult']);
	add_variable('child', $step_1['child']);
	add_variable('no_of_bedroom', $step_1['no_of_bedroom']);
	
	$step_2  = $_SESSION['reservation_ss']['step_2'];
	$acco_id = $step_2['villa_id'];
	$cIn				= strtotime($step_1['cIn']);
	$cOut				= strtotime($step_1['cOut']);
	$days				= ($cOut-$cIn)/86400;
	
	$dtv = data_tabel('lumonata_articles',"where larticle_type='villas' and larticle_status='publish' and larticle_id=$acco_id",'array');
	$dt_price = data_prices_villa($cIn,$cOut,$days,$acco_id);//print_r($dt_price); maintenance surecharge yang salah
	$discount = $dt_price['discount_total']+ $dt_price['discount_alltime_total'];
	if($discount!=0 || $discount!='') add_variable('discount',"<p>Discount : USD ".number_format($discount,2)."</p>");
	$early_bird = ($dt_price['early_bird_total']+ $dt_price['early_bird_alltime_total']);
	if($early_bird!=0 || $early_bird!='') add_variable('early_bird',"<p>Early Bird : USD ".number_format($early_bird,2)."</p>");	
	$surecharge = ($dt_price['surecharge_total']+ $dt_price['surecharge_alltime_total']);
	if($surecharge!=0 || $surecharge!='') add_variable('surecharge',"<p>Surecharge : USD ".number_format($surecharge,2)."</p>");
	//print_r($dt_price);
	//$dtav = check_availability_villa($cIn,$cOut,$days,$acco_id);
	add_variable('villa_name',$dtv['larticle_title']);
	add_variable('price_per_night',number_format($dt_price['min_price_per_night'],2));
	add_variable('total',number_format($dt_price['total'],2));
	
	//validasi dp or full payment
	if($dt_price['deposit']<100) $text_payment_metode = 'Deposit Payment '.$dt_price['deposit'].'% : USD '.number_format($dt_price['deposit_rate'],2);
	else $text_payment_metode = 'Full Payment : USD '.number_format($dt_price['deposit_rate'],2);
	
	
	add_variable('payment_metode','<p>'.$text_payment_metode).'</p>';
	
	if(isset($_POST['villa_id'])) echo return_template('step_3_block','b_step_3',false);
	else return return_template('step_3_block','b_step_3',false);	
}



function step_two_set_session(){
	if(isset($_POST['villa_id'])){
		$step_2 = array();
		$step_2['villa_id'] = $_POST['villa_id'];
		session_start();
		$_SESSION['reservation_ss']['step_2'] = $step_2;			
	}	
}

add_actions('view-detail-villa-ajax_page', 'get_popup_detail_villa');
add_actions('list-availability-vp-ajax_page', 'step_two');
add_actions('book-this-vp-ajax_page', 'step_three');

/*RESET SESSION EVENT*/
function back_to_step_1(){
	session_start(); unset($_SESSION['reservation_ss']['step_1']); echo step_one();
}
function back_to_step_2(){
	session_start(); unset($_SESSION['reservation_ss']['step_2']); echo step_two();
}
add_actions('back-to-step-1-ajax_page','back_to_step_1');
add_actions('back-to-step-2-ajax_page','back_to_step_2');
?>