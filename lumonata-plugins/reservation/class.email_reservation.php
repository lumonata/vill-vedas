<?php 
class email_reservation extends db{
	function email_reservation($appName=""){
		$this->appName=$appName;
		$this->setMetaTitle("email reservation");	
	}
	
	function load(){echo 'hihih';}	
	
	function just_detail_booking($book_id,$type_show='',$show_border_bottom=true){
		global $db;
		
		$OUT_TEMPLATE="template-just-detail-booking.html";
		$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html/");
		$t->set_file('template_just_detail', $OUT_TEMPLATE);
		//set block
		$t->set_block('template_just_detail', 'mainBlock',  'mB');	
		
		$code = 'USD';
		
		$bk = $this->data_tabel('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
		$bk_det = $this->data_tabel('lumonata_accommodation_booking_detail',"where lbook_id=$book_id",'array');
		$acco_id = $bk_det['larticle_id'];
		$acco_dt =  $this->data_tabel('lumonata_articles',"where larticle_id=".$acco_id." and larticle_type='villas' and larticle_status='publish'",'array');
		
		$thumb = $this->get_thumb_villa($acco_id);
		if($thumb!='')	$thumb = 'http://'.SITE_URL.$thumb;
		$night = ($bk['lcheck_out'] - $bk['lcheck_in'])/86400;
		
		
		
		$t->set_var('book_id',$bk['lbook_id']);
		$t->set_var('avatar',$thumb);
		$t->set_var('villa_name',$acco_dt['larticle_title']);
		$t->set_var('check_in',date('d M Y',$bk['lcheck_in']));
		$t->set_var('check_out',date('d M Y',$bk['lcheck_out']));
		$t->set_var('night',$night.($night>1 ? ' nights' : ' night'));
		$t->set_var('price',$code.' '.number_format($bk_det['ltotal'],2));
		
		//discount
		$dtDisc = json_decode($bk_det['ldiscount']);
		$discTemp = '';
		if(!empty($dtDisc)){
			$discVal = $dtDisc->discount_total + $dtDisc->discount_alltime_total;
			if($discVal!=0)$discTemp ='<tr><td>Discount</td><td>:</td><td>'.$code.' '.number_format($discVal,2).'</td></tr>';
		}
		$t->set_var('discount',$discTemp);
		
		//early bird
		$dtEarly = json_decode($bk_det['learly_bird']);
		$earlyTemp = '';
		if(!empty($dtEarly)){
			$earlyVal = $dtEarly->early_bird_total + $dtEarly->early_bird_alltime_total;
			if($earlyVal!=0)$earlyTemp ='<tr><td>Early Bird</td><td>:</td><td>'.$code.' '.number_format($earlyVal,2).'</td></tr>';
		}
		$t->set_var('early_bird',$earlyTemp);
		
		//surecharge
		$dtSC = json_decode($bk_det['lsurecharge']);
		$scTemp = '';
		if(!empty($dtSC)){
			$scVal = $dtSC->surecharge_total + $dtSC->surecharge_alltime_total;
			if($scVal!=0)$scTemp ='<tr><td>Surecharge</td><td>:</td><td>'.$code.' '.number_format($scVal,2).'</td></tr>';
		}
		$t->set_var('surecharge',$scTemp);
		
		$t->set_var('first_name',$bk['lfirst_name']);
		$t->set_var('last_name',$bk['llast_name']);	
		$t->set_var('adult',$bk['ladult']);	
		$t->set_var('child',$bk['lchild']);	
		$t->set_var('email',$bk['lemail']);	
		$t->set_var('email',$bk['lemail']);	
		$t->set_var('phone',$bk['lphone']);	
		$t->set_var('special_note',$bk['lspecial_note']);
		
		$t->set_var('border_bottom',($show_border_bottom==true ?"<div style=\"border-bottom:solid 1px rgb(119, 97, 41);width:100%;margin: 25px 0;\"></div>": ""));
		
		$t->set_var('total',number_format($bk['ltotal'],2));
		$this->show_information_payment($type_show,$t,$code,$book_id,json_decode($bk['ldeposit']));
		return $t->Parse('mB', 'mainBlock', false);
	
	}
	
	function show_information_payment($type,$t,$code,$book_id,$dt_deposit){
		//print_r($dt_deposit);
		$txt_deposit = $dt_deposit->deposit."%";
		if($type=='dp'){
			$dtDp = $this->data_tabel('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='dp'",'array');
			$dpAmount = $code.' '.number_format($dtDp['lgross'],2);
			$t->set_var('deposite_dp',"<p style=\"margin:0;\">$txt_deposit Deposit Payment : $dpAmount</p>");	
			$t->set_var('deposite_fp','');	
		}else if($type=='fp'){
			$dtDp = $this->data_tabel('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='dp'",'array');
			$dpAmount = $code.' '.number_format($dtDp['lgross'],2);
			$t->set_var('deposite_dp',"<p style=\"margin:0;\">$txt_deposit Deposit Payment : $dpAmount</p>");
			$dtfp = $this->data_tabel('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='fp'",'array');
			$fpAmount = $code.' '.number_format($dtfp['lgross'],2);	
			$t->set_var('deposite_fp',"<p style=\"margin:0;\">Balance Payment : $fpAmount</p>");
		}elseif($type=='dp_before'){
			$dpAmount = $code.' '.number_format($dt_deposit->rate,2);
			$t->set_var('deposite_dp',"<p style=\"margin:0;\">$txt_deposit Deposit Payment : $dpAmount</p>");	
			$t->set_var('deposite_fp','');	
		}elseif($type=='fp_before'){
			$dpAmount = $code.' '.number_format($dt_deposit->rate,2);
			$t->set_var('deposite_dp',"<p style=\"margin:0;\">$txt_deposit Deposit Payment : $dpAmount</p>");
			$bk = $this->data_tabel('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
			$fpAmount = $code.' '.number_format($bk['ltotal']-$dt_deposit->rate,2);		
			$t->set_var('deposite_fp',"<p style=\"margin:0;\">Balance Payment : $fpAmount</p>");
		}		
	}
	
	
	
	
	function set_content_to_main_email_template($subject,$content){
		require_once(ROOT_PATH."/lumonata-admin/functions/globalAdmin.php");
        $this->globalAdmin = new globalAdmin();
		
		
		$OUT_TEMPLATE="main-email.html";
		$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html/");
		$t->set_file('template_main_email', $OUT_TEMPLATE);
		//set block
		$t->set_block('template_main_email', 'mainBlock',  'mB');	
		$site_url = 'http://'.SITE_URL;
		$t->set_var('site_url',$site_url);
		$t->set_var('subject',$subject);
		$t->set_var('content',$content);
		
		return $t->Parse('mB', 'mainBlock', false);
	}
	
	function best_regrads_email(){
		$OUT_TEMPLATE="template-best-regrads.html";
		$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html/");
		$t->set_file('brb', $OUT_TEMPLATE);
		$t->set_block('brb', 'best_regradsBlock', 'brbBlock');
		
		$t->set_var('site_url','http://'.SITE_URL);
		$temp=$t->Parse('brbBlock', 'best_regradsBlock', false);
		return $temp;
	}
		
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function is_image_mime($field){
		$valid = "$field = 'image/jpg' or $field = 'image/jpeg' or $field = 'image/png'";
		return $valid;
	}
	
	function get_thumb_villa($acco_id){
		$is_image = $this->is_image_mime('mime_type');
		$thumb = $this->data_tabel("lumonata_attachment"," where larticle_id=$acco_id and ($is_image) order by lorder",'array');
		if(!empty($thumb)) return $thumb['lattach_loc_thumb'];
		else return '';
	}	
	
	function get_detail_accommodation($book_id){
		global $db;
		$q = $db->prepare_query("select la.*, 
									lab.lfirst_name first_name, lab.llast_name last_name , lab.ldeposit, lab.lcheck_in, 
									lab.lcheck_out,lab.lphone phone_guest,lab.lemail
									from lumonata_accommodation_booking lab 
									inner join lumonata_accommodation_booking_detail labd on lab.lbook_id=labd.lbook_id
									inner join lumonata_articles la on labd.larticle_id=la.larticle_id 
									where lab.lbook_id=%d",$book_id);				
		$r = $db->do_query($q);							
		return $db->fetch_array($r);
	}

	
	function generate_pdf_receipt($book_id){
		global $db;
		$OUT_TEMPLATE="template-receipt.html";
		$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html/");
		$t->set_file('template_receipt', $OUT_TEMPLATE);
		//set block
		$t->set_block('template_receipt', 'mainBlock',  'mB');	
		$site_url = 'http://'.SITE_URL;
		$t->set_var('site_url',$site_url);
		$t->set_var('subject',$subject);
		$t->set_var('content',$content);
		
		$dac = $this->get_detail_accommodation($book_id);
		
		$dtPay = $this->data_tabel('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='dp'",'array');
		//print_r($dtPay);
		$t->set_var('villa_name',$dac['larticle_title']);
		$t->set_var('book_id',$book_id);
		$t->set_var('date',date('d M Y',$dtPay['lpaid']));
		$t->set_var('guest_name',$dac['first_name'].' '.$dac['last_name']);
		$t->set_var('amount',number_format($dtPay['lgross'],2));
		
		$dtDeposit = json_decode($dac['ldeposit']);
		$t->set_var('deposit',$dtDeposit->deposit.'%');
		$t->set_var('period',date('d M Y',$dac['lcheck_in']).' - '.date('d M Y',$dac['lcheck_out']));
		$t->set_var('paid_by',$dtPay['lpayment_type']);
		
		$folder = date('MY',$dac['lcheck_in']);
		//echo $folder.'#';
		
		$content =  $t->Parse('mB', 'mainBlock', false);
		//echo $content; //exit;
		$file_path = $this->generate_pdf_file($content,$book_id,'dp',$folder);
		return $file_path;
		//echo $content;
		
	}
	
	function generate_pdf_file($content,$book_id,$type,$folder){	
		set_include_path(ROOT_PATH."/dompdf/");
		require_once "dompdf_config.inc.php";	 
		
		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->render();	 
		$output = $dompdf->output();
		
		if(!file_exists(ROOT_PATH."/dompdf/pdf_result/".$folder)) mkdir(ROOT_PATH."/dompdf/pdf_result/".$folder);		
		$file_path = ROOT_PATH."/dompdf/pdf_result/".$folder."/".$type."_".$book_id.".pdf";
		if(file_exists($file_path))unlink($file_path);
		file_put_contents($file_path, $output);	
		return $file_path;
	}
	
	function generate_pdf_invoice($book_id){
		global $db;
		$code = 'USD';
		
		$OUT_TEMPLATE="invoice.html";
		$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/pdf-temp/");
		$t->set_file('template_invoice', $OUT_TEMPLATE);
		//set block
		$t->set_block('template_invoice', 'detail_booking',  'mB');
		$t->set_block('template_invoice', 'mainBlock',  'mB');	
		$dac = $this->get_detail_accommodation($book_id);
		$t->set_var('logo','http://'.SITE_URL."/lumonata-content/themes/villa-vedas/images/just-logo.png");
		$t->set_var('style',"http://".SITE_URL."/lumonata-plugins/reservation/pdf-temp/style-invoice.css");
		
		$dbk = $this->data_tabel('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
		
		//print_r($dac);
		$admContact = $this->contact_admin();
		$t->set_var('admin_email',$admContact['cu_email']);
		$t->set_var('name',$dac['first_name'].' '.$dac['last_name']);
		$t->set_var('phone',$dac['phone_guest']);
		$t->set_var('guest_email',$dac['lemail']);
		
		//dt invoice
		$dtInvoice = $this->data_tabel('lumonata_accommodation_payment',"where lbook_id=$book_id and ltype_paid='fp'",'array');
		$t->set_var('txn_id',$dtInvoice['ltxn_id']);
		$t->set_var('date_invoice',date('d M Y',$dtInvoice['lpaid']));
		$t->set_var('balance_date',date('d M Y',$dbk['lbalance']));
		
		$t->set_var('villa_name',$dac['larticle_title']);
		$desc = $dac['larticle_content'];
		$dt_desc = explode('<!-- pagebreak -->',$desc);
		$brief = $dt_desc[0];
		$t->set_var('brief',$brief);
		
		$dbkd = $this->data_tabel('lumonata_accommodation_booking_detail',"where lbook_id=$book_id",'array');
		$dt_disc = json_decode($dbkd['ldiscount']);
		$disc = $dt_disc->discount_total + $dt_disc->discount_alltime_total;
		$dt_eb = json_decode($dbkd['learly_bird']);
		$earlyBird = $dt_eb->early_bird_total + $dt_eb->early_bird_alltime_total;
		$disc_all = $disc + $earlyBird;
		
		$dt_su = json_decode($dbkd['lsurecharge']);
		$surecharge = $dt_su->surecharge_total + $dt_su->surecharge_alltime_total;
		//$price = ($dbkd['ltotal'] - ($disc+$earlyBird)) + $surecharge;
		$price = $dbkd['ltotal'];
		
		//echo $dbkd['ltotal'] ."+ ($disc+$earlyBird) - $surecharge";
		
		
		$t->set_var('price',$code.' '.number_format($price,2));
		/*$t->set_var('disc',$code.' '.number_format($disc_all,2));
		$t->set_var('tax',$code.' '.number_format($surecharge,2));
		$t->set_var('grand_total',$code.' '.number_format($dbk['ltotal'],2));*/
		$temp_disc = "";
		if(!empty($disc_all)){
			$temp_disc = "<tr><td>DISCOUNT</td><td style=\"padding-right:10px;\">".$code.' '.number_format($disc_all,2)."</td></tr>";
		}
		$t->set_var('disc',$temp_disc);
		$temp_surcharge = "";
		if(!empty($surecharge)){
			$temp_surcharge = "<tr><td>TAX</td><td style=\"padding-right:10px;\">".$code.' '.number_format($surecharge,2)."</td></tr>";
		}
		$t->set_var('tax',$temp_surcharge);
		$temp_grand_total="";
		if(!empty($surecharge) || !empty($disc_all)){
			$temp_grand_total = "<tr><td>GRAND TOTAL</td><td style=\"padding-right:10px;\">".$code.' '.number_format($dbk['ltotal'],2)."</td></tr>";
		}
		$t->set_var('grand_total',$temp_grand_total);
		
		/*$t->set_var('hidden_disc',(empty($disc_all)? 'style="display:none;"':''));
		$t->set_var('hidden_tax',(empty($surecharge)? 'style="display:none;"':''));
		$t->set_var('hidden_grand_total',(empty($surecharge) && empty($disc_all)? 'style="display:none;"':''));*/
		$t->set_var('style_thx',(!empty($surecharge) || !empty($disc_all)? 'style="margin-top:0px;"':''));
		
		$t->Parse('mB', 'detail_booking', true);
		
		
		$folder = date('MY',$dac['lcheck_in']);		
		$content =  $t->Parse('mB', 'mainBlock', false);
		
		//echo $content;exit;
			
		$file_path = $this->generate_pdf_file($content,$book_id,'fp',$folder);
		return $file_path;
		//
	}
	
	
	
	
	function contact_admin(){
		$data_contact = $this->data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];
		/*$contact_us_email = get_additional_field($post_id, 'contact_us_email', $article_type);
		$contact_us_email_cc = get_additional_field($post_id, 'contact_us_cc', $article_type);	*/
		$return = array();
		$return['cu_email'] = get_additional_field($post_id, 'contact_us_email', $article_type);
		$return['cu_email_cc'] = get_additional_field($post_id, 'contact_us_cc', $article_type);	
		return $return;
	}
	
	function save_send_email_history($id,$time,$app_name){
		global $db;
		$q = $db->prepare_query("insert into lumonata_send_email_history (lsend_id,lbook_id,lapp_name) values (%d,%d,%s)",$time,$id,$app_name);
		$db->do_query($q);						
	}
	
	
	function smtp_info(){
		global $db;
		$result = array();
		$dt_email  = $this->data_tabel('lumonata_meta_data',"where lmeta_name = 'email_user_smtp'",'array');
		$result['email'] = $dt_email['lmeta_value'];
		
		$dt_pass_email  = $this->data_tabel('lumonata_meta_data',"where lmeta_name = 'pass_email_user_smtp'",'array');
		$base64_decode_pass = base64_decode($dt_pass_email['lmeta_value']);
		
		$json_decode_pass = json_decode($base64_decode_pass);
		$result['pass'] = $json_decode_pass->p_e_u_smtp;
		return $result;		
	}
	
	
	function setMetaTitle($metaTitle=''){
		$this->meta_title=$metaTitle;
	}
	function getMetaTitle(){
		return $this->meta_title;
	}
	function setMetaDescriptions($metaDesc=''){
		$this->meta_desc=$metaDesc;
	}
	function getMetaDescriptions(){
		return $this->meta_desc;
	}
	function setMetaKeywords($metaKey=''){
		$this->meta_key=$metaKey;
	}
	function getMetaKeywords(){
		return $this->meta_key;
	}
	
	
}
?>