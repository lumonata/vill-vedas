<?php 
function send_email_success_payment_admin($type,$dbk){
	global $db;
	require_once(ROOT_PATH."/lumonata-plugins/reservation/class.email_reservation.php");
	$email_reservation =  new email_reservation();
	$dac = $email_reservation->get_detail_accommodation($dbk['lbook_id']);
	$dt_depos = json_decode($dbk['ldeposit']);
	//print_r($dt_depos);
	if($type=='dp') $subject = 'Receive Down Payment Nina Bali Villa from '.$dbk['lbook_id'];
	if($type=='fp') $subject = 'Receive '.($dt_depos->deposit=='100'? 'Full Payment':'Balance Payment').' Nina Bali Villa from '.$dbk['lbook_id'];	
	
	
	$OUT_TEMPLATE="temp-email-confirm-payment-admin.html";
	$t=new Template(ROOT_PATH."/lumonata-plugins/reservation/email-html/");
	$t->set_file('template_ecpa', $OUT_TEMPLATE);
	//set block
	$t->set_block('template_ecpa', 'ecpd_Block',  'mB');	
	if($type=='dp')$txt_type = 'down payment';
	if($type=='fp')$txt_type = ($dt_depos->deposit=='100'? 'full payment' : 'balance payment');
	$t->set_var('type_payment',$txt_type);		
	$detail = $email_reservation->just_detail_booking($dbk['lbook_id'],$type,false);
	$t->set_var('booking_detail',$detail);
	$content =  $t->Parse('mB', 'ecpd_Block', false);
	$message = $email_reservation->set_content_to_main_email_template($subject,$content);
	
	$data_contact = data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
	$post_id = $data_contact['larticle_id'];
	$article_type = $data_contact['larticle_type'];
	$contact_us_email = get_additional_field($post_id, 'contact_us_email', $article_type);
	$contact_us_email_cc = get_additional_field($post_id, 'contact_us_cc', $article_type);	
	
	$data_smtp = get_smtp_info();
	$email_smtp = $data_smtp['email'];
	$pass_smtp = $data_smtp['pass'];
	$web_name = get_meta_data('web_title');
	$SMTP_SERVER = get_meta_data('smtp');
	//echo $message; exit;
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
	$mail = new PHPMailer(true);	
	try {
		$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Port       = 2525; 
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($contact_us_email,$web_name);
		$mail->AddAddress($contact_us_email,$web_name);
		$mail->SetFrom($contact_us_email, $web_name);
		$mail->Subject = $subject;
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$mail->Send();
	}catch (phpmailerException $e) {
		echo $e->errorMessage().$web_name;
	}catch (Exception $e) {
		echo $e->getMessage();
	}
}



?>