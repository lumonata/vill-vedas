<?php 
function send_email_check_availability($book_id){
	global $db;
	require_once(ROOT_PATH."/lumonata-plugins/reservation/class.email_reservation.php");
	$email_reservation = new email_reservation(true);
	$q = $db->prepare_query("select * from lumonata_accommodation_booking where lbook_id=%d",$book_id);
	$r = $db->do_query($q);
	$return = array();
	
	while($dt = $db->fetch_array($r)){
		$content = "<p style=\"margin: 0; text-align: center;font-size: 30px; font-weight: 300;font-family:'latoregular',sans-serif;margin-bottom:25px;\">CHECK AVAILABILITY</p>					
					<table cellpadding=\"0\" cellspacing=\"0\" style=\"margin:10px 0 ;padding: 0;width:100%;\">
						<tr>
							<td style=\"width:35%;\"></td>
							<td style=\"width:30%;padding-bottom:25px;\">
								<table style=\"width: 100%;\">
									<tr>
										<td style=\"border-bottom:solid 1px #000;\"></td>
									</tr>
								</table>
							</td>
							<td style=\"width:35%;\"></td>
						</tr>
					</table>
							
					<div>
						<p style=\"padding: 5px 0 0\">Congratulations,</p>
						<p style=\"padding: 5px 0 0\">You get a new inquiry from the website. Please check the Administrator pages.</p>
					</div>";	
		$subject = "New inquiry from ".$dt['lfirst_name']." ".$dt['llast_name'];
		
		//echo 'dsadad';
		$message = $email_reservation->set_content_to_main_email_template($subject,$content);
		//echo $message; exit;
		//return $message;		
		$data_contact = data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];
		$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
		$contact_us_email_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
		
		$data_smtp = get_smtp_info();
		$email_smtp = $data_smtp['email'];
		$pass_smtp = $data_smtp['pass'];
		
		$web_name = get_meta_data('web_title');
		$SMTP_SERVER = get_meta_data('smtp');
		
		$email = $dt['lemail'];
		$name = $dt['lfirst_name'].' '.$dt['llast_name']; 
		
		//echo "$contact_us_email # $contact_us_email_cc # $email_smtp # $pass_smtp # $SMTP_SERVER";
		require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
		$mail = new PHPMailer(true);		
		try {
			$mail->SMTPDebug  = 1;
			$mail->IsSMTP();
			$mail->Host = $SMTP_SERVER;
			$mail->SMTPAuth = true;
			
			$mail->Port       = 2525; 
			$mail->Username   = $email_smtp;
			$mail->Password   = $pass_smtp;
			$mail->AddReplyTo($email, $name);
			$mail->AddAddress($contact_us_email,$web_name);
			
			/*if($contact_us_email_cc!=''){$mail->AddAddress($contact_us_email_cc,$web_name);}*/
			$mail->SetFrom($email_smtp,$web_name);
			$mail->Subject = $subject;
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

			$mail->MsgHTML($message);
			$mail->Send();
			$return['process'] = 'success';
			$return['data'] = 'Send email successfully done, we will immediately respond later';
		}catch (phpmailerException $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
			$return['error_info'] = $mail->ErrorInfo;
		}catch (Exception $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
			$return['error_info'] = $mail->ErrorInfo;
		}
		
	}//end while
	//print_r($return);
	return $return;
}
function test_email_check_availability(){
	echo send_email_check_availability($_POST['book_id']);
}
add_actions('test-email-check-availability-ajax_page', 'test_email_check_availability');
?>