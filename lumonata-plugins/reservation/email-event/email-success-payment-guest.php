<?php
function send_email_success_payment_guest($type,$dbk){
	global $db;
	require_once(ROOT_PATH."/lumonata-plugins/reservation/class.email_reservation.php");
	$email_reservation =  new email_reservation();
	
	$dac= $email_reservation->get_detail_accommodation($dbk['lbook_id']);
	$dt_email = content_email_guest($type,$dac['larticle_title'],date('d M Y',$dbk['lcheck_in']),date('d M Y',$dbk['lcheck_out']));
	$message = $email_reservation->set_content_to_main_email_template($dt_email['subject'],$dt_email['text_detail']);
	$subject = $dt_email['subject'];
	
	if($type=='dp') $url_pdf = $email_reservation->generate_pdf_receipt($dbk['lbook_id']);
	if($type=='fp') $url_pdf = $email_reservation->generate_pdf_invoice($dbk['lbook_id']);
	//echo $message; exit;
	
	$data_contact = data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
	$post_id = $data_contact['larticle_id'];
	$article_type = $data_contact['larticle_type'];
	$contact_us_email = get_additional_field($post_id, 'contact_us_email', $article_type);
	$contact_us_email_cc = get_additional_field($post_id, 'contact_us_cc', $article_type);	
	
	$data_smtp = get_smtp_info();
	$email_smtp = $data_smtp['email'];
	$pass_smtp = $data_smtp['pass'];
	$web_name = get_meta_data('web_title');
	$SMTP_SERVER = get_meta_data('smtp');
	$to = $dbk['lemail'];
	$guest_name = $dbk['lfirst_name'].' '.$dbk['llast_name'];
	
	//echo "$contact_us_email,$contact_us_email_cc,$email_smtp,$pass_smtp,$web_name,$SMTP_SERVER,$to,$guest_name,$url_pdf";exit;	
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
	require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
	$mail = new PHPMailer(true);	
	try {
		$mail->SMTPDebug  = 1;
		$mail->IsSMTP();
		$mail->Host = $SMTP_SERVER;
		$mail->SMTPAuth = true;
		$mail->Port       = 2525; 
		$mail->Username   = $email_smtp;  // GMAIL username
		$mail->Password   = $pass_smtp; // GMAIL password
		$mail->AddReplyTo($contact_us_email, $web_name);
		$mail->AddAddress($to,$guest_name);
		$mail->SetFrom($contact_us_email, $web_name);
		$mail->Subject = $subject;
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$mail->AddAttachment($url_pdf);
		$mail->Send();
	}catch (phpmailerException $e) {
		echo $e->errorMessage().$web_name;
	}catch (Exception $e) {
		echo $e->getMessage();
	}
	
	//echo $message;
}


function content_email_guest($type,$villa_name,$check_in,$check_out){
	if($type=='dp'){
		$text_detail = "<p style=\"margin:15px 0 0 0;\">Greetings from Villa Vedas. </p>
						<p style=\"margin:5px 0 15px;\">Thank you for your payment of the deposit and your booking is now confirmed. We will send you a 
reminder when the balance is due. </p>
						<p style=\"margin:15px 0\">Please see attached your receipt for payment of the deposit.</p>";
		$subject = "Receipt for payment of the deposit. $villa_name, ($check_in/$check_out).";
	}
	if($type=='fp'){
		$link = "<a href=\"http://www.adobe.com/products/reader.html\">click here</a>";
		$text_detail = "<p style=\"margin:15px 0;\">Your booking with Villa Vedas is confirmed and complete. </p>
						<p style=\"margin:15px 0 5px;\">Please present either an electronic or paper copy of your hotel voucher upon check-in. </p>
						<p style=\"margin:15px 0\">If you experience problems downloading or opening the voucher please $link to install Adobe Reader. </p>
						<p>We look forward in having you stay in one of our beautiful villas.</p>";
		$subject = "Your booking is now confirmed. $villa_name, ($check_in/$check_out)";
	}
	$return = array();
	$return['text_detail'] = $text_detail;
	$return['subject'] = $subject;
	return $return;
}

 ?>