<?php 
/*
For general function email reservation and include each event send email reservation
*/

function just_detail_booking($book_id,$type_show='',$show_border_bottom=true){
	global $db;
	set_template(PLUGINS_PATH."/reservation/email-html/template-just-detail-booking.html",'template_just_detail');
	add_block('mainBlock','mB','template_just_detail');
	$code = 'USD';
	
	$bk = data_tabel('lumonata_accommodation_booking',"where lbook_id=$book_id",'array');
	$bk_det = data_tabel('lumonata_accommodation_booking_detail',"where lbook_id=$book_id",'array');
	$acco_id = $bk_det['larticle_id'];
	$acco_dt =  data_tabel('lumonata_articles',"where larticle_id=".$acco_id." and larticle_type='villas' and larticle_status='publish'",'array');
	
	$thumb = get_thumb_villa($acco_id);
	if($thumb!='')	$thumb = 'http://'.SITE_URL.$thumb;
	$night = ($bk['lcheck_out'] - $bk['lcheck_in'])/86400;
	
	add_variable('book_id',$bk['lbook_id']);
	add_variable('avatar',$thumb);
	add_variable('villa_name',$acco_dt['larticle_title']);
	add_variable('check_in',date('d M Y',$bk['lcheck_in']));
	add_variable('check_out',date('d M Y',$bk['lcheck_out']));
	add_variable('night',$night.($night>1 ? ' nights' : ' night'));
	add_variable('price',$code.' '.number_format($bk_det['ltotal'],0));
	
	//discount
	$dtDisc = json_decode($bk_det['ldiscount']);
	$discTemp = '';
	if(!empty($dtDisc)){
		$discVal = $dtDisc->discount_total + $dtDisc->discount_alltime_total;
		if($discVal!=0)$discTemp ='<tr><td>Discount</td><td>:</td><td>'.$code.' '.number_format($discVal,0).'</td></tr>';
	}
	add_variable('discount',$discTemp);
	
	//early bird
	$dtEarly = json_decode($bk_det['learly_bird']);
	$earlyTemp = '';
	if(!empty($dtEarly)){
		$earlyVal = $dtEarly->early_bird_total + $dtEarly->early_bird_alltime_total;
		if($earlyVal!=0)$earlyTemp ='<tr><td>Early Bird</td><td>:</td><td>'.$code.' '.number_format($earlyVal,0).'</td></tr>';
	}
	add_variable('early_bird',$earlyTemp);
	
	//surecharge
	$dtSC = json_decode($bk_det['lsurecharge']);
	$scTemp = '';
	if(!empty($dtSC)){
		$scVal = $dtSC->surecharge_total + $dtSC->surecharge_alltime_total;
		if($scVal!=0)$scTemp ='<tr><td>Surecharge</td><td>:</td><td>'.$code.' '.number_format($scVal,0).'</td></tr>';
	}
	add_variable('surecharge',$scTemp);
	
	add_variable('first_name',$bk['lfirst_name']);
	add_variable('last_name',$bk['llast_name']);	
	add_variable('adult',$bk['ladult']);	
	add_variable('child',$bk['lchild']);	
	add_variable('email',$bk['lemail']);	
	add_variable('email',$bk['lemail']);	
	add_variable('phone',$bk['lphone']);	
	add_variable('special_note',$bk['lspecial_note']);	
	
	/*echo $thumb;
	print_r($bk);
	print_r($bk_det);
	print_r($acco_dt);*/
	
	return return_template('mainBlock','mB',false);	
}

/*function test_just_detail_booking(){
	echo just_detail_booking($_POST['book_id'],$_POST['type_show']);
}*/


function set_content_to_main_email_template($subject,$content){
	require_once(ROOT_PATH."/lumonata-admin/functions/globalAdmin.php");
	//$globalAdmin = new globalAdmin();
	set_template(PLUGINS_PATH."/reservation/email-html/main-email.html",'template_main_email');
	add_block('mainBlock','mB','template_main_email');
	
	$site_url = 'http://'.SITE_URL;
	add_variable('site_url',$site_url);
	add_variable('subject',$subject);
	add_variable('content',$content);
	
	$post_id	= 221;
	$facebook 	= get_additional_field( $post_id, 'facebook_url', 'social_media');
	$twitter 	= get_additional_field( $post_id, 'twitter_url', 'social_media');
	$instagram 	= get_additional_field( $post_id, 'instagram_url', 'social_media');
	
	add_variable('facebook',($facebook!='' ? "<td style=\"padding:5px\"><a href=\"$facebook\">
													<img src=\"$site_url/lumonata-content/themes/villa-vedas/images/facebook2.svg\" width=\"20\" /></a></td>":""));
	add_variable('twitter',($twitter!=''? "<td style=\"padding:5px\"><a href=\"$twitter\">
													<img src=\"$site_url/lumonata-content/themes/villa-vedas/images/twitter2.svg\" width=\"20\" /></a></td>":""));
	add_variable('instagram',($instagram!=''?" <td style=\"padding:5px\"><a href=\"$instagram\">
													<img src=\"$site_url/lumonata-content/themes/villa-vedas/images/instagram.svg\" width=\"20\" /></a></td>":""));
	
	return return_template('mainBlock','mB',false);	
}



add_actions('test-just-detail-booking-ajax_page','test_just_detail_booking');

require_once('email-event/email-check-availability.php');

?>