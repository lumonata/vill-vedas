<?php 
function step_four(){
	global $db;	
	//session_start();
	step_three_set_session();
	
	$ss1 = $_SESSION['reservation_ss']['step_1'];
	$ss2 = $_SESSION['reservation_ss']['step_2'];
	$ss3 = $_SESSION['reservation_ss']['step_3'];
	
	$book_id=time();
	$type_reservation 	= $ss1['type_reservation'];
	$cIn				= strtotime($ss1['cIn']);
	$cOut				= strtotime($ss1['cOut']);
	$days				= ($cOut-$cIn)/86400;
	$adult  			= ($ss3['adult'] !='' || $ss3['adult'] !=0 ? $ss3['adult'] : 0 );
	$child 				= ($ss3['child'] !='' || $ss3['child'] !=0 ? $ss3['child'] : 0 );
	$no_of_bedroom		= ($ss1['no_of_bedroom'] !='' || $ss1['no_of_bedroom'] !=0 ? $ss1['no_of_bedroom'] : 0);
	$acco_id			= $ss2['villa_id'];
	
	$first_name 		= $ss3['first_name'];
	$last_name			= $ss3['last_name'];
	$email				= $ss3['email'];
	$phone				= ($ss3['phone'] != '' ? $ss3['phone'] : '-');
	$adult				= ($ss3['adult'] != '' && $ss3['adult'] !=0 ? $ss3['adult'] : 0); 
	$child				= ($ss3['child'] != '' && $ss3['child'] !=0 ? $ss3['child'] : 0); 
	$special_notes		= nl2br($ss3['special_notes']);
	
	$dt_price = data_prices_villa($cIn,$cOut,$days,$acco_id);
	
	
	$price_without_discount = $dt_price['price_without_discount'];
	$discount = ($dt_price['discount_total'] + $dt_price['discount_alltime_total']);
	$early_bird = ($dt_price['early_bird_total'] + $dt_price['early_bird_alltime_total']);
	$surecharge = ($dt_price['surecharge_total'] + $dt_price['surecharge_alltime_total']);
	$total = $price_without_discount - ($discount + $early_bird) + $surecharge;
	
//	echo $total; print_r($dt_price);exit;
	
	$gb_deposite = data_tabel('lumonata_meta_data',"WHERE lmeta_name='dp_presentase' and lapp_name='global_setting'",'array');
	$dt_deposite = array();
	$dt_deposite['deposit'] = $dt_price['deposit'];
	$dt_deposite['rate']	= $dt_price['deposit_rate'];
	/*if(!empty($dt_deposite)){
		$dt_deposite['deposit'] = $gb_deposite['lmeta_value'];
		$dt_deposite['rate']	= ($total * $gb_deposite['lmeta_value']) /100;
	}else {
		$dt_deposite['deposit'] = 50;
		$dt_deposite['rate']	= $total/2;
	}*/
	$dt_deposite = json_encode($dt_deposite);
	
	$gb_balance = data_tabel('lumonata_meta_data',"WHERE lmeta_name='due_date_final_payment' and lapp_name='global_setting'",'array');
	$gb_cronjob_balance = data_tabel('lumonata_meta_data',"WHERE lmeta_name='days_crojob_final_payment' and lapp_name='global_setting'",'array');
	
	$balance_date = $cIn - ($gb_balance['lmeta_value'] * 86400);
	/*$cron_job_balance = $balance_date - ($gb_cronjob_balance['lmeta_value'] * 86400);
	$dt_balance = array();
	$dt_balance['balance_date'] = $balance_date;
	$dt_balance['cron_job_balance'] = $cron_job_balance;
	$dt_balance = json_encode($dt_balance);*/
	
	
	//print_r($dt_price); exit;
	$error = 0;
	if(save_reservation_detail($book_id,$acco_id,$price_without_discount,$dt_price)){
		if(save_reservation($book_id,$cIn,$cOut,$first_name,$last_name,$email,$phone,$adult,$child,$special_notes,$total,$type_reservation,$dt_deposite,$balance_date)){
			if(!change_status_availability_villa($acco_id,$cIn,$cOut,$days))$error++;
		}else $error++;
	}else $error++;
	
	$result = array();
	if($error==0){
		$result['process'] = 'success';
		$send_email = send_email_check_availability($book_id);//send email to admin
		if(!empty($send_email) && $send_email['process'] == 'success') $result['process'] = 'success';
		else $result['process'] = 'failed';
		 $result['process'] = 'success';
	}else{
		$result['process'] 	= 'failed';
	}	
	
	unset($_SESSION['reservation_ss']);
	echo json_encode($result);
}

function save_reservation($book_id,$cIn,$cOut,$first_name,$last_name,$email,$phone,$adult,$child,$special_notes,$total,$type_reservation,$dt_deposite,$balance){
	global $db;	
	
	$q = $db->prepare_query("insert into lumonata_accommodation_booking 
							(lbook_id,lcheck_in,lcheck_out,lfirst_name,llast_name,
							 lemail,lphone,ladult,lchild,lspecial_note,
							 ltotal,ldeposit,lbalance,lstatus,lbook_type) values 
							 (%d,%d,%d,%s,%s,
							  %s,%s,%d,%d,%s,
							  %s,%s,%d,%d,%s)",
							  $book_id,$cIn,$cOut,$first_name,$last_name,
							  $email,$phone,$adult,$child,$special_notes,
							  $total,$dt_deposite,$balance, 1,$type_reservation);
	if($db->do_query($q))return true;
	else return false;						  
}

function save_reservation_detail($book_id,$acco_id,$price_without_discount,$dt_price){
	$dt_disc = array();
	$dt_disc['discount_total'] 			= $dt_price['discount_total'];
	$dt_disc['discount_txt'] 			= $dt_price['discount_txt'];
	$dt_disc['discount_alltime_total'] 	= $dt_price['discount_alltime_total'];
	$dt_disc['discount_alltime_txt'] 	= $dt_price['discount_alltime_txt'];
	$dt_disc = json_encode($dt_disc);
	
	$dt_early_bird = array();
	$dt_early_bird['early_bird_total'] 			= $dt_price['early_bird_total'];
	$dt_early_bird['early_bird_txt'] 			= $dt_price['early_bird_txt'];
	$dt_early_bird['early_bird_alltime_total'] 	= $dt_price['early_bird_alltime_total'];
	$dt_early_bird['early_bird_alltime_txt'] 	= $dt_price['early_bird_alltime_txt'];
	$dt_early_bird = json_encode($dt_early_bird);
	
	$dt_surecharge = array();
	$dt_surecharge['surecharge_total'] 			= $dt_price['surecharge_total'];
	$dt_surecharge['surecharge_txt'] 			= $dt_price['surecharge_txt'];
	$dt_surecharge['surecharge_txt_full'] 		= $dt_price['surecharge_txt_full'];
	$dt_surecharge['surecharge_alltime_total'] 	= $dt_price['surecharge_alltime_total'];
	$dt_surecharge['surecharge_alltime_txt'] 	= $dt_price['surecharge_alltime_txt'];
	$dt_surecharge['surecharge_alltime_txt_full']	= $dt_price['surecharge_alltime_txt_full'];
	$dt_surecharge = json_encode($dt_surecharge);
	
	global $db;
	$q = $db->prepare_query("insert into lumonata_accommodation_booking_detail (lbook_id,larticle_id,ltotal,ldiscount,learly_bird,lsurecharge) 
							values (%d,%d,%d,%s,%s,%s)",$book_id,$acco_id,$price_without_discount,$dt_disc,$dt_early_bird,$dt_surecharge);
	if($db->do_query($q))return true;
	else return false;						
	
}


function change_status_availability_villa($villa_id,$arrival_date,$departure_date,$days,$status=5){
	global $db;
	$error = 0;
	for($i=0;$i<$days;$i++){
		$date = $arrival_date + ($i * 86400);
		//echo date('d M Y h:i:s',$date).'#';
		$n = data_tabel('lumonata_availability',"WHERE ldate=$date AND lacco_id=$villa_id",'num_row');		
		if($n > 0){
			
			
			
			$update		= $db->prepare_query("update lumonata_availability set lstatus=%d where ldate=%d and lacco_id=%d",$status,$date,$villa_id);
			$do_update	= $db->do_query($update);
			if($do_update) $error;
			else $error++;
		}
	}
	if($error==0) return true;
	else return false;
}

function is_cIn_cOut_booking($villa_id,$date,$field){
	global $db;
	$q = $db->prepare_query("select lab.lbook_id, lab.lcheck_in, lab.lcheck_out, labd.larticle_id 
			from lumonata_accommodation_booking lab inner join lumonata_accommodation_booking_detail labd
			on lab.lbook_id=labd.lbook_id where lab.$field=%d and labd.larticle_id=%d",$date,$villa_id);
			
	$r = $db->do_query($q);		
	$d = $db->fetch_array($r);
	if(!empty($d))return true;
	else return false;
	
}



function step_three_set_session(){
	$step_3 = array();
	$step_3['first_name']	= $_POST['first_name'];
	$step_3['last_name'] 	= $_POST['last_name'];
	$step_3['email'] 		= $_POST['email'];
	$step_3['phone'] 		= $_POST['phone'];
	$step_3['adult'] 		= $_POST['adult'];
	$step_3['child'] 		= $_POST['child'];
	$step_3['special_notes'] = $_POST['special_notes'];
	session_start();
	$_SESSION['reservation_ss']['step_3'] = $step_3;	
}

add_actions('save-reservation-ajax_page', 'step_four');

?>