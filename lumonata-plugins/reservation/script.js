jQuery(document).ready(function(e) {
	set_event_reservation();
	
	set_window_resize();
	jQuery(window).resize(function(){
		set_window_resize();
	});

});





function set_height_availabelity(){
	var w = jQuery(window).width();
	var hra = jQuery("#result-availability").height();	
	var hla = jQuery("#list-available-villa").height();
	if(w>640) hla = hla+100;
	
	if(hla>hra) jQuery("#result-availability").addClass('scroll');
	else jQuery("#result-availability").removeClass('scroll');
	
}

function set_event_reservation(){
	set_height_availabelity();
	
	jQuery(".view-detail").unbind('click');
	jQuery(".view-detail").click(function(){
		var villa_id = jQuery(this).attr('rel');
		var url = 'http://'+jQuery('[name=site_url]').val()+'/view-detail-villa-ajax/';
		jQuery('.black-overlay').addClass('with-loader').fadeIn(80);
		jQuery.post(url,{'villa_id':villa_id},function(response){
			jQuery('#popup-container').html(response);
			
			var hf = jQuery('.for-floating.default-skin').height();	
			var ct = jQuery('.cont-mid').height();
			if(hf > ct){
				var hn = (ct*95)/100;
				jQuery('.for-floating.default-skin').css({'overflow-y':'scroll','height':hn+'px'});
			}
			popup_detail_event();
			jQuery('#popup-container .floating-content').css('opacity',1);
			jQuery('#popup-container').fadeIn(80);
		});
	});	
	
	jQuery('.black-overlay').unbind('click');
	jQuery('.black-overlay').click(function(){
		jQuery('#popup-container,.black-overlay').fadeOut(100);
		jQuery('.black-overlay').removeClass('with-loader');
	});
	
	jQuery('[name=check-availability]').unbind('click');
	jQuery('[name=check-availability]').click(function(){
		if(validate_form_1()) check_availability();	
	});
	
	jQuery('[name=back-step]').unbind('click');
	jQuery('[name=back-step]').click(function(){
		var rel =  jQuery(this).attr('rel');
		back_step(rel);	
	});
	
	jQuery('[name=book-this-villa]').unbind('click');
	jQuery('[name=book-this-villa]').click(function(){
		var rel =  jQuery(this).attr('rel');
		book_this_villa(rel);	
	});
	set_timeframe();
	
	jQuery('[name=book-now]').unbind('click');
	jQuery('[name=book-now]').click(function(){
		if(validate_form_2()) book_now();
	});
	
	reset_validate_form_1();
	
	jQuery('[name=retype_email]').unbind('on');
	jQuery('[name=retype_email]').on('paste', function () {
		var title = 'Alert';
		var desc = "Can't paste email here. Please typed.";
		show_minimal_popup(title,desc)
	});
	
	
	jQuery('.close-spf').unbind('click');
	jQuery('.close-spf').click(function(){
		jQuery('.small-popup-info,.big-loader').fadeOut(80);
		jQuery('[name=retype_email]').focus();
	});
	//var w  =jQuery(window).width();
	/*if(jQuery('#sec-book-info').length>0 && w <=1024 ){
		var h_desc_stay = jQuery('#your-stay .desc-stay').height();
		var h_block_info = jQuery('#your-stay .block-info').height();
		
		h = h_desc_stay+h_block_info+20;
		console.log(h);
		jQuery('#your-stay').css('height',h+'px');
	}*/
	
	/*var h_desc_stay = jQuery('#your-stay .desc-stay').height();
	var h_block_info = jQuery('#your-stay .block-info').height();
	var h_your_stay = 	 h_desc_stay+h_block_info+20;
	if(jQuery('#sec-book-info').length>0 && w <=1024 ){	
		jQuery('#your-stay').css('height',h_your_stay+'px');
	}
	
	if(jQuery('#result-availability').length>0 && w<=1024){
		jQuery('#result-availability').css('top',h_your_stay+'px');
	}*/
	
}

function show_minimal_popup(title,desc){
	jQuery('.header-spf').text(title);
	jQuery('.desc-spf').text(desc);
	jQuery('.small-popup-info,.big-loader').fadeIn(80);
}

function set_window_resize(){
	/*var w  =jQuery(window).width();
		
	var h_desc_stay = jQuery('#your-stay .desc-stay').height();
	var h_block_info = jQuery('#your-stay .block-info').height();
	console.log(w);
	if(w<=480 && w>415){
		console.log(h_desc_stay);
		console.log(h_block_info);
		var h_your_stay = (h_desc_stay>h_block_info? h_desc_stay: h_block_info)+20;		
	}else if(w<415){
		var h_your_stay = 	 h_desc_stay+h_block_info+20;	
	}
	console.log(h_your_stay)	
	if(jQuery('#sec-book-info').length>0 && w <=480 ){	
		jQuery('#your-stay').css('height',h_your_stay+'px');
	}else jQuery('#your-stay').removeAttr('style');
	if(jQuery('#result-availability').length>0 && w<=480){
		jQuery('#result-availability').css('top',h_your_stay+'px');
	}else jQuery('#result-availability').removeAttr('style');*/
	
}



function popup_detail_event(){
	jQuery('.floating-content .close').click(function(){
		jQuery('#popup-container,.black-overlay').fadeOut(100);
		jQuery('.black-overlay').removeClass('with-loader');
	});	
	jQuery('.list-image-detail li:nth-child(1)').addClass('selected');
	
	jQuery('.container-image .next').click(function(){
		if(jQuery('.list-image-detail li.selected').is(':last-child')){
			var next = 	jQuery('.list-image-detail li:nth-child(1)');
		}else var next = 	jQuery('.list-image-detail li.selected').next();
		jQuery('.list-image-detail li').removeClass('selected');
		var url = jQuery(next).addClass('selected').html();
		jQuery('.container-image').css('background-image','url('+url+')');
	});
	
	jQuery('.container-image .prev').click(function(){
		if(jQuery('.list-image-detail li.selected').is(':first-child')){
			var prev = 	jQuery('.list-image-detail li:last-child');
		}else var prev = 	jQuery('.list-image-detail li.selected').prev();
		jQuery('.list-image-detail li').removeClass('selected');
		var url = jQuery(prev).addClass('selected').html();
		jQuery('.container-image').css('background-image','url('+url+')');
	});
	
}
function validate_form_1(){
	jQuery('#cont-reservation .error').removeClass('error');
	jQuery('.info-state').html('').removeClass('error-info success-info');
	var cIn 	= jQuery('[name=cIn]').val();
	var cOut 	= jQuery('[name=cOut]').val();
	var adult 	= jQuery('[name=adult]').val();
	var child 	= jQuery('[name=child]').val();
	var no_of_bedroom 	= jQuery('[name=no_of_bedroom]').val();
	var error=0; var txt = '';
	if(cIn=='')	{
		jQuery('[name=cIn]').addClass('error');
		txt = 'Check In';
		error++;
	}
	
	if(cOut=='')	{
		jQuery('[name=cOut]').addClass('error');
		if(txt!='') txt += ', ';
		txt += 'Check Out';
		error++;
	}
	
	if(no_of_bedroom=='' || no_of_bedroom=='NO OF BEDROOM')	{
		jQuery('[name=no_of_bedroom]').addClass('error');
		if(txt!='') txt += ', ';
		txt += 'No Of Bed';
		error++;
	}
	
	if(txt!=''){
		jQuery('.info-state').html('<p>'+txt+' is required</p>');	
	}
	
	if(adult!='' && isNaN(adult)){
		error++;
		jQuery('[name=adult]').addClass('error');
		jQuery('.info-state p:last-child').after('<p>Adult must be numeric</p>');
	}
	if(child!='' && isNaN(child)){
		error++;
		jQuery('[name=child]').addClass('error');
		jQuery('.info-state p:last-child').after('<p>Child must be numeric</p>');
	}
	
	
	if(error==0){
		return true;	
	}else{
		jQuery('.info-state').removeClass('error-info').addClass('error-info');
		return false;	
	}
}

function reset_validate_form_1(){
	jQuery('[name=cIn]').click(function(){
		jQuery(this).removeClass('error').attr('placeholder','CHECK IN');
	});	
	jQuery('[name=cOut]').click(function(){
		jQuery(this).removeClass('error').attr('placeholder','CHECK OUT');
	});	
	jQuery('[name=no_of_bedroom]').click(function(){
		jQuery(this).removeClass('error');
		jQuery('[name=no_of_bedroom] option[value="NO OF BEDROOM"').text('NO OF BEDROOM');
	});
}


function check_availability(){
	var url = 'http://'+jQuery('[name=site_url]').val()+'/list-availability-vp-ajax/';
	var data = new Object;
		data.pKEY 	= 'is_use_ajax';
		data.cIn 	= jQuery('[name=cIn]').val();
		data.cOut	= jQuery('[name=cOut]').val();
		data.adult	= jQuery('[name=adult]').val();
		data.child	= jQuery('[name=child]').val();
		data.no_of_bedroom = jQuery('[name=no_of_bedroom]').val();
		data.type_reservation = jQuery('[name=type_reservation]').val();
	jQuery('.big-loader').fadeIn(80);	
	jQuery.post(url,data,function(response){
		jQuery('#sec-form').fadeOut(80,function(){
			jQuery('#layout-top.reservation-page').css('opacity,0');
			jQuery('#sec-form').remove();
			//jQuery('#layout-top.reservation-page').html('');
			jQuery('#layout-top.reservation-page').append(response);
			
			//set_height_availabelity();
			jQuery('#layout-top.reservation-page').animate({'opacity':1},80,function(){
				set_event_reservation();
				jQuery('.big-loader').fadeOut(80);	
			});
		});
	});
}

function back_step(rel){
	var url = 'http://'+jQuery('[name=site_url]').val()+'/'+rel+'/';	
	jQuery('.big-loader').fadeIn(80);
	jQuery.post(url,{pKEY:'is_use_ajax'},function(response){
		jQuery('#layout-top.reservation-page').animate({'opacity':0},80,function(){
			jQuery('#layout-top.reservation-page section').remove();
			jQuery('#layout-top.reservation-page').append(response);
			jQuery('#layout-top.reservation-page').animate({'opacity':1},80,function(){
				set_event_reservation();
				jQuery('.big-loader').fadeOut(80);	
			});
		});
	});
}
function set_timeframe(){
	if(jQuery('[name=cIn]').length){
		new Timeframe('calendars', {
        startField: 'cIn',
        endField: 'cOut',
        earliest: new Date(),
		format: '%d %b %Y',
        resetButton: 'reset' });
		
		jQuery('#calendars #calendars_menu').before('<div class="just-triangular"></div>')
		
		jQuery('[name=cIn]').click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 1}, 200).removeClass('cin-act cout-act').addClass('show-calendar cin-act');	
			jQuery(".overlay-calendar").fadeIn(80);
		});
		
		jQuery('[name=cOut]').click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 1}, 200).removeClass('cin-act cout-act').addClass('show-calendar cout-act');	
			jQuery(".overlay-calendar").fadeIn(80);
		});
		
		jQuery(".overlay-calendar").click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 0}, 200).removeClass('show-calendar');
					jQuery(".wrap-src-home").css('z-index',100);
			jQuery(this).fadeOut(80);
		});
		
	}	
}

function reset_validate_form_2(){
	jQuery('[name=first_name],[name=last_name],[name=email],[name=retype_email]').removeClass('error');
	jQuery('.info-state').html('');
}

function validate_form_2(){
	jQuery('.info-state').removeClass('error-info success-info');
	reset_validate_form_2();
	var first_name 		= jQuery('[name=first_name]').val();
	var last_name  		= jQuery('[name=last_name]').val();
	var email 			= jQuery('[name=email]').val();
	var retype_email 	= jQuery('[name=retype_email]').val();
	var adult 			= jQuery('[name=adult]').val();
	var error=0; var txt='';
	if(first_name==''){
		jQuery('[name=first_name]').addClass('error');
		txt = 'First Name';
		error++;
	}
	if(last_name==''){
		jQuery('[name=last_name]').addClass('error');
		txt = (txt!='' ? txt+', Last Name' : 'Last Name');
		error++;
	}
	if(email==''){
		jQuery('[name=email]').addClass('error');
		txt = (txt !='' ? txt+', Email':'Email');
		error++;
	}
	
	if(retype_email==''){
		jQuery('[name=retype_email]').addClass('error');
		txt = (txt != '' ? txt+', Retype' : 'Retype');
		error++;
	}	
	//console.log(error);
	if(txt!='')	jQuery('.info-state').append('<p>'+txt+' is required.</p>');
	
	if(email!='' && validateTheEmail(email)==false){
		jQuery('[name=email]').addClass('error');
		jQuery('.info-state').append('<p>Format Email is false.</p>');
		error++;console.log('dd');
	}	
	
	if(retype_email!='' && validateTheEmail(retype_email)==false){
		jQuery('[name=retype_email]').addClass('error').val('');
		jQuery('.info-state').append('<p>Format Retype Email is false.</p>');
		error++;console.log('dd2');
	}
	
	if( (retype_email!='' && validateTheEmail(retype_email)==true) &&  email!='' && validateTheEmail(email)==true){
		if(email != retype_email)	{
			jQuery('.info-state').append('<p>Email and Retype Email is not match.</p>');
			error++;console.log('d3');
		}
	}
		
	if(error==0)return true;
	else {
		jQuery('.info-state').addClass('error-info');	
		return false;
	}
}

function book_now(){
	var url = 'http://'+jQuery('[name=site_url]').val()+'/save-reservation-ajax/';
	var data = new Object;
		data.pKEY 	= 'is_use_ajax';
		data.first_name = jQuery('[name=first_name]').val();
		data.last_name	= jQuery('[name=last_name]').val();
		data.email		= jQuery('[name=email]').val();
		data.phone		= jQuery('[name=phone]').val();
		data.adult		= jQuery('[name=adult]').val();
		data.child		= jQuery('[name=child]').val();
		data.special_notes = jQuery('[name=special_notes]').val();
	jQuery('.big-loader').fadeIn(80);		
	jQuery.post(url,data,function(response){
		var result = jQuery.parseJSON(response);
		console.log(response);
		if(result.process=='success'){
			var url_success = 'http://'+jQuery('[name=site_url]').val()+'/success-inquiry/';
			window.location.href = url_success;	
		}else jQuery('.info-state').append('<p>Failed to send booking inquiry.</p>').addClass('success-info');
		jQuery('.big-loader').fadeOut(80);	
	});	
		
}


function book_this_villa(rel){
	var url = 'http://'+jQuery('[name=site_url]').val()+'/book-this-vp-ajax/';
	var data = new Object;
		data.pKEY 	= 'is_use_ajax';
		data.villa_id 	= rel;
		
	jQuery('.big-loader').fadeIn(80);	
	jQuery.post(url,data,function(response){
		jQuery('#layout-top.reservation-page').animate({'opacity':0},80,function(){
			jQuery('#layout-top.reservation-page section').remove();
			jQuery('#layout-top.reservation-page').append(response);
			jQuery('#layout-top.reservation-page').animate({'opacity':1},80,function(){
				set_event_reservation();
				jQuery('.big-loader').fadeOut(80);	
			});
		});
	});
	
}

function validateTheEmail(address) {
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var address;
    if(reg.test(address) == false) {
       return false;
    }else{
       return true;
    }
}