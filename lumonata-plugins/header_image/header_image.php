<?php
/*
    Plugin Name: Background Image
    Plugin URL: 
    Description: It's plugin  is use for costume header using image at every page.
    Author: Yana
    Author URL: 
    Version: 1.0.0
    Created Date : 03 Juni 2013
*/

/* PLUGIN NOTE :
 * lapp_name : page_header_image
 * lkey	: phi_id, lvalue : {larticle_id}
 * lkey : phi_data, lvalue : json{img, url, text1,text2}
 */

/*This is the first important step that you have to do.
 * Who are allowed to access the application
 * 
 */

define('HI_PLUGIN_NAME', 'header_image');
add_privileges('administrator', 'header_image', 'insert');
add_privileges('administrator', 'header_image', 'update');
add_privileges('administrator', 'header_image', 'delete');


/*Custom Icon Menu*/
function menu_css_header_image(){
    return "<style type=\"text/css\">.lumonata_menu ul li.header_image{
                                background:url('../lumonata-plugins/header_image/images/ico-slide.png') no-repeat left top;
                                }</style>";
}
/*Aply css this plugin*/
add_actions('header_elements','menu_css_header_image');

/*Custom Icon Menu*/
 

/* 
 * Add sub menu under applications menu
 *  
 * */

add_main_menu(array('header_image'=>'Background Image'));
include ("header_image_admin.php");
include ("header_image_front.php");
add_actions("header_image","get_admin_header_image");
?>
