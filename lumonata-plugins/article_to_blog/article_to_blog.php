<?php
/*
    Plugin Name: Article to Blog
    Plugin URL: http://www.lumonata.com/
    Description: Change articles to blog
    Author: Dana Asmara
    Author URL: http://www.lumonata.com/about-us/
    Version: 1.0
*/

//echo the_looping_articles();

function the_blog_content_list($type='articles',$args=''){
	global $db;
	 //set template
	set_template(TEMPLATE_PATH."/article-lists.html",'article');
	//set block
	
	add_block('loopArticle','lArticle','article');
	if(!empty($args)){
		$id=post_to_id();
		if($args=='category')
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_type=%s AND a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",$type,'publish',$id);
		else
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",'publish',$id);
			
		   
	}else{
		$sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s AND lshare_to=0 order by lorder",$type,'publish');
	}
   
	$r=$db->do_query($sql);
	$i=1;
	while($data=$db->fetch_array($r)){
		if( $i<=post_viewed() ){
			$theme = get_meta_data('front_theme','themes');
			add_variable('template_url', SITE_URL.'/lumonata-content/themes/'.$theme);
			
			add_variable('the_thumbs',the_attachment($data['larticle_id']));
			add_variable('artilce_link',permalink($data['larticle_id']));
			add_variable('the_title',$data['larticle_title']);
			add_variable('the_brief',filter_content($data['larticle_content'],true));
			add_variable('post_date',date(get_date_format(),strtotime($data['lpost_date'])));
			add_variable('the_user',the_user($data['lpost_by']));
			add_variable('the_categories',the_categories($data['larticle_id'],$data['larticle_type']));
			add_variable('the_tags',the_tags($data['larticle_id'],$data['larticle_type']));
			
			if(is_break_comment()){
				add_variable('comments',comments($data['larticle_id'],$data['lcomment_status'],comment_per_page())); 
			}
			parse_template('loopArticle','lArticle',true);
			$i++;
		}else{
			break;
		}
		
	}
	return return_template('article');
}



function the_blog_content(){	
	set_template(TEMPLATE_PATH."/blog-wrapper.html",'blog');
	add_block('theBlogWrapper','lBlog','blog');
	
	add_variable('theBlogList', the_blog_content_list());
	add_variable('the_category_list', blog_sidebar_category());
	
	parse_template('theBlogWrapper','lBlog',true);
	
	return return_template('blog');
}

function blog_sidebar_category(){
	$all = '';
	global $db;
	
	$s = $db->prepare_query("SELECT * FROM lumonata_rules WHERE lrule='categories' AND lgroup='articles' ORDER BY lname ASC");
	$r = $db->do_query($s);
	while($d=$db->fetch_array($r)){
		$all .= '<li><a href="http://'.SITE_URL.'/blog/'.$d['lsef'].'/">'.$d['lname'].'</a></li>';
	}
	
	return '<ul class="cat_list">'.$all.'</ul>';
}

function blog_fav_sidebar(){
	$all = '';
	global $db;
	
	$s = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_status='publish' AND larticle_type='articles' ORDER BY lcomment_count DESC LIMIT 0,10");
	$r = $db->do_query($s);
	while($d=$db->fetch_array($r)){
		$all .= '<li><a href="'.str_replace('/articles/','/blog/',permalink($d['larticle_id'])).'">'.$d['larticle_title'].'</a></li>';
	}
	
	return '<ul class="fav_list">'.$all.'</ul>';
}


/*--------------------START function that copy from portfolio-----------------------*/
function is_custom_app_details(){
	$uri = get_uri();
	$prt = array_reverse(explode('.',$uri));
	$nnn = count($prt);
	if( $nnn>1 && $prt[0]=='html' ){
		return true;
	}else{
		return false;
	}
}

function get_portfolio_category(){
	$uri = get_uri();
	$prt = explode('.',$uri);
	$z   = explode('/',$prt[0]);
	
	if(isset($z[1])){ return $z[1]; }
}
function get_portfolio_category_name($lsef, $lgroup){
	global $db;
	$sql = $db->prepare_query("SELECT lname FROM lumonata_rules WHERE lsef=%s AND lgroup=%s AND lrule='categories'",$lsef,$lgroup);
	$r   = $db->do_query($sql);
	$dat = $db->fetch_array($r);
	
	return $dat['lname'];
}
function get_portfolio_category_ID($lsef, $lgroup){
	global $db;
	
	if($lsef=='uncategorized'){$addition="OR lgroup='default'";}else{$addition='';}
	$sql = $db->prepare_query("SELECT lrule_id FROM lumonata_rules WHERE lsef=%s AND lgroup=%s $addition AND lrule='categories'",$lsef,$lgroup);
	$r   = $db->do_query($sql);
	$dat = $db->fetch_array($r);
	
	return $dat['lrule_id'];
}


function get_portfolio_page_num(){
	$uri = get_uri();
	$cat = get_portfolio_category();
	
	$tmp = str_replace('portfolio/','',$uri);
	$tmp = str_replace($cat.'/','',$tmp);
	
	return $tmp;
}

function get_custom_app_page_num($appname='portfolio'){
	$uri = get_uri();
	$cat = get_portfolio_category();
	
	$tmp = str_replace($appname.'/','',$uri);
	$tmp = str_replace($cat.'/','',$tmp);
	
	return $tmp;
}

function get_portfolio_page_sef(){
	$uri = get_uri();
	$z   = array_reverse(explode('/',$uri));
	$y   = explode('.',$z[0]);
	
	return $y[0];
}
/*-----------------------END function that copy from portfolio plugins----------------*/
function the_blog_page(){
	$type='articles';
	global $db;
	
	set_template(TEMPLATE_PATH."/custom-lists.html",'article');
	add_block('loopArticle','lArticle','article');
	
	$tgPage = get_custom_app_page_num('blog');
	$prPage = 5;
	if( empty($tgPage) || $tgPage<1 ){$tgPage=1;}
	$start = ($tgPage-1)*$prPage;
	$limit = ' LIMIT '.$start.','.$prPage.' ';	
	
	$isCategory = false;
	$isSearchPage = false;
	
	$post_cat = get_portfolio_category();
	if($post_cat=='search-result'){ $post_cat=''; $isSearchPage = true; }
	if($post_cat==$tgPage){ $post_cat=''; }
	if(!empty($post_cat)){ $args='category'; $isCategory = true; }
	if(!$isSearchPage ){ $_SESSION['prev_sKey']=''; }
	
	if(!empty($_POST['sKey']) && $_POST['sKey']!='search what?'){
		$_SESSION['prev_sKey']=$_POST['sKey'];
		$search_STR = ' AND  ';
	}else{
	
	}
	
	
	if(!empty($args)){
		$id = get_portfolio_category_ID($post_cat, $type);
		if($args=='category'){
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_type=%s AND a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder".$limit,$type,'publish',$id);
			$sql2=$db->prepare_query("SELECT COUNT(*) as nRec
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_type=%s AND a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",$type,'publish',$id);
		}else{
			$sql=$db->prepare_query("SELECT a.*
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder".$limit,'publish',$id);
			$sql2=$db->prepare_query("SELECT COUNT(*) as nRec
									FROM lumonata_articles a,lumonata_rule_relationship b
									WHERE a.larticle_status=%s
									AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
									ORDER BY a.lorder",'publish',$id);
		}  
	}else{
		$sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s AND lshare_to=0 ORDER BY lorder".$limit,$type,'publish');
		$sql2=$db->prepare_query("SELECT count(*) as nRec FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s AND lshare_to=0 ORDER BY lorder",$type,'publish');
	}
   	
	if(!empty($_POST['sKey']) && $_POST['sKey']!='search what?'){
		$_SESSION['prev_sKey']=$_POST['sKey'];
		$tm1 = explode('ORDER BY', $sql);
		$tm2 = explode('ORDER BY', $sql2);
		
		$searc_STR = " AND larticle_title LIKE %s ";
		$sql  = $db->prepare_query($tm1[0].$searc_STR.' ORDER BY '.$tm1[1],'%'.$_POST['sKey'].'%');
		$sql2 = $db->prepare_query($tm2[0].$searc_STR.' ORDER BY '.$tm2[1],'%'.$_POST['sKey'].'%');
	}
	if(empty($_POST['sKey']) && $isSearchPage){
		$tm1 = explode('ORDER BY', $sql);
		$tm2 = explode('ORDER BY', $sql2);
		
		$searc_STR = " AND larticle_title LIKE %s ";
		$sql  = $db->prepare_query($tm1[0].$searc_STR.' ORDER BY '.$tm1[1],'%'.$_SESSION['prev_sKey'].'%');
		$sql2 = $db->prepare_query($tm2[0].$searc_STR.' ORDER BY '.$tm2[1],'%'.$_SESSION['prev_sKey'].'%');
	}
	
	
	$r = $db->do_query($sql);
	$nRec = $db->num_rows($r);
	while($data=$db->fetch_array($r)){
		$theme = get_meta_data('front_theme','themes');
		add_variable('template_url', SITE_URL.'/lumonata-content/themes/'.$theme);
		add_variable('the_thumbs',the_attachment($data['larticle_id']));
		
		add_variable('artilce_link',  	str_replace('/articles/','/blog/',permalink($data['larticle_id']))  );
		add_variable('the_categories',	str_replace('/articles/','/blog/',the_categories($data['larticle_id'],$data['larticle_type']))  );
		
		add_variable('the_title',	$data['larticle_title']);
		add_variable('the_brief',	filter_content($data['larticle_content'],true));
		add_variable('post_date',	date(get_date_format(),strtotime($data['lpost_date'])));
		add_variable('the_user',	the_user($data['lpost_by']));
		if($data['lcomment_count']>1){
			add_variable('n_comments',	$data['lcomment_count'].' Comments');
		}else{
			add_variable('n_comments',	$data['lcomment_count'].' Comment');
		}
		
		parse_template('loopArticle','lArticle',true);
	}
	if($nRec<=0){ add_variable('ifNoDataAvailable', '<p style="text-align:center; padding-top:20px;">No record available at the moment...</p>'); }else{ add_variable('ifNoDataAvailable', ''); }
	$blogList = return_template('article');
	
	//parse_template('portfolioLoop','lPortfolio',true);
	$r2 = $db->do_query($sql2);
	$dat2  = $db->fetch_array($r2);
	$dat2['nRec'];
	$nPage = floor($dat2['nRec']/$prPage);
	if(($nPage*$prPage)<$dat2['nRec']){$nPage++;}
	
	$pLinks = '';
	$curURL = str_replace('/'.$tgPage.'/','/','http://'.SITE_URL.'/'.get_uri().'/');
	if($nPage>1){
	for($j=1;$j<=$nPage;$j++){
		$class = '';
		if($j==$tgPage){$class=' class="current" ';}
		$pLinks .= '<a '.$class.' href="'.$curURL.$j.'/">'.$j.'</a>';
	}}else{
		$pLinks = '';
	}
	$thePageLinks = $pLinks;	
	
	if(!empty($post_cat)){
		add_actions("meta_title",get_portfolio_category_name($post_cat, 'articles').' - Blog');
	}else{
		add_actions("meta_title",'Pacha Express Blog');
	}
	add_actions("meta_keywords",	'');
	add_actions("meta_description",	'');
	
	//=======================================
	set_template(TEMPLATE_PATH."/blog-wrapper.html",'blog');
	add_block('theBlogWrapper','lBlog','blog');
	
	add_variable('site_url', SITE_URL);
	add_variable('theBlogList',  $blogList);
	add_variable('thePageLinks', $thePageLinks);
	if(isset($isCategory) && $isCategory){
		add_variable('thePostedUnder', '<div class="oneBlog" style="padding-bottom:10px;"><p style="margin:0;">Post(s) under category <strong style="font-style:italic;">'.get_portfolio_category_name($post_cat, 'articles').'</strong></p></div>');
	}
	if(!empty($_POST['sKey']) && $_POST['sKey']!='search what?'){
		add_variable('the_sVal', $_POST['sKey']);
		add_variable('thePostedUnder', '<div class="oneBlog" style="padding-bottom:10px;"><p style="margin:0;">Your search for <strong style="font-style:italic;">"'.$_POST['sKey'].'"</strong> returned '.$dat2['nRec'].' result(s).</p></div>');
	}elseif(empty($_POST['sKey']) && !empty($_SESSION['prev_sKey'])){
		add_variable('the_sVal', $_SESSION['prev_sKey']);
		add_variable('thePostedUnder', '<div class="oneBlog" style="padding-bottom:10px;"><p style="margin:0;">Your search for <strong style="font-style:italic;">"'.$_SESSION['prev_sKey'].'"</strong> returned '.$dat2['nRec'].' result(s).</p></div>');
	}else{
		add_variable('the_sVal', '');
	}
	
	parse_template('theBlogWrapper','lBlog',true);
	add_variable('the_category_list', blog_sidebar_category()); 
	add_variable('the_fav_list', blog_fav_sidebar()); 
	
	
	return return_template('blog');
}

function the_blog_page_details(){
	$type = 'articles';
	global $db;
	
	set_template(TEMPLATE_PATH."/custom-detail.html",'article');
	add_block('loopArticle','lArticle','article');
	
	
	//==============================================================POST SOME COMMENTS
	$eMessages  = '';
	//print_r($_POST);
	if(!empty($_POST)){ 
		if(!empty($_POST['theName']) && !empty($_POST['theEmail']) && !empty($_POST['theContent']) && !empty($_POST['justCode'])  ){
			$commStatus = check_comment_validation();
			//print_r($commStatus) ;
			if($commStatus=='valid'){
				//echo 'kbhbjbjibib---';
				$_SESSION['latest_comment_from'] = $_POST['theName'];
				$_SESSION['latest_comment_mail'] = $_POST['theEmail'];
				$status = insert_comment($_POST['theParent'], $_POST['thePostID'], $_POST['theContent'],$_POST['theName'],$_POST['theEmail'],$_POST['theURL'],$comment_type='comment');
			}else if(is_array($commStatus)){
				if($commStatus['invalidemail']){ $eMessages .= '<p style="padding:0; margin:0;">you input invalid email format</p>'; } 
				if($commStatus['invalidcode']){  $eMessages .= '<p style="padding:0; margin:0;">you input invalid verivication code</p>'; } 
			}
		}else{
			//one of the field is empty
			$errorEmpty = true;
			$eMessages .= '<p style="padding:0; margin:0;">some of the required field are empty, please fill the required field.</p>'; 
		}
	}
	//==============================================================POST SOME COMMENTS
	
	
	
	$post_cat = get_portfolio_category();
	$post_sef = get_portfolio_page_sef();
	
	$sql = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND lsef=%s AND larticle_status=%s AND lshare_to=0 order by lorder",$type,$post_sef,'publish');
	$r   = $db->do_query($sql);
	$data = $db->fetch_array($r);

	$theme = get_meta_data('front_theme','themes');
	add_variable('template_url', SITE_URL.'/lumonata-content/themes/'.$theme);
	add_variable('site_url', SITE_URL);
	add_variable('the_thumbs',the_attachment($data['larticle_id']));
	add_variable('json_url',	 'http://'.SITE_URL.'/lumonata-plugins/portfolio/jquery.json.min.js');
		
	add_variable('artilce_link',  	str_replace('/articles/','/blog/',permalink($data['larticle_id']))  );
	add_variable('the_categories',	str_replace('/articles/','/blog/',the_categories($data['larticle_id'],$data['larticle_type']))  );
	
	add_variable('thePostID',	$data['larticle_id']);
	add_variable('the_title',	$data['larticle_title']);
	add_variable('the_brief',	filter_content($data['larticle_content'],false));
	add_variable('post_date',	date(get_date_format(),strtotime($data['lpost_date'])));
	add_variable('the_user',	the_user($data['lpost_by']));
	
	if($data['lcomment_count']>1){
		add_variable('n_comments',	$data['lcomment_count'].' Comments');
	}else{
		add_variable('n_comments',	$data['lcomment_count'].' Comment');
	}
	add_variable( 'comments', get_the_blog_comment($data['larticle_id']) ); 
	add_variable('site_url', SITE_URL);
	add_variable('theErrorDisplay', 'style="display:none;"');
	
		
	if(!empty($eMessages)){
		add_variable('theErrorDisplay', 'style="display:block;"');
		add_variable('error_messages', $eMessages );
	}
	
	
	//print_r($_SESSION);
	
	
	
	
	if(isset($_POST['submitCommentNow'])){ 
		//if(!empty($_POST['theName']) && !empty($_POST['theEmail']) && !empty($_POST['theContent']) && !empty($_POST['justCode'])  ){
		//}
		add_variable('theName', 	$_POST['theName'] );
		add_variable('theEmail', 	$_POST['theEmail'] );
		add_variable('theURL', 		$_POST['theURL'] );
		add_variable('theContent', 	$_POST['theContent'] );
		
		$theTextarea = '<textarea class="required" name="theContent" id="theCommentTextarea">'.$_POST['theContent'].'</textarea>';	
	}else{
		$theTextarea = '<textarea class="required" name="theContent" id="theCommentTextarea"></textarea>';
	}
	
	if(is_user_logged()){ 
		$sql = $db->prepare_query("SELECT * FROM lumonata_users WHERE luser_id=%d", $_COOKIE['user_id']);
		$r   = $db->do_query($sql);
		$usr = $db->fetch_array($r);
		
		add_variable('theName', $usr['ldisplay_name'] );
		add_variable('theEmail', $usr['lemail'] ); 
		
		$specialCode = md5(time());
		$_SESSION['rand_code'] = $specialCode;
		add_variable('specialCodeForRegistered', $_SESSION['rand_code'] ); 
		
		//hideForLoggedUser
		add_variable('hideForLoggedUser', 'display:none;' ); 
		add_variable('aditionalStyleForForm', 'width:100%');
		add_variable('loggedButtonStyle', 'margin-left:70px;');
		
		add_variable('specialJSCodeForAdmin', "retData.code='valid'");
		add_variable('commTextareaPos2', $theTextarea);
		add_variable('loggedAvatar', get_avatar_URL($usr['lemail']));	
		add_variable('isUserLoggedIn', 'andYes');
	}else{
		add_variable('hideForNonLoggedUser', 'display:none;' ); 
		add_variable('commTextareaPos1', $theTextarea);
		add_variable('isUserLoggedIn', 'andNo');
	}
	
	if(get_meta_data('is_login_to_comment')==1 && !is_user_logged()){
		add_variable('comment_login_form_action', get_admin_url()."/?state=login&redirect=".base64_encode(cur_pageURL())  );
		add_variable('justHideTheCommentForm', 'display:none;' ); 
	}else{
		add_variable('justHideTheLoginForComment', 'display:none;' ); 
	}
	
	
	
	parse_template('loopArticle','lArticle',true);
	$blogDetails = return_template('article');
	
	$theMetaTitle = get_meta_title_custom($data['larticle_id']);
	if(empty($theMetaTitle)){ $theMetaTitle=$data['larticle_title']; }
	add_actions("meta_title",		$theMetaTitle.' - Blog');
	add_actions("meta_keywords",	get_meta_keywords_custom($data['larticle_id']));
	add_actions("meta_description",	get_meta_description_custom($data['larticle_id']));
	
	//===================================================
	set_template(TEMPLATE_PATH."/blog-wrapper.html",'blog');
	add_block('theBlogWrapper','lBlog','blog');
	
	add_variable('site_url', SITE_URL);
	add_variable('theBlogList',  $blogDetails);
	add_variable('thePageLinks', '');
	
	parse_template('theBlogWrapper','lBlog',true);
	add_variable('the_category_list', blog_sidebar_category());
	add_variable('the_fav_list', blog_fav_sidebar()); 
	
	
	return return_template('blog');
}

function get_the_blog_comment($postID){
	global $db;
	
	set_template(TEMPLATE_PATH."/custom-comments.html",'comments');
	add_block('loopComments','lComment','comments');	
	
	$theMail = '';
	if(isset($_SESSION['latest_comment_mail'])){
		$theMail = $_SESSION['latest_comment_mail'];
	}
	
	$q = $db->prepare_query("SELECT * from lumonata_comments
                             WHERE lcomment_parent=0 AND larticle_id=%d AND (lcomment_status='approved' OR lcomentator_email=%s) AND lcomment_type='comment' ORDER BY lcomment_date ASC",$postID,$theMail);
	$r = $db->do_query($q);
    $nn1 = $db->num_rows($r);
    while($d=$db->fetch_array($r)){
    	add_variable('justID', 			$d['lcomment_id']);
    	add_variable('comment_ID', 		'the-comment-'.$d['lcomment_id']);
    	add_variable('the_avatar', 		get_avatar_URL($d['lcomentator_email']));
    	add_variable('the_name', 		$d['lcomentator_name']);  
    	if($d['lcomment_status']=='approved'){
    		add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])));
    	}else{
    		add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])).' <strong style="color:#EE3F34; font-style:italic;">(this comment is waiting for moderation)</strong> ');
    	}
    	add_variable('the_comment', $d['lcomment']);
    	add_variable('the_replay_link', '#post-your-comment');
    	add_variable('addClass', '');
    	add_variable('replayAdd', ''); 
    	if($d['lcomment_status']!='approved'){ add_variable('replayAdd', 'style="display:none;"'); }  
    	
    	
    	parse_template('loopComments','lComment',true);
    	    	
    	
    	
    	$q2 = $db->prepare_query("SELECT * from lumonata_comments
    	                         WHERE lcomment_parent=%d AND larticle_id=%d AND (lcomment_status='approved' OR lcomentator_email=%s) AND lcomment_type='comment' ORDER BY lcomment_date ASC",$d['lcomment_id'],$postID,$theMail);
    	$r2 = $db->do_query($q2);
    	$nn2 = $db->num_rows($r2);
    	if($nn2>0){
    		while($d=$db->fetch_array($r2)){
    			add_variable('justID', 			$d['lcomment_id']);
    			add_variable('comment_ID', 		'the-comment-'.$d['lcomment_id']);
    			add_variable('the_avatar', 		get_avatar_URL($d['lcomentator_email']));
    			add_variable('the_name', 		$d['lcomentator_name']);  
    			if($d['lcomment_status']=='approved'){
    				add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])));
    			}else{
    				add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])).' <strong style="color:#EE3F34; font-style:italic;">(this comment is waiting for moderation)</strong> ');
    			}
    			add_variable('the_comment', $d['lcomment']);
    			add_variable('the_replay_link', '#post-your-comment');
    			add_variable('addClass', 'level1');
    			add_variable('replayAdd', '');  
    			if($d['lcomment_status']!='approved'){ add_variable('replayAdd', 'style="display:none;"'); }    			
    			
    			parse_template('loopComments','lComment',true);
    			
    			
    			
    			$q3 = $db->prepare_query("SELECT * from lumonata_comments
    			                         WHERE lcomment_parent=%d AND larticle_id=%d AND (lcomment_status='approved' OR lcomentator_email=%s) AND lcomment_type='comment' ORDER BY lcomment_date ASC",$d['lcomment_id'],$postID,$theMail);
    			$r3 = $db->do_query($q3);
    			$nn3 = $db->num_rows($r2);
    			if($nn3>0){
    				while($d=$db->fetch_array($r3)){
    					add_variable('justID', 			$d['lcomment_id']);
    					add_variable('comment_ID', 		'the-comment-'.$d['lcomment_id']);
    					add_variable('the_avatar', 		get_avatar_URL($d['lcomentator_email']));
    					add_variable('the_name', 		$d['lcomentator_name']);  
    					if($d['lcomment_status']=='approved'){
    						add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])));
    					}else{
    						add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])).' <strong style="color:#EE3F34; font-style:italic;">(this comment is waiting for moderation)</strong> ');
    					}
    					add_variable('the_comment', $d['lcomment']);
    					add_variable('the_replay_link', '#post-your-comment');
    					add_variable('addClass', 'level2');  
    					add_variable('replayAdd', 'style="display:none;"');  
    					  			
    					
    					parse_template('loopComments','lComment',true);
    					
    				}
    			}
    			
    		}
    	}
    }
    
    return return_template('comments');
}

function check_comment_validation(){
	$status =  Array();
	
	if(!empty($_POST)){
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['theEmail'])) {
			$status['invalidemail'] = true;
			//echo 'invalid1';
		}
		if($_POST['justCode']!=$_SESSION['rand_code']){
			$status['invalidcode'] = true;
			//echo 'invalid2';
		}
	}
	if(!empty($status)){
		return $status;
	}else{
		return 'valid';
	}
}

function get_avatar_URL($email){
	global $db;
	$size = 56;
	$default = 'http://'.TEMPLATE_URL.'/dummy/ava.png';
	
	
	$q = $db->prepare_query("SELECT lavatar FROM lumonata_users WHERE lemail=%s",$email);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	$d = $db->fetch_array($r);
	if($n>0){
		$prt = explode('|',$d['lavatar']);
		$sizeSTR = 'w='.$size.'&h='.$size;
		$theAvatar = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-content/files/users/'.$prt[0];
	}else{
		$theAvatar = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;		
	}
	return $theAvatar;
}



function ajax_post_comment(){
	add_actions('is_use_ajax', true);
	global $db;
	
	if(!empty($_POST) && $_POST['ajx_com']=='yes it is'){
		if(!empty($_POST['theName']) && !empty($_POST['theEmail']) && !empty($_POST['theContent']) && !empty($_POST['justCode'])  ){
			$commStatus = check_comment_validation();
			
			//if(empty($commStatus['invalidemail'])){
				//echo 'valid doms';
				$_SESSION['latest_comment_from'] = $_POST['theName'];
				$_SESSION['latest_comment_mail'] = $_POST['theEmail'];
				$status = insert_comment($_POST['theParent'], $_POST['thePostID'], $_POST['theContent'],$_POST['theName'],$_POST['theEmail'],$_POST['theURL'],$comment_type='comment');
				if($status){
					set_template(TEMPLATE_PATH."/custom-comments.html",'comments');
					add_block('loopComments','lComment','comments');
					
					
					$q = $db->prepare_query("SELECT * from lumonata_comments
					                         WHERE larticle_id=%d AND lcomentator_email=%s AND lcomment_type='comment' 
					                         ORDER BY lcomment_id DESC LIMIT 0,1",$_POST['thePostID'],$_POST['theEmail']);
					$r = $db->do_query($q);
					$d = $db->fetch_array($r);
					
					
					add_variable('justID', 			$d['lcomment_id']);
					add_variable('comment_ID', 		'the-comment-'.$d['lcomment_id']);
					add_variable('the_avatar', 		get_avatar_URL($d['lcomentator_email']));
					add_variable('the_name', 		$d['lcomentator_name']);  
					if($d['lcomment_status']=='approved'){
						add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])));
					}else{
						add_variable('the_date', 	date(get_date_format(),strtotime($d['lcomment_date'])).' <strong style="color:#EE3F34; font-style:italic;">(this comment is waiting for moderation)</strong> ');
					}
					add_variable('the_comment', $d['lcomment']);
					add_variable('the_replay_link', '#post-your-comment');
					add_variable('addClass',  'newlyAdded');  
					add_variable('replayAdd', 'style="display:none;"');  
					
					
					
					parse_template('loopComments','lComment',true);
					echo return_template('comments');
				}else{
					echo 'just error 1';
				}
			//}else{
				//echo 'just error 2';
			//}
		}else{
			echo 'just error 3';
		}
	}
}
add_actions('ajax-post-comment_page', 'ajax_post_comment');











	
add_actions('blog_page', 'the_blog_page');
if(get_appname()=='blog'){	
	if(is_custom_app_details()){
		add_actions('thecontent', 'the_blog_page_details');
	}else{
		add_actions('thecontent', 'the_blog_page');
	}
}

function get_meta_title_custom($id){	
	if(!empty($id)){
		return strip_tags(get_additional_field($id,'meta_title','articles'));
	}
}
function get_meta_keywords_custom($id){
	if(!empty($id)){
		$meta_keywords=strip_tags(get_additional_field($id,'meta_keywords','articles'));
		if(!empty($meta_keywords)){
			return "<meta name=\"keywords\" value=\"".$meta_keywords."\" />";
		}
	}
}
function get_meta_description_custom($id){
	if(!empty($id)){
		$meta_description=strip_tags(get_additional_field($id,'meta_description','articles'));
		if(!empty($meta_description)){
			return "<meta name=\"description\" value=\"".$meta_description."\" />";
		}
	}
}


?>