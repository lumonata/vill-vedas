<?php
/*
    Plugin Name: Manage Villa Program
    Plugin URL: http://lumonata.com/
    Description: This plugin is used for manage villa and program.
    Author: Adi Juliartha
    Author URL: http://lumonata.com/
    Version: 1.0.1
    
*/
include 'app_villas.php';
include 'app_programs.php';
/*include 'app_aboutus.php';
include 'app_mediahub.php';
include 'app_wellness.php';
include 'app_programs.php';
include 'app_facilities.php';
include 'app_cuisine.php';
include 'app_additional_service.php';
include 'app_rates_reservation_ajax.php';*/

if (isset($_GET['state']) || isset($_GET['sub'])){
	//echo "M";
	if ((isset($_GET['state']) And $_GET['state']=='articles')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			foreach($_POST['select'] as $index=>$post_id){
				add_actions('articles_additional_data_'.$index,'additional_data','News & Events','additional_data_article_news');
			
			}
		}else{
			add_actions('articles_additional_data','additional_data','News & Events','additional_data_article_news');
			
		}	
		if(is_publish()){
			//print_r($_POST);
			if(isset($_POST['additional_fields']) && $_POST['additional_fields']['article_news'][0]==1){
				$sql=$db->prepare_query("UPDATE
											lumonata_additional_fields
											SET lvalue=%s
											WHERE lkey=%s AND lapp_name=%s",
											0,"article_news","articles");
								
				$r=$db->do_query($sql);
			}
		}
	}
}


function additional_data_article_news(){
	global $thepost;
	global $db;
	
	$i = $thepost->post_index;
    $post_id = $thepost->post_id;
	if($_GET['state']=='applications') $metaKey = $_GET['sub'];
	else $metaKey = $_GET['state'];
	$key="article_news";
	$article_news = get_additional_field( $post_id, $key, $metaKey);
	$check1='';
	$check2='checked="checked"';
	if($article_news==1){
		$check1='checked="checked"';
		$check2='';
	}
	$layout = '	<script>
				jQuery(document).ready(function(){
				//alert("M");
					var iii = parseInt("'.$i.'");
					var tg1 = jQuery("#meta_data_'.$i.'").parent();
					var tg2 = jQuery("#additional_data_article_news_'.$i.'").parent();
					jQuery(tg2).insertBefore(tg1);
				});
				</script>
				<input '.$check1.' type="radio" value="1" name="additional_fields['.$key.']['.$i.']">
				Yes
				<input '.$check2.' type="radio" value="0" name="additional_fields['.$key.']['.$i.']">
				No
				';
	
	return $layout;
	//return "MMM";
}

?>