<?php
function menu_css_facilities(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.facilities{
	background:url('../lumonata-plugins/sukhavati/images/ico-themes.png') no-repeat left top;
	}
	</style>";
}

add_actions('header_elements','menu_css_facilities');

add_privileges('administrator', 'facilities', 'insert');
add_privileges('administrator', 'facilities', 'update');
add_privileges('administrator', 'facilities', 'delete');

//add_main_menu(array('facilities'=>'Facilities'));

add_actions("facilities","get_admin_article","facilities","Facilities|Facilities");
//get_admin_article();
if (isset($_GET['state'])){
	if ((isset($_GET['state']) And $_GET['state']=='facilities')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			
		}else{
			
		}	
	}
}

if ((isset($_GET['state']) And $_GET['state']=='facilities')){
	add_actions("header_elements","facilities_CSS");
}
/* E facilities */


function facilities_CSS(){
	$text = '<style>
			/*facilities_CSS*/
			ul.tabs li:last-child{
				display: none;
			}
			</style>
			';
	return $text;
}


function is_facilities_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='facilities'){
		if(count_rules("rule_id=60") > 0){
			$data = fetch_rule("rule_id=60");
			$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    		$actions->action['meta_title']['args'][0] = '';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}



function front_facilities(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='facilities';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_facilities.html",'template_facilities');
	//set block
       
	//print_r($_POST);
    add_block('Bfacilities_block','b_facilities','template_facilities');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	if ($cek_url[0]=='facilities'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=60") > 0){
				$data = fetch_rule("rule_id=60");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				
				$navigation = navigation('facilities',$data);
				add_variable('navigation', $navigation);
				
				$rule_id=60;
				$categories =get_article_by_rule($rule_id, $type);
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/facilities/';
				$titleNow=$data['lname']." - ".web_title();
			}
		}else{
			if(is_num_articles("type=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_artciles("type=".$type."&sef=".$cek_url[1]);
				$actions->action['meta_title']['func_name'][0] = $data['larticle_title']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['larticle_title']);
				add_variable('description', $data['larticle_content']);
				$navigation = navigation_article($type, $data);
				add_variable('navigation', $navigation);
				
				$image =json_decode(get_front_right_image($data['larticle_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/facilities/'.$data['lsef'].'/';
				$titleNow=$data['larticle_title']." - ".web_title();
				
				$meta_title=get_additional_field($data['larticle_id'], 'meta_title', $type);
				if(!empty($meta_title)){
					$titleNow=$meta_title." - ".web_title();
					$actions->action['meta_title']['func_name'][0] = $titleNow;
				}
				$meta_description=get_additional_field($data['larticle_id'], 'meta_description', $type);
				$actions->action['meta_description']['func_name'][0] = '<meta name="description" value="'.$meta_description.'" />';
				$meta_keywords=get_additional_field($data['larticle_id'], 'meta_keywords', $type);
				$actions->action['meta_keywords']['func_name'][0] = '<meta name="keywords" value="'.$meta_keywords.'" />';
				//print_r($actions);
			}
		}
	}
    
    parse_template('Bfacilities_block','b_facilities',false);
         
	if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_facilities'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_facilities');	
    }
}
?>