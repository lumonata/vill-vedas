<?php
function menu_css_programs(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.programs{
	background:url('../lumonata-plugins/manage_villa_program/images/ico-themes.png') no-repeat left top;
	}
	</style>";
}

add_actions('header_elements','menu_css_programs');

add_privileges('administrator', 'programs', 'insert');
add_privileges('administrator', 'programs', 'update');
add_privileges('administrator', 'programs', 'delete');

add_main_menu(array('programs'=>'Program'));

add_actions("programs","get_admin_article","programs","Programs|Programs");
//get_admin_article();
if (isset($_GET['state'])){
	if ((isset($_GET['state']) And $_GET['state']=='programs')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			
		}else{
			
		}	
	}
}

if ((isset($_GET['state']) And $_GET['state']=='programs')){
	add_actions("header_elements","programs_CSS");
}
/* E Rates & Reservations */


function programs_CSS(){
	$text = '<style>
			/*ratesreservations_CSS*/
			ul.tabs li:last-child{
				display: none;
			}
			</style>
			';
	return $text;
}


function is_ratesreservations_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='rates-reservations'){
		if(count_rules("rule_id=51") > 0){
			$data = fetch_rule("rule_id=51");
			$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    		$actions->action['meta_title']['args'][0] = '';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}



function front_ratesreservations(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='ratesreservations';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_ratesreservations.html",'template_ratesreservations');
	//set block
       
	//print_r($_POST);
    add_block('Bratesreservations_block','b_ratesreservations','template_ratesreservations');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	if ($cek_url[0]=='rates-reservations'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=51") > 0){
				$data = fetch_rule("rule_id=51");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=51;
				$categories =get_rule_child($rule_id,$type);
				add_variable('categories', $categories);
				
				//add_variable('image', get_front_right_image($data['lrule_id']));
				//$contentright=get_front_right_image($data['lrule_id']);
				//$contentright =json_decode(get_front_right_image($data['lrule_id']),true);
				//add_variable('contentright', $contentright['gallery_list']);
				$contentright=form_reservation($data['larticle_id'],'article');
				add_variable('contentright', $contentright);
				
				
				
				$urlNow='http://'.SITE_URL.'/rates-reservations/';
				$titleNow=$data['lname']." - ".web_title();
			}
		}else{
			if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=$data['lrule_id'];
				$categories = get_article_by_rule_n_show_front_end($rule_id,$type);
				//$categories =get_rule_child($rule_id,$type);
				//$contentright=get_front_right_image($data['lrule_id']);
				$contentright =json_decode(get_front_right_image($data['lrule_id']),true);
				$contentright =$contentright['gallery_list'];
				if(empty($categories)){
					//$categories =get_article_by_rule($rule_id, $type);
				}
				if(empty($categories)){
					$contentright=form_reservation($data['lrule_id'],'rule');
				}
				add_variable('categories', $categories);
				
				//add_variable('image', get_front_right_image($data['lrule_id']));
				
				$contentright=form_reservation($data['larticle_id'],'article');
				add_variable('contentright', $contentright);
				
				$urlNow='http://'.SITE_URL.'/rates-reservations/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();
				
			}else if(is_num_articles("type=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_artciles("type=".$type."&sef=".$cek_url[1]);
				//print_r($data);
				$actions->action['meta_title']['func_name'][0] = $data['larticle_title']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['larticle_title']);
				add_variable('description', $data['larticle_content']);
				$navigation = navigation_article($type, $data);
				add_variable('navigation', str_replace('rates-reservations-2/','',$navigation));
				
				//add_variable('image', get_front_right_image($data['larticle_id']));
				$contentright=form_reservation($data['larticle_id'],'article');
				add_variable('contentright', $contentright);
				
				$urlNow='http://'.SITE_URL.'/rates-reservations/'.$data['lsef'].'/';
				$titleNow=$data['larticle_title']." - ".web_title();
			}
		}
	}
    
    parse_template('Bratesreservations_block','b_ratesreservations',false);
         
	if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_ratesreservations'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_ratesreservations');	
    }
}


function form_reservation($id,$type,$mobile=false){
	global $actions;
	global $db;
	$cek_url = cek_url();
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_ratesreservations_form.html",'template_form_reservation_'.$mobile);
	//set block
	//print_r($_POST);
    add_block('Bform_reservation_block','b_form_reservation_'.$mobile,'template_form_reservation_'.$mobile);
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	add_variable('site_url',SITE_URL);
    add_variable('template_url_imgs',"http://".TEMPLATE_URL."/images");
	
	$thaUrl= 'http://'.SITE_URL.'/check-chapta2';
	add_variable('thaUrl_cekChapta',$thaUrl);
	$thaUrl= 'http://'.SITE_URL.'/rates-reservations-ajax';
	add_variable('thaUrl_sendMail',$thaUrl);
	
	add_variable('id',$id);
	add_variable('type',$type);
	
	$option_ratesreservations=option_ratesreservations($id);
	add_variable('option_ratesreservations', $option_ratesreservations);
	
	$option_ratesreservations=option_ratesreservations($id,'li');
	add_variable('li_ratesreservations', $option_ratesreservations);
	
	$list_villa = get_list_villa();
	add_variable('li_villas', $list_villa);
	
	if(!$mobile)add_variable('custom-scrollbar', 'custom-scrollbar');
	else add_variable('custom-scrollbar', '');
	add_variable('step_reservation',show_step_reservation($mobile));
	
	add_variable('additional_service_list',get_list_additional_service(0));
	
	parse_template('Bform_reservation_block','b_form_reservation_'.$mobile,false);
	return return_template('template_form_reservation_'.$mobile);
}

function show_step_reservation($mobile='false'){
	
	session_start();
	//unset($_SESSION['reservation_ss']['step_2']);
	//print_r($_SESSION);
	//unset($_SESSION['reservation_ss']);
	if(empty($_SESSION['reservation_ss']) || empty($_SESSION['reservation_ss']['step_1'])){
		set_template(PLUGINS_PATH."/reservation_event/step_1.html",'template_step_1_'.$mobile);
		add_block('step_1_block','b_step_1_'.$mobile,'template_step_1_'.$mobile);
		return return_template('step_1_block','b_step_1_'.$mobile,false);
	}
	if(!empty($_SESSION['reservation_ss']['step_1']) && empty($_SESSION['reservation_ss']['step_2'])){
		set_template(PLUGINS_PATH."/reservation_event/step_2.html",'template_step_2_'.$mobile);
		add_block('step_2_block','b_step_2_'.$mobile,'template_step_2_'.$mobile);
		add_variable('arrival_date',$_SESSION['reservation_ss']['step_1']['arrival_date']);
		add_variable('departure_date',$_SESSION['reservation_ss']['step_1']['departure_date']);
		
		$type_reservation 	= $_SESSION['reservation_ss']['step_1']['type_reservation'];
		$arrival_date		= strtotime($_SESSION['reservation_ss']['step_1']['arrival_date']);
		$departure_date		= strtotime($_SESSION['reservation_ss']['step_1']['departure_date']);
		$nights = ($departure_date-$arrival_date)/86400;
		
		if($type_reservation== 'villa') {
			$data_availability  =  json_decode(list_availabel_villa($arrival_date,$departure_date));
			add_variable('check_villa_only','checked');
			add_variable('text_step_2','Choose Acoomodation');
			
		}else{
			$data_availability  =  json_decode(list_availabel_prog_villa($arrival_date,$departure_date));
			add_variable('check_prog_villa','checked');
			add_variable('text_step_2','Choose Programs');
			add_variable('prog_step_mode','prog_step_mode');
		}
		
		add_variable('list_available',$data_availability->data);
		add_variable('night',$data_availability->night);
		
		//add_variable('night'		
		return return_template('step_2_block','b_step_2_'.$mobile,false);
	}
	
	if(!empty($_SESSION['reservation_ss']['step_1']) && !empty($_SESSION['reservation_ss']['step_2']) && empty($_SESSION['reservation_ss']['step_3'])){
		set_template(PLUGINS_PATH."/reservation_event/step_3.html",'template_step_3_'.$mobile);
		add_block('step_3_block','b_step_3_'.$mobile,'template_step_3_'.$mobile);
		$type_reservation 	= $_SESSION['reservation_ss']['step_2']['type_reservation'];
		if($type_reservation== 'villa') {
			$data = step_two_accom();
			add_variable('text_step_2','Choose Acoomodation');
			add_variable('type_reservation','just-accomodation');
		}else{
			$data = step_two_prog();
			add_variable('text_step_2','Choose Programs');
			add_variable('prog_step_mode','prog_step_mode');
			add_variable('date_booking',$data['date_booking']);
			add_variable('type_reservation','prog-reser');
		}
		
		//print_r($data);
		add_variable('data_booking',$data['temp']);
		add_variable('surcharge_cont',$data['surcharge_cont']);
		add_variable('grand_total',$data['grand_total']);
		add_variable('total',$data['total']);
		
		//else echo list_availabel_prog_villa();
		//add_variable('night'		
		return return_template('step_3_block','b_step_3_'.$mobile,false);
	}
	if(!empty($_SESSION['reservation_ss']['step_1']) && !empty($_SESSION['reservation_ss']['step_2']) && !empty($_SESSION['reservation_ss']['step_3'])){
		set_template(PLUGINS_PATH."/reservation_event/step_4.html",'template_step_4_'.$mobile);
		add_block('step_4_block','b_step_4_'.$mobile,'template_step_4_'.$mobile);	
		return return_template('step_4_block','b_step_4_'.$mobile,false);
	}
}



function get_list_villa($article_id=0,$status='li'){
	global $db;
	$str = $db->prepare_query("select * from lumonata_articles where larticle_type=%s",'villas');
	$r = $db->do_query($str);
	
	$option='';
	$li='';
	while ($data=$db->fetch_array($r)){
		$villa = $data['larticle_title'];
		/*$program = str_replace("Rates & Dates", "", $program);
		$program = str_replace("Rates &amp; Dates", "", $program);*/
		if($article_id==$data['larticle_id']){
			$option.='<option selected="selected" value="'.$data['larticle_id'].'">'.$villa.'</option>';
			$li.='<li rel="'.$data['larticle_id'].'" class="selected">'.$villa.'</li>';
		}else{
			$option.='<option value="'.$data['larticle_id'].'">'.$villa.'</option>';
			$li.='<li rel="'.$data['larticle_id'].'">'.$villa.'</li>';	
		}
		
	}
	if($status=="li"){
		return $li;
	}else{
		return $option;
	}
	
	
	while($data=$db->fetch_array($r)){
		print_r($data);
		
	}
}


function rates_reservations_ajax(){
	session_start();
	add_actions('is_use_ajax', true);		
	global $db;
	if(!empty($_POST['act']) && $_POST['act']=='submit'){
		if (!empty($_POST['name']) && !empty($_POST['email']) && 
		!empty($_POST['country']) && !empty($_POST['phone']) && !empty($_POST['arrival_date']) && !empty($_POST['departure_date']) && !empty($_POST['guest']) && 
		!empty($_POST['message']) &&
		!empty($_POST['id']) && !empty($_POST['type'])){
			$data = $_POST;		
			
			//if(isset($_POST['code']) && $_POST['code']==$_SESSION['rand_code']){
				if (rates_reservations_send($data)){
						echo 'success';
				}else{
						echo 'error';
				}
			//}else{
			//	echo 'error';
			//}
		}else{
			echo 'error';
		}
	}else{
		echo 'error';
	}
}

function rates_reservations_send($data){
	global $db;	
	if (empty($data['name']) || empty($data['email']) || 
		empty($data['country']) || empty($data['phone']) || empty($data['arrival_date']) || empty($data['departure_date']) || empty($data['guest']) || 
		empty($data['message']) ||
		empty($data['id']) || empty($data['type'])){
		return false;	
	}
	
	set_template(PLUGINS_PATH."/sukhavati/mail.html",'the_mail');
	add_block('Bmail','bm','the_mail');
	
	
	$name = $data['name'];
	$email = $data['email'];
	
	$country = $data['country'];
	$phone = $data['phone'];
	$arrival_date = $data['arrival_date'];
	$departure_date = $data['departure_date'];
	$guest = $data['guest'];
	
	$message = $data['message'];
	
	$id = $data['id'];
	$type = $data['type'];
	
	if($type=='rule'){
		$data = fetch_rule('group=ratesreservations&rule_id='.$id);
		$program = $data['lname'];
	}elseif ($type=='article'){
		$data = fetch_artciles('type=ratesreservations&id='.$id);
		$program = $data['larticle_title'];
	}else{
		return false;
	}
	
	$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'contact-us','contact-us');
	
	$r=$db->do_query($sql);
	$f=$db->fetch_array($r);
	$n=$db->num_rows($r);
	//echo
	$post_id = $f['larticle_id'];
	$article_type = $f['larticle_type'];
		
	$contact_us_email = get_additional_field( $post_id, 'contact_us_email', $article_type);
	$contact_us_cc = get_additional_field( $post_id, 'contact_us_cc', $article_type);
	$contact_us_bcc = get_additional_field( $post_id, 'contact_us_bcc', $article_type);
	
	
	$program = str_replace("Rates & Dates", "", $program);
	$program = str_replace("Rates &amp; Dates", "", $program);
	add_variable('program', $program);
	
	add_variable('name', $name);
	add_variable('email', $email);

	add_variable('country', $country);	
	add_variable('phone', $phone);	
	add_variable('arrival_date', $arrival_date);	
	add_variable('departure_date', $departure_date);
	add_variable('guest', $guest);		
	
			
	add_variable('message', nl2br($message));
	
	parse_template('Bmail','bm',true);
	$theMailContent = return_template('the_mail');
	

	$sendToMail = $contact_us_email;
	$cc = $contact_us_cc;
	$bcc = $contact_us_bcc;
	
	$web_title = get_meta_data("web_title");
	
	///ini_set("SMTP", SMTP_SERVER);
	///ini_set("sendmail_from", $sendToMail);
	
	//$subject = "[$web_title] Reserve Program From ".$name;
	$subject = "Inquiry for ".$program;
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";							
	$headers .= "From:  $name <$email>\r\nReply-to :$email\r\n";
	if(!empty($cc))
		$headers .= "Cc: ".$cc."\r\n";
	if(!empty($bcc))	
		$headers .= "Bcc: ".$bcc."\r\n";
	$send = mail($sendToMail,$subject,$theMailContent,$headers);
	//$send = true;
	if($send){
		return true;
	}else{
		return false;
	}
}

function option_ratesreservations($article_id="",$status=""){
	global $db;
	$query=$db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s",'ratesreservations');
	$result=$db->do_query($query);
	$option='';
	$li='';
	while ($data=$db->fetch_array($result)){
		$program = $data['larticle_title'];
		$program = str_replace("Rates & Dates", "", $program);
		$program = str_replace("Rates &amp; Dates", "", $program);
		if($article_id==$data['larticle_id']){
			$option.='<option selected="selected" value="'.$data['larticle_id'].'">'.$program.'</option>';
			$li.='<li rel="'.$data['larticle_id'].'" class="selected">'.$program.'</li>';
		}else{
			$option.='<option value="'.$data['larticle_id'].'">'.$program.'</option>';
			$li.='<li rel="'.$data['larticle_id'].'">'.$program.'</li>';	
		}
		
	}
	if($status=="li"){
		return $li;
	}else{
		return $option;
	}
	
}

add_actions('rates-reservations-ajax_page', 'rates_reservations_ajax');
?>