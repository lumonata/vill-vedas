<?php
function menu_css_wellness(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.wellness{
	background:url('../lumonata-plugins/sukhavati/images/ico-themes.png') no-repeat left top;
	}
	</style>";
}

add_actions('header_elements','menu_css_wellness');

add_privileges('administrator', 'wellness', 'insert');
add_privileges('administrator', 'wellness', 'update');
add_privileges('administrator', 'wellness', 'delete');

add_main_menu(array('wellness'=>'Wellness Experience'));

add_actions("wellness","get_admin_article","wellness","Wellness Experience | Wellness Experience");
//get_admin_article();
if (isset($_GET['state'])){
	if ((isset($_GET['state']) And $_GET['state']=='wellness')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			
		}else{
			
		}	
	}
}

if ((isset($_GET['state']) And $_GET['state']=='wellness')){
	add_actions("header_elements","wellness_CSS");
}
/* E Wellness */


function wellness_CSS(){
	$text = '<style>
			/*wellness_CSS*/
			ul.tabs li:last-child{
				display: none;
			}
			</style>
			';
	return $text;
}


function is_wellness_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='wellness'){
		if(count_rules("rule_id=28") > 0){
			$data = fetch_rule("rule_id=28");
			$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    		$actions->action['meta_title']['args'][0] = '';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}



function front_wellness(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='wellness';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_wellness.html",'template_wellness');
	//set block
       
	//print_r($_POST);
    add_block('Bwellness_block','b_wellness','template_wellness');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	if ($cek_url[0]=='wellness'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=28") > 0){
				$data = fetch_rule("rule_id=28");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=28;
				$categories =get_rule_child($rule_id,$type);
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/wellness/';
				$titleNow=$data['lname']." - ".web_title();
				
			}
		}else{
			if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=$data['lrule_id'];
				$categories =get_rule_child($rule_id,$type);
				if(empty($categories)){
					$categories =get_article_by_rule($rule_id, $type);
				}
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();
				
			}else if(is_num_articles("type=".$type."&sef=".$cek_url[1]) > 0){
			
				$data = fetch_artciles("type=".$type."&sef=".$cek_url[1]);
				$actions->action['meta_title']['func_name'][0] = $data['larticle_title']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['larticle_title']);
				add_variable('description', $data['larticle_content']);
				
				$navigation = navigation_article($type, $data);
				add_variable('navigation', $navigation);
				
				$image =json_decode(get_front_right_image($data['larticle_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['larticle_title']." - ".web_title();
				
				$meta_title=get_additional_field($data['larticle_id'], 'meta_title', $type);
				if(!empty($meta_title)){
					$titleNow=$meta_title." - ".web_title();
					$actions->action['meta_title']['func_name'][0] = $titleNow;
				}
				$meta_description=get_additional_field($data['larticle_id'], 'meta_description', $type);
				$actions->action['meta_description']['func_name'][0] = '<meta name="description" value="'.$meta_description.'" />';
				$meta_keywords=get_additional_field($data['larticle_id'], 'meta_keywords', $type);
				$actions->action['meta_keywords']['func_name'][0] = '<meta name="keywords" value="'.$meta_keywords.'" />';
				//print_r($actions);
			
			}
		}
	}
    
    parse_template('Bwellness_block','b_wellness',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_wellness'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_wellness');	
    }
}
?>