<?php
function menu_css_villas(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.villas{
	background:url('../lumonata-plugins/manage_villa_program/images/ico-themes.png') no-repeat left top;
	}
	</style>";
}

add_actions('header_elements','menu_css_villas');

add_privileges('administrator', 'villas', 'insert');
add_privileges('administrator', 'villas', 'update');
add_privileges('administrator', 'villas', 'delete');

add_main_menu(array('villas'=>'Villa'));

add_actions("villas","get_admin_article","villas","Villa|Villa",array('room_type'=> 'Room Type'));
//get_admin_article();
if (isset($_GET['state'])){
	if ((isset($_GET['state']) And $_GET['state']=='villas')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			
		}else{
			
		}	
	}
}

if ((isset($_GET['state']) And $_GET['state']=='villas')){
	add_actions("header_elements","villas_CSS");
}
/* E Villas */


function villas_CSS(){
	$text = '<style>
			/*villas_CSS*/
			ul.tabs li:nth-child(3){
				display: none;
			}
			</style>
			';
	return $text;
}


function is_villas_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='villas'){
		if(count_rules("rule_id=59") > 0){
			$data = fetch_rule("rule_id=59");
			$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    		$actions->action['meta_title']['args'][0] = '';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}



function front_villas(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='villas';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_villas.html",'template_villas');
	//set block
       
	//print_r($_POST);
    add_block('Bvillas_block','b_villas','template_villas');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	if ($cek_url[0]=='villas'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=59") > 0){
				$data = fetch_rule("rule_id=59");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				
				$navigation = navigation('villas',$data);
				add_variable('navigation', $navigation);
				
				$rule_id=59;
				//$categories =get_article_by_rule($rule_id, $type);
				$categories =get_rule_child($rule_id,$type);
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/villas/';
				$titleNow=$data['lname']." - ".web_title();
			}
		}else{
			if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=$data['lrule_id'];
				$categories =get_rule_child($rule_id,$type);
				if(empty($categories)){
					$categories =get_article_by_rule($rule_id, $type);
				}
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();
				
			}else if(is_num_articles("type=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_artciles("type=".$type."&sef=".$cek_url[1]);
				$actions->action['meta_title']['func_name'][0] = $data['larticle_title']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['larticle_title']);
				add_variable('description', $data['larticle_content']);
				
				$navigation = navigation_article($type, $data);
				add_variable('navigation', $navigation);
				
				$image =json_decode(get_front_right_image($data['larticle_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/villas/'.$data['lsef'].'/';
				$titleNow=$data['larticle_title']." - ".web_title();
				
				$meta_title=get_additional_field($data['larticle_id'], 'meta_title', $type);
				if(!empty($meta_title)){
					$titleNow=$meta_title." - ".web_title();
					$actions->action['meta_title']['func_name'][0] = $titleNow;
				}
				$meta_description=get_additional_field($data['larticle_id'], 'meta_description', $type);
				$actions->action['meta_description']['func_name'][0] = '<meta name="description" value="'.$meta_description.'" />';
				$meta_keywords=get_additional_field($data['larticle_id'], 'meta_keywords', $type);
				$actions->action['meta_keywords']['func_name'][0] = '<meta name="keywords" value="'.$meta_keywords.'" />';
				//print_r($actions);
			}
		}
	}
    
    parse_template('Bvillas_block','b_villas',false);
         
	if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_villas'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_villas');	
    }
}
?>