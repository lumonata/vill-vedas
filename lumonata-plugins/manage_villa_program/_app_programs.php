<?php
function menu_css_programs(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.programs{
	background:url('../lumonata-plugins/sukhavati/images/ico-themes.png') no-repeat left top;
	}
	</style>";
}

add_actions('header_elements','menu_css_programs');

add_privileges('administrator', 'programs', 'insert');
add_privileges('administrator', 'programs', 'update');
add_privileges('administrator', 'programs', 'delete');

//add_main_menu(array('programs'=>'Programs'));

add_actions("programs","get_admin_article","programs","Programs|Programs");
//get_admin_article();
if (isset($_GET['state'])){
	if ((isset($_GET['state']) And $_GET['state']=='programs')){
		if(is_edit_all() && !(is_save_draft() || is_publish())){
			
		}else{
			
		}	
	}
}

if ((isset($_GET['state']) And $_GET['state']=='programs')){
	add_actions("header_elements","programs_CSS");
}
/* E programs */


function programs_CSS(){
	$text = '<style>
			/*programs_CSS*/
			ul.tabs li:last-child{
				display: none;
			}
			</style>
			';
	return $text;
}


function is_programs_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='programs'){
		if(count_rules("rule_id=42") > 0){
			$data = fetch_rule("rule_id=42");
			$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    		$actions->action['meta_title']['args'][0] = '';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}



function front_programs(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	
	$type='programs';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_programs.html",'template_programs');
	//set block
       
	//print_r($_POST);
    add_block('Bprograms_block','b_programs','template_programs');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	if ($cek_url[0]=='programs'){
		if(!isset($cek_url[1])){
			if(count_rules("rule_id=42") >= 0){
				$data = fetch_rule("rule_id=42");
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=42;
				$categories =get_rule_child($rule_id,$type);
				//$categories .=get_article_by_rule($rule_id, $type);
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/programs/';
				$titleNow=$data['lname']." - ".web_title();
				
			}
		}else{
			if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0){
				$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
				$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['lname']);
				add_variable('description', $data['ldescription']);
				
				$navigation = navigation($type,$data);
				add_variable('navigation', $navigation);
				
				$rule_id=$data['lrule_id'];
				$categories =get_rule_child($rule_id,$type);
				if(empty($categories)){
					$categories =get_article_by_rule($rule_id, $type);
				}
				add_variable('categories', $categories);
				
				$image =json_decode(get_front_right_image($data['lrule_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['lname']." - ".web_title();
				
			}else if(is_num_articles("type=".$type."&sef=".$cek_url[1]) > 0){
			
				$data = fetch_artciles("type=".$type."&sef=".$cek_url[1]);
				$actions->action['meta_title']['func_name'][0] = $data['larticle_title']." - ".web_title();   
	    		$actions->action['meta_title']['args'][0] = '';
				add_variable('title', $data['larticle_title']);
				add_variable('description', $data['larticle_content']);
				
				$navigation = navigation_article($type, $data);
				add_variable('navigation', $navigation);
				
				$image =json_decode(get_front_right_image($data['larticle_id']),true);
				add_variable('image', $image['gallery_list']);
				
				$urlNow='http://'.SITE_URL.'/'.$type.'/'.$data['lsef'].'/';
				$titleNow=$data['larticle_title']." - ".web_title();
				
				$meta_title=get_additional_field($data['larticle_id'], 'meta_title', $type);
				if(!empty($meta_title)){
					$titleNow=$meta_title." - ".web_title();
					$actions->action['meta_title']['func_name'][0] = $titleNow;
				}
				$meta_description=get_additional_field($data['larticle_id'], 'meta_description', $type);
				$actions->action['meta_description']['func_name'][0] = '<meta name="description" value="'.$meta_description.'" />';
				$meta_keywords=get_additional_field($data['larticle_id'], 'meta_keywords', $type);
				$actions->action['meta_keywords']['func_name'][0] = '<meta name="keywords" value="'.$meta_keywords.'" />';
				//print_r($actions);
			
			}
		}
	}
    
    parse_template('Bprograms_block','b_programs',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_programs'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_programs');	
    }
}
?>