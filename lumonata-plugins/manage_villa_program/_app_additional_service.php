<?php 
function get_list_additional_service($index,$data_selected=array()){
		global $db;
		$list = "";
		$data_categories = data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=0");
		$num_categories = data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=0",'num_row');
		$i=1;
		$class = "bb-abu";
		while($category = $db->fetch_array($data_categories)){
			if($i==$num_categories)$class='';
			$name = $category['lname'];
			$parent_id = $category['lrule_id'];
			$num_sub_categories = data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$parent_id",'num_row');
			$sub_additional_service = "";
			if($num_sub_categories>0){
				$sub_additional_service = get_sub_category_additional_service($category['lrule_id'],$index,$data_selected);
			}else{
				$sub_additional_service = get_direct_list_sub_additional_service($category['lrule_id'],$name,$index,$data_selected);
			}
			
			$list .= "<li class=\"$class\"><span><b>$name</b></span> <div class=\"clear\"></div>$sub_additional_service</li>";
			$i++;
		}
		$temp = "<div class=\"list_additional_service\">
                    	<ul>
                        	$list
                        </ul>
                    </div>";
		
		return $temp;
	}
	
	function get_direct_list_sub_additional_service($rule_id,$name,$index,$data_selected){
		global $db;
		
		$list = "";
		$data_rule_relationship = data_tabel('lumonata_rule_relationship',"where lrule_id=$rule_id");
		while($rule_relationship = $db->fetch_array($data_rule_relationship)){
			$id_adf = $rule_relationship['lapp_id'];
			$data_additional_service = data_tabel('lumonata_articles',"where larticle_id=$id_adf",'array');
			$name = "<label>".$data_additional_service['larticle_title']."</label>";
			$data_price_additional_service = data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf");
			
			$list_price = "";
			//validate if use time
			$num_price_time = data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_times'",'num_row');
			//echo $id_adf.'#';
			if($num_price_time>0){
				$times = data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_times'",'array');
				$times = explode(';',$times['lvalue']);
				$prices = data_tabel('lumonata_additional_fields',"where lapp_id=$id_adf and lkey='additional_service_prices'",'array');
				$prices  = explode(';',$prices['lvalue']);
				
				foreach ($prices as $i => $price){
					$time = $times[$i];
					$value = $rule_id.'_'.$id_adf.'_'.$i;
					//validate for selected	
					$class ='';
					$sub_total='';
					$checked='';
					if(!empty($data_selected[$value])){
						$class = 'show';
						$checked = 'checked="checked"';
						$sub_total = $data_selected[$value]['lprice'];
						$qty = $data_selected[$value]['lqty'];
					}			
					
					$list_price .= "<div class=\"price_additonal_service\">
										<input type=\"checkbox\" class=\"checkbox get_price_additional_service\" name=\"get_price_additional_service[$index][]\" 
										 value=\"$value\" rel=\"$id_adf\" $checked /> 
										<span>$price.00 - $time minutes</span>
										<input type=\"text\" value=\"$qty\" name=\"qty_additional_service_".$value."[$index]\" class =\"qty_additional_service $class\" />    
										<span class=\"sub_total_addtional_service\">$sub_total</span>
										<div class=\"clear\"></div>
									</div>";
				}
				
			}else{
				while($additional_price = $db->fetch_array($data_price_additional_service)){
					$price = $additional_price['lvalue'];
					$value = $rule_id.'_'.$id_adf.'_0';
					
					//validate for selected$class ='';
					$sub_total='';
					$checked='';
					$days='';
					if(!empty($data_selected[$value])){
						$class = 'show';
						$checked = 'checked="checked"';
						$sub_total = $data_selected[$value]['lprice'];
						$qty = $data_selected[$value]['lqty'];
						$days = $data_selected[$value]['llday'];
					}		
					
					
					$list_price .= "<div class=\"price_additonal_service\">
										<input type=\"checkbox\" class=\"checkbox get_price_additional_service\" name=\"get_price_additional_service[$index][]\" 		
										 rel=\"$id_adf\" value=\"$value\" $checked /> 
										<span>$price</span>
										<input type=\"text\" value=\"$qty\" name=\"qty_additional_service_".$value."[$index]\" class =\"qty_additional_service $class\" />
										<input type=\"text\" value=\"$days\" name=\"days_additional_service_".$value."[$index]\" class =\"days_additional_service $class\" />     									<span class=\"sub_total_addtional_service \">$sub_total</span>
										<div class=\"clear\"></div>
									</div>";
				}
			}
			$price_detail = "<div class=\"list_price\">$list_price<div class=\"clear\"></div></div>";
			
			$list .= "<li >
							$name$price_detail
							<div class=\"clear\"></div>
					  </li>";
		}
		$temp = "<ul>
					$list
				</ul>";
				
		return $temp;		
	}
	
	function get_sub_category_additional_service($parent_id,$index,$data_selected){
		global $db;
		
		$list_addtional_service = "";
		$data_sub_categories = data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$parent_id");
		while($sub_category = $db->fetch_array($data_sub_categories)){
			//check if have child
			$rule_id = $sub_category['lrule_id'];
			$name = $sub_category['lname'];
			$data_child = data_tabel('lumonata_rules',"where lgroup='additional_service' and lparent=$rule_id",'num_row');
			if($data_child>0){
				//get_sub	
				$list_addtional_service .= get_sub_category_additional_service($rule_id,$index,$data_selected);
			}else{
				$list_addtional_service .= get_direct_list_sub_additional_service($rule_id,$name,$index,$data_selected);
			}
		}
		$temp = "<ul>
					$list_addtional_service
				</ul>";
				
		return $temp;		
		
	}	
	
	function data_tabel($t,$w,$f='result_only'){
		global $db;
		$str = $db->prepare_query("select * from $t $w");
		$result = $db->do_query($str);
		if($f=='result_only') return $result;
		else if($f=='array') return $db->fetch_array($result);
		else if($f=='num_row') return $db->num_rows($result);
	}
	
	function get_additional_service_price_ajax(){
		add_actions('is_use_ajax', true);
		
		global $db;
		$data_id	 = explode('_',$_POST['val_id']);
		$id_rule 	 = $data_id[0];
		$id_adt		 = $data_id[1];
		$index_price = $data_id[2];
		$qty		 = $_POST['val_qty']; 
		
		$num_price_time = data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					      "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						   and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'num_row');	
		$price = 0;				   
		if($num_price_time > 0){
			$data_prices = data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_prices'",'array');	
			$data_prices = explode(';',$data_prices['lvalue']);
			$price = $data_prices[$index_price];		
			$price = $price * $qty;
		}else{
			$data_price = data_tabel('lumonata_additional_fields as a, lumonata_rule_relationship as b, lumonata_rules as c',
					           "where a.lapp_id = b.lapp_id and b.lrule_id=c.lrule_id and c.lgroup = 'additional_service' 
						        and a.lapp_id=$id_adt and b.lrule_id=$id_rule and a.lkey='additional_service_price'",'array');	
			$days  = $_POST['val_day']; 				
			$price = ($data_price['lvalue'] * $qty) * $days;
		}
		echo $price ;			
	
	}
	
	add_actions('get-additional-service-price-ajax_page', 'get_additional_service_price_ajax');


?>