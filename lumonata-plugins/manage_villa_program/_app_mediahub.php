<?php
function is_mediahub_category(){
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	if ($cek_url[0]=='media-hub'){
		$actions->action['meta_title']['func_name'][0] = "Media Hub - ".web_title();   
    	$actions->action['meta_title']['args'][0] = '';
    	return true;
	}else{
		return false;
	}
}

function front_mediahub(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles';
	if ($cek_url[0]=='media-hub'){
		if(!isset($cek_url[1])){
			return front_mediahub_parent();
		}else{
			if($cek_url[1]=='social-media'){
				return front_mediahub_social();
			}else if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0 && $cek_url[1]=='testimonials'){
				return front_mediahub_testimonial();
			}else if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0 && $cek_url[1]=='media-coverage'){
				return front_mediahub_coverage();
			}else if(count_rules("group=".$type."&sef=".$cek_url[1]) > 0 && $cek_url[1]=='blog'){
				if(is_blog_search()){
					return front_mediahub_blog_category("search");	
				}elseif(is_blog_detail()){
					return front_mediahub_blog_detail();	
				}else if(is_blog_category()){
					return front_mediahub_blog_category("category");
				}else{
					return front_mediahub_blog_category();
				}
			}
		}
	}
}

function front_mediahub_parent(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	$actions->action['meta_title']['func_name'][0] = "Media Hub - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', "Media Hub");
	add_variable('description', "");
	$data=array("lgroup"=>"articles");
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	$rule_id=82;
	$categories =get_rule_child($rule_id,$type);
	add_variable('categories', $categories);
	
	$image =json_decode(get_front_right_image($rule_id),true);
	add_variable('image', $image['gallery_list']);
	
	$urlNow='http://'.SITE_URL.'/'.$type_url.'/';
	$titleNow="Media Hub  - ".web_title();
    
    parse_template('Bmediahub_block','b_mediahub',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}


function front_mediahub_testimonial(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub_testimonial.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
	$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', $data['lname']);
	add_variable('description', $data['ldescription']);
	
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	$image =json_decode(get_front_right_image($data['lrule_id']),true);
	add_variable('image', $image['gallery_list']);
	
	$type="articles";
	$id=67;
	$sql=$db->prepare_query("SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder",$type,'publish',$id);
	$r=$db->do_query($sql);
	$content='<ul class="list">';
	while($d=$db->fetch_array($r)){
		$content.='
		<li>
			'.$d[larticle_content].'
			<b>'.$d[larticle_title].'</b>
		</li>
		';		
	}
	$content.='</ul>';
	add_variable('description', $content);
	
	$urlNow='http://'.SITE_URL.'/'.$type_url.'/'.$data['lsef'].'/';
	$titleNow=$data['lname']." - ".web_title();
    
    parse_template('Bmediahub_block','b_mediahub',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}



function front_mediahub_coverage(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub_coverage.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
	$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', $data['lname']);
	add_variable('description', $data['ldescription']);
	
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	$type="articles";
	$id=69;
	$sql=$db->prepare_query("SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder",$type,'publish',$id);
	$r=$db->do_query($sql);
	$content='<ul class="list-files">';
	$no=1;
	while($d=$db->fetch_array($r)){
		
		$article_id=$d['larticle_id'];
		$sql_a=$db->prepare_query("select lattach_loc from lumonata_attachment where larticle_id=%d AND mime_type=%s",$article_id,'application/pdf');
    	$r_a=$db->do_query($sql_a);
    	$d_a=$db->fetch_array($r_a);
    
		$url_file='http://'.SITE_URL.$d_a['lattach_loc'];
		
		$sql_a=$db->prepare_query("select lattach_loc from lumonata_attachment where larticle_id=%d AND (mime_type=%s OR mime_type=%s)",$article_id,'image/jpg','image/jpeg');
    	$r_a=$db->do_query($sql_a);
    	$d_a=$db->fetch_array($r_a);
		$url_image='http://'.SITE_URL.$d_a['lattach_loc'];
		$content.='
			<li class="li_'.$no.'">
				<a href="'.$url_file.'" target="_blank" title="'.$d[larticle_title].'">
				<img alt="" src="'.$url_image.'">
				</a>
				<h4>'.$d[larticle_title].'</h4>
			</li>';
		/*
		$content.='
			<li class="li_'.$no.'">
				<a href="'.$url_file.'" target="_blank" title="'.$d[larticle_title].'">
				<img alt="" src="'.$url_image.'">
				</a>
			</li>';
		*/
		if($no==3){
			$no=0;
		}		
		$no++;
	}
	$content.='</ul>';
	add_variable('content', $content);
	
	$urlNow='http://'.SITE_URL.'/'.$type_url.'/'.$data['lsef'].'/';
	$titleNow=$data['lname']." - ".web_title();
    
    parse_template('Bmediahub_block','b_mediahub',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}

function is_blog_search(){
	global $db;
	$cek_url = cek_url();
	$error=false;	
	if(isset($cek_url[2]) && $cek_url[2]=='search-result'){
    	$error=true;
	}
	
	return $error;
}

function is_blog_category(){
	global $db;
	$cek_url = cek_url();
	$error=false;	
	if(isset($cek_url[2])){
		$sef=$cek_url[2];
		$sql_a=$db->prepare_query("select lrule_id from lumonata_rules where lsef=%s AND lgroup=%s",$sef,'articles');
    	$r_a=$db->do_query($sql_a);
    	$n_a=$db->num_rows($r_a);
    	if(!empty($n_a)){
    		$error=true;
    	}
	}
	
	return $error;
}

function is_blog_detail(){
	global $db;
	$cek_url = cek_url();
	$error=false;
	if(isset($cek_url[3])){
		$ex = explode(".", $cek_url[3]);
		if($ex[1]=='html'){
			$sef=$ex['0'];
			$sql_a=$db->prepare_query("select larticle_id from lumonata_articles where lsef=%s AND larticle_type=%s",$sef,'articles');
	    	$r_a=$db->do_query($sql_a);
	    	$n_a=$db->num_rows($r_a);
	    	if(!empty($n_a)){
	    		$error=true;
	    	}
		}
	}
	
	return $error;
}

function front_mediahub_blog_category($args=''){
	global $actions;
	global $db;
	$cek_url = cek_url();
	//print_r($cek_url);
	
	$type='articles';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub_blog.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	add_variable('url_search','http://'.SITE_URL.'/media-hub/blog/search-result/');
	
	$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
	$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', $data['lname']);
	add_variable('description', $data['ldescription']);
	
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	$type="articles";
	if($args=='search'){
		$id=68;
		$title='Blog Search';
		if(isset($_POST['sKey'])){
			$sKey=$_POST['sKey'];
		}else if(isset($cek_url[3])){
			$sKey=sef_url_back($cek_url[3]);
		}else{
			$sKey='';
		}
		
		if(!empty($sKey)){
			$sql=$db->prepare_query("SELECT a.*
	                                        FROM lumonata_articles a,lumonata_rule_relationship b
	                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
	                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
	                                        AND (a.larticle_title like %s or a.larticle_content like %s)
	                                        ORDER BY a.lorder",$type,'publish',$id,"%".$sKey."%","%".$sKey."%");
		}else{
			$sql=$db->prepare_query("SELECT a.*
	                                        FROM lumonata_articles a,lumonata_rule_relationship b
	                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
	                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
	                                        ORDER BY a.lorder",$type,'publish',$id);
		}
		add_variable('title', $title);
		if(!empty($sKey)){
			$urlNow='http://'.SITE_URL.'/'.$type_url.'/blog/search-result/'.sef_url($sKey).'/';
		}else{
			$sKey=sef_url($sKey);
			$urlNow='http://'.SITE_URL.'/'.$type_url.'/blog/search-result/';
		}
		
		$titleNow=$title." - ".web_title();
		$image =get_blog_image($id);
		add_variable('image', $image);
		add_variable('sKey', $sKey);
	}else if($args=='category'){
		$cek_url = cek_url();
		$sef=$cek_url[2];
		$sql_a=$db->prepare_query("select * from lumonata_rules where lsef=%s AND lgroup=%s",$sef,'articles');
    	$r_a=$db->do_query($sql_a);
    	$d_a=$db->fetch_array($r_a);
    	$id=$d_a['lrule_id'];
    	$title=$d_a['lname'];
		$sql=$db->prepare_query("SELECT a.*
	                                        FROM lumonata_articles a,lumonata_rule_relationship b
	                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
	                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
	                                        ORDER BY a.lorder",$type,'publish',$id);
		add_variable('title', $title);
		$urlNow='http://'.SITE_URL.'/'.$type_url.'/blog/'.$sef.'/';
		$titleNow=$title." - ".web_title();
		$image =get_blog_image($id);
		add_variable('image', $image);
		
	}else{
		$id=68;
		$title='Blog';
		$sql=$db->prepare_query("SELECT a.*
	                                        FROM lumonata_articles a,lumonata_rule_relationship b
	                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
	                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
	                                        ORDER BY a.lorder",$type,'publish',$id);
		add_variable('title', $title);
		$urlNow='http://'.SITE_URL.'/'.$type_url.'/blog/';
		$titleNow=$data['lname']." - ".web_title();
		$image =get_blog_image($id);
		add_variable('image', $image);
	}
	//echo $sql;
	$r=$db->do_query($sql);
	$n=$db->num_rows($r);
	$content='';
	if($args=='search'){
		$content.='<p style="margin-top: 0px; font-style: italic;">Your search for "<b>'.$sKey.'</b>" returned <b>'.$n.'</b> result(s).</p>';
	}
	$content.='<ul>';
	while($d=$db->fetch_array($r)){
		$article_id=$d['larticle_id'];
		$the_title=$d['larticle_title'];
		$the_brief=filter_content($d['larticle_content'],true);
		$post_date=date(get_date_format(),strtotime($d['lpost_date']));
		//$the_categories=the_categories($d['larticle_id'],$d['larticle_type']);
		
		$the_selected_categories=find_selected_rules($d['larticle_id'],'categories',$d['larticle_type']);
		$the_categories=recursive_taxonomy_custom(0,'categories',$d['larticle_type'],'category',$the_selected_categories,'',68);
		$the_categories=trim($the_categories,', ');
		
		$sef=$d['lsef'];
		$frr=fetch_rule_relationship('app_id='.$article_id);
		//print_r($frr);
		if(isset($frr[1]))	$rule_id=$frr[1];
		else $rule_id=$frr[0]; //edited by adi
		
		$d_r=fetch_rule('rule_id='.$rule_id);
		$sef_r=$d_r['lsef'];
		if(isset($cek_url[2])){
			$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$cek_url[2].'/'.$sef.'.html';
		}else{
			$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$sef_r.'/'.$sef.'.html';	
		}
		
		$the_title = '<a href="'.$url_detail.'">'.$the_title.'</a>';
		$content.='
		<li>
		<h3>'.$the_title.'</h3>
		<div class="meta">
		<span>
		Category :
		'.$the_categories.'
		</span>
		<span class="post-date">'.$post_date.'</span>
		</div>
		<div class="post-content">
		'.$the_brief.'
		
		</div>
		<a href="'.$url_detail.'" class="readMore href_ajax_post">Read More &raquo;</a>
		</li>
		';
	}
	$content.='</ul>';
	add_variable('content', $content);
	
	
	
	
	$rule_id=68;
	$type='articles-blog';
	$categories =get_rule_child($rule_id,$type);
	add_variable('categories', $categories);
    
    parse_template('Bmediahub_block','b_mediahub',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}

function front_mediahub_blog_detail(){
	global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles-detail';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub_blog.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	add_variable('url_search','http://'.SITE_URL.'/media-hub/blog/search-result/');
	
	$cek_url = cek_url();
	$ex = explode(".", $cek_url[3]);
	$sef=$ex['0'];
	$sql=$db->prepare_query("select * from lumonata_articles where lsef=%s AND larticle_type=%s",$sef,'articles');
	
	//echo $sql;
	$r=$db->do_query($sql);
	$content='<ul>';
	$d=$db->fetch_array($r);
	$article_id=$d['larticle_id'];
	$the_title=$d['larticle_title'];
	$the_post=$d['larticle_content'];
        
	$the_post=filter_content($the_post);
    $the_post=str_replace("<p>[album_set]</p>", "<div class=\"album_set\">".the_attachment($d['larticle_id'],0)."<br clear=\"both\" /></div>", $the_post,$count_replace);
    $the_post=str_replace("[album_set]", "<div class=\"album_set\">".the_attachment($d['larticle_id'],0)."<br clear=\"both\" /></div>", $the_post,$count_replace2);
        
	$post_date=date(get_date_format(),strtotime($d['lpost_date']));
	//$the_categories=the_categories($d['larticle_id'],$d['larticle_type']);
	
	$the_selected_categories=find_selected_rules($d['larticle_id'],'categories',$d['larticle_type']);
	$the_categories=recursive_taxonomy_custom(0,'categories',$d['larticle_type'],'category',$the_selected_categories,'',68);
	$the_categories=trim($the_categories,', ');
		
	$sef=$d['lsef'];
	$frr=fetch_rule_relationship('app_id='.$article_id);
	$rule_id=$frr[0];
	//echo
	$d_r=fetch_rule('rule_id='.$rule_id);
	$sef_r=$d_r['lsef'];
	if(isset($cek_url[2])){
		$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$cek_url[2].'/'.$sef.'.html';
		$url_back='http://'.SITE_URL.'/media-hub/blog/'.$cek_url[2].'/';
		$data=array("larticle_type"=>"articles","lsef_rule"=>$cek_url[2]);
	}else{
		$url_detail='http://'.SITE_URL.'/media-hub/blog/'.$sef_r.'/'.$sef.'.html';
		$url_back='http://'.SITE_URL.'/media-hub/blog/'.$sef_r.'/';
		$data=array("larticle_type"=>"articles","lsef_rule"=>$sef_r);
	}
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	//get pdf
	$container_pdf = "";
	/*$dt_pdf = data_tabel('lumonata_attachment',"where larticle_id=$article_id and mime_type='application/pdf'",'array');
	if(!empty($dt_pdf)){
		$file_pdf = 'http://'.SITE_URL.$dt_pdf['lattach_loc'];
		
		//get image
		$dt_image = data_tabel('lumonata_attachment',"where larticle_id=$article_id and ltitle='thumb_pdf'",'array');
		$thumb_pdf = 'http://'.SITE_URL.$dt_image['lattach_loc'];
		
		$container_pdf = "<div>
								<!--<p>Download PDF</p>-->
								<a href=\"$file_pdf\">
									<img src=\"$thumb_pdf\" >
								</a>
						 </div>";
		
	}*/
	
	$nextprev='
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$url_prev.'" class="readMore href_ajax_post">Prev</a>
	&nbsp;&nbsp;&nbsp;<a href="'.$url_next.'" class="readMore href_ajax_post">Next</a>
	';
	$nextprev='';
	$temp_shared = "<div class=\"addthis_sharing_toolbox\"></div>
					<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53d9abdf5da6a017\"></script>
					";	
	$content.='
	<li>
	<div class="meta"  style="margin-top: -10px;">
	<span>
	Category :
	'.$the_categories.'
	</span>
	<span class="post-date">'.$post_date.'</span>
	</div>
	<div class="post-content">
	'.$the_post.'
	'.$container_pdf.'
	<br />
	'.$temp_shared.'
	<br />
	<a href="'.$url_back.'" class="readMore href_ajax_post">Back to List </a>
	'.$nextprev.'
	</div>
	</li>
	';
	
	add_variable('title', $the_title);
	add_variable('like_list_blog', 'like-list-blog');
	$urlNow=$url_detail;
	$titleNow=$the_title." - ".web_title();
	
	$content.='</ul>';
	add_variable('content', $content);
	
	
	$image =get_blog_image($article_id);
	add_variable('image', $image);
	
	$rule_id=68;
	$type='articles-blog';
	$categories =get_rule_child($rule_id,$type);
	add_variable('categories', $categories);
    
    parse_template('Bmediahub_block','b_mediahub',false);
    
    $actions->action['meta_title']['func_name'][0] = $the_title." - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
    
    $type='articles';
    $meta_title=get_additional_field($article_id, 'meta_title', $type);
	if(!empty($meta_title)){
		$titleNow=$meta_title." - ".web_title();
		$actions->action['meta_title']['func_name'][0] = $titleNow;
	}
	$meta_description=get_additional_field($article_id, 'meta_description', $type);
	$actions->action['meta_description']['func_name'][0] = '<meta name="description" value="'.$meta_description.'" />';
	$meta_keywords=get_additional_field($article_id, 'meta_keywords', $type);
	$actions->action['meta_keywords']['func_name'][0] = '<meta name="keywords" value="'.$meta_keywords.'" />';
	//print_r($actions);
    
	
	     
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}

function front_mediahub_social(){
global $actions;
	global $db;
	$cek_url = cek_url();
	$type='articles';
	$type_url='media-hub';
	//set template
	set_template(PLUGINS_PATH."/sukhavati/template_front_mediahub_social.html",'template_mediahub');
	//set block
       
	//print_r($_POST);
    add_block('Bmediahub_block','b_mediahub','template_mediahub');
    
    /* Global */
	add_variable('url_themes',get_theme());
	add_variable('url_plugin',SITE_URL.'/lumonata-plugins/sukhavati');
	
	$data = fetch_rule("sef=".$cek_url[1]."&group=".$type);
	$actions->action['meta_title']['func_name'][0] = $data['lname']." - ".web_title();   
    $actions->action['meta_title']['args'][0] = '';
	add_variable('title', $data['lname']);
	add_variable('description', $data['ldescription']);
	
	$navigation = navigation($type,$data);
	add_variable('navigation', $navigation);
	
	$type="articles";
	$id=69;
	$sql=$db->prepare_query("SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder",$type,'publish',$id);
	$r=$db->do_query($sql);
	$content='<ul class="list-files">';
	$no=1;
	while($d=$db->fetch_array($r)){
		
		$article_id=$d['larticle_id'];
		$sql_a=$db->prepare_query("select lattach_loc from lumonata_attachment where larticle_id=%d AND mime_type=%s",$article_id,'application/pdf');
    	$r_a=$db->do_query($sql_a);
    	$d_a=$db->fetch_array($r_a);
    
		$url_file='http://'.SITE_URL.$d_a['lattach_loc'];
		
		$sql_a=$db->prepare_query("select lattach_loc from lumonata_attachment where larticle_id=%d AND (mime_type=%s OR mime_type=%s)",$article_id,'image/jpg','image/jpeg');
    	$r_a=$db->do_query($sql_a);
    	$d_a=$db->fetch_array($r_a);
		$url_image='http://'.SITE_URL.$d_a['lattach_loc'];
		$content.='
			<li class="li_'.$no.'">
				<a href="'.$url_file.'" target="_blank" title="'.$d[larticle_title].'">
				<img alt="" src="'.$url_image.'">
				</a>
			</li>';
		/*
		$content.='
			<li class="li_'.$no.'">
				<a href="'.$url_file.'" target="_blank" title="'.$d[larticle_title].'">
				<img alt="" src="'.$url_image.'">
				</a>
			</li>';
		*/
		if($no==3){
			$no=0;
		}		
		$no++;
	}
	$content.='</ul>';
	add_variable('content', $content);
	
	$urlNow='http://'.SITE_URL.'/'.$type_url.'/'.$data['lsef'].'/';
	$titleNow=$data['lname']." - ".web_title();
    
    parse_template('Bmediahub_block','b_mediahub',false);
         
    if(isset($_POST['pKEY'])=='is_use_ajax'){
    	$value=array('content'=>return_template('template_mediahub'),'urlNow'=>$urlNow,'titleNow'=>$titleNow);
    	return json_encode($value);
    }else{
    	return return_template('template_mediahub');	
    }
}


?>