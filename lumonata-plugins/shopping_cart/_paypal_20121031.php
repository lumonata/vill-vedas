<?php

/*  PHP Paypal IPN Integration Class Demonstration File
 *  4.16.2005 - Micah Carrick, email@micahcarrick.com
 *
 *  This file demonstrates the usage of paypal.class.php, a class designed  
 *  to aid in the interfacing between your website, paypal, and the instant
 *  payment notification (IPN) interface.  This single file serves as 4 
 *  virtual pages depending on the "action" varialble passed in the URL. It's
 *  the processing page which processes form data being submitted to paypal, it
 *  is the page paypal returns a user to upon success, it's the page paypal
 *  returns a user to upon canceling an order, and finally, it's the page that
 *  handles the IPN request from Paypal.
 *
 *  I tried to comment this file, aswell as the acutall class file, as well as
 *  I possibly could.  Please email me with questions, comments, and suggestions.
 *  See the header of paypal.class.php for additional resources and information.
*/


require_once('paypal.class.php');  // include the class file
require_once("../../lumonata_config.php");
require_once("../../lumonata-functions/settings.php");

global $db;

///// Setup class
$p = new paypal_class;             // initiate an instance of the class

function variant_product($rule_id,$lparent){
	global $db;
    $sql=$db->prepare_query("SELECT lname
   		FROM lumonata_rules WHERE lrule_id='%s' and lparent='%s'",$rule_id,$lparent);
    $data=$db->do_query($sql);
    $result=$db->fetch_array($data);
    
    return $result;
}
/////
	  $lorder_id_md5 = $_GET['ns'];
  	  $sql_cek = $db->prepare_query("Select * From lumonata_order");
      $result_cek = $db->do_query($sql_cek);
      while($cek=$db->fetch_array($result_cek)){
       if (md5($cek['lorder_id'])==$lorder_id_md5){
	       $lorder_id = $cek['lorder_id'];
	       $grand_total = $cek['lprice_total'] + $cek['tax_shipping'] + $cek['tax_product'] + $cek['lshipping_price'];
	   }
	 }
$sql_cek_lo = $db->prepare_query("Select * From lumonata_order where lorder_id=%s",$lorder_id);
$result_cek_lo = $db->do_query($sql_cek_lo);
$data_cel_lo = $db->fetch_array($result_cek_lo);

//if($data_cel_lo['lstatus ']==1){ // if paid before

///data send to paypal
$time = time();
$value[0]='name';
$value[1]='text';
$value[2]='mode';
$count_p=0;
$tmp_array_parent=array();
$data_payments=get_meta_data('payments','product_setting');
$array_payments = json_decode($data_payments,true);
      if(is_array($array_payments)){
	      foreach($array_payments['parent_payment'] as $key=>$val){
		      $count_c=0;
		      $tmp_array_parent['parent_payment'][$count_p]=$val;
		     //echo "parent".$key.":".$val;
		     
		      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

		      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
		      		//echo "child ".$subkey.":".$subval."<br>";
			      	$count_c++;
		      	
		      }
		    $count_p++;
       	 }
      }

      for($j=0;$j<$count_p;$j++){
      	if($tmp_array_parent['parent_payment'][$j]=="payments_paypal_standard"){
      		$mode_paypal = $tmp_array_parent['child_payment'][$j]['mode'];
      		$business = $tmp_array_parent['child_payment'][$j]['text'];
      		$method  = "payments_paypal_standard";
      	}
      }

      if($mode_paypal==0){
      	$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
      }else{
      	$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; 
      }
//*/

//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url

$p->paypal_url = $paypal_url;   
     
// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

// if there is not action variable, set the default action of 'process'
if (empty($_GET['action'])) $_GET['action'] = 'process';  

switch ($_GET['action']) {
    
   case 'process':      // Process and order...

      // There should be no output at this point.  To process the POST data,
      // the submit_paypal_post() function will output all the HTML tags which
      // contains a FORM which is submited instantaneously using the BODY onload
      // attribute.  In other words, don't echo or printf anything when you're
      // going to be calling the submit_paypal_post() function.
 
      // This is where you would have your form validation  and all that jazz.
      // You would take your POST vars and load them into the class like below,
      // only using the POST values instead of constant string expressions.
 
      // For example, after ensureing all the POST variables from your custom
      // order form are valid, you might have:
      //
      // $p->add_field('first_name', $_POST['first_name']);
      // $p->add_field('last_name', $_POST['last_name']);
      
   	  ///*
	      
      
      /*$qc=$db->prepare_query("Select * From lumonata_meta_data Where lmeta_name = %s",'paypal_email');
	  $rc=$db->do_query($qc);
	  $dc=$db->fetch_array($rc);
      $business = $dc['lmeta_value'];*/
      //*/
      
      $p->add_field('business', $business);
      //$p->add_field('business', 'l_sell_1293161703_biz@lumonata.com');
      
      $ii=1;
      $array_variant=array();
      $sql_order = $db->prepare_query("SELECT *
					FROM lumonata_order
					INNER JOIN lumonata_order_detail ON lumonata_order.lorder_id = lumonata_order_detail.lorder_id
					INNER JOIN lumonata_order_detail_material ON lumonata_order_detail.ldetail_id = lumonata_order_detail_material.ldetail_id
					WHERE lumonata_order.lorder_id = '$lorder_id'");
      $query_order = $db->do_query($sql_order);
      while ($data_order = $db->fetch_array($query_order)){
      		$product_id = $data_order['lproduct_id'];
      		$member_id = $data_order['lmember_id'];
      		$lquantity_total = $data_order['lquantity_total'];
      		$lshipping_price = $data_order['lshipping_price'];
      		$tax_product = $data_order['tax_product'];
      		$tax_shipping = $data_order['tax_shipping'];
      		$price = $data_order['lprice'];
      		$qty = $data_order['lqty'];
      		
      		//Currency
      		$sql_currency=$db->prepare_query("SELECT lcode from lumonata_currency where lcurrency_id=%s",$data_order['lcurrency_id']);
			$query_currency=$db->do_query($sql_currency);
			$data_currency = $db->fetch_array($query_currency);
			$currency = $data_currency['lcode'];
      		
			//Product Name
      		$sql_article=$db->prepare_query("select larticle_title from lumonata_articles where larticle_id=%s",$product_id);
      		$query_article = $db->do_query($sql_article);
      		$data_article = $db->fetch_array($query_article);
      		$product_name=$data_article['larticle_title'];
      		
      		//variant product
      		if(empty($data_order['product_variant'])){
				$parent_text['lname']='';
				$child_text['lname']='';
      		}else{
      			$array_variant = json_decode($data_order['product_variant'],true);
	      		$product_parent = $array_variant['parent_variant'];
	      		$product_child = $array_variant['child_variant'];
	      		$parent_text = variant_product($product_parent,0);
				$child_text = variant_product($product_child,$product_parent);
      		}
			
			$p->add_field('item_name_'.$ii,$product_name);
			$p->add_field('quantity_'.$ii, $qty);
			$p->add_field('amount_'.$ii, $price);
			$p->add_field('on0_'.$ii, $parent_text['lname']);
		  	$p->add_field('os0_'.$ii, $child_text['lname']);
		  	
			$ii++;
      }
      
      $p->add_field('amount_'.$ii, 1);
      $p->add_field('item_name_'.$ii, 'Tax');
      $p->add_field('amount_'.$ii, $tax_product);
	  
	  $ii++;
      $p->add_field('item_name_'.$ii, 'Shipping');
      $p->add_field('amount_'.$ii, $lshipping_price);
      $p->add_field('on0_'.$ii, 'Ship to');
	  $p->add_field('os0_'.$ii, 'ship loc');
      
	  $ii++;
      $p->add_field('item_name_'.$ii, 'Shipping Tax');
      $p->add_field('amount_'.$ii, $tax_shipping);
      
      ///*
      $p->add_field('return', $this_script.'?action=success&id='.$lorder_id);
      $p->add_field('cancel_return', $this_script.'?action=cancel');
      $p->add_field('notify_url', $this_script.'?action=ipn&id='.$lorder_id);
      $p->add_field('upload',1);
      
      $p->add_field('currency_code',$currency); 
      
      //*/
      $check_order= $db->prepare_query("Select * From lumonata_order_payment where lorder_id= %s",$lorder_id);
      $qry_check_order = $db->do_query($check_order);
      if($db->num_rows($qry_check_order)==0){
	      $q=$db->prepare_query("Select * From lumonata_order,lumonata_members 
	      							Where lumonata_order.lmember_id=lumonata_members.lmember_id 
	      							and lumonata_order.lorder_id= %s",$lorder_id);
		  $r=$db->do_query($q);
		  $d=$db->fetch_array($r);
		  $fname=$d['lfname'];
		  $username=$d['lusername'];
	      $qi=$db->prepare_query("Insert Into lumonata_order_payment (lorder_id,
	      																lstatus,
	      																lpaid,
	      																lcreated_by,
	      																lcreated_date,
	      																lusername,
	      																ldlu,
	      																first_name,
	      																payer_email,
	      																lpayment_method,
	      																ldate_trans,
	      																lname_paid)
																	values(%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
																		$lorder_id,
																		0,
																		$grand_total,
																		$member_id,
																		$time,
																		$username,
																		$time,
																		$fname,
																		'',
																		$method,
																		$time,
																		$fname);
		  $ri=$db->do_query($qi);
      }

      //print_r($p);
     $p->submit_paypal_post(); // submit the fields to paypal
      //$p->dump_fields();      // for debugging, output a table of all the fields
      break;
      
   case 'success':      // Order was successful...
   
      // This is where you would probably want to thank the user for their order
      // or what have you.  The order information at this point is in POST 
      // variables.  However, you don't want to "process" the order until you
      // get validation from the IPN.  That's where you would have the code to
      // email an admin, update the database with payment status, activate a
      // membership, etc.  
 	  
      /*echo "<html><head><title>Success</title></head><body><h3>Thank you for your order.</h3>";
      foreach ($_POST as $key => $value) { echo "$key: $value<br>"; }
      echo "</body></html>";*/
      
   		$val = array();
		foreach ($_POST as $key => $value) { 
			$val[$key]=$value;
		}
		
		$payer_email=$val['payer_email'];
		
      	/*$time = time();
   	    $lorder_id = $_GET['id'];
      	$q=$db->prepare_query("Select lfname From lumonata_order,lumonata_members Where lumonata_order.lmember_id=lumonata_members.lmember_id and lumonata_order.lorder_id= %s",$lorder_id);
		$r=$db->do_query($q);
		$d=$db->fetch_array($r);
		//$grand_total=$d['lgrand_total'];
		$fname=$d['lfname'];
		
		$first_name=$val['first_name'];
		$payer_email=$val['payer_email'];
		$payment_gross=$val['payment_gross'];
		
		$qi=$db->prepare_query("Insert Into lumonata_order_payment 
		(lorder_id,lsatus,lpaid,lcreated_by,lcreated_date,lusername,ldlu,first_name,payer_email,lpayment_method,ldate_trans,lname_paid,lipn)
		values(%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
		$lorder_id,1,$payment_gross,$fname,$time,$fname,$time,
		$first_name,$payer_email,$method,$time,$fname,'');
		$ri=$db->do_query($qi);*/
		
		$lorder_id = $_GET['id'];
		//update status payment status
		$sql_update_payment = $db->prepare_query("UPDATE lumonata_order_payment 
														SET lstatus=%s,payer_email=%s
														WHERE lorder_id = %s",'2',$payer_email,$lorder_id);
		$qry_update_payment = $db->do_query($sql_update_payment);
		
		//update status order status
		$sql_update_order = $db->prepare_query("UPDATE lumonata_order
												SET lstatus=%s
												WHERE lorder_id = %s",'2',$lorder_id);
		$qry_update_order = $db->do_query($sql_update_order);
		
		//send email
		$check_payment=$db->prepare_query("SELECT * FROM lumonata_order_payment WHERE lstatus=%s and lorder_id=%s","2",$lorder_id);
		$query_check=$db->do_query($check_payment);
		$data_payment=$db->fetch_array($query_check);
		
		$dattrans= date('d/m/Y',$data_payment['ldlu']);
		
		$theMailContent='<table width="100%" style="background-color:#666" cellpadding="1" cellspacing="1">
							<tr>
					            <td colspan="4" style="background-color:#CCC">Payment Confirmation</td>
					        </tr>
					        <tr>
					            <td style="background-color:#FFF">Date:</td>
					            <td style="background-color:#FFF" colspan="3">'.$dattrans.'</td>
					        </tr>
					        <tr>
					            <td width="16%"  style="background-color:#FFF">Name Paid:</td>
					            <td width="84%" style="background-color:#FFF" colspan="3">'.$data_payment['lname_paid'].'</td>
					        </tr>
					         <tr>
					            <td style="background-color:#FFF">Paymer Email:</td>
					            <td style="background-color:#FFF" colspan="3">'.$data_payment['payer_email'].'</td>
					        </tr>
					    </table>';
		
		$email_admin = get_meta_data('email');
			       
		$web_title = get_meta_data("web_title");	
		$sendToMail = $email_admin;
		$cc = '';
		$bcc = '';
		
		$subject = 'Confirmation Payment : ORDER ID ['.$lorder_id.']';
	    $headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		//$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";							
		$headers .= "From: ".$email_admin."\r\n";
		$headers .= "Cc: " . "\r\n";
		$headers .= "Bcc: " . "\r\n"; 
		
	  	$send = mail($sendToMail,$subject,$theMailContent,$headers);
		
		if ($qry_update_payment and $qry_update_order and $send){
			$_SESSION['payment_success'] = 1;
			//header("Location: 10.10.10.18/baliworking4u/confirmation-order");
			header("Location: http://".site_url()."/confirmation-order");
		}
      
      // You could also simply re-direct them to another page, or your own 
      // order status page which presents the user with the status of their
      // order based on a database (which can be modified with the IPN code 
      // below).
      
      break;
      
   case 'cancel':       // Order was canceled...

      // The order was canceled before being completed.
 
      //echo "<html><head><title>Canceled</title></head><body><h3>The order was canceled.</h3>";
      //echo "</body></html>";
      header("Location: http://".site_url()."");
      
      break;
      
   case 'ipn':          // Paypal is calling page for IPN validation...
   
      // It's important to remember that paypal calling this script.  There
      // is no output here.  This is where you validate the IPN data and if it's
      // valid, update your database to signify that the user has payed.  If
      // you try and use an echo or printf function here it's not going to do you
      // a bit of good.  This is on the "backend".  That is why, by default, the
      // class logs all IPN data to a text file.
        
   	  /*
   	    $time = time();
   	    $booking_id = $_GET['id'];
      	$q=$db->prepare_query("Select * From lumonata_accommodation_booking Where lbooking_id = %s",$booking_id);
		$r=$db->do_query($q);
		$d=$db->fetch_array($r);
		$grand_total=$d['lgrand_total'];
		$fname=$d['lfname'];
		
		$qi=$db->prepare_query("Insert Into lumonata_accommodation_booking_payment 
		(lbooking_id,lsatus,lpaid,lcreated_by,lcreated_date,lusername,ldlu) values
		(%s,%d,%s,%s,%d,%s,%d)",$booking_id,1,$grand_total,$fname,$time,$fname,$time);
		$ri=$db->do_query($qi);
		if ($ri){
			header("Location: http://www.lumonatalabs.com/dipanvillarental/villa-reservation-details/");
		}
		*/
      
      if ($p->validate_ipn()) {
          
         // Payment has been recieved and IPN is verified.  This is where you
         // update your database to activate or process the order, or setup
         // the database with the user's order details, email an administrator,
         // etc.  You can access a slew of information via the ipn_data() array.
  
         // Check the paypal documentation for specifics on what information
         // is available in the IPN POST variables.  Basically, all the POST vars
         // which paypal sends, which we send back for validation, are now stored
         // in the ipn_data() array.
  
         // For this example, we'll just email ourselves ALL the data.
         $subject = 'Instant Payment Notification - Recieved Payment';
         $to = 'dana@lumonata.com';    //  your email
         $body =  "An instant payment notification was successfully recieved\n";
         $body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
         $body .= " at ".date('g:i A')."\n\nDetails:\n";
         
         foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
         echo "SUCCESS";
         mail($to, $subject, $body);
      }
      break;
 }     

?>