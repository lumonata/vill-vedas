<?php 
	function get_admin_product_order($type='shoppingcart&sub=product_order',$thetitle='Order|Order',$tabs=array()){
		$articletabtitle=explode("|",$thetitle);
        $tabs=array_merge(array('order'=>$articletabtitle[1]));
        if(is_contributor() || is_author()){
            //$tabs=array_slice($tabs, 0,1);
            foreach($tabs as $key=>$val){
                if(is_grant_app($key)){
                    $thetabs[$key]=$val;
                }
            }
            $tabs=$thetabs;
            
        }else{
             $tabs=$tabs;
        }

        /* Configure the tabs
            $the_tab is the selected tab */
        $tab_keys=array_keys($tabs);
        $tabb='';
        if(empty($_GET['tab']))
             $the_tab=$tab_keys[0];
        else
             $the_tab=$_GET['tab'];
       
        $articles_tabs=set_tabs($tabs,$the_tab);
        add_variable('tab',$articles_tabs);

		if(is_delete_all()){
                add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this Order:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these Orders:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                        $warning.="<li>".$val."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('shoppingcart&sub=product_order')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
                return $warning;
	   	}elseif(is_confirm_delete()){
                foreach($_POST['id'] as $key=>$val){
					delete_product_order($val);
                }
                return get_order_list($type='','Order',$articles_tabs);
        }else{
				return get_order_list($type='','Order',$articles_tabs);
        }
	}
	
	function get_order_list($type='products',$title,$articles_tabs){
		global $db;
        $list='';
        
		/*if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }*/
        
		/*if(is_search()){
                $sql=$db->prepare_query("select * from lumonata_order where lorder_id=%s",$_GET['s']);
                $num_rows=count_rows($sql);
                
        }else{*/
                //$show_data=$_GET['data_to_show'];  
                //$where=$db->prepare_query("WHERE $w larticle_type=%s",$type);
                //setup paging system
                $url=get_state_url('shoppingcart&sub=product_order')."&page=";
                
                $num_rows=count_rows("select * from lumonata_order");
        /*}*/
	
        $viewed=list_viewed();
        if(isset($_GET['page'])){
            $page= $_GET['page'];
        }else{
            $page=1;
        }
        
		$limit=($page-1)*$viewed;
        if(is_search()){
                $sql=$db->prepare_query("select * from lumonata_order where lorder_id=%s limit %d, %d","%".$_POST['s']."%","%".$_POST['s']."%",$limit,$viewed);
               
        }else{
                   // $where=$db->prepare_query("WHERE $w lorder_id=%s",$type);     
                $sql=$db->prepare_query("select * from lumonata_order order by lorder_id limit %d, %d",$limit,$viewed);
               
        }
        
        //if($viewed*$page > $num_rows && $num_rows!=0 && $page>1)
        //    header("location:".$url."1");
        $result=$db->do_query($sql);
        
        $start_order=($page - 1) * $viewed + 1; //start order number
        if($_COOKIE['user_type']=="contributor"){
            $button="<li>".button('button=delete&type=submit&enable=false')."</li>";
        }else{
            $button="<li>".button('button=delete&type=submit&enable=false')."</li>";
        }
        
		$list.="<h1>Order</h1>
        <ul class=\"tabs\">$articles_tabs</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('shoppingcart&sub=product_order')."\" method=\"post\" name=\"alist\">
                           <div class=\"button_right\">
                                ".search_box_order('http://'.SITE_URL.'/front-product-ajax','list_item','src','right','alert_green_form','Search pages')."
                           </div>
                           <br clear=\"all\" />
                           
                           <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
                           <input type=\"hidden\" name=\"state\" value=\"pages\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                          $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"status_to_show\"></div>
                            
                            <div class=\"list\">
                                <div class=\"list_title\">
                                    <input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" />
                                    <div class=\"pages_title\" style=\"width:160px\">Order Id</div>
                                    <div class=\"list_author\" style=\"width:150px\">Order Date</div>
                                    <div class=\"avatar\"></div>
                                    <div class=\"list_comments\" style=\"text-align:left\">Qty</div>
                                    <div class=\"list_date\">Item</div>
                                    <div class=\"list_date\" style=\"padding-left:3px;width:70px\">Status</div>
                                    <div class=\"list_date\" style=\"padding-left:25px\">Total Price</div>
                                    <!--div class=\"list_date\">Total Price</div -->
                                </div>
                                <div id=\"list_item\">".order_list($result,$start_order,'',2).'</div>
                			</div>';
                        $list.="<div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                        $button
                                    </ul>   
                                </div>
                        </div>
                        <div class=\"paging_right\">
                                    ". paging($url,$num_rows,$page,$viewed,5)."
                        </div>
                </div>
            </form> 
            <script type=\"text/javascript\" language=\"javascript\">   
            </script>";
            add_actions('section_title','ShoppingCart - Order');
        return $list;
	}
	
	function order_list($result,$i=1,$id='',$status=0){
		global $db;
        $list="";
        if($db->num_rows($result)==0 && $status==1)
            return "<div class=\"alert_yellow_form\">No result found for <em>".$id."</em>. Check your spellling or try another terms</div>";
        elseif($db->num_rows($result)==0)
            return "<div class=\"alert_yellow_form\">No record on database</div>";
		
	while($d=$db->fetch_array($result)){
				//$total_price =0;
				$total_price = $d['tax_product'] + $d['tax_shipping'] + $d['lprice_total'] + $d['lshipping_price'];
				
				$sql=$db->prepare_query("SELECT * from lumonata_currency where lcurrency_id=%d",$d['lcurrency_id']);
				$query=$db->do_query($sql);
				$data_currency=$db->fetch_array($query);
				$symbol =$data_currency['lsymbol'];
				if($d['lstatus']==1){
					$status= "Pending";
				}else if($d['lstatus']==2){
					$status="Paid";
				}else if($d['lstatus']==4){
					$status="Partially Shipped";
				}else if($d['lstatus']==5){
					$status="Shipped";
				}else{
					$status="Cancel";
				}
                $list.= "<div class=\"list_item clearfix\" id=\"theitem_".$d['lorder_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['lorder_id']."\"/>
                                <div class=\"pages_title\" style=\"width:153px\">".$d['lorder_id']."</div>
                                <div class=\"avatar\" style=\"width:150px\">".$d['lorder_date']."</div>
                                
                                <div class=\"list_comments\" style=\"text-align:center;\">".$d['lquantity_total']."</div>
                                <div class=\"list_date\" style=\"margin-left:40px\">".$d['litem_total']."</div>
                                <div class=\"list_date\" style=\"padding-left:0px\"><span class=\"status_order\" id=\"status_order_".$d['lorder_id']."\" rel=".$d['lorder_id'].">".$status." <a style=\"text-decoration:underline;cursor:pointer;font-size: 10px;display:none\" rel=".$d['lorder_id']." id=\"edit_status_".$d['lorder_id']."\" class=\"edit_status_order\" >[Edit]</a></div>
                                <div class=\"list_date\" style=\"padding-left:10px\">".$data_currency['lsymbol']." ".$total_price."</div>
                                <!-- div class=\"list_order\" --><input type=\"hidden\" value=\"\" id=\"order_\" class=\"small_textbox\" name=\"order[]\"><!-- /div -->"; 
                $list.='<div class="view_detail_order" rel="'.$d['lorder_id'].'" id="the_detail_order_'.$d['lorder_id'].'" style="display:none">
                		<div class="button_wrapper" style="clear: both;height:30px">
                			<span style="float:left;padding:7px;">Order Detail<span>
                		</div>
                		<div class="detail_order" id="detail_order" style="clear: both;margin:  0px 0 10px 20px;">
                		<table width="100%">
	                		<tr>
								<td>Product</td>
	                			<td>&nbsp;</td>
	                			<td>Item Price</td>
	                			<td>QTY</td>
	                			<td>Total</td>
	                		</tr>
	                		<tr>
	                			<td colspan="5"><div style="border-top: 1px dotted #61503D;clear:both"></div></td>
	                		</tr>';
                
                $sql_order = $db->prepare_query("SELECT *
					FROM lumonata_order
					INNER JOIN lumonata_order_detail ON lumonata_order.lorder_id = lumonata_order_detail.lorder_id
					INNER JOIN lumonata_order_detail_material ON lumonata_order_detail.ldetail_id = lumonata_order_detail_material.ldetail_id
					WHERE lumonata_order.lorder_id = '$d[lorder_id]'");
      			$query_order = $db->do_query($sql_order);
      			while ($data_order = $db->fetch_array($query_order)){
      				$total_z = $data_order['lprice_total'];
      				$shipping =$data_order['tax_shipping'];
      				$tax_z = $data_order['tax_product'];
      				$price_z=$data_order['lshipping_price'];
	      			$images_url = get_product_detail_images($data_order['lproduct_id']);
	      			
	      			$query_product=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.larticle_id=%s",
                                'categories','products',0,$data_order['lproduct_id']);
					$result_product=$db->do_query($query_product);
			      	$data_product=$db->fetch_array($result_product);
			      	
	      			//variant product
		      		$array_variant = json_decode($data_order['product_variant'],true);
		      		$product_parent = $array_variant['parent_variant'];
		      		$product_child = $array_variant['child_variant'];
		      		$parent_text= variant_product_be($product_parent,0);
					$child_text = variant_product_be($product_child,$product_parent);
					
      				$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data_order['lproduct_id'],'product_variant','products');
				      $result_variant=$db->do_query($sql_variant);
				      $data_variant=$db->fetch_array($result_variant);
				      //echo $data_variant['lvalue'];
				      $obj_variant = json_decode( $data_variant['lvalue'],true);
				      $count_it=0;
				      
				      $varian_idparent=array();
				        if(is_array($obj_variant)){
						      foreach($obj_variant['parent_variant'] as $key=>$val){
							    $sql_added=$db->prepare_query("select * from lumonata_rules
							                where
							                lrule_id = %d", $val);
								$execute_added=$db->do_query($sql_added);
								$data_added=$db->fetch_array($execute_added);
								$varian_idparent[$count_it]=$val;
								$varian_parent[$count_it]=$data_added['lname'];
								$j=0;
								if($varian_idparent[$count_it] == $product_parent){
								      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
								      			if($subkey == $product_child){
								      				$the_value[$j]=$subval[0];
									      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
										                where
										                lparent = %d and lrule_id= %s",$val, $subkey);
									   				$execute_added_child=$db->do_query($sql_added_child);
									   				$data_added_child=$db->fetch_array($execute_added_child);
									   				$the_var_id[$j]=$subkey;
									   				$the_var[$j]=$data_added_child['lname'];
									      			$j++;
									      			
								      			}
								      	}
								      	break;
								}
						      	$count_it++;
				      		}
				   		}
				   	if (isset($the_value[0])){
						$the_price = $the_value[0];
				   	}else{
				   		$the_price = '';
				   	}
					//$the_price=get_product_variant_detail($data_order['lproduct_id'],$product_parent,$product_parent);
		      		/*$the_price=get_product_variant_detail($data_order['lproduct_id'],$product_parent,$product_parent);*/
					
					$additional_product=default_price($data_order['lproduct_id']);
					if($the_price==''){
						$the_price=$additional_product['price'];
					}
					///by yana
					$the_price = $data_order['lprice'];
					$item_total_price = $the_price * $data_order['lqty'];
					///
					$the_code=$additional_product['code'];
					$list.='
	                		<tr>
								<td width="12%"><img src="'.$images_url.'" style="width:80px;border: 1px solid #CCCCCC;"/></td>
	                			<td width="40%">
	                			<strong>'.$the_code."<br>".$data_product['larticle_title'].'</strong><br>
	                			'.$parent_text['lname'].' : '.$child_text['lname'].'</td>
	                			<td width="15%">'.$data_currency['lsymbol'].' '.$the_price.'</td>
	                			<td width="15%">'.$data_order['lqty'].'</td>
	                			<td width="18%">'.$data_currency['lsymbol'].' '.$item_total_price.'</td>
	                		</tr>
	                		<tr>
	                			<td colspan="5"><div style="border-top: 1px dotted #61503D;clear:both"></div></td>
	                		</tr>
						';
      			}$grand =$total_z + $price_z + $tax_z + $shipping;
      			$list.='<tr>
                			<td align="right" colspan="4"><strong>Sub Total :<strong></td>
                			<td>'.$symbol.' '.$total_z.'</td>
                		</tr>
      					<tr>
                			<td align="right" colspan="4"><strong>Shipping [</strong><i>'.$d['lshipping_option'].'</i><strong>] :</strong></td>
                			<td>'.$symbol.' '.$price_z.'</td>
                		</tr>
                		<tr>
                			<td align="right" colspan="4"><strong>Tax :<strong></td>
                			<td>'.$symbol.' '.$tax_z.'</td>
                		</tr>
                		<tr>
                			<td align="right" colspan="4"><strong>Shipping Tax :<strong></td>
                			<td>'.$symbol.' '.$shipping.'</td>
                		</tr>
                		<tr>
                			<td align="right" colspan="4"><strong>Grand Total :<strong></td>
                			<td>'.$symbol.' '.$grand.'</td>
                		</tr>
                		</table>
                	</div>';
                $sql_member=$db->prepare_query("Select lumonata_members.* From lumonata_order,lumonata_members 
	      							Where lumonata_order.lmember_id=lumonata_members.lmember_id 
	      							and lumonata_order.lorder_id= %s",$d['lorder_id']);
			  	$query_member=$db->do_query($sql_member);
			  	$data_member=$db->fetch_array($query_member);
			  	
			  	$sql_shipping_address=$db->prepare_query("Select lumonata_order_shipping.* From lumonata_order,lumonata_order_shipping 
							      				Where lumonata_order.lorder_shipping_id=lumonata_order_shipping.lorder_shipping_id 
							      				and lumonata_order.lorder_id= %s",$d['lorder_id']);
			  	$query_shipping_address=$db->do_query($sql_shipping_address);
			  	$data_shipping_address=$db->fetch_array($query_shipping_address);
			  	$country_member = get_country($data_member['lcountry_id']);
			  	$data_country_member = $db->fetch_array($country_member);
			  	$country_shipping = get_country($data_shipping_address['lcountry_id']);
			  	$data_country_shipping = $db->fetch_array($country_shipping);
			  	$sql_payment=$db->prepare_query("Select * From lumonata_order_payment where lorder_id= %s",$d['lorder_id']);
			  	$query_payment=$db->do_query($sql_payment);
			  	$data_payment=$db->fetch_array($query_payment);
			  	if($data_payment['lpayment_method']=="payments_paypal_standard"){
			  		$the_methode="PAYPAL";
			  	}else if($data_payment['lpayment_method']=="payments_cheque"){
			  		$the_methode="CHEQUE";
			  	}elseif($data_payment['lpayment_method']=="payments_bank_transfer"){
			  		$the_methode="BANK TRANSFER";
			  	}else{
			  		$the_methode="-";
			  	}
				if(!empty($data_payment['laccount_number'])){
					$account_number_payment = $data_payment['laccount_number'];
				}else{$account_number_payment="-";}
				
				if(!empty($data_payment['payer_email'])){
					$email_payment = $data_payment['payer_email'];
				}else{$email_payment="-";}
				
				if(!empty($data_payment['ldestination_bank'])){
					$bank_payment = $data_payment['ldestination_bank'];
				}else{$bank_payment="-";}
				
				if($data_payment['lstatus']==2){
					$trans_date = date('d/m/Y',$data_payment['ldlu']);
               	}else{
               		$trans_date='-';
               	}
               	
				if(!empty($data_payment['lfile'])){
					$img = $data_payment['lfile'];
					$url_img ='<a rel="view_colorbox_elastic" href="http://'.SITE_URL.$img.'"><img src="http://'.SITE_URL.$img.'" style="width:100px"></a>';
               	}else{
               		$img='-';
               		$url_img ='-';
               	}
               	
				if(!empty($data_payment['cheque_number'])){
					$cheque_number = $data_payment['cheque_number'];
               	}else{
               		$cheque_number='-';
               	}
				
                $list.='
                		<div class="detail_order" id="detail_order" style="clear: both;margin:  0px 0 10px 20px;">
	                		<table width="100%">
	                		<tr><td width="50%">
	                			<div class="button_wrapper" style="clear: both;height:30px;margin-left: 0px;margin-bottom:0px;border-radius: 5px 5px 0 0;">
                					<span style="float:left;padding:7px;">Billing Address<span>
                				</div>
                				<div style="border: 1px solid #BBBBBB;border-radius: 0px 0px 5px 5px;width:96%">
		                		<table width="100%">
		                		<tr>
									<td width="50%">Name</td>
		                			<td>'.$data_member['lfname'].' '.$data_member['llname'].'</td>
		                		</tr>
		                		<tr>
								<td>Address First Line</td>
		                			<td>'.$data_member['laddress'].'</td>
		                		</tr>
		                		<tr>
								<td>Address Second Line</td>
		                			<td>'.$data_member['laddress2'].'</td>
		                		</tr>
		                		<tr>
								<td>Address Third Line</td>
		                			<td>'.$data_member['laddress3'].'</td>
		                		</tr>
		                		<tr>
		                			<td>City</td>
		                			<td>'.$data_member['lcity'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Region</td>
		                			<td>'.$data_member['lregion'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Postal Code</td>
		                			<td>'.$data_member['lpostal_code'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Country</td>
		                			<td>'.$data_country_member['lcountry'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Primary Phone Number </td>
		                			<td>'.$data_member['lphone'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Alternative Phone Number</td>
		                			<td>'.$data_member['lphone2'].'</td>
		                		</tr>
		                		<tr>
		                			<td>Email</td>
		                			<td>'.$data_member['lemail'].'</td>
		                		</tr>
								</table>
								</div>
							</td>
							<td width="50%">
								<div class="button_wrapper" style="clear: both;height:30px;margin-left: 0px;margin-bottom:0px;border-radius: 5px 5px 0 0;">
                					<span style="float:left;padding:7px;">Shipping Address<span>
                				</div>
                				<div style="border: 1px solid #BBBBBB;border-radius: 0px 0px 5px 5px;width:96%">
									<table width="100%">
				                		<tr>
											<td width="50%">Name</td>
				                			<td>'.$data_shipping_address['lfname'].' '.$data_shipping_address['llname'].'</td>
				                		</tr>
				                		<tr>
										<td>Address First Line</td>
				                			<td>'.$data_shipping_address['laddress'].'</td>
				                		</tr>
				                		<tr>
										<td>Address Second Line</td>
				                			<td>'.$data_shipping_address['laddress2'].'</td>
				                		</tr>
				                		<tr>
										<td>Address Third Line</td>
				                			<td>'.$data_shipping_address['laddress3'].'</td>
				                		</tr>
				                		<tr>
				                			<td>City</td>
				                			<td>'.$data_shipping_address['lcity'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Region</td>
				                			<td>'.$data_shipping_address['lregion'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Postal Code</td>
				                			<td>'.$data_shipping_address['lpostal_code'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Country</td>
				                			<td>'.$data_country_shipping['lcountry'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Primary Phone Number </td>
				                			<td>'.$data_shipping_address['lphone'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Alternative Phone Number</td>
				                			<td>'.$data_shipping_address['lphone2'].'</td>
				                		</tr>
				                		<tr>
				                			<td>Email</td>
				                			<td>'.$data_shipping_address['lemail'].'</td>
				                		</tr>
									</table>
								</div>	
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="button_wrapper" style="clear: both;height:30px;margin-left: 0px;margin-bottom:0px;border-radius: 5px 5px 0 0;;width:98%;">
                					<span style="float:left;padding:7px;">Payment<span>
                				</div>
                				<div style="border: 1px solid #BBBBBB;border-radius: 0px 0px 5px 5px;width:98%">
									<table width="100%">
										<tr>
											<td width="25%">Date</td>
				                			<td>'.$trans_date.'</td>
				                		</tr>
				                		<tr>
											<td>Name</td>
				                			<td>'.$data_payment['lname_paid'].'</td>
				                		</tr>
				                		<tr>
										<td>Method</td>
				                			<td>'.$the_methode.'</td>
				                		</tr>
				                		<tr>
										<td>Account Number</td>
				                			<td>'.$account_number_payment.'</td>
				                		</tr>
				                		<tr>
										<td>Bank</td>
				                			<td>'.$bank_payment.'</td>
				                		</tr>
				                		<td>Cheque Number</td>
				                			<td>'.$cheque_number.'</td>
				                		</tr>
				                		<tr>
				                			<td>Paymer Email</td>
				                			<td>'.$email_payment.'</td>
				                		</tr>
				                		<tr>
				                			<td>Transfer Receipt File</td>
				                			<td>'.$url_img.'</td>
				                		</tr>
									</table>
								</div>	
							</td>
						</tr>
						</table>
						</div>';
                $list.='</div>';
                $list.="<div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\";padding-left:2px\" id=\"the_navigation_".$d['lorder_id']."\">
                                                <a href=\"javascript:;\" rel=\"delete_".$d['lorder_id']."\">Delete</a> |
                                                <a style=\"text-decoration:underline;cursor:pointer\" rel=".$d['lorder_id']." id=\"show_".$d['lorder_id']."\" class=\"show_details\" >Show Detail</a>       
                                        </div>
                                </div>
                               <script type=\"text/javascript\" language=\"javascript\">
                                $('#the_navigation_".$d['lorder_id']."').hide();
                                        $('#theitem_".$d['lorder_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['lorder_id']."').show();
                                                $('#edit_status_".$d['lorder_id']."').css('display','');
                                        });
                                        $('#theitem_".$d['lorder_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['lorder_id']."').hide();
                                                $('#edit_status_".$d['lorder_id']."').css('display','none');
                                        });
                                </script></div>";
                $id = $d['lorder_id'];
		        $post_index = $d['lorder_id'];
		        $thaUrl= 'http://'.SITE_URL.'/front-product-ajax';
		        $close_frameid = "theitem_".$d['lorder_id'];
		        $var = 'state=pages&prc=delete&id='.$d['lorder_id'];
		        $var_no='';
              	$msg="Are sure want to delete order ID ".$d['lorder_id']."?";
               	//add_actions('admin_tail','delete_confirmation_box_product',$d['lorder_id'],$msg,"http://".SITE_URL."/product_ajax","theitem_".$d['lorder_id'],'pKEY=delete_product&id='.$d['lorder_id']);
                $delete_confirmation_box2 = delete_confirmation_order($id,$post_index,$msg,$thaUrl,$close_frameid,$var='',$var_no='');
		        $msg1="Are you sure edit order status ".$d['lorder_id']."?";
		        $edit_status_box=edit_box_order($id,$post_index,$msg1,$thaUrl,$close_frameid,$var='',$var_no='');
               	$list.=$edit_status_box.$delete_confirmation_box2;
                $i++;

        }
         $list.='<script>
           			$(document).ready(function(){
						$(".show_details").click(function(){
							var id_order = $(this).attr("rel");
							$("#the_detail_order_"+id_order).slideToggle("slow");
							
							var text =$(this).text();
							if(text=="Show Detail"){
								$(this).text("Hide Detail");
							}else{
								$(this).text("Show Detail");
							}
						});
						
						$("input[name=select_all]").removeAttr("checked");
				        $("input[name=select[]]").each(function(){
							$("input[name=select[]]").removeAttr("checked");
						});
						
						$("input[name=select_all]").click(function(){
                			var checked_status = this.checked;
                			 $(".select").each(function(){
                			 	 this.checked = checked_status;
                        		if(checked_status){
									$("input[name=delete]").removeClass("btn_delete_disable");
                            		$("input[name=delete]").addClass("btn_delete_enable");
                            		$("input[name=delete]").removeAttr("disabled");
								}else{
									$("input[name=delete]").removeClass("btn_delete_enable");
                            		$("input[name=delete]").addClass("btn_delete_disable");
                            		$("input[name=delete]").attr("disabled", "disabled");
								}
							})
                		});
						
                		$(".select").click(function(){
	                		if($(".select:checked").length > 0){
								$("input[name=delete]").removeClass("btn_delete_disable");
								$("input[name=delete]").addClass("btn_delete_enable");
					            $("input[name=delete]").removeAttr("disabled");
							} else{
								$("input[name=delete]").removeClass("btn_delete_enable");
				                $("input[name=delete]").addClass("btn_delete_disable");
							}
						});
						
						return_colorbox();
					});
					
	            	
					function return_colorbox (){
				       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
					}
           			</script>';
		return $list;
	}
	
function variant_product_be($rule_id,$lparent){
	global $db;
    $sql=$db->prepare_query("SELECT lname
   		FROM lumonata_rules WHERE lrule_id='%s' and lparent='%s'",$rule_id,$lparent);
    $data=$db->do_query($sql);
    $result=$db->fetch_array($data);
    
    return $result;
}

function get_variant_price_be($productid,$product_parent,$product_child){
	global $db;
	$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$productid,'product_variant','products');
      $result_variant=$db->do_query($sql_variant);
      $data_variant=$db->fetch_array($result_variant);
      //echo $data_variant['lvalue'];
      $obj_variant = json_decode( $data_variant['lvalue'],true);
      $count_it=0;
	  $varian_idparent=array();
        if(is_array($obj_variant)){
	      foreach($obj_variant['parent_variant'] as $key=>$val){
		    $sql_added=$db->prepare_query("select * from lumonata_rules
		                where
		                lrule_id = %d", $val);
			$execute_added=$db->do_query($sql_added);
			$data_added=$db->fetch_array($execute_added);
			$varian_idparent[$count_it]=$val;
			$varian_parent[$count_it]=$data_added['lname'];
			$j=0;
			if($varian_idparent[$count_it] == $product_parent){
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			if($subkey == $product_child){
			      				$the_value[$j]=$subval[0];
				      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
					                where
					                lparent = %d and lrule_id= %s",$val, $subkey);
				   				$execute_added_child=$db->do_query($sql_added_child);
				   				$data_added_child=$db->fetch_array($execute_added_child);
				   				$the_var_id[$j]=$subkey;
				   				$the_var[$j]=$data_added_child['lname'];
				      			$j++;
				      			
			      			}
			      	}
			      	break;
			}
	      	$count_it++;
      	}
   	}
	return $the_value[0];
}

function search_box_order($keyup_action='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
		$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
						<div class=\"textwrap\">
						    <input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
						</div>
						<div class=\"buttonwrap\">
						    <input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
						</div>
            	 	</div>
            	 	<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
            	 		<img src=\"". get_theme_img() ."/loader.gif\"  />
            	 	</div>
            	 	";
			    
		if(!empty($keyup_action)){
			$searchbox.="<script type=\"text/javascript\">
				$(function(){
					
					$('.searchtext').keyup(function(){
						
						$('#$results_id').html('<div class=".$class.">Searching...</div>');
						var s = $('input[name=s]').val();
						var parameter='".$param."s='+s;
						
						$('#search_loader').show();
						/*$.post('".$keyup_action."',parameter,function(data){
							 $('#".$results_id."').html(data);
							 $('#search_loader').hide();
						});*/
						
						jQuery.post('".$keyup_action."',{ 
				    		pKEY:'".$param."',
				    		lvalue:s
			              },function(data){
			              		$('#".$results_id."').html(data);
							 	$('#search_loader').hide();
						  });
						$('#response').html('');
						
					});
					
					
				});
				
				$(function(){
					$('.searchtext').focus(function(){
						$('.searchtext').val('');
					});
				});
				$(function(){
					var search_text='".$text."';
					$('.searchtext').blur(function(){
						$('.searchtext').val($(this).val()==''?search_text:$(this).val());
					});
				});
				</script>";
		}	    
		return $searchbox;
	}

	function delete_confirmation_order($id,$post_index,$msg,$url,$close_frameid,$var='',$var_no=''){

		/*if(empty($var))
			echo $var[$post_index]="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			echo "test ".$var='';
		else
			echo "value ".$var=$var;*/
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 15px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\" style=\"padding-top:3px\">Yes</button>";
						//$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\" style=\"padding-top:3px\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						thaUrl = '$url';
						var id = '$id';
						jQuery.post(thaUrl,{ 
				                pKEY:'delete_product_order',
				                val:id
					    },function(data){
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    $('#theitem_".$id."').remove();
				        });
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		$delbox.="</div>";
		
		return $delbox;
	}
	
	function edit_box_order($id,$post_index,$msg,$url,$close_frameid,$var='',$var_no=''){

		/*if(empty($var))
			echo $var[$post_index]="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			echo "test ".$var='';
		else
			echo "value ".$var=$var;*/
			
		$delbox="<div id=\"edit_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg<br><select name='option_status' id='option_status_".$id."' style='width:95%'>".status_order($id)."</select></div>";
					$delbox.="<div style=\"text-align:right;margin:10px 15px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_edit\" value=\"yes\" class=\"button\" id=\"edit_yes_".$id."\" style=\"padding-top:3px\">Yes</button>";
						//$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_edit\" value=\"cancel\" class=\"button\" id=\"edit_cancel_".$id."\" style=\"padding-top:3px\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"edit_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('#edit_status_".$id."').click(function(){
							$('#edit_confirmation_wrapper_".$id."').show('fast');
						});
					});
			
					$(function(){
						$('a[rel=edit_status_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#edit_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					
					$(function(){
						$('#edit_cancel_".$id."').click(function(){
							$('select').show();
						    $('#edit_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		
		$delbox.="$(function(){
					$('#edit_yes_".$id."').click(function(){
						$('select').show();
						thaUrl = '$url';
						var id = '$id';
						var lvalue = $('#option_status_".$id." option:selected').val();
						jQuery.post(thaUrl,{ 
				                pKEY:'edit_status_order',
				                val:id,
				                value:lvalue
					    },function(data){
						    $('#edit_confirmation_wrapper_".$id."').hide('fast');
						    location.reload();
						   // $('#theitem_".$id."').remove();
				        });
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		$delbox.="</div>";
		
		return $delbox;
	}
	
function status_order($id_order){
	global $db;
	$sql=$db->prepare_query("select * from lumonata_order where lorder_id=%s",$id_order);
    $qry=$db->do_query($sql);
    $data=$db->fetch_array($qry);
    if($data['lstatus']==3){
    	$a='selected="selected"';
    	$b='';
    	$c='';
    	$d='';
    	$e='';
    }else if($data['lstatus']==2){
    	$b='selected="selected"';
    	$a='';
    	$c='';
    	$d='';
    	$e='';
    }else if($data['lstatus']==4){
    	$d='selected="selected"';
    	$b='';
    	$a='';
    	$c='';
    	$e='';
	}else if($data['lstatus']==5){
    	$e='selected="selected"';
    	$a='';
    	$c='';
    	$b='';
    	$d='';
    }else{
    	$c='selected="selected"';
    	$b='';
    	$a='';
    	$d='';
    	$e='';
    }
	$result = "<option value='1' $c style='background-color:#FFFFFF'>Pending</option>
				<option value='2' $a style='background-color:#FFFFFF'>Paid</option>
				<option value='3' $b style='background-color:#FFFFFF'>Cancel</option>
				<option value='4' $d style='background-color:#FFFFFF'>Partially Shipped</option>
				<option value='5' $e style='background-color:#FFFFFF'>Shipped</option>";
	return $result;
}

function delete_product_order($val){
	global $db;
	$sql_relation = $db->prepare_query("SELECT lumonata_order.lorder_id, lumonata_order_detail.ldetail_id,lumonata_order.lorder_shipping_id 
  								FROM lumonata_order,lumonata_order_detail 
  								where lumonata_order.lorder_id=lumonata_order_detail.lorder_id 
  								and lumonata_order.lorder_id=%s",$val);
  	$query_relation = $db->do_query($sql_relation);
  	while($data_relation=$db->fetch_array($query_relation)){
  		$delete_order_material=$db->prepare_query("DELETE FROM lumonata_order_detail_material WHERE ldetail_id=%s",$data_relation['ldetail_id']);
  		$query_del_order_material=$db->do_query($delete_order_material);
  		$shipping_id = $data_relation['lorder_shipping_id'];
  	}
	if(empty($shipping_id)){
		$shipping_id='';
	}
	
    $sql_payment=$db->prepare_query("select * from lumonata_order_payment WHERE lorder_id=%s",$val);
    $r_payment=$db->do_query($sql_payment);
    $d_payment=$db->fetch_array($r_payment);
    
    if(!empty($d_payment['lfile'])){
    	$file=ROOT_PATH.$d_payment['lfile'];
	    if(file_exists($file)){
			unlink($file);
	    }
    }
	
	
  	$delete_order_detail=$db->prepare_query("DELETE FROM lumonata_order_detail WHERE lorder_id=%s",$val);
  	$query_del_order_detail=$db->do_query($delete_order_detail);

  	$delete_order_shipping=$db->prepare_query("DELETE FROM lumonata_order_shipping WHERE lorder_shipping_id=%s",$shipping_id);
  	$query_del_order_shipping=$db->do_query($delete_order_shipping);
  	
  	$delete_order_payment=$db->prepare_query("DELETE FROM lumonata_order_payment WHERE lorder_id=%s",$val);
  	$query_del_order_payment=$db->do_query($delete_order_payment);
  	
  	$delete_order=$db->prepare_query("DELETE FROM lumonata_order WHERE lorder_id=%s",$val);
  	$query_del_order=$db->do_query($delete_order);
}
?>