<?php
function get_admin_products($type='shoppingcart&sub=products',$thetitle='Product|Product',$tabs=array()){ 		
		$post_id=0;
      
        $articletabtitle=explode("|",$thetitle);
        $tabs=array_merge(array('products'=>$articletabtitle[1],'categories'=>'Categories','tags'=>'Tags'));
        if(is_contributor() || is_author()){
            //$tabs=array_slice($tabs, 0,1);
            foreach($tabs as $key=>$val){
                if(is_grant_app($key)){
                    $thetabs[$key]=$val;
                }
            }
            $tabs=$thetabs;
        }else{
             $tabs=$tabs;
        }

        /* Configure the tabs
            $the_tab is the selected tab */
        $tab_keys=array_keys($tabs);
        $tabb='';
        if(empty($_GET['tab']))
               $the_tab=$tab_keys[0];
        else
               $the_tab=$_GET['tab'];
       
        $articles_tabs=set_tabs($tabs,$the_tab);
        add_variable('tab',$articles_tabs);
        /*
            Filter the tabs. Articles tab can be accessed by all user type.
            But for Categories and Tags tabs only granted for Editor and Administrator.
        */
        
        if($the_tab=='products'){
        	add_actions('admin_tail',get_postPriceGroup_js()); ///
        	//Publish or Save Draft Actions
	        if(is_save_draft() || is_publish()){
	            //set status and hook defined actions
	            if(is_save_draft()){
	                $status='draft';
	                run_actions('page_draft');
	            }elseif(is_publish()){
	                $status='publish';
	                run_actions('page_publish'); 
	                
	                if(isset($_POST['select'])){
	                    foreach($_POST['select'] as $key=>$val){
	                        update_articles_status($val,'publish');
	                    }
	                }
	            }
            
	            if(is_add_new()){
	            	
	                $title=$_POST['title'][0];	
	                //Hook Add New Actions
	                if(is_save_draft()){
	                    run_actions('page_addnew_draft');
	                }elseif(is_publish()){
	                    run_actions('page_addnew_publish');
	                }
	                if(isset($_POST['allow_comments'][0]))
	                    $comments="allowed";
	                else
	                    $comments="not_allowed";
                
	                if(!is_saved()){
	                	global $db;
	                	$index=0;

						save_new_product($title,$_POST['post'][0],$status,'products',$comments,$_POST['sef_box'][0],0);
						$post_id=mysql_insert_id();
						
						additional_sync($_POST['post_id'][0],$post_id);///
						attachment_sync($_POST['post_id'][0],$post_id);
						
						//	SET IMAGE THUMBNAIL
						if(isset($_POST['the_id_images'][0])){
							$image_id=$_POST['the_id_images'][0];
							add_additional_field($post_id,'product_image_thumbnail',$image_id,'products');
						}
						
	                 	//SAVE THE CATEGORY OF PRODUCT
	                	if(isset($_POST['category'][0])){
	                            foreach($_POST['category'][0] as $val){
	                            	insert_rules_relationship($post_id,$val);
	                            }
	                     }else{
	                        insert_rules_relationship($post_id,1);
	                    }
						
						//SAVE THE TAGS OF PRODUCT
	                    if(isset($_POST['tags'][0])){
	                          foreach($_POST['tags'][0] as $val){
	                         	$rule_id=insert_rules(0,$val,'','tags','products',false);
	                         	insert_rules_relationship($post_id,$rule_id);
	                        }
	                     }
	                     
	                     if(isset($_POST['in_stock'][0])){
	                     	$_POST['in_stock'][0]=$_POST['in_stock'][0];
	                     }else{
	                     	$_POST['in_stock'][0]='';
	                     }
	                     
	                	if(isset($_POST['set_as_featured'][0])){
	                     	$_POST['set_as_featured'][0]=$_POST['set_as_featured'][0];
	                    }else{
	                     	$_POST['set_as_featured'][0]='';
	                    }
	                     
	                     //	SAVE PRODUCT ADDITIONAL FIELD
						$val=array('code'=>$_POST['code'][0],'price'=>$_POST['price'][0],'weight'=>$_POST['weight'][0],'tax'=>$_POST['tax'][0],'stock_limit'=>$_POST['stock_limit'][0],'product_buy_out_of_stock'=>$_POST['in_stock'][0],'trackstok'=>$_POST['trackstock'][0],'set_as_featured'=>$_POST['set_as_featured'][0]);
						$val=json_encode($val);
						add_additional_field($post_id,'product_additional',$val,'products');
						
						
			            $sql_select_variant=$db->prepare_query("select * from lumonata_rules
							                where
							                lrule= %s and lgroup=%s and lparent=%d order by lrule_id",'variant','product','0');
						$execute_select_variant=$db->do_query($sql_select_variant);
						$x=0;
						$child_status=false;
						while($data_select_variant=$db->fetch_array($execute_select_variant)){
							$id_rule_variant=$data_select_variant['lrule_id'];
							$post_parent_variant='var_typeid_'.$index.'_'.$id_rule_variant;//id parent
							
							if(isset($_POST[$post_parent_variant])){
								$post_child_variant='var_gridoptions_'.$index.'_'.$id_rule_variant; //id child
								if(isset($_POST[$post_parent_variant])){
									$parent[$x]=$_POST[$post_parent_variant];
								}
								if(isset($_POST[$post_child_variant])){
									$child[$x]=$_POST[$post_child_variant];
									$child_status=true;
								}
								$x++;
							}
						}

						if($child_status==true){
			             	$array_all_2='';
							$var_array_parent=array();
							$parent_status=false;
							for($y=0;$y<$x;$y++){
								$split_child=explode(',', $child[$y]);
								$count_child=count($split_child);
								$array_all_1='';
								$array_json='';
								$variant_status=false;
								//$var_array_parent['parent_variant'][$y]=$parent[$y];
								for($z=0;$z<$count_child;$z++){
									$price_variant1='var_price_'.$index.'_'.$parent[$y]."_".$split_child[$z];
									if(isset($_POST[$price_variant1])){
										$price_variant2=$_POST[$price_variant1];
										$var_array_parent['child_variant'][$y][$split_child[$z]]=$price_variant2;
										$variant_status=true;
									}	
								}
								if($variant_status==true){
									$var_array_parent['parent_variant'][$y]=$parent[$y];
									$parent_status=true;
								}
							}

							if($parent_status==true){
								$val_variant_encode=json_encode($var_array_parent);
								add_additional_field($post_id,'product_variant',$val_variant_encode,'products');
							}
						}
	   
	           }else{
	           	
		           	$index=0;
		           	global  $db;
			          update_article($_POST['post_id'][$index],$_POST['title'][$index],$_POST['post'][$index],$status,'products','',$_POST['sef_box'][$index],0);
		           			
	                //	DELETE ADDITIONAL FIELD
	                ///delete_additional_field($_POST['post_id'][$index], 'products');
	                delete_additional_field_except_lkey($_POST['post_id'][$index], 'products','product_price_range'); //kecuali lkey=product_price_range
	                
	                if(isset($_POST['the_id_images'][0])){
						$image_id=$_POST['the_id_images'][0];
						add_additional_field($_POST['post_id'][$index],'product_image_thumbnail',$image_id,'products');
					}
	                
					//	UPDATE VARIANT
		            $sql_select_variant=$db->prepare_query("select * from lumonata_rules
						                where
						                lrule= %s and lgroup=%s and lparent=%d order by lrule_id",'variant','product','0');
					$execute_select_variant=$db->do_query($sql_select_variant);
					$x=0;
					$child_status=false;
					while($data_select_variant=$db->fetch_array($execute_select_variant)){
						$id_rule_variant=$data_select_variant['lrule_id'];
						$post_parent_variant='var_typeid_'.$index.'_'.$id_rule_variant;//id parent
						
						if(isset($_POST[$post_parent_variant])){
							$post_child_variant='var_gridoptions_'.$index.'_'.$id_rule_variant; //id child
							if(isset($_POST[$post_parent_variant])){
								$parent[$x]=$_POST[$post_parent_variant];
							}
							//print_r($_POST);
							if(isset($_POST[$post_child_variant])){
								$child[$x]=$_POST[$post_child_variant];
								$child_status=true;
							}
							$x++;
						}
					}
						
					if($child_status==true){
		             	$array_all_2='';
						$var_array_parent=array();
						$parent_status=false;
						for($y=0;$y<$x;$y++){
							$split_child=explode(',', $child[$y]);
							$count_child=count($split_child);
							$array_all_1='';
							$array_json='';
							$variant_status=false;
							//$var_array_parent['parent_variant'][$y]=$parent[$y];
							for($z=0;$z<$count_child;$z++){
								$price_variant1='var_price_'.$index.'_'.$parent[$y]."_".$split_child[$z];
								if(isset($_POST[$price_variant1])and !empty($_POST[$price_variant1])){
									$price_variant2=$_POST[$price_variant1];
									$var_array_parent['child_variant'][$y][$split_child[$z]]=$price_variant2;
									$variant_status=true;
								}
							}
							if($variant_status==true){
								$var_array_parent['parent_variant'][$y]=$parent[$y];
								$parent_status=true;
							}
						}
						
						if($parent_status==true){
							$val_variant_encode=json_encode($var_array_parent);
							add_additional_field($_POST['post_id'][$index],'product_variant',$val_variant_encode,'products');
						}
					}
					
		            if(isset($_POST['in_stock'][0])){
	                  $_POST['in_stock'][0]=$_POST['in_stock'][0];
	                }else{
	                  $_POST['in_stock'][0]='';
	                }
					
	           		if(isset($_POST['set_as_featured'][$index])){
                     	$_POST['set_as_featured'][$index]=$_POST['set_as_featured'][$index];
                    }else{
                     	$_POST['set_as_featured'][$index]='';
                    }
	                //	UPDATE PRODUCT ADDITIONAL FIELD
					$val=array('code'=>$_POST['code'][$index],'price'=>$_POST['price'][$index],'weight'=>$_POST['weight'][$index],'tax'=>$_POST['tax'][$index],'stock_limit'=>$_POST['stock_limit'][$index],'product_buy_out_of_stock'=>$_POST['in_stock'][$index],'trackstok'=>$_POST['trackstock'][$index],'set_as_featured'=>$_POST['set_as_featured'][$index]);
					$val=json_encode($val);
					add_additional_field($_POST['post_id'][$index],'product_additional',$val,'products');
		
						
	               //UPDATE THE CATEGORY OF PRODUCT
	                if(isset($_POST['category'][$index])){
	                	  delete_rules_relationship("app_id=".$_POST['post_id'][$index],'categories','products');
		                  foreach($_POST['category'][$index] as $val){
		                    insert_rules_relationship($_POST['post_id'][$index],$val);
		                           
		                  }
	                }else{
	                     //insert_product_relationship($post_id,1);
	                     insert_rules_relationship($_POST['post_id'][$index],1);
	                }
					
					//UPDATE THE TAGS OF PRODUCT
	                if(isset($_POST['tags'][$index])){
	                   delete_rules_relationship("app_id=".$_POST['post_id'][$index],'tags','products');
	                   foreach($_POST['tags'][$index] as $val){
	                         $rule_id=insert_rules(0,$val,'','tags','products',false);
	                         insert_rules_relationship($_POST['post_id'][$index],$rule_id);
	                   }
	                }
                     
	           }
            }elseif(is_edit()){
            	global $db;
            	$index=0;
            	
                //Hook Single Edit Actions
                if(is_save_draft()){
                    run_actions('page_edit_draft');
                }elseif(is_publish()){
                    run_actions('page_edit_publish');
                }
				
                //	Update the articles
                 if(isset($_POST['allow_comments'][0]))
	                 $comments="allowed";
	             else
	                 $comments="not_allowed";    
		                
	             //	UPDATE PRODUCT
	           	update_article($_POST['post_id'][$index],$_POST['title'][$index],$_POST['post'][$index],$status,'products','',$_POST['sef_box'][$index],0);
				
	           	
                //	DELETE ADDITIONAL FIELD 
                // delete_additional_field($_POST['post_id'][$index], 'products'); 
                // kecuali lkey=product_price_range
                delete_additional_field_except_lkey($_POST['post_id'][$index], 'products','product_price_range');
                
                if(isset($_POST['the_id_images'][0])){
					$image_id=$_POST['the_id_images'][0];
					add_additional_field($_POST['post_id'][$index],'product_image_thumbnail',$image_id,'products');
				}
                
				//	UPDATE VARIANT
	            $sql_select_variant=$db->prepare_query("select * from lumonata_rules
					                where
					                lrule= %s and lgroup=%s and lparent=%d order by lrule_id",'variant','product','0');
				$execute_select_variant=$db->do_query($sql_select_variant);
				$x=0;
				$child_status=false;
				while($data_select_variant=$db->fetch_array($execute_select_variant)){
					$id_rule_variant=$data_select_variant['lrule_id'];
					$post_parent_variant='var_typeid_'.$index.'_'.$id_rule_variant;//id parent
					
					if(isset($_POST[$post_parent_variant])){
						$post_child_variant='var_gridoptions_'.$index.'_'.$id_rule_variant; //id child
						if(isset($_POST[$post_parent_variant])){
							$parent[$x]=$_POST[$post_parent_variant];
						}
						//print_r($_POST);
						if(isset($_POST[$post_child_variant])){
							$child[$x]=$_POST[$post_child_variant];
							$child_status=true;
						}
						$x++;
					}
				}
					
				if($child_status==true){
	             	$array_all_2='';
					$var_array_parent=array();
					$parent_status=false;
					for($y=0;$y<$x;$y++){
						$split_child=explode(',', $child[$y]);
						$count_child=count($split_child);
						$array_all_1='';
						$array_json='';
						$variant_status=false;
						//$var_array_parent['parent_variant'][$y]=$parent[$y];
						for($z=0;$z<$count_child;$z++){
							$price_variant1='var_price_'.$index.'_'.$parent[$y]."_".$split_child[$z];
							if(isset($_POST[$price_variant1])and !empty($_POST[$price_variant1])){
								$price_variant2=$_POST[$price_variant1];
								$var_array_parent['child_variant'][$y][$split_child[$z]]=$price_variant2;
								$variant_status=true;
							}
							
						}
						if($variant_status==true){
							$var_array_parent['parent_variant'][$y]=$parent[$y];
							$parent_status=true;
						}
					}
					if($parent_status==true){
						$val_variant_encode=json_encode($var_array_parent);
						add_additional_field($_POST['post_id'][$index],'product_variant',$val_variant_encode,'products');
					}
				}
				
            	if(isset($_POST['set_as_featured'][$index])){
                     $_POST['set_as_featured'][$index]=$_POST['set_as_featured'][$index];
                }else{
                     $_POST['set_as_featured'][$index]='';
                }
				
                //	UPDATE PRODUCT ADDITIONAL FIELD
				$val=array('code'=>$_POST['code'][$index],'price'=>$_POST['price'][$index],'weight'=>$_POST['weight'][$index],'tax'=>$_POST['tax'][$index],'stock_limit'=>$_POST['stock_limit'][$index],'product_buy_out_of_stock'=>$_POST['in_stock'][$index],'trackstok'=>$_POST['trackstock'][$index],'set_as_featured'=>$_POST['set_as_featured'][$index]);
				$val=json_encode($val);
				add_additional_field($_POST['post_id'][$index],'product_additional',$val,'products');
	
					
               //UPDATE THE CATEGORY OF PRODUCT
                if(isset($_POST['category'][$index])){
                	  delete_rules_relationship("app_id=".$_POST['post_id'][$index],'categories','products');
	                  foreach($_POST['category'][$index] as $val){
	                    insert_rules_relationship($_POST['post_id'][$index],$val);	                           
	                  }
                }else{
                     //insert_product_relationship($post_id,1);
                     insert_rules_relationship($_POST['post_id'][$index],1);
                }
				
				//UPDATE THE TAGS OF PRODUCT
                if(isset($_POST['tags'][$index])){
                   delete_rules_relationship("app_id=".$_POST['post_id'][$index],'tags','products');
                   foreach($_POST['tags'][$index] as $val){
                         $rule_id=insert_rules(0,$val,'','tags','products',false);
                         insert_rules_relationship($_POST['post_id'][$index],$rule_id);
                   }
                }
                     
            }elseif(is_edit_all()){
            	global $db;
            	
                //Hook Edit All Actions
                if(is_save_draft()){
                    run_actions('page_editall_draft');
                }elseif(is_publish()){
                    run_actions('page_editall_publish');
                }
                //	Update the articles
                foreach($_POST['post_id'] as $index=>$value){
                	//print_r($_POST);
	                 if(isset($_POST['allow_comments'][$index]))
		                 $comments="allowed";
		             else
		                 $comments="not_allowed";    
		                
		             //	UPDATE PRODUCT
		           	update_article($_POST['post_id'][$index],$_POST['title'][$index],$_POST['post'][$index],$status,'products','',$_POST['sef_box'][$index],0);
					
	                //	DELETE ADDITIONAL FIELD
	               ///delete_additional_field($_POST['post_id'][$index], 'products');
	               delete_additional_field_except_lkey($_POST['post_id'][$index], 'products','product_price_range'); //kecuali price range
                   
	               if(isset($_POST['the_id_images'][$index])){
						$image_id=$_POST['the_id_images'][$index];
						add_additional_field($_POST['post_id'][$index],'product_image_thumbnail',$image_id,'products');
					}
	               
					//	UPDATE VARIANT
	                $sql_select_variant=$db->prepare_query("select * from lumonata_rules
						                where
						                lrule= %s and lgroup=%s and lparent=%d order by lrule_id",'variant','product','0');
					$execute_select_variant=$db->do_query($sql_select_variant);
					$x=0;
					$child_status=false;
					while($data_select_variant=$db->fetch_array($execute_select_variant)){
						$id_rule_variant=$data_select_variant['lrule_id'];
						$post_parent_variant='var_typeid_'.$index.'_'.$id_rule_variant;//id parent
						
						if(isset($_POST[$post_parent_variant])){
							$post_child_variant='var_gridoptions_'.$index.'_'.$id_rule_variant; //id child
							if(isset($_POST[$post_parent_variant])){
								$parent[$x]=$_POST[$post_parent_variant];
							}
							//print_r($_POST);
							if(isset($_POST[$post_child_variant])){
								$child[$x]=$_POST[$post_child_variant];
								$child_status=true;
							}
							$x++;
						}
					}

					if($child_status==true){
		             	$array_all_2='';
						$var_array_parent=array();
						$parent_status=false;
						for($y=0;$y<$x;$y++){
							$split_child=explode(',', $child[$y]);
							$count_child=count($split_child);
							$array_all_1='';
							$array_json='';
							$variant_status=false;
							//$var_array_parent['parent_variant'][$y]=$parent[$y];
							for($z=0;$z<$count_child;$z++){
								$price_variant1='var_price_'.$index.'_'.$parent[$y]."_".$split_child[$z];
								if(isset($_POST[$price_variant1]) and !empty($_POST[$price_variant1])){
									$price_variant2=$_POST[$price_variant1][$index];
									//print_r($price_variant2); echo " - ".$index;
									$var_array_parent['child_variant'][$y][$split_child[$z]]=$price_variant2;
									$variant_status=true;
								}
							}
							if($variant_status==true){
								$var_array_parent['parent_variant'][$y]=$parent[$y];
								$parent_status=true;
							}
						
						}
						if($parent_status==true){
							$val_variant_encode=json_encode($var_array_parent);
							add_additional_field($_POST['post_id'][$index],'product_variant',$val_variant_encode,'products');
						}
					}
					
                	if(isset($_POST['set_as_featured'][$index])){
	                     	$_POST['set_as_featured'][$index]=$_POST['set_as_featured'][$index];
	                }else{
	                     	$_POST['set_as_featured'][$index]='';
	                }
					
	                //	UPDATE PRODUCT ADDITIONAL FIELD
					$val=array('code'=>$_POST['code'][$index],'price'=>$_POST['price'][$index],'weight'=>$_POST['weight'][$index],'tax'=>$_POST['tax'][$index],'stock_limit'=>$_POST['stock_limit'][$index],'product_buy_out_of_stock'=>$_POST['in_stock'][$index],'trackstok'=>$_POST['trackstock'][$index],'set_as_featured'=>$_POST['set_as_featured'][$index]);
					$val=json_encode($val);
					add_additional_field($_POST['post_id'][$index],'product_additional',$val,'products');
	
					
	               //UPDATE THE CATEGORY OF PRODUCT
	                if(isset($_POST['category'][$index])){
	                	  delete_rules_relationship("app_id=".$_POST['post_id'][$index],'categories','products');
		                  foreach($_POST['category'][$index] as $val){
		                    insert_rules_relationship($_POST['post_id'][$index],$val);
		                           
		                  }
	                }else{
	                     //insert_product_relationship($post_id,1);
	                     insert_rules_relationship($_POST['post_id'][$index],1);
	                }
				
					//UPDATE THE TAGS OF PRODUCT
                   if(isset($_POST['tags'][$index])){
                   		delete_rules_relationship("app_id=".$_POST['post_id'][$index],'tags','products');
                        foreach($_POST['tags'][$index] as $val){
                         	$rule_id=insert_rules(0,$val,'','tags','products',false);
                         	insert_rules_relationship($_POST['post_id'][$index],$rule_id);
                        }
                     }
	                
	            }
                   
            }
        }elseif(is_unpublish()){
            if(isset($_POST['select'])){
                foreach($_POST['select'] as $key=>$val){
                    update_articles_status($val,'unpublish');
                }
            }
        }
        
        //Automatic to display add new when there is no records on database
        if(is_num_articles()==0 && !isset($_GET['prc'])){
            header("location:".get_state_url('shoppingcart&sub=products')."&prc=add_new");
        }
                
         
        //Display add new form
        if(is_add_new()){        	
            return add_new_products($post_id,$type,$thetitle) ;
        }elseif(is_edit()){ 
            if(is_contributor() || is_author()){
                if(is_num_articles("id=".$_GET['id'])>0){
                    return edit_product($_GET['id'],$type,$thetitle);
                }else{
                    return "<div class=\"alert_red_form\">You don't have an authorization to access this page</div>";
                }
            }else{

                return edit_product($_GET['id'],$type,$thetitle);
            }
        }elseif(is_edit_all() && isset($_POST['select'])){
            return edit_product(0,$type,$thetitle);
        }elseif(is_delete_all()){
                add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this comment:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these comments:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                        $d=fetch_product("id=".$val);
                        $warning.="<li>".$d['larticle_title']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$d['larticle_id']."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('shoppingcart&sub=products')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
                return $warning;
        }elseif(is_confirm_delete()){
                foreach($_POST['id'] as $key=>$val){
                        delete_article($val,'products');
                        delete_additional_field($val, 'products');
                        while($data=get_attachment_id($val)){
                        	delete_attachment($data['lattach_id']);
                        }
                }
        }
        
        //Display Users Lists
        if(is_num_articles()>0){
                add_actions('header_elements','get_javascript','jquery_ui');
                add_actions('header_elements','get_javascript','articles_list');
                return get_products_list($type,$thetitle,$articles_tabs);
        }
        }elseif($the_tab=="tags" && (is_administrator() || is_editor())){
            return get_admin_rule($the_tab,$type,$thetitle,$tabs);
        }elseif(($the_tab=="categories") && (is_administrator() || is_editor())){
        	return get_product_categories_rule($the_tab,$type,$thetitle,$tabs);
        }else{
            return "<div class=\"alert_red_form\">You don't have an authorization to access this page</div>";
        }
    }
	
	//Product List Here
	function get_products_list($type='products',$title,$articles_tabs){
        global $db;
        $list='';
        $option_viewed="";
        $data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
        
        if(isset($_POST['data_to_show']))
            $show_data=$_POST['data_to_show'];
        elseif(isset($_GET['data_to_show']))
            $show_data=$_GET['data_to_show'];
       
        
        foreach($data_to_show as $key=>$val){
            if(isset($show_data)){
                if($show_data==$key){
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
                }else{
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
                }
            }elseif($key=='all'){
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
            }else{
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
            }
        }
        
        
        
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }

        if(is_search()){
                $sql=$db->prepare_query("select * from lumonata_articles where larticle_type=%s and (larticle_title like %s or larticle_content like %s)",'products',"%".$_POST['s']."%","%".$_POST['s']."%");
                $num_rows=count_rows($sql);
                
        }else{
                if((isset($_POST['data_to_show']) && $_POST['data_to_show']!="all") || (isset($_GET['data_to_show']) && $_GET['data_to_show']!="all")){
                    //setup paging system
                    if(isset($_POST['data_to_show']))
                        $show_data=$_POST['data_to_show'];
                    else
                        $show_data=$_GET['data_to_show'];
                        
                    $url=get_state_url('shoppingcart&sub=products')."&data_to_show=$show_data&page=";
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$show_data,$type);
                }else{
                    $where=$db->prepare_query("WHERE $w larticle_type=%s",$type);
                    //setup paging system
                    $url=get_state_url('shoppingcart&sub=products')."&page=";
                }    
                $num_rows=count_rows("select * from lumonata_articles $where");
        }
        
        $viewed=list_viewed();
        if(isset($_GET['page'])){
            $page= $_GET['page'];
        }else{
            $page=1;
        }
        
		$limit=($page-1)*$viewed;
        if(is_search()){
                $sql=$db->prepare_query("select * from lumonata_articles where $w larticle_type=%s and (larticle_title like %s or larticle_content like %s) limit %d, %d",$type,"%".$_POST['s']."%","%".$_POST['s']."%",$limit,$viewed);
               
        }else{
                if(isset($_POST['data_to_show']) && $_POST['data_to_show']!="all")
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$_POST['data_to_show'],$type);
                else
                    $where=$db->prepare_query("WHERE $w larticle_type=%s",$type);
                    
                $sql=$db->prepare_query("select * from lumonata_articles $where order by lorder limit %d, %d",$limit,$viewed);
               
        }
        
        //if($viewed*$page > $num_rows && $num_rows!=0 && $page>1)
        //    header("location:".$url."1");
        
        $result=$db->do_query($sql);
        
        $start_order=($page - 1) * $viewed + 1; //start order number
        if($_COOKIE['user_type']=="contributor"){
            $button="<li>".button("button=add_new",get_state_url("shoppingcart&sub=products")."&prc=add_new")."</li>
            		<li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>";
            		//<li>".button('button=edit&type=submit&enable=false')."</li>
        }else{
            $button="<li>".button("button=add_new",get_state_url("shoppingcart&sub=products")."&prc=add_new")."</li>
            		<li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>
                    <li>".button('button=publish&type=submit&enable=false')."</li>
                    <li>".button('button=unpublish&type=submit&enable=false')."</li>";
            		//<li>".button('button=edit&type=submit&enable=false')."</li>
        }
        $list.="<h1>Product</h1>
        <ul class=\"tabs\">$articles_tabs</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('shoppingcart&sub=products')."\" method=\"post\" name=\"alist\">
                           <div class=\"button_right\">
                                ".search_box_product('http://'.SITE_URL.'/product_ajax','list_item','src_product','right','alert_green_form','Search pages')."
                           </div>
                           <br clear=\"all\" />
                           
                           <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
                           <input type=\"hidden\" name=\"state\" value=\"pages\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                          $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"status_to_show\">Show: $option_viewed</div>
                            <div class=\"list\">
                                <div class=\"list_title\">
                                    <input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" />
                                    <div class=\"pages_title\" style=\"width:160px\">Image</div>
                                    <div class=\"list_author\" style=\"width:150px\">Product</div>
                                    <div class=\"avatar\"></div>
                                    <div class=\"list_comments\" style=\"text-align:left\">Stock</div>
                                    <div class=\"list_date\">Variant</div>
                                    <div class=\"list_date\" style=\"padding-left:3px;width:120px\">Date</div>
                                    <div class=\"list_date\" style=\"padding-left:25px\">Status</div>
                                    <!--div class=\"list_date\">Order</div -->
                                </div>
                                <div id=\"list_item\">";
                                	$list.=products_list($result,$start_order);
                $list.="		</div>
                			</div>
                        
                        
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                        <div class=\"paging_right\">
                                    ". paging($url,$num_rows,$page,$viewed,5)."
                        </div>
                </div>
            </form>";
            
        add_actions('section_title','ShoppingCart - Product');
        return $list;
    }
	
	function products_list($result,$i=1){
        global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No page found</div>";
        }

 		while($d=$db->fetch_array($result)){
                if($d['larticle_status']!='publish')
                    $status=" - <strong style=\"color:red;\">".ucfirst($d['larticle_status'])."</strong>";
                else
                    $status="- <strong style=\"color:red;\">".ucfirst($d['larticle_status'])."</strong>";

                /*if($d['lshare_to']==0){
                    $share_to="<span style=\"font-size:10px;\">Everyone</span>";
                }else{
                    $share_data=get_friend_list_by_id($d['lshare_to']);
                    $share_to="<span style=\"font-size:10px;\">".$share_data['llist_name']."</span>";
                }   */
                    
                // Select Stok
                $sql_stock= $db->prepare_query("select * from lumonata_additional_fields where lapp_id=%s and lkey=%s and lapp_name=%s",$d['larticle_id'],'product_additional','products');
                $result_stok=$db->do_query($sql_stock);
                $data_stok=$db->fetch_array($result_stok);
                $obj_stok = json_decode($data_stok['lvalue'],true);
               	$the_product_stock=$obj_stok['stock_limit'];
               	if(empty($the_product_stock)){
               		$the_product_stock=0;
               	}
               
 			 //select variant array in addition fields
		      $sql_variant= $db->prepare_query("select * from lumonata_additional_fields where lapp_id=%s and lkey=%s and lapp_name=%s",$d['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);
		      //echo $data_variant['lvalue'];
		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$count_it++;
			     	 	foreach($obj_variant['child_variant'][$key] as $subkey=>$subval){
			      		//echo "child ".$subkey.":".$subval."<br>";
			              }
			       }
		      }
 		 	  if($count_it<>0){
               		$variant_count_parent=$count_it." Variant";
              }else{
               		$variant_count_parent='-';
              }
               
		      //images product
		       $sql_product_images= $db->prepare_query("select
				lattach_id,
				larticle_id ,
				lattach_loc,
				lattach_loc_thumb,
				lattach_loc_medium,
				lattach_loc_large,
				ltitle 
				 from 
				lumonata_attachment,lumonata_additional_fields 
				where 
				lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
				and lumonata_additional_fields.lapp_id=%d
				and lumonata_additional_fields.lkey='product_image_thumbnail'",$d['larticle_id']);
		       $result_product_images=$db->do_query($sql_product_images);
		       $data_product_images=$db->fetch_array($result_product_images);
		       $images_name=$data_product_images['lattach_loc_thumb'];
		       $rows_images=$db->num_rows($result_product_images);
		       if($rows_images<>0){
		       		$images_url="http://".SITE_URL.$images_name;
		       }else{
		       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
		       }
              
                
                $user_fetched=fetch_user($d['lpost_by']);
                $list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['larticle_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['larticle_id']."\"/>
                                <div class=\"pages_title\" style=\"width:153px\"><img style=\"width:80px;border: 1px solid #CCCCCC;;\" src=\"".$images_url."\" /></div>
                                <div class=\"avatar\" style=\"width:150px\">".$d['larticle_title']."</div>
                                
                                <div class=\"list_comments\" style=\"text-align:center;\">".$the_product_stock."</div>
                                <div class=\"list_date\" style=\"margin-left:26px\">".$variant_count_parent."</div>
                                <div class=\"list_date\" style=\"padding-left:10px\">".date(get_date_format(),strtotime($d['lpost_date']))."</div>
                                <div class=\"list_date\" style=\"padding-left:2px\">$status</div>
                                <!-- div class=\"list_order\" --><input type=\"hidden\" value=\"$i\" id=\"order_".$d['larticle_id']."\" class=\"small_textbox\" name=\"order[".$i."]\"><!-- /div -->
                                
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;padding-left:2px\" id=\"the_navigation_".$d['larticle_id']."\">
                                                <a href=\"".get_state_url('shoppingcart&sub=products')."&prc=edit&id=".$d['larticle_id']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['larticle_id']."\">Delete</a>
                                                
                                        </div>
                                </div></div>
                               <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['larticle_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['larticle_id']."').show();
                                        });
                                        $('#theitem_".$d['larticle_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['larticle_id']."').hide();
                                        });
                                </script>
                                
                        ";
                $msg="Are sure want to delete ".$d['larticle_title']."?";
                add_actions('admin_tail','delete_confirmation_box_product',$d['larticle_id'],$msg,"http://".SITE_URL."/product_ajax","theitem_".$d['larticle_id'],'pKEY=delete_product&id='.$d['larticle_id']);
                //delete_confirmation_box($d['larticle_id'],"Are sure want to delete ".$d['larticle_title']."?","articles.php","theitem_".$d['larticle_id'],'state=pages&prc=delete&id='.$d['larticle_id'])
                $i++;
        }
        return $list;
    }
	
	//add new product here
	function add_new_products($post_id=0,$type,$title){
		global $thepost;
        $args=array($index=0,$post_id);
        $thepost->post_id=$post_id;
        $thepost->post_index=$index;
        set_products_template();
        //print_r($_POST['title'][$index]);
       if(is_admin_application())
            $url=get_application_url($type);
        else
            $url=get_state_url($type);
        
        $thetitle=explode("|",$title);
        $thetitle=$thetitle[0];
        add_variable('app_title',"Add New ".$thetitle);
        
        $button="";
        if(!is_contributor())
            $button.="<li>".button("button=add_new",get_state_url('shoppingcart&sub=products')."&prc=add_new")."</li>
            <li>".button("button=save_draft")."</li>
            <li>".button("button=publish")."</li>
            <li>".button("button=cancel",get_state_url('shoppingcart&sub=products'))."</li>";
                        
        else
            $button.="<li>".button("button=add_new",get_state_url('shoppingcart&sub=products')."&prc=add_new")."</li>
            <li>".button("button=save_draft&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('shoppingcart&sub=products'))."</li>";
            
        //	set the page Title
        add_actions('section_title','Products - Add New');
        if(is_save_draft() || is_publish()){
            if(!is_saved()){
                $post_id=$post_id;
            }else{
               $post_id=$_POST['post_id'][0];
            }
            
         	//	Categories'
        	$selected_categories=find_selected_rules($post_id,'categories',$type);
           
           //	Categories
            add_variable('all_categories',all_categories(0,'categories',$type));
            add_variable('most_used_categories',get_most_used_categories($type));
            if(is_editor() || is_administrator()){
                add_variable('add_new_category',article_new_category(0,'categories',$type));
            }
            
            //	Tags
            add_variable('all_tags',get_post_tags($post_id,0,$type));
            add_variable('most_used_tags',get_most_used_tags($type));
            add_variable('add_new_tag',add_new_tag($post_id,0));    
                
            //	Get The Permalink
            if(is_permalink()){
                if(isset($_POST['sef_box'][0]))
                    $sef=$_POST['sef_box'][0];
                else 
                    $sef="";
                    
               	$_POST['index']=0;
                
                if(strlen($sef)>50)$more="...";else $more="";
	                $sef_scheme="<div id=\"sef_scheme_0\">";
	                $sef_scheme.="<strong>Permalink:</strong> 
	                        	  http://".site_url()."/products/
	                        	  <span id=\"the_sef_".$_POST['index']."\">
	                    			  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
	                                      substr($sef,0,50).$more.
	                                  "</span>/
	                                  <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
	                              </span>
	                              <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
	                              <span>
	                              	<input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" />/
	                              	<input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
	                              </span>
	                              </span>
	                              
	                              <script type=\"text/javascript\">
	                              		$('#the_sef_".$_POST['index']."').click(function(){
	                              			$('#the_sef_".$_POST['index']."').hide();
	                              			$('#sef_box_".$_POST['index']."').show();
	                              			
	                    				});
	                    				$('#edit_sef_".$_POST['index']."').click(function(){
	                              			$('#the_sef_".$_POST['index']."').hide();
	                              			$('#sef_box_".$_POST['index']."').show();
	                    				});
	                    				$('#done_edit_sef_".$_POST['index']."').click(function(){
	                    					                    					
	                    					var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
	                    						
	                    					if(new_sef.length>50){
	                    						var more='...'
	                    						
	                    					}else{
	                        					var more='';
	                        					
	                        				}
	                        				
	                              			$('#the_sef_".$_POST['index']."').show();
	                              			$('#sef_box_".$_POST['index']."').hide();
	                              			$.post('articles.php',
	                              			{ 'update_sef' 	: 'true',
	             							  'post_id' 	: ".$post_id.",
	             							  'type' 		: 'pages',
	             							  'title' 		: $('input[name=title[0]]').val(),
	             							  'new_sef'	 	: new_sef },
	                              			function(theResponse){
	                              				if(theResponse=='BAD'){
	                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
	                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
	                    						}else if(theResponse=='OK'){
	                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
	             								}
	             							});
	             							
	             							
	                    				});
	                              </script>"; 
                    	$sef_scheme.="</div>";
            }
            
            //add_variable('jquery_ui_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/js/jquery-ui-1.8.16.custom.min.js\" ></script>");  
     		//add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/reservation/js/jquery-ui/css/ui-darkness/jquery-ui-1.8.14.custom.css\" type=\"text/css\" media=\"screen\" />");
     		
            add_variable('group',$type);
     		$thaUrl= 'http://'.SITE_URL.'/product_ajax';
     		add_variable('urlcss', 'http://'.SITE_URL.'/');
     		add_variable('thaUrl', $thaUrl);
     		add_variable('upload_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/ajaxupload.js\"></script>");
            add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />");
            add_variable('product_code',rem_slashes($_POST['code'][0]));
            add_variable('textarea',textarea('post[0]',0,rem_slashes($_POST['post'][0]),$post_id));
            add_variable('title',rem_slashes($_POST['title'][0]));
            add_variable('is_saved',"<input type=\"hidden\" name=\"article_saved\" value=\"1\" />");
            
            $selected_categories=find_selected_rules($post_id,'categories','products');
			$args=array($_POST['index'],$post_id);
        
         	add_variable('all_categories',all_categories($_POST['index'],'categories','products',$selected_categories));
            add_variable('most_used_categories',get_most_used_categories('products',$_POST['index'],$selected_categories));
            if(is_editor() || is_administrator()){
                add_variable('add_new_category',article_new_category($_POST['index'],'categories','products'));
            }
            
            //add_variable('prices_setting',additional_data_product("Pricing and Stock Tracking","price_setting",true,$args));
	        //add_variable('variants_setting',additional_data_variant("Variant",'variant_setting',true,$args));
	       	//add_variable('add_images',add_images("Upload Images","upload_images",true,$args));
        }else{
			
        	//add_variable('jquery_ui_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/js/jquery-ui-1.8.16.custom.min.js\" ></script>");  
     		//add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/reservation/js/jquery-ui/css/ui-darkness/jquery-ui-1.8.14.custom.css\" type=\"text/css\" media=\"screen\" />");
			//add_variable('checkimages', checkimages($product_id));
     		$images_save="http://".SITE_URL."/lumonata-plugins/shopping_cart/uploads/product/thumbnail/";
			add_variable('gambar', $images_save);
     		$thaUrl= 'http://'.SITE_URL.'/product_ajax';
     		add_variable('urlcss', 'http://'.SITE_URL.'/');
     		add_variable('thaUrl', $thaUrl);
     		add_variable('upload_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/ajaxupload.js\"></script>");  
     		add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />");
        	//Categories
            add_variable('all_categories',all_categories(0,'categories',$type));
            add_variable('most_used_categories',get_most_used_categories($type));
            if(is_editor() || is_administrator()){
                add_variable('add_new_category',article_new_category(0,'categories',$type));
            }
            
            //Tags
            add_variable('all_tags',get_post_tags($post_id,0,$type));
            add_variable('most_used_tags',get_most_used_tags($type));
            add_variable('add_new_tag',add_new_tag($post_id,0));
            //Get The Permalink
        	$sef_scheme="<div id=\"sef_scheme_0\"></div>";
                $sef_scheme.="<script type=\"text/javascript\">
                					$(function(){
                						$('input[name=title[0]]').blur(function(){
                							$.post('articles.php',
                							{ 'get_sef' : 'true',
                							   'type'	: '".$type."',
                							   'index'	: 0,
                							   'title'	: $(this).val() 
            								},function(theResponse){
            									$('#sef_scheme_0').html(theResponse);
            								});
                							
            							});
            						});
                			   </script>"; 
            //}
            
            add_variable('group',$type);
            add_variable('textarea',textarea('post[0]',0,'','',false));
            add_variable('title','');
            add_variable('product_code', '');

        }
        //add_actions('admin_tail',get_postPriceGroup_js()); ///
        
        add_variable('sef_scheme', $sef_scheme);
        add_variable('i',0);
       	add_variable('prices_setting',additional_data_product("Pricing and Stock Tracking","price_setting",true,$args));
        add_variable('variants_setting',additional_data_variant("Variant",'variant_setting',true,$args));
        //add_variable('additional_data',attemp_actions('page_additional_data'));
        add_variable('add_images',add_images("Upload Images","upload_images",true,$args));
        add_variable('product_options',add_product_options("Product Options","product_options",true,$args));
        add_variable('button',$button);
        add_variable('upload_button',upload_button());
        parse_template('loopPage','lPage',false);
         
        return return_page_template();
        
    }
	
	function set_products_template(){
        //set template
       	set_template(PLUGINS_PATH."/shopping_cart/products.html",'pages');
        //set block
        add_block('loopPage','lPage','pages');
        add_block('pageAddNew','pAddNew','pages');
    }
	
	 //block price
     function additional_data_product($name,$tag,$display=true,$args=array()){
        global $thepost;
        if(function_exists($tag)){
            $details=call_user_func_array($tag,$args);
        }else{
            $details=$tag;
        }
        
        if($display==false)
            $display="style=\"display:none;\"";
        else
            $display="";
            
        $data="
        <div class=\"additional_data\">
                    <h2 id=\"".$tag."_".$thepost->post_index."\">$name</h2>
                    <div class=\"additional_content\" id=\"".$tag."_details_".$thepost->post_index."\" $display >
                       $details
                    </div>
                </div>
                <script type=\"text/javascript\">
                    $(function(){
                        $('#".$tag."_".$thepost->post_index."').click(function(){
                            $('#".$tag."_details_".$thepost->post_index."').slideToggle(100);
                            return false;
                        });
                    });
                </script>
                ";
        return $data;
        
    }
	
	//price
	function price_setting($i,$post_id,$type='products'){
		$unit = get_unit_system();
			   
	   if($unit=='imperial'){
	   	$unit_system='lb';
	   }else if($unit=='metric'){
	   	$unit_system='kg';
	   }else{
	   	$unit_system='';
	   }
	   
		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==1){
			$product_tax = 'Price including taxes';
		}else{
			$product_tax = 'Price taxes';
		}
		
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes = 'Used to calculate shipping costs';
		}else{
			$shipped_taxes = 'Used to calculate shipping costs';
		}
	   
		if(is_save_draft() || is_publish()){
			if(is_edit() || is_edit_all()){
			 	$d=fetch_additional_product("id=".$post_id."&type=".$type);
			 	$obj = json_decode($d['lvalue'],true);
			 	if(!empty($obj['price'])){
			 		$the_price=$obj['price'];
			 	}else{
			 		$the_price=0;
			 	}
			 	
			 	if(!empty($obj['weight'])){
			 		$the_weight=$obj['weight'];
			 	}else{
			 		$the_weight=0;
			 	}
			 	
			 	if(!empty($obj['tax'])){
			 		$the_tax=$obj['tax'];
			 	}else{
			 		$the_tax='';
			 	}
				if(!empty($obj['stock_limit'])){
			 		$limit_stock=$obj['stock_limit'];
			 	}else{
			 		$limit_stock='0';
			 	}
			 	
			 	if(!empty($obj['trackstok']) and ($obj['trackstok']=="1")){
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_selected='selected="selected"';
			 		$trackstok_default='';
			 		$the_remove_attribute="<script> var theDiv = $(\".is1\");theDiv.slideDown().removeClass(\"hidden\"); </script>";
			 	}else{
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_default='selected="selected"';
			 		$trackstok_selected='';
			 		$the_remove_attribute='';
			 	}
			 	
			 	if(!empty($obj['product_buy_out_of_stock']) && $obj['product_buy_out_of_stock']=='1' ){
			 		$product_buy_out_of_stock_1='checked="checked"';
			 		$product_buy_out_of_stock_0='';
			 	}else{
			 		$product_buy_out_of_stock_0='checked="checked"';
			 		$product_buy_out_of_stock_1='';
			 	}
			 	
			 	$result="
		    	 <fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Pricing & weight</font></legend>
		    	 <div style=\"display: inline;float: left;clear: left; margin: 0;padding:0;\">
		    	 				<div style=\"display: inline; float:left;margin: 1px 0 0;padding: 0 0;text-align: left;width:220px\">
		    	 					<p><b>Selling price</b></p>
		    	 					<input type=\"text\" name=\"price[".$i."]\" style=\"width:60px;\" value=\"".$the_price."\"> <span style=\"font-size:10px\">".get_symbol_currency()." (".$product_tax.")</span>
		    	 				</div>
		    	 				<div style=\"display: inline; float:left;margin: 1px 0 0;padding: 0 0;text-align: left;\">
		    	 					<p><b>Product weight</b></p>
		    	 					<input type=\"text\" name=\"weight[".$i."]\" id=\"weight\" style=\"width:60px;\" value=\"".$the_weight."\"> <span style=\"font-size:10px\">".$unit_system." (".$shipped_taxes.")</span>
		    	 				</div>
		    	 		</div>
		    	 		<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
		    				<p><b>Select tax group</b></p>
		    	 			".default_tax_groups($i,$the_tax)."
		    	 </fieldset>
		    	 <p>
		    	 	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Stock level tracking</font></legend>
		    	 	<p><b>Track stock?</b></p>
			    	 	<select name=\"trackstock[".$i."]\" id='trackstock_".$i."'>
			    	 		<option ".$trackstok_default." value=\"0\">No, don't track stock level</option>
			    	 		<option ".$trackstok_selected." value=\"1\">Yes, track stock level</option>
			    	 	</select>
			    	    <div class=\"hidden is1\"><b>How many are in stock right now?</b><p>
			    	    		<input type=\"text\" name=\"stock_limit[".$i."]\" style=\"width:60px;\" id=\"stock_limit\" value=\"".$the_stock_limit."\"></p>
			    	    	<p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"1\" ".$product_buy_out_of_stock_1.">Users can buy this item when it's out of stock
			    	    	</p><p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"0\" ".$product_buy_out_of_stock_0.">Users cannot buy this item when it's out of stock
			    	    	</p>
			    	    </div>
		    			<div class=\"hidden is0\"></div>
		    	 	</fieldset>
		    	 </p>
				".$the_remove_attribute."";
			 }else{
			 	$d=fetch_additional_product("id=".$post_id."&type=".$type);
			 	$obj = json_decode($d['lvalue'],true);
			 	if(!empty($obj['price'])){
			 		$the_price=$obj['price'];
			 	}else{
			 		$the_price=0;
			 	}
			 	
			 	if(!empty($obj['weight'])){
			 		$the_weight=$obj['weight'];
			 	}else{
			 		$the_weight=0;
			 	}
			 	
			 	if(!empty($obj['tax'])){
			 		$the_tax=$obj['tax'];
			 	}else{
			 		$the_tax='';
			 	}
			 	
			 	if(!empty($obj['stock_limit'])){
			 		$limit_stock=$obj['stock_limit'];
			 	}else{
			 		$limit_stock='0';
			 	}
			 	
			 	if(!empty($obj['trackstok']) and ($obj['trackstok']=="1")){
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_selected='selected="selected"';
			 		$trackstok_default='';
			 		$the_remove_attribute="<script> var theDiv = $(\".is1\");theDiv.slideDown().removeClass(\"hidden\"); </script>";
			 	}else{
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_default='selected="selected"';
			 		$trackstok_selected='';
			 		$the_remove_attribute='';
			 	}

			 	if(!empty($obj['product_buy_out_of_stock']) && $obj['product_buy_out_of_stock']=='1' ){
			 		$product_buy_out_of_stock_1='checked="checked"';
			 		$product_buy_out_of_stock_0='';
			 	}else{
			 		$product_buy_out_of_stock_0='checked="checked"';
			 		$product_buy_out_of_stock_1='';
			 	}			 	
			 	$result="
		    	 <fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Pricing & weight</font></legend>
		    	 <div style=\"display: inline;float: left;clear: left; margin: 0;padding:0;\">
		    	 				<div style=\"display: inline; float:left;margin: 1px 0 0;padding: 0 0;text-align: left;width:220px\">
		    	 					<p><b>Selling price</b></p>
		    	 					<input type=\"text\" name=\"price[".$i."]\" style=\"width:60px;\" value=\"".$the_price."\"> <span style=\"font-size:10px\">".get_symbol_currency()." (".$product_tax.")</span>
		    	 				</div>
		    	 				<div style=\"display: inline; float:left;margin: 1px 0 0;padding: 0 0;text-align: left;\">
		    	 					<p><b>Product weight</b></p>
		    	 					<input type=\"text\" name=\"weight[".$i."]\" id=\"weight\" style=\"width:60px;\" value=\"".$the_weight."\"> <span style=\"font-size:10px\">".$unit_system." (".$shipped_taxes.")</span>
		    	 				</div>
		    	 		</div>
		    	 		<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
		    				<p><b>Select tax group</b></p>
		    	 			".default_tax_groups($i,$the_tax)."
		    	 </fieldset>
		    	 <p>
		    	 	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Stock level tracking</font></legend>
		    	 	<p><b>Track stock?</b></p>
			    	 	<select name=\"trackstock[".$i."]\" id='trackstock_".$i."'>
			    	 		<option ".$trackstok_default." value=\"0\">No, don't track stock level</option>
			    	 		<option ".$trackstok_selected." value=\"1\">Yes, track stock level</option>
			    	 	</select>
			    	    <div class=\"hidden is1\"><b>How many are in stock right now?</b><p>
			    	    		<input type=\"text\" name=\"stock_limit[".$i."]\" style=\"width:60px;\" id=\"stock_limit\" value=\"".$the_stock_limit."\"></p>
			    	    	<p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"1\" ".$product_buy_out_of_stock_1.">Users can buy this item when it's out of stock
			    	    	</p><p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"0\" ".$product_buy_out_of_stock_0.">Users cannot buy this item when it's out of stock
			    	    	</p>
			    	    </div>
		    			<div class=\"hidden is0\"></div>
		    	 	</fieldset>
		    	 </p>
				".$the_remove_attribute."";
			 }
		}else{
			if(is_edit_all() || is_edit()){
			 	$d=fetch_additional_product("id=".$post_id."&type=".$type);
			 	$obj = json_decode($d['lvalue'],true);
			 	if(!empty($obj['price'])){
			 		$the_price=$obj['price'];
			 	}else{
			 		$the_price=0;
			 	}
			 	
			 	if(!empty($obj['weight'])){
			 		$the_weight=$obj['weight'];
			 	}else{
			 		$the_weight=0;
			 	}
			 	
			 	if(!empty($obj['tax'])){
			 		$the_tax=$obj['tax'];
			 	}else{
			 		$the_tax='';
			 	}
			 	
				if(!empty($obj['stock_limit'])){
			 		$limit_stock=$obj['stock_limit'];
			 	}else{
			 		$limit_stock='0';
			 	}
			 	
			 	
			 	if(!empty($obj['trackstok']) and ($obj['trackstok']=="1")){
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_selected='selected="selected"';
			 		$trackstok_default='';
			 		$the_remove_attribute="<script> var theDiv = $(\".is1\");theDiv.slideDown().removeClass(\"hidden\"); </script>";
			 	}else{
			 		$the_stock_limit=$limit_stock;
			 		$trackstok_default='selected="selected"';
			 		$trackstok_selected='';
			 		$the_remove_attribute='';
			 	}
			 	
			 	if(!empty($obj['product_buy_out_of_stock']) && $obj['product_buy_out_of_stock']=='1' ){
			 		$product_buy_out_of_stock_1='checked="checked"';
			 		$product_buy_out_of_stock_0='';
			 	}else{
			 		$product_buy_out_of_stock_0='checked="checked"';
			 		$product_buy_out_of_stock_1='';
			 	}
			 	
			 	$result="
		    	 <fieldset style=\"padding: 1em;border-width: 1px 1px 1px;margin-left:0;\"><legend><span style=\"font-size:10px\">Pricing & Group pricing</font></legend>
			    	 <div style=\"display: block;clear: left; margin: 0;padding:0;\">
			    	 
			    	 		<div style=\"display: block; margin: 1px 0 0;padding: 0 0;text-align: left;width:220px\">
	    	 					<p><b>Selling price</b></p>
	    	 					<input type=\"text\" name=\"price[".$i."]\" style=\"width:60px;\" value=\"".$the_price."\"> <span style=\"font-size:10px\">".get_symbol_currency()." (".$product_tax.")</span>
	    	 				</div>
	    	 				
	    	 				<br>
    	 					<fieldset class=\"div_price_group\" style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\"> Group price by range </span></legend>
	    	 					<span style='width:95px;display:inline-block;'>Start</span> End <span style='width:100px;margin-left:65px;display:inline-block;'>Price</span> <br>
	    	 					<input type=\"text\" id='txt_item_start".$i."' style='width:40px;margin-right:20px;'>
	    	 					to
	    	 					<input type=\"text\" id='txt_item_end".$i."' style='width:40px;margin-left:20px;'> 
	    	 					
	    	 					<input type=\"text\" id='txt_price".$i."' style='width:40px;margin-left:45px;'> 
	    	 					
	    	 					<input style='padding:0 4px !important;height:21px;margin-left:30px;' type=\"button\" value=\"Add\" id=\"".$i."\" class=\"button add_price_range_btn\" name=\"add_price_range_btn[".$i."]\">		    	 					
    	 						<input type='hidden' id='tmp_product_id".$i."' value='".$post_id."'>
	    	 					<br>
    	 						<style type='text/css'>
    	 							.tb_price_group{
    	 								margin-top:5px;
    	 								border:1px solid #ccc;
    	 							}
    	 							.tb_price_group tr:hover{
    	 								background : #f4f4f4;
    	 							}
    	 							.tb_price_group td{
    	 								text-align:center;
 									}
    	 						</style>
	    	 					<table id='tb_group_price".$i."' class='tb_price_group' style='width:300px;'>
	    	 						<tr><th style='width:50%;'>Range (items)</th> <th>Price ($)</th><th>Delete</th></tr>
	    	 						".get_price_group($post_id)."
	    	 					</table>
	    	 				</fieldset>
			    	</div>
		    	</fieldset>
		    	
		    	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Weight & Tax</font></legend>
		    			<div style=\"display: block;clear: left; margin: 0;padding:0;\"> 
		    	
		    	 				<div style=\"display: inline; float:left;margin: 1px 0 0;padding: 0 0;text-align: left;\">
		    	 					<p><b>Product weight</b></p>
		    	 					<input type=\"text\" name=\"weight[".$i."]\" id=\"weight\" style=\"width:60px;\" value=\"".$the_weight."\"> <span style=\"font-size:10px\">".$unit_system." (".$shipped_taxes.")</span>
		    	 				</div>
		    	 		</div>
		    	 		<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
		    				<p><b>Select tax group</b></p>
		    	 			".default_tax_groups($i,$the_tax)."
		    	 </fieldset>
		    	 <p>
		    	 	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Stock level tracking</font></legend>
		    	 	<p><b>Track stock?</b></p>
			    	 	<select name=\"trackstock[".$i."]\" id='trackstock_".$i."'>
			    	 		<option ".$trackstok_default." value=\"0\">No, don't track stock level</option>
			    	 		<option ".$trackstok_selected." value=\"1\">Yes, track stock level</option>
			    	 	</select>
			    	    <div class=\"hidden is1\"><b>How many are in stock right now?</b><p>
			    	    		<input type=\"text\" name=\"stock_limit[".$i."]\" style=\"width:60px;\" id=\"stock_limit\" value=\"".$the_stock_limit."\"></p>
			    	    	<p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"1\" ".$product_buy_out_of_stock_1.">Users can buy this item when it's out of stock
			    	    	</p><p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"0\" ".$product_buy_out_of_stock_0.">Users cannot buy this item when it's out of stock
			    	    	</p>
			    	    </div>
		    			<div class=\"hidden is0\"></div>
		    	 	</fieldset>
		    	 </p>
				".$the_remove_attribute."";
			 }else{
			 	 $post_id=set_auto_id('lumonata_articles','larticle_id','');
		    	 $result="
		    	 <fieldset style=\"padding: 1em;border-width: 1px 1px 1px;margin-left:0;\"><legend><span style=\"font-size:10px\">Pricing & Group pricing</font></legend>
			    	 <div style=\"display: block;clear: left; margin: 0;padding:0;\">
			    	 
			    	 				<div style=\"display: block; margin: 1px 0 0;padding: 0 0;text-align: left;width:220px\">
			    	 					<p><b>Selling price</b></p>
			    	 					<input type=\"text\" name=\"price[".$i."]\" style=\"width:60px;\" value=\"0.00\"> <span style=\"font-size:10px\">".get_symbol_currency()." (".$product_tax.")</span>
			    	 				</div>
			    	 				<br>
			    	 					<fieldset class=\"div_price_group\" style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\"> Group price by range </span></legend>
				    	 					<span style='width:95px;display:inline-block;'>Start</span> End <span style='width:100px;margin-left:65px;display:inline-block;'>Price</span> <br>
				    	 					<input type=\"text\" id='txt_item_start0' style='width:40px;margin-right:20px;'>
				    	 					to
				    	 					<input type=\"text\" id='txt_item_end0' style='width:40px;margin-left:20px;'> 
				    	 					
				    	 					<input type=\"text\" id='txt_price0' style='width:40px;margin-left:45px;'> 
				    	 					
				    	 					<input style='padding:0 4px !important;height:21px;margin-left:30px;' type=\"button\" value=\"Add\" id=\"0\" class=\"button add_price_range_btn\" name=\"add_price_range_btn[0]\">		    	 					
			    	 						<input type='hidden' id='tmp_product_id0' value='".$post_id."'>
				    	 					<br>
			    	 						<style type='text/css'>
			    	 							.tb_price_group{
			    	 								margin-top:5px;
			    	 								border:1px solid #ccc;
			    	 							}
			    	 							.tb_price_group tr:hover{
			    	 								background : #f4f4f4;
			    	 							}
			    	 							.tb_price_group td{
			    	 								text-align:center;
			 									}
			    	 						</style>
				    	 					<table id='tb_group_price0' class='tb_price_group' style='width:300px;'>
				    	 						<tr><th style='width:50%;'>Range (items)</th> <th>Price ($)</th><th>Delete</th></tr>
				    	 						
				    	 					</table>
				    	 				</fieldset>
			    	</div>
		    	</fieldset>
		    	<br>
		    	
		    	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Weight & Tax</font></legend>
		    			<div style=\"display: block;clear: left; margin: 0;padding:0;\"> 				
		    	 				<div style=\"display: block; margin: 1px 0 0;padding: 0 0;text-align: left;\">
		    	 					<p><b>Product weight</b></p>
		    	 					<input type=\"text\" name=\"weight[".$i."]\" id=\"weight\" style=\"width:60px;\" value=\"0\"> <span style=\"font-size:10px\">".$unit_system." (".$shipped_taxes.")</span>
		    	 				</div>
		    	 		</div>
		    	 		<p>&nbsp;</p>
		    				<p><b>Select tax group</b></p>
		    	 			".default_tax_groups($i)."
		    	 </fieldset>
		    	 
		    	 <p>
		    	 	<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">Stock level tracking</font></legend>
		    	 	<p><b>Track stock?</b></p>
			    	 	<select name=\"trackstock[".$i."]\" id='trackstock_".$i."'>
			    	 		<option value=\"0\">No, don't track stock level</option>
			    	 		<option value=\"1\">Yes, track stock level</option>
			    	 	</select>
			    	    <div class=\"hidden is1\"><b>How many are in stock right now?</b><p>
			    	    		<input type=\"text\" name=\"stock_limit[".$i."]\" style=\"width:60px;\" id=\"stock_limit\" value=\"0\"></p>
			    	    	<p>
			    	    		<input type=\"radio\" checked=\"checked\" name=\"in_stock[".$i."]\" value=\"1\" >Users can buy this item when it's out of stock
			    	    	</p><p>
			    	    		<input type=\"radio\" name=\"in_stock[".$i."]\" value=\"0\" >Users cannot buy this item when it's out of stock
			    	    	</p>
			    	    </div>
		    			<div class=\"hidden is0\"></div>
		    	 	</fieldset>
		    	 </p>
				";
			 }
		}
    	 return $result;    
    }
    
    function default_tax_groups($index,$id_tax=''){
    	global $db;
    	$result='';
    	if(is_save_draft() || is_publish()){
			if(is_edit() || is_edit_all()){
					$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
			 		$query=$db->do_query($sql);
			 		while($data=$db->fetch_array($query)){
				 		if($data['ltax_groups_ID']==$id_tax){
				 			$select='checked="checked"';
				 		}else{
				 			$select='';
				 		}
			 			$result.='<p><input type="radio" name="tax['.$index.']" value="'.$data['ltax_groups_ID'].'" '.$select.'>'.$data['ldescription'].'</p>';
			 		}
			}else{
			 		$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
			 		$query=$db->do_query($sql);
			 		while($data=$db->fetch_array($query)){
				 		if($data['ldefault']==1){
				 			$select='checked="checked"';
				 		}else{
				 			$select='';
				 		}
			 			$result.='<p><input type="radio" name="tax['.$index.']" value="'.$data['ltax_groups_ID'].'" '.$select.'>'.$data['ldescription'].'</p>';
			 		}
			}
    	}else{
    		if(is_edit() || is_edit_all()){
    				$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
			 		$query=$db->do_query($sql);
			 		while($data=$db->fetch_array($query)){
				 		if($data['ltax_groups_ID']==$id_tax){
				 			$select='checked="checked"';
				 		}else{
				 			$select='';
				 		}
			 			$result.='<p><input type="radio" name="tax['.$index.']" value="'.$data['ltax_groups_ID'].'" '.$select.'>'.$data['ldescription'].'</p>';
			 		}
			}else{
    				$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
			 		$query=$db->do_query($sql);
			 		while($data=$db->fetch_array($query)){
				 		if($data['ldefault']==1){
				 			$select='checked="checked"';
				 		}else{
				 			$select='';
				 		}
			 			$result.='<p><input type="radio" name="tax['.$index.']" value="'.$data['ltax_groups_ID'].'" '.$select.'>'.$data['ldescription'].'</p>';
			 		}
			}
    	}
 		return $result;
    }
	
	//block Variant
     function additional_data_variant($name,$tag,$display=true,$args=array()){
        global $thepost;
        if(function_exists($tag)){
            $details=call_user_func_array($tag,$args);
        }else{
            $details=$tag;
        }
        
        if($display==false)
            $display="style=\"display:none;\"";
        else
            $display="";
            
        $data="
        <div class=\"additional_data\">
                    <h2 id=\"".$tag."_".$thepost->post_index."\">$name</h2>
                    <div class=\"additional_content\" id=\"".$tag."_details_".$thepost->post_index."\" $display >
                      	<div id=\"block_variant\">
                       		$details
                   		</div>
                    </div>
                </div>
                <script type=\"text/javascript\">
                    $(function(){
                        $('#".$tag."_".$thepost->post_index."').click(function(){
                            $('#".$tag."_details_".$thepost->post_index."').slideToggle(100);
                            return false;
                        });
                    });
                </script>
                ";
        return $data;
        
    }
	
	function variant_setting($index=0,$post_id,$type='products'){
		$rule_name='variant';
		$group='product';
		if(is_save_draft() || is_publish()){
			if(is_edit() || is_edit_all()){
				$d=fetch_variant("id=".$post_id."&type=".$type);
			 	$number_selected='';
				$obj_variant = json_decode( $d['lvalue'],true);
		      	$count_it=0;
		      	$variant_in_db='';
			    if(is_array($obj_variant)){
				   foreach($obj_variant['parent_variant'] as $key=>$val){
				     $count_it++;
				     $count_child_it=0;
				    	//echo "Parent ".$key.":".$val."<br>";
				     /*$disable_class="<script>
				     				jQuery('#the_variant_table tr').each(function(){
										jQuery(this).find('#variantDetailsBox_".$val."').addclass('variantDetailsBox');
									});
				     				</script>";*/
				     $variant_in_db=variant_added_to_table($index,$val,$post_id,$obj_variant['child_variant'][$key]).$variant_in_db;
				     //print_r($obj_variant['child_variant'][$key]);
				     //echo"---";
				     foreach($obj_variant['child_variant'][$key] as $subkey=>$subval){
				      	//echo "child ".$subkey.":".$subval."<br>";
				      	$count_child_it++;
				      	
				     }
				     $number_selected="<script>$('#number_of_options_selected_".$index."_".$val."').text('(".$count_child_it." selected)')</script>";
				   }
			    }

			    if($count_it<0){
			    	$unique_selected='';
			    	$unique_default='selected="selected"';
			    	$default_content="";
			    	$the_remove_attribute="<script> $(\"#table_variants_delete_row_".$index."\").attr(\"style\",\"\");</script>";
			    }else{
			    	$unique_selected='selected="selected"';
			    	$unique_default='';
			    	$the_remove_attribute='<script>var theDiv = $(".isnot_unique");theDiv.slideDown().removeClass("hidden"); $("#table_variants_delete_row_'.$index.'").css("display","none");</script>';
			    	$default_content="";	    	
			    }
				
				$result=add_new_variant($index)."<p><select name=\"unique[".$index."]\" id='unique_".$index."'>
			    	 		<option value=\"unique\"".$unique_default.">This product is unique and doesn't come in different variants</option>
			    	 		<option value=\"not_unique\"".$unique_selected.">This product comes in different variants</option>
			    	 	</select>
			    	    
			    	    	<p><div class=\"hidden isnot_unique\">
			    	    		<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">maintain variant</font></legend>
			    	    			    <div style=\"margin:0 0 2px 0;\" id=\"select_parent_$index\" class=\"select_parent\">
					                      ".selectbox_variant($index)."
					                    </div>
			    	 				<p><input type=\"button\" id=\"add_link_variant_".$index."\" class=\"button\" name=\"linkvariant\" value=\"Add Link Variant\"></p>
			    	 				".edit_variant($index).delete_variant($index)."
			    	    		</fiedset>
			    	    		<table cellpading=1 cellspacing=1 style=\"background-color:#cccccc\" id=\"the_variant_table_".$index."\">
			    	    	 	<tr id=\"table_variants_delete_row_1_".$index."\" class=\"table_variants_delete_row_1\">	
			    	    	 		<th style=\"width:200px;border-top:0px;border-bottom:0px\">Variant</th>
			    	    	 		<th style=\"width:450px;border-top:0px;border-bottom:0px\">Options (click to enable/disable)</th>
			    	    	 		<th style=\"width:50px;border-top:0px;border-bottom:0px\"></th>
			    	    	 	</tr>
			    	    	 	<tr id=\"table_variants_delete_row_".$index."\" class=\"table_variants_delete_row\">
			    	    	 		<td colspan=\"3\" style=\"background-color:#ffffff;padding: 4px;\">
			    	    	 		You haven't added any variants to this product yet...
			    	    	 		</td>
			    	    	 	</tr>".$default_content.$variant_in_db.$number_selected."
			    	    	 </table>
			    	    		</div>
			    	    	</p>
			    	    	<div class=\"hidden isunique\"></div></p>
			    	    	 ".$the_remove_attribute;
			}else{ 
				$d=fetch_variant("id=".$post_id."&type=".$type);
			 	$number_selected='';
				$obj_variant = json_decode( $d['lvalue'],true);
		      	$count_it=0;
		      	$variant_in_db='';
			    if(is_array($obj_variant)){
				   foreach($obj_variant['parent_variant'] as $key=>$val){
				     $count_it++;
				     $count_child_it=0;
				    	//echo "Parent ".$key.":".$val."<br>";
				     /*$disable_class="<script>
				     				jQuery('#the_variant_table tr').each(function(){
										jQuery(this).find('#variantDetailsBox_".$val."').addclass('variantDetailsBox');
									});
				     				</script>";*/
				     $variant_in_db=variant_added_to_table($index,$val,$post_id,$obj_variant['child_variant'][$key]).$variant_in_db;
				     //print_r($obj_variant['child_variant'][$key]);
				     //echo"---";
				     foreach($obj_variant['child_variant'][$key] as $subkey=>$subval){
				      	//echo "child ".$subkey.":".$subval."<br>";
				      	$count_child_it++;
				      	
				     }
				     $number_selected="<script>$('#number_of_options_selected_".$index."_".$val."').text('(".$count_child_it." selected)')</script>";
				   }
			    }

			    if($count_it<0){
			    	$unique_selected='';
			    	$unique_default='selected="selected"';
			    	$default_content="";
			    	$the_remove_attribute="<script> $(\"#table_variants_delete_row_".$index."\").attr(\"style\",\"\");</script>";
			    }else{
			    	$unique_selected='selected="selected"';
			    	$unique_default='';
			    	$the_remove_attribute='<script>var theDiv = $(".isnot_unique");theDiv.slideDown().removeClass("hidden"); $("#table_variants_delete_row_'.$index.'").css("display","none");</script>';
			    	$default_content="";	    	
			    }
				
				$result=add_new_variant($index)."<p><select name=\"unique[".$index."]\" id='unique_".$index."'>
			    	 		<option value=\"unique\"".$unique_default.">This product is unique and doesn't come in different variants</option>
			    	 		<option value=\"not_unique\"".$unique_selected.">This product comes in different variants</option>
			    	 	</select>
			    	    
			    	    	<p><div class=\"hidden isnot_unique\">
			    	    		<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">maintain variant</font></legend>
			    	    			    <div style=\"margin:0 0 2px 0;\" id=\"select_parent_$index\" class=\"select_parent\">
					                      ".selectbox_variant($index)."
					                    </div>
			    	 				<p><input type=\"button\" id=\"add_link_variant_".$index."\" class=\"button\" name=\"linkvariant\" value=\"Add Link Variant\"></p>
			    	 				".edit_variant($index).delete_variant($index)."
			    	    		</fiedset>
			    	    		<table cellpading=1 cellspacing=1 style=\"background-color:#cccccc\" id=\"the_variant_table_".$index."\">
			    	    	 	<tr id=\"table_variants_delete_row_1_".$index."\" class=\"table_variants_delete_row_1\">	
			    	    	 		<th style=\"width:200px;border-top:0px;border-bottom:0px\">Variant</th>
			    	    	 		<th style=\"width:450px;border-top:0px;border-bottom:0px\">Options (click to enable/disable)</th>
			    	    	 		<th style=\"width:50px;border-top:0px;border-bottom:0px\"></th>
			    	    	 	</tr>
			    	    	 	<tr id=\"table_variants_delete_row_".$index."\" class=\"table_variants_delete_row\">
			    	    	 		<td colspan=\"3\" style=\"background-color:#ffffff;padding: 4px;\">
			    	    	 		You haven't added any variants to this product yet...
			    	    	 		</td>
			    	    	 	</tr>".$default_content.$variant_in_db.$number_selected."
			    	    	 </table>
			    	    		</div>
			    	    	</p>
			    	    	<div class=\"hidden isunique\"></div></p>
			    	    	 ".$the_remove_attribute;
			}
		}else{
			if(is_edit_all() || is_edit()){
				$d=fetch_variant("id=".$post_id."&type=".$type);
			 	$number_selected='';
				$obj_variant = json_decode( $d['lvalue'],true);
		      	$count_it=0;
		      	$variant_in_db='';
			    if(is_array($obj_variant)){
				   foreach($obj_variant['parent_variant'] as $key=>$val){
				     $count_it++;
				     $count_child_it=0;
				     //echo "Parent ".$key.":".$val."<br>";
				     /*$disable_class="<script>
				     				jQuery('#the_variant_table tr').each(function(){
										jQuery(this).find('#variantDetailsBox_".$val."').addclass('variantDetailsBox');
									});
				     				</script>";*/
				     $variant_in_db=variant_added_to_table($index,$val,$post_id,$obj_variant['child_variant'][$key]).$variant_in_db;
				     //print_r($obj_variant['child_variant'][$key]);
				     //echo"---";
				     foreach($obj_variant['child_variant'][$key] as $subkey=>$subval){
				      	//echo "child ".$subkey.":".$subval."<br>";
				      	$count_child_it++;
				      	
				     }
				     $number_selected="<script>$('#number_of_options_selected_".$index."_".$val."').text('(".$count_child_it." selected)')</script>";
				   }
			    }

			    if($count_it<=0){
			    	$unique_selected='';
			    	$unique_default='selected="selected"';
			    	$default_content="";
			    	$the_remove_attribute="<script> $(\"#table_variants_delete_row_".$index."\").attr(\"style\",\"\");</script>";
			    }else{
			    	$unique_selected='selected="selected"';
			    	$unique_default='';
			    	$the_remove_attribute='<script>var theDiv = $(".isnot_unique");theDiv.slideDown().removeClass("hidden"); $("#table_variants_delete_row_'.$index.'").css("display","none");</script>';
			    	$default_content="";	    	
			    }
				
				$result=add_new_variant($index)."<p><select name=\"unique[".$index."]\" id='unique_".$index."'>
			    	 		<option value=\"unique\"".$unique_default.">This product is unique and doesn't come in different variants</option>
			    	 		<option value=\"not_unique\"".$unique_selected.">This product comes in different variants</option>
			    	 	</select>
			    	    
			    	    	<p><div class=\"hidden isnot_unique\">
			    	    		<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">maintain variant</font></legend>
			    	    			    <div style=\"margin:0 0 2px 0;\" id=\"select_parent_$index\" class=\"select_parent\">
					                      ".selectbox_variant($index)."
					                    </div>
			    	 				<p><input type=\"button\" id=\"add_link_variant_".$index."\" class=\"button\" name=\"linkvariant\" value=\"Add Link Variant\"></p>
			    	 				".edit_variant($index).delete_variant($index)."
			    	    		</fiedset>
			    	    		<table cellpading=1 cellspacing=1 style=\"background-color:#cccccc\" id=\"the_variant_table_".$index."\">
			    	    	 	<tr id=\"table_variants_delete_row_1_".$index."\" class=\"table_variants_delete_row_1\">	
			    	    	 		<th style=\"width:200px;border-top:0px;border-bottom:0px\">Variant</th>
			    	    	 		<th style=\"width:450px;border-top:0px;border-bottom:0px\">Options (click to enable/disable)</th>
			    	    	 		<th style=\"width:50px;border-top:0px;border-bottom:0px\"></th>
			    	    	 	</tr>
			    	    	 	<tr id=\"table_variants_delete_row_".$index."\" class=\"table_variants_delete_row\">
			    	    	 		<td colspan=\"3\" style=\"background-color:#ffffff;padding: 4px;\">
			    	    	 		You haven't added any variants to this product yet...
			    	    	 		</td>
			    	    	 	</tr>".$default_content.$variant_in_db.$number_selected."
			    	    	 </table>
			    	    		</div>
			    	    	</p>
			    	    	<div class=\"hidden isunique\"></div></p>
			    	    	 ".$the_remove_attribute;
			}else{
		   		$result=add_new_variant($index)."<p><select name=\"unique[".$index."]\" id='unique_".$index."'>
			    	 		<option value=\"unique\">This product is unique and doesn't come in different variants</option>
			    	 		<option value=\"not_unique\">This product comes in different variants</option>
			    	 	</select>
			    	    
			    	    	<p><div class=\"hidden isnot_unique\">
			    	    		<fieldset style=\"padding: 1em;border-width: 1px 1px 1px;\"><legend><span style=\"font-size:10px\">maintain variant</font></legend>
			    	    			    <div style=\"margin:0 0 2px 0;\" id=\"select_parent_$index\" class=\"select_parent\">
					                      ".selectbox_variant($index)."
					                    </div>
			    	 				<p><input type=\"button\" id=\"add_link_variant_".$index."\" class=\"button\" name=\"linkvariant\" value=\"Add Link Variant\"></p>
			    	 				".edit_variant($index).delete_variant($index)."
			    	    		</fiedset>
			    	    		<table cellpading=1 cellspacing=1 style=\"background-color:#cccccc\" id=\"the_variant_table_".$index."\">
			    	    	 	<tr id=\"table_variants_delete_row_1_".$index."\" class=\"table_variants_delete_row_1\">	
			    	    	 		<th style=\"width:200px;border-top:0px;border-bottom:0px\">Variant</th>
			    	    	 		<th style=\"width:450px;border-top:0px;border-bottom:0px\">Options (click to enable/disable)</th>
			    	    	 		<th style=\"width:50px;border-top:0px;border-bottom:0px\"></th>
			    	    	 	</tr>
			    	    	 	<tr id=\"table_variants_delete_row_".$index."\" class=\"table_variants_delete_row\">
			    	    	 		<td colspan=\"3\" style=\"background-color:#ffffff;padding: 4px;\">
			    	    	 		You haven't added any variants to this product yet...
			    	    	 		</td>
			    	    	 	</tr>
			    	    	 </table>
			    	    		</div>
			    	    	</p>
			    	    	<div class=\"hidden isunique\"></div></p>
			    	    	 ";
			}
		}
   		return $result;
   	}
   	
   	function variant_added_to_table($index=0,$variant_selectVal,$post_id='',$post_child='',$key=''){
   		global $db,$id_active;
   		$offset=$key;
   		//print_r($variant_selectVal);
   		//print_r($post_child);
   		$var='';
   		$active_count=0;
   		//if($post_id==''){
	   		if($variant_selectVal<>'nothink'){
				$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d order by lrule_id", $variant_selectVal);
				$execute_added=$db->do_query($sql_added);
				$data_added=$db->fetch_array($execute_added);
				$varian_parent=$data_added['lname'];
				
				$rows_added=$db->num_rows($execute_added);
	   			if($rows_added<>0){
	   				$sql_added_child=$db->prepare_query("select * from lumonata_rules
		                where
		                lparent = %d order by lrule_id", $variant_selectVal);
	   				$execute_added_child=$db->do_query($sql_added_child);
	   				
	   					while($data_added_child=$db->fetch_array($execute_added_child)){	
	   					$sel_css="";
	   					$sel_css2="";
	   					$the_value="0.00";
	   					$disabled='disabled="disabled"';
	   					if(!empty($post_id)){
	   						//print_r($post_child);
	   						
		   					foreach ($post_child as $key=>$value){
		   						if($key==$data_added_child['lrule_id']){
		   							$sel_css="added";
		   							$sel_css2="tagvariantoptionadded";
		   							$the_value=$value[0];
		   							$disabled='';
		   						}
		   					}
	   					}else{
	   						$sel_css="added";
	   						$sel_css2="tagvariantoptionadded";
	   						$disabled='';
	   					}
	   					
	   					$varian_child=$data_added_child['lname'];
	   					$var = "<div class=\"variantDetailsBox ".$sel_css."\" id=\"variantDetailsBox_".$index."_".$data_added_child['lrule_id']."\">
	   					<a class=\"tagvariantoption ".$sel_css2."\" id=\"tagvariantoption_".$index."_".$data_added_child['lrule_id']."\" title=\"Click to toggle\" rel=\"".$index."_".$data_added['lrule_id']."_".$data_added_child['lrule_id']."\">".$varian_child."</a>
	   					<div class=\"variantDetailsPrice\" id=\"variantDetailsPrice_".$index."_".$data_added_child['lrule_id']."\">&plusmn;<input type=\"text\" value=\"".$the_value."\" id=\"var_price_".$index."_".$data_added_child['lrule_id']."\" class=\"var_price\" name=\"var_price_".$index."_".$data_added['lrule_id']."_".$data_added_child['lrule_id']."[".$index."]\" size=\"5\" ".$disabled.">$</div></div>&nbsp".$var;
	   					$id_active=$data_added_child['lrule_id'].",".$id_active;
	   					$active_count++;
	   				}
	   					
	   					
	   				
	   			}
	   		
			
	   		$result="<tr colspan=\"3\" class=\"variantlist delete_variant_add_link_".$data_added['lrule_id']."\" id=\"delete_variant_add_link_".$index."_".$data_added['lrule_id']."\" >
	    	    	 		<td style=\"width:200px; background-color:#ffffff;padding-left:10px\"><input type=\"hidden\" id=\"variant_typeid\" name=\"var_typeid_".$index."_".$data_added['lrule_id']."\" value=\"".$data_added['lrule_id']."\">".$varian_parent."<span class=\"number_of_options_selected\" id=\"number_of_options_selected_".$index."_".$data_added['lrule_id']."\" rel=\"".$active_count."\">(".$active_count." selected)</span></td>
	    	    	 		<td style=\"width:420px; background-color:#ffffff; text-align:center;padding:0 3px;\"><input id=\"var_gridoptions_".$index."_".$data_added['lrule_id']."\" type=\"hidden\" name=\"var_gridoptions_".$index."_".$data_added['lrule_id']."\"  value=\"".rtrim($id_active, ',')."\">$var</td>
	    	    	 		<td style=\"width:50px;background-color:#ffffff;text-align:center;padding:0 3px;\"><a id=\"link_variants_delete_".$index."_".$data_added['lrule_id']."\" class=\"link_variants_delete_".$index."\" name=\"link_variants_delete\" rel=\"".$data_added['lrule_id']."\" style=\"text-decoration:none;cursor:pointer\">delete</a></td>
	    	    	 	</tr>".delete_variant_grid($index,$data_added['lrule_id']);
						
			$result.= '<script>
					$(document).ready(function(){
						$("#link_variants_delete_'.$index.'_'.$data_added['lrule_id'].'").click(function() {
							//$("#delete_variant_grid_form_'.$index.'_'.$data_added['lrule_id'].'").dialog("open");
							
							var record_sub_variant=jQuery("#the_variant_table_'.$index.' tr.variantlist").length;
							if(record_sub_variant<1){
								 $("#table_variants_delete_row_'.$index.'").attr("style","");
							}
						});
						
						/* $("#delete_variant_grid_form_'.$index.'_'.$data_added['lrule_id'].'").dialog({
						      bgiframe: true,
						      autoOpen: false,
						      resizable: false,
						      height:170,
						      width:330,
						      modal: true,							      
						      buttons: {
						        "Yes, delete this variant": function() {
						         	$(this).dialog("close");
						           
						          },Cancel: function() {
						            $(this).dialog("close");
						          }
						        }
						   })*/
					});
				</script>';
	   		}
   		
   		return $result;
   	}
   	
	function delete_variant_grid($index='',$name){
   		$delete_it="<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />
		  <div id=\"delete_variant_grid_form_".$index."_".$name."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this variant?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
	
   	//selectbox variant
   	function selectbox_variant($index){
   		 $result_selectbox="<b>Variant : </b>
			                        
			                        <select name=\"variant_product[".$index."]\" id=\"variant_product_".$index."\" class=\"variant_product\" >
			                        	<option id=\"default\" class=\"default\" value=\"nothink\">Select variant...</option>
			                            ".view_variant_product($index,$rule_name='variant',$group='product','select')."
				                      	<optgroup id=\"optionOr\" label=\"Or\">
											<option id=\"create-user_".$index."\">Create new variant</option>
										</optgroup>
									</select>
									<span class=\"edit_variant_".$index."\"></span><span class=\"setvariable_".$index."\"></span>&nbsp;<span class=\"delete_variant_".$index."\"></span>
							";
   		 return $result_selectbox;
   	
   	}
   	
   	function view_variant_product($index,$rule_name,$group,$type='select',$order='ASC',$parent=0){
        global $db;
        $items='';
        $sql=$db->prepare_query("SELECT *
                                 FROM lumonata_rules 
                                 WHERE lrule=%s AND (lgroup=%s OR lgroup=%s) AND lparent=%d  order by lorder $order",$rule_name,$group,'default',$parent);
        
        $r=$db->do_query($sql);
        while($d=$db->fetch_array($r)){
              $items.= "<option value=\"".$d['lrule_id']."\">".$d['lname']."</option>" ;
        }
        return $items;
        
   	}
   	
   	function save_new_product($title,$content,$status,$type,$comments,$sef='',$share_to=0){
   		global $db,$allowedposttags,$allowedtitletags;
   		
   		if(empty($title)){
           //$num_untitled=is_num_articles('title=Untitled&type='.$type)+1;
           $title="Untitled";
        }else{
            $title=kses(rem_slashes($title),$allowedtitletags);
        }
        $content=kses(rem_slashes($content),$allowedposttags);
        
   		if(empty($sef)){
            $num_by_title_and_type=is_num_articles('title='.$title.'&type='.$type);
            if($num_by_title_and_type>0){
                for($i=2;$i<=$num_by_title_and_type+1;$i++){
                	$sef=generateSefUrl($title)."-".$i;
                	if(is_num_articles('sef='.$sef.'&type='.$type) < 1){
                		$sef=$sef;
                		break;
                	}
                }
            }else{
                $sef=generateSefUrl($title);
            }
        }
   		
   		//save_new_product($title,$_POST['post'][0],$status,$_POST['price'],$_POST['wight'],$_POST['tax'],$_POST['stock_limit']);
   		$sql=$db->prepare_query("INSERT INTO lumonata_articles( larticle_title,
                                                                larticle_content,
                                                                larticle_status,
                                                                larticle_type,
                                                                lcomment_status,
                                                                lsef,
                                                                lpost_by,
                                                                lpost_date,
                                                                lupdated_by,
                                                                ldlu,
                                                                lshare_to)
                                VALUES(%s,%s,%s,%s,%s,%s,%d,%s,%d,%s,%d)",
                                                                $title,
                                                                $content,
                                                                $status,
                                                                $type,
                                                               	$comments,
                                                                $sef,
                                                                $_COOKIE['user_id'],
                                                                date("Y-m-d H:i:s"),
                                                                $_COOKIE['user_id'],
                                                                date("Y-m-d H:i:s"),
                                                                $share_to
                                                               );
       
       if(reset_order_id("lumonata_articles"))
            return $db->do_query($sql);
        
        return false;
   	}

   	//COSTUME PRODUCT AUTO id function
   	function set_auto_id($table,$id,$prefix){
   		global $db;
   		/*$result='';
   		$sql=$db->prepare_query("Select MAX($id) as larticle_id from $table");
   		$result=$db->do_query($sql);
   		$record=$db->num_rows($result);
   		if($record<>0){
	   		$data=$db->fetch_array($result);
	   		//$long_text=strlen($data['l_id']);
	   		//$take_number=substr($data['l_id'],1,$long_text);
	   		$suffix=$data['larticle_id']+1;
	   		$id_auto_inc=$prefix.$suffix;
	   		
   		}else{
   			$id_auto_inc="1";
   		}*/
   		$id_auto_inc=time();
   		return($id_auto_inc);
   	}
   	
   	function insert_product_relationship($app_id,$rule_id){
        global $db;
      
        if(count_product_relationship("lproduct_id=$app_id&rule_id=$rule_id")==0){
           $sql=$db->prepare_query("INSERT INTO lumonata_product_relationship(lproduct_id,lrule_id)
                                      VALUES(%d,%d)",$app_id,$rule_id);
           if($db->do_query($sql)){
                $rule_count=count_product_relationship("rule_id=$rule_id");
                return update_product_count($rule_id,$rule_count);
            }
        }
    }
    
    function count_product_relationship($args=''){
         global $db;
        $var_name['app_id']='';
        $var_name['rule_id']='';
        $where="";
        
        if(!empty($args)){
            $args=explode('&',$args);
            
            $where=" WHERE ";
           
            $i=1;
            foreach($args as $val){
                list($variable,$value)=explode('=',$val);
                if($variable=='app_id' || $variable=='rule_id'){
                    $where.="l".$variable."=".$db->_real_escape($value);
                    if($i!=count($args)){
                        echo $where.=" AND ";
                    }
                    
                }
                $i++;
            }
        }
        
        $sql=$db->prepare_query("SELECT * FROM lumonata_product_relationship $where ");
       
        return $db->num_rows($db->do_query($sql));
    }
    
    function update_product_count($rule_id,$rule_count){
        global $db;
        $sql=$db->prepare_query("UPDATE lumonata_rules
                                SET lcount=%d
                                WHERE lrule_id=%d",$rule_count,$rule_id);
        return $db->do_query($sql);
    }
    
    //add images
     function add_images($name,$tag,$display=true,$args=array()){	
        global $thepost,$db;
               
        if(function_exists($tag)){
            $details=call_user_func_array($tag,$args);
        }else{
            $details=$tag;
        }
        
        /*if($display==false)
            $display="style=\"display:none;\"";
        else
            $display="";*/
            
            $data="
        <div class=\"additional_data\">
                    <h2 id=\"".$tag."_".$thepost->post_index."\">$name</h2>
                    <div class=\"additional_content\" id=\"".$tag."_details_".$thepost->post_index."\" $display >
                      	<div id=\"block_images\">
                       		$details
                   		</div>
                    </div>
                </div>
                <script type=\"text/javascript\">
                    $(function(){
                        $('#".$tag."_".$thepost->post_index."').click(function(){
                            $('#".$tag."_details_".$thepost->post_index."').slideToggle(100);
                            return false;
                        });
                    });
                </script>
                ";
        return $data;
        
    }
    
    function upload_images($index=0,$post_id,$type='products'){
    	global $db;
    	if(is_save_draft() || is_publish()){
		    	if(is_edit()||is_edit_all()){
					$result="
					<input type=\"hidden\"  id=\"tmp_id_".$post_id."\" value=\"".$post_id."\">
		        	<span style=\"font-size:13px;font-weight:bold;\">Upload a new image</span>
								<span id=\"example1\" class=\"example\">
									<div class=\"wrapper\" style=\"width: 99%; margin: 0 auto;\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"button1_".$index."\" class=\"button upload\" rel=\"".$post_id."\">Upload</div>
									</div>
									</p>
								</span>
								<p><span id=\"loader_".$post_id."\"  style=\"display:none\"><img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\"></span></p>
								<div class=\"ui-sortable\" id=\"list_item\">
				   			<div class=\"key_product_image\" id=\"key_product_images_".$post_id."\" style=\"padding: 0px;width:99%;\"></div>
				   		</div>";
					$sql=$db->prepare_query("select * from lumonata_attachment
			              where
			              larticle_id = %d order by lattach_id Desc", $post_id);
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
				    	$result.=product_images_list($r,$index,$index);
			       	}else{
			            //nothink to do
			        }   
		     	}else{
		     	  $result="
					<input type=\"hidden\"  id=\"tmp_id_".$post_id."\" value=\"".$post_id."\">
		        	<span style=\"font-size:13px;font-weight:bold;\">Upload a new image</span>
								<span id=\"example1\" class=\"example\">
									<div class=\"wrapper\" style=\"width: 99%; margin: 0 auto;\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"button1_".$index."\" class=\"button upload\" rel=\"".$post_id."\">Upload</div>
									</div>
									</p>
								</span>
								<p><span id=\"loader_".$post_id."\"  style=\"display:none\"><img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\"></span></p>
								<div class=\"ui-sortable\" id=\"list_item\">
				   			<div class=\"key_product_image\" id=\"key_product_images_".$post_id."\" style=\"padding: 0px;width:99%;\"></div>
				   		</div>";
		     	  	$sql=$db->prepare_query("select * from lumonata_attachment
			              where
			              larticle_id = %d order by lattach_id Desc", $post_id);
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
				    	$result.=product_images_list($r,$index,$index);
			       	}else{
			            //nothink to do
			        }   
		     	}
    	}else{
    		if(is_edit()||is_edit_all()){
					$result="
					<input type=\"hidden\"  id=\"tmp_id_".$post_id."\" value=\"".$post_id."\">
		        	<span style=\"font-size:13px;font-weight:bold;\">Upload a new image</span>
								<span id=\"example1\" class=\"example\">
									<div class=\"wrapper\" style=\"width: 99%; margin: 0 auto;\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"button1_".$index."\" class=\"button upload\" rel=\"".$post_id."\">Upload</div>
									</div>
									</p>
								</span>
								<p><span id=\"loader_".$post_id."\"  style=\"display:none\"><img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\"></span></p>
								<div class=\"ui-sortable\" id=\"list_item\">
				   			<div class=\"key_product_image\" id=\"key_product_images_".$post_id."\" style=\"padding: 0px;width:99%;\"></div>
				   		</div>";
					$sql=$db->prepare_query("select * from lumonata_attachment
			              where
			              larticle_id = %d order by lattach_id Desc", $post_id);
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
				    	$result.=product_images_list($r,$index,$index);
			       	}else{
			            //nothink to do
			        }   
		     }else{
		    	 $post_id=set_auto_id('lumonata_articles','larticle_id','');
				    	  $result="
				    	  <input type=\"hidden\"  id=\"tmp_id_".$post_id."\" value=\"".$post_id."\">
				        	<span style=\"font-size:13px;font-weight:bold;\">Upload a new image</span>
										<span id=\"example1\" class=\"example\">
											<div class=\"wrapper\" style=\"width: 99%; margin: 0 auto;\">
											<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
												<div id=\"button1_".$index."\" class=\"button upload\" rel=\"".$post_id."\">Upload</div>
											</div>				
										</span>
										<p><span id=\"loader_".$post_id."\"  style=\"display:none\"><img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\"></span></p>
										<div class=\"ui-sortable\" id=\"list_item\">
						   			<div class=\"key_product_image\" id=\"key_product_images_".$post_id."\" style=\"padding: 0px;width:99%;\"></div>
						   		</div>";
				 $sql=$db->prepare_query("select * from lumonata_attachment
		              where
		              larticle_id = %d order by lattach_id Desc", $post_id);
		    	$r=$db->do_query($sql);
		        if($db->num_rows($r) > 0){
			    	$result.=product_images_list($r,$index,$index);
		       	}else{
		            //nothink to do
		        }   
			}   
	    }
        return $result;
    }
    
  //add product_options
  function add_product_options($name,$tag,$display=true,$args=array()){	
        global $thepost,$db;
               
        if(function_exists($tag)){
            $details=call_user_func_array($tag,$args);
        }else{
            $details=$tag;
        }      
       
            
       $data="<div class=\"additional_data\">
                    <h2 id=\"".$tag."_".$thepost->post_index."\">$name</h2>
                    <div class=\"additional_content\" id=\"".$tag."_details_".$thepost->post_index."\" $display >
                      	<div id=\"block_options\">
                       		$details
                   		</div>
                    </div>
                </div>
                <script type=\"text/javascript\">
                    $(function(){
                        $('#".$tag."_".$thepost->post_index."').click(function(){
                            $('#".$tag."_details_".$thepost->post_index."').slideToggle(100);
                            return false;
                        });
                    });
                </script>
                ";
        return $data;
        
    }

   //product options
   function product_options($index=0,$post_id,$type='products'){
    	global $db;
    	$result = '';
    	if(is_save_draft() || is_publish()){
		    	if(is_edit()||is_edit_all()){					
					$sql=$db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s", $post_id,'product_additional');
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
			        	$d = $db->fetch_array($r);
			        	$arr_add_field = json_decode($d['lvalue'],true); 
			        	if (isset($arr_add_field['set_as_featured']) && ($arr_add_field['set_as_featured']=='1')) {					    	
				    		$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" checked="checked" /> Set as featured';
			       		}else{
			            	$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" /> Set as featured';		    
			       		 }  
			        } 
		     	}else{
		     	 	$sql=$db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s", $post_id,'product_additional');
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
			        	$d = $db->fetch_array($r);
			        	$arr_add_field = json_decode($d['lvalue'],true); 
			        	if (isset($arr_add_field['set_as_featured']) && ($arr_add_field['set_as_featured']=='1')) {					    	
				    		$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" checked="checked" /> Set as featured';
			       		}else{
			            	$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" /> Set as featured';		    
			       		 }  
			        } 
		     	}
    	}else{
    		if(is_edit()||is_edit_all()){
					$sql=$db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s", $post_id,'product_additional');
			    	$r=$db->do_query($sql);
			        if($db->num_rows($r) > 0){
			        	$d = $db->fetch_array($r);
			        	$arr_add_field = json_decode($d['lvalue'],true); 
			        	if (isset($arr_add_field['set_as_featured']) && ($arr_add_field['set_as_featured']=='1')) {					    	
				    		$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" checked="checked" /> Set as featured';
			       		}else{
			            	$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" /> Set as featured';		    
			       		 }  
			        } 
		     }else{
		    	$result = '<input style="vertical-align:middle;" type="checkbox" id="set_as_featured_'.$index.'" value="1" name="set_as_featured['.$index.']" /> <span>Set as featured</span>';
			}   
	    }
        return $result;
    }
     
  add_actions('product_ajax_page', 'product_ajax');
     
  function product_ajax(){
	add_actions('is_use_ajax', true);
	global $db;
	if(isset($_FILES['myfile']) && !empty($_FILES['myfile'])){
		if(isset($_POST['post_id'])){
			$product_id = $_POST['post_id'];
		}else{
			$product_id=set_auto_id('lumonata_articles','larticle_id','');
		}
		
		if(isset($_POST['code'])){
			$product_code = $_POST['code'];
		}else{
			$product_code='';
		}
			 //$post_by = $_COOKIE['user_id'];urldecode($str)
			 //$user_fetched=fetch_user($post_by);
			// $display_name = $user_fetched['ldisplay_name'];
		     $file_name = $_FILES['myfile']['name'];
	         $file_size = $_FILES['myfile']['size'];
	         $file_type = $_FILES['myfile']['type'];
	         $file_source = $_FILES['myfile']['tmp_name'];
	                    
	         if(is_allow_file_size($file_size)){
	            if(is_allow_file_type($file_type,'image')){
	               // $fix_file_name=file_name_filter($product_code);
	                
	                 if (empty($product_code)){
	                 	$fix_file_name='untitled';	
	                 }else{
	                 	$fix_file_name=file_name_filter($product_code);
	                 }
	                 
	                 $file_ext=file_name_filter($file_name,true);
					 
	                 $sef_url = $fix_file_name.'-'.time();
	                 $file_name_t=$sef_url. $file_ext;
	                 $file_name_m=$sef_url.'-medium'. $file_ext;
	                 $file_name_l=$sef_url.'-large'. $file_ext;
	
	                 $destination1=PLUGINS_PATH."/shopping_cart/uploads/product/".$file_name_t;
	                 $destination1_m=PLUGINS_PATH."/shopping_cart/uploads/product/".$file_name_m;
	                 $destination1_l=PLUGINS_PATH."/shopping_cart/uploads/product/".$file_name_l;
	                 $destination2=PLUGINS_PATH."/shopping_cart/uploads/product/thumbnail/".$file_name_t;
	                 //for database
	                 $destinationsave1="/lumonata-plugins/shopping_cart/uploads/product/".$file_name_t;
	                 $destinationsave1_m="/lumonata-plugins/shopping_cart/uploads/product/".$file_name_m;
	                 $destinationsave1_l="/lumonata-plugins/shopping_cart/uploads/product/".$file_name_l;
	                 $destinationsave2="/lumonata-plugins/shopping_cart/uploads/product/thumbnail/".$file_name_t;
	                 
					 //delete_file
					 upload_resize($file_source,$destination2,$file_type,thumbnail_image_width(),thumbnail_image_height());
	                 upload_resize($file_source,$destination1_m,$file_type,medium_image_width(),medium_image_height());
	                 upload_resize($file_source,$destination1_l,$file_type,large_image_width(),large_image_height());
	                 upload($file_source,$destination1);
					         
	           		 $sql=$db->prepare_query("Insert Into lumonata_attachment
	                 (larticle_id,lattach_loc,lattach_loc_thumb,lattach_loc_medium,lattach_loc_large,ltitle,
	                 mime_type,lorder,upload_date) 
	                 Values
	                 (%d,%s,%s,%s,%s,%s,%s,%s,%s)",
	                 $product_id,$destinationsave1,$destinationsave2,$destinationsave1_m,$destinationsave1_l,$file_name_t,$file_type,1,date("Y-m-d H:i:s"));
	                 $r=$db->do_query($sql);
	                 
	                 echo "success";
	           }else{
	                echo "The maximum file size is 2MB";
	           }
	       }else{
	                echo "The maximum file size is 2MB";
	                         
	       }
       
		}
		
	if (isset($_POST['pKEY']) and $_POST['pKEY']=='get_product_images'){
			$sql=$db->prepare_query("select * from lumonata_attachment
	                where
	                larticle_id = %d order by lattach_id Desc Limit 1", $_POST['val_tmp_id']);
	    	$r=$db->do_query($sql);
	        if($db->num_rows($r) > 0){
		    	//echo upload_images($r,$_POST['product_index']);
		    	echo product_images_list($r,$_POST['product_index']);
	       	}else{
	            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_tmp_id']."</em>. Check your spellling or try another terms</div>";
	        }
	}
	
  if (isset($_POST['pKEY']) and $_POST['pKEY']=='set_delete_product'){
		
		$val = $_POST['val'];
		$sqlc=$db->prepare_query("Select * From lumonata_attachment Where lattach_id=%d",$val);
		$resultc=$db->do_query($sqlc);
		$datac=$db->fetch_array($resultc);
		$file_name=$datac['ltitle'];
		$file_name_m=$datac['lattach_loc_medium'];
		$file_name_l=$datac['lattach_loc_large'];
		$sql=$db->prepare_query("DELETE FROM lumonata_attachment
				WHERE lattach_id=%d",$val);
		$result=$db->do_query($sql);
		if ($result){
	        $destination1=PLUGINS_PATH."/shopping_cart/uploads/product/".$file_name;
			$destination2=PLUGINS_PATH."/shopping_cart/uploads/product/thumbnail/".$file_name;
			$destination_m=ROOT_PATH.$file_name_m;
			$destination_l=ROOT_PATH.$file_name_l;
			delete_file($destination1);
			delete_file($destination2);
			delete_file($destination_m);
			delete_file($destination_l);
			
			echo "success";
		}
		
		
	}
	
	if (isset($_POST['pKEY']) and $_POST['pKEY']=='add_variant_product'){
		$namevariant=$_POST['names'];
		$sub_variant=$_POST['sub_variant'];
		$TheSef=generateSefUrl($namevariant);
		
		echo save_new_variant('0',$namevariant,$TheSef,'','variant','product','0','1','arunna',$sub_variant);
	}
	
   if (isset($_POST['pKEY']) and $_POST['pKEY']=='setvariable_variant_product'){
   		$child_variant="";
   		$child_variant_id="";
   		$keys_post=$_POST['keys_post'];
  		$variant_id=$_POST['variant_idpost'];
  		
  		
  		$sql=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d order by lrule_id", $variant_id);
	    $run_variant=$db->do_query($sql);
	    $data_variant=$db->fetch_array($run_variant);
	   	$parent_variant=$data_variant['lname'];
	   	
	   	$rows=$db->num_rows($run_variant);
	   	if($rows<>0){
	   		$sql_child=$db->prepare_query("select * from lumonata_rules
	                where
	                lparent = %d order by lrule_id", $data_variant['lrule_id']);
	   		 $run_child_variant=$db->do_query($sql_child);
	   		 while($data_child_variant=$db->fetch_array($run_child_variant)){
	   		 	$child_variant=$data_child_variant['lname'].",".$child_variant;
	   		 	$child_variant_id=$data_child_variant['lrule_id'].",".$child_variant_id;
	   		 }
	   	}else{
	   		$child_variant="";
	   	}
	   	if($keys_post=='parent'){
	   		echo $parent_variant;
	   	}else if($keys_post=='child'){
			//echo $child_variant."?".$child_variant_id;
			echo $child_variant;
	   	}
	}
	
  	if (isset($_POST['pKEY']) and $_POST['pKEY']=='edit_variant_product'){
  		$id_variant=$_POST['id_variant'];
  		$namevariant=$_POST['name_post'];
  		$sub_edit_variant=$_POST['sub_variant_post'];
  		
  		//update parent
  		$sql_update_variant=$db->prepare_query("UPDATE lumonata_rules SET lname=%s where lrule_id=%d",$namevariant,$id_variant);
		$execute_update_variant=$db->do_query($sql_update_variant);
		
		//update child
		$fields_edit=explode(",", $sub_edit_variant);
        $count_of_edit_variant=count($fields_edit);
        
        //$sql_delete_variant=$db->prepare_query("DELETE FROM lumonata_rules where lparent=%d",$id_variant);
       // $execute_delete_variant=$db->do_query($sql_delete_variant);
        $subsiteEdit='arunna';
        $orderEdit='1';
        $countEdit='0';
        $groupEdit='product';
        $ruleEdit='variant';
        $descEdit='';
        
        for($i=0;$i<$count_of_edit_variant;$i++){
        	if($fields_edit[$i]<>''){
        		$TheSefChildEdit=generateSefUrl($fields_edit[$i]);
        		$sql_select_rule=$db->prepare_query("SELECT * FROM lumonata_rules WHERE lname=%s and lparent=%s",$fields_edit[$i],$id_variant);
				$rule_query=$db->do_query($sql_select_rule);
				if(($db->num_rows($rule_query))==0){
					$sql=$db->prepare_query("INSERT INTO lumonata_rules(	
		   																lparent,
		   																lname,
		   																lsef,
		   																ldescription,
		   																lrule,
		   																lgroup,
		   																lcount,
		   																lorder,
		   																lsubsite)
		                                VALUES(%s,%s,%s,%s,%s,%s,%s,%d,%s)",		   																
		                                                                $id_variant,
		                                                                $fields_edit[$i],
		                                                                $TheSefChildEdit,
		                                                                $descEdit,
		                                                                $ruleEdit,
		                                                                $groupEdit,
		                                                                $countEdit,
		                                                                $orderEdit,
		                                                                $subsiteEdit
		                                                             );
		         $db->do_query($sql);
				}
        	}
        }
        
       /* if($execute_delete_variant){
        	for($i=0;$i<$count_of_edit_variant;$i++){
        		if($fields_edit[$i]<>''){
        		$TheSefChildEdit=generateSefUrl($fields_edit[$i]);
        		$sql=$db->prepare_query("INSERT INTO lumonata_rules(	
		   																lparent,
		   																lname,
		   																lsef,
		   																ldescription,
		   																lrule,
		   																lgroup,
		   																lcount,
		   																lorder,
		   																lsubsite)
		                                VALUES(%s,%s,%s,%s,%s,%s,%s,%d,%s)",
		   																
		                                                                $id_variant,
		                                                                $fields_edit[$i],
		                                                                $TheSefChildEdit,
		                                                                $descEdit,
		                                                                $ruleEdit,
		                                                                $groupEdit,
		                                                                $countEdit,
		                                                                $orderEdit,
		                                                                $subsiteEdit
		                                                             );
		         $db->do_query($sql);
        		}
        	}
  	 	
      }*/
		echo selectbox_variant('0');
	}
	
	if (isset($_POST['pKEY']) and $_POST['pKEY']=='del_variant_product'){
		global $db;
		$variant_idpost=$_POST['variant_idpost'];
		$lrule="variant";
		$lgroup="product";
		$sql=$db->prepare_query("DELETE FROM lumonata_rules where lrule=%s and lgroup=%s and lparent=%d or lrule_id=%d",$lrule,$lgroup,$variant_idpost,$variant_idpost);
		$execute=$db->do_query($sql);
		echo selectbox_variant('0');
	}

	if (isset($_POST['pKEY']) and $_POST['pKEY']=='add_link_variant'){
		global $db;
		$variant_selectVal=$_POST['variant_selectVal'];
		$index=$_POST['index'];
		echo variant_added_to_table($index,$variant_selectVal);
	}

	if (isset($_POST['pKEY']) and $_POST['pKEY']=='delete_product'){
			$get_product_id=$_POST['val'];
	        $the_action= delete_article($get_product_id,'products');
            $the_action.= delete_additional_field($get_product_id, 'products');
            $the_action.=$args_product="app_id=".$get_product_id;
            $the_action.=delete_rules_relationship($args_product);
            while($data_attach=get_attachment_id($get_product_id)){
               	$the_action.=delete_attachment($data_attach['lattach_id']);
            }
            echo $the_action;
            
	}
  
	if(isset($_FILES['upload_categories_images']) && !empty($_FILES['upload_categories_images'])){

			$category_id = $_POST['category_id'];
			
			 //$post_by = $_COOKIE['user_id'];urldecode($str)
			 //$user_fetched=fetch_user($post_by);
			// $display_name = $user_fetched['ldisplay_name'];
		     $file_name = $_FILES['upload_categories_images']['name'];
	         $file_size = $_FILES['upload_categories_images']['size'];
	         $file_type = $_FILES['upload_categories_images']['type'];
	         $file_source = $_FILES['upload_categories_images']['tmp_name'];
	                    
	         if(is_allow_file_size($file_size)){
	            if(is_allow_file_type($file_type,'image')){
	                $fix_file_name=file_name_filter($_COOKIE['username']);
	                 $title_image_gallery = $file_name;
	                 if (empty($title_image_gallery)){
	                 	$fix_file_name='untitled-'.time();	
	                 	$title = "Untitled";
	                 }else{
	                 	$fix_file_name=file_name_filter($title_image_gallery);
	                 	$title = $title_image_gallery;
	                 }
	                 
	                 //$_POST['title_image_gallery'][$post_index];
	                 $file_ext=file_name_filter($file_name,true);
					 
	                 $sef_url = $fix_file_name.'-'.time();
	                 $file_name=$sef_url. $file_ext;
	                 
	                 $file_name_m=$sef_url.'-'.time().'-medium'. $file_ext;
					 $file_name_l=$sef_url.'-'.time().'-large'. $file_ext;
					 $destination1_m=PLUGINS_PATH."/shopping_cart/uploads/categories/".$file_name_m;
	                 $destination1_l=PLUGINS_PATH."/shopping_cart/uploads/categories/".$file_name_l;
	                 $destination1=PLUGINS_PATH."/shopping_cart/uploads/categories/".$file_name;
	                 $destination2=PLUGINS_PATH."/shopping_cart/uploads/categories/thumbnail/".$file_name;
	                 
	                 //for database
	                 
	                 $destinationsave1="/lumonata-plugins/shopping_cart/uploads/categories/".$file_name;
	                 $destinationsave1_m="/lumonata-plugins/shopping_cart/uploads/categories/".$file_name_m;
	                 $destinationsave1_l="/lumonata-plugins/shopping_cart/uploads/categories/".$file_name_l;
	                 $destinationsave2="/lumonata-plugins/shopping_cart/uploads/categories/thumbnail/".$file_name;
	                 
					 //delete_file
					 upload_resize($file_source,$destination2,$file_type,thumbnail_image_width(),thumbnail_image_height());
	                 upload_resize($file_source,$destination1_m,$file_type,medium_image_width(),medium_image_height());
	                 upload_resize($file_source,$destination1_l,$file_type,large_image_width(),large_image_height());
	                 upload($file_source,$destination1);
					         
	           		$sql=$db->prepare_query("Insert Into lumonata_attachment
	                 (larticle_id,lattach_loc,lattach_loc_thumb,lattach_loc_medium,lattach_loc_large,ltitle,
	                 mime_type,lorder,upload_date) 
	                 Values
	                 (%d,%s,%s,%s,%s,%s,%s,%s,%s)",
	                 $category_id,$destinationsave1,$destinationsave2,$destinationsave1_m,$destinationsave1_l,$file_name,$file_type,1,date("Y-m-d H:i:s"));
	                 $r=$db->do_query($sql);
	                 
	                 echo "success";
	           }else{
	                echo "The maximum file size is 2MB";
	           }
	       }else{
	                echo "The maximum file size is 2MB";
	                         
	       }
       
		}
		
  if (isset($_POST['pKEY']) and $_POST['pKEY']=='get_category_images'){
			$sql=$db->prepare_query("select * from lumonata_attachment
	                where
	                larticle_id = %d order by lattach_id Desc Limit 1", $_POST['category_index']);
	    	$r=$db->do_query($sql);
	        if($db->num_rows($r) > 0){
	        	
		    	echo categories_images_list($r,$_POST['category_index']);
	       	}else{
	            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_tmp_id']."</em>. Check your spellling or try another terms</div>";
	        }
	}
	
   if (isset($_POST['pKEY']) and $_POST['pKEY']=='set_delete_categories'){
		
		$val = $_POST['val'];
		$sqlc=$db->prepare_query("Select * From lumonata_attachment Where lattach_id=%d",$val);
		$resultc=$db->do_query($sqlc);
		$datac=$db->fetch_array($resultc);
		$file_name=$datac['ltitle'];
		$file_name_l=$datac['lattach_loc_medium'];
		$file_name_m=$datac['lattach_loc_large'];
		$sql=$db->prepare_query("DELETE FROM lumonata_attachment
				WHERE lattach_id=%d",$val);
		$result=$db->do_query($sql);
		if ($result){
	        $destination1=PLUGINS_PATH."/shopping_cart/uploads/categories/".$file_name;
			$destination2=PLUGINS_PATH."/shopping_cart/uploads/categories/thumbnail/".$file_name;
			$destination3=ROOT_PATH.$file_name_l;
			$destination4=ROOT_PATH.$file_name_m;
			delete_file($destination1);
			delete_file($destination2);
			delete_file($destination3);
			delete_file($destination4);
			
			echo "success";
		}
		
		
	}
	
   if (isset($_POST['pKEY']) and $_POST['pKEY']=='delete_categories'){
			$get_categories_id=$_POST['val'];
            $the_action.= delete_additional_field($get_categories_id,'categories');
            $the_action.=$args_categories="app_id=".$get_categories_id;
            $the_action.=delete_rule($get_categories_id);
            while($data_attach=get_attachment_id($get_categories_id)){
               	$the_action.=delete_attachment($data_attach['lattach_id']);
            }
            echo $the_action;
            
	}
	
   if (isset($_POST['pKEY']) and $_POST['pKEY']=='src_product'){
    	$sql=$db->prepare_query("select * from lumonata_articles where larticle_type=%s and (larticle_title like %s or larticle_content like %s)",'products',"%".$_POST['lvalue']."%","%".$_POST['lvalue']."%");
    	$result=$db->do_query($sql);
	 	echo products_list($result,$i=1);
   }
	
  }
  
	//function save the variant
  function save_new_variant($parent,$name,$sef,$desc,$rule,$group,$count,$order,$subsite,$sub_variant){
   		global $db;
   		$sql=$db->prepare_query("INSERT INTO lumonata_rules(	
   																lparent,
   																lname,
   																lsef,
   																ldescription,
   																lrule,
   																lgroup,
   																lcount,
   																lorder,
   																lsubsite)
                                VALUES(%d,%s,%s,%s,%s,%s,%d,%d,%s)",
   																
                                                                $parent,
                                                                $name,
                                                                $sef,
                                                                $desc,
                                                                $rule,
                                                                $group,
                                                                $count,
                                                                $order,
                                                                $subsite
                                                             );
       
        if(reset_order_id("lumonata_rules")){
            $db->do_query($sql);
            return view_id_variant($parent,$name,$sef,$desc,$rule,$group,$count,$order,$subsite,$sub_variant);
        }else{
        	return false;
        }
   	}
   	
   function view_id_variant($parent,$name,$sef,$desc,$rule,$group,$count,$order,$subsite,$sub_variant){
   		global $db;

   		$sql=$db->prepare_query("SELECT lrule_id
   									FROM lumonata_rules WHERE lname='%s' order by lrule_id DESC limit 1",$name);
       	$data=$db->do_query($sql);
       	$result=$db->fetch_array($data);
        if($db->num_rows($data)<>0){
        	$fields=explode(",", $sub_variant);
        	$count_of_new_variant=count($fields);
        	for($i=0;$i<$count_of_new_variant;$i++){
				if($fields[$i]<>''){
					$TheSefChild=generateSefUrl($fields[$i]);
		        	$sql=$db->prepare_query("INSERT INTO lumonata_rules(	
		   																lparent,
		   																lname,
		   																lsef,
		   																ldescription,
		   																lrule,
		   																lgroup,
		   																lcount,
		   																lorder,
		   																lsubsite)
		                                VALUES(%s,%s,%s,%s,%s,%s,%s,%d,%s)",
		   																
		                                                                $result['lrule_id'],
		                                                                $fields[$i],
		                                                                $TheSefChild,
		                                                                $desc,
		                                                                $rule,
		                                                                $group,
		                                                                $count,
		                                                                $order,
		                                                                $subsite
		                                                             );
		         $db->do_query($sql);
			}else{
					//nothink to do 
			}
        }
        return selectbox_variant('0');
      }else{
        return selectbox_variant('0');
      }
   	}
   	
   	function add_new_variant($index){
   		$add_variant_modal="
   		<div id=\"dialog_".$index."\" title=\"Create variant\" >
		
  			<fieldset style=\"padding:0; border:0; margin-top:25px;\">
	    		<label for=\"name\"><span style=\"font-size: 81.0%;\">name (e.g. \"color\")</span></label>
	    		<input type=\"text\" style=\"margin-bottom:12px; width:92%; padding: .4em;\" name=\"name[".$index."]\" id=\"name_".$index."\" class=\"text ui-widget-content ui-corner-all\"/>
	    		<label for=\"option\"><p style=\"font-size: 81.0%; margin-bottom: 2px;\">Option (add one option at a time e.g. \"Red\")</p></label>
	    		<input type=\"text\" style=\"margin-bottom:12px; width:92%; padding: .4em;\"  name=\"sub_variant[".$index."]\" id=\"sub_variant_".$index."\" class=\"text ui-widget-content ui-corner-all\" />
	    		<span name=\"addoption_".$index."\" id=\"addoption_".$index."\" style=\"padding:2px 2px 3px;font-size: 81.0%; margin-bottom: 2px;\" class=\"button\">add option</span><span name=\"removeoption[".$index."]\" id=\"removeoption_".$index."\" style=\"padding:2px 2px 3px;font-size: 81.0%; margin-bottom: 2px;\" class=\"button\">remove option</span>
	    		<label for=\"option\"></label>
	  			<p style=\"margin-bottom: 2px;\"><select style=\"resize:none;margin-bottom:2px; width:95%; padding: .4em;height:100px;\" name=\"allvariant[".$index."]\" id=\"allvariant_".$index."\" class=\"text ui-widget-content ui-corner-all\" size=\"8\"></select></p>
  			</fieldset>

  		</div>";
   		return $add_variant_modal;
   	}
   	
   	function edit_variant($index){
   		$edit_variant_modal="
   		<div id=\"edit_variant_dialog_".$index."\" title=\"Edit variant\" >
  			<fieldset style=\"padding:0; border:0; margin-top:25px;\">
	    		<label for=\"name\"><span style=\"font-size: 81.0%;\">name (e.g. \"color\")</span></label>
	    		<input type=\"text\" style=\"margin-bottom:12px; width:92%; padding: .4em;\" name=\"E_name[".$index."]\" id=\"E_name_".$index."\" class=\"text ui-widget-content ui-corner-all\"/>
	    		<label for=\"option\"><p style=\"font-size: 81.0%; margin-bottom: 2px;\">Option (add one option at a time e.g. \"Red\")</p></label>
	    		<input type=\"text\" style=\"margin-bottom:12px; width:92%; padding: .4em;\"  name=\"E_sub_variant[".$index."]\" id=\"E_sub_variant_".$index."\" class=\"text ui-widget-content ui-corner-all\" />
	    		<span name=\"E_addoption[".$index."]\" id=\"E_addoption_".$index."\" style=\"padding:2px 2px 3px;font-size: 81.0%; margin-bottom: 2px;\" class=\"button\">add option</span><span name=\"E_removeoption[".$index."]\" id=\"E_removeoption_".$index."\" style=\"padding:2px 2px 3px;font-size: 81.0%; margin-bottom: 2px;\" class=\"button\">remove option</span>
	    		<label for=\"option\"></label>
	  			<p style=\"margin-bottom: 2px;\"><select style=\"resize:none;margin-bottom:2px; width:95%; padding: .4em;height:100px;\" name=\"E_allvariant[".$index."]\" id=\"E_allvariant_".$index."\" class=\"text ui-widget-content ui-corner-all\" size=\"8\"></select></p>
  			</fieldset>
  			<span id=\"loader_variant_".$index."\" style=\"display:none\"><p><img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/loading_data.gif\" width=\"20\" /></p></span>
  		</div>";
   		return $edit_variant_modal;
   	}
   	
   	function delete_variant($index){
   		$delete_it="
		  <div id=\"delete_variant_dialog_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this variant?</span></label>
		    		<input type=\"hidden\" name=\"variant_id_[".$index."]\" value=\"\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
     
	function product_images_list($result,$product_index,$i=0){
        global $db;
        $list='';
        $theImages='';
        $theIdImages='';
        while($d=$db->fetch_array($result)){
        	$theImages=$d['lattach_loc_thumb'];
        	$theIdImages=$d['lattach_id'];
        	$sql_image=$db->prepare_query("select
				lattach_id,
				larticle_id,
				lattach_loc,
				lattach_loc_thumb,
				lattach_loc_medium,
				lattach_loc_large,
				ltitle 
				 from 
				lumonata_attachment,lumonata_additional_fields 
				where 
				lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
				and lumonata_additional_fields.lapp_id=%d and lumonata_additional_fields.lvalue=%s
				and lumonata_additional_fields.lkey='product_image_thumbnail'",$d['larticle_id'],$theIdImages);
        	$query_images_thumb=$db->do_query($sql_image);
        	if($db->num_rows($query_images_thumb)<>0){
        		$data_images_thumb=$db->fetch_array($query_images_thumb);
        		$checked_image='checked="checked"';
        	}else{
        		$checked_image='';

        	}
        	$url = 'http://'.SITE_URL.$d['lattach_loc_large'];
			$url_thumb = 'http://'.SITE_URL.$theImages;
        	$list.='<div class="list_item clearfix images_list" id="product_images_list_'.$theIdImages.'" style="width:720px;">
                    <div class="list_author" style="width:180px;">
						<img src="'.$url_thumb.'" style="width:100px;">
					</div>
					<div class="list_author" style="float:right;text-align:center;width:250px;padding-top:20px">
                         <a href="" style="cursor: pointer;"> 
                         	<a rel="view_colorbox_elastic" href="'.$url.'">view</a>
                         </a>
                         <span>|
                         	<a href="javascript:;" rel="delete_'.$theIdImages.'">delete</a>
                         </a>
                         </span>
                         <span>|
                         <input type="radio" name="the_id_images['.$i.']" id="the_id_images'.$theIdImages.'" value="'.$theIdImages.'"'.$checked_image.'>Set thumbnail
                         </span>
                     </div>
					</div>';
        	
        	$image_id = $theIdImages;
        	$id = $image_id;
	        $post_index = $product_index;
	        $msg="Are sure want to delete ".$d['ltitle']."?";
	        $thaUrl= 'http://'.SITE_URL.'/product_ajax';
	        $close_frameid = "product_images_list_".$image_id;
	        $var = 'state=pages&prc=delete&id='.$image_id;
	        $var_no='';
      	 	$delete_confirmation_box = delete_confirmation_product($id,$post_index,$msg,$thaUrl,$close_frameid,$var='',$var_no='');
	     	
      	 	$script="<script type=\"text/javascript\">
         		function alert_run(){
        			jQuery('.footer').after(".$delete_confirmation_box.");
        		}
        		</script>
        		";
       		$script = '';
			$list.=$script.$delete_confirmation_box;
			//$i++;

        }
        return $list;
	}
	
	//funtion delete imagaes product
	function delete_confirmation_product($id,$post_index,$msg,$url,$close_frameid,$var='',$var_no=''){

		/*if(empty($var))
			echo $var[$post_index]="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			echo "test ".$var='';
		else
			echo "value ".$var=$var;*/
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 15px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\" style=\"padding-top:3px\">Yes</button>";
						//$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\" style=\"padding-top:3px\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						thaUrl = '$url';
						var id = $id;
						jQuery.post(thaUrl,{ 
				                pKEY:'set_delete_product',
				                val:id
					    },function(data){
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    $('#product_images_list_".$id."').remove();
				        });
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		$delbox.="</div>";
		
		return $delbox;
	}
	
	//this function for check image in db with idproduct
	function checkimages($idproduct,$product_index=1){
			global $db;
			$sql=$db->prepare_query("select * from lumonata_attachment
	                where
	                larticle_id = %d order by lattach_id Desc", $idproduct);
	    	$r=$db->do_query($sql);
	        if($db->num_rows($r) > 0){
		    	return product_images_list($r,$product_index);
	       	}else{
	           //do nothink
	        }	
	
	}
	
	//this function to send ajax virtual page
   function product_send_to_ajax(){
		add_actions('is_use_ajax', true);

		global $db;
		if (isset($_POST['pKEY']) and $_POST['pKEY']=='product_images'){
			$sql=$db->prepare_query("select * from lumonata_attachment
	                where
	                (larticle_id = %s)
	              	order by lorder","%".$_POST['s']."%");
	    	$r=$db->do_query($sql);
	        if($db->num_rows($r) > 0){
		    	//echo room_type_list($r);
	       	}else{
	            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
	        }
		}
   }
   
   add_actions('product-send-to-ajax_page','product_send_to_ajax');
   
   function edit_product($post_id=0,$type,$title){
   		global $thepost,$db;
        $index=0;
        $button="";
        //set template plugin
        set_products_template();

        if(is_admin_application())
            $url=get_application_url($type);
        else
            $url=get_state_url($type);
        
         if(!is_contributor())
            $button.="<li>".button("button=add_new",get_state_url('shoppingcart&sub=products')."&prc=add_new")."</li>
            <li>".button("button=save_draft")."</li>
            <li>".button("button=publish")."</li>
            <li>".button("button=cancel",get_state_url('shoppingcart&sub=products'))."</li>";
                        
        else
            $button.="<li>".button("button=add_new",get_state_url('shoppingcart&sub=products')."&prc=add_new")."</li>
            <li>".button("button=save_draft&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('shoppingcart&sub=products'))."</li>";
        
        //set the page Title
	   $thetitle=explode("|",$title);
	        if(isset($_POST['select']) && count($_POST['select'])>1){
	            if(count($thetitle)==2)
	                $thetitle=$thetitle[1];
	            else
	                $thetitle=$thetitle[0];
	        }else{
	            $thetitle=$thetitle[0];
	        }
            
        add_actions('section_title',$thetitle.' - Edit');
        add_variable('app_title',"Edit ".$thetitle);

   if(is_edit_all()){
   	//for edit all 
            foreach($_POST['select'] as $index=>$post_id){
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;

                $data_articles=fetch_product("id=".$post_id."&type=".$type);
                
                add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($data_articles['larticle_content']),$post_id));
                add_variable('title',rem_slashes($data_articles['larticle_title']));

				$args=array($index,$post_id,$type);
				add_variable('is_edit_all',"<input type=\"hidden\" name=\"edit\" value=\"Edit\">");
                add_variable('group',$type);
                add_variable('i',$index);
                
                //Categories
                $selected_categories=find_selected_rules($post_id,'categories',$type);
               
                add_variable('all_categories',all_categories($index,'categories',$type,$selected_categories));
                add_variable('most_used_categories',get_most_used_categories($type,$index,$selected_categories));
                if(is_editor() || is_administrator()){
                    add_variable('add_new_category',article_new_category($index,'categories',$type));
                }
                
                //Tags
                add_variable('all_tags',get_post_tags($post_id,$index,$type));
                add_variable('most_used_tags',get_most_used_tags($type,$index));
                add_variable('add_new_tag',add_new_tag($post_id,$index));

                add_variable('prices_setting',additional_data_product("Pricing and Stock Tracking","price_setting",true,$args));
                add_variable('variants_setting',additional_data_variant("Variant",'variant_setting',true,$args));
                add_variable('add_images',add_images("Upload Images","upload_images",true,$args));
                add_variable('product_options',add_product_options("Product Options","product_options",true,$args)); 
                //Get The Permalink
                if(is_permalink()){
                    if(isset($_POST['sef_box'][$index]))
                        $sef=$_POST['sef_box'][$index];
                    else 
                        $sef=$data_articles['lsef'];
                        
                    $_POST['index']=$index;
                    $_POST['type']=$type;
                   
                    if(strlen($sef)>50)$more="...";else $more="";
                    
                    $sef_scheme="<div id=\"sef_scheme_0\">";
                    if(strlen($sef)>50)$more="...";else $more="";
                    
                    $sef_scheme="<div id=\"sef_scheme_0\">";
                    $sef_scheme.="<strong>Permalink:</strong> 
                            	  http://".site_url()."/".$_POST['type']."/
                            	  <span id=\"the_sef_".$_POST['index']."\">
                        			  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
                                          substr($sef,0,50).$more.
                                      "
                                      </span>
                                      .html
                                      <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
                                  </span>
                                  <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
                                  <span>
                                  	<input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" />
                                  	.html <input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
                                  </span>
                                  </span>
                                  
                                  <script type=\"text/javascript\">
                                  		$('#the_sef_".$_POST['index']."').click(function(){
                                  			$('#the_sef_".$_POST['index']."').hide();
                                  			$('#sef_box_".$_POST['index']."').show();
                                  			
                        				});
                        				$('#edit_sef_".$_POST['index']."').click(function(){
                                  			$('#the_sef_".$_POST['index']."').hide();
                                  			$('#sef_box_".$_POST['index']."').show();
                        				});
                        				$('#done_edit_sef_".$_POST['index']."').click(function(){
                        					
                        					var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
                        					if(new_sef.length>50)
                        						var more='...'
                        					else
                        						var more='';
                        					
                                  			$('#the_sef_".$_POST['index']."').show();
                                  			$('#sef_box_".$_POST['index']."').hide();
                                  			$.post('articles.php',
                                  			{ 'update_sef' 	: 'true',
                 							  'post_id' 	: ".$post_id.",
                 							  'type' 		: '".$type."',
                 							  'title' 		: $('input[name=title[".$index."]]').val(),
                 							  'new_sef'	 	: new_sef },
                                  			function(theResponse){
                                  				if(theResponse=='BAD'){
	                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
	                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
	                    						}else if(theResponse=='OK'){
	                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
	             								}
                 							});
                 							
                        				});
                                  </script>"; 
                        $sef_scheme.="</div>";
                }
                add_variable("sef_scheme", $sef_scheme);
               
                //add_variable('share_to', additional_data("Share To", "article_share_option", true,$args));
                //add_variable('comments_settings',additional_data("Comments","discussion_settings",false,$args));
                //add_variable('i',$index);
               	// add_variable('is_edit_all',"<input type=\"hidden\" name=\"edit\" value=\"Edit\">");
               	
                $the_code=fetch_additional_product("id=".$post_id."&type=products");
		        $obj = json_decode($the_code['lvalue'],true);
			 	if(!empty($obj['code'])){
			 		$code=$obj['code'];
			 	}else{
			 		$code='';
			 	}
		 		add_variable('product_code', $code);
	     		$thaUrl= 'http://'.SITE_URL.'/product_ajax';
	     		add_variable('urlcss', 'http://'.SITE_URL.'/');
	     		add_variable('thaUrl', $thaUrl);
	     		add_variable('upload_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/ajaxupload.js\"></script>");
	            add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />");
	            
                
                add_variable('additional_data',attemp_actions('page_additional_data_'.$index));
                parse_template('loopPage','lPage',true);
            }
           
        }else{
        		//Is single edit  
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;
				
                
                if(is_save_draft() || is_publish()){
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($_POST['post'][$index]),$post_id,0));
                    add_variable('title',rem_slashes($_POST['title'][$index]));
                    $selected_categories=find_selected_rules($post_id,'categories',$type);
                }else{
                    $data_articles=fetch_product("id=".$post_id."&type=".$type);
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($data_articles['larticle_content']),$post_id,false));
                    add_variable('title',rem_slashes($data_articles['larticle_title']));
                    
                     //Selected Categories
                    $selected_categories=find_selected_rules($post_id,'categories',$type);
                    
                }
					$args=array($index,$post_id);
					
                //Get The Permalink
                if(is_permalink()){
                    if(isset($_POST['sef_box'][$index]))
                        $sef=$_POST['sef_box'][$index];
                    else 
                        $sef=$data_articles['lsef'];
                        
                    $_POST['index']=$index;
                   
                    if(strlen($sef)>50)$more="...";else $more="";
                    
                    $sef_scheme="<div id=\"sef_scheme_0\">";
                    $sef_scheme.="<strong>Permalink:</strong> 
                              http://".site_url()."/
                              <span id=\"the_sef_".$_POST['index']."\">
                        		  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
                                          substr($sef,0,50).$more.
                                      "
                                      </span>
                                      /
                                      <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
                                  </span>
                                  <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
                                  <span>
                                  <input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" /> /
                                  <input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
                                  </span>
                                  </span>
                                  
                                  <script type=\"text/javascript\">
                                  	$('#the_sef_".$_POST['index']."').click(function(){
                                  		$('#the_sef_".$_POST['index']."').hide();
                                  		$('#sef_box_".$_POST['index']."').show();
                                  		
                        			});
                        			$('#edit_sef_".$_POST['index']."').click(function(){
                                  		$('#the_sef_".$_POST['index']."').hide();
                                  		$('#sef_box_".$_POST['index']."').show();
                        			});
                        			$('#done_edit_sef_".$_POST['index']."').click(function(){
                        				                   						
                        				var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
                        				if(new_sef.length>50)
                        					var more='...'
                        				else
                        					var more='';
                        				
                                  		$('#the_sef_".$_POST['index']."').show();
                                  		$('#sef_box_".$_POST['index']."').hide();
                                  		$.post('articles.php',
                                  		{ 'update_sef' 	: 'true',
                 						  'post_id' 	: ".$post_id.",
                 						  'type' 		: 'pages',
                 						  'title' 		: $('input[name=title[".$index."]]').val(),
                 						  'new_sef'	 	: new_sef },
                                  		function(theResponse){
                                  			if(theResponse=='BAD'){
                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
                    						}else if(theResponse=='OK'){
                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
             								}
                 						});
                 						
                 						
                        			});
                                  </script>"; 
                        $sef_scheme.="</div>";
                }
                add_variable("sef_scheme", $sef_scheme);

			// Select Stok
            /*$sql_stock= $db->prepare_query("select * from lumonata_additional_fields where lapp_id=%s and lkey=%s and lapp_name=%s",$post_id,'product_additional','products');
            $result_stok=$db->do_query($sql_stock);
            $data_stok=$db->fetch_array($result_stok);
            $obj_stok = json_decode($data_stok['lvalue'],true);
            $the_product_stock=$obj_stok['price'];*/
            $the_code=fetch_additional_product("id=".$post_id."&type=products");
	        $obj = json_decode($the_code['lvalue'],true);
		 	if(!empty($obj['code'])){
		 		$code=$obj['code'];
		 	}else{
		 		$code=0;
		 	}
		 	add_variable('product_code', $code);
            add_variable('group',$type);
     		$thaUrl= 'http://'.SITE_URL.'/product_ajax';
     		add_variable('urlcss', 'http://'.SITE_URL.'/');
     		add_variable('thaUrl', $thaUrl);
     		add_variable('upload_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/ajaxupload.js\"></script>");
            add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />");
                               
			$images_save="http://".SITE_URL."/lumonata-plugins/shopping_cart/uploads/product/thumbnail/";
			add_variable('gambar', $images_save);
     		add_variable('urlcss', 'http://'.SITE_URL.'/');
     		add_variable('upload_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/ajaxupload.js\"></script>");  
     		add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />");
        	add_variable('i',$index);
     		
     		//Categories
            add_variable('all_categories',all_categories($index,'categories',$type,$selected_categories));
            add_variable('most_used_categories',get_most_used_categories($type,$index,$selected_categories));
            if(is_editor() || is_administrator()){
                add_variable('add_new_category',article_new_category($index,'categories',$type));
            }
            
            //Tags
            add_variable('all_tags',get_post_tags($post_id,$index,$type));
            add_variable('most_used_tags',get_most_used_tags($type,$index));
            add_variable('add_new_tag',add_new_tag($post_id,$index));
                
            //add_variable('additional_data',attemp_actions('page_additional_data'));
            add_variable('prices_setting',additional_data_product("Pricing and Stock Tracking","price_setting",true,$args));
	        add_variable('variants_setting',additional_data_variant("Variant",'variant_setting',true,$args));
	        //add_variable('additional_data',attemp_actions('page_additional_data'));
	        add_variable('add_images',add_images("Upload Images","upload_images",true,$args));
	        add_variable('product_options',add_product_options("Product Options","product_options",true,$args));
	        add_variable('button',$button);
	        add_variable('upload_button',upload_button());
	        
            parse_template('loopPage','lPage',false);
           
        }
        
       
        add_variable('button',$button);
        return return_page_template();
   }
   
   //	function to take value of product in table article
    function fetch_product($args='',$fetch=true){
        global $db;
        $var_name['title']='';
        $var_name['id']='';
        $var_name['type']='products';

        if(!empty($args)){
            $args=explode('&',$args);
            foreach($args as $val){
                list($variable,$value)=explode('=',$val);
                if($variable=='title' || $variable=='id' || $variable=='type')
               	$var_name[$variable]=$value;
                
            }
        }
        $w="";
        if(isset($_COOKIE['user_type']) && isset($_COOKIE['user_type']))
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }

        if(!empty($var_name['title']) && !empty($var_name['id'])){
            $sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d",$var_name['type'],$var_name['id']);
        }elseif(!empty($var_name['id']) && empty($var_name['title'])){
            $sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d",$var_name['type'],$var_name['id']);
        }elseif(!empty($var_name['title']) && empty($var_name['id'])){
            $sql=$db->prepare_query("SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_title=%d",$var_name['type'],$var_name['title']);
        }else{
        	$sql=$db->prepare_query("SELECT * from lumonata_articles WHERE $w larticle_type=%s",$var_name['type']);
        }
        
        $r=$db->do_query($sql);
        
        if($fetch==true)
            return $db->fetch_array($r);
        
        return $r;
    }
    
    //	function to take the value of product additional field 
    function fetch_additional_product($args='',$fetch=true){
        global $db;
        $var_name['title']='';
        $var_name['id']='';
        $var_name['type']='products';
        
        if(!empty($args)){
            $args=explode('&',$args);
            foreach($args as $val){
                list($variable,$value)=explode('=',$val);
                if($variable=='title' || $variable=='id' || $variable=='type')
                $var_name[$variable]=$value;
                
            }
        }
        $w="";
        if(isset($_COOKIE['user_type']) && isset($_COOKIE['user_type']))
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }
        
    	if(!empty($var_name['id'])){
            $sql= $db->prepare_query("select * from lumonata_additional_fields where lapp_id=%s and lkey=%s and lapp_name=%s",$var_name['id'],'product_additional',$var_name['type']);
        }
        
        $r=$db->do_query($sql);
        
        if($fetch==true)
            return $db->fetch_array($r);
        
        return $r;
    }
    
    //	function to take value of variant Product in addtional fields
    function fetch_variant($args='',$fetch=true){
        global $db;
        $var_name['title']='';
        $var_name['id']='';
        $var_name['type']='products';
        
        if(!empty($args)){
            $args=explode('&',$args);
            foreach($args as $val){
                list($variable,$value)=explode('=',$val);
                if($variable=='title' || $variable=='id' || $variable=='type')
                $var_name[$variable]=$value;
                
            }
        }
        $w="";
        if(isset($_COOKIE['user_type']) && isset($_COOKIE['user_type']))
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }
        
    	if(!empty($var_name['id'])){
            $sql= $db->prepare_query("select * from lumonata_additional_fields where lapp_id=%s and lkey=%s and lapp_name=%s",$var_name['id'],'product_variant',$var_name['type']);
        }
        
        $r=$db->do_query($sql);
        
        if($fetch==true)
            return $db->fetch_array($r);
        
        return $r;
    }
    
 function get_attachment_id($article_id=0){
    global $db;
    
	$sql=$db->prepare_query("select * from lumonata_attachment where larticle_id=%d",$article_id);
	$sql_query=$db->do_query($sql);
	
    return $db->fetch_array($sql_query);
 }
   
function delete_confirmation_box_product($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						thaUrl = '$url';
						var id = $id;

						jQuery.post(thaUrl,{ 
				                pKEY:'delete_product',
				                val:id
					    },function(data){
						  	$('#delete_confirmation_wrapper_".$id."').hide('fast');
				    		$('#".$close_frameid."').css('background','#FF6666');
				    		$('#".$close_frameid."').delay(500);
				    		$('#".$close_frameid."').fadeOut(700);
				    		setTimeout(
					    		function(){
					  				location.reload(true);
	                    		}, 1500);
				        	});
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		
		return $delbox;
	}


//tabs categories 	
function get_product_categories_rule($rule,$group,$thetitle,$tabs,$subsite='arunna'){
        
        //Publish or Save Draft Actions
        if(is_contributor() || is_author())
            $tabs=$tabs[0];
        else
            $tabs=$tabs;
             
        /*
            Configure the tabs
            $the_tab is the selected tab
        */
        $tabb='';
        $tab_keys=array_keys($tabs);
        if(empty($_GET['tab']))
                $the_tab=$tab_keys[0];
        else
                $the_tab=$_GET['tab'];
        
        $tabs=set_tabs($tabs,$the_tab);
        add_variable('tab',$tabs);
        /*
            Filter the tabs. Articles tab can be accessed by all user type.
            But for Categories and Tags tabs only granted for Editor and Administrator.
        */
        
        if(is_publish()){
            //Hook actions defined
            run_actions($rule.'_save');
           
            if(is_add_new()){
                //Hook Add New Actions
                
                //save the taxonomy
                    $parent=$_POST['parent'][0];
                    
                if(insert_rules($parent,$_POST['name'][0],$_POST['description'][0],$rule,$group,false,$subsite)){
                    $rule_id=mysql_insert_id();
					attachment_sync($_POST['post_id'],$rule_id);
                	if(isset($_POST['the_id_images'][0])){
							$image_id=$_POST['the_id_images'][0];
							add_additional_field($rule_id,'categories_image_thumbnail',$image_id,'categories');
					}
					
					if(is_numeric($_POST['min_price'][0])){
						$min_price=$_POST['min_price'][0];
					}else{
						$min_price=0;
					}
					add_additional_field($rule_id, 'minimum_price', $min_price, 'categories_price');
                    
                    if(is_admin_application())
                        header("location:".get_application_url($group)."&tab=".$rule."&prc=add_new");
                    else    
                        header("location:".get_state_url($group)."&tab=".$rule."&prc=add_new");
                    }    
                
            }elseif(is_edit()){
                //Hook Single Edit Actions
                run_actions($rule.'_edit');
                
                
                //Update the article
                update_rules($_POST['rule_id'][0],$_POST['parent'][0],$_POST['name'][0],$_POST['description'][0],$rule,$group,$subsite);
                attachment_sync($_POST['post_id'],$_POST['rule_id'][0]);
                
              	delete_additional_field($_POST['rule_id'][0], 'categories');
              	
            	if(is_numeric($_POST['min_price'][0])){
					$min_price=$_POST['min_price'][0];
				}else{
					$min_price=0;
				}
              	
                if(isset($_POST['the_id_images'])){
					$image_id=$_POST['the_id_images'][0];
					add_additional_field($_POST['rule_id'][0],'categories_image_thumbnail',$image_id,'categories');
				}
                
                edit_additional_field($_POST['rule_id'][0], 'minimum_price', $min_price, 'categories_price');
            }elseif(is_edit_all()){
                run_actions($rule.'_editall');
	
                foreach($_POST['rule_id'] as $index=>$value){
                    update_rules($_POST['rule_id'][$index],$_POST['parent'][$index],$_POST['name'][$index],$_POST['description'][$index],$rule,$group,$subsite);
	                
                    delete_additional_field($_POST['rule_id'][$index], 'categories');
                    
	                if(is_numeric($_POST['min_price'][$index])){
						$min_price=$_POST['min_price'][$index];
					}else{
						$min_price=0;
					}
                     edit_additional_field($_POST['rule_id'][$index], 'minimum_price', $min_price, 'categories_price');
	                   
	               if(isset($_POST['the_id_images'][$index])){
						$image_id=$_POST['the_id_images'][$index];
						add_additional_field($_POST['rule_id'][$index],'categories_image_thumbnail',$image_id,'categories');
					}
                }
                
                
            }
        }
       
       //Automatic to add new when there is no records on tag rule database
       $count_cond='rule='.$rule."&group=".$group;
            
        if(count_rules($count_cond)==0 && !isset($_GET['prc'])){
            if($rule=="tags"){
                 if(isset($_GET['state']) && $_GET['state']=='applications')
					header("location:".get_application_url($group)."&tab=tags&prc=add_new");                  
                 else    
                	header("location:".get_state_url($group)."&tab=tags&prc=add_new");
                    
            }elseif($rule=="categories"){
                if(count_rules('rule='.$rule."&group=default")==0)
                    insert_rules(0,"Uncategorized","","category",'default',true);
            }
        }
        
        //Is add new Rule, View the desain
        if(is_add_new()){
            return add_new_categories_rule($rule,$group,$thetitle) ;
        }elseif(is_edit()){
            return edit_rule_categories($rule,$group,$thetitle,$_GET['id']);
        }elseif(is_edit_all() && isset($_POST['select'])){
            return edit_rule_categories($rule,$group,$thetitle);
        }elseif(is_delete_all()){
            $thetitle=explode("|",$thetitle);   
            $warning="<form action=\"\" method=\"post\">";
            if(count($_POST['select'])==1){
                    $rule=($rule=="categories")?"Category":"Tag";
                    add_actions('section_title','Delete '.$thetitle[0]." ".$rule);
                    $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this ".$thetitle[0]." ".$rule.":</strong>";
            }else{
                    $rule=($rule=="categories")?"Categories":"Tags";
                    add_actions('section_title','Delete '.$thetitle[1]." ".$rule);
                    $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these ".$thetitle[0]." ".$rule.":</strong>";
            }        
            $warning.="<ol>";	
            foreach($_POST['select'] as $key=>$val){
                    $d=count_rules("rule_id=".$val,false);
                    $warning.="<li>".$d['lname']."</li>";
                    $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$d['lrule_id']."\">";
            }
            $warning.="</ol></div>";
            $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
            $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
            if(is_admin_application())
            	$warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_application_url($group)."'\" />";
            else
	            $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url($group)."'\" />";
	            $warning.="</div>";
	            $warning.="</form>";
            return $warning;
        }elseif(is_confirm_delete()){
            foreach($_POST['id'] as $key=>$val){
                delete_rule($val);
                delete_additional_field($val, 'categories');
             	while($data=get_attachment_id($val)){
                  	delete_attachment($data['lattach_id']);
               	}
            }
        }
        
        //Display Users Lists
        //if(count_rules('rule='.$rule."&group=".$group)>0){
                add_actions('header_elements','get_javascript','jquery_ui');
                add_actions('header_elements','get_javascript','articles_list');
                return get_rule_list_categories($rule,$group,$thetitle,$tabs);
        //}
         
    }
    
   
    
    function get_rule_list_categories($rule,$type,$title,$tabs){
        global $db;
        $list='';
        $option_viewed="";
        $data_order=array('asc'=>'Ascending','desc'=>'Descending');
        foreach($data_order as $key=>$val){
            if(isset($_POST['data_order'])){
                if($_POST['data_order']==$key){
                    $option_viewed.="<input type=\"radio\" name=\"data_order\" value=\"$key\" checked=\"checked\" />$val";
                }else{
                    $option_viewed.="<input type=\"radio\" name=\"data_order\" value=\"$key\"  />$val";
                }
            }elseif($key=='asc'){
                $option_viewed.="<input type=\"radio\" name=\"data_order\" value=\"$key\" checked=\"checked\"  />$val";
            }else{
                $option_viewed.="<input type=\"radio\" name=\"data_order\" value=\"$key\"  />$val";
            }
        }
        //setup paging system
        if(is_admin_application())
            $url=get_application_url($type)."&tab=".$rule."&page=";
        else
            $url=get_state_url($type)."&tab=".$rule."&page=";
        
                
        if(is_search()){
                //if($rule=='tags')
                //    $sql=$db->prepare_query("select * from lumonata_rules where lrule=%s and (lname like %s or ldescription like %s)",$rule,"%".$_POST['s']."%","%".$_POST['s']."%");
                //else
                    $sql=$db->prepare_query("select * from lumonata_rules where lrule=%s and (lname like %s or ldescription like %s) and lgroup=%s",$rule,"%".$_POST['s']."%","%".$_POST['s']."%",$type);
                $num_rows=count_rows($sql);
        }else{
                //if($rule=='tags')
                //    $where=$db->prepare_query("WHERE lrule=%s AND lgroup=%s",$rule);
                //else
                   	$where=$db->prepare_query("WHERE lrule=%s AND lgroup=%s",$rule,$type);
                    
                $num_rows=count_rows("select * from lumonata_rules $where ");
        }
        
        $viewed=list_viewed();
        if(isset($_GET['page'])){
            $page= $_GET['page'];
        }else{
            $page=1;
        }
        
        $limit=($page-1)*$viewed;
        if(is_search()){
            //if($rule=='tags')
            //    $sql=$db->prepare_query("select * from lumonata_rules where lrule=%s and (lname like %s or ldescription like %s) limit %d, %d",$rule,"%".$_POST['s']."%","%".$_POST['s']."%",$limit,$viewed);
            //else
                $sql=$db->prepare_query("select * from lumonata_rules where lrule=%s and (lname like %s or ldescription like %s) and lgroup=%s limit %d, %d",$rule,"%".$_POST['s']."%","%".$_POST['s']."%",$type,$limit,$viewed);
        }else{
                if(isset($_POST['data_order'])){
                    $order_by=$_POST['data_order'];
                }else{
                    $order_by="";
                }
                $sql=$db->prepare_query("select * from lumonata_rules $where order by lorder $order_by limit %d, %d",$limit,$viewed);
        }
        $result=$db->do_query($sql);
       
        $start_order=($page - 1) * $viewed + 1; //start order number
        $addnew_url=(is_admin_application())?get_application_url($type)."&tab=".$rule:get_state_url($type)."&tab=".$rule;
        
        $button="<li>".button("button=add_new",$addnew_url."&prc=add_new")."</li>
                <li>".button('button=edit&type=submit&enable=false')."</li>
                <li>".button('button=delete&type=submit&enable=false')."</li>";
        
        
        $title=explode("|",$title);
        if($num_rows>1){
            if(count($title)==2)
                $title=$title[1];
            else
                $title=$title[0];
        }else{
            $title=$title[0];
        }
        $list.="<h1>$title ".ucwords($rule)."</h1>
                <ul class=\"tabs\">$tabs</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"\" method=\"post\" name=\"alist\">
                            <div class=\"button_right\">
                                ".search_box('taxonomy.php','list_taxonomy','state='.$_GET['state'].'&rule='.$rule.'&group='.$type.'&prc=search&','right','alert_green_form')."
                            </div>
                            <br clear=\"all\" />	
                           <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
                           <input type=\"hidden\" name=\"state\" value=\"".$type."\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                                $button
                                        </ul>
                                </div>
                            </div>
                            <div class=\"status_to_show\">Ordering: $option_viewed</div>
                            <div class=\"list\">
                                <div class=\"list_title\">
                                    <input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" />
                                    <div class=\"rule_parent\">Parent</div>
                                    <div class=\"rule_name\">Name</div>
                                    <div class=\"rule_description\">Description</div>
                                    <!--div class=\"list_order\">Order</div -->
                                </div>
                                <div id=\"list_taxonomy\">";
                                $list.=rule_list_category($result,$type,$rule,$start_order);
                $list.="	</div>
                        
                        </div>
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                      </form>
                        <div class=\"paging_right\">
                        	". paging($url,$num_rows,$page,$viewed,5)."
                        </div>
                </div>
            <script type=\"text/javascript\" language=\"javascript\">
                
                
            </script>";
            
        add_actions('section_title',$title);
        return $list;
    }
    
   function rule_list_category($result,$type,$rule,$i=1){
        global $db;
        $list='';
        
        if(is_admin_application())
            $url=get_application_url($type)."&tab=".$rule;
        else
            $url=get_state_url($type)."&tab=".$rule;
            
        if($db->num_rows($result)==0 && isset($_POST['s']))
            return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        elseif($db->num_rows($result)==0)
            return "<div class=\"alert_yellow_form\">No record on database</div>";
        
        while($d=$db->fetch_array($result)){
            $parent=get_rule_structure($d['lrule_id'],$d['lrule_id']);    
            $list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['lrule_id']."\">
                            <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['lrule_id']."\" />
                            <div class=\"rule_parent\" >".$parent."</div>
                            <div class=\"rule_name\">".$d['lname']."</div>
                            <div class=\"rule_description\">".$d['ldescription']."</div>
                            <!--div class=\"list_order\" --><input type=\"hidden\" value=\"$i\" id=\"order_".$d['lrule_id']."\" class=\"small_textbox\" name=\"order[".$i."]\"><!-- /div -->
                            <div class=\"the_navigation_list\">
                                <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['lrule_id']."\">
                                        <a href=\"".$url."&prc=edit&id=".$d['lrule_id']."\">Edit</a> |
                                        <a href=\"#\" rel=\"delete_".$d['lrule_id']."\">Delete</a>
                                </div>
                            </div>
                            
                            <script type=\"text/javascript\" language=\"javascript\">
                                    $('#theitem_".$d['lrule_id']."').mouseover(function(){
                                            $('#the_navigation_".$d['lrule_id']."').show();
                                    });
                                    $('#theitem_".$d['lrule_id']."').mouseout(function(){
                                            $('#the_navigation_".$d['lrule_id']."').hide();
                                    });
                            </script>
                     </div>";
            
	        $thaUrl= 'http://'.SITE_URL.'/product_ajax';
            //delete_confirmation_box($d['lrule_id'],"Are sure want to delete ".$d['lname']."?","taxonomy.php","theitem_".$d['lrule_id'],'state='.$rule.'&prc=delete&id='.$d['lrule_id'])
            add_actions('admin_tail','delete_confirmation_box_categories',$d['lrule_id'],"Are sure want to delete ".$d['lname']."?",$thaUrl,"theitem_".$d['lrule_id'],'state='.$rule.'&prc=delete&id='.$d['lrule_id']);
            $i++;
        }
        return $list;
    }

    function article_new_category_product($index,$rule_name,$group){
        $return="<div class=\"add_new_cattags\">
                    <input type=\"text\" class=\"category_textbox\" name=\"new_category[$index]\" value=\"New category name\" />
                    <div style=\"margin:0 0 2px 0;\" id=\"select_parent_$index\">
                        <select name=\"parent[$index]\" >
                            <option value=\"0\">Parent</option>
                            ".recursive_taxonomy($index,$rule_name,$group,'select')."
                        </select>
                    </div>
                    <div class=\"add_cattags_button\">
                        <input type=\"button\" name=\"add_category_btn[$index]\" value=\"Add\" class=\"button\" />
                    </div>
                </div>";
                
        return $return;
    }
    
     function add_new_categories_rule($rule,$group,$title){
        //$args=array($index=0,$post_id);
        set_rule_template();
        
        if(is_admin_application())
            $url=get_application_url($group)."&tab=".$rule;
        else
            $url=get_state_url($group)."&tab=".$rule;
        
        $thetitle=explode("|",$title);
        $thetitle=$thetitle[0];
        add_variable('app_title',"Add New ".$thetitle." ".ucwords($rule));
       
        $button="";
        if(!is_contributor() && !is_author()){
            $button.="<li>".button("button=add_new",$url."&prc=add_new")."</li>
            <li>".button("button=publish&label=Add")."</li>
            <li>".button("button=cancel",$url)."</li>";
                        
        }
        
        //set the page Title
        add_actions('section_title',$thetitle.' - Add New '.ucwords($rule));
        if($rule=="categories"){
        	$parent='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/ajaxupload.js"></script>
        			<script>
        			
        				function trim(str) {
							var	str = str.replace(/^\s\s*/, ""),
								ws = /\s/,
								i = str.length;
							while (ws.test(str.charAt(--i)));
							return str.slice(0, i + 1);
						}
						
						function get_product_images_c(post_index){
						
						   	var thaUrl 	= "http://'.SITE_URL.'/product_ajax";
						   	jQuery.post(thaUrl,{ 
						   			pKEY:"get_category_images",
						            category_index:post_index
						       },function(data){
							       	if(trim(data)!="error"){
							       		jQuery("#key_product_images_Up").after(data);
							       		return_colorbox();
							       	}else{
							        	//alert("Error");
							       	}
						       });
						}
						
						function return_colorbox (){
					       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
							}

        				$(document).ready(function(){
							var button = $("#buttonUp"), interval;
							var post_index = $("#buttonUp").attr("rel");
							
		    				var thaUpload = new AjaxUpload(button, {
			 				action: "http://'.SITE_URL.'/product_ajax", 
			 				name: "upload_categories_images",
			 			onSubmit : function(file, ext){
			 				$("#loaderuP").css("display","");
				             thaUpload.setData({ 
				            	category_id:post_index
				             })
		
			 			},
			 			onComplete: function(file, response){
			 				if(trim(response)=="success"){
			 					$("#loaderuP").css("display","none")
			 					get_product_images_c(post_index);
								
			 				}else{
			 					//NOTHINK TO DO
			 				}	
			 			}
			 		}); 
			 			})
					</script>';
            $parent.="<style>div.button {
							height: 20px;	
							width: 89px;
							font-size: 12px;
				    		font-weight: bold;
							text-align: center; padding-top: 10px;
						}</style>
				<fieldset>
				<input type='hidden' name='post_id' value='".time()."'> 
                   </p>Parent :<p>
                   <p>
                        <select name=\"parent[0]\" class=\"big\">
                           <option value=\"0\">Parent</option>
                           ".recursive_taxonomy(0,$rule,$group)."
                        </select>
                    </p>
                </fieldset>
                <fieldset>
                	</p>Minimum Order :<p>
                	<p>
                		<input type='text' class='small_textbox' name='min_price[]' value=''> ".get_symbol_currency()."
                	</p>
                </fieldset>
                <fieldset>
                 <p><strong>Upload Images :</strong></p>
						<span id=\"example2\" class=\"example2\">
							<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
								<div id=\"buttonUp\" class=\"button\" rel=\"".time()."\">Upload</div>
							</p>
						</span>
						<p><span id=\"loaderuP\"  style=\"display:none\">
							<img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\">
						</span></p>
						<div class=\"ui-sortable-Up\" id=\"list_item_Up\">
		   				<div class=\"key_product_image_Up\" id=\"key_product_images_Up\" style=\"padding: 0px;width:99%;\"></div>
		   			</div>
                </fieldset>";
            add_variable('parent',$parent);
        }
        add_variable('button',$button);
        parse_template('loopRule','lRule',false);
        return return_rule_template();
        
    }
    
    
    function categories_images_list($result,$category_index,$i=0){
        global $db;
        $list='';
        $theImages='';
        $theIdImages='';
        while($d=$db->fetch_array($result)){
        	$theImages=$d['lattach_loc_thumb'];
        	$theIdImages=$d['lattach_id'];
        	$sql_image=$db->prepare_query("select
				lattach_id,
				larticle_id,
				lattach_loc,
				lattach_loc_thumb,
				lattach_loc_medium,
				lattach_loc_large,
				ltitle 
				 from 
				lumonata_attachment,lumonata_additional_fields 
				where 
				lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
				and lumonata_additional_fields.lapp_id=%d and lumonata_additional_fields.lvalue=%s
				and lumonata_additional_fields.lkey='categories_image_thumbnail'",$d['larticle_id'],$theIdImages);
        	$query_images_thumb=$db->do_query($sql_image);
        	if($db->num_rows($query_images_thumb)<>0){
        		$data_images_thumb=$db->fetch_array($query_images_thumb);
        		$checked_image='checked="checked"';
        	}else{
        			$checked_image='';

        	}
        	$url = 'http://'.SITE_URL.$d['lattach_loc_large'];
			$url_thumb = 'http://'.SITE_URL.$theImages;
        	$list.='<div class="list_item clearfix images_list" id="product_images_list_'.$theIdImages.'" style="width:720px;">
                    <div class="list_author" style="width:180px;">
						<img src="'.$url_thumb.'" style="width:100px;">
					</div>
					<div class="list_author" style="float:right;text-align:center;width:250px;padding-top:20px">
                         <a href="" style="cursor: pointer;"> 
                         	<a rel="view_colorbox_elastic" href="'.$url.'">view</a>
                         </a>
                         <span>|
                         	<a href="javascript:;" rel="delete_'.$theIdImages.'">delete</a>
                         </a>
                         </span>
                         <span>|
                         <input type="radio" name="the_id_images['.$i.']" id="the_id_images'.$theIdImages.'" value="'.$theIdImages.'"'.$checked_image.'>Set thumbnail
                         </span>
                     </div>
					</div>';
        	
        	$image_id = $theIdImages;
        	$id = $image_id;
	        $post_index = $category_index;
	        $msg="Are sure want to delete ".$d['ltitle']."?";
	        $thaUrl= 'http://'.SITE_URL.'/product_ajax';
	        $close_frameid = "product_images_list_".$image_id;
	        $var = 'state=pages&prc=delete&id='.$image_id;
	        $var_no='';
      	 	$delete_confirmation_box = delete_confirmation_catagories($id,$post_index,$msg,$thaUrl,$close_frameid,$var='',$var_no='');
	     	
      	 	$script="<script type=\"text/javascript\">
         		function alert_run(){
        			jQuery('.footer').after(".$delete_confirmation_box.");
        		}
        		</script>
        		";
       		$script = '';
			$list.=$script.$delete_confirmation_box;
			if(is_edit_all()){
				$i++;
			}

        }
        return $list;
	}
	
	function delete_confirmation_catagories($id,$post_index,$msg,$url,$close_frameid,$var='',$var_no=''){

		/*if(empty($var))
			echo $var[$post_index]="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			echo "test ".$var='';
		else
			echo "value ".$var=$var;*/
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 15px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\" style=\"padding-top:3px\">Yes</button>";
						//$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\" style=\"padding-top:3px\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						thaUrl = '$url';
						var id = $id;
						jQuery.post(thaUrl,{ 
				                pKEY:'set_delete_categories',
				                val:id
					    },function(data){
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    $('#product_images_list_".$id."').remove();
				        });
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		$delbox.="</div>";
		
		return $delbox;
	}
	
	//edit rule
	 function edit_rule_categories($rule,$group,$title,$rule_id=0){
	 	global $db;
        $index=0;
        $button="";
        set_rule_template();
       
        if(is_admin_application())
            $url=get_application_url($group)."&tab=".$rule;
        else
            $url=get_state_url($group)."&tab=".$rule;
        
        $thetitle=explode("|",$title);
        
        if(isset($_POST['select']) && count($_POST['select'])>1){
            if(count($thetitle)==2)
                $thetitle=$thetitle[1];
            else
                $thetitle=$thetitle[0];
        }else{
            $thetitle=$thetitle[0];
        }
        
        
       if(!is_contributor() && !is_author()){
            $button.="<li>".button("button=add_new",$url."&prc=add_new")."</li>
            <li>".button("button=publish&label=Save")."</li>
            <li>".button("button=cancel",$url)."</li>";
                        
        }
        
        //set the page Title
        add_actions('section_title','Edit - '.$thetitle.' '.ucwords($rule));
        add_variable('app_title',"Edit ".$thetitle." ".ucwords($rule));
        
        if(is_edit_all()){
            foreach($_POST['select'] as $index=>$rule_id){
            	$result='';
                if(is_publish()){
                    if($rule=="categories"){
	                	$sql=$db->prepare_query("select * from lumonata_attachment
				              where
				              larticle_id = %d order by lattach_id Desc", $rule_id);
				    	$r=$db->do_query($sql);
				        if($db->num_rows($r) > 0){
					    	$result=categories_images_list($r,'1',$index);
				       	}
				       	$minimum_price = get_additional_field($rule_id, 'minimum_price', 'categories_price');
				       	$min_price = number_format($minimum_price,2,'.','');
				       	$parent='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/ajaxupload.js"></script>
        			<script>
        			
        				function trim(str) {
							var	str = str.replace(/^\s\s*/, ""),
								ws = /\s/,
								i = str.length;
							while (ws.test(str.charAt(--i)));
							return str.slice(0, i + 1);
						}
						
						function get_product_images_c(post_index){
						
						   	var thaUrl 	= "http://'.SITE_URL.'/product_ajax";
						   	jQuery.post(thaUrl,{ 
						   			pKEY:"get_category_images",
						            category_index:post_index
						       },function(data){
							       	if(trim(data)!="error"){
							       		jQuery("#key_product_images_Up").after(data);
							       		return_colorbox();
							       	}else{
							        	//alert("Error");
							       	}
						       });
						}
						
						function return_colorbox (){
					       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
							}

        				$(document).ready(function(){
							var button = $("#buttonUp"), interval;
							var post_index = $("#buttonUp").attr("rel");
							
		    				var thaUpload = new AjaxUpload(button, {
			 				action: "http://'.SITE_URL.'/product_ajax", 
			 				name: "upload_categories_images",
			 			onSubmit : function(file, ext){
			 				$("#loaderuP").css("display","");
				             thaUpload.setData({ 
				            	category_id:post_index
				             })
		
			 			},
			 			onComplete: function(file, response){
			 				if(trim(response)=="success"){
			 					$("#loaderuP").css("display","none")
			 					get_product_images_c(post_index);
								
			 				}else{
			 					//NOTHINK TO DO
			 				}	
			 			}
			 		}); 
			 			})
					</script>';
                       $parent.="<style>div.button {
									height: 20px;	
									width: 89px;
									font-size: 12px;
						    		font-weight: bold;
									text-align: center; padding-top: 10px;
								}</style>
						<fieldset>
						<input type='hidden' name='post_id' value='".time()."'> 
		                   </p>Parent :<p>
		                   <p>
		                        <select name=\"parent[".$index."]\" class=\"big\">
		                           <option value=\"0\">Parent</option>
		                           ".recursive_taxonomy(0,$rule,$group)."
		                        </select>
		                    </p>
		                </fieldset>
		                <fieldset>
		                	</p>Minimum Price :<p>
		                	<p>
		                		<input type='text' class='small_textbox' name='min_price[]' value=\"".$min_price."\"> ".get_symbol_currency()."
		                	</p>
		                </fieldset>
		                <fieldset>
		                 <p><strong>Upload Images :</strong></p>
								<span id=\"example2\" class=\"example2\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"buttonUp\" class=\"button\" rel=\"".$rule_id."\">Upload</div>
									</p>
								</span>
								<p><span id=\"loaderuP\"  style=\"display:none\">
									<img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\">
								</span></p>
								<div class=\"ui-sortable-Up\" id=\"list_item_Up\">
				   				<div class=\"key_product_image_Up\" id=\"key_product_images_Up\" style=\"padding: 0px;width:99%;\"></div>
				   				".$result."<script>return_colorbox();</script>
				   			</div>
		                </fieldset>";
                            
                        add_variable('parent',$parent);
                    }
                    add_variable('name',$_POST['name'][$index]);
                    add_variable('description',$_POST['description'][$index]);
                    add_variable('rule_id',$rule_id);
                }else{
                	$parent='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/ajaxupload.js"></script>
                		<script>
                			function trim(str) {
							var	str = str.replace(/^\s\s*/, ""),
								ws = /\s/,
								i = str.length;
							while (ws.test(str.charAt(--i)));
							return str.slice(0, i + 1);
						}
						
						function return_colorbox (){
					       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
							}
                		</script>
                		';
                	
                    $d=count_rules("rule_id=".$rule_id,false);
                    if($rule=="categories"){
                      	$sql=$db->prepare_query("select * from lumonata_attachment
				              where
				              larticle_id = %d order by lattach_id Desc", $rule_id);
				    	$r=$db->do_query($sql);
				        if($db->num_rows($r) > 0){
					    	$result=categories_images_list($r,'1',$index);
				       	}
				       	$minimum_price = get_additional_field($rule_id, 'minimum_price', 'categories_price');
				       	$min_price = number_format($minimum_price,2,'.','');
				       	$parent.='<script>      						
						function get_product_images_c_'.$index.'(post_index){
						
						   	var thaUrl 	= "http://'.SITE_URL.'/product_ajax";
						   	jQuery.post(thaUrl,{ 
						   			pKEY:"get_category_images",
						            category_index:post_index
						       },function(data){
							       	if(trim(data)!="error"){
							       		jQuery("#key_product_images_Up_'.$index.'").after(data);
							       		return_colorbox();
							       	}else{
							        	//alert("Error");
							       	}
						       });
						}
        				$(document).ready(function(){
							var button = $("#buttonUp_'.$index.'"), interval;
							var post_index = $("#buttonUp_'.$index.'").attr("rel");
		    				var thaUpload = new AjaxUpload(button, {
			 				action: "http://'.SITE_URL.'/product_ajax", 
			 				name: "upload_categories_images",
			 			onSubmit : function(file, ext){
			 				$("#loaderuP").css("display","");
				             thaUpload.setData({ 
				            	category_id:post_index
				             })
		
			 			},
			 			onComplete: function(file, response){
			 				if(trim(response)=="success"){
			 					$("#loaderuP").css("display","none")
			 					get_product_images_c_'.$index.'(post_index);
								
			 				}else{
			 					//NOTHINK TO DO
			 				}	
			 			}
			 		}); 
			 			})
					</script>';
                       $parent.="<style>div.button {
									height: 20px;	
									width: 89px;
									font-size: 12px;
						    		font-weight: bold;
									text-align: center; padding-top: 10px;
								}</style>
						<fieldset>
						<input type='hidden' name='post_id' value='".time()."'> 
		                   </p>Parent :<p>
		                   <p>
		                        <select name=\"parent[".$index."]\" class=\"big\">
		                           <option value=\"0\">Parent</option>
		                           ".recursive_taxonomy(0,$rule,$group)."
		                        </select>
		                    </p>
		                </fieldset>
		                <fieldset>
		                	</p>Minimum Price :<p>
		                	<p>
		                		<input type='text' class='small_textbox' name='min_price[]' value=\"".$min_price."\"> ".get_symbol_currency()."
		                	</p>
                		</fieldset>
		                <fieldset>
		                 <p><strong>Upload Images :</strong></p>
								<span id=\"example2\" class=\"example2\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"buttonUp_".$index."\" class=\"button\" rel=\"".$rule_id."\">Upload</div>
									</p>
								</span>
								<p><span id=\"loaderuP\"  style=\"display:none\">
									<img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\">
								</span></p>
								<div class=\"ui-sortable-Up\" id=\"list_item_Up\">
				   				<div class=\"key_product_image_Up\" id=\"key_product_images_Up_".$index."\" style=\"padding: 0px;width:99%;\"></div>
				   				".$result."<script>return_colorbox();</script>
				   			</div>
		                </fieldset>";
                        add_variable('parent',$parent);
                    }
                    add_variable('name',$d['lname']);
                    add_variable('description',$d['ldescription']);
                    add_variable('rule_id',$rule_id);
                }
                
                add_variable('is_edit_all',"<input type=\"hidden\" name=\"edit\" value=\"Edit\">");
                parse_template('loopRule','lRule',true);
            }
           
        }else{
        	$result='';
                if(is_publish()){ 	
                    if($rule=="categories"){
	                    $sql=$db->prepare_query("select * from lumonata_attachment
				              where
				              larticle_id = %d order by lattach_id Desc", $rule_id);
				    	$r=$db->do_query($sql);
				        if($db->num_rows($r) > 0){
					    	$result=categories_images_list($r,'1',$index);
				       	}
				       	$minimum_price = get_additional_field($rule_id, 'minimum_price', 'categories_price');
				       	$min_price = number_format($minimum_price,2,'.','');
				       	
                        $parent='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/ajaxupload.js"></script>
        			<script>
        			
        				function trim(str) {
							var	str = str.replace(/^\s\s*/, ""),
								ws = /\s/,
								i = str.length;
							while (ws.test(str.charAt(--i)));
							return str.slice(0, i + 1);
						}
						
						function get_product_images_c(post_index){
						
						   	var thaUrl 	= "http://'.SITE_URL.'/product_ajax";
						   	jQuery.post(thaUrl,{ 
						   			pKEY:"get_category_images",
						            category_index:post_index
						       },function(data){
							       	if(trim(data)!="error"){
							       		jQuery("#key_product_images_Up").after(data);
							       		return_colorbox();
							       	}else{
							        	//alert("Error");
							       	}
						       });
						}
						
						function return_colorbox (){
					       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
							}

        				$(document).ready(function(){
							var button = $("#buttonUp"), interval;
							var post_index = $("#buttonUp").attr("rel");
							
		    				var thaUpload = new AjaxUpload(button, {
			 				action: "http://'.SITE_URL.'/product_ajax", 
			 				name: "upload_categories_images",
			 			onSubmit : function(file, ext){
			 				$("#loaderuP").css("display","");
				             thaUpload.setData({ 
				            	category_id:post_index
				             })
		
			 			},
			 			onComplete: function(file, response){
			 				if(trim(response)=="success"){
			 					$("#loaderuP").css("display","none")
			 					get_product_images_c(post_index);
								
			 				}else{
			 					//NOTHINK TO DO
			 				}	
			 			}
			 		}); 
			 			})
					</script>';
		            $parent.="<style>div.button {
									height: 20px;	
									width: 89px;
									font-size: 12px;
						    		font-weight: bold;
									text-align: center; padding-top: 10px;
								}</style>
						<fieldset>
						<input type='hidden' name='post_id' value='".time()."'> 
		                   </p>Parent :<p>
		                   <p>
		                        <select name=\"parent[0]\" class=\"big\">
		                           <option value=\"0\">Parent</option>
		                           ".recursive_taxonomy(0,$rule,$group)."
		                        </select>
		                    </p>
		                </fieldset>
		                <fieldset>
		                	</p>Minimum Price :<p>
		                	<p>
		                		<input type='text' class='small_textbox' name='min_price[]' value=\"".$min_price."\"> ".get_symbol_currency()."
		                	</p>
		                </fieldset>		                
		                <fieldset>
		                 <p><strong>Upload Images :</strong></p>
								<span id=\"example2\" class=\"example2\">
									<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
										<div id=\"buttonUp\" class=\"button\" rel=\"".time()."\">Upload</div>
									</p>
								</span>
								<p><span id=\"loaderuP\"  style=\"display:none\">
									<img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\">
								</span></p>
								<div class=\"ui-sortable-Up\" id=\"list_item_Up\">
				   				<div class=\"key_product_image_Up\" id=\"key_product_images_Up\" style=\"padding: 0px;width:99%;\"></div>
				   				".$result."<script>return_colorbox();</script>
				   			</div>
		                </fieldset>";
                            
                        add_variable('parent',$parent);
                    }
                    add_variable('name',$_POST['name'][$index]);
                    add_variable('description',$_POST['description'][$index]);
                    add_variable('rule_id',$rule_id);
                }else{
                    if($rule=="categories"){
	                    $d=count_rules("rule_id=".$rule_id,false);
	                    
		                $sql=$db->prepare_query("select * from lumonata_attachment
				              where
				              larticle_id = %d order by lattach_id Desc", $rule_id);
				    	$r=$db->do_query($sql);
				        if($db->num_rows($r) > 0){
					    	$result=categories_images_list($r,'1',$index);
				       	}
				       	
				       	$minimum_price = get_additional_field($rule_id, 'minimum_price', 'categories_price');
				       	$min_price = number_format($minimum_price,2,'.','');
				       	
                        $parent='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/ajaxupload.js"></script>
        			<script>
        			
        				function trim(str) {
							var	str = str.replace(/^\s\s*/, ""),
								ws = /\s/,
								i = str.length;
							while (ws.test(str.charAt(--i)));
							return str.slice(0, i + 1);
						}
						
						function get_product_images_c(post_index){
						
						   	var thaUrl 	= "http://'.SITE_URL.'/product_ajax";
						   	jQuery.post(thaUrl,{ 
						   			pKEY:"get_category_images",
						            category_index:post_index
						       },function(data){
							       	if(trim(data)!="error"){
							       		jQuery("#key_product_images_Up").after(data);
							       		return_colorbox();
							       	}else{
							        	//alert("Error");
							       	}
						       });
						}
						
						function return_colorbox (){
					       	$("a[rel=\'view_colorbox_elastic\']").colorbox();
							}

        				$(document).ready(function(){
							var button = $("#buttonUp"), interval;
							var post_index = $("#buttonUp").attr("rel");
							
		    				var thaUpload = new AjaxUpload(button, {
			 				action: "http://'.SITE_URL.'/product_ajax", 
			 				name: "upload_categories_images",
			 			onSubmit : function(file, ext){
			 				$("#loaderuP").css("display","");
				             thaUpload.setData({ 
				            	category_id:post_index
				             })
		
			 			},
			 			onComplete: function(file, response){
			 				if(trim(response)=="success"){
			 					$("#loaderuP").css("display","none")
			 					get_product_images_c(post_index);
								
			 				}else{
			 					//NOTHINK TO DO
			 				}	
			 			}
			 		}); 
			 			})
					</script>';
            $parent.="<style>div.button {
							height: 20px;	
							width: 89px;
							font-size: 12px;
				    		font-weight: bold;
							text-align: center; padding-top: 10px;
						}</style>
				<fieldset>
				<input type='hidden' name='post_id' value='".time()."'> 
                   </p>Parent :<p>
                   <p>
                        <select name=\"parent[0]\" class=\"big\">
                           <option value=\"0\">Parent</option>
                           ".recursive_taxonomy(0,$rule,$group)."
                        </select>
                    </p>
                </fieldset>
                <fieldset>
                	</p>Minimum Price :<p>
                	<p>
                		<input type='text' class='small_textbox' name='min_price[]' value=\"".$min_price."\"> ".get_symbol_currency()."
                	</p>
                </fieldset>                
                <fieldset>
                 <p><strong>Upload Images :</strong></p>
						<span id=\"example2\" class=\"example2\">
							<p>Images can be .JPG, .GIF or .PNG and must be at least 100x100 pixels in size.</p>
								<div id=\"buttonUp\" class=\"button\" rel=\"".time()."\">Upload</div>
							</p>
						</span>
						<p><span id=\"loaderuP\"  style=\"display:none\">
							<img src=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/images/ajax-loader.gif\" width=\"220\" height=\"19\">
						</span></p>
						<div class=\"ui-sortable-Up\" id=\"list_item_Up\">
		   				<div class=\"key_product_image_Up\" id=\"key_product_images_Up\" style=\"padding: 0px;width:99%;\"></div>
		   				".$result."<script>return_colorbox();</script>
		   			</div>
                </fieldset>";
                            
                        add_variable('parent',$parent);
                    }
                    add_variable('name',$d['lname']);
                    add_variable('description',$d['ldescription']);
                    add_variable('rule_id',$rule_id);
                }
                
                parse_template('loopRule','lRule',false);
           
        }
        
        
        add_variable('button',$button);
        return return_rule_template();
    }
    
    function delete_confirmation_box_categories($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						thaUrl = '$url';
						var id = $id;

						jQuery.post(thaUrl,{ 
				                pKEY:'delete_categories',
				                val:id
					    },function(data){
						  	$('#delete_confirmation_wrapper_".$id."').hide('fast');
				    		$('#".$close_frameid."').css('background','#FF6666');
				    		$('#".$close_frameid."').delay(500);
				    		$('#".$close_frameid."').fadeOut(700);
				    		setTimeout(
					    		function(){
					  				location.reload(true);
	                    		}, 1500);
				        	});
					    return false;
					   
				    	});
			 		});
				";
		$delbox.="</script>";
		
		return $delbox;
	}
	
	function search_box_product($keyup_action='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
		$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
						<div class=\"textwrap\">
						    <input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
						</div>
						<div class=\"buttonwrap\">
						    <input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
						</div>
            	 	</div>
            	 	<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
            	 		<img src=\"". get_theme_img() ."/loader.gif\"  />
            	 	</div>
            	 	";
			    
		if(!empty($keyup_action)){
			$searchbox.="<script type=\"text/javascript\">
				$(function(){
					
					$('.searchtext').keyup(function(){
						
						$('#$results_id').html('<div class=".$class.">Searching...</div>');
						var s = $('input[name=s]').val();
						var parameter='".$param."s='+s;
						
						$('#search_loader').show();
						
						/*$.post('".$keyup_action."',parameter,function(data){
							 $('#".$results_id."').html(data);
							 $('#search_loader').hide();
						});*/
						jQuery.post('".$keyup_action."',{ 
				    		pKEY:'".$param."',
				    		lvalue:s
			              },function(data){ //alert(data);
			              		$('#".$results_id."').html(data);
							 	$('#search_loader').hide();
						  });
						$('#response').html('');
						
					});
					
					
				});
				
				$(function(){
					$('.searchtext').focus(function(){
						$('.searchtext').val('');
					});
				});
				$(function(){
					var search_text='".$text."';
					$('.searchtext').blur(function(){
						$('.searchtext').val($(this).val()==''?search_text:$(this).val());
					});
				});
				</script>";
		}	    
		return $searchbox;
	}
	
	
    function get_postPriceGroup_js(){   	
	    $type = $_GET['state'];
	    $js= "
						<script type=\"text/javascript\">
							jQuery('.div_price_group').find('input').css('border','1px solid #ccc');							
							jQuery('.div_price_group').find('input').click(function(){
								jQuery(this).css('background','#fff');	
								jQuery(this).css('border','1px solid #ccc');			
							});
							
							$(function(){
								$('.add_price_range_btn').live(\"click\",function(){
									var thaURL = 'http://".SITE_URL."/shopping-cart-action-ajax';
									var PKEY = 'add_priceGroup';
									var ID   = $(this).attr(\"id\"); 
									var TYPE = \"$type\";
									var temp_PID	= $('#tmp_product_id'+ID).val();
									var item_start  = $('#txt_item_start'+ID).val();
									var item_end 	= $('#txt_item_end'+ID).val();
									var price		= $('#txt_price'+ID).val();
									
									if(item_start==''){
										$('#txt_item_start'+ID).css('background','#E8AEB6');
										$('#txt_item_start'+ID).css('border','1px solid red');
									}else if(item_end==''){
    									$('#txt_item_end'+ID).css('background','#E8AEB6');
    									$('#txt_item_end'+ID).css('border','1px solid red');
    								}else if(price==''){
    									$('#txt_price'+ID).css('background','#E8AEB6');
    									$('#txt_price'+ID).css('border','1px solid red');
    								}else{												
										$.ajax({
											type: \"POST\",
											url: thaURL,
											data: \"pKEY=\"+ PKEY +\"&index=\"+ ID + \"&type=\" + TYPE + \"&temp_pid=\" + temp_PID + \"&start=\" + item_start + \"&end=\" + item_end + \"&price=\" + price,
											cache: false,
											success: function(html){																						
												if(html!='error'){													
													jQuery('.div_price_group').find('input:text').val('');
													$(\"#tb_group_price\"+ID).append(html);
												}else{													
													alert('Ooops,, something error!');
    											}												
											}
										});
									}
									return false;
								});
							});	
							
							
							$(function(){								
								$('.del_price_range').live(\"click\",function(){
									var thaURL 	= 'http://".SITE_URL."/shopping-cart-action-ajax';
									var PKEY 	= 'delete_priceGroup';
									var PID   	= $(this).attr(\"id\"); 
									var VALUE	= $(this).attr(\"rel\");
									var r		= confirm('Are you sure to delete this item?');
									if (r==true){
										$.ajax({
												type: \"POST\",
												url: thaURL,
												data: \"pKEY=\"+ PKEY + \"&pid=\" + PID + \"&value=\" + VALUE,
												cache: false,
												success: function(html){																						
													if(html!='error'){													
														$(\".tr_\"+PID+\"_\"+VALUE).hide();
													}else{													
														alert('Ooops,, something error!');
	    											}												
												}
										});
									}
									return false;
								});
							});
						</script>
	          		  ";
	    return $js;
    }
    function add_priceGroup(){   
    	global $db; 	
    	$pKEY	= $_POST['pKEY'];
     	$index	= $_POST['index'];	
     	$type	= $_POST['type'];
     	$id		= $_POST['temp_pid'];
     	$start	= $_POST['start'];	
     	$end	= $_POST['end'];		
		$price	= $_POST['price'];	
     	
		$app_id  		= $id;
		$key 			= "product_price_range";
		//$value		= "{"1-5":"200","6-10":"190","11-15":"180","16-20":"170","21-25":"160","26-30":"150"}";		
		//$value		= '{"'.$start.'-'.$end.'":"'.$price.'"}';
		$arr			= array(''.$start.'-'.$end.'' => $price);
		$value 		    = json_encode($arr);
		$app_name		= "products";
		
		$q = $db->prepare_query("SELECT lapp_id,lvalue from lumonata_additional_fields where lapp_id=%d AND lkey=%s AND lapp_name=%s",$app_id,$key,$app_name);
		$r = $db->do_query($q);
		$n = $db->num_rows($r);
		if ($n==0){
			add_additional_field($app_id,$key,$value,$app_name);
		}else{
			$d 			= $db->fetch_array($r);			
			$arr_old 	= json_decode($d['lvalue'],true);		
			
			$arr_old[''.$start.'-'.$end.'']  	= $price;	
			$value 		    					= json_encode($arr_old);		
			edit_additional_field($app_id,$key,$value,$app_name);
		}
   		
		
		
        $row 	= '<tr class="tr_'.$id.'_'.$start.'-'.$end.'"><td>'.$start.' - '.$end.'</td> <td>'.$price.'</td><td><a class="del_price_range" id="'.$id.'" rel="'.$start.'-'.$end.'" href="javascript;">X</a></td></tr>';
        echo $row; 
    }
    function delete_priceGroup(){
    	global $db; 
    	$pKEY	= $_POST['pKEY'];
    	$pid	= $_POST['pid'];
    	$value	= $_POST['value'];
    	$key 			= "product_price_range";		
		$app_name		= "products";
    	
    	$q = $db->prepare_query("SELECT lapp_id,lvalue from lumonata_additional_fields where lapp_id=%d AND lkey=%s AND lapp_name=%s",$pid,$key,$app_name);
		$r = $db->do_query($q);
		$n = $db->num_rows($r);
		if ($n==0){
			echo "error";
		}else{
			$d		= $db->fetch_array($r);			
			$arr 	= json_decode($d['lvalue'],true);	
			unset ($arr[$value]);
			$value 	= json_encode($arr);		
			edit_additional_field($pid,$key,$value,$app_name);
		}
    }
    function get_price_group($post_id){
    	global $db;
    	$key 		= "product_price_range";		
		$app_name	= "products";
    	$data 		= '';
    	$q = $db->prepare_query("SELECT lvalue from lumonata_additional_fields where lapp_id=%d AND lkey=%s AND lapp_name=%s",$post_id,$key,$app_name);
    	$r = $db->do_query($q);
    	$n = $db->num_rows($r);
		if ($n==0){
			return $data;
		}else{
			$d		= $db->fetch_array($r);			
			$arr 	= json_decode($d['lvalue'],true);			
			foreach ($arr as $key => $value) {
    			$data .= '<tr class="tr_'.$post_id.'_'.$key.'"><td>'.$key.'</td> <td>'.$value.'</td> <td><a class="del_price_range" id="'.$post_id.'" rel="'.$key.'" href="javascript;">X</a><td> </tr>';
			}		
		}
    	return $data;
    }
    function additional_sync($id_old,$id_new){
    	global $db;
        $sql=$db->prepare_query("UPDATE lumonata_additional_fields
                SET lapp_id=%d
                WHERE lapp_id=%d",
                $id_new,$id_old);
        return $db->do_query($sql); 
    }
    function delete_additional_field_except_lkey($app_id,$app_name,$lkey){
		global $db;
		$sql=$db->prepare_query("DELETE FROM
					lumonata_additional_fields
					WHERE lapp_id=%d AND lapp_name=%s AND lkey!=%s",
					$app_id,$app_name,$lkey);
		
		return $r=$db->do_query($sql);
	}
?>