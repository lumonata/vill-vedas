<?php
require_once("../../lumonata_config.php");
require_once("../../lumonata-functions/settings.php");
global $db;

// STEP 1: Read POST data
// reading posted data from directly from $_POST causes serialization 
// issues with array data in POST
// reading raw POST data from input stream instead. 
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
  $keyval = explode ('=', $keyval);
  if (count($keyval) == 2)
     $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
   $get_magic_quotes_exists = true;
} 
foreach ($myPost as $key => $value) {        
   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
        $value = urlencode(stripslashes($value)); 
   } else {
        $value = urlencode($value);
   }
   $req .= "&$key=$value";
}

 
// STEP 2: Post IPN data back to paypal to validate

///$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
$ch = curl_init(get_paypal_url_mode());
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
// of the certificate as shown below.
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if( !($res = curl_exec($ch)) ) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);
 

// STEP 3: Inspect IPN validation result and act accordingly

if (strcmp ($res, "VERIFIED") == 0) {
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment

    // assign posted variables to local variables
    $item_name 			= $_POST['item_name'];
    $item_number 		= $_POST['item_number'];
    $payment_status 	= $_POST['payment_status'];
    $payment_amount 	= $_POST['mc_gross'];
    $payment_currency 	= $_POST['mc_currency'];
    $txn_id 			= $_POST['txn_id'];
    $receiver_email 	= $_POST['receiver_email'];
    $payer_email 		= $_POST['payer_email'];
    
    //do action here
    confirm_payment_succesfully();
    
    
} else if (strcmp ($res, "INVALID") == 0) {
    // log for manual investigation
}

?>

<?php 
function confirm_payment_succesfully(){
	$order_id	= $_GET['o'];
	$subject = 'Instant Payment Notification - Recieved Payment ['.$order_id.']';
    $to = 'dana@lumonata.com';    //  your email
    $body =  "An instant payment notification was successfully recieved\n";
    $body .= "from ".$payer_email." on ".date('m/d/Y');
    $body .= " at ".date('g:i A')."\n\nDetails:\n";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
	$headers .= "From:dana@lumonata.com\r\n";
                 
    mail($to, $subject, $body,$headers);
    
    ///
	$email_admin = get_meta_data('email');
	$email_client='';
		
	send_mail_order($order_id,$email_admin,'admin');
	send_mail_order($order_id,$email_client,'client');
	
   
}

function get_paypal_url_mode(){
	$value[0]='name';
	$value[1]='text';
	$value[2]='mode';
	$count_p=0;
	$tmp_array_parent=array();
	$data_payments=get_meta_data('payments','product_setting');
	$array_payments = json_decode($data_payments,true);
      if(is_array($array_payments)){
	      foreach($array_payments['parent_payment'] as $key=>$val){
		      $count_c=0;
		      $tmp_array_parent['parent_payment'][$count_p]=$val;
		    
		     
		      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

		      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;		      		
			      	$count_c++;
		      	
		      }
		    $count_p++;
       	 }
      }

      for($j=0;$j<$count_p;$j++){
      	if($tmp_array_parent['parent_payment'][$j]=="payments_paypal_standard"){
      		$mode_paypal = $tmp_array_parent['child_payment'][$j]['mode'];
      		$business = $tmp_array_parent['child_payment'][$j]['text'];
      		$method  = "payments_paypal_standard";
      	}
      }

      if($mode_paypal==0){
      	 $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
      	 return $paypal_url;
      }else{
      	$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; 
      	return $paypal_url;
      }

}

?>
