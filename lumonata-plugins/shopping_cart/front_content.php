<?php
function get_the_categories_product($args=''){
	global $db;
	$var['app_name']='';
	$var['parent_id']=0;
	$var['type']='li';
	$var['order']='ASC';
	$var['category_name']='';
	$var['sef']='';
	//echo $args;
	
	if(!empty($args)){
       $args=explode('&',$args);
       foreach($args as $val){
                list($variable,$value)=explode('=',$val);
                if($variable=='app_name' || $variable=='parent_id' || $variable=='type' || $variable=='order' || $variable=='category_name' || $variable=='sef'){
                $var[$variable]=$value;
                
                }
       }
    }
	
	if(!empty($var['category_name']) && !empty($var['app_name'])){
		$query=$db->prepare_query("SELECT a.* 
									FROM lumonata_rules a
									WHERE a.lname=%s AND 
											a.lrule='categories' AND 
											a.lgroup=%s AND
											",
									$var['category_name'],$var['app_name']);
		
											
		$result=$db->do_query($query);
		$data=$db->fetch_array($result);
		$var['parent_id']=$data['lrule_id'];
	}elseif(!empty($var['sef']) && !empty($var['app_name'])){
		$query=$db->prepare_query("SELECT a.* 
									FROM lumonata_rules a 
									WHERE lsef=%s AND 
											lrule='categories' AND 
											lgroup=%s AND
											",
									        $var['sef'],$var['app_name']);
		
											
		$result=$db->do_query($query);
		$data=$db->fetch_array($result);
		$var['parent_id']=$data['lrule_id'];
	}
	
	
	return recursive_taxonomy_product(0, 'categories', $var['app_name'],$var['type'], array(),$var['order'],$var['parent_id'],0,true);
	   
}

function recursive_taxonomy_product($index,$rule_name,$group,$type='select',$rule_id=array(),$order='ASC',$parent=0,$level=0,$related_to_article=false){
        global $db;
        if(!$related_to_article)
        $sql=$db->prepare_query("SELECT *
                                 FROM lumonata_rules 
                                 WHERE lrule=%s AND (lgroup=%s OR lgroup=%s) AND lparent=%d  order by lorder $order",$rule_name,$group,'default',$parent);
        else 
        $sql=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%d AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.lshare_to=0
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder $order",$rule_name,$group,$parent);
       
        $r=$db->do_query($sql);
        $num=$db->num_rows($r);
        
        $items="";
        $end_item="";
        $sts="";
        
        if($type=='li' && $num >0){
            $items="<ul>";
            $end_item="</ul>";
        }elseif($type=='checkbox'){
            if($level==0){
                $items="<ul class=\"the_categories\">";
            }else{
                $items="<ul>";
            }
            
            
            $end_item="</ul>";
        }  
        
        if($num >0){
            $level+=1;
        }else{
            $level=$level;
           
        }
        
        $dashed="";
        for($i=0;$i< $level;$i++){
                $x = $level - 1;
                if ($x == $i) 
                        $dashed.="";
                else
                        $dashed.="&nbsp;&nbsp;&nbsp;";
        }
        
       
        
        while($d=$db->fetch_array($r)){
          
            $next_level = recursive_taxonomy_product($index,$rule_name,$group,$type,$rule_id,$order,$d['lrule_id'],$level,$related_to_article);
            
            if($type=='select'){
                $sts=(in_array($d['lrule_id'],$rule_id))?"selected=\"selected\"":"";
                $items .= "<option value=\"".$d['lrule_id']."\" $sts >".$dashed.$d['lname']."</option>" ;
                $items .= $next_level;
            }elseif($type=='checkbox'){
                if(is_array($rule_id))
                $sts=(in_array($d['lrule_id'],$rule_id))?"checked=\"checked\"":"";
                
                $items .= "<li><input type=\"checkbox\" name=\"category[$index][]\" value=\"".$d['lrule_id']."\" id=\"the_category_".$index."_".$d['lrule_id']."\" $sts />".$d['lname']."</li>";
                $items.="<script type=\"text/javascript\">
							$('#the_category_".$index."_".$d['lrule_id']."').click(function(){
							    var selected_val=$(this).val();
							   
							    var checked_status = this.checked;
							    $('#the_most_category_".$index."_".$d['lrule_id']."').each(function(){
								if($(this).val()==selected_val){
								    this.checked = checked_status;
								}
							    });
							});
                      </script>";
                $items .= $next_level;
            }elseif($type=='li'){
                //if(is_array($rule_id))
                //	if(in_array($d['lrule_id'],$rule_id))
            	if($rule_name=='categories'){
                        if(is_permalink())
                            $the_link="http://".site_url()."/".$group."/".$d['lsef']."/";
                        else
                            $the_link="http://".site_url()."/?app_name=".$group."&cat_id=".$d['lrule_id'];
                            
                }elseif($rule_name=='tags'){
                		
                        if(is_permalink())
                            $the_link="http://".site_url()."/tag/".$d['lsef']."/";
                        else
                            $the_link="http://".site_url()."/?tag=".$d['lsef'];
                        
                }
                $items .= "<li><a href=\"".$the_link."\">".$d['lname']."</a>".$next_level."</li>" ;
            }else{
                if(is_array($rule_id))
                if(in_array($d['lrule_id'],$rule_id)){
                    
                    $sign=", ";
                    if($rule_name=='categories'){
                        if(is_permalink())
                            $the_category_link="http://".site_url()."/".$group."/".$d['lsef']."/";
                        else
                            $the_category_link="http://".site_url()."/?app_name=".$group."&cat_id=".$d['lrule_id'];
                            
                        $items .= "<a href=\"".$the_category_link."\">".$d['lname']."</a>".$sign;
                    }elseif($rule_name=='tags'){
                        
                        if(is_permalink())
                            $the_tag_link="http://".site_url()."/tag/".$d['lsef']."/";
                        else
                            $the_tag_link="http://".site_url()."/?tag=".$d['lsef'];
                        
                        $items .= "<a href=\"".$the_tag_link."\">".$d['lname']."</a>".$sign;
                    }
                }
             	$items .= $next_level;   
            }
            
            $items .= "";
            
        }
        
        
        $items.=$end_item;
            
        return $items;
    }

function get_product_category($app_name){
	global $db;
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Products';   
    $actions->action['meta_title']['args'][0] = '';	
	$items  = '<div class="caption_pink"> OUR PRODUCTS </div>';
    $items .= '<ul class="products-cat">';
	$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories',$app_name,0);
	
					
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	while($data=$db->fetch_array($result)){
		$sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='categories_image_thumbnail'",$data['lrule_id']);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_name=$data_product_images['lattach_loc_thumb'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
	   $mg_info = getimagesize(str_replace(" ",'%20',$images_url));   
		
	    $items .= '<li>';
	   
		$items.= '<a href="http://'.SITE_URL.'/'.$app_name.'/'.$data['lsef'].'">
							<img src="http://'.site_url().'/app/tb/tb.php?w=150&h=150&src='.$images_url.'" /> 
				 </a>';
		$items.= '<a href="http://'.SITE_URL.'/'.$app_name.'/'.$data['lsef'].'">'.$data['lname'].'</a>';
	
		$items.= '</li>';
		
	}
	$items .= '</ul>';
	return $items;
}
function get_product_maincategory($app_name){
	global $db;
	global $actions;
	$url = cek_url(); //products/subproducts/nama-products
	$arr_rule = get_array_of_rule($url[1],'categories','products');
	$actions->action['meta_title']['func_name'][0] = 'Products';   
    				$actions->action['meta_title']['args'][0] = '';	
    				
    $items  = get_track_link();
	$items .= '<div class="caption_pink"> '.$arr_rule['lname'].' </div>';
    $items .= '<ul class="products-cat">';
	$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories',$app_name,$arr_rule['lrule_id']);
	
					
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	while($data=$db->fetch_array($result)){
		$sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='categories_image_thumbnail'",$data['lrule_id']);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_name=$data_product_images['lattach_loc_thumb'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
	   $mg_info = getimagesize(str_replace(" ",'%20',$images_url));   
		
	    $items .= '<li>';
	   
		$items.= '<a href="http://'.SITE_URL.'/'.$app_name.'/'.$url[1].'/'.$data['lsef'].'">
							<img src="http://'.site_url().'/app/tb/tb.php?w=150&h=150&src='.$images_url.'" />
				 </a>';
		$items.= '<a href="http://'.SITE_URL.'/'.$app_name.'/'.$url[1].'/'.$data['lsef'].'">'.$data['lname'].'</a>';
	
		$items.= '</li>';
		
	}
	$items .= '</ul>';
	return $items;
}
/*
function cek_url(){
	$uri = get_uri();
	$ex = explode('/',$uri);
	return $ex;
}
*/
function is_about_us(){
	global $db;
	$cek_url = cek_url();
	if ($cek_url[0]=='about-us'){
		return true;
	}else{
		return false;
	}
}
function get_array_of_rule($sef,$rule='categories',$group='products'){
	global $db;	
	$d = '';
  	$q = $db->prepare_query("SELECT * from  lumonata_rules WHERE 
  							 lsef=%s AND lrule=%s AND lgroup=%s
  							",$sef,$rule,$group);
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	if ($n>0){
  		$d = $db->fetch_array($r);  		  		
  	}else{
  		$d = "No DATA!";  		
  	}
  	return $d;
}
function get_track_link(){
	$al = '';
	$l  = '';
	$b  = '';
	$cek_url = cek_url();
	
	for ($i=0;$i<count($cek_url);$i++){
		if ($i!=0){
			$b .= $cek_url[$i-1];
			$b .= '/';
		}
		$l   = 'http://'.site_url().'/'.$b.$cek_url[$i].'';		
		
		if ($i!=(count($cek_url)-1)){
			$al .= '<a href="'.$l.'">'.$cek_url[$i].'</a>';
			$al .= ' &raquo; ';
		}else{
			$al .= $cek_url[$i];
		}
	}

	$link  = '<div class="track-link"><span>Your are here : </span>';
	$link .= $al;
	$link .= '</div>';
	return $link;
}
function is_product_category(){
	global $actions;
	global $db;
	$cek_url = cek_url();

	if ($cek_url[0]=='products'){
		if (isset($cek_url[1])){
				$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories','products',0,$cek_url[1]);
				$result=$db->do_query($query);
				if($db->num_rows($result)<>0){
					$datax=$db->fetch_array($result);
					$actions->action['meta_title']['func_name'][0] = $datax['lname'];   
    				$actions->action['meta_title']['args'][0] = '';	
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function is_product_maincategory(){
	global $actions;
	global $db;
	$cek_url = cek_url();	
	if ($cek_url[0]=='products'){
		if (isset($cek_url[1])){
				$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories','products',$cek_url[1],$cek_url[1]);
				$result=$db->do_query($query);
				if($db->num_rows($result)<>0){
					$datax=$db->fetch_array($result);
					$actions->action['meta_title']['func_name'][0] = $datax['lname'];   
    				$actions->action['meta_title']['args'][0] = '';	
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function is_product_subcategory(){
	global $actions;
	global $db;
	$cek_url = cek_url();	
	if ($cek_url[0]=='products'){
		if (isset($cek_url[2])){				
				$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories','products',$cek_url[2],$cek_url[2]);
				$result=$db->do_query($query);
				if($db->num_rows($result)<>0){
					$datax=$db->fetch_array($result);
					$actions->action['meta_title']['func_name'][0] = $datax['lname'];   
    				$actions->action['meta_title']['args'][0] = '';	
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function get_product_category_list($app_name){
	global $db;
	$cek_url = cek_url();
	
	$items='';
	
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s AND 
                                 c.larticle_status=%s
                                 ORDER BY a.lorder ASC",
                                 'categories','products',0,$cek_url[1],'publish');
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	if($db->num_rows($result)<>0){
		$items.='<div id="in-content">Products</div>';
		$i=0;
		while($data=$db->fetch_array($result)){
				$sql_product_images= $db->prepare_query("select
							lattach_id,
							larticle_id ,
							lattach_loc,
							lattach_loc_thumb,
							lattach_loc_medium,
							lattach_loc_large,
							ltitle 
							 from 
							lumonata_attachment,lumonata_additional_fields 
							where 
							lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
							and lumonata_additional_fields.lapp_id=%d
							and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
		       $result_product_images=$db->do_query($sql_product_images);
		       $data_product_images=$db->fetch_array($result_product_images);
		       $images_name=$data_product_images['lattach_loc_thumb'];
		       $rows_images=$db->num_rows($result_product_images);
		       if($rows_images<>0){
		       		$images_url="http://".SITE_URL.$images_name;
		       }else{
		       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
		       }
		       
			$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);

		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      //print_r($obj_variant);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
		      
		       $sql_stock= $db->prepare_query("select * 
		       									from lumonata_additional_fields 
		       									where lapp_id=%s and lkey=%s and 
		       									lapp_name=%s",
		       									$data['larticle_id'],'product_additional','products');
	           $result_stok=$db->do_query($sql_stock);
	           $data_stok=$db->fetch_array($result_stok);
	           $obj_stok = json_decode($data_stok['lvalue'],true);
	           $the_product_stock=$obj_stok['price'];
	               	
			   $data_currency=get_symbol_currency();
			   $unit = get_unit_system();
			   
			   if($unit=='imperial'){
			   	$unit_system='inch';
			   }else if($unit=='metric'){
			   	$unit_system='cm';
			   }else{
			   	$unit_system='';
			   }

			   $variant='';
			if($i%4==0){
				$items.='<div id="content-product">';
			}
				$mg_info = getimagesize(str_replace(" ",'%20',$images_url));
				$items.='<div class="content-product-inside">
							<div class="content-product-image img-product"><a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$data['lsef'].'"><img width="'.$mg_info[0].'" height="'.$mg_info[1].'" src="'.$images_url.'" /></a></div>
							<div class="content-product-category  title-product"><a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$data['lsef'].'">'.$data['larticle_title'].'</a></div>
							<div class="product-size">';
					$variant='';
					for($a=0;$a<$count_it;$a++){
						$items.=$varian_parent[$a].': ';
						for($b=0;$b<$loop[$a];$b++){
							//$variant=$variant.$the_value[$b].$unit_system.' x ';
							$variant = $variant.$the_var[$a][$b].', ';
						}

						$items.=rtrim($variant,', ')."<br>";
						
					}
		                 $items.='Price: '.$data_currency.' '.number_format($the_product_stock,2,'.','').'
		                    </div>
	                    	<a style="text-decoration:none;" href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$data['lsef'].'">
		                    	<div class="bg-detail">
		                    			<div class="narrow">View Detail</div>
		                    	</div>
	                    	</a>
	                    </div>';

			if($i<>0 and $i%3==0 or $row_count-1==$i){
				$items.='</div>';
			}
			$i++;
		}
	}else{
		$items.='<div class="content-product-inside" style="width: 645px;"><b>Product Empty</b></div>';
	}
	return $items;
}
function get_product_list($app_name){
	global $db;
	$cek_url 	= cek_url();
	$arr_rule 	= get_array_of_rule($cek_url[2],'categories','products');
	$limit		= get_meta_data('post_viewed','global_setting',0);	
	
	$items 		= get_track_link();
	$items		.= '<script type="text/javascript">
					$(function(){
						$(".link-load-more").live("click",function(){
							var thaURL 	= "http://'.SITE_URL.'/shopping-cart-action-ajax";
							var ID 		= $(this).attr("id");							
							var PKEY 	= "loadMoreProduct_frontSide";	
							var URI 	= $(this).attr("rel");			
											
							if(ID){
								$(this).html("Loading...");		
								$.ajax({
									type: "POST",
									url: thaURL,
									data: "lastID="+ ID + "&pKEY=" + PKEY + "&uri=" + URI,
									cache: false,
									success: function(html){										
										$(".link-load-more").remove(); 
										$("ul.products-list").append(html);
									}
								});
							}else{
								$(".link-load-more").html("The End");
							}
							return false;
						});
					});
					</script>';
	$span		= '<span class="filter"> </span>';
	$items 	   .= '<div class="caption_blue">'.$arr_rule['lname'].' '.$span.'</div>';
	
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%d AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s AND 
                                 c.larticle_status=%s
                                 ORDER BY a.lorder ASC",
                                 'categories','products',$arr_rule['lparent'],$cek_url[2],'publish'); 
	
	$result=$db->do_query($query);
	$num=$db->num_rows($result);
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%d AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s AND 
                                 c.larticle_status=%s
                                 ORDER BY a.lorder ASC limit %d",
                                 'categories','products',$arr_rule['lparent'],$cek_url[2],'publish',$limit); 
	
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	if($db->num_rows($result)<>0){
		$items.='<ul class="products-list">';
		
		while($data=$db->fetch_array($result)){
				$sql_product_images= $db->prepare_query("select
							lattach_id,
							larticle_id ,
							lattach_loc,
							lattach_loc_thumb,
							lattach_loc_medium,
							lattach_loc_large,
							ltitle 
							 from 
							lumonata_attachment,lumonata_additional_fields 
							where 
							lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
							and lumonata_additional_fields.lapp_id=%d
							and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
		       $result_product_images=$db->do_query($sql_product_images);
		       $data_product_images=$db->fetch_array($result_product_images);
		       $images_name=$data_product_images['lattach_loc_thumb'];
		       $rows_images=$db->num_rows($result_product_images);
		       if($rows_images<>0){
		       		$images_url="http://".SITE_URL.$images_name;
		       }else{
		       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
		       }
		       
			$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);

		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      //print_r($obj_variant);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
		      
		       $sql_stock= $db->prepare_query("select * 
		       									from lumonata_additional_fields 
		       									where lapp_id=%s and lkey=%s and 
		       									lapp_name=%s",
		       									$data['larticle_id'],'product_additional','products');
	           $result_stok=$db->do_query($sql_stock);
	           $data_stok=$db->fetch_array($result_stok);
	           $obj_stok = json_decode($data_stok['lvalue'],true);
	           $the_product_stock=$obj_stok['price'];
	               	
			   $data_currency=get_symbol_currency();
			   $unit = get_unit_system();
			   
			   if($unit=='imperial'){
			   	$unit_system='inch';
			   }else if($unit=='metric'){
			   	$unit_system='cm';
			   }else{
			   	$unit_system='';
			   }

			
				$mg_info = getimagesize(str_replace(" ",'%20',$images_url));
				$items.='<li>';				
				//$items.='<a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'"><img width="'.$mg_info[0].'" height="'.$mg_info[1].'" src="'.$images_url.'" /></a>';
				$items.='<a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'"><img src="http://'.site_url().'/app/tb/tb.php?w=94&h=94&src='.$images_url.'"  /></a>';
				$items.='<div class="name"><a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'">'.$data['larticle_title'].'</a></div>';
				$items.=get_price_range($data['larticle_id'],$app_name,true);				
				
				$items.='<div class="buy-this"><span>Buy this item </span>';
				$items.='<form id="formaddtocart'.$data['larticle_id'].'">';
				$items.='<input type="hidden" name="product_id" value="'.$data['larticle_id'].'">';
				$items.='<input type="hidden" name="sef_cat" value="'.$data['larticle_title'].'">';
	            $items.='<input type="text" id="txt_qty'.$data['larticle_id'].'" name="qty" value=""/>';
	            $items.='<button type="button" class="addtocart_only" id="'.$data['larticle_id'].'"> </button>';
	            $items.='</form>';  

	            $items.='<div class="errorContent" id="ErrorContent'.$data['larticle_id'].'"><div class="error" id="Error'.$data['larticle_id'].'"> </div></div>';
                $items.='</div>';
					
				$items.='</li>';	
			
			
		}
		$items.='</ul>';
		if ($num>$limit){			
			$items.=' <a href="#" id="1" rel="'.get_uri().'" class="link-load-more"> Load more products </a>';
		}
	}else{
		$items.='<div class="caption_blue"><b>Product Empty</b></div>';
	}	
	return $items;
}
function is_detail_new_product(){		
	$cek_url = cek_url();
	if ($cek_url[0]=='new-products'){
		if (isset($cek_url[1])){
			return true;
		}else{
			return false;	
		}
	}else{
		return false;
	}	
}
function is_detail_featured_product(){		
	$cek_url = cek_url();
	if ($cek_url[0]=='featured-products'){
		if (isset($cek_url[1])){
			return true;
		}else{
			return false;	
		}
	}else{
		return false;
	}	
}
function is_product_detail(){
	global $db;
	$cek_url = cek_url();
	if ($cek_url[0]=='products'){
		if (isset($cek_url[1])){
			if (isset($cek_url[2])){
				if (isset($cek_url[3])){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
	
}
function get_product_detail($app_name){
	global $db;
	global $actions;
	
	$cek_url = cek_url();	
	$items=get_track_link();
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND
                                
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.lsef=%s AND 
                                 c.larticle_status=%s",
                                'categories','products',$cek_url[3],'publish');
	$result=$db->do_query($query);
	
	$items.='<div class="detail">';
	$i=0;
	$data=$db->fetch_array($result);
			$actions->action['meta_title']['func_name'][0] = $data['larticle_title'];   
    		$actions->action['meta_title']['args'][0] = '';	
	
		 $sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_url_large = "http://".SITE_URL.$data_product_images['lattach_loc_large'];
       $images_name=$data_product_images['lattach_loc_medium'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
		
		$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);
		      //echo $data_variant['lvalue'];
		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_idparent[$count_it]=$val;
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var_id[$count_it][$j]=$subkey;
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
				
       
       $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$data['larticle_id'],'product_additional','products');
       $result_stok=$db->do_query($sql_stock);
       $data_stok=$db->fetch_array($result_stok);
       $obj_stok = json_decode($data_stok['lvalue'],true);
       $the_product_price=$obj_stok['price'];
       $the_product_weight=$obj_stok['weight'];
       $the_product_code=$obj_stok['code'];
       $the_product_tax=$obj_stok['tax'];
      
	   $data_currency=get_symbol_currency();
	   $unit = get_unit_system();
	   if($unit=='imperial'){
	   	$unit_system='inch';
	   	$unit_weight='pounds';
	   }else if($unit=='metric'){
	   	$unit_system='cm';
	   	$unit_weight='kg';
	   }else{
	   	$unit_system='';
	   	$unit_weight='';
	   }
	   
		$items.='<style>
							.Error .ErrorContent {
							    background: none repeat scroll 0 0 #EE0101;
							    border: 2px solid #DDDDDD;
							    border-radius: 6px 6px 6px 6px;
							    box-shadow: 0 0 6px #000000;
							    color: #FFFFFF;
							    font-family: tahoma;
							    font-size: 11px;
							    padding: 20px 10px;
							    position: relative;
							    width: 150px;
							    z-index: 5001;
							}
		          </style>
				<script>
					
											
					$(document).ready(function(){
						$("#parent option:selected").removeAttr("selected");
						$("#parent").change(function(){ 
							var parent_selected= $("#parent option:selected").val();
							
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"variant_parent",
					    		lvalue:parent_selected,
					    		app_id:id
				              },function(data){
				              	split=data.split("&");
				              	//alert(split[0]);
				              	$("#text_price").html(split[0]);	
				              	$("#child").html(split[1]);
							});
						});
						
						$("#child").change(function(){ 
							var child_selected= $("#child option:selected").val();
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"price_child",
					    		lvalue:child_selected,
					    		app_id:id
				              },function(data){
				              	$("#text_price").html(data);
							});
						});
						
					});
				</script>';

			$items.='<!-- fb-comment-->
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, \'script\', \'facebook-jssdk\'));</script>
						<!--/fb-comment-->';
		
			if($the_product_weight<>"0"){
                $the_product_weight ='Weight: '.$the_product_weight.' '.$unit_weight.'';
            }else{
            	$the_product_weight = "";
            }
			$items.='<img src="'.$images_url.'" />';
			$items.='<div class="desc">
		              
		               <div class="name">'.$the_product_code.' </div>
		               <p>
		               <a href="http://'.SITE_URL.'/products/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'">'.$data['larticle_title'].'</a>
		               <br>
		               '.$data['larticle_content'].'		              
		               '.$the_product_weight.'		              
		               </p>         
		             
		              
              		';
 					
					/*/variant price
						if($count_it==1){
							$items.='<p>'.$varian_parent[0].'<input type="hidden" name="parent" value="'.$varian_idparent[0].'"> : ';
							if($loop[0]==1){
								$items.=$the_var[0][0].'<input type="hidden" name="child" value="'.$the_var_id[0][0].'"></p>';
								$the_product_price=$the_value[0][0];
							}else{
                                 $items.='<select name="child" id="child">';
									for($b=0;$b<$loop[0];$b++){
										$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
									}
							     $items.='</select></p>';
							     $the_product_price=$the_value[0][0];
							}
						}else if($count_it>1){
                         	$items.='<p><select name="parent" id="parent">';
							for($a=0;$a<$count_it;$a++){
										$items.='<option value="'.$varian_idparent[$a].'">'.$varian_parent[$a].'</option>';
							}
                                 $items.='</select></p>';
                                 $items.='<p><select name="child" id="child">';
								for($b=0;$b<$loop[0];$b++){
									$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
								}
						     $items.='</select></p>';
						     $the_product_price=$the_value[0][0];
						}else{
							 $items.='<div style="min-height:10px;float: left; width: 280px;"></div>';//nothink do
						}
						*/
						
                      $items.='<p id="price-product">
                            		<span class="title-product">Flat Price : </span>
                            		<span id="title-product2">'.$data_currency.' <span id="text_price">'.number_format($the_product_price,2,'.','').'</span></span>
                            	</p>
                      		  ';
                       
                      $items.=get_price_range($data['larticle_id'],'products',true);
                      
                      
                      $items.='<div class="buy-this"><span>Buy this item </span>';
                      $items.='<form name="formaddtocart" id="formaddtocart" class="addtocart">';
                      $items.='<input type="hidden" name="product_id" value="'.$data['larticle_id'].'">
		               		   <input type="hidden" name="sef_cat" value="'.$cek_url[2].'">';
			          $items.='
			                      			<div style="text-align: right; height: 40px;position:absolute;right:0;">
				                            	<div class="Error" style="position: absolute; text-align: left; z-index: 1200; float: left;display: none;">
											        <div class="ErrorContent">
												        * The quantity only numeric												       
											        </div>
											    </div>
											 </div>
				                               
				                                <input type="text" id="txt_qty" name="qty" value="1"/>
				                               
				                                <button type="button" id="addtocart" class="link1"></button>';
			          $items.='</form>';
			          $items.='</div></div></div>';
			          
			          $items.='<!--fb comments-->           
					            <div class="t-fb-comment">Comment about this product</div>
					            <div style="background:#fff;">
					            	<div class="fb-comments" data-href="http://'.SITE_URL.'/products/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'" data-num-posts="2" data-width="730"></div>    
					            </div> 
					            <!--/fb comments-->';
			          
				      $items.= get_related_product($app_name);
		
		
			
		$items.='<script>
				$(document).ready(function(){
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
				
					function IsNumeric(strString)
					   //  check for valid numeric strings	
					   {
					   var strValidChars = "0123456789";
					   var strChar;
					   var blnResult = true;
					
					   if (strString.length == 0) return false;
					
					   //  test strString consists of valid characters listed above
					   for (i = 0; i < strString.length && blnResult == true; i++)
					      {
					      strChar = strString.charAt(i);
					      if (strValidChars.indexOf(strChar) == -1)
					         {
					         blnResult = false;
					         }
					      }
					   return blnResult;
					 }
				$(".Error").css("display","none");
					$("#addtocart").click(function(){
							var x = $("#txt_qty").val();
							if(IsNumeric($("#txt_qty").val())==true && x!=0){
								//$("#formaddtocart").click(function(){
									var str = $("#formaddtocart").serialize();
									var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
									jQuery.post(thaUrl,{ 
							    		pKEY:"add_to_chart",
							    		lvalue:str
						              },function(data){
						              	//$("#text_price").html(data);
						              	if(trim(data)=="failed"){ 
											$(".ErrorContent").html("Product Out Of Stock");
											$(".Error").fadeIn(200).delay(3000).fadeOut(200);
										}else{
						              		document.location="http://'.SITE_URL.'/shopping-cart";
						              	}
						              	
									//});
								});
							}else{
								$(".ErrorContent").html("*The quantity is numeric only");
								$(".Error").fadeIn(200).delay(3000).fadeOut(200);
							}
						});
				})
				
			</script>';
					
	return $items;

}
function get_detail_new_product($app_name){
	global $db;
	global $actions;
	
	$cek_url = cek_url();	
	$items=get_track_link();
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND
                                
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.lsef=%s AND 
                                 c.larticle_status=%s",
                                'categories','products',$cek_url[1],'publish');
	$result=$db->do_query($query);
	
	$items.='<div class="detail">';
	$i=0;
	$data=$db->fetch_array($result);
			$actions->action['meta_title']['func_name'][0] = $data['larticle_title'];   
    		$actions->action['meta_title']['args'][0] = '';	
	
		 $sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_url_large = "http://".SITE_URL.$data_product_images['lattach_loc_large'];
       $images_name=$data_product_images['lattach_loc_medium'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
		
		$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);
		      //echo $data_variant['lvalue'];
		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_idparent[$count_it]=$val;
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var_id[$count_it][$j]=$subkey;
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
				
       
       $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$data['larticle_id'],'product_additional','products');
       $result_stok=$db->do_query($sql_stock);
       $data_stok=$db->fetch_array($result_stok);
       $obj_stok = json_decode($data_stok['lvalue'],true);
       $the_product_price=$obj_stok['price'];
       $the_product_weight=$obj_stok['weight'];
       $the_product_code=$obj_stok['code'];
       $the_product_tax=$obj_stok['tax'];
      
	   $data_currency=get_symbol_currency();
	   $unit = get_unit_system();
	   if($unit=='imperial'){
	   	$unit_system='inch';
	   	$unit_weight='pounds';
	   }else if($unit=='metric'){
	   	$unit_system='cm';
	   	$unit_weight='kg';
	   }else{
	   	$unit_system='';
	   	$unit_weight='';
	   }
	   
		$items.='<style>
							.Error .ErrorContent {
							    background: none repeat scroll 0 0 #EE0101;
							    border: 2px solid #DDDDDD;
							    border-radius: 6px 6px 6px 6px;
							    box-shadow: 0 0 6px #000000;
							    color: #FFFFFF;
							    font-family: tahoma;
							    font-size: 11px;
							    padding: 20px 10px;
							    position: relative;
							    width: 150px;
							    z-index: 5001;
							}
		          </style>
				<script>
					
											
					$(document).ready(function(){
						$("#parent option:selected").removeAttr("selected");
						$("#parent").change(function(){ 
							var parent_selected= $("#parent option:selected").val();
							
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"variant_parent",
					    		lvalue:parent_selected,
					    		app_id:id
				              },function(data){
				              	split=data.split("&");
				              	//alert(split[0]);
				              	$("#text_price").html(split[0]);	
				              	$("#child").html(split[1]);
							});
						});
						
						$("#child").change(function(){ 
							var child_selected= $("#child option:selected").val();
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"price_child",
					    		lvalue:child_selected,
					    		app_id:id
				              },function(data){
				              	$("#text_price").html(data);
							});
						});
						
					});
				</script>';

			$items.='<!-- fb-comment-->
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, \'script\', \'facebook-jssdk\'));</script>
						<!--/fb-comment-->';
		
			if($the_product_weight<>"0"){
                $the_product_weight ='Weight: '.$the_product_weight.' '.$unit_weight.'';
            }else{
            	$the_product_weight = "";
            }
			$items.='<img src="'.$images_url.'" />';
			$items.='<div class="desc">
		              
		               <div class="name">'.$the_product_code.' </div>
		               <p>
		               <a href="http://'.SITE_URL.'/'.$cek_url[0].'/'.$cek_url[1].'">'.$data['larticle_title'].'</a>
		               <br>
		               '.$data['larticle_content'].'		              
		               '.$the_product_weight.'		              
		               </p>         
		             
		              
              		';
 					
					/*/variant price
						if($count_it==1){
							$items.='<p>'.$varian_parent[0].'<input type="hidden" name="parent" value="'.$varian_idparent[0].'"> : ';
							if($loop[0]==1){
								$items.=$the_var[0][0].'<input type="hidden" name="child" value="'.$the_var_id[0][0].'"></p>';
								$the_product_price=$the_value[0][0];
							}else{
                                 $items.='<select name="child" id="child">';
									for($b=0;$b<$loop[0];$b++){
										$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
									}
							     $items.='</select></p>';
							     $the_product_price=$the_value[0][0];
							}
						}else if($count_it>1){
                         	$items.='<p><select name="parent" id="parent">';
							for($a=0;$a<$count_it;$a++){
										$items.='<option value="'.$varian_idparent[$a].'">'.$varian_parent[$a].'</option>';
							}
                                 $items.='</select></p>';
                                 $items.='<p><select name="child" id="child">';
								for($b=0;$b<$loop[0];$b++){
									$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
								}
						     $items.='</select></p>';
						     $the_product_price=$the_value[0][0];
						}else{
							 $items.='<div style="min-height:10px;float: left; width: 280px;"></div>';//nothink do
						}
						*/
						
                      $items.='<p id="price-product">
                            		<span class="title-product">Flat Price : </span>
                            		<span id="title-product2">'.$data_currency.' <span id="text_price">'.number_format($the_product_price,2,'.','').'</span></span>
                            	</p>
                      		  ';
                       
                      $items.=get_price_range($data['larticle_id'],'products',true);
                      
                      
                      $items.='<div class="buy-this"><span>Buy this item </span>';
                      $items.='<form name="formaddtocart" id="formaddtocart" class="addtocart">';
                      $items.='<input type="hidden" name="product_id" value="'.$data['larticle_id'].'">
		               		   <input type="hidden" name="sef_cat" value="'.$cek_url[0].'">';
			          $items.='
			                      			<div style="text-align: right; height: 40px;position:absolute;right:0;">
				                            	<div class="Error" style="position: absolute; text-align: left; z-index: 1200; float: left;display: none;">
											        <div class="ErrorContent">
												        * The quantity only numeric												       
											        </div>
											    </div>
											 </div>
				                               
				                                <input type="text" id="txt_qty" name="qty" value="1"/>
				                               
				                                <button type="button" id="addtocart" class="link1"></button>';
			          $items.='</form>';
			          $items.='</div></div></div>';
			          
			          $items.='<!--fb comments-->           
					            <div class="t-fb-comment">Comment about this product</div>
					            <div style="background:#fff;">
					            	<div class="fb-comments" data-href="http://'.SITE_URL.'/'.$cek_url[0].'/'.$cek_url[1].'" data-num-posts="2" data-width="730"></div>    
					            </div> 
					            <!--/fb comments-->';
			          
				     // $items.= get_related_product($app_name);
		
		
			
		$items.='<script>
				$(document).ready(function(){
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
				
					function IsNumeric(strString)
					   //  check for valid numeric strings	
					   {
					   var strValidChars = "0123456789";
					   var strChar;
					   var blnResult = true;
					
					   if (strString.length == 0) return false;
					
					   //  test strString consists of valid characters listed above
					   for (i = 0; i < strString.length && blnResult == true; i++)
					      {
					      strChar = strString.charAt(i);
					      if (strValidChars.indexOf(strChar) == -1)
					         {
					         blnResult = false;
					         }
					      }
					   return blnResult;
					 }
				$(".Error").css("display","none");
					$("#addtocart").click(function(){
							var x = $("#txt_qty").val();
							if(IsNumeric($("#txt_qty").val())==true && x!=0){
								//$("#formaddtocart").click(function(){
									var str = $("#formaddtocart").serialize();
									var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
									jQuery.post(thaUrl,{ 
							    		pKEY:"add_to_chart",
							    		lvalue:str
						              },function(data){
						              	//$("#text_price").html(data);
						              	if(trim(data)=="failed"){ 
											$(".ErrorContent").html("Product Out Of Stock");
											$(".Error").fadeIn(200).delay(3000).fadeOut(200);
										}else{
						              		document.location="http://'.SITE_URL.'/shopping-cart";
						              	}
						              	
									//});
								});
							}else{
								$(".ErrorContent").html("*The quantity is numeric only");
								$(".Error").fadeIn(200).delay(3000).fadeOut(200);
							}
						});
				})
				
			</script>';
					
	return $items;

}
function get_related_product($app_name='products'){
	global $db;
	$cek_url = cek_url();
	$arr_rule = get_array_of_rule($cek_url[2],'categories','products');
	$items = '';
	$items .='<div class="caption heart"> RELATED PRODUCTS </div>';
	
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%d AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s AND 
                                 c.larticle_status=%s AND
                                 c.lsef!=%s
                                 ORDER BY a.lorder ASC limit 4",
                                 'categories','products',$arr_rule['lparent'],$cek_url[2],'publish',$cek_url[3]);
	
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	if($db->num_rows($result)<>0){
		$items.='<ul class="items">';
		
		while($data=$db->fetch_array($result)){
				$sql_product_images= $db->prepare_query("select
							lattach_id,
							larticle_id ,
							lattach_loc,
							lattach_loc_thumb,
							lattach_loc_medium,
							lattach_loc_large,
							ltitle 
							 from 
							lumonata_attachment,lumonata_additional_fields 
							where 
							lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
							and lumonata_additional_fields.lapp_id=%d
							and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
		       $result_product_images=$db->do_query($sql_product_images);
		       $data_product_images=$db->fetch_array($result_product_images);
		       $images_name=$data_product_images['lattach_loc_thumb'];
		       $rows_images=$db->num_rows($result_product_images);
		       if($rows_images<>0){
		       		$images_url="http://".SITE_URL.$images_name;
		       }else{
		       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
		       }
		       
			$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);

		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      //print_r($obj_variant);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
		      
		       $sql_stock= $db->prepare_query("select * 
		       									from lumonata_additional_fields 
		       									where lapp_id=%s and lkey=%s and 
		       									lapp_name=%s",
		       									$data['larticle_id'],'product_additional','products');
	           $result_stok=$db->do_query($sql_stock);
	           $data_stok=$db->fetch_array($result_stok);
	           $obj_stok = json_decode($data_stok['lvalue'],true);
	           $the_product_stock=$obj_stok['price'];
	               	
			   $data_currency=get_symbol_currency();
			   $unit = get_unit_system();
			   
			   if($unit=='imperial'){
			   	$unit_system='inch';
			   }else if($unit=='metric'){
			   	$unit_system='cm';
			   }else{
			   	$unit_system='';
			   }

			
				$mg_info = getimagesize(str_replace(" ",'%20',$images_url));
				$items.='<li>';
				$items.='<a href="http://'.SITE_URL.'/'.$app_name.'/'.$cek_url[1].'/'.$cek_url[2].'/'.$data['lsef'].'"><img width="'.$mg_info[0].'" height="'.$mg_info[1].'" src="http://'.site_url().'/app/tb/tb.php?w=94&h=94&src='.$images_url.'" /></a>';
				$items.='<span class="name">'.$data['larticle_title'].'</span>';
				$items.=get_price_range($data['larticle_id'],$app_name,false);
				$items.=' <a href="#" class="add-to-cart"> </a>';
				$items.='</li>';	
			
			
		}
	}else{
		$items.='<div class="caption_blue"><b>Product Empty</b></div>';
	}
	$items.='</ul>';
	return $items;
	
}
function is_chart_list1(){
	global $db;
	$cek_url = cek_url();
	if($cek_url[0]=='shopping-cart'){
		if(isset($_SESSION['cart']) and count($_SESSION['cart'])>0){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function is_chart_shipping(){
	global $db;
	
	$_SESSION['error']=false;
	$cek_url = cek_url();
	if($cek_url[0]=='shipping-detail'){
		if(isset($_SESSION['cart']) and count($_SESSION['cart'])>0){
		$loop=0;
		foreach($_SESSION['cart'] as $product ) {
			$data_product=get_detail_product($product['id']);
			$additional_product=default_price($product['id']);
			$the_images=get_product_detail_images($product['id']);
			if(empty($product['parent'])){
				$the_price=$additional_product['price'];
				$variant_type='';
				$product['parent'] = '';
				$product['child'] = '';
			}else{
				$the_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				$variant_type = find_rules_name($product['parent'],0,'product').' : '.find_rules_name($product['child'],$product['parent'],'product');
			}
			$total_this_product=$the_price*$product['qty'];
			
			$testing = fetch_rule_relationship('app_id='.$product['id']);//id category
			$minimum_price = get_additional_field($testing[0], 'minimum_price', 'categories_price'); //min price category
			
			if(!empty($var[$testing[0]])){
				$total_purchase[$testing[0]] = $total_purchase[$testing[0]] + $total_this_product;
			}else{
				$tmp_min_cat[$loop] = $minimum_price;
				$tmp_cat[$loop] = $testing[0];
				$var[$testing[0]] = $testing[0];
				$total_purchase[$testing[0]] = $total_this_product;
				$loop++;
			}
		}
		
		$minimum_total_purchase = get_meta_data('minimum_price','product_setting');
		
		$parameter = true;
		for($k=0;$k<$loop;$k++){
			if($total_purchase[$tmp_cat[$k]]<$tmp_min_cat[$k]){
				$parameter = false;
				$categori_id = $tmp_cat[$k];
			}
		}
		if($parameter==false){
			$_SESSION['error'] = true;
			header("Location: http://".SITE_URL."/shopping-cart");
		}else{
			if($_SESSION['total_price'] < $minimum_total_purchase){
				$_SESSION['error'] = true;
				header("Location: http://".SITE_URL."/shopping-cart");
			}else{
				return true;
			}
		}
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function get_shipping_detail($app_name){
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Billing & Shipping Detail, Order Review';   
    $actions->action['meta_title']['args'][0] = '';	
	$tmp_id='';
	$i=0;
	foreach($_SESSION['cart'] as $product ) {
			if($tmp_id==$product['id']){
				$tmp_product_id[$i]=$tmp_id;
				$i++;
			}
			$tmp_id=$product['id'];
		}

		$k=0;
		//id product with same value
		for($j=0;$j<$i;$j++){
			$value=0;
			foreach($_SESSION['cart'] as $product ) {
				if($tmp_product_id[$j]==$product['id']){
					if($value=="0" or $value=''){
						$id_result[$k]=$product['id'];
						$value=1;
						$k++;
					}
				}else{
					$id_result[$k]=$product['id'];
					$k++;
				}
			}
			
		}
		
		for($l=0;$l<$k;$l++){
			$tmp_qty=0;
			$total_this_product=0;
			$tmp_total=0;
			$tmp_price=0;
			$the_qty=0;
			$thetotal=array();
			$theqty=array();
			foreach($_SESSION['cart'] as $product ) {
				$data_product=get_detail_product($product['id']);
				$additional_product=default_price($product['id']);
				if(!empty($product['parent'])){
					$parent_id = $product['parent'];
					$child_id = $product['child'];
					$tmp_price=get_product_variant_detail($product['id'],$parent_id,$child_id);
				}else{
					$parent_id = '';
					$child_id = '';
					$tmp_price=$additional_product['price'];
				}
				
				$tmp_total=$tmp_price*$product['qty'];
				$the_qty = $product['qty'];
				
				if($id_result[$l]==$product['id']){
					$total_this_product=$total_this_product+$tmp_total;
					$tmp_qty = $tmp_qty + $the_qty;
				}
			}
				$thetotal[$l]=$total_this_product;
				$theqty[$l]=$tmp_qty;

		}
		
		$error_minimum_total = false;
		$minimum_total_purchase = get_meta_data('minimum_price','product_setting');
		if($_SESSION['total_price'] < $minimum_total_purchase){
			$error_minimum_total = true;
		}
		
	$items=' <style>
				table,input,textarea{
					  font-family: arial;
					  font-size: 14px;
				  }
			 	</style>';
	$items='<div class="billing-shipping">'; 
	$items.= get_link_please_login();
	$items.='<form id="form-order" class="form-order" method="post" action="">
				<div class="finput">
            	<h1>BILLING ADDRESS</h1>            	
               	<div class="checksame"><span class="additional-fontsize">Please note that fields with * are requried</span></div>
            	'.get_form_input_billing().'           
            	</div>';
	
	$items.='            
            <div class="finput" id="finputShipping" style="margin-left:40px;margin-right:0 !important;">
            <h1>SHIPPING ADDRESS</h1>
              <div class="checksame"> <input type="checkbox" id="shipping-address" name="shipping-address" value="1"/><span class="additional-fontsize">Use the same address as the billing address</span> </div>

             <ul class="first">
             	<li>First Name*</li>
                <li><input class="validate[required] input_large" type="text" name="firstname_sh" id="firstname_sh" value=""/></li>
             </ul>
             
             <ul>
                <li>Last Name* </li>
                <li><input class="validate[required] input_large" type="text" name="lastname_sh" id="lastname_sh" value=""/></li>
             </ul>

             <ul>
             	<li>Address*</li>
                <li><input class="validate[required] input_large" type="text" name="address_sh" id="address_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li> </li>
             	<li> <input class="input_large" type="text" name="second_address_sh" id="second_address_sh" value=""/> </li>
             </ul>
            
              <ul>
             	<li> </li>
             	<li><input class="input_large" type="text" name="third_address_sh" id="third_address_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li>City*</li>
             	<li><input class="validate[required] input_large" type="text" name="city_sh" id="city_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li>Region/State*</li>
             	<li><input class="input_large" type="text" name="region_sh" id="region_sh" value=""/><span class="fi-note">type None if not aplicable</span></li>
             </ul>
             
             <ul>
             	<li>Postal Code*</li>
                <li><input class="validate[required,,custom[onlyNumber] input_medium" type="text" name="postal_sh" id="postal_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li>Country*</li>
                <li><div class="c-shipping"><span id="select_view"><select name="country_sh" title="select country" id="country_sh" class="validate[required] class_country_sh select" rel="middle">'.country_shipping2().'</select></span></div></li>
             </ul> 
             
             <ul>
             	<li>Phone*</li>
             	<li><input class="validate[required] input_large" type="text" name="primary_phone_sh" id="primary_phone_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li>Phone 2</li>
             	<li><input class="input_large" type="text" name="alternative_phone_sh" id="alternative_phone_sh" value=""/></li>
             </ul>
             
             <ul>
             	<li>Email*</li>
             	<li><input class="validate[required,custom[email]] input_large" type="text" name="email_sh" id="email_sh" value=""/> </li>
             </ul>
           </div>';
	
	
	$items.='<div class="t-sc">REVIEW ORDER</div>';
	$items.='<div id="review_order">'.list_chart_shipping_detail('products').'</div>';
	$items.='</form>';
	
	$items.='<style>
					.Error .ErrorContent {
					    background: none repeat scroll 0 0 #EE0101;
					    border: 2px solid #DDDDDD;
					    border-radius: 6px 6px 6px 6px;
					    box-shadow: 0 0 6px #000000;
					    color: #FFFFFF;
					    font-family: tahoma;
					    font-size: 11px;
					    padding: 4px 10px;
					    position: relative;
					    width: 150px;
					    z-index: 5001;
					}
          		</style>';
	
	$items.='<script>
			//function
		  	function getALertTerms(field, rules, i, options){
			  	if ($("#shipping-address-term").is(":checked")) {
	    				
				} else {
	    				return options.allrules.termsConditions.alertText;
				} 			
		 	}
		 	
			function trim(str) {
				var	str = str.replace(/^\s\s*/, ""),
					ws = /\s/,
					i = str.length;
				while (ws.test(str.charAt(--i)));
				return str.slice(0, i + 1);
			}
          	
			
			$(document).ready(function(){
				//function trim dipindah ke atas         	
          
          	$("#shipping-address").attr("checked",false);          	
          	
          	$("#shipping-address").click(function(){
          		var country = $("#country").val();
          		var text_country = $("#country option[value="+country+"]").text()          		
				var checked = jQuery(this).attr("checked");
				
				if(checked){	
					//all span that use as combo
					$("span.select").html(text_country); 
	              	$("#payment_option span.select").html(""); //except payment method
	              			
					//select shipped						
					$("#shipping_country option").removeAttr("selected");
	              	$("#shipping_country option[value="+country+"]").attr("selected",true);
	              	
	              	//shipping address
	              	$("#country_sh option").removeAttr("selected");
	              	$("#country_sh option[value="+country+"]").attr("selected",true);
	              	
	              	
	              	//copy value
	              	$("input#firstname_sh").val($("input#firstname").val());
	              	$("input#lastname_sh").val($("input#lastname").val());
	              	$("input#address_sh").val($("input#address").val());
	              	$("input#second_address_sh").val($("input#second_address").val());
	              	$("input#third_address_sh").val($("input#third_address").val());
	              	$("input#city_sh").val($("input#city").val());
	              	$("input#region_sh").val($("input#region").val());
	              	$("input#postal_sh").val($("input#postal").val());
	              	$("input#primary_phone_sh").val($("input#primary_phone").val());
	              	$("input#alternative_phone_sh").val($("input#alternative_phone").val());
	              	$("input#email_sh").val($("input#email").val());  
					
	              	//disable form shipping
	              	$("#finputShipping input[type=text]").attr("disabled","disabled");
	              	//$("#finputShipping input[type=select]").attr("disabled","disabled");
				
				}else{
					$("#country_sh option").removeAttr("selected");
	              	$("#country_sh option[value="+country+"]").attr("selected",true);
	              	
	              	//remove copied value
	              	$("input#firstname_sh").val("");
	              	$("input#lastname_sh").val("");
	              	$("input#address_sh").val("");
	              	$("input#second_address_sh").val("");
	              	$("input#third_address_sh").val("");
	              	$("input#city_sh").val("");
	              	$("input#region_sh").val("");
	              	$("input#postal_sh").val("");
	              	$("input#primary_phone_sh").val("");
	              	$("input#alternative_phone_sh").val("");
	              	$("input#email_sh").val("");   

	              	//enable form shipping
	              	$("#finputShipping input[type=text]").removeAttr("disabled");
	              	
				}
				
				if (checked) {				
					$(".firstname_shformError").remove();
					$(".lastname_shformError").remove();
					$(".city_shformError").remove();
					$(".postal_shformError").remove();
					$(".primary_phone_shformError").remove();
					$(".email_shformError").remove();
					$(".address_shformError").remove();
					$(".country_shformError").remove();
						
				}else{
					$("#another_shipping_address").slideDown("fast");	
				}
				
			});
			
			
          	
			$(".class_country_sh").change(function(){
				var country=$(this).find("option:selected").val();
				var param = $(this).attr("rel");
				var checked = jQuery("[name=shipping-address]").attr("checked");
				var text_country = $(this).find("option:selected").text()
				 
				if(param=="middle"){
					$("#shipping_country option").removeAttr("selected");
	              	$("#shipping_country option[value="+country+"]").attr("selected",true);	              	
	              	//span that use as combo
					$("#shipping_country").next().html(text_country); 	           		
				}
				if(param=="bottom"){
					if(checked){
						$("#country option").removeAttr("selected");
	              		$("#country option[value="+country+"]").attr("selected",true);
	              		//span that use as combo
						$("#country").next().html(text_country); 
						
						$("#country_sh option").removeAttr("selected");
	              		$("#country_sh option[value="+country+"]").attr("selected",true);
	              		//span that use as combo
	              		$("#country_sh").next().html(text_country); 
					}else{
						$("#country_sh option").removeAttr("selected");
	              		$("#country_sh option[value="+country+"]").attr("selected",true);
	              		//span that use as combo
	              		$("#country_sh").next().html(text_country); 
					}
				}
				if(param=="top" && checked){
					$("#country_sh option").removeAttr("selected");
	              	$("#country_sh option[value="+country+"]").attr("selected",true);
	              	//span that use as combo
	              	$("#country_sh").next().html(text_country); 	
				
					$("#shipping_country option").removeAttr("selected");
              		$("#shipping_country option[value="+country+"]").attr("selected",true);
              		$("#shipping_country").next().html(text_country); 
				}
				
				
				
				var selCountry = $("#shipping_country").val();
				var id_so 	= $("#shipping_option option:selected").val();
				var thaUrl	= "http://'.SITE_URL.'/front-product-ajax";
				
				if(trim(id_so)==""){
				
				}else{
					jQuery.post(thaUrl,{ 
			    		pKEY:"shipping_to_country",
			    		id_country:country,
					    id_shipping_option : id_so,		    		
			    		parameter:param
		              },function(data){
		              	var test=data.split("&");
		              	$("#shipped_country").html(test[1]);
		              	$("#tax_price").html(test[0]);
		              	$("#grand_total").html(test[2]);
		              	$("#shipping_taxes").html(test[4]);
		              	
					});
				}
				
			});
			
		  })
		  
		  //shipping_option changed
		  $("#shipping_option").change(function(){
				var country	= $("#shipping_country option:selected").val();
				var id_so 	= $("#shipping_option option:selected").val();
				var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
				
				if(trim(country)==""){						
					alert("Please select country first!");
				}else{
					jQuery.post(thaUrl,{ 
			    		pKEY:"ship_to_country_with_shipping_option",
			    		id_country:country,
			    		id_shipping_option : id_so
		              },function(data){
		              	var test=data.split("&");
		              	$("#shipped_country").html(test[1]);
		              	$("#tax_price").html(test[0]);
		              	$("#grand_total").html(test[2]);
		              	$("#shipping_taxes").html(test[4]);       	
					});
				}
			});
		  
		  
		  //submit
		  $(function(){
		  	$("#form-order").validationEngine();
		  	
		  	$("#shipping-address-term").click(function(){
				$(".Error").css("display","none");
			});
		  	
			var min_subtotal="'.$error_minimum_total.'";
			$("#form-order").submit(function(){
				var str = $("#form-order").serialize();
				var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
				var the_check=$("#shipping-address:checked").val();
				var term=$("#shipping-address-term:checked").val();
				 if($("#form-order").validationEngine("validate") == true){
				 	if(min_subtotal == false){
					 	if(term == 1){
					 		var currentHtml = $("#prosesing_indikator").html();
					 		$("#prosesing_indikator").fadeIn();
					 		$("input#true_submit").attr("disabled","disabled");
							jQuery.post(thaUrl,{ 
					    		pKEY:"confirm_order",
					    		lvalue:str,
					    		same:the_check
				              },function(data){				             
					              	if (trim(data)=="emailRegister"){					              		
					              		//$("#prosesing_indikator").fadeOut();
					              		$("#prosesing_indikator").css("color","red");
					              		$("#prosesing_indikator").css("margin-left","-80px");
					              		$("#prosesing_indikator").html ("Your email is already register. Please login!");
					              		$("#prosesing_indikator").fadeIn().delay(3000).fadeOut("slow",function() {
    										$("input#true_submit").removeAttr("disabled");	
					              			$("#prosesing_indikator").html(currentHtml);
  										});
											
										
					              				              		
					              	}else{
						              	var the_parameter=data.split("&");
						              	if(trim(the_parameter[0])=="payments_paypal_standard"){
											document.location="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/paypal.php?ns="+the_parameter[1];
										}
										if(trim(the_parameter[0])=="payments_bank_transfer"){
											document.location="http://'.SITE_URL.'/confirmation-order";
										}
										if(trim(the_parameter[0])=="payments_cheque"){
											document.location="http://'.SITE_URL.'/confirmation-order";
										}	
									}
							});
							return false;
						}else{
							var thaUrl 	= "";
							$(".Error").css("display","");
							$(".error_subtotal").css("display","");
							return false;
						}
					}else{
						var thaUrl 	= "";
						$(".error_subtotal").css("display","");
						return false;
					}
				  }
			});
		  });
          </script>';
	$items .= '<div class="space50"></div>';
	return $items;
}

function list_chart_shipping_detail($app_name){
	$items='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery.confirm.js"></script>';
	$items.='<ul class="shopping-cart">';
	$items.='<li><ul><li> Item </li><li> Unit Price </li><li> Qty </li><li> Cost </li><li> </li></ul></li>';
	$data_currency=get_symbol_currency(); if ($data_currency=="USD"){$data_currency='$';}
	$nominal_min_price=0;
	if(is_array($_SESSION['cart'])){
		foreach($_SESSION['cart'] as $product ) {
			$data_product=get_detail_product($product['id']);
			$the_images=get_product_detail_images($product['id']);
			$additional_product=default_price($product['id']);
			if(empty($product['parent'])){
				$the_price=$additional_product['price'];
				$product['parent']='';
				$product['child'] = '';
				$variant_rule = "";
			}else{
				$the_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				$variant_rule = find_rules_name($product['parent'],0,'product').' : '.find_rules_name($product['child'],$product['parent'],'product');
			}

			$price_per_item = $data_currency.''.number_format($the_price,2,'.','');
			
			///check item price by qty [yana]
				if (check_item_price($product['id'], $product['qty'])!=false){
					$the_price		= number_format($the_price,2,'.','');
					$the_price_new  = check_item_price($product['id'], $product['qty']);
					$the_price_new  = number_format($the_price_new,2,'.','');
					
					if ($the_price_new==$the_price){
						$price_per_item = $data_currency.''.number_format($the_price,2,'.',''); 
					}else{
						$price_per_item = '<font style="text-decoration:line-through;color:red;"><font style="color:#333;">'.$data_currency.''.number_format($the_price,2,'.','').'</font></font><br>';
						$price_per_item .= $data_currency.''.number_format($the_price_new,2,'.','');
						$the_price = $the_price_new;
					}
				}else{
					$price_per_item = $data_currency.''.number_format($the_price,2,'.','');
				}
			///			
			
			$total_this_product=$the_price*$product['qty'];
			
			$items.='<li class="list-cart" id="list-cart_'.$product['id'].'">
						<ul>
		                	<li> <img src="http://'.site_url().'/app/tb/tb.php?w=50&h=50&src='.$the_images.'" />
		                    	'.$data_product['larticle_title'].'</li>
		                    <li>'.$price_per_item.'</li>
		                    <li>'.$product['qty'].'</li>
		                    <li>'.$data_currency.''.number_format($total_this_product,2,'.','').'</li>
		                    <li>
		                    	<a id="del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'">
		                        	<img src="http://'.TEMPLATE_URL.'/images/icon_del.png" />
		                        </a>
		                        
		                        <script>
									$(document).ready(function(){	
										$("#del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").click(function(){
											var value = "'.$product['id'].'";
											var parent_id="'.$product['parent'].'";
											var child_id="'.$product['child'].'";
											var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
											jQuery.post(thaUrl,{ 
									    		pKEY:"del_chart_list",
									    		product_id:value,
									    		parent:parent_id,
									    		child:child_id,
									    		keyword:"billing"
								              },function(data){
								              	if(data==2){
								              		document.location="http://'.SITE_URL.'/";
												}else{
													$("#review_order").html(data);
												}
								              	
											});
										});
										
										$("a#del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").confirm({ 
										 	msg:"<span style=\"color:#5C5423\">Delete this item? </span>"
										});											
										
									});
								</script>
		                        
		                    </li>
		                 </ul>
	                </li>';					
		}
		$items.='</ul>';
		
		$items.='<ul class="sc-calculate">';
		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==1){
			$product_tax = 0.00;
			$vat='<div class="include_price standard-font-bold">Taxes are included in all prices</div>';
		}else{
			$product_tax = calculate_tax($_SESSION['cart'],$_SESSION['country']);
			$vat= '<ul><li>VAT</li><li>'.$data_currency.'<span id="tax_price">'.number_format($product_tax,2,'.','').'</span></li></ul>';
		}
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes= (get_shipped_default($_SESSION['country'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
		}else{
			$shipped_taxes=0;
		}

		if(!empty($_SESSION['country']) && !empty($_SESSION['shipping_option']) ){
			$ship_taxes = get_shipped_default($_SESSION['country'],$_SESSION['shipping_option']);
		}elseif(!empty($_SESSION['country'])){
			$ship_taxes = get_shipped_default($_SESSION['country']);
		}else{
			$ship_taxes='0.00';
		}
		
		$total=$_SESSION['total_price'] + $ship_taxes + $product_tax + $shipped_taxes;
		$items.=' <li> 
						<ul> 
							<li>Sub Total </li>
                        	<li>'.$data_currency.''.number_format($_SESSION['total_price'],2,'.','').'                        	
		                        <div class="Error error_subtotal" style="position: absolute; text-align: left; z-index: 1200; float: left; right: 70px; bottom: 480px;display:none">
							        <div class="ErrorContent">
							        	The subtotal your order does not reach the minimum order limit!
							        	<br>
							    	</div>
							    </div>
							</li>
                    	</ul>
                 </li>
	             <li>       
                    	<ul>
                    		<li> Shipped to 
                    			 <div class="c-shipping">
                    			 	<span id="view_select">
                    			 	<select name="shipping_country" id="shipping_country" class="validate[required] class_country_sh select" rel="bottom">'.country_shipping().'</select>
                    			 	</span>
                    			 	
                    			 	<select name="shipping_option"  id="shipping_option" class="select validate[required]">
		                					<option value="">Shipping option</option>
		                					'.shipping_option().'
		                				</select>
                    			 </div>
                    			 
                    		</li>
                        	<li>'.$data_currency.'<span id="shipped_country">'.$ship_taxes.'</span></li>
                       </ul>
                 </li>
                 
                 <li>'.$vat.'</li>
                 
                 <li>
                 	<ul>
                 		<li>Shipping Tax </li>
                 		<li>'.$data_currency.'<span id="shipping_taxes">'.number_format($shipped_taxes,2,'.','').'<span></li>
                 	</ul>
                 </li>
                 
                
                 <li style="display:none;">
                 	<ul>
                 		<li>Payment Method</li>
                 		<li><div class="c-shipping" id="payment_option"><select name="payment_methode" title="Payment Method" id="payment_methode" class="validate[required] select" style="height: 24px;padding: 1px;">'.payment_option().'</select></div></li>
                 	</ul>
                 </li>
                 
                 
                <li>
                 	<ul>
                 		<li> Grand Total </li>
                        <li><span class="gt">'.$data_currency.'<span id="grand_total">'.number_format($total,2,'.','').'</span></span></li>
                  	</ul>
                 </li>';
		$items.='</ul>';
                 
        $items.='<div class="c-checkout">         
                   
                     <div style="text-align: right; top: -100px; height: 15px;">
                            <div class="Error" style="position: absolute;text-align: left;padding-left: 430px; top:5px; z-index: 1200;left:-50px;display: none;">
					        <div class="ErrorContent">
					        * Please check the box to provide your consent.
					        <br>
					        </div>
					    </div>
					 </div>
					 
					 <span class="terms">                   	
	                        <input type="checkbox" name="shipping-address-term" id="shipping-address-term" value="1" class="validate[funcCall[getALertTerms]]" /><span class="additional-fontsize">
	                        <span> I have read and agree with the <a href="http://'.site_url().'/terms-conditions" target="_blank"><b>Terms and Conditions</b></a></span>
                     </span>
                     '.get_user_agreement().'           

                     <span id="prosesing_indikator" style="display: none; color: purple; position: absolute; left: 16px; margin-top: 10px;">
							<img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif">
							Processing...
					 </span>
               	  	 <input type="submit" class="button-checkout-bottom2 btn-paypal" name="submit" id="true_submit" value="">
	                   
                 </div>';
	}	
	return $items;
}

function country_member(){
	if(isset($_SESSION['country'])){
		$result=select_country_shipped($_SESSION['country']);
	}else{
		$result='';
	}
	return $result;	
}

function country_shipping(){
	if(isset($_SESSION['country'])){
		$result=select_country_shipped($_SESSION['country']);
	}else{
		$result='';
	}
	return $result;
}

function country_shipping2(){
	if(isset($_SESSION['country'])){
		$result=select_country_shipped($_SESSION['country']);
	}else{
		$result='';
	}
	return $result;	
}
function shipping_option($id=''){
	global $db;
	$result='';	
	$sql=$db->prepare_query("SELECT * from lumonata_shipping_option order by lorder ASC");
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){	
		$selected = '';
		if(isset($_SESSION['shipping_option']) && $data['lshipping_option_id']==$_SESSION['shipping_option']){
			$selected = 'selected';
		}	
		$result.='<option '.$selected.' value="'.$data['lshipping_option_id'].'">'.$data['loption'].'</option>';		
	}
	return $result;
}
function get_chart_list($app_name){
	cart();
	global $db;
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'My Cart';   
    $actions->action['meta_title']['args'][0] = '';	
	$items='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery.confirm.js"></script>';
	$items.='<div class="t-sc">SHOPPING CART</div> <div id="listed">';
	
	$items.=list_chart($app_name);
	if($_SESSION['error']){
		$items.='<script>
				$(document).ready(function(){
					jQuery("#checkout_link").click();
				});
				</script>';
		$_SESSION['error']=false;
	}
	$items.='</div>';
	return $items;
}

function list_chart($app_name,$element_id=''){
	if(!isset($_SESSION['country'])){
		$_SESSION['country']='';
	}

	$items='<div id="listed">';
	$items='';
	$items.='<ul class="shopping-cart"> 
       		<li> 
            	<ul>
                	<li> Item </li>
                    <li> Unit Price </li>
                    <li> Qty </li>
                    <li style="text-align: center;"> Cost </li>
                    <li>&nbsp;</li>
                </ul>
            </li>
			';
	if(!empty($element_id)){
		$items.='<script>
			$(document).ready(function(){
				$(".error_add_'.$element_id.'").css("display","");
				$(".error_add_'.$element_id.'").fadeIn(200).delay(5000).fadeOut(200);
			})
		</script>';
	}
	$data_currency=get_symbol_currency(); if ($data_currency=="USD") $data_currency = "$";
	$loop= 0;
	$x=0;
	if(is_array($_SESSION['cart'])){
		foreach($_SESSION['cart'] as $product ) {
			$data_product=get_detail_product($product['id']);
			$additional_product=default_price($product['id']);
			$the_images=get_product_detail_images($product['id']);
			if(empty($product['parent'])){
				$the_price=$additional_product['price'];
				$variant_type='';
				$product['parent'] = '';
				$product['child'] = '';
			}else{
				$the_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				$variant_type = find_rules_name($product['parent'],0,'product').' : '.find_rules_name($product['child'],$product['parent'],'product');
			}
			
			$price_per_item = $data_currency.''.number_format($the_price,2,'.','');
			
			///check item price by qty [yana]
				if (check_item_price($product['id'], $product['qty'])!=false){
					$the_price		= number_format($the_price,2,'.','');
					$the_price_new  = check_item_price($product['id'], $product['qty']);
					$the_price_new  = number_format($the_price_new,2,'.','');
					
					if ($the_price_new==$the_price){
						$price_per_item = $data_currency.''.number_format($the_price,2,'.',''); 
					}else{
						$price_per_item = '<font style="text-decoration:line-through;color:red;"><font style="color:#333;">'.$data_currency.''.number_format($the_price,2,'.','').'</font></font><br>';
						$price_per_item .= $data_currency.''.number_format($the_price_new,2,'.','');
						$the_price = $the_price_new;
					}
				}else{
					$price_per_item = $data_currency.''.number_format($the_price,2,'.','');
				}
			///			
			
			$total_this_product=$the_price*$product['qty'];
			$testing = fetch_rule_relationship('app_id='.$product['id']);//id category
			$cat = fetch_rule('rule_id='.$testing[0]);
			$the_cat_name = $cat['lname'];			
			
			
			$items.='<li class="list-cart" id="list-cart_'.$product['id'].'">
						<ul>
						
	                	<li>
	                		<img src="http://'.site_url().'/app/tb/tb.php?w=50&h=50&src='.$the_images.'" /> '.$data_product['larticle_title'].'
	                    </li>
	                    		
	                    <li>'.$price_per_item.'</li>
	                    
	                    <li>	                     
	                    	<input type="text" id="text_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'" name="qty" value="'.$product['qty'].'" size="3" class="txt-qty">
	                    	<button class="btn-qty" id="update_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'">Update</button>
	                    	<div class="Error error_add_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'" style="position: absolute;  top: 29px;text-align: left; z-index: 1200;display:none ;">
						        <div class="ErrorContent" style="width: 110px;">
						        	Product Out Of Stock
						        	<br>
						       </div>
						   </div>                   
	                    </li> 
	                    
	                    <li>
	                    	'.$data_currency.''.number_format($total_this_product,2,'.','').' 
		                    <div class="Error error_purchase_'.$testing[0].'" style="position: absolute; text-align: left; z-index: 1200; float: left;display: none;">
						        <div class="ErrorContent">
						        	The '.$the_cat_name.' category your order does not reach the minimum order limit!
						        	<br>
						        </div>
						    </div>						   
					    </li>
					    
					    <li>	                   
	                    	<a id="del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'">
	                        	<img src="http://'.TEMPLATE_URL.'/images/icon_del.png" />
	                        </a>
	                    </li>
	                    </ul>
	                
	                ';
			$items.='
					<script>
						$(document).ready(function(){	
							$("#del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").click(function(){
								var value = "'.$product['id'].'";
								var parent_id="'.$product['parent'].'";
								var child_id="'.$product['child'].'";
								var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
								jQuery.post(thaUrl,{ 
						    		pKEY:"del_chart_list",
						    		product_id:value,
						    		parent:parent_id,
						    		child:child_id,
						    		keyword:"chart"
					              },function(data){

					              	if(data==2){
					              		document.location="http://'.SITE_URL.'/";
									}else{
										$("#listed").html(data); 
									}
					              	
								});
							});
							
							$("a#del_product_detail_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").confirm({ 
							 	msg:"<span style=\"color:#666;font-weight:bold;\">Delete this item? </span>"
							});	
							
							$("#update_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").click(function(){
								var up_product_id = "'.$product['id'].'";
								var up_parent_id="'.$product['parent'].'";
								var up_child_id="'.$product['child'].'";
								var text_qty=$("#text_'.$product['id'].'_'.$product['parent'].'_'.$product['child'].'").val();
								var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
								jQuery.post(thaUrl,{ 
						    		pKEY:"update_chart_list",
						    		product_id:up_product_id,
						    		parent:up_parent_id,
						    		child:up_child_id,
						    		qty_value:text_qty
					              },function(data){
					              	$("#listed").html(data);          	
								});
							});
						});
					</script>';
			 $items.='</li>';
		}//end foreach item
		$items.='</ul>';

		
		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==1){
			$product_tax = 0.00;
			$vat='<div class="include_price standard-font-bold">Taxes are included in all prices</div>';
		}else{
			$product_tax = calculate_tax($_SESSION['cart'],$_SESSION['country']);
			$vat= '<ul> <li>VAT</li> <li>'.$data_currency.'<span id="tax_price">'.number_format($product_tax,2,'.','').'</span></li></ul>';
		}
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes= (get_shipped_default($_SESSION['country'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
		}else{
			$shipped_taxes=0;
		}
		
		if(!empty($_SESSION['country'])){
			$ship_taxes = get_shipped_default($_SESSION['country']);
		}else{
			$ship_taxes='0.00';
		}
		
		$total=$_SESSION['total_price'] + $ship_taxes + $product_tax + $shipped_taxes;
		$items.=' <style>
					.Error .ErrorContent, .Error .error_subtotal {
					    background: none repeat scroll 0 0 #EE0101;
					    border: 1px solid #DDDDDD;
					    border-radius: 5px 5px 5px 5px;
					    box-shadow: 0 0 6px #000000;
					    color: #FFFFFF;
					    font-family: tahoma;
					    font-size: 11px;
					    left: -20px;
					    line-height: 11px;
					    padding: 4px 5px;
					    position: relative;
					    top: 2px;
					    width: 190px;
					    z-index: 5001;
					}
          </style>';
		$items.='  <ul class="sc-calculate">
                	<li>
	                		<ul>
		                		<li> Sub Total </li>
		                		<li>'.$data_currency.''.number_format($_SESSION['total_price'],2,'.','').'
			                        <div class="Error error_subtotal" style="position: absolute; text-align: left; z-index: 1200; float: left; right: 70px; bottom: 350px;display:none">
								        <div class="ErrorContent">
								        	The subtotal your order does not reach the minimum order limit!
								        	<br>
								    	</div>
								    </div>
							    </li>
		                    </ul>
                    </li>
                    <li>
		                   <ul>
		                		<li> Shipped to 
		                			<div class="c-shipping"> 
		                				<select name="shipping_country" id="shipping_country" class="select validate[required]">'.country_shipping().'</select>
		                				<select name="shipping_option"  id="shipping_option" class="select validate[required]">
		                					<option value="">Shipping option</option>
		                					'.shipping_option().'
		                				</select>
		                			</div>
		                		</li>
		                        <li>'.$data_currency.'<span id="shipped_country">'.$ship_taxes.'</span></li>
		                    </ul>
                    </li>
                    
                    <li>
                    	'.$vat.'			                  
                     </li>
                    
                    <li>
                    		<ul>			                	
			                    <li>Shipping Tax </li>
			                    <li>'.$data_currency.'<span id="shipping_taxes">'.number_format($shipped_taxes,2,'.','').'</span></li>			                  	
			                </ul>
                  	</li>
                  	
                  	<li>
		                   <ul>
		                		<li>Total </li>
		                        <li>
		                        	<span class="gt">'.$data_currency.'<span id="grand_total">'.number_format($total,2,'.','').'</span></span>
		                        	<div class="Error"><div class="error_subtotal" style="display:none;">The number of orders is less than our minimum order charge</div></div>
		                        </li>
		                  </ul>
                  	</li>
                 </ul> <!--/sc-calculate-->
				';
		
		$items.='<div class="c-checkout">
					<a href="http://'.SITE_URL.'/products/"> &laquo; Continue Shopping</a>
					<span id="prosesing_indikator" style="display: none; color: purple; position: absolute; padding: 5px 10px; right: 300px;">
						<img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif">
						Processing...
					</span>
                    <input type="button" value="Check Out" class="btn-checkout" id="checkout_link">				
                 </div>';
		$items.='<div style="height:50px;">
              <script>
              $(document).ready(function(){
	              $("#shipping_country").change(function(){
						var country=$("#shipping_country option:selected").val();
						var id_so 	= $("#shipping_option option:selected").val();
						var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
						if(trim(id_so)==""){						
							
						}else{							
							jQuery.post(thaUrl,{ 
					    		pKEY:"shipping_to_country1",
					    		id_country:country,
					    		id_shipping_option : id_so
				              },function(data){
				              	var test=data.split("&");
				              	$("#shipped_country").html(test[1]);
				              	$("#tax_price").html(test[0]);
				              	$("#grand_total").html(test[2]);
				              	$("#shipping_taxes").html(test[4]);       	
							});
						}
					});
					
					$("#shipping_option").change(function(){
						var country	= $("#shipping_country option:selected").val();
						var id_so 	= $("#shipping_option option:selected").val();
						var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
						
						if(trim(country)==""){						
							alert("Please select country first!");
						}else{
							jQuery.post(thaUrl,{ 
					    		pKEY:"ship_to_country_with_shipping_option",
					    		id_country:country,
					    		id_shipping_option : id_so
				              },function(data){
				              	var test=data.split("&");
				              	$("#shipped_country").html(test[1]);
				              	$("#tax_price").html(test[0]);
				              	$("#grand_total").html(test[2]);
				              	$("#shipping_taxes").html(test[4]);       	
							});
						}
					});
					
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
					
					$("#checkout_link").click(function(){
						$("#prosesing_indikator").fadeIn();
						$("input#checkout_link").attr("disabled","disabled");
						var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
						jQuery.post(thaUrl,{ 
				    		pKEY:"checkout_minimum_price"
			              },function(data){
			              	value=data.split("&");
			              if(trim(value[0])=="1"){
			              	$(".error_purchase_"+value[1]).css("display","");   	
							$(".error_purchase_"+value[1]).fadeIn(200).delay(5000).fadeOut(200);
							
							$("#prosesing_indikator").fadeOut();
							$("input#checkout_link").removeAttr("disabled");
						  }
						  if(trim(value[0])=="2"){
						  	$(".error_subtotal").css("display","");   	
							$(".error_subtotal").fadeIn(200).delay(5000).fadeOut(200);
							
							$("#prosesing_indikator").fadeOut();
							$("input#checkout_link").removeAttr("disabled");
						  } 
						  if(trim(value[0])=="3"){
							document.location="http://'.SITE_URL.'/shipping-detail/";
						  }
						});
					})
			  })
              </script> </div>  
              ';
	}
	
	return $items;
}

function select_country_shipped($country_id=''){
	global $db;
	$result=''; 
	$opt_group  = '<optgroup style="border-bottom:1px solid #BDD9FF;">';
	$opt		= '';
	$result='<option value="">select country</option>';
	$sql=$db->prepare_query("SELECT * from lumonata_country where lpublish=%s order by lorder_id, lcountry ASC",1);
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){
		if($country_id==$data['lcountry_id']){
			$q="selected='selected'";
		}else{
			$q='';
		}
		if($data['lcountry_id']=='247'){ // rest the world
			
		}else{
			if ($data['lshow_group']==1){
				$opt_group.='<option value="'.$data['lcountry_id'].'" '.$q.'>'.$data['lcountry'].'</option>';
			}else{
				$opt.='<option value="'.$data['lcountry_id'].'" '.$q.'>'.$data['lcountry'].'</option>';
			}
		}
	}
	$opt_group .= '</optgroup>';
	$result .= $opt_group.$opt;
	return $result;
}

 function find_rules_name($rule_id,$parent_id,$group){
        global $db;
        $result='';
        $sql=$db->prepare_query("SELECT lname FROM lumonata_rules WHERE lrule_id=%d AND lparent=%d AND lgroup=%s",$rule_id,$parent_id,$group);
        
        $r=$db->do_query($sql);
        $d=$db->fetch_array($r);
            $result=$d['lname'];
        return $result;
    }

function get_detail_product($product_id){
	global $db;
	/*ori
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.larticle_id=%d AND 
                                 c.larticle_status=%s",
                                'categories','products',0,$product_id,'publish');
    */
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.larticle_id=%d AND 
                                 c.larticle_status=%s",
                                'categories','products',$product_id,'publish');
	$result=$db->do_query($query);
	$data=$db->fetch_array($result);
	return $data;
}

function get_product_detail_images($product_id){
	global $db;
 	$sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='product_image_thumbnail'",$product_id);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_name=$data_product_images['lattach_loc_thumb'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
       
       return $images_url;
}

function get_product_variant_detail($product_id,$parent_id,$child_id){
	global $db;
	$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$product_id,'product_variant','products');
      $result_variant=$db->do_query($sql_variant);
      $data_variant=$db->fetch_array($result_variant);
      //echo $data_variant['lvalue'];
      $obj_variant = json_decode( $data_variant['lvalue'],true);
      $count_it=0;
        if(is_array($obj_variant)){
		      foreach($obj_variant['parent_variant'] as $key=>$val){
			    $sql_added=$db->prepare_query("select * from lumonata_rules
			                where
			                lrule_id = %d", $val);
				$execute_added=$db->do_query($sql_added);
				$data_added=$db->fetch_array($execute_added);
				$varian_idparent[$count_it]=$val;
				$varian_parent[$count_it]=$data_added['lname'];
				$j=0;
				if($varian_idparent[$count_it] == $parent_id){
				      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
				      			if($subkey == $child_id){
				      				$the_value[$j]=$subval[0];
					      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
						                where
						                lparent = %d and lrule_id= %s",$val, $subkey);
					   				$execute_added_child=$db->do_query($sql_added_child);
					   				$data_added_child=$db->fetch_array($execute_added_child);
					   				$the_var_id[$j]=$subkey;
					   				$the_var[$j]=$data_added_child['lname'];
					      			$j++;
					      			
				      			}
				      	}
				      	break;
				}
		      	$count_it++;
      		}
   		}
   	return $the_value[0];
}

function get_shipped_default($country,$shipping_option=''){
		global $db;
  		$query_shpping=get_shipping_price($country,$shipping_option);
  		$shipping_price='';
  		$row_shipped=$db->num_rows($query_shpping);
  		$shipping_charge=$db->fetch_array($query_shpping);
  		if($row_shipped<>0){
  			if($shipping_charge['lshipping_type']=='price'){
  				$var_compare = $_SESSION['total_price'];
  			}elseif($shipping_charge['lshipping_type']=='weight'){
  				$var_compare = $_SESSION['total_weight'];
  			}else{
  				$var_compare = $_SESSION['total_items'];
  			}
  			
  			$y=0;
			$query_shpping2=get_shipping_price($country,$shipping_option);
  			while($shipping_charge2=$db->fetch_array($query_shpping2)){
  				$tmp[$y] = $shipping_charge2['lformula'];
  				$tmp_price[$y] = $shipping_charge2['lprice'];
  				$y++;
  			}

  			for($i=0;$i<$y;$i++){
  					if(isset($tmp[$i+1])){
  						if($tmp[$i] <= $var_compare and $var_compare <= $tmp[$i+1]){
  							$shipping_price=$tmp_price[$i];
  						}
  					}
  			}
  			
  			if($shipping_price==''){
  				$shipping_price=$tmp_price[$y-1];
  			}
  			
  		}else{
  			
  			$query_shipping_rest_the_wold=get_shipping_price('247',$shipping_option);
  			$row_rest_the_world=$db->num_rows($query_shipping_rest_the_wold);
  			$shipping_charge_rest_the_wold=$db->fetch_array($query_shipping_rest_the_wold);
  			if($shipping_charge_rest_the_wold > 1){
  				if($shipping_charge_rest_the_wold['lshipping_type']=='price'){
  					$var_compare = $_SESSION['total_price'];
  				}elseif($shipping_charge_rest_the_wold['lshipping_type']=='weight'){
  					$var_compare = $_SESSION['total_weight'];
  				}else{
  					$var_compare = $_SESSION['total_items'];
  				}
				$y=0;
				$query_shipping_rest_the_wold2=get_shipping_price('247',$shipping_option);
  				while($row_rest_the_world2=$db->fetch_array($query_shipping_rest_the_wold2)){
  					$tmp[$y] = $row_rest_the_world2['lformula'];
  					$tmp_price[$y] = $row_rest_the_world2['lprice'];
  					$y++;
  				}

  				for($i=0;$i<$y;$i++){
  						if(isset($tmp[$i+1])){
	  						if($tmp[$i] <= $var_compare and $var_compare <= $tmp[$i+1]){
	  							$shipping_price=$tmp_price[$i];
	  						}
  						}
  				}
  				
  				if($shipping_price==''){
  					$shipping_price=$tmp_price[$y-1];
  				}
  				
  			}else{
  				$shipping_price=$shipping_charge_rest_the_wold['lprice'];
  			}
  		}
  		return $shipping_price;
}

function tax_setting(){
	global $db;
	
	$record=$db->prepare_query("SELECT * FROM lumonata_tax");
    $query=$db->do_query($record);
    $result=$db->fetch_array($query);
    
   return $result;
}

function default_price($product_id){
	global $db;
	$sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$product_id,'product_additional','products');
	       $result_stok=$db->do_query($sql_stock);
	       $data_stok=$db->fetch_array($result_stok);
	       $obj_stok = json_decode($data_stok['lvalue'],true);
	return $obj_stok;
}

//SET CHART SESSION
function cart(){ 
	$time = time(); 
	$expire = $time + 3600; 

	// If the session variables haven't been registered yet, do that now. 
	if(!isset($_SESSION['cart'])) { 
		
		if(isset($_COOKIE['cart'])) { 
			
			$_SESSION['cart'] = array(); 
			$_SESSION['cart'] = unserialize(stripslashes($_COOKIE['cart'])); 
			$_SESSION['total_items'] = cart_calculate_items($_SESSION['cart']); 
			$_SESSION['total_price'] = cart_calculate_price($_SESSION['cart']);
			$_SESSION['total_weight'] = cart_calculate_price($_SESSION['cart']);
			if(!empty($_SESSION['country'])){
				$_SESSION['country']=$_SESSION['country'];
			}else{
				$_SESSION['country']='';
			}
			$_SESSION['idproduct']=array();	
		} else { 
			$_SESSION['cart'] = array(); 
			$_SESSION['total_items'] = 0; 
			$_SESSION['total_price'] = 0.00;
			$_SESSION['total_weight'] = 0;
			$_SESSION['country'] = '';
			$_SESSION['idproduct']=array();

			$s_cart = serialize($_SESSION['cart']); 
			setcookie('cart', $s_cart, $expire); 
		} 
	} 

		return $_SESSION;
} 
    
//ADD TO CHART HERE
function cart_add_item($item,$parent='',$child='',$qty,$sef_cat=''){			
			if(count($_SESSION['cart']) > 0){				
				$exist = false;
				$j = 0;
				foreach( $_SESSION['cart'] as $product ){
					if( $product['id']==$item && $product['parent']==$parent && $product['child']==$child ){
						$exist = true;
						$sel = $j;
						$_SESSION['cart'][$sel]['qty'] += $qty;
					}
					$j++;
				}
				if($exist==true){
					//$_SESSION['cart'][$sel]['qty'] += $qty;
				}else{
					$numItems = count($_SESSION['cart']);
					$newItems = $numItems;
					$_SESSION['cart'][$newItems]['id']   = $item;
					$_SESSION['cart'][$newItems]['cat']  = $sef_cat;
					if($parent<>''){
						$_SESSION['cart'][$newItems]['parent'] = $parent;
						$_SESSION['cart'][$newItems]['child']  = $child;
					}
					$_SESSION['cart'][$newItems]['qty']  = $qty;
				}
			}else{
				$i = 0;
				/*
				$i = count($_SESSION['cart']);
				if($i<0){$i=0;}
				*/
				$_SESSION['cart'][$i]['id']   = $item;
				$_SESSION['cart'][$i]['cat']  = $sef_cat;
				if($parent<>''){
					$_SESSION['cart'][$i]['parent'] = $parent;
					$_SESSION['cart'][$i]['child']  = $child;
				}
				$_SESSION['cart'][$i]['qty']  = $qty;
				
			}

			$_SESSION['total_items'] = cart_calculate_items($_SESSION['cart']); 
			$_SESSION['total_price'] = cart_calculate_price($_SESSION['cart']); 
			$_SESSION['total_weight'] = cart_calculate_weight($_SESSION['cart']);
			$s_cart = serialize($_SESSION['cart']); 
			$expire=3600;
			setcookie('cart', $s_cart, $expire);
			
		return $_SESSION['cart'];
	} 

//CALCULATE ITEM
	function cart_calculate_items($cart) { 
    // Initialise variable to default value 
    $items = 0; 
	//print_r($_SESSION);
    //echo $cart;
    // Make sure the cart is actully an array! 
    if(is_array($cart)) 
        { 
        // Loop through the cart and add the quanity of each item the the total 
        foreach($_SESSION['cart'] as $product) 
            { 
			//echo $qty;
            //$items += $qty; 
				$items += $product['qty'];
				//echo $items;
            } 
        } 
    // Return the total number of items 
    return $items; 
    }
    
//CALCULATE PRICE
function cart_calculate_price($cart) {
	global $db;
	
    // Initialise variable to default value 
    $price = 0; 
    
    // Make sure the cart is actully an array! 
    if(is_array($cart)) { 
      foreach($_SESSION['cart'] as $product){ 
            
           $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$product['id'],'product_additional','products');
	       $result_stok=$db->do_query($sql_stock);
	       $data_stok=$db->fetch_array($result_stok);
	       $obj_stok = json_decode($data_stok['lvalue'],true);
	       $the_product_price=$obj_stok['price'];
            
     	   $sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$product['id'],'product_variant','products');
	      $result_variant=$db->do_query($sql_variant);
	      $data_variant=$db->fetch_array($result_variant);
	      $obj_variant = json_decode( $data_variant['lvalue'],true);
	      $count_it=0;
          if(is_array($obj_variant)){
	      			foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
			                where
			                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_idparent[$count_it]=$val;
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
					if($varian_idparent[$count_it] == $product['parent']){
					      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
					      			if($subkey == $product['child']){
					      				$the_value[$j]=$subval[0];
						      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
							                where
							                lparent = %d and lrule_id= %s",$val, $subkey);
						   				$execute_added_child=$db->do_query($sql_added_child);
						   				$data_added_child=$db->fetch_array($execute_added_child);
						   				$the_var_id[$j]=$subkey;
						   				$the_var[$j]=$data_added_child['lname'];
						      			$j++;
						      			
					      			}
					      	}
					      	break;
					}
			      	$count_it++;
			      }
		   }
            // Then adding the (item price * quantity) to the total price 
            // and starting the loop again                 
                
		   		if(empty($the_value[0])) {
		      		$additional_product=default_price($product['id']);
		      		$item_price = $additional_product['price'];
		      		$item_price = $item_price;
                }else{
                	$item_price = $the_value[0];
                }
				
		   		/// by yana
		   		$cip = check_item_price($product['id'],$product['qty']);
		   		if ($cip!=false){ 
		   			$item_price = $cip;
		   		}		   		
		   		///		
		   		   		
                $price += $item_price * $product['qty'] ; 
				
         }         
             
        }
    // Return the total price 
		return $price; 
}

function check_item_price($id,$qty){//by yana
	global $db;
	$q= $db->prepare_query("select lvalue 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$id,'product_price_range','products');
    $r= $db->do_query($q);
    $n= $db->num_rows($r);
    if ($n==0){
    	return false;
    }else{
    	 $d		=$db->fetch_array($r);
    	 $arr 	=json_decode($d['lvalue'],true);    	 
    	 
    	 foreach ($arr as $key => $price) {    
			list ($start,$end) = explode('-', $key);
			if ($end=='up'){ 
				if ($qty>=$start){
					return $price;
				}
			}else{	
				if ( ($qty>=$start) && ($qty<=$end) ){
					return $price;
				}
			}
		 }	    	 
    }
    return false;
} 
//CALCULATE Weight
function cart_calculate_weight($cart) {
	global $db;
    $item_weight=0;
    // Make sure the cart is actully an array! 
    if(is_array($cart)) { 
      foreach($_SESSION['cart'] as $product){ 
            
           $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$product['id'],'product_additional','products');
	       $result_stok=$db->do_query($sql_stock);
	       $data_stok=$db->fetch_array($result_stok);
	       $obj_stok = json_decode($data_stok['lvalue'],true);
	       $the_product_price=$obj_stok['price'];
	       $the_product_weight=$obj_stok['weight'];
             
            // Then adding the (item price * quantity) to the total price 
            //  and starting the loop again 
            	$item_weight = $item_weight + $the_product_weight;
				
         }         
             
    }
    // Return the total weight 
		return $item_weight; 
    }
    
function calculate_tax($cart,$country){
	global $db;
	$taxes = 0;
	if(is_array($cart)){ 
      foreach($_SESSION['cart'] as $product){ 
            
           $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$product['id'],'product_additional','products');
	       $result_stok=$db->do_query($sql_stock);
	       $data_stok=$db->fetch_array($result_stok);
	       $obj_stok = json_decode($data_stok['lvalue'],true);
	       
	       $the_product_price=$obj_stok['price'];
	       $the_product_tax=$obj_stok['tax'];
	       
	       	$sql_tax_detail=$db->prepare_query("select * from lumonata_tax_detail where lcountry_id=%s and ltax_groups_ID=%s",$country,$the_product_tax);
			$query_tax_detail=$db->do_query($sql_tax_detail);
			$row_tax_detail=$db->num_rows($query_tax_detail);
			if($row_tax_detail<>0){
				$data_tax_details=$db->fetch_array($query_tax_detail);
				$tax_item = $data_tax_details['ltax_charge'];
			}else{
				$sql_tax_detail2=$db->prepare_query("select * from lumonata_tax_detail where lcountry_id=%s and ltax_groups_ID=%s",'247',$the_product_tax);
				$query_tax_detail2=$db->do_query($sql_tax_detail2);
				$data_tax_details2=$db->fetch_array($query_tax_detail2);
				$tax_item= $data_tax_details2['ltax_charge'];;
			}
            
     	   $sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$product['id'],'product_variant','products');
	      $result_variant=$db->do_query($sql_variant);
	      $check_variant_rowss=$db->num_rows($result_variant);
	      
	      if($check_variant_rowss<>0){
		      $data_variant=$db->fetch_array($result_variant);
	
		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      $count_it=0;
	          if(is_array($obj_variant)){
		      			foreach($obj_variant['parent_variant'] as $key=>$val){
				      	$sql_added=$db->prepare_query("select * from lumonata_rules
				                where
				                lrule_id = %d", $val);
						$execute_added=$db->do_query($sql_added);
						$data_added=$db->fetch_array($execute_added);
						$varian_idparent[$count_it]=$val;
						$varian_parent[$count_it]=$data_added['lname'];
						$j=0;
						if($varian_idparent[$count_it] == $product['parent']){
						      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
						      			if($subkey == $product['child']){
						      				$the_value[$j]=$subval[0];
							      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
								                where
								                lparent = %d and lrule_id= %s",$val, $subkey);
							   				$execute_added_child=$db->do_query($sql_added_child);
							   				$data_added_child=$db->fetch_array($execute_added_child);
							   				$the_var_id[$j]=$subkey;
							   				$the_var[$j]=$data_added_child['lname'];
							      			$j++;
							      			
						      			}
						      	}
						      	break;
						}
				      	$count_it++;
				      }
			   }
	           
			   //  and starting the loop again 
	           $item_price = $the_value[0]; 
	      }else{
	      		$additional_product=default_price($product['id']);
	      		$item_price = $additional_product['price'];
	      }
           $taxes += ($item_price * $product['qty'])*($tax_item/100) ; 	
         }         
	}
	return $taxes;
}

function calculate_shipping_tax($cart,$country){
	global $db;
  	$query_shpping=get_shipping_price($country);
  	$shipping_price='';
  	$row_shipped=$db->num_rows($query_shpping);
  	$shipping_charge=$db->fetch_array($query_shpping);
  	if($row_shipped<>0){
  		if($shipping_charge['lshipping_type']=='price'){
  			$var_compare = $_SESSION['total_price'];
  		}elseif($shipping_charge['lshipping_type']=='weight'){
  			$var_compare = $_SESSION['total_weight'];
  		}else{
  			$var_compare = $_SESSION['total_items'];
  		}
  		
  		$y=0;
		$query_shpping2=get_shipping_price($country);
  		while($shipping_charge2=$db->fetch_array($query_shpping2)){
  			$tmp[$y] = $shipping_charge2['lformula'];
  			$tmp_price[$y] = $shipping_charge2['lprice'];
  			$tmp_tax[$y] = $shipping_charge2['lshipping_tax'];
  			$y++;
  		}

  		for($i=0;$i<$y;$i++){
  				if(isset($tmp[$i+1])){
  					if($tmp[$i] <= $var_compare and $var_compare <= $tmp[$i+1]){
  						$shipping_price=$tmp_price[$i];
  						$shipping_tax = $tmp_tax[$i];
  					}
  				}else{
  					if($var_compare >= $tmp[$i]){
  						$shipping_price=$tmp_price[$i];
  						$shipping_tax = $tmp_tax[$i];
  					}
  				}
  		}
  		
  		if($shipping_price==''){
  			$shipping_price=$tmp_price[$y-1];
  			$shipping_tax = $tmp_tax[$y-1];
  		}
  		
  	}else{
  		
  		$query_shipping_rest_the_wold=get_shipping_price('247');
  		$row_rest_the_world=$db->num_rows($query_shipping_rest_the_wold);
  		$shipping_charge_rest_the_wold=$db->fetch_array($query_shipping_rest_the_wold);
  		if($shipping_charge_rest_the_wold > 1){
  			if($shipping_charge_rest_the_wold['lshipping_type']=='price'){
  				$var_compare = $_SESSION['total_price'];
  			}elseif($shipping_charge_rest_the_wold['lshipping_type']=='weight'){
  				$var_compare = $_SESSION['total_weight'];
  			}else{
  				$var_compare = $_SESSION['total_items'];
  			}
			$y=0;
			$query_shipping_rest_the_wold2=get_shipping_price('247');
  			while($row_rest_the_world2=$db->fetch_array($query_shipping_rest_the_wold2)){
  				$tmp[$y] = $row_rest_the_world2['lformula'];
  				$tmp_price[$y] = $row_rest_the_world2['lprice'];
  				$tmp_tax[$y] = $row_rest_the_world2['lshipping_tax'];
  				$y++;
  			}

  			for($i=0;$i<$y;$i++){
  					if(isset($tmp[$i+1])){
  						if($tmp[$i] <= $var_compare and $var_compare <= $tmp[$i+1]){
  							$shipping_price=$tmp_price[$i];
  							$shipping_tax = $tmp_tax[$i];
  						}
  					}else{
	  					if($var_compare >= $tmp[$i]){
	  						$shipping_price=$tmp_price[$i];
	  						$shipping_tax = $tmp_tax[$i];
	  					}
  					}
  			}
  			
  			if($shipping_price==''){
  				$shipping_price=$tmp_price[$y-1];
  				$shipping_tax = $tmp_tax[$y-1];
  			}
  			
  		}else{
  			$shipping_price=$shipping_charge_rest_the_wold['lprice'];
  			$shipping_tax=$shipping_charge_rest_the_wold['lshipping_tax'];
  		}
  	}
  	return $shipping_price;
}
    
 //UPDATE COUNT PRODUCT
 function cart_edit_item($product_id,$parent,$child,$qty_value) 
    { 

		$exist = false;
		$j = 0;
		
		foreach( $_SESSION['cart'] as $product ){
			if(empty($parent)){
				if( $product['id']==$product_id){
					$exist = true;
					$sel = $j;
				}
			}else{
				if( $product['id']==$product_id && $product['parent']==$parent && $product['child']==$child ){
					$exist = true;
					$sel = $j;
				}
			}
			$j++;
		}
       
		   if($qty_value ==  0 || empty($qty_value)) { 
				//echo 'empty...';
                //unset($_SESSION['cart'][$productID]); 
	       } else {
                $_SESSION['cart'][$sel]['qty']  = $qty_value;
                
			} 
	  
		// Recalculate totals 
		$_SESSION['total_items'] = cart_calculate_items($_SESSION['cart']); 
		$_SESSION['total_price'] = cart_calculate_price($_SESSION['cart']);
		$_SESSION['total_weight'] = cart_calculate_weight($_SESSION['cart']);
		$s_cart = serialize($_SESSION['cart']); 
		$expire=time()+3600;
		setcookie('cart', $s_cart, $expire);
		
		return;
    } 
    
 //DELETE PRODUCT 
 function cart_delete_item($item,$parent='',$child=''){
		// fine status data yang di edit
		$exist = false;
		$temp='';
		
		$j = 0;
		foreach( $_SESSION['cart'] as $product ){
			if(empty($parent)){
				if( $product['id']==$item){	
					///abaikan [jgn disimpan]				
				}else{
					///simpan ke temp
					$temp[$j] = $product;
					$j++;
				}
				
			}else{
				if( $product['id']==$item && $product['parent']==$parent && $product['child']==$child ){
					///abaikan [jgn disimpan]						
				}else{
					///simpan ke temp
					$temp[$j] = $product;
					$j++;
				}
				
			}
			
		}
		if($j==0){
			$_SESSION['cart'] = array(); 
			$_SESSION['total_items'] = 0; 
			$_SESSION['total_price'] = 0.00;
			$_SESSION['taxes'] = 0.00;
			$_SESSION['country'] = '';
			$_SESSION['idproduct']=array();
			$expire=3600;
			$expire=time()+$expire;
			$s_cart = serialize($_SESSION['cart']); 
			setcookie('cart', $s_cart, $expire);			
		}else{			
			$_SESSION['cart']= $temp;
			$exist=true;
		}
		
		// Recalculate totals 
		$_SESSION['total_items'] = cart_calculate_items($_SESSION['cart']); 
		$_SESSION['total_price'] = cart_calculate_price($_SESSION['cart']);
		$_SESSION['total_weight'] = cart_calculate_weight($_SESSION['cart']);
		
		$s_cart = serialize($_SESSION['cart']);
		$expire=time()+3600;
		setcookie('cart', $s_cart, $expire); 
	return $exist;
			
} 

function set_id_auto($table,$field,$kode){
	global $db;
	$sql=$db->prepare_query("SELECT MAX(substring(".$field.",7,5)) 
					FROM ".$table." 
					WHERE substring(".$field.",3,2)=%s and substring(".$field.",5,2)=%s
					",
					date("y",time()),date("m",time()));
	$r = $db->do_query($sql);
	$data = $db->fetch_array($r);
	
	if($data[0]==NULL){
		$code=1;	
	}else{
		$code=$data[0]+1;
		
	}
	
	
	$mixcode=$kode.date("y",time()).date("m",time());
	
	if($code < 10){
		$mixcode=$mixcode."0000".$code;
	}elseif($code<100){
		$mixcode=$mixcode."000".$code;
	}elseif($code<1000){
		$mixcode=$mixcode."00".$code;
	}elseif($code<10000){
		$mixcode=$mixcode."0".$code;
	}elseif($code<100000){
		$mixcode=$mixcode.$code;
	}
	return $mixcode;
}

function the_member($param){
	global $db;
  	$id_member = set_id_auto('lumonata_members','lmember_id','MI');
  	$password = createRandomPassword();
  	$password2 = md5($password);
  	$id_member_md5 = md5($id_member);
	$sql=$db->prepare_query("INSERT INTO lumonata_members(lmember_id,
  															lfname,	
  															llname, 
  															lemail,
  															lphone,
  															lphone2,
  															lcountry_id,
  															lregion,
  															lcity,
  															laddress,
  															laddress2,
  															laddress3,
  															lpostal_code,
  															lpassword,
  															lpass,
  															lactv_code,
  															laccount_status,
  															llast_login,
  															lcreated_by,
  															lcreated_date,
  															lusername,
  															ldlu,
  															llang_id)
  															VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
  															$id_member,
  															$param["firstname"],
  															$param["lastname"],
  															$param["email"],
  															$param["primary_phone"],
  															$param["alternative_phone"],
  															$param["country"],
  															$param["region"],
  															$param["city"],
  															$param["address"],
  															$param["second_address"],
  															$param["third_address"],
  															$param["postal"],
  															$password,
  															$password2,
  															$id_member_md5,
  															'2',
  															'',
  															$id_member,
  															date("Y-m-d H:i:s"),
  															$id_member,
  															date("Y-m-d H:i:s"),
  															''
  														);
  	$query_insert=$db->do_query($sql);
  	//$id_member_z=mysql_insert_id();
  	
  	return $id_member;
}

function order_shipping($member_id,$param,$checked){
	global $db;
	$id_order_shipping 	= set_id_auto('lumonata_order_shipping','lorder_shipping_id','OS');
	$shipping_price		= get_shipped_default($_SESSION['country']);
	
	if($checked==1){
		$sql=$db->prepare_query("INSERT INTO lumonata_order_shipping(lorder_shipping_id,																	
																	lfname,
																	llname,
																	lemail,
																	lphone,
																	lphone2,
																	lcountry_id,
																	lregion, 
																	lcity, 
																	laddress, 
																	laddress2,
																	laddress3,
																	lpostal_code, 
																	lshipping_price,
																	lcreated_by,
																	lcreated_date, 
																	lusername,
																	ldlu)
																VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
																	$id_order_shipping,																	
																	$param["firstname"],
																	$param["lastname"],
																	$param["email"],
																	$param["primary_phone"],
		  															$param["alternative_phone"],
		  															$param["country"],
		  															$param["region"],
		  															$param["city"],
		  															$param["address"],
		  															$param["second_address"],
		  															$param["third_address"],
		  															$param["postal"],
		  															$shipping_price,
																	$member_id,
		  															date("Y-m-d H:i:s"),
		  															$member_id,
		  															date("Y-m-d H:i:s")
																);
	}else{
		$sql=$db->prepare_query("INSERT INTO lumonata_order_shipping(lorder_shipping_id,																	
																	lfname,
																	llname,
																	lemail,
																	lphone,
																	lphone2,
																	lcountry_id,
																	lregion, 
																	lcity, 
																	laddress, 
																	laddress2,
																	laddress3,
																	lpostal_code,
																	lshipping_price,
																	lcreated_by,
																	lcreated_date, 
																	lusername,
																	ldlu)
																VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
																	$id_order_shipping,																	
																	$param["firstname_sh"],
																	$param["lastname_sh"],
																	$param["email_sh"],
																	$param["primary_phone_sh"],
		  															$param["alternative_phone_sh"],
		  															$param["country_sh"],
		  															$param["region_sh"],
		  															$param["city_sh"],
		  															$param["address_sh"],
		  															$param["second_address_sh"],
		  															$param["third_address_sh"],
		  															$param["postal_sh"],
		  															$shipping_price,
																	$param["firstname"],
		  															date("Y-m-d H:i:s"),
		  															$param["firstname"],
		  															date("Y-m-d H:i:s")
																);
	}
	$query_insert=$db->do_query($sql);
  	//$the_id_order_shipping=mysql_insert_id();

  	return $id_order_shipping;
}

function order($member_id,$order_shipping_id,$param){
	global $db;
  	$id_order = set_id_auto('lumonata_order','lorder_id','OI');
  	$shipping_price=get_shipped_default($_SESSION['country']);
  	$total_qty=0;
  	
	$setting_tax = tax_setting();
	if($setting_tax['price_include_taxes']==0){
  		$tax_product = calculate_tax($_SESSION['cart'],$_SESSION['country']);
	}else{
		$tax_product=0;
	}
	
  	if($setting_tax['taxes_on_shipping']==1){
		$shipped_taxes= (get_shipped_default($_SESSION['country'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
	}else{
		$shipped_taxes=0;
	}
  	
	$total_item = 0;
  	foreach($_SESSION['cart'] as $product ) {
  		$total_item++;
  		$total_qty=$total_qty+$product["qty"];
  	}
  	
  	$data_currency=get_meta_data('currency','product_setting');
  	///shipping option
	$shipping_option	= $db->fetch_array(get_shipping_option($param["shipping_option"]));
	
	$sql=$db->prepare_query("INSERT INTO lumonata_order(lorder_id, 
															lmember_id, 
															lorder_shipping_id, 
															lorder_shipping_status, 
															lorder_date, 
															lstatus, 
															lstatus_archive,
															litem_total,
															lquantity_total, 
															lcurrency_id, 
															lshipping_option,
															lshipping_price,
															lprice_total,
															tax_product,
															tax_shipping,
															lpayment_method,
															lcreated_by, 
															lcreated_date, 
															lusername, 
															ldlu, 
															llang_id)
  															VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
  															$id_order,
  															$member_id,
  															$order_shipping_id,
  															'',
  															date("Y-m-d H:i:s"),
  															'1',
  															'',
  															$total_item,
  															$total_qty,
  															$data_currency,
  															$shipping_option['loption'],
  															$shipping_price,
  															$_SESSION['total_price'],
  															$tax_product,
  															$shipped_taxes,
  															$param['payment_methode'],
  															$member_id,
  															date("Y-m-d H:i:s"),
  															$member_id,
  															date("Y-m-d H:i:s"),
  															''
  														);
  	$query_insert=$db->do_query($sql);
  	//$id_member_order=mysql_insert_id();
  	
  	return $id_order;
}

function order_detail($member_id,$lorder_id){
	global $db;
  	$total_qty=0;
	$tmp_id='';
	$i=0;
	$id_result= '';
	
	if(is_array($_SESSION['cart'])){
		foreach($_SESSION['cart'] as $product ) {
			if($tmp_id==$product['id']){
				$tmp_product_id[$i]=$tmp_id;
				$i++;
			}
			$tmp_id=$product['id'];
		}
		if($i==0){
			foreach($_SESSION['cart'] as $product ) {
				$data_product=get_detail_product($product['id']);
				$additional_product=default_price($product['id']);
				if(empty($product['parent'])){
					$tmp_price=$additional_product['price'];
				}else{
					$tmp_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				}
				
				///check item price by qty [yana] / ambil harga berdasarkan price range
				if (check_item_price($product['id'], $product['qty'])!=false){
					$tmp_price		= number_format($tmp_price,2,'.','');
					$the_price_new  = check_item_price($product['id'], $product['qty']);
					$the_price_new  = number_format($the_price_new,2,'.','');
					
					if ($the_price_new==$tmp_price){
						$tmp_price = $tmp_price; 
					}else{
						$tmp_price = $the_price_new;
					}
				}else{
					$tmp_price = $tmp_price; 
				}
				///			
				
				
				///$tmp_total=$tmp_price*$product['qty']; //salah, langsung disimpan harga satuan saja dan tidak perlu di kalikan dg qty
				$tmp_total=$tmp_price;
				
				$the_qty = $product['qty'];
				
				$sql=$db->prepare_query("INSERT INTO lumonata_order_detail(lorder_id,
																			lproduct_id,
																			lprice,
																			lqty,
																			lcreated_by,
																			lcreated_date,
																			lusername,
																			ldlu)
					  													VALUES(%s,%s,%s,%s,%s,%s,%s,%s)",
																			$lorder_id,
																			$product['id'],
																			$tmp_total,
																			$the_qty,
					  														$member_id,
					  														date("Y-m-d H:i:s"),
					  														$member_id,
					  														date("Y-m-d H:i:s")	
					  														);
			  	$query_insert=$db->do_query($sql);
			  	$id_detail = mysql_insert_id();
			  	order_detail_material($member_id,$id_detail,$product['id']);
			  	
			  	$obj_stock = check_stock($product['id']);
			  	
			  	$arr_price = $obj_stock['price'];
			  	$arr_weight = $obj_stock['weight'];
			  	$arr_tax = $obj_stock['tax'];
			  	
			  	if($obj_stock['stock_limit']<>"0" and $obj_stock['stock_limit']>$the_qty){
			  		$arr_stock_limit = $obj_stock['stock_limit']-$the_qty;
			  	}else{
			  		$arr_stock_limit = "0";	
			  	}
			  	$arr_in_stock = $obj_stock['product_buy_out_of_stock'];
			  	$arr_trackstok = $obj_stock['trackstok'];

			  	if($obj_stock['trackstok']=="1"){
			  		$val=array('code'=>$obj_stock['code'],'price'=>$arr_price,'weight'=>$arr_weight,'tax'=>$arr_tax,'stock_limit'=>$arr_stock_limit,'product_buy_out_of_stock'=>$arr_in_stock,'trackstok'=>$arr_trackstok);
					$lvalue=json_encode($val);
			  		$sql_update_stock =$db->prepare_query("UPDATE lumonata_additional_fields 
			  											SET lvalue=%s
			  											where lapp_id=%s 
			  											and lkey=%s 
			  											and lapp_name=%s",
       													$lvalue,$product['id'],'product_additional','products');
       				$db->do_query($sql_update_stock);
			  	}
			}
		}else{
			$k=0;
			//id product with same value
			for($j=0;$j<$i;$j++){
				$value=0;
				foreach($_SESSION['cart'] as $product ) {
					if($tmp_product_id[$j]==$product['id']){
						if($value=="0" or $value=''){
							$id_result[$k]=$product['id'];
							$value=1;
							$k++;
						}
					}else{
						$id_result[$k]=$product['id'];
						$k++;
					}
				}
				
			}
			$n=0;
			$thetotal=array();
			$theqty=array();
			$sql='';
			for($l=0;$l<$k;$l++){
				$tmp_qty=0;
				$total_this_product=0;
				$tmp_total=0;
				$tmp_price=0;
				$the_qty=0;
				foreach($_SESSION['cart'] as $product ) {
					$data_product=get_detail_product($product['id']);
					$additional_product=default_price($product['id']);
					$tmp_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
					if($tmp_price==''){
						$tmp_price=$additional_product['price'];
					}
					
					///check item price by qty [yana] / ambil harga berdasarkan price range
						if (check_item_price($product['id'], $product['qty'])!=false){
							$tmp_price		= number_format($tmp_price,2,'.','');
							$the_price_new  = check_item_price($product['id'], $product['qty']);
							$the_price_new  = number_format($the_price_new,2,'.','');
							
							if ($the_price_new==$tmp_price){
								$tmp_price = $tmp_price; 
							}else{
								$tmp_price = $the_price_new;
							}
						}else{
							$tmp_price = $tmp_price; 
						}
					///						
						
						
					///$tmp_total=$tmp_price*$product['qty']; //salah, langsung disimpan harga satuan saja dan tidak perlu di kalikan dg qty
					$tmp_total=$tmp_price;
					
					$the_qty = $product['qty'];
					
					if($id_result[$l]==$product['id']){
						$total_this_product=$total_this_product+$tmp_total;
						$tmp_qty = $tmp_qty + $the_qty;
					}
				}
				$thetotal[$n]=$total_this_product;
				$theqty[$n]=$tmp_qty;
				$n++;
			} 

			for($m=0;$m<$n;$m++){
				$sql=$db->prepare_query("INSERT INTO lumonata_order_detail(lorder_id,
																			lproduct_id,
																			lprice,
																			lqty,
																			lcreated_by,
																			lcreated_date,
																			lusername,
																			ldlu)
					  													VALUES(%s,%s,%s,%s,%s,%s,%s,%s)",
																			$lorder_id,
																			$id_result[$m],
																			$thetotal[$m],
																			$theqty[$m],
					  														$member_id,
					  														date("Y-m-d H:i:s"),
					  														$member_id,
					  														date("Y-m-d H:i:s")	
					  														);
			  	$query_insert=$db->do_query($sql);
			  	$id_detail = mysql_insert_id();
				order_detail_material($member_id,$id_detail,$id_result[$m]);	
			  	
				$obj_stock = check_stock($id_result[$m]);
			  	
			  	$arr_price = $obj_stock['price'];
			  	$arr_weight = $obj_stock['weight'];
			  	$arr_tax = $obj_stock['tax'];
			  	
				if($obj_stock['stock_limit']<>"0" and $obj_stock['stock_limit']>$theqty[$m]){
			  		$arr_stock_limit = $obj_stock['stock_limit']-$theqty[$m];
			  	}else{
			  		$arr_stock_limit = "0";	
			  	}
			  	
			  	$arr_in_stock = $obj_stock['product_buy_out_of_stock'];
			  	$arr_trackstok = $obj_stock['trackstok'];
			  	
			  	if($obj_stock['trackstok']=="1"){
			  		$val=array('code'=>$obj_stock['code'],'price'=>$arr_price,'weight'=>$arr_weight,'tax'=>$arr_tax,'stock_limit'=>$arr_stock_limit,'product_buy_out_of_stock'=>$arr_in_stock,'trackstok'=>$arr_trackstok);
					$lvalue=json_encode($val);
			  		$sql_update_stock =$db->prepare_query("UPDATE lumonata_additional_fields 
			  											SET lvalue=%s
			  											where lapp_id=%s 
			  											and lkey=%s 
			  											and lapp_name=%s",
       													$lvalue,$id_result[$m],'product_additional','products');
       				$db->do_query($sql_update_stock);
			  	}
			}

		}	
			
	}
  	return $id_detail;
}

function order_detail_material($member_id,$lorder_id,$product_id){
	global $db;
	$var_array_parent = array();
	if(is_array($_SESSION['cart'])){
		foreach($_SESSION['cart'] as $product ) {
			$data_product=get_detail_product($product['id']);
			$additional_product=default_price($product['id']);
			if(empty($product['parent'])){
				$the_price=$additional_product['price'];
				$val_variant_encode="";
			}else{
				$the_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				$var_array_parent['parent_variant']=$product['parent'];
				$var_array_parent['child_variant']=$product['child'];
				$val_variant_encode=json_encode($var_array_parent);
			}
			
			///check item price by qty [yana] / ambil harga berdasarkan price range
				if (check_item_price($product['id'], $product['qty'])!=false){
					$the_price		= number_format($the_price,2,'.','');
					$the_price_new  = check_item_price($product['id'], $product['qty']);
					$the_price_new  = number_format($the_price_new,2,'.','');
					
					if ($the_price_new==$the_price){
						$the_price = $the_price; 
					}else{
						$the_price = $the_price_new;
					}
				}else{
					$the_price = $the_price; 
				}
			///					
			
			///$total_this_product=$the_price*$product['qty']; ///salah, simpan harga satuan saja, tidak perlu dikalikan dengan qty
			$total_this_product=$the_price; //fix by yana
			
			if($product_id == $product['id']){
			$sql=$db->prepare_query("INSERT INTO lumonata_order_detail_material(ldetail_id,
																	lproduct_id,
																	product_variant,
																	lprice,
																	lqty,
																	lcreated_by,
																	lcreated_date,
																	lusername,
																	ldlu)
			  													VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)",
																	$lorder_id,
																	$product['id'],
																	$val_variant_encode,
																	$total_this_product,
																	$product['qty'],
			  														$member_id,
			  														date("Y-m-d H:i:s"),
			  														$member_id,
			  														date("Y-m-d H:i:s")	
			  														);
		 	$query_insert=$db->do_query($sql);
			}
		}
	}
	return true;
}

function createRandomPassword() {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 7) {

        $num = rand() % 33;

        $tmp = substr($chars, $num, 1);

        $pass = $pass . $tmp;

        $i++;

    }
    return $pass;
}

function payment_option(){
	$db;
 	$value[0]='name';
	$value[1]='text';
	$value[2]='mode';
 	$resutl='';
 	$count_p=0;
	$tmp_array_parent=array();
 	$data_payments=get_meta_data('payments','product_setting');
	
	$array_payments = json_decode($data_payments,true);
      if(is_array($array_payments)){
	      foreach($array_payments['parent_payment'] as $key=>$val){
		      $count_c=0;
		      $tmp_array_parent['parent_payment'][$count_p]=$val;
		     //echo "parent".$key.":".$val;
		     
		      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

		      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
		      		//echo "child ".$subkey.":".$subval."<br>";
			      	$count_c++;
		      	
		      }
		    $count_p++;
       	 }
      }
     	//$resutl.='<option value="">Payment Method</option>';
       for($j=0;$j<$count_p;$j++){
       		$resutl.='<option value="'.$tmp_array_parent['parent_payment'][$j].'">'.$tmp_array_parent['child_payment'][$j]['name'].'</option>';
       }
	return $resutl;
}

function get_confirmation_order(){
	$items='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery.confirm.js"></script>';
	$items.='<div class="title-content" style="min-height:230px"> The order was paid</div> ';
	
	return $items;
}

function the_session(){
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Confirm Order';   
    $actions->action['meta_title']['args'][0] = '';	
	$items='<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery.confirm.js"></script>';;
	if(isset($_SESSION['payment_success']) and $_SESSION['payment_success']==1){
		$items.='<div class="title-content" style="min-height:230px">Thank you for your payment<br>Please Check Your Email.</div>';
	}else if(isset($_SESSION['payment_success']) and $_SESSION['payment_success']==2){
		$items.='<div class="title-content" style="min-height:230px"> The order was paid</div>';
	}else if(isset($_SESSION['payment_success']) and $_SESSION['payment_success']==3){
		$items.='<div class="title-content" style="min-height:230px">Thank you for your order<br>Please Check Your Email.</div>';
	}
	return $items;
}

function check_stock($id_article){
	global $db;
	$sql_stock_r= $db->prepare_query("select * 
       						from lumonata_additional_fields 
       						where lapp_id=%s and lkey=%s and 
       						lapp_name=%s",
       						$id_article,'product_additional','products');
    $result_stok_r=$db->do_query($sql_stock_r);
    $data_stok_r=$db->fetch_array($result_stok_r);
    $obj_stok_r = json_decode($data_stok_r['lvalue'],true);
    //$stock_limit=$obj_stok_r['stock_limit'];
    //$out_of_stock=$obj_stok_r['product_buy_out_of_stock'];
    return $obj_stok_r;
}

function isEmailRegister($email){
	global $db;
	$q = $db->prepare_query("select * from lumonata_members where lemail=%s AND laccount_status=%d",$email,1);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if ($n>=1){
		return true;
	}else{
		return false;
	}
}
//THE FRONT VIRTUAL PAGE HERE
  add_actions('front-product-ajax_page', 'front_product_ajax');
  
  function front_product_ajax(){
  	add_actions('is_use_ajax', true);

  	global $db;
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='variant_parent'){
  	  $the_value=array();
  	  $the_parent=$_POST['lvalue'];
  	  $app_id=$_POST['app_id'];
  	  $sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$app_id,'product_variant','products');
      $result_variant=$db->do_query($sql_variant);
      $data_variant=$db->fetch_array($result_variant);
      //echo $data_variant['lvalue'];
      $obj_variant = json_decode( $data_variant['lvalue'],true);
      $count_it=0;
      
      if(is_array($obj_variant)){
	      foreach($obj_variant['parent_variant'] as $key=>$val){
	      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
			$execute_added=$db->do_query($sql_added);
			$data_added=$db->fetch_array($execute_added);
			$varian_idparent[$count_it]=$val;
			$varian_parent[$count_it]=$data_added['lname'];
			$j=0;
			if($varian_idparent[$count_it] == $the_parent){
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var_id[$j]=$subkey;
			   				$the_var[$j]=$data_added_child['lname'];
			      			$j++;
			      	}
			      	break;
			}
	      	$count_it++;
	      }
     }
     		$price_array_0=number_format($the_value[0],2,'.','');
     		$items=$price_array_0.'&';
			for($a=0;$a<$j;$a++){
						$items.='<option value="'.$the_var_id[$a].'">'.$the_var[$a].'</option>';
			}
     
  	echo $items;	
  	}
  	
  if(isset($_POST['pKEY']) and $_POST['pKEY']=='price_child'){
  	  $the_value=array();
  	  $the_child=$_POST['lvalue'];
  	  $app_id=$_POST['app_id'];
  	  $sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$app_id,'product_variant','products');
      $result_variant=$db->do_query($sql_variant);
      $data_variant=$db->fetch_array($result_variant);
      $obj_variant = json_decode( $data_variant['lvalue'],true);
      $count_it=0;
      if(is_array($obj_variant)){
	      foreach($obj_variant['parent_variant'] as $key=>$val){
	      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
			$execute_added=$db->do_query($sql_added);
			$data_added=$db->fetch_array($execute_added);
			$varian_idparent[$count_it]=$val;
			$varian_parent[$count_it]=$data_added['lname'];
			$j=0;
	      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
	      		//echo $subkey.'='.$the_child.'<br>';
	      		if($subkey == $the_child){
	      			$the_value[0]=$subval[0];
	      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
		                where
		                lparent = %d and lrule_id= %s",$val, $subkey);
	   				$execute_added_child=$db->do_query($sql_added_child);
	   				$data_added_child=$db->fetch_array($execute_added_child);
	   				$the_var[$j]=$data_added_child['lname'];
	      		}
	      		$j++;
			}
	      	$count_it++;
	      }
     }
  	 echo number_format($the_value[0],2,'.','');	
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='add_to_chart'){
  		if(isset($_SESSION['payment_success'])){
  			unset($_SESSION['payment_success']);
  		}
  		$fields = explode("&",$_POST['lvalue']);
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);
			eval("$$key = \"$value\";");
  		}
  		
  		///validasi agar qty tidak bernilai minus & selalu bernilai positif integer
  		$qty = (int)$qty;
  		if ($qty<=0){
  			echo "wrongQty";  			
  			exit;
  		}  	
  		///end validasi
  		
  		
  		//stock 
		$obj_stock = check_stock($product_id);
		$stock_limit=$obj_stock['stock_limit'];
		$out_of_stock=$obj_stock['product_buy_out_of_stock'];
		if(empty($stock_limit)){
			$stock_limit=0;
		}
		
		if($obj_stock['trackstok']=="1"){
			if($out_of_stock=="0"){
				if($qty>$stock_limit){
					echo "failed";
				}else{
					$j = 0;
					$qty_session = 0;
					if(count($_SESSION['cart']) > 0){
						foreach( $_SESSION['cart'] as $product ){
							if( $product['id']==$product_id){
								$exist = true;
								$sel = $j;
								$qty_session =$_SESSION['cart'][$sel]['qty']+$qty_session;
							}
							$j++;
						}
					}
					if($qty_session >= $stock_limit){
						echo "failed";
					}else{
						echo cart_add_item($product_id,$parent,$child,$qty,$sef_cat);
					}
				}
			}else{
				echo cart_add_item($product_id,$parent,$child,$qty,$sef_cat);
			}
		}else{
			echo cart_add_item($product_id,$parent,$child,$qty,$sef_cat);
		}
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='del_chart_list'){
  		$product_id=$_POST['product_id'];
  		$parent_id=$_POST['parent'];
  		$child_id=$_POST['child'];
  		$the_value=cart_delete_item($product_id,$parent_id,$child_id);
  		if($_POST['keyword']=="billing"){
  			if($the_value==true){
	  			///echo get_shipping_detail('products'); 
	  			echo list_chart_shipping_detail('products');
	  		}else{
	  			echo "2";
	  		}
  		}elseif ($_POST['keyword']=="chart"){
	  		if($the_value==true){
	  			echo list_chart('products');
	  		}else{
	  			echo "2";
	  		}
  		}
  	}
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='update_chart_list'){
  		$product_id=$_POST['product_id'];
  		$parent_id=$_POST['parent'];
  		$child_id=$_POST['child'];
  		$qty_value=$_POST['qty_value'];
  		
  		$element_id=$product_id."_".$parent_id."_".$child_id;
  		
  		///validasi agar qty tidak bernilai minus & selalu bernilai positif integer
  		$qty_value = (int)$qty_value;
  		if ($qty_value<=0){
  			echo list_chart('products');
  			exit;
  		}  	
  		///end validasi
  		
  		$obj_stock = check_stock($product_id);
		$stock_limit=$obj_stock['stock_limit'];
		$out_of_stock=$obj_stock['product_buy_out_of_stock'];
		if(empty($stock_limit)){
			$stock_limit=0;
		}
		
	

		if($obj_stock['trackstok']=="1"){
	  		if($out_of_stock=="0"){
				if($qty_value>$stock_limit){
					//echo "failed";
					echo list_chart('products',$element_id);
				}else{
					$j = 0;
					$qty_session = 0;
					if(count($_SESSION['cart']) > 0){
						foreach( $_SESSION['cart'] as $product ){
							if( $product['id']==$product_id){
								$exist = true;
								$sel = $j;
								$qty_session =$_SESSION['cart'][$sel]['qty']+$qty_session;
							}
							$j++;
						}
					}
					if($qty_session > $stock_limit){
						//echo "failed";
						echo list_chart('products',$element_id);
					}else{
						cart_edit_item($product_id,$parent_id,$child_id,$qty_value);
						echo list_chart('products');
					}
				}
			}else{
				cart_edit_item($product_id,$parent_id,$child_id,$qty_value);
				echo list_chart('products');
			}
		}else{
			cart_edit_item($product_id,$parent_id,$child_id,$qty_value);
			echo list_chart('products');
		}
		
   	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='shipping_to_country'){
  		$country 						= $_POST['id_country'];
  		$shipping_option 				= $_POST['id_shipping_option'];
  		
  		$_SESSION['country']			= $country;
  		$_SESSION['shipping_option']	= $shipping_option;
  		
  		$shipping_price=get_shipped_default($country,$shipping_option);
  		
  		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==0){
  			$tax=calculate_tax($_SESSION['cart'],$country);
		}else{
			$tax=0;
		}
  		
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes= (get_shipped_default($_SESSION['country'],$_SESSION['shipping_option'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
		}else{
			$shipped_taxes=0;
		}
		
		$total=$_SESSION['total_price'] + $shipping_price + $tax + $shipped_taxes;
		
		$grand_total = number_format($total,2,'.','');
		
  		if($country==''){
  			$shipping_price='0.00';
  		}else{
  			$shipping_price=$shipping_price;
  		}
  		
  		$select_country= country_shipping2();
  		
	  	echo number_format($tax,2,'.','')."&".$shipping_price."&".$grand_total."&".$select_country."&".number_format($shipped_taxes,2,'.','');
	  		
  		
  	}
  	
  if(isset($_POST['pKEY']) and $_POST['pKEY']=='shipping_to_country1'){
  		$country 						= $_POST['id_country'];
  		$shipping_option 				= $_POST['id_shipping_option'];
  		
  		$_SESSION['country']			= $country;
  		$_SESSION['shipping_option']	= $shipping_option;
  		
  		$shipping_price=get_shipped_default($country,$shipping_option);
  		
  		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==0){
  			$tax=number_format(calculate_tax($_SESSION['cart'],$country),2,'.','');
		}else{
			$tax=0;
		}
  		
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes= (get_shipped_default($_SESSION['country'],$_SESSION['shipping_option'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
		}else{
			$shipped_taxes=0;
		}
		
		$total=$_SESSION['total_price'] + $shipping_price + $tax + $shipped_taxes;
		
		$grand_total = number_format($total,2,'.','');
		
  		if($country==''){
  			$shipping_price='0.00';
  		}else{
  			$shipping_price=$shipping_price;
  		}
  		
  		$select_country= country_shipping2();
  		
  		echo $tax."&".$shipping_price."&".$grand_total."&".$select_country."&".number_format($shipped_taxes,2,'.','');
  		
  	}
  	
  if(isset($_POST['pKEY']) and $_POST['pKEY']=='ship_to_country_with_shipping_option'){
  		$country 						= $_POST['id_country'];
  		$shipping_option 				= $_POST['id_shipping_option'];
  		
  		$_SESSION['country']			= $country;
  		$_SESSION['shipping_option']	= $shipping_option;
  		
  		$shipping_price=get_shipped_default($country,$shipping_option);
  		
  		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==0){
  			$tax=number_format(calculate_tax($_SESSION['cart'],$country),2,'.','');
		}else{
			$tax=0;
		}
  		
		if($setting_tax['taxes_on_shipping']==1){
			$shipped_taxes= (get_shipped_default($_SESSION['country'],$_SESSION['shipping_option'])) * (calculate_shipping_tax($_SESSION['cart'],$_SESSION['country'])/100);
		}else{
			$shipped_taxes=0;
		}
		
		$total=$_SESSION['total_price'] + $shipping_price + $tax + $shipped_taxes;
		
		$grand_total = number_format($total,2,'.','');
		
  		if($country==''){
  			$shipping_price='0.00';
  		}else{
  			$shipping_price=$shipping_price;
  		}
  		
  		$select_country= country_shipping2();
  		
  		echo $tax."&".$shipping_price."&".$grand_total."&".$select_country."&".number_format($shipped_taxes,2,'.','');
  		
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='confirm_order'){
  		global $db;
  		$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);
			//eval("$$key = \"$value\";");
			$vars[$key]=$value;
  		}
  		
  		if(isset($_POST['same'])){
  			$checked=$_POST['same'];
  		}else{
  			$checked="0";
  		}
  		
		///$member_id
		$member_id = '';
		if (isMemberLogin()){
			$member_id = $_SESSION['memberpanel_clientid'];
		}elseif(isEmailRegister($vars['email'])){			
			echo "emailRegister";
			exit;
		}else{
			$member_id = the_member($vars);
			
			///send link to activation code by yana	
			$data_member  = get_member($member_id);  		
	  		$fullname	= $vars['firstname'].' '.$vars['lastname'];
	  		$code_actv	= md5($member_id);
	  		$to_email	= $vars['email'];
	  		$data_account = 'Below is detail of your account : <br>
							 Email : '.$to_email.' <br>
							 Password : '.$data_member['lpassword'].'
							<br><br>
	  						';
	  		send_mail_activate_account($fullname,$code_actv,$to_email,$data_account);		
			///
		}
		///
		
		$order_shipping_id	= order_shipping($member_id,$vars,$checked);
		$order_id 			= order($member_id,$order_shipping_id,$vars);
		$order_details 		= order_detail($member_id,$order_id);
		///$var = order_detail_material($member_id,$order_details);

			$time=time();
			$fname='';
			$username='';
			$check_order= $db->prepare_query("Select * From lumonata_order_payment where lorder_id= %s",$order_id);
      		$qry_check_order = $db->do_query($check_order);
		    	if($db->num_rows($qry_check_order)==0){
			      $q=$db->prepare_query("Select * From lumonata_order,lumonata_members 
			      							Where lumonata_order.lmember_id=lumonata_members.lmember_id 
			      							and lumonata_order.lorder_id= %s",$order_id);
				  $r=$db->do_query($q);
				  $d=$db->fetch_array($r);
				  $fname=$d['lfname'];
				  $username=$d['lusername'];
				  
			  	  $sql_cek = $db->prepare_query("Select * From lumonata_order where lorder_id= %s",$order_id);
			      $result_cek = $db->do_query($sql_cek);
			      $cek=$db->fetch_array($result_cek);
				  $grand_total = $cek['lprice_total'] + $cek['tax_shipping'] + $cek['tax_product'] + $cek['lshipping_price'];
				  
			      $qi=$db->prepare_query("Insert Into lumonata_order_payment (lorder_id,
			      																lstatus,
			      																lpaid,
			      																lcreated_by,
			      																lcreated_date,
			      																lusername,
			      																ldlu,
			      																first_name,
			      																payer_email,
			      																lpayment_method,
			      																ldate_trans,
			      																lname_paid)
																			values(%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
																				$order_id,
																				0,
																				$grand_total,
																				$member_id,
																				$time,
																				$username,
																				$time,
																				'',
																				'',
																				$vars['payment_methode'],
																				$time,
																				$fname);
				  $ri=$db->do_query($qi);
		      }
	
		if($vars['payment_methode']=="payments_cheque" or $vars['payment_methode']=="payments_bank_transfer"){      
			$_SESSION['payment_success'] = 3;
		}
		
		$email_admin = get_meta_data('email');
		$email_client='';
		
		///kirim email juga payment_methode selain paypal
		if($vars['payment_methode']=="payments_paypal_standard"){
			//send email later after paid by paypal
		}else{
			send_mail_order($order_id,$email_admin,'admin');
			send_mail_order($order_id,$email_client,'client');
		}
		
	  	foreach($_SESSION['cart'] as $rem_key => $rem_value){
			if (array_key_exists($rem_key, $_SESSION['cart'])){
			unset($_SESSION['cart'][$rem_key]);
			}
		}
		
		unset($_SESSION['total_items']);
		unset($_SESSION['total_price']);
		unset($_SESSION['total_weight']);
		unset($_SESSION['country']);
		setcookie('cart', '', time() - 3600); 
		//cart();
		
		//setcookie('cart');
		echo $vars['payment_methode']."&".md5($order_id);
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='shipping_address_check'){
  		$_SESSION['country']=$_POST['lvalue'];
  		$select_country_member=country_member();
  		
  		echo $select_country_member;
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='member_to_country'){
  		$country = $_POST['lvalue'];
  		$_SESSION['country']=$country;
  		
  		$shipping_price=get_shipped_default($country);
  		
  		$setting_tax = tax_setting();
		if($setting_tax['price_include_taxes']==0){
  			$tax=calculate_tax($_SESSION['cart'],$country);
		}else{
			$tax=0;
		}
  		
		$total=$_SESSION['total_price'] + $shipping_price + $tax;
		
		$grand_total = number_format($total,2,'.','');
		
  		if($country==''){
  			$shipping_price='0.00';
  		}else{
  			$shipping_price=$shipping_price;
  		}

  		$select_country= country_shipping();
  		echo $tax."&".$shipping_price."&".$grand_total."&".$select_country;
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='src'){
  		$value = $_POST['lvalue'];
  		$sql=$db->prepare_query("select * from lumonata_order where lorder_id like %s","%".$value."%");
  		$result=$db->do_query($sql);    
        echo order_list($result,0,$value,1);
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='delete_product_order'){
  		$id = $_POST['val'];
  		
  		$sql_relation = $db->prepare_query("SELECT lumonata_order.lorder_id, lumonata_order_detail.ldetail_id,lumonata_order.lorder_shipping_id 
  												FROM lumonata_order,lumonata_order_detail 
  												where lumonata_order.lorder_id=lumonata_order_detail.lorder_id 
  												and lumonata_order.lorder_id=%s",$id);
  		$query_relation = $db->do_query($sql_relation);
  		while($data_relation=$db->fetch_array($query_relation)){
  			$delete_order_material=$db->prepare_query("DELETE FROM lumonata_order_detail_material WHERE ldetail_id=%s",$data_relation['ldetail_id']);
  			$query_del_order_material=$db->do_query($delete_order_material);
  			$shipping_id = $data_relation['lorder_shipping_id'];
  		}
  		
  	    $sql_payment=$db->prepare_query("select * from lumonata_order_payment where WHERE lorder_id=%s",$id);
	    $r_payment=$db->do_query($sql_payment);
	    $d_payment=$db->fetch_array($r_payment);
	    
	    if(!empty($d_payment['lfile'])){
	    	$file=ROOT_PATH.$d_payment['lfile'];
		    if(file_exists($file)){
				unlink($file);
		    }
	    }

  		$delete_order_detail=$db->prepare_query("DELETE FROM lumonata_order_detail WHERE lorder_id=%s",$id);
  		$query_del_order_detail=$db->do_query($delete_order_detail);

  		$delete_order_shipping=$db->prepare_query("DELETE FROM lumonata_order_shipping WHERE lorder_shipping_id=%s",$shipping_id);
  		$query_del_order_shipping=$db->do_query($delete_order_shipping);
  		
  		$delete_order_payment=$db->prepare_query("DELETE FROM lumonata_order_payment WHERE lorder_id=%s",$id);
  		$query_del_order_payment=$db->do_query($delete_order_payment);
  		
  		$delete_order=$db->prepare_query("DELETE FROM lumonata_order WHERE lorder_id=%s",$id);
  		$query_del_order=$db->do_query($delete_order);
  		
  		echo "success";	
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='edit_status_order'){
  		$id_order = $_POST['val'];
  		$lvalue = $_POST['value'];
  		$sql=$db->prepare_query("UPDATE lumonata_order set lstatus=%s where lorder_id=%s",$lvalue,$id_order);
  		$query=$db->do_query($sql);
  		
  		$sql_order_paymen=$db->prepare_query("UPDATE lumonata_order_payment set lstatus=%s where lorder_id=%s",$lvalue,$id_order);
  		$query_order_paymen=$db->do_query($sql_order_paymen);
  		
  		echo "Success";
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='checkout_minimum_price'){
  		$loop=0;
		foreach($_SESSION['cart'] as $product ) {
			$data_product=get_detail_product($product['id']);
			$additional_product=default_price($product['id']);
			$the_images=get_product_detail_images($product['id']);
			if(empty($product['parent'])){
				$the_price=$additional_product['price'];
				$variant_type='';
				$product['parent'] = '';
				$product['child'] = '';
			}else{
				$the_price=get_product_variant_detail($product['id'],$product['parent'],$product['child']);
				$variant_type = find_rules_name($product['parent'],0,'product').' : '.find_rules_name($product['child'],$product['parent'],'product');
			}
			$total_this_product=$the_price*$product['qty'];
			
			$testing = fetch_rule_relationship('app_id='.$product['id']);//id category
			$minimum_price = get_additional_field($testing[0], 'minimum_price', 'categories_price'); //min price category
			
			if(!empty($var[$testing[0]])){
				$total_purchase[$testing[0]] = $total_purchase[$testing[0]] + $total_this_product;
			}else{
				$tmp_min_cat[$loop] = $minimum_price;
				$tmp_cat[$loop] = $testing[0];
				$var[$testing[0]] = $testing[0];
				$total_purchase[$testing[0]] = $total_this_product;
				$loop++;
			}
		}
		
		$minimum_total_purchase = get_meta_data('minimum_price','product_setting');
		
		$parameter = true;
	  	for($k=0;$k<$loop;$k++){
			if($total_purchase[$tmp_cat[$k]]<$tmp_min_cat[$k]){
				$parameter = false;
				$categori_id = $tmp_cat[$k];
			}
		}
		if($parameter==false){
			echo "1&".$categori_id;
		}else{
			if($_SESSION['total_price'] < $minimum_total_purchase){
				echo "2";
			}else{
				echo "3";
			}
		}
  	 }
   	if(isset($_POST['pKEY']) and $_POST['pKEY']=='confirm_payment_order'){
   		$time=time();
  		$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);
			//eval("$$key = \"$value\";");
			$vars[$key]=$value;
  		}
  		$date_trans = strtotime($vars['DateTrans']);
  		print_r($_POST);
  		echo $_FILES['file'];
  		if(isset($_FILES['file']) && !empty($_FILES['file'])){
  			$file_name = $_FILES['file']['name'];
  			$file_size = $_FILES['file']['size'];
	        $file_type = $_FILES['file']['type'];
	        $file_source = $_FILES['file']['tmp_name'];
	         if(is_allow_file_size($file_size)){
	         	if(is_allow_file_type($file_type,'image')){
	                 $title_image_gallery = $file_name;
	                 if (empty($title_image_gallery)){
	                 	$fix_file_name='untitled-'.time();	
	                 	$title = "Untitled";
	                 }else{
	                 	$fix_file_name=file_name_filter($title_image_gallery);
	                 	$title = $title_image_gallery;
	                 }
	                 
	                 $file_ext=file_name_filter($file_name,true);
	                 $file_name=$sef_url. $file_ext;
	                 
	                 $sef_url = $fix_file_name.'-'.time();
	                 $destination1=PLUGINS_PATH."/shopping_cart/uploads/payment/".$file_name;
	                 upload_resize($file_source,$destination1_m,$file_type,medium_image_width(),medium_image_height());
	         	}
	         }
  		 }else{
  		 	$destination1="";
  		 }
  		
  		if(!empty($vars['order_id'])){
			$check_payment=$db->prepare_query("SELECT * FROM lumonata_order_payment WHERE lstatus=%s and lorder_id=%s","2",$vars['order_id']);
			$query_check=$db->do_query($check_payment);
			if($db->num_rows($query_check)<>0){
				echo "paid";
			}else{
		  		echo $qi=$db->prepare_query("UPDATE lumonata_order_payment SET lstatus=%s,
		      															ldlu=%s,
		      															ldate_trans=%s,
		      															lname_paid=%s,
		      															ldestination_bank=%s,
		      															laccount_number=%s,
		      															lfile=%s
		      															WHERE lorder_id=%s",'2',$time,$date_trans,$vars['name_paid'],$vars['destination_bank'],$vars['account_number'],$destination1,$vars['order_id']);
				$ri=$db->do_query($qi);
				if($ri){
					//echo "success";
				}else{
					//echo "failed";
				}
	   		}
  		}else{
  			echo "failed";
  		}
   	} 
  	 
  }
  
  function send_mail_order($lorder_id,$email_value,$status){
  		set_template(PLUGINS_PATH."/shopping_cart/mail.html",$status);
  		//add_block('loopPage','lPage',$status);
		add_block('Bmail','xm',$status);
		global $db;	

		$items = '<style>td {padding : 5px;}</style>';
		
		//MEMBER
		$q=$db->prepare_query("Select * From lumonata_order,lumonata_members 
      							Where lumonata_order.lmember_id=lumonata_members.lmember_id 
      							and lumonata_order.lorder_id= %s",$lorder_id);
	  	$r=$db->do_query($q);
	  	$d=$db->fetch_array($r);
	  	
	  	if($status=="admin"){
	  		$messH1 = 'Congratulations,';
	  		$messH2 = 'Somebody has created a new order in your store.';
	  	}else{
		  	$messH1 = 'Dear '.$d['lfname'].' '.$d['llname'].',';
		    $messH2 = 'We here by confirm your order of the following items:';
	  	}
	    //add_variable('header_email', 'we here by confirm your order of the following items:');
	    add_variable('greeting', $messH1);
	    add_variable('greeting2', $messH2);

	  	//ORDER DETAIL
       $sql_order = $db->prepare_query("SELECT *
			FROM lumonata_order
			INNER JOIN lumonata_order_detail ON lumonata_order.lorder_id = lumonata_order_detail.lorder_id
			INNER JOIN lumonata_order_detail_material ON lumonata_order_detail.ldetail_id = lumonata_order_detail_material.ldetail_id
			WHERE lumonata_order.lorder_id = '$lorder_id'");
       $query_order = $db->do_query($sql_order);
		$i=0;
       while ($data_order = $db->fetch_array($query_order)){
	       	$product_id[$i] = $data_order['lproduct_id'];
      		$member_id[$i] = $data_order['lmember_id'];
      		$method = $data_order['lpayment_method'];
      		
      		$lquantity_total = $data_order['lquantity_total'];
      		$lshipping_price = $data_order['lshipping_price'];
      		$shipping_option = ' : '.$data_order['lshipping_option'].'';
      		$tax_product = $data_order['tax_product'];
      		$tax_shipping = $data_order['tax_shipping'];
      		$currency_id = $data_order['lcurrency_id'];
      		
      		//Currency
      		$sql_currency=$db->prepare_query("SELECT lcode from lumonata_currency where lcurrency_id=%s",$currency_id);
			$query_currency=$db->do_query($sql_currency);
			$data_currency = $db->fetch_array($query_currency);
			$currency = $data_currency['lcode'];
      		
      		$price[$i] = $data_order['lprice'];
      		$qty[$i] = $data_order['lqty'];
      		$price_total=$data_order['lprice_total'];
      		
			//Product Name
      		$sql_article=$db->prepare_query("select larticle_title from lumonata_articles where larticle_id=%s",$product_id[$i]);
      		$query_article = $db->do_query($sql_article);
      		$data_article = $db->fetch_array($query_article);
      		$product_name[$i]=$data_article['larticle_title'];
      		
      		//variant product
      		$array_variant = json_decode($data_order['product_variant'],true);
      		$product_parent = $array_variant['parent_variant'];
      		$product_child = $array_variant['child_variant'];
      		$parent_text = variant_product2($product_parent,0);
			$child_text = variant_product2($product_child,$product_parent);
			
       		$additional_product=default_price($product_id[$i]);
       		
       		$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data_order['lproduct_id'],'product_variant','products');
				      $result_variant=$db->do_query($sql_variant);
				      $data_variant=$db->fetch_array($result_variant);
				      //echo $data_variant['lvalue'];
				      $obj_variant = json_decode( $data_variant['lvalue'],true);
       		$count_it=0;
       		$varian_idparent=array();
       		$the_value[0]='';
	        if(is_array($array_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
				    $sql_added=$db->prepare_query("select * from lumonata_rules
				                where
				                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_idparent[$count_it]=$val;
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
					if($varian_idparent[$count_it] == $product_parent){
					      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
					      			if($subkey == $product_child){
					      				$the_value[$j]=$subval[0];
						      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
							                where
							                lparent = %d and lrule_id= %s",$val, $subkey);
						   				$execute_added_child=$db->do_query($sql_added_child);
						   				$data_added_child=$db->fetch_array($execute_added_child);
						   				$the_var_id[$j]=$subkey;
						   				$the_var[$j]=$data_added_child['lname'];
						      			$j++;
						      			
					      			}
					      	}
					      	break;
					}
			      	$count_it++;
	      		}
	   		}
			if($the_value[0]==''){
				$the_price[$i]=number_format($additional_product['price'],2,'.','');
			}else{
				$the_price[$i] = number_format($the_value[0],2,'.','');
			}
			
			///check item price by qty [yana] / ambil harga berdasarkan price range
				if (check_item_price($data_order['lproduct_id'], $qty[$i])!=false){
					$the_price[$i]	= number_format($the_price[$i],2,'.','');
					$the_price_new  = check_item_price($data_order['lproduct_id'], $qty[$i]);
					$the_price_new  = number_format($the_price_new,2,'.','');
					
					if ($the_price_new==$the_price[$i]){
						$the_price[$i] = $the_price[$i];
					}else{
						$the_price[$i] = $the_price_new;
					}
				}else{
					$the_price[$i] = $the_price[$i]; 
				}
			///			
			
			
			$the_code=$additional_product['code'];
			
			$the_parent_text[$i]=$parent_text['lname'];
			$the_child_text[$i]=$child_text['lname'];
			
			
			if(!empty($the_parent_text[$i])){
				$additional_variant = $the_parent_text[$i].' : '.$the_child_text[$i];
			}else{
				$additional_variant = "";
			}
			$items .='<tr>
						<td width="50%" style="background-color:#FFF">
							'.$the_code.'<br>'.$product_name[$i].'
								<br>'.$additional_variant.'
						</td>
						<td style="background-color:#FFF" align="right">'.$the_price[$i].'</td>
						<td style="background-color:#FFF" align="right">'.$qty[$i].'</td>						
						<td style="background-color:#FFF" align="right">'.number_format(($price[$i]*$qty[$i]),2,'.','').'</td>
					 </tr>';
			$i++;
			
			
			
       }
       
       $grand_total= $price_total + $lshipping_price + $tax_product+ $tax_shipping;
       $items .='<tr>
					<td colspan="3" style="text-align:right;background-color:#FFF" ><strong>Subtotal</strong> </td>
					<td style="background-color:#FFF;font-weight:bold;" align="right">'.$price_total.'</td>
				</tr>
				 <tr>
					<td colspan="3" style="text-align:right;background-color:#FFF" >Shipping '.$shipping_option.' </td>
					<td style="background-color:#FFF" align="right">'.$lshipping_price.'</td>
				</tr>
				 <tr>
					<td colspan="3" style="text-align:right;background-color:#FFF" >Tax </td>
					<td style="background-color:#FFF" align="right">'.$tax_product.'</td>
				</tr>
				 <tr>
					<td colspan="3" style="text-align:right;background-color:#FFF">Tax Shipping </td>
					<td style="background-color:#FFF" align="right">'.$tax_shipping.'</td>
				</tr>
				 <tr>
					<td colspan="3" style="text-align:right;background-color:#FFF" ><strong>Grand Total</strong> </td>
					<td style="background-color:#FFF;font-weight:bold;" align="right">'.$grand_total.'</td>
				</tr>
			    </table><br />';
       
       	$tbody='<table width="100%" style="background-color:#666;" cellpadding="1" cellspacing="1">
			    <tr style="background-color:#CCC">
					<td><strong>Product</strong></td>
			        <td align="right"><strong>Item Price ('.$currency.')</strong></td>
			        <td align="right"><strong>QTY</strong></td>
			        <td align="right"><strong>Total ('.$currency.')</strong></td>
			    </tr>';
       	
       	$items =$tbody.$items;
        add_variable('product_items',$items);
      
       
		$sql_payment=$db->prepare_query("Select * From lumonata_order_payment where lorder_id= %s",$d['lorder_id']);
	  	$query_payment=$db->do_query($sql_payment);
	  	$data_payment=$db->fetch_array($query_payment);
	  	
	  	if($data_payment['lpayment_method']=="payments_paypal_standard"){
	  		$the_methode="PAYPAL";
	  		$compare = "PayPal";
	  		///$link='<br>Please click link below to confirmation your payment<br><a href="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/paypal.php?ns='.md5($lorder_id).'">http://'.SITE_URL.'/lumonata-plugins/shopping_cart/paypal.php?ns='.md5($lorder_id).'</a>';
	  		$link = '';
	  	}else if($data_payment['lpayment_method']=="payments_cheque"){
	  		$the_methode="CHEQUE";
	  		$compare = "Cheque";
	  		$link='<br>Please click link below to confirmation your payment<br><a href="http://'.SITE_URL.'/payment-confirmation&ns='.md5($lorder_id).'">http://'.SITE_URL.'/payment-confirmation&ns='.md5($lorder_id).'</a>';
	  	}elseif($data_payment['lpayment_method']=="payments_bank_transfer"){
	  		$the_methode="BANK TRANSFER";
	  		$compare = "Bank transfer";
	  		$link='<br>Please click link below to confirmation your payment<br> <a href="http://'.SITE_URL.'/payment-confirmation&ns='.md5($lorder_id).'">http://'.SITE_URL.'/payment-confirmation&ns='.md5($lorder_id).'</a>';
	  	}else{
	  		$the_methode="-";
	  		$compare = "-";
	  		$link='-';
	  	}
	  	
		if(!empty($data_payment['laccount_number'])){
			$account_number_payment = $data_payment['laccount_number'];
		}else{$account_number_payment="-";}
		
		if(!empty($data_payment['payer_email'])){
			$email_payment = $data_payment['payer_email'];
		}else{$email_payment="-";}
		
		if(!empty($data_payment['ldestination_bank'])){
			$bank_payment = $data_payment['ldestination_bank'];
		}else{$bank_payment="-";}
		
	   add_variable('name_payer',$data_payment['lname_paid']);
       add_variable('method',$the_methode);
       add_variable('account_number',$account_number_payment);
       add_variable('bank',$bank_payment);
       add_variable('payer_email',$email_payment);
       
       $country_member = get_country($d['lcountry_id']);
	   $data_country_member = $db->fetch_array($country_member);
	   
       	add_variable('name_member',$d['lfname']." ".$d['llname']);
	    add_variable('address1_member',$d['laddress']);
	    add_variable('address2_member',$d['laddress2']);
	    add_variable('address3_member',$d['laddress3']);
	    add_variable('city_member',$d['lcity']);
	    add_variable('region_member',$d['lregion']);
	    add_variable('postalcode_member',$d['lpostal_code']);
	    add_variable('country_member',$data_country_member['lcountry']);
	    add_variable('phone1_member',$d['lphone']);
	    add_variable('phone2_member',$d['lphone2']);
	    add_variable('email_member',$d['lemail']);
       
       $sql_shipping_address=$db->prepare_query("Select lumonata_order_shipping.* From lumonata_order,lumonata_order_shipping 
						      				Where lumonata_order.lorder_shipping_id=lumonata_order_shipping.lorder_shipping_id 
						      				and lumonata_order.lorder_id= %s",$d['lorder_id']);
	  	$query_shipping_address=$db->do_query($sql_shipping_address);
	  	$data_shipping_address=$db->fetch_array($query_shipping_address);
	  	$country_shipping = get_country($data_shipping_address['lcountry_id']);
		$data_country_shipping = $db->fetch_array($country_shipping);
	  	
	  	add_variable('name_shipping',$data_shipping_address['lfname'].' '.$data_shipping_address['llname']);
	    add_variable('address1',$data_shipping_address['laddress']);
	    add_variable('address2',$data_shipping_address['laddress2']);
	    add_variable('address3',$data_shipping_address['laddress3']);
	    add_variable('city',$data_shipping_address['lcity']);
	    add_variable('region',$data_shipping_address['lregion']);
	    add_variable('postalcode',$data_shipping_address['lpostal_code']);
	    add_variable('country',$data_country_shipping['lcountry']);
	    add_variable('phone1',$data_shipping_address['lphone']);
	    add_variable('phone2',$data_shipping_address['lphone2']);
	    add_variable('email',$data_shipping_address['lemail']);

	    $text='';
	    $number='';
	    $data_payments=get_meta_data('payments','product_setting');
	  	$tmp_array_parent=array();
	  	$count_p=0;
	  	$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		$array_payments = json_decode($data_payments,true);
	      if(is_array($array_payments)){
		      foreach($array_payments['parent_payment'] as $key=>$val){
			      $count_c=0;
			      $tmp_array_parent['parent_payment'][$count_p]=$val;
					if($tmp_array_parent['parent_payment'][$count_p]==$data_payment['lpayment_method']){
						$number=$count_p;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
		
				      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
				      		//echo "child ".$subkey.":".$subval."<br>";
					      	$count_c++;
				      	
				      }
				      break;
					}
			    $count_p++;
	       	 }
	      }
	    if(isset($tmp_array_parent['child_payment'][$number]['text']) and !empty($tmp_array_parent['child_payment'][$number]['text'])){
	    	if($data_payment['lpayment_method']=="payments_paypal_standard"){
	    		$text .='';
	    	}else{
	    		$text .= $tmp_array_parent['child_payment'][$number]['text'];
	    	}
	    }
	    
	    $text .=$link;
	    
        $text.='<br>Thank you for shopping with us and have a nice day!';
        
        if($status=="admin"){
        	add_variable('text',' -');
        }else{
			add_variable('text',$text);
  		}

        parse_template('Bmail','xm',false);
		$theMailContent = return_template($status);
		
		$email_admin = get_meta_data('email');
		       
		$web_title = get_meta_data("web_title");
		
		///nonaktifkan ini_set utk sementara
		///ini_set("SMTP", SMTP_SERVER);
		///ini_set("sendmail_from", $email_admin);
		
		if($email_admin==$email_value){
			$sendToMail = $email_admin;
			$cc = '';
			$bcc = '';
			
			$subject = 'Pending Payment : ORDER ID ['.$lorder_id.'] - '.$d['lfname'].' '.$d['llname'];
			if ($the_methode=='PAYPAL'){
				$subject = 'New Order Paid : ORDER ID ['.$lorder_id.'] - '.$d['lfname'].' '.$d['llname'];
			}
		    $headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
			$headers .= "From: ".$email_admin."\r\n";
			$headers .= "Cc: " . "\r\n";
			$headers .= "Bcc: " . "\r\n"; 
		}else{
			$sendToMail = $d['lemail'];
			$cc = '';
			$bcc = '';
		
			$subject = 'New Order Notification for '.$web_title.' - Pending Payment : ORDER ID ['.$lorder_id.'] - '.$d['lfname'].' '.$d['llname'];
			if ($the_methode=='PAYPAL'){
				$subject = 'New Order Notification for '.$web_title.' - Paid : ORDER ID ['.$lorder_id.'] - '.$d['lfname'].' '.$d['llname'];
			}
		    $headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
			$headers .= "From: ".$email_admin."\r\n";
			$headers .= "Cc: " . "\r\n";
			$headers .= "Bcc: " . "\r\n"; 
		}
		
  		$send = mail($sendToMail,$subject,$theMailContent,$headers);
		
		if($send){
			return true;
		}else{
			return false;
		}
  }
  function variant_product2($rule_id,$lparent){
	global $db;
    $sql=$db->prepare_query("SELECT lname
   		FROM lumonata_rules WHERE lrule_id='%s' and lparent='%s'",$rule_id,$lparent);
    $data=$db->do_query($sql);
    $result=$db->fetch_array($data);
    
    return $result;
}

function is_payment_confirmation(){
	global $db;
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if($split_url[0]=='payment-confirmation'){
		return true;
	}else{
		return false;
	}
}

function payment_confirmation(){
	set_template(PLUGINS_PATH."/shopping_cart/templatePayment.html","Payment");
	add_block('mainBlock','Pm',"Payment");
	global $db;
	$lorder_id='';
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if(isset($split_url[1])){
		$lorder_id_encrypt = explode('=',$split_url[1]);
		$md5_order_id = $lorder_id_encrypt[1];
	  	$sql_cek = $db->prepare_query("Select * From lumonata_order");
	    $result_cek = $db->do_query($sql_cek);
	    while($cek=$db->fetch_array($result_cek)){
	    	if (md5($cek['lorder_id'])==$md5_order_id){
		       $lorder_id = $cek['lorder_id'];
		   	}
		 }
		 add_variable('lorder_id', $lorder_id);
	}

	//SAVE Confirmation
	$time=time();
	if(isset($_POST) && !empty($_POST)){
		$date_trans = strtotime($_POST['DateTrans']);
	  	if(!empty($_POST['order_id'])){
	  		$check_payment1=$db->prepare_query("SELECT * FROM lumonata_order_payment WHERE lorder_id=%s",$_POST['order_id']);
			$query_check1=$db->do_query($check_payment1);
	  		
			$check_payment=$db->prepare_query("SELECT * FROM lumonata_order_payment WHERE lstatus=%s and lorder_id=%s","2",$_POST['order_id']);
			$query_check=$db->do_query($check_payment);
			if($db->num_rows($query_check)<>0){
				$status='<div id="alertForm" style="display:none;background:#fdafaf;border:1px solid #fdafaf; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
				The Order Was Paid
				</div>';
			}else{
				$data_payment = $db->fetch_array($query_check1);

				if(isset($_POST['type']) && ($_POST['type']==$data_payment['lpayment_method']) && $_POST['type']=="payments_bank_transfer"){
					if(isset($_FILES['file']) && !empty($_FILES['file'])){
				  		$file_name = $_FILES['file']['name'];
				  		$file_size = $_FILES['file']['size'];
				        $file_type = $_FILES['file']['type'];
				        $file_source = $_FILES['file']['tmp_name'];
				         if(is_allow_file_size($file_size)){
				         	if(is_allow_file_type($file_type,'image')){
				                 $title_image_gallery = $file_name;
				                 if (empty($title_image_gallery)){
				                 	$fix_file_name='untitled';	
				                 	$title = "Untitled";
				                 }else{
				                 	$fix_file_name=file_name_filter($title_image_gallery);
				                 	$title = $title_image_gallery;
				                 }
				                 
				                 $file_ext=file_name_filter($file_name,true);
				                 $sef_url = $fix_file_name.'-'.time();
				                 $file_name=$sef_url. $file_ext;
				                 
				                 $sef_url = $fix_file_name.'-'.time();
				                 $destination1=PLUGINS_PATH."/shopping_cart/uploads/payment/".$file_name;
				                 $destinationsave="/lumonata-plugins/shopping_cart/uploads/payment/".$file_name;
				                 upload_resize($file_source,$destination1,$file_type,medium_image_width(),medium_image_height());
				         	}
				         }
				  	 }else{
				  	 	$destination1="";
				  	 	$destinationsave="";
				  	 }
	
			  		$qi=$db->prepare_query("UPDATE lumonata_order_payment SET lstatus=%s,
			      															ldlu=%s,
			      															ldate_trans=%s,
			      															lname_paid=%s,
			      															ldestination_bank=%s,
			      															laccount_number=%s,
			      															lfile=%s
			      															WHERE lorder_id=%s",'2',$time,$date_trans,$_POST['name_paid'],$_POST['destination_bank'],$_POST['account_number'],$destinationsave,$_POST['order_id']);
					$ri=$db->do_query($qi);
					if($ri){
						send_mail_confirmation($_POST['order_id'],"payments_bank_transfer");
						$status='<div id="alertForm" style="background:#84FF90;border:1px solid #84FF90; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
						Confirmation Order Successfully
						</div>';
					}else{
						$status='<div id="alertForm" style="background:#fdafaf;border:1px solid #fdafaf; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
						Failed Send Payment Confirmation
						</div>';
					}
			   	}elseif(isset($_POST['type']) && ($_POST['type']==$data_payment['lpayment_method']) && $_POST['type']=="payments_cheque"){
			   		
			   		$qi=$db->prepare_query("UPDATE lumonata_order_payment SET lstatus=%s,
			      															ldlu=%s,
			      															ldate_trans=%s,
			      															lname_paid=%s,
			      															cheque_number=%s
			      															WHERE lorder_id=%s",'2',$time,$date_trans,$_POST['name_paid'],$_POST['no_cheque'],$_POST['order_id']);
					$ri=$db->do_query($qi);
					if($ri){
						send_mail_confirmation($_POST['order_id'],"payments_cheque");
						$status='<div id="alertForm" style="background:#84FF90;border:1px solid #84FF90; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
						Confirmation Order Successfully
						</div>';
					}else{
						$status='<div id="alertForm" style="background:#fdafaf;border:1px solid #fdafaf; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
						Failed Send Payment Confirmation
						</div>';
					}
				}else{
					$status='<div id="alertForm" style="background:#fdafaf;border:1px solid #fdafaf; padding:5px; width:64%; text-align:left; margin-bottom:10px;margin-top:10px">
					Failed Send Payment Confirmation
					</div>';
				}
			}
	  	}else{
	  		$status='<div id="alertForm" style="background:#fdafaf;border:1px solid #fdafaf; padding:5px; width:100%; text-align:left; margin-bottom:2px;margin-top:0px">
					Failed Send Payment Confirmation
					</div>';
	  	}
	  	add_variable('status', $status);
	  	$jq='<script>
	  		$(document).ready(function(){
	  			$("#alertForm").fadeIn(200).delay(5000).fadeOut(200);
	  		});	
	  		</script>';
	  	add_variable('jdelay', $jq);
	}
	
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Payment Confirmation';   
    $actions->action['meta_title']['args'][0] = '';	
	$css = '<link rel="stylesheet" href="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery-ui/css/ui-lightness/jquery-ui-1.8.16.custom.css" type="text/css" media="screen" />';
	$js = '<script type="text/javascript" src="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery-ui/js/jquery-ui-1.8.16.custom.min.js"/></script>';
	add_variable('jquery_ui',$js);
	//$form_post = $css = SITE_URL.'/lumonata-plugins/shopping_cart/front_content';
	add_variable('jquery.ui.css',$css);
    add_variable('validationEngine.jquery',get_css('validationEngine.jquery.css'));
    
    
    $sql_art = $db->prepare_query("SELECT * FROM lumonata_articles where larticle_id=%s",164);//online 191
	$query_art=$db->do_query($sql_art);
	$data_art = $db->fetch_array($query_art);
	
    //Validation js
	add_variable('jquery.form',get_the_javascript('jquery.form.js'));
	add_variable('jquery.validationEngine',get_the_javascript('jquery.validationEngine.js'));
	add_variable('jquery.validationEngine-en',get_the_javascript('jquery.validationEngine-en.js'));
	add_variable('url', SITE_URL);
	add_variable('title','Payment Confirmation');
	add_variable('nameNamepaid','Name Paid');
	add_variable('nameDestinationbank','Destination Bank');
	add_variable('nameDestinationaccount','Destination Account');
	add_variable('nameDateTrans','Date');
	add_variable('textalertBrowseTransferReceiptFile','Transfer Receipt File');
	add_variable('link_terms_conditions','http://'.site_url().'/terms-conditions');
	add_variable('textTerm1','Terms & Conditions');
	add_variable('buttonConfirm','Confirm');
	
	add_variable('namet',$data_art['larticle_title']);
	add_variable('descriptiont',$data_art['larticle_content']);
	
	parse_template('mainBlock','Pm',false);
	return return_template('Payment');
	
}

  function send_mail_confirmation($lorder_id,$type){
  	global $db;
  	set_template(PLUGINS_PATH."/shopping_cart/mail_confirmation.html",'admin_mail');
	add_block('Cmail','ym','admin_mail');
	
	$check_payment=$db->prepare_query("SELECT * FROM lumonata_order_payment WHERE lstatus=%s and lorder_id=%s","2",$lorder_id);
	$query_check=$db->do_query($check_payment);
	$data_payment=$db->fetch_array($query_check);
	
	$img= '<img src="http://'.SITE_URL.$data_payment['lfile'].'">';
	$dattrans= date('d/m/Y',$data_payment['ldlu']);
	/*add_variable('date', $dattrans);
	add_variable('name', $data_payment['lname_paid']);
	add_variable('bank', $data_payment['ldestination_bank']);
	add_variable('account', $data_payment['laccount_number']);
	add_variable('image', $img);*/
	
	if($type=="payments_bank_transfer"){
		$content_email = '<tr>
            <td colspan="4" style="background-color:#CCC">Payment Confirmation</td>
        </tr>
        <tr>
            <td width="16%"  style="background-color:#FFF">Name Paid:</td>
            <td width="84%" style="background-color:#FFF" colspan="3">'.$data_payment['lname_paid'].'</td>
        </tr>
         <tr>
            <td style="background-color:#FFF">Destination Bank:</td>
            <td style="background-color:#FFF" colspan="3">'.$data_payment['ldestination_bank'].'</td>
        </tr>
         <tr>
            <td  style="background-color:#FFF">Destination Account:</td>
            <td style="background-color:#FFF" colspan="3">'.$data_payment['laccount_number'].'</td>
        </tr>
         <tr>
            <td style="background-color:#FFF">Date:</td>
            <td style="background-color:#FFF" colspan="3">'.$dattrans.'</td>
        </tr>
        <tr>
            <td style="background-color:#FFF">Transfer Receipt File</td>
            <td style="background-color:#FFF" colspan="3">'.$img.'</td>
        </tr>';
	}elseif ($type=="payments_cheque"){
		$content_email = '<tr>
            <td colspan="4" style="background-color:#CCC">Payment Confirmation</td>
        </tr>
        <tr>
            <td style="background-color:#FFF">Cheque Number</td>
            <td style="background-color:#FFF" colspan="3">'.$data_payment['cheque_number'].'</td>
        </tr>
        <tr>
            <td width="16%"  style="background-color:#FFF">Name Paid:</td>
            <td width="84%" style="background-color:#FFF" colspan="3">'.$data_payment['lname_paid'].'</td>
        </tr>
         <tr>
            <td style="background-color:#FFF">Date:</td>
            <td style="background-color:#FFF" colspan="3">'.$dattrans.'</td>
        </tr>';
	}else{
		$content_email="";
	}
	add_variable('content_email', $content_email);
	
	$email_admin = get_meta_data('email');
		       
	$web_title = get_meta_data("web_title");
	
	///nonaktifkan ini_set utk sementara
	///ini_set("SMTP", SMTP_SERVER);
	///ini_set("sendmail_from", $email_admin);
	$sendToMail = $email_admin;
	$cc = '';
	$bcc = '';
	
	$subject = 'Confirmation Payment : ORDER ID ['.$lorder_id.']';
    $headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
	//$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";							
	$headers .= "From: ".$email_admin."\r\n";
	$headers .= "Cc: " . "\r\n";
	$headers .= "Bcc: " . "\r\n"; 
	
    parse_template('Cmail','ym',false);
	$theMailContent = return_template('admin_mail');
  	$send = mail($sendToMail,$subject,$theMailContent,$headers);
	
	if($send){
		return true;
	}else{
		return false;
	}

  }
  function get_product_menu($app_name){
	global $db;
	global $actions;
	$mn = '';
	
	$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories',$app_name,0);
	
				
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	while($d=$db->fetch_array($result)){
		$mn .= '<H1><a href="http://'.site_url().'/products/'.$d['lsef'].'">'.$d['lname'].'</a><span class="toggle" rel="C'.$d['lrule_id'].'" title="Hide"> </span></H1>';
		$mn .= get_product_submenu($app_name,$d['lsef'],$d['lrule_id']);
	}
	return $mn;	
  }
  function get_product_submenu($app_name,$parent,$id){ 
	global $db;
	global $actions;
	$mn = '';
	
	$query=$db->prepare_query("SELECT a.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%s AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id
                                 GROUP BY a.lrule_id
                                 ORDER BY a.lorder ASC",
                                 'categories',$app_name,$id);
	
				
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	if ($row_count>0){
		$mn .= '<ul id="C'.$id.'">';
		while($d=$db->fetch_array($result)){
			$mn .= '<li><a href="http://'.site_url().'/'.$app_name.'/'.$parent.'/'.$d['lsef'].'">'.ucfirst($d['lname']).'</a></li>';		
		}
		$mn .= '</ul>';
	}else{
		return $mn;	
	}
	return $mn;	
 }
  function get_new_product($app_name='products',$limit=4,$show_link=true){
  	global $db;
  	global $actions;
  	$actions->action['meta_title']['func_name'][0] = 'New Products';   
    $actions->action['meta_title']['args'][0] = 'New Products';	   
    
    $cek_url = cek_url();   
    //jangan tampilkan new products jika category=shop-by-materials
    if (($cek_url[0]=='products') AND isset($cek_url[1])){  
    	if ($cek_url[1]=='shop-by-materials'){
    		return;
    		exit;
    	}
    }
    
    $link = '';
    if (($cek_url[0]=='products') AND isset($cek_url[1])){    
	    if ($show_link==true){
	    	$link = '<a href="http://'.site_url().'/new-products"> View All </a>';
	    }
	    $arr_rule = get_array_of_rule($cek_url[1],'categories','products');
	    $text_np = 'NEW '.strtoupper($arr_rule['lname']).'';
    }else{
     	if ($show_link==true){
	    	$link = '<a href="http://'.site_url().'/new-products"> View All </a>';
	    }
	    $text_np = 'NEW PRODUCTS';
    }
  	$caption = '<div class="caption star"> '.$text_np.' '.$link.'</div>';
  	$p 		 = '';
  	
  	
  
    if (($cek_url[0]=='products') AND isset($cek_url[1])){    	
		$arr_rule = get_array_of_rule($cek_url[1],'categories','products');
    	$q=$db->prepare_query("SELECT c.*,d.*,e.*
                                 FROM 
                                 	lumonata_rules a, 
                                 	lumonata_rule_relationship b, 
                                 	lumonata_articles c,
                                 	lumonata_attachment d,
                                 	lumonata_additional_fields e
                                 WHERE a.lrule=%s AND 
                                 	a.lgroup=%s AND 
                                 	a.lparent=%d AND
                                 	a.lrule_id=b.lrule_id AND
                                 	b.lapp_id=c.larticle_id AND
                                 	a.lsef=%s AND 
                                 	c.larticle_status=%s AND 
                                 	c.larticle_id=d.larticle_id AND 
  									c.larticle_id=e.lapp_id AND
  									lkey=%s 
                                 ORDER BY c.larticle_id DESC LIMIT %d",
                                 'categories','products',$arr_rule['lparent'],$cek_url[1],'publish','product_additional',8); 	
    
    }else{
    	$q = $db->prepare_query("SELECT * from lumonata_articles, lumonata_attachment,lumonata_additional_fields WHERE 
  							lumonata_articles.larticle_id=lumonata_attachment.larticle_id AND 
  							lumonata_articles.larticle_id=lumonata_additional_fields.lapp_id AND 
  							larticle_type=%s AND larticle_status=%s AND lkey=%s 
  							order by lumonata_articles.larticle_id DESC limit %d",$app_name,'publish','product_additional',$limit);
    }
    
  	
  
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	if ($n>0){
  		$p .= '<ul class="items">';
  		while ($d=$db->fetch_array($r)){
  			$arr_add_field = json_decode($d['lvalue'],true);  			
  			$p .= '<li>';
  			$p .= '<form id="formaddtocart'.$d['larticle_id'].'">';
  			$p .= '<input type="hidden" name="product_id" value="'.$d['larticle_id'].'">';
  			$p .= '<input type="hidden" name="sef_cat" value="'.$d['larticle_title'].'">';
  			$p .= '<input type="hidden" id="txt_qty'.$d['larticle_id'].'" name="qty" value="1"/>';
  			$p .= '<span class="alas"><a class="link-img" href="http://'.site_url().'/new-products/'.$d['lsef'].'"><img  id="img_'.$d['larticle_id'].'"  src="http://'.site_url().'/app/tb/tb.php?w=150&h=150&src=http://'.site_url().$d['lattach_loc_thumb'].'"  /></a><img class="link-qv" id="'.$d['larticle_id'].'" src="http://'.TEMPLATE_URL.'/images/icon_qv.png"></span>';  
  			$p .= '<span class="name">'.$arr_add_field['code'].'</span>';
  			$p .= get_price_range($d['larticle_id']);
  			$p .= ' <a href="javascript;" onClick="return false" class="addtocart_only" id="'.$d['larticle_id'].'"> </a>';
  			$p .= '</form>';
  			$p .= get_quick_view_product($d['larticle_id']);
  			$p .= '</li>';
  		}
  		$p .= '</ul>';
  	}else{
  		return;
  		exit;
  	}
  	$p = $caption.$p;
  	return $p;
  }
  function get_featured_product($app_name='products',$limit=4,$show_link=true){
  	global $db;
  	global $actions;
  	$actions->action['meta_title']['func_name'][0] = 'Featured Products';   
    $actions->action['meta_title']['args'][0] = 'Featured Products';	
    
    $cek_url = cek_url();

    //jangan tampilkan featured products jika category=shop-by-materials
    if (($cek_url[0]=='products') AND isset($cek_url[1])){  
    	if ($cek_url[1]=='shop-by-materials'){
    		return;
    		exit;
    	}
    }
    
    $link = '';
    if (($cek_url[0]=='products') AND isset($cek_url[1])){  
	    if ($show_link==true){
	    	$link = '<a href="http://'.site_url().'/featured-products"> View All </a>';
	    }
	    $arr_rule = get_array_of_rule($cek_url[1],'categories','products');
	    $text_fp = 'FEATURED '.strtoupper($arr_rule['lname']).'';	    
    }else{
    	if ($show_link==true){
	    	$link = '<a href="http://'.site_url().'/featured-products"> View All </a>';
	    }
	    $text_fp = 'FEATURED PRODUCTS';    	
    }    
  	$caption 	= '<div class="caption heart"> '.$text_fp.' '.$link.'</div>';
  	$p			= '';
  	
  	if (($cek_url[0]=='products') AND isset($cek_url[1])){  
	  	$arr_rule = get_array_of_rule($cek_url[1],'categories','products');
    	$q=$db->prepare_query("SELECT c.*,d.*,e.*
                                 FROM 
                                 	lumonata_rules a, 
                                 	lumonata_rule_relationship b, 
                                 	lumonata_articles c,
                                 	lumonata_attachment d,
                                 	lumonata_additional_fields e
                                 WHERE a.lrule=%s AND 
                                 	a.lgroup=%s AND 
                                 	a.lparent=%d AND
                                 	a.lrule_id=b.lrule_id AND
                                 	b.lapp_id=c.larticle_id AND
                                 	a.lsef=%s AND 
                                 	c.larticle_status=%s AND 
                                 	c.larticle_id=d.larticle_id AND 
  									c.larticle_id=e.lapp_id AND
  									lkey=%s 
                                 ORDER BY c.larticle_id DESC",
                                 'categories','products',$arr_rule['lparent'],$cek_url[1],'publish','product_additional'); 	
  	}else{
  		$q = $db->prepare_query("SELECT * from lumonata_articles, lumonata_attachment,lumonata_additional_fields WHERE 
	  							lumonata_articles.larticle_id=lumonata_attachment.larticle_id AND 
	  							lumonata_articles.larticle_id=lumonata_additional_fields.lapp_id AND 
	  							larticle_type=%s AND larticle_status=%s AND lkey=%s 
	  							order by lumonata_articles.larticle_id DESC",$app_name,'publish','product_additional');
  	}
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	$fi = 0;
  	if ($n>0){
  		$p .= '<ul class="items">';  		
  		while ($d=$db->fetch_array($r)){
  			$arr_add_field = json_decode($d['lvalue'],true); 
  			if ($fi<$limit) {
	  			if (isset($arr_add_field['set_as_featured']) && ($arr_add_field['set_as_featured']=='1')) {			
		  			$p .= '<li>';
		  			$p .= '<form id="formaddtocart'.$d['larticle_id'].'">';
		  			$p .= '<input type="hidden" name="product_id" value="'.$d['larticle_id'].'">';
		  			$p .= '<input type="hidden" name="sef_cat" value="'.$d['larticle_title'].'">';
		  			$p .= '<input type="hidden" id="txt_qty'.$d['larticle_id'].'" name="qty" value="1"/>';
		  			$p .= '<span class="alas"><a href="http://'.site_url().'/featured-products/'.$d['lsef'].'"><img src="http://'.site_url().'/app/tb/tb.php?w=150&h=150&src=http://'.site_url().$d['lattach_loc_thumb'].'"  /></a><img class="link-qv" id="'.$d['larticle_id'].'" src="http://'.TEMPLATE_URL.'/images/icon_qv.png"></span>';  
		  			$p .= '<span class="name">'.$arr_add_field['code'].'</span>';
		  			$p .= get_price_range($d['larticle_id']);
		  			$p .= ' <a href="#" class="addtocart_only" id="'.$d['larticle_id'].'"> </a>';
		  			$p .= '</form>';
		  			$p .= get_quick_view_product($d['larticle_id']);
		  			$p .= '</li>';
		  			
		  			$fi++;
	  			}
  			}  		
  		}  	
  		$p .= '</ul>';	
  	}else{
  		return; exit;
  	}
  	if ($fi==0){
  		return; exit;
  	}
  	
  	$p = $caption.$p;
  	return $p;
  }
  function get_quick_view_product($pid){
  	global $db;
	$items = '';
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND
                                
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 c.larticle_id=%d AND 
                                 c.larticle_status=%s",
                                'categories','products',$pid,'publish');
	$result=$db->do_query($query);
	
	$items.='<div class="detail" style="border:none;width:auto;">';
	$i=0;
	$data=$db->fetch_array($result);
			
	
		 $sql_product_images= $db->prepare_query("select
					lattach_id,
					larticle_id ,
					lattach_loc,
					lattach_loc_thumb,
					lattach_loc_medium,
					lattach_loc_large,
					ltitle 
					 from 
					lumonata_attachment,lumonata_additional_fields 
					where 
					lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
					and lumonata_additional_fields.lapp_id=%d
					and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
       $result_product_images=$db->do_query($sql_product_images);
       $data_product_images=$db->fetch_array($result_product_images);
       $images_url_large = "http://".SITE_URL.$data_product_images['lattach_loc_large'];
       $images_name=$data_product_images['lattach_loc_medium'];
       $rows_images=$db->num_rows($result_product_images);
       if($rows_images<>0){
       		$images_url="http://".SITE_URL.$images_name;
       }else{
       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
       }
		
		$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);
		      //echo $data_variant['lvalue'];
		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_idparent[$count_it]=$val;
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var_id[$count_it][$j]=$subkey;
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
				
       
       $sql_stock= $db->prepare_query("select * 
       								from lumonata_additional_fields 
       								where lapp_id=%s and lkey=%s and 
       								lapp_name=%s",
       								$data['larticle_id'],'product_additional','products');
       $result_stok=$db->do_query($sql_stock);
       $data_stok=$db->fetch_array($result_stok);
       $obj_stok = json_decode($data_stok['lvalue'],true);
       $the_product_price=$obj_stok['price'];
       $the_product_weight=$obj_stok['weight'];
       $the_product_code=$obj_stok['code'];
       $the_product_tax=$obj_stok['tax'];
      
	   $data_currency=get_symbol_currency();
	   $unit = get_unit_system();
	   if($unit=='imperial'){
	   	$unit_system='inch';
	   	$unit_weight='pounds';
	   }else if($unit=='metric'){
	   	$unit_system='cm';
	   	$unit_weight='kg';
	   }else{
	   	$unit_system='';
	   	$unit_weight='';
	   }
	   
		$items.='<style>
							.Error .ErrorContent {
							    background: none repeat scroll 0 0 #EE0101;
							    border: 2px solid #DDDDDD;
							    border-radius: 6px 6px 6px 6px;
							    box-shadow: 0 0 6px #000000;
							    color: #FFFFFF;
							    font-family: tahoma;
							    font-size: 11px;
							    padding: 20px 10px;
							    position: relative;
							    width: 150px;
							    z-index: 5001;
							}
		          </style>
				<script>
					
											
					$(document).ready(function(){
						$("#parent option:selected").removeAttr("selected");
						$("#parent").change(function(){ 
							var parent_selected= $("#parent option:selected").val();
							
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"variant_parent",
					    		lvalue:parent_selected,
					    		app_id:id
				              },function(data){
				              	split=data.split("&");
				              	//alert(split[0]);
				              	$("#text_price").html(split[0]);	
				              	$("#child").html(split[1]);
							});
						});
						
						$("#child").change(function(){ 
							var child_selected= $("#child option:selected").val();
							var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
							var id = "'.$data['larticle_id'].'";
					    	jQuery.post(thaUrl,{ 
					    		pKEY:"price_child",
					    		lvalue:child_selected,
					    		app_id:id
				              },function(data){
				              	$("#text_price").html(data);
							});
						});
						
					});
				</script>';

			
			if($the_product_weight<>"0"){
                $the_product_weight ='Weight: '.$the_product_weight.' '.$unit_weight.'';
            }else{
            	$the_product_weight = "";
            }
			$items.='<img src="'.$images_url.'" />';
			$items.='<div class="desc">
		              
		               <div class="name">'.$the_product_code.' </div>
		               <p>
		               '.$data['larticle_title'].'
		               <br>
		               '.nl2br($data['larticle_content']).'		              
		               '.$the_product_weight.'		              
		               </p>         
		             
		              
              		';
 					
					/*/variant price
						if($count_it==1){
							$items.='<p>'.$varian_parent[0].'<input type="hidden" name="parent" value="'.$varian_idparent[0].'"> : ';
							if($loop[0]==1){
								$items.=$the_var[0][0].'<input type="hidden" name="child" value="'.$the_var_id[0][0].'"></p>';
								$the_product_price=$the_value[0][0];
							}else{
                                 $items.='<select name="child" id="child">';
									for($b=0;$b<$loop[0];$b++){
										$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
									}
							     $items.='</select></p>';
							     $the_product_price=$the_value[0][0];
							}
						}else if($count_it>1){
                         	$items.='<p><select name="parent" id="parent">';
							for($a=0;$a<$count_it;$a++){
										$items.='<option value="'.$varian_idparent[$a].'">'.$varian_parent[$a].'</option>';
							}
                                 $items.='</select></p>';
                                 $items.='<p><select name="child" id="child">';
								for($b=0;$b<$loop[0];$b++){
									$items.='<option value="'.$the_var_id[0][$b].'">'.$the_var[0][$b].'</option>';
								}
						     $items.='</select></p>';
						     $the_product_price=$the_value[0][0];
						}else{
							 $items.='<div style="min-height:10px;float: left; width: 280px;"></div>';//nothink do
						}
						*/
						
                      $items.='<p id="price-product">
                            		<span class="title-product">Flat Price : </span>
                            		<span id="title-product2">'.$data_currency.' <span id="text_price">'.number_format($the_product_price,2,'.','').'</span></span>
                            	</p>
                      		  ';
                       
                      $items.=get_price_range($data['larticle_id'],'products',true);
                      
                      
                      $items.='<div class="buy-this"><span>Buy this item </span>';
                      $items.='<form name="formaddtocart_'.$pid.'" id="formaddtocart_'.$pid.'" class="addtocart">';
                      $items.='<input type="hidden" name="product_id" value="'.$data['larticle_id'].'">
		               		   <input type="hidden" name="sef_cat" value="">';
			          $items.='
			                      			<div style="text-align: right; height: 40px;position:absolute;right:0;">
				                            	<div class="Error" style="position: absolute; text-align: left; z-index: 1200; float: left;display: none;">
											        <div class="ErrorContent">
												        * The quantity only numeric												       
											        </div>
											    </div>
											 </div>
				                               
				                                <input type="text" id="txt_qty" name="qty" value="1"/>
				                               
				                                <button type="button" id="addtocart_'.$pid.'" class="link1"></button>';
			          $items.='</form>';
			          $items.='</div></div></div>'; 
			        
		
		
			
		$items.='<script>
				$(document).ready(function(){
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
				
					function IsNumeric(strString)
					   //  check for valid numeric strings	
					   {
					   var strValidChars = "0123456789";
					   var strChar;
					   var blnResult = true;
					
					   if (strString.length == 0) return false;
					
					   //  test strString consists of valid characters listed above
					   for (i = 0; i < strString.length && blnResult == true; i++)
					      {
					      strChar = strString.charAt(i);
					      if (strValidChars.indexOf(strChar) == -1)
					         {
					         blnResult = false;
					         }
					      }
					   return blnResult;
					 }
				$(".Error").css("display","none");
					$("#addtocart_'.$pid.'").click(function(){
							var x = $("#txt_qty").val();
							if(IsNumeric($("#txt_qty").val())==true && x!=0){
								//$("#formaddtocart").click(function(){
									var str = $("#formaddtocart_'.$pid.'").serialize();
									var thaUrl 	= "http://'.SITE_URL.'/front-product-ajax";
									jQuery.post(thaUrl,{ 
							    		pKEY:"add_to_chart",
							    		lvalue:str
						              },function(data){
						              	//$("#text_price").html(data);
						              	if(trim(data)=="failed"){ 
											$(".ErrorContent").html("Product Out Of Stock");
											$(".Error").fadeIn(200).delay(3000).fadeOut(200);
										}else{
						              		document.location="http://'.SITE_URL.'/shopping-cart";
						              	}
						              	
									//});
								});
							}else{
								$(".ErrorContent").html("*The quantity is numeric only");
								$(".Error").fadeIn(200).delay(3000).fadeOut(200);
							}
						});
				})
				
			</script>';
	$qc = '<div class="quickview" id="quickview_'.$pid.'"><a class="closeQuickView" id="'.$pid.'"><img src="http://'.TEMPLATE_URL.'/images/icon_close.png" /></a> '.$items.'</div>';;		
	return $qc;
  }
  function get_price_range($id,$app_name='products',$show_title=false){
  	global $db;
  	$pr = '';
  	$q = $db->prepare_query("SELECT lvalue from  lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s",$id,'product_price_range',$app_name);
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	if ($n>0){
  		$pr .= '<ul class="pr">';
  		if ($show_title==true){
  			$pr .='<li><span class="range"> Qty </span><span class="price"> Price </span></li>';
  		}
  		$d 		= $db->fetch_array($r);
  		$arr 	= json_decode($d['lvalue'],true);			
		foreach ($arr as $key => $value) {    
			list ($start,$end) = explode('-', $key);
			if ($end=='up'){ 
				$pr .= '<li> <span class="range"> '.$start.'+ </span> <span class="price"> $'.$value.' </span> </li>';
			}elseif($start==$end){
				$pr .= '<li> <span class="range"> '.$start.' </span> <span class="price"> $'.$value.' </span> </li>';
			}else{				
				$pr .= '<li> <span class="range"> '.$key.' </span> <span class="price"> $'.$value.' </span> </li>';
			}
		}	
		$pr .= '</ul>';
  	}
  	return $pr;
  }
  function loadMoreProduct_frontSide($app_name='products'){
  	global $db;
	$url		= explode('/',$_POST['uri']);	
	$arr_rule 	= get_array_of_rule($url[2],'categories','products');
	$limit		= get_meta_data('post_viewed','global_setting',0);
	$page		= $_POST['lastID'];     
	$start		=($page)*$limit;
    
	$items 		= '';		
	$query=$db->prepare_query("SELECT c.*
                                 FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c 
                                 WHERE a.lrule=%s AND 
                                 a.lgroup=%s AND 
                                 a.lparent=%d AND
                                 a.lrule_id=b.lrule_id AND
                                 b.lapp_id=c.larticle_id AND
                                 a.lsef=%s AND 
                                 c.larticle_status=%s
                                 ORDER BY a.lorder ASC limit %d,%d",
                                 'categories','products',$arr_rule['lparent'],$url[2],'publish',$start,$limit); 
	
	$result=$db->do_query($query);
	$row_count=$db->num_rows($result);
	if($db->num_rows($result)<>0){		
		
		while($data=$db->fetch_array($result)){
				$sql_product_images= $db->prepare_query("select
							lattach_id,
							larticle_id ,
							lattach_loc,
							lattach_loc_thumb,
							lattach_loc_medium,
							lattach_loc_large,
							ltitle 
							 from 
							lumonata_attachment,lumonata_additional_fields 
							where 
							lumonata_attachment.lattach_id = lumonata_additional_fields.lvalue
							and lumonata_additional_fields.lapp_id=%d
							and lumonata_additional_fields.lkey='product_image_thumbnail'",$data['larticle_id']);
		       $result_product_images=$db->do_query($sql_product_images);
		       $data_product_images=$db->fetch_array($result_product_images);
		       $images_name=$data_product_images['lattach_loc_thumb'];
		       $rows_images=$db->num_rows($result_product_images);
		       if($rows_images<>0){
		       		$images_url="http://".SITE_URL.$images_name;
		       }else{
		       		$images_url="http://".SITE_URL."/lumonata-plugins/shopping_cart/images/no-image-available.gif";
		       }
		       
			$sql_variant= $db->prepare_query("select * 
											from lumonata_additional_fields 
											where lapp_id=%s and lkey=%s and
											lapp_name=%s",
											$data['larticle_id'],'product_variant','products');
		      $result_variant=$db->do_query($sql_variant);
		      $data_variant=$db->fetch_array($result_variant);

		      $obj_variant = json_decode( $data_variant['lvalue'],true);
		      //print_r($obj_variant);
		      $count_it=0;
		      if(is_array($obj_variant)){
			      foreach($obj_variant['parent_variant'] as $key=>$val){
			      	$sql_added=$db->prepare_query("select * from lumonata_rules
	                where
	                lrule_id = %d", $val);
					$execute_added=$db->do_query($sql_added);
					$data_added=$db->fetch_array($execute_added);
					$varian_parent[$count_it]=$data_added['lname'];
					$j=0;
			      	foreach($obj_variant['child_variant'][$key] as $subkey => $subval){
			      			$the_value[$count_it][$j]=$subval[0];
			      			$sql_added_child=$db->prepare_query("select * from lumonata_rules
				                where
				                lparent = %d and lrule_id= %s",$val, $subkey);
			   				$execute_added_child=$db->do_query($sql_added_child);
			   				$data_added_child=$db->fetch_array($execute_added_child);
			   				$the_var[$count_it][$j]=$data_added_child['lname'];
			      			$j++;
			      			$loop[$count_it]=$j;
			      	}
			      	$count_it++;
			      }
		      }
		      
		       $sql_stock= $db->prepare_query("select * 
		       									from lumonata_additional_fields 
		       									where lapp_id=%s and lkey=%s and 
		       									lapp_name=%s",
		       									$data['larticle_id'],'product_additional','products');
	           $result_stok=$db->do_query($sql_stock);
	           $data_stok=$db->fetch_array($result_stok);
	           $obj_stok = json_decode($data_stok['lvalue'],true);
	           $the_product_stock=$obj_stok['price'];
	               	
			   $data_currency=get_symbol_currency();
			   $unit = get_unit_system();
			   
			   if($unit=='imperial'){
			   	$unit_system='inch';
			   }else if($unit=='metric'){
			   	$unit_system='cm';
			   }else{
			   	$unit_system='';
			   }

			
				$mg_info = getimagesize(str_replace(" ",'%20',$images_url));
				$items.='<li>';
				$items.='<a href="http://'.SITE_URL.'/'.$app_name.'/'.$url[1].'/'.$url[2].'/'.$data['lsef'].'"><img src="http://'.site_url().'/app/tb/tb.php?w=94&h=94&src='.$images_url.'"  /></a>';
				$items.='<div class="name"><a href="http://'.SITE_URL.'/'.$app_name.'/'.$url[1].'/'.$url[2].'/'.$data['lsef'].'">'.$data['larticle_title'].'</a></div>';
				$items.=get_price_range($data['larticle_id'],$app_name,true);
				
				$items.='<div class="buy-this"><span>Buy this item </span>';
				$items.='<form id="formaddtocart'.$data['larticle_id'].'">';
				$items.='<input type="hidden" name="product_id" value="'.$data['larticle_id'].'">';
				$items.='<input type="hidden" name="sef_cat" value="'.$data['larticle_title'].'">';
				$items.='<input type="text" id="txt_qty'.$data['larticle_id'].'" name="qty" value=""/>';
				$items.='<button type="button" class="addtocart_only" id="'.$data['larticle_id'].'"> </button>';
				$items.='</form>';  
				
				$items.='<div class="errorContent" id="ErrorContent'.$data['larticle_id'].'"><div class="error" id="Error'.$data['larticle_id'].'"> </div></div>';
				
				$items.='</li>';			
		}	
		$items.='<a id="'.($page+1).'" class="link-load-more" rel="'.$_POST['uri'].'" href="#"> Load more products </a>';
	}else{
		$items.='<img src="http://'.TEMPLATE_URL.'/images/bg_border.png">'; 
	}	
	echo $items;	
  }
  function get_stepBystep_order(){
  	$url = cek_url();
  	$html = '';
  	switch ($url[0]){
  		case "shopping-cart" : 
  			$html = '<ul class="step">
		            	<li class="done"> 1 <span> My Cart </span></li>
		                <li> 2 <span> Shipping and Billing </span></li>
		                <li> 3 <span> Order Confirmation </span></li>
            		</ul>';
  			break;
  		case "shipping-detail" : 
  			$html = '<ul class="step">
		            	<li class="done"> 1 <span> My Cart </span></li>
		                <li class="done"> 2 <span> Shipping and Billing </span></li>
		                <li> 3 <span> Order Confirmation </span></li>
            		</ul>';
  			break;  		
  		case "confirmation-order" : 
  			$html = '<ul class="step">
		            	<li  class="done"> 1 <span> My Cart </span></li>
		                <li  class="done"> 2 <span> Shipping and Billing </span></li>
		                <li  class="done"> 3 <span> Order Confirmation </span></li>
            		</ul>';
  			break; 	
  		default :
  			$html = '<ul class="step">
		            	<li> 1 <span> My Cart </span></li>
		                <li> 2 <span> Shipping and Billing </span></li>
		                <li> 3 <span> Order Confirmation </span></li>
            		</ul>';
  			break; 	
  	}  	
  	return $html;
  }
 function is_search_product(){
 	if (isset($_POST['key_search_product'])){
 		return true;
 	}
 	return false;
 }
 function search_product(){
 	global $db,$actions;
 	$p = '';
 	$p .= '<style> 
 			.warning_search{
 				background:#FFF9D5;
			    border: 1px solid #EFD054;
			    border-radius: 5px 5px 5px 5px;
			    box-shadow: 0 0 5px 2px #CCCCCC;
			    padding: 10px 5px;
			    position: absolute;			    
			   
			    -moz-box-sizing: border-box;
        		-webkit-box-sizing: border-box;
        		box-sizing: border-box;
 			}
 		  </style>';
 	$actions->action['meta_title']['func_name'][0] = 'Search Products';   
    $actions->action['meta_title']['args'][0] = 'Featured Products';	
 	
    $key = $_POST['key_search_product'];
    if($key==""){
    	$p .= '<div class="warning_search" style="width:730px;margin-top:50px;"> Please type a keyword!</div>';
    	return $p;
    }
    $q = $db->prepare_query("SELECT * from lumonata_articles, lumonata_attachment,lumonata_additional_fields WHERE 
  							lumonata_articles.larticle_id=lumonata_attachment.larticle_id AND 
  							lumonata_articles.larticle_id=lumonata_additional_fields.lapp_id AND 
  							larticle_type=%s AND larticle_status=%s AND lkey=%s AND 
  							(lumonata_articles.larticle_id LIKE %s OR  							
  							lumonata_articles.larticle_title LIKE %s OR
  							lumonata_articles.larticle_content LIKE %s )
  							order by lumonata_articles.larticle_id DESC",'products','publish','product_additional','%'.$key.'%','%'.$key.'%','%'.$key.'%');
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	if ($n>0){
  		$p = '<div class="caption star"> Search result</div>';
  		$p .= '<ul class="items">';
  		while ($d=$db->fetch_array($r)){
  			$arr_add_field = json_decode($d['lvalue'],true);  			
  			$p .= '<li>';
  			$p .= '<form id="formaddtocart'.$d['larticle_id'].'">';
  			$p .= '<input type="hidden" name="product_id" value="'.$d['larticle_id'].'">';
  			$p .= '<input type="hidden" name="sef_cat" value="'.$d['larticle_title'].'">';
  			$p .= '<input type="hidden" id="txt_qty'.$d['larticle_id'].'" name="qty" value="1"/>';
  			$p .= '<span class="alas"><a class="link-img" href="http://'.site_url().'/new-products/'.$d['lsef'].'"><img  id="img_'.$d['larticle_id'].'"  src="http://'.site_url().'/app/tb/tb.php?w=150&h=150&src=http://'.site_url().$d['lattach_loc_thumb'].'"  /></a><img class="link-qv" id="'.$d['larticle_id'].'" src="http://'.TEMPLATE_URL.'/images/icon_qv.png"></span>';  
  			$p .= '<span class="name">'.$arr_add_field['code'].'</span>';
  			$p .= get_price_range($d['larticle_id']);
  			$p .= ' <a href="javascript;" onClick="return false" class="addtocart_only" id="'.$d['larticle_id'].'"> </a>';
  			$p .= '</form>';
  			$p .= get_quick_view_product($d['larticle_id']);
  			$p .= '</li>';
  		}
  		$p .= '</ul>';
  	}else{
  		$p .= '<div class="warning_search" style="width:730px;margin-top:50px;"> No product found!</div>';
  		return $p;
  	}
  	return $p;
    
    
 }
?>