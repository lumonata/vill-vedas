<?php
/*
    Plugin Name: Shopping Cart
    Plugin URL: http://www.lumonata.com/
    Description: Plugin for arunna
    Author: WAP
    Author URL: http://lumonata.com/about-us/
    Version: 1.0
    
*/

include 'products.php';
include 'product_setting.php';
include 'front_content.php';
include 'product_order.php';
///system member by yana
include 'member.php';

add_privileges('administrator', 'shoppingcart', 'insert');
add_privileges('administrator', 'shoppingcart', 'update');
add_privileges('administrator', 'shoppingcart', 'delete');

add_privileges('administrator', 'products', 'insert');
add_privileges('administrator', 'products', 'update');
add_privileges('administrator', 'products', 'delete');

add_privileges('administrator', 'product_setting', 'insert');
add_privileges('administrator', 'product_setting', 'update');
add_privileges('administrator', 'product_setting', 'delete');

add_privileges('administrator', 'product_order', 'insert');
add_privileges('administrator', 'product_order', 'update');
add_privileges('administrator', 'product_order', 'delete');


add_main_menu(array('shoppingcart'=>'Online Shop'));
add_sub_menu('shoppingcart',array('product_setting'=>'Setting','products'=>'Product','product_order'=>'Order','contact_us'=>'Contact Email','social_media'=>'Social Media'));


function menu_css_shopping(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.shoppingcart{
		background:url('../lumonata-plugins/shopping_cart/images/ico-themes.png') no-repeat left top;
	}
	.lumonata_menu ul li.shoppingcart a#shoppingcart{
		background:url('../lumonata-plugins/shopping_cart/images/ico-arrow.png') no-repeat scroll right center rgba(0, 0, 0, 0);
		margin:0 5px 0 0;
	}
	</style>";
}

//Attemp the CSS to header
add_actions('header_elements','menu_css_shopping');


add_actions('products','get_admin_products','products','Product|Product');
add_actions('product_setting','get_admin_product_setting','product_setting','setting|General');
add_actions('product_order','get_admin_product_order','product_order','Order|Order');

///all ajax utk plugin shopping_cart
function shopping_cart_action_ajax(){ 
     add_actions('is_use_ajax', true); 
     //backend 
     if (isset($_POST['pKEY']) and $_POST['pKEY']=='add_priceGroup'){
     	add_priceGroup();       
     } 
     if (isset($_POST['pKEY']) and $_POST['pKEY']=='delete_priceGroup'){
     	delete_priceGroup();     	
     }  

     //frontend
     if (isset($_POST['pKEY']) and $_POST['pKEY']=='loadMoreProduct_frontSide'){
     	loadMoreProduct_frontSide();      	  	
     }  
   
        
} 
add_actions('shopping-cart-action-ajax_page', 'shopping_cart_action_ajax');

?>