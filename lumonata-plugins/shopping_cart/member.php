<?php 
//THE FRONT for member VIRTUAL PAGE HERE
add_actions('member-panel-ajax_page', 'member_panel_ajax');
  
function member_panel_ajax(){
  	add_actions('is_use_ajax', true);
  	
  	global $db;
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='do_member_login'){
  		do_member_login();
  	}
  	
	if(isset($_POST['pKEY']) and $_POST['pKEY']=='do_member_logout'){
  		do_member_logout();
  	}
  	
	if(isset($_POST['pKEY']) and $_POST['pKEY']=='member_register'){
  		member_register();
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='member_update_profile'){
  		member_update_profile();
  	}
  	
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='request_reset_password'){
  		request_reset_password();
  	}
  	if(isset($_POST['pKEY']) and $_POST['pKEY']=='do_reset_password_member'){
  		do_reset_password_member();
  	}
}
function do_member_login(){
	global $db;
	$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);			
			$vars[$key]=$value;
  		}
  	
  	$u_email= $vars['txt_email'];
  	$u_pwd  = $vars['txt_password']; 
  	$q = $db->prepare_query("SELECT * from lumonata_members where lemail=%s AND lpass=%s AND laccount_status =%d",$u_email,md5($u_pwd),1);
  	$r = $db->do_query($q);
  	$n = $db->num_rows($r);
  	$d = $db->fetch_array($r);
  	if ($n==0){
  		echo "Wrong username / password!";
  	}else{
  		$_SESSION['memberpanel_clientid']	=$d['lmember_id'];
		$_SESSION['memberpanel_email']		=$d['lemail'];
		$_SESSION['memberpanel_username']	=$d['lusername'];
		$_SESSION['memberpanel_fname']		=$d['lfname'];
		$_SESSION['memberpanel_lname']		=$d['llname'];
		echo "ok";
  	} 	
  	
}
function form_login(){
  	$f = '';
  	if (isMemberLogin()){
  		$f = get_form_member_active();
  	}else{
  		$f = get_form_login();
  	}  	
  	return $f;
  }
  
function isMemberLogin(){
  	if (isset($_SESSION['memberpanel_clientid']) && ($_SESSION['memberpanel_clientid']!='')){  		
  		return true;
  	}else{
  		return false;
  	}
  }

function get_form_login(){
  	$form = '<script>
				
		        $(document).ready(function(){					
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
				
				
		        	$(".closeForm").click(function () {
		        		$("#loginForm").css("display","none");
		        		$("#mask").css("display","none");
		        		$("#forgetForm").css("display","none");
		        	});
		        	
		        	$("#loginLink").click(function () {
		        		 $("#mask").css("display","block");        		
		        	     $("#loginForm").css("display","block");
		        	     $("#loginForm").css("z-index","1000");
		            });	
		           
		            //proses login ketika the form is submitted
					$("#formLogin").submit(function(){  
						var pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;"><img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif" /> Processing ... </div>\';
						
						var txt_email = $("#login_email").val();
						var txt_pwd   = $("#login_password").val();						
						
						if ((trim(txt_email)=="") || (trim(txt_pwd)=="")){
							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">Please complete input form below! </div>\';
							$("#loginResponse").html(pesan);
							$("#loginResponse").slideDown("slow");
						}else{
							$("#loginResponse").html(pesan);
							$("#loginResponse").slideDown("slow");
							
							var str = $(this).serialize();  
							var thaUrl 	  = "http://'.site_url().'/member-panel-ajax";							
							jQuery.post(thaUrl,{ 
					    		 pKEY: "do_member_login",
					    		lvalue:str
				            },function(msg){					            									
	 							if(trim(msg)=="ok")  {  			 								
									pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:green;">You are successfully logged in! <br /> Please wait while you are redirected...</div>\';
									$("#loginResponse").html("");
									$("#loginResponse").html(pesan);
									$("#loginResponse").slideDown("slow");								
									
									setTimeout(\'document.location="http://'.site_url().'/'.get_uri().'"\', 3000); 
		 						}else{ 
		 							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">\'+msg+\'</div>\';
		 							$("#loginResponse").html(pesan);
		 							$("#formLogin input[type=text]").val("");
		 							$("#formLogin input[type=password]").val("");
		 						}  
	      
	 						});
							return false;	   
	  						
	  					  }//end if
						 return false;

						}); 
					
					//forget password
					$("#forgetLink").click(function () { 
						  $("#loginForm").css("display","none");  		
		        	      $("#forgetForm").css("z-index","1001");        	     
		        	      $("#forgetForm").slideDown();
		            });	
		           
		            //proses forget ketika the form is submitted
					$("#formForget").submit(function(){  
						var pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;"><img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif" /> Processing ... </div>\';
						
						var txt_email = $("#forget_email").val();						
						
						if (trim(txt_email)==""){
							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">Please type your email! </div>\';
							$("#forgetResponse").html(pesan);
							$("#forgetResponse").slideDown("slow");
						}else{
							$("#forgetResponse").html(pesan);
							$("#forgetResponse").slideDown("slow");
							
							
							var thaUrl 	  = "http://'.site_url().'/member-panel-ajax";							
							jQuery.post(thaUrl,{ 
					    		 pKEY: "request_reset_password",
					    		 email:txt_email
				            },function(msg){					            									
	 							if(trim(msg)=="ok")  {  			 								
									pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:green;">Please check your email to get a confirmation link. </div>\';
									$("#forgetResponse").html("");
									$("#forgetResponse").html(pesan);
									$("#forgetResponse").slideDown("slow");	
									$("#formForget input[type=text]").val("");			
									
		 						}else{ 
		 							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">\'+msg+\'</div>\';
		 							$("#forgetResponse").html(pesan);
		 							$("#formForget input[type=text]").val("");		 							
		 						}  
	      
	 						});
							return false;	   
	  						
	  					  }//end if
						 return false;

						}); 	
						
		  	}) 
			</script>';
  	  
	$form .='<div id="mask"></div>
			<!-- login -->
			            	<div class="login_area" id="login_area_default" style="background: none;">
			            		<a id="loginLink"><img src="http://'.TEMPLATE_URL.'/images/icon_login.png" /></a>		            		
			            		
			            		<a href="http://'.site_url().'/member-register"><img src="http://'.TEMPLATE_URL.'/images/icon_register.png" /></a>
			            	</div>
			            	
			            	<div class="login_area" id="loginForm" style="display:none;">
			            		<img src="http://'.TEMPLATE_URL.'/images/icon_login.png" />
			            		<a class="closeForm"><img src="http://'.TEMPLATE_URL.'/images/icon_close.png" /></a> 
			            		<div class="loginResponse" id="loginResponse"></div>           		
			            		<form id="formLogin" name="formLogin">
			            			<input type="text" placeholder="Email" id="login_email" name="txt_email">
			            			<input type="password" placeholder="Password" id="login_password" name="txt_password">
			            			<label>Forget password? <a href="#" id="forgetLink">Click here</a></label>
			            			<input type="submit" value="Login" name="btn_login" id="btn_login">			            			
			            		</form>            		
			            	</div>
			            	
			            	<div class="login_area" id="forgetForm" style="display:none;min-height: 165px;height:auto; width: 422px;">			            		
			            		<a class="closeForm"><img src="http://'.TEMPLATE_URL.'/images/icon_close.png" /></a> 
			            		<div class="loginResponse" id="loginResponse"></div>           		
			            		<form id="formForget" name="formForget">
			            			<p><b>Forget Password</b></p>
			            			<label>Please insert the email that you use to register, we will send the link to reset your password to that email</label>
			            			<br>
			            			<div class="loginResponse" id="forgetResponse"></div>
			            			<input type="text" placeholder="Email" id="forget_email" name="txt_email" style="width:100%;">			            			
			            			
			            			<input type="submit" value="Submit Email" name="btn_forget" id="btn_forget" style="width:160px;">			            			
			            		</form>            		
			            	</div>
			<!--/login-->';
  	return $form;
  }
  
function get_form_member_active(){
	$form  = '';
	$form .= '<script>				
		        $(document).ready(function(){		        	
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
		        	$("#linkLogout").click(function () {
		        			var thaUrl 	  = "http://'.site_url().'/member-panel-ajax";							
							jQuery.post(thaUrl,{ 
					    		 pKEY: "do_member_logout"
				            },function(msg){										
	 							if(trim(msg)=="ok"){			
									setTimeout(\'document.location="http://'.site_url().'/'.get_uri().'"\', 1000); 
		 						}else{}  
	      
	 						});
							return false;	   
		            });		
		        })
		      </script>';
	$form .= '<div class="login_area" id="login_area_default" style="background: none;">
			   <img src="http://'.TEMPLATE_URL.'/images/icon_logout.png" />
			   <span style="color:#666;font-size:14px;">Hi <a href="http://'.site_url().'/profile">'.$_SESSION['memberpanel_fname'].' '.$_SESSION['memberpanel_lname'].'</a></span> | <a  id="linkLogout" rel="http://'.site_url().'/'.get_uri().'"> Logout </a>
			 </div>
			';
	return $form; 
}
function do_member_logout(){
	if (isset($_SESSION['memberpanel_clientid'])){
		unset($_SESSION['memberpanel_clientid']);
		unset($_SESSION['memberpanel_email']);		
		unset($_SESSION['memberpanel_username']	);
		unset($_SESSION['memberpanel_fname']);	
		unset($_SESSION['memberpanel_lname']);
		echo "ok";	
	}
  			
}
function get_form_login_2(){
  	$form = '<script>
				
		        $(document).ready(function(){					
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
								
		        	
		        	$(".closeForm").click(function () {
		        		$("#loginForm").css("display","none");
		        		$("#mask").css("display","none");
		        		$("#forgetForm").css("display","none");
		        	});
		        	
		        	$("#loginLink").click(function () {
		        		 $("#mask").css("display","block");        		
		        	     $("#loginForm").css("display","block");
		        	     $("#loginForm").css("z-index","1000");
		            });		 

		            //proses login ketika the form is submitted
					$("#formLogin").submit(function(){  
						var pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;"><img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif" /> Processing ... </div>\';
						
						var txt_email = $("#login_email").val();
						var txt_pwd   = $("#login_password").val();						
						
						if ((trim(txt_email)=="") || (trim(txt_pwd)=="")){
							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">Please complete input form below! </div>\';
							$("#loginResponse").html(pesan);
							$("#loginResponse").slideDown("slow");
						}else{
							$("#loginResponse").html(pesan);
							$("#loginResponse").slideDown("slow");
							
							var str = $(this).serialize();  
							var thaUrl 	  = "http://'.site_url().'/member-panel-ajax";							
							jQuery.post(thaUrl,{ 
					    		 pKEY: "do_member_login",
					    		lvalue:str
				            },function(msg){					            									
	 							if(trim(msg)=="ok")  {  			 								
									pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:green;">You are successfully logged in! <br /> Please wait while you are redirected...</div>\';
									$("#loginResponse").html("");
									$("#loginResponse").html(pesan);
									$("#loginResponse").slideDown("slow");								
									
									setTimeout(\'document.location="http://'.site_url().'/'.get_uri().'"\', 3000); 
		 						}else{ 
		 							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">\'+msg+\'</div>\';
		 							$("#loginResponse").html(pesan);
		 							$("#formLogin input[type=text]").val("");
		 							$("#formLogin input[type=password]").val("");
		 						}  
	      
	 						});
							return false;	   
	  						
	  					}//end if
						return false;

					}); 

					
					//forget password
					$("#forgetLink").click(function () { 
						  $("#loginForm").css("display","none");  		
		        	      $("#forgetForm").css("z-index","1001");        	     
		        	      $("#forgetForm").slideDown();
		            });	
		           
		            //proses forget ketika the form is submitted
					$("#formForget").submit(function(){  
						var pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;"><img style="vertical-align:middle;" src="http://'.TEMPLATE_URL.'/images/spinner.gif" /> Processing ... </div>\';
						
						var txt_email = $("#forget_email").val();						
						
						if (trim(txt_email)==""){
							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">Please type your email! </div>\';
							$("#forgetResponse").html(pesan);
							$("#forgetResponse").slideDown("slow");
						}else{
							$("#forgetResponse").html(pesan);
							$("#forgetResponse").slideDown("slow");
							
							
							var thaUrl 	  = "http://'.site_url().'/member-panel-ajax";							
							jQuery.post(thaUrl,{ 
					    		 pKEY: "request_reset_password",
					    		 email:txt_email
				            },function(msg){					            									
	 							if(trim(msg)=="ok")  {  			 								
									pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:green;">Please check your email to get a confirmation link. </div>\';
									$("#forgetResponse").html("");
									$("#forgetResponse").html(pesan);
									$("#forgetResponse").slideDown("slow");	
									$("#formForget input[type=text]").val("");			
									
		 						}else{ 
		 							pesan = \'<div style="display:block;margin:10px 0 -2px 0;padding: 3px;vertical-align: middle;color:red;">\'+msg+\'</div>\';
		 							$("#forgetResponse").html(pesan);
		 							$("#formForget input[type=text]").val("");		 							
		 						}  
	      
	 						});
							return false;	   
	  						
	  					  }//end if
						 return false;

						}); 	
		  	}) 
			</script>';
  	  
	$form .='<div id="mask"></div>
			<!-- login -->
			            	
			            	<div class="login_area" id="loginForm" style="display:none;">
			            		<img src="http://'.TEMPLATE_URL.'/images/icon_login.png" />
			            		<a class="closeForm"><img src="http://'.TEMPLATE_URL.'/images/icon_close.png" /></a> 
			            		<div class="loginResponse" id="loginResponse"></div>           		
			            		<form id="formLogin" name="formLogin">
			            			<input type="text" placeholder="Email" id="login_email" name="txt_email">
			            			<input type="password" placeholder="Password" id="login_password" name="txt_password">
			            			<label>Forget password? <a id="forgetLink" href="#">Click here</a></label>
			            			<input type="submit" value="Login" name="btn_login" id="btn_login">			            			
			            		</form>            		
			            	</div>
			            	
			            	<div class="login_area" id="forgetForm" style="display:none;min-height: 165px;height:auto; width: 422px;">			            		
			            		<a class="closeForm"><img src="http://'.TEMPLATE_URL.'/images/icon_close.png" /></a> 
			            		<div class="loginResponse" id="loginResponse"></div>           		
			            		<form id="formForget" name="formForget">
			            			<p><b>Forget Password</b></p>
			            			<label>Please insert the email that you use to register, we will send the link to reset your password to that email</label>
			            			<br>
			            			<div class="loginResponse" id="forgetResponse"></div>
			            			<input type="text" placeholder="Email" id="forget_email" name="txt_email" style="width:100%;">			            			
			            			
			            			<input type="submit" value="Submit Email" name="btn_forget" id="btn_forget" style="width:160px;">			            			
			            		</form>            		
			            	</div>
			<!--/login-->';
  	return $form;
  }
function get_link_please_login(){
	$link = '';
	
	if (isMemberLogin()){
		$link = '<div class="please-login"><img src="http://'.TEMPLATE_URL.'/images/you_are_login.png"></div>';
	}else{
		$link = '<div class="please-login"> Already a member? Please <a id="loginLink"><b>login</b> <img src="http://'.TEMPLATE_URL.'/images/icon_please_login.png"></a></div>';
		$link .= get_form_login_2();
	}
	return $link;
}
function get_form_input_billing(){
	global $db;
	$f = '';
	if (isMemberLogin()){
		$q = $db->prepare_query("select * from lumonata_members where lmember_id=%s",$_SESSION['memberpanel_clientid']);
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);
		
		$_SESSION['country']=$d['lcountry_id'];
		
		$f = '<ul class="first">
            		<li>First Name* </li>
                    <li><input class="validate[required] input_large" type="text" name="firstname" id="firstname" value="'.$d['lfname'].'"/> </li>
                </ul>
                
                <ul>
                    <li>Last Name* </li>
                    <li><input class="validate[required] input_large" type="text" name="lastname" id="lastname" value="'.$d['llname'].'"/></li>
                </ul>

                <ul>
                	<li>Address*</li>
                    <li><input class="validate[required] input_large" type="text" name="address" id="address" value="'.$d['laddress'].'"/></li>
                </ul>
                 
                <ul>
                	<li>&nbsp;</li>
                    <li><input class="input_large" type="text" name="second_address" id="second_address" value="'.$d['laddress2'].'"/></li>
                </ul>
                
                <ul>
                    <li>&nbsp;</li>
                    <li><input class="input_large" type="text" name="third_address" id="third_address" value="'.$d['laddress3'].'"/></li>
               </ul>
              	
               <ul>
               		<li>City*</li>
                    <li><input class="validate[required] input_large" type="text" name="city" id="city" value="'.$d['lcity'].'"/></li>
                </ul>

                 <ul>
               		<li>Region/State*</li>
                    <li><input class="input_large" type="text" name="region" id="region" value="'.$d['lregion'].'"/><span class="fi-note">type None if not aplicable</span></li>
                 </ul>
                 
                 <ul>
               		<li>Postal Code*</li>
                    <li><input class="validate[required,custom[onlyNumber] input_medium" type="text" name="postal" id="postal" value="'.$d['lpostal_code'].'"/></li>
                  </ul>

                 <ul>
               		<li>Country*</li>
                    <li><div class="c-shipping"><span id="select_view"><select name="country" title="select country" id="country" class="validate[required] class_country_sh select" rel="top">'.country_member().'</select><span></div></li>
                  </ul>
                 
                 <ul>
               		<li>Phone*</li>                            
                    <li><input class="validate[required] input_large s" type="text" name="primary_phone" id="primary_phone" value="'.$d['lphone'].'"/></li>
                 </ul>
                 
                 <ul>
               		<li>Phone 2</li>
					<li><input class="input_large" type="text" name="alternative_phone" id="alternative_phone" value="'.$d['lphone2'].'"/></li>
				</ul>
                
				<ul>
               		<li>Email*</li>
                    <li><input class="validate[required,custom[email]] input_large" type="text" name="email" id="email" value="'.$d['lemail'].'"/></li>
               </ul>';	
	}else{
		$f = '<ul class="first">
            		<li>First Name* </li>
                    <li><input class="validate[required] input_large" type="text" name="firstname" id="firstname" value=""/> </li>
                </ul>
                
                <ul>
                    <li>Last Name* </li>
                    <li><input class="validate[required] input_large" type="text" name="lastname" id="lastname" value=""/></li>
                </ul>

                <ul>
                	<li>Address*</li>
                    <li><input class="validate[required] input_large" type="text" name="address" id="address" value=""/></li>
                </ul>
                 
                <ul>
                	<li>&nbsp;</li>
                    <li><input class="input_large" type="text" name="second_address" id="second_address" value=""/></li>
                </ul>
                
                <ul>
                    <li>&nbsp;</li>
                    <li><input class="input_large" type="text" name="third_address" id="third_address" value=""/></li>
               </ul>
              	
               <ul>
               		<li>City*</li>
                    <li><input class="validate[required] input_large" type="text" name="city" id="city" value=""/></li>
                </ul>

                 <ul>
               		<li>Region/State*</li>
                    <li><input class="input_large" type="text" name="region" id="region" value=""/><span class="fi-note">type None if not aplicable</span></li>
                 </ul>
                 
                 <ul>
               		<li>Postal Code*</li>
                    <li><input class="validate[required,custom[onlyNumber] input_medium" type="text" name="postal" id="postal" value=""/></li>
                  </ul>

                 <ul>
               		<li>Country*</li>
                    <li><div class="c-shipping"><span id="select_view"><select name="country" title="select country" id="country" class="validate[required] class_country_sh select" rel="top">'.country_member().'</select><span></div></li>
                  </ul>
                 
                 <ul>
               		<li>Phone*</li>                            
                    <li><input class="validate[required] input_large s" type="text" name="primary_phone" id="primary_phone" value=""/></li>
                 </ul>
                 
                 <ul>
               		<li>Phone 2</li>
					<li><input class="input_large" type="text" name="alternative_phone" id="alternative_phone" value=""/></li>
				</ul>
                
				<ul>
               		<li>Email*</li>
                    <li><input class="validate[required,custom[email]] input_large" type="text" name="email" id="email" value=""/></li>
               </ul>';	
	}
	return $f;
}
function get_user_agreement(){
	$a = '';
	if(!isMemberLogin()){
		$a .= '<div class="agreement">';
		$a .= 'By clicking the check out button, an account will be created automatically, please check your email to get your password. You can change your password in our member area';
		$a .= '</div>';
	}
	return $a;
}
function form_profile(){
	global $db, $actions;
	$actions->action['meta_title']['func_name'][0] = 'My profile';   
    $actions->action['meta_title']['args'][0] = '';	
	if (!isMemberLogin()){
		header('Location: http://'.site_url().'/');		
	}
	
	$q = $db->prepare_query("SELECT * from lumonata_members where lmember_id=%s", $_SESSION['memberpanel_clientid']);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	
    $f = '<h1 style="color: #666666;font-size: 18px;margin: 0;padding: 15px 0;"> My Profile </h1>';
    $f.= '<div class="register-note"></div>';
    $f.= '<form id="formProfile" name="formProfile">';
    $f.= '<div class="finput" style="width:100%;">            	
            	<ul>
            		<li>First Name* </li>
                    <li><input type="text" value="'.$d['lfname'].'" id="firstname" name="firstname" class="validate[required] input_large"> </li>
                </ul>
                
                <ul>
                    <li>Last Name* </li>
                    <li><input type="text" value="'.$d['llname'].'" id="lastname" name="lastname" class="validate[required] input_large"></li>
                </ul>

                <ul>
                	<li>Address*</li>
                    <li><input type="text" value="'.$d['laddress'].'" id="address" name="address" class="validate[required] input_large"></li>
                </ul>
                 
                <ul>
                	<li>&nbsp;</li>
                    <li><input type="text" value="'.$d['laddress2'].'" id="second_address" name="second_address" class="input_large"></li>
                </ul>
                
                <ul>
                    <li>&nbsp;</li>
                    <li><input type="text" value="'.$d['laddress3'].'" id="third_address" name="third_address" class="input_large"></li>
               </ul>
              	
               <ul>
               		<li>City*</li>
                    <li><input type="text" value="'.$d['lcity'].'" id="city" name="city" class="validate[required] input_large"></li>
                </ul>

                 <ul>
               		<li>Region/State*</li>
                    <li><input type="text" value="'.$d['lregion'].'" id="region" name="region" class="input_large"><span class="fi-note">type None if not aplicable</span></li>
                 </ul>
                 
                 <ul>
               		<li>Postal Code*</li>
                    <li><input type="text" value="'.$d['lpostal_code'].'" id="postal" name="postal" class="validate[required,custom[onlyNumber] input_medium"></li>
                  </ul>

                 <ul>
               		<li>Country*</li>
                    <li style=" position: relative; width: 140px;"><div class="c-shipping"><span id="select_view"><select name="country" title="select country" id="country" class="validate[required] class_country_sh select">'.select_country_shipped($d['lcountry_id']).'</select></span></div></li>
                  </ul>
                 
                 <ul>
               		<li>Phone*</li>                            
                    <li><input type="text" value="'.$d['lphone'].'" id="primary_phone" name="primary_phone" class="validate[required] input_large s"></li>
                 </ul>
                 
                 <ul>
               		<li>Phone 2</li>
					<li><input type="text" value="'.$d['lphone2'].'" id="alternative_phone" name="alternative_phone" class="input_large"></li>
				</ul>
                
				<ul>
               		<li>Email*</li>
                    <li><input type="text" value="'.$d['lemail'].'" id="email" name="email" class="validate[required,custom[email]] input_large"></li>
               </ul>  

               <div style="color: #666666;font-size: 18px;margin: 0;padding: 15px 0;">Change Password</div>
               <div style="color: #666666;font-size: 14px;margin: 0;padding: 15px 0;font-family:GandhiSansRegular,Helvetica,sans-serif;">If you need to change your password, please fill the form below. Otherwise, leave it as it is</div>
               
               <ul>
               		<li style="width:170px;">Current Password*</li>
					<li><input type="password" value="" id="pwd0" name="pwd0" class="input_large"></li>
			   </ul>
			   
               <ul>
               		<li style="width:170px;">New Password*</li>
					<li><input type="password" value="" id="pwd1" name="pwd1" class="input_large"></li>
				</ul>
                
				<ul>
               		<li style="width:170px;">Re-type New Password*</li>
                    <li><input type="password" value="" id="pwd2" name="pwd2" class="input_large"></li>
               </ul>    

               
              
            	
               <input type="submit" class="btn_register_submit" id="btn_register_submit" style="float:left;" value="Save">
               		<div id="responseError" class="responseError">
               		
               		</div>
               </div>';
	$f.='</form>';
	$f.='<style> 
			.responseError{
			  	display:none;
			    border: 1px solid #EFD054;
			    color: red;
			    float: left;
			    margin-left: 20px;
			    margin-top: 20px;
			    overflow: hidden;
			    padding: 10px;
			    background: none repeat scroll 0 0 #FFF9D5;
			}
		</style>';
	$f.='<script>
			
			function trim(str) {
				var	str = str.replace(/^\s\s*/, ""),
					ws = /\s/,
					i = str.length;
				while (ws.test(str.charAt(--i)));
				return str.slice(0, i + 1);
			}
          	
			
		$(document).ready(function(){
		  $("#formProfile input").click(function(){
					$("#responseError").fadeOut();
		  });
		  
		 
		  //submit
		  $(function(){
		  	$("#formProfile").validationEngine();		  
			$("#formProfile").submit(function(){
				var str = $("#formProfile").serialize();
				var thaUrl 	= "http://'.site_url().'/member-panel-ajax";				
				 if($("#formProfile").validationEngine("validate") == true){				 	
					jQuery.post(thaUrl,{ 
			    		pKEY:"member_update_profile",
			    		lvalue:str
		              },function(data){
		              	if (trim(data)=="passwordIsNotMacth"){	
		                	$("#formProfile input[type=password]").val("");
		                	$("#responseError").html("Your password is not match!");
		                	$("#responseError").fadeIn();		               
		                }else  if (trim(data)=="newPasswordIsEmpty"){            	
		                	$("#responseError").html("Please type new password!");
		                	$("#responseError").fadeIn();
		                }else if (trim(data)=="currentPasswordIsWrong"){		                	
		                	$("#formProfile input[type=password]").val("");
		                	$("#responseError").html("Your current password is wrong!");
		                	$("#responseError").fadeIn();	
		                }else if (trim(data)=="ok"){         	
		                	$("#btn_register_submit").slideUp();
		                	$("#responseError").css("color","#99CC33");
		                	$("#responseError").css("margin-left","0");
		                	$("#responseError").css("width","450px");
		                	$("#responseError").html("Your profile is update succesfully");
		                	$("#responseError").fadeIn();
		                }else{
		                	$("#responseError").html("There are something wrong. Please try again later!");
		                	$("#responseError").fadeIn();
		                }
		                
		                
					});
					return false;
				 }
			});
		  });
		})
      </script>';
	return $f;
}
function form_register(){
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Resgiter';   
    $actions->action['meta_title']['args'][0] = '';	
	if (isMemberLogin()){
		header('Location: http://'.site_url().'/');		
	}
    $f = '<h1 style="color: #666666;font-size: 18px;margin: 0;padding: 15px 0;"> Register </h1>';
    $f.= '<div class="register-note">Register as our member so you don\'t have to re-fill your information everytime you make an order, please note that fields with * are required</div>';
    $f.= '<form id="formRegister" name="formRegister">';
    $f.= '<div class="finput">            	
            	<ul>
            		<li>First Name* </li>
                    <li><input type="text" value="" id="firstname" name="firstname" class="validate[required] input_large"> </li>
                </ul>
                
                <ul>
                    <li>Last Name* </li>
                    <li><input type="text" value="" id="lastname" name="lastname" class="validate[required] input_large"></li>
                </ul>

                <ul>
                	<li>Address*</li>
                    <li><input type="text" value="" id="address" name="address" class="validate[required] input_large"></li>
                </ul>
                 
                <ul>
                	<li>&nbsp;</li>
                    <li><input type="text" value="" id="second_address" name="second_address" class="input_large"></li>
                </ul>
                
                <ul>
                    <li>&nbsp;</li>
                    <li><input type="text" value="" id="third_address" name="third_address" class="input_large"></li>
               </ul>
              	
               <ul>
               		<li>City*</li>
                    <li><input type="text" value="" id="city" name="city" class="validate[required] input_large"></li>
                </ul>

                 <ul>
               		<li>Region/State*</li>
                    <li><input type="text" value="" id="region" name="region" class="input_large"><span class="fi-note">type None if not aplicable</span></li>
                 </ul>
                 
                 <ul>
               		<li>Postal Code*</li>
                    <li><input type="text" value="" id="postal" name="postal" class="validate[required,custom[onlyNumber] input_medium"></li>
                  </ul>

                 <ul>
               		<li>Country*</li>
                    <li style=" position: relative; width: 140px;"><div class="c-shipping"><span id="select_view"><select name="country" title="select country" id="country" class="validate[required] class_country_sh select">'.select_country_shipped().'</select></span></div></li>
                  </ul>
                 
                 <ul>
               		<li>Phone*</li>                            
                    <li><input type="text" value="" id="primary_phone" name="primary_phone" class="validate[required] input_large s"></li>
                 </ul>
                 
                 <ul>
               		<li>Phone 2</li>
					<li><input type="text" value="" id="alternative_phone" name="alternative_phone" class="input_large"></li>
				</ul>
                
				<ul>
               		<li>Email*</li>
                    <li><input type="text" value="" id="email" name="email" class="validate[required,custom[email]] input_large"></li>
               </ul>  

               <div style="color: #666666;font-size: 18px;margin: 0;padding: 15px 0;">Password</div>
               <ul>
               		<li>Type Password*</li>
					<li><input type="password" value="" id="pwd1" name="pwd1" class="validate[required] input_large"></li>
				</ul>
                
				<ul>
               		<li>Re-type Password*</li>
                    <li><input type="password" value="" id="pwd2" name="pwd2" class="validate[required] input_large"></li>
               </ul>    

               <div style="color: #333;font-size: 14px;margin: 0;padding: 15px 0;">Please type the code below*</div>
               <div style="display:block;">
	               <img style="height: 26px; vertical-align: middle;" src="http://'.site_url().'/app/chapta/val-image.php">
	               <input name="txt_chapta" id="txt_chapta" class="validate[required]" type="text" style="width:80px;margin-left:10px;vertical-align:middle;">
               </div>
            	
               <input type="submit" class="btn_register_submit" id="btn_register_submit" style="float:left;" value="Register">
               		<div id="responseError" class="responseError">
               		
               		</div>
               </div>';
	$f.='</form>';
	$f.='<style> 
			.responseError{
			  	display:none;
			    border: 1px solid #EFD054;
			    color: red;
			    float: left;
			    margin-left: 20px;
			    margin-top: 20px;
			    overflow: hidden;
			    padding: 10px;
			    background: none repeat scroll 0 0 #FFF9D5;
			}
		</style>';
	$f.='<script>
			
			function trim(str) {
				var	str = str.replace(/^\s\s*/, ""),
					ws = /\s/,
					i = str.length;
				while (ws.test(str.charAt(--i)));
				return str.slice(0, i + 1);
			}
          	
			
		$(document).ready(function(){
		  $("#formRegister input").click(function(){
					$("#responseError").fadeOut();
		  });
		  
		 
		  //submit
		  $(function(){
		  	$("#formRegister").validationEngine();		  
			$("#formRegister").submit(function(){
				var str = $("#formRegister").serialize();
				var thaUrl 	= "http://'.site_url().'/member-panel-ajax";				
				 if($("#formRegister").validationEngine("validate") == true){				 	
					jQuery.post(thaUrl,{ 
			    		pKEY:"member_register",
			    		lvalue:str
		              },function(data){
		              	if (trim(data)=="emailIsExist"){	
		                	$("#responseError").html("Your email is already register!");
		                	$("#responseError").fadeIn();		               
		                }else  if (trim(data)=="passwordNotMatch"){		                	
		                	$("#formRegister input[type=password]").val("");
		                	$("#responseError").html("Your password is not match!");
		                	$("#responseError").fadeIn();
		                }else if (trim(data)=="chaptaNotValid"){		                	
		                	$("#formRegister input[id=txt_chapta]").val("");
		                	$("#responseError").html("Please type the code correctly!");
		                	$("#responseError").fadeIn();
		                }else if (trim(data)=="ok"){		                	
		                	$("#formRegister input").val("");
		                	$("#btn_register_submit").slideUp();
		                	$("#responseError").css("color","#99CC33");
		                	$("#responseError").css("margin-left","0");
		                	$("#responseError").css("width","450px");
		                	$("#responseError").html("Registration is succesfully. Please check your email to activate your account.");
		                	$("#responseError").fadeIn();
		                }else{
		                	$("#responseError").html("There are something wrong. Please try again later!");
		                	$("#responseError").fadeIn();
		                }
		                
		                
					});
					return false;
				 }
			});
		  });
		})
      </script>';
	return $f;
}
function member_register(){
	global $db;
	$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);			
			$vars[$key]=$value;
  		}
  	
  	//validasi
	$q = $db->prepare_query("SELECT lemail from lumonata_members where lemail=%s AND laccount_status=%d",$vars['email'],1);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		echo "emailIsExist";
	}elseif ($vars['pwd1']!=$vars['pwd2']){
  		echo "passwordNotMatch";
  	}elseif($vars['txt_chapta']!=$_SESSION['rand_code']){
  		echo "chaptaNotValid";  
  	}else{
  		//do register and echo "ok"
  		$id_member 	= save_member($vars); //lactv_code = md5(lmember_id)
  		$fullname	= $vars['firstname'].' '.$vars['lastname'];
  		$code		= md5($id_member);
  		$to_email	= $vars['email'];
  		if (send_mail_activate_account($fullname,$code,$to_email)){
  			echo "ok";
  		}else{
  			echo "Failed to send an email!";
  		}
  	}
}
function send_mail_activate_account($fullname,$code,$to_email,$data_account=''){
		
		$subject   	= 'Activate your account at '.site_url().'';
  		$message	= 'Dear '.$fullname.',<br>
  					   Thank you for register in our site http://'.site_url().' <br>
  					   '.$data_account.'
  					   
  					   Please complete your registration by click link below to activate your account. <br>
  					   <a href="http://'.site_url().'/member-activate&'.$code.'"> http://'.site_url().'/member-activate&'.$code.' </a>  <br> <br>
  					   
  					   Register as our member so you don\'t have to re-fill your information everytime you make an order. <br><br>
  					   
  					   Best regards,<br>
  					   '.site_url().'
  					  ';
  		$from_email = get_meta_data('email');
  		$from_name  = '';
  		$to_email   = $to_email;
  		$to_name	= $fullname;
  		$type		= 'html';
  		
  		if (sendMail($subject,$message,$from_email,$from_name,$to_email,$to_name,$type)){
  			return true;
  		}else{
  			return false;
  		}
}
function get_member($id=''){
	global $db;
	$q = $db->prepare_query("SELECT * from lumonata_members where lmember_id=%s",$id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	return $d;
}
function save_member($param){
	global $db;
  	$id_member = set_id_auto('lumonata_members','lmember_id','MI');  
	$sql=$db->prepare_query("INSERT INTO lumonata_members(lmember_id,
  															lfname,	
  															llname, 
  															lemail,
  															lphone,
  															lphone2,
  															lcountry_id,
  															lregion,
  															lcity,
  															laddress,
  															laddress2,
  															laddress3,
  															lpostal_code,
  															lpassword,
  															lpass,
  															lactv_code,
  															laccount_status,
  															llast_login,
  															lcreated_by,
  															lcreated_date,
  															lusername,
  															ldlu,
  															llang_id)
  															VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
  															$id_member,
  															$param["firstname"],
  															$param["lastname"],
  															$param["email"],
  															$param["primary_phone"],
  															$param["alternative_phone"],
  															$param["country"],
  															$param["region"],
  															$param["city"],
  															$param["address"],
  															$param["second_address"],
  															$param["third_address"],
  															$param["postal"],
  															md5($param["pwd1"]),
  															md5($param["pwd1"]),
  															md5($id_member),
  															'2',
  															'',
  															$id_member,
  															date("Y-m-d H:i:s"),
  															$id_member,
  															date("Y-m-d H:i:s"),
  															''
  														);
  	$query_insert=$db->do_query($sql);  	
  	return $id_member;
}
function is_member_activate(){
	global $db;
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if($split_url[0]=='member-activate'){
		return true;
	}else{	
		return false;
	}
}
function member_activate(){
	global $db,$actions;	
	$actions->action['meta_title']['func_name'][0] = 'Activate Account';   
    $actions->action['meta_title']['args'][0] = '';	
	$content = '';
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if (isset($split_url[1]) && ($split_url[1]!='')){
		$code_act = $split_url[1];
		$q = $db->prepare_query("update lumonata_members set laccount_status=%d where lactv_code=%s",1,$code_act);
		$r = $db->do_query($q);
		if ($r){			
			$content .= '<div class="caption star"> Account Activation</div>';
			$content .= '<div class="activation_succes">';
			$content .= '<h4>Your account is activated succesfully.</h4> <br>  ';
			$content .= '<p>You may want do login or start shopping by follow link below.</p> <br>';
			$content .= '<a class="link-after_act loginLink" style="margin-left:0;"> Login </a> | <a class="link-after_act" href="http://'.site_url().'/products/">Start Shopping</a>';
			$content .= '</div>';
			
			$content .= '<style> 
						 .link-after_act{
						 		cursor : pointer;
						 	    background: none repeat scroll 0 0 #AA6FB1;							   
							    color: #FFFFFF;
							    font-family: "GandhiSansRegular",Helvetica,sans-serif;
							    font-size: 16px;
							    margin: 10px 20px;
							    padding: 5px 10px;
							    position: relative;
						 }
						</style>';
			$content .= '<script>
						$(document).ready(function(){					
								$(".loginLink").click(function () {
					        		 $("#mask").css("display","block");        		
					        	     $("#loginForm").css("display","block");
					        	     $("#loginForm").css("z-index","1000");
				            	});		 
			
						 	}) 
						</script>
						';
		}
	}else{
		//redirect aja
		header('location:http://'.site_url().'/');
	}
	
	return $content;
}
function member_update_profile(){
	global $db;
	$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);			
			$vars[$key]=$value;
  		}  	
  	//validasi
	$q = $db->prepare_query("SELECT * from lumonata_members where lmember_id=%s",$_SESSION['memberpanel_clientid']);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	
	if ($vars['pwd0']!="" AND md5($vars['pwd0'])==$d['lpass']){
		if ($vars['pwd1']!=$vars['pwd2']){
  			echo "passwordIsNotMacth";
  		}else{
  			if ($vars['pwd1']=="" OR $vars['pwd2']==""){
  				echo "newPasswordIsEmpty";
  			}else{
	  			$updatePwd = true;
	  			update_profile($vars,$updatePwd);
	  			echo "ok";
  			}
  		}
  	}elseif($vars['pwd0']!="" AND md5($vars['pwd0'])!=$d['lpass']){
  		echo "currentPasswordIsWrong";
  	}else{
  		//update profile only
  		$updatePwd = false;
  		update_profile($vars,$updatePwd);
  		echo "ok";
  	}
}
function update_profile($param,$updatePwd=false){
	global $db;  
	if ($updatePwd==false){
			$sql=$db->prepare_query("UPDATE lumonata_members SET
  															lfname=%s,	
  															llname=%s,	
  															lemail=%s,	
  															lphone=%s,	
  															lphone2=%s,	
  															lcountry_id=%s,	
  															lregion=%s,	
  															lcity=%s,	
  															laddress=%s,	
  															laddress2=%s,	
  															laddress3=%s,	
  															lpostal_code=%s,  																												
  															ldlu=%s
  															WHERE lmember_id=%s
  															",
  															$param['firstname'],
  															$param["lastname"],  															
  															$param["email"],
  															$param["primary_phone"],
  															$param["alternative_phone"],
  															$param["country"],
  															$param["region"],
  															$param["city"],
  															$param["address"],
  															$param["second_address"],
  															$param["third_address"],
  															$param["postal"],  															
  															date("Y-m-d H:i:s"),
  															$_SESSION['memberpanel_clientid']
  														);
	}else{
		$sql=$db->prepare_query("UPDATE lumonata_members SET
  															lfname=%s,	
  															llname=%s,	
  															lemail=%s,	
  															lphone=%s,	
  															lphone2=%s,	
  															lcountry_id=%s,	
  															lregion=%s,	
  															lcity=%s,	
  															laddress=%s,	
  															laddress2=%s,	
  															laddress3=%s,	
  															lpostal_code=%s,  
  															lpass=%s,
  															lpassword=%s,																													
  															ldlu=%s
  															WHERE lmember_id=%s
  															",
  															$param['firstname'],
  															$param["lastname"],  															
  															$param["email"],
  															$param["primary_phone"],
  															$param["alternative_phone"],
  															$param["country"],
  															$param["region"],
  															$param["city"],
  															$param["address"],
  															$param["second_address"],
  															$param["third_address"],
  															$param["postal"],  
  															md5($param["pwd1"]), 
  															md5($param["pwd1"]),  															
  															date("Y-m-d H:i:s"),
  															$_SESSION['memberpanel_clientid']
  															
  														);
	}
  	$query_insert=$db->do_query($sql);  	
  	return $query_insert;
}
function request_reset_password(){
	global $db;
	$email = $_POST['email'];
	$q = $db->prepare_query("SELECT * from lumonata_members where lemail=%s AND laccount_status=%d",$email,1);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if ($n==0){
		echo "Email not found!";
	}else{
		//send email 
		$d = $db->fetch_array($r);
		$subject 	= 'Reset password at '.site_url().'';
		$message 	= 'Dear '.$d['lfname'].' '.$d['llname'].'
					   <br>
					   Please follow this link to reset your password : 
					   <br>
					   <a href="http://'.site_url().'/reset-password&'.md5($d['lmember_id']).'">http://'.site_url().'/reset-password&'.md5($d['lmember_id']).'</a>
					   <br>
					   <br>
					   Best regards,
					   <br>
					   '.site_url().'
					  ';
		$from_email = get_meta_data('email');
  		$from_name  = '';
  		$to_email   = $d['lemail'];
  		$to_name	= $d['lfname'].' '.$d['llname'];
  		$type		= 'html';
  		
  		if (sendMail($subject,$message,$from_email,$from_name,$to_email,$to_name,$type)){
			echo "ok";
		}else{
			echo  "There are something wrong. Please try again later!";
		}		
	}
}
function is_reset_password(){
	global $db;
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if($split_url[0]=='reset-password'){
		return true;
	}else{	
		return false;
	}
}
function reset_password_member(){
	global $db,$actions;	
	$actions->action['meta_title']['func_name'][0] = 'Reset Password';   
    $actions->action['meta_title']['args'][0] = '';	
	$content = '';
	$status  = false;
	$cek_url = cek_url();
	$split_url = explode('&',$cek_url[0]);
	if (isset($split_url[1]) && ($split_url[1]!='')){
		$id = $split_url[1];
		$q  = $db->prepare_query("SELECT * from lumonata_members");
		$r  = $db->do_query($q);

		while ($d=$db->fetch_array($r)){
			if (md5($d['lmember_id']==$id)){
				$status = true;
			}
		}
		
		if ($status==true){
			$content .= '<h1 style="color: #666666;   font-family:GandhiSansBold,Helvetica,sans-serif;font-size: 18px;margin-bottom: 30px;">Reset Password</h1>';
			$content .= form_reset_password($id);
		}
		
	}else{
		//redirect aja
		header ('location:http://'.site_url().'/');
	}
	
	return $content;
}
function form_reset_password($id){
	global $db;
	$f  ='<form id="fResetPassword">';
	$f .='<div class="finput" style="width:90%;">
			<ul>
				<li style="width:180px;"> New Password*</li>
				<li> <input type="password" class="validate[required] input_large" name="pwd1" id="pwd1" value=""></li>
			</ul>
			<ul>
				<li style="width:180px;"> Re-type New Password*</li>
				<li> <input type="password" class="validate[required] input_large" name="pwd2" id="pwd2" value=""></li>
			</ul>
			
			 <div style="color: #333;font-size: 14px;margin: 0;padding: 15px 0;">Please type the code below*</div>
               <div style="display:block;">
	               <img style="height: 26px; vertical-align: middle;" src="http://'.site_url().'/app/chapta/val-image.php">
	               <input name="txt_chapta" id="txt_chapta" class="validate[required]" type="text" style="width:80px;margin-left:10px;vertical-align:middle;">
               </div>
            	<input type="hidden" value="'.$id.'" name="md5id">
               <input type="submit" class="btn_register_submit" id="btn_reset_password" style="float:left;" value="Reset">
               		<div id="responseError" class="responseError">
               		
               		</div>		
		  </div>';
	$f .='</form>';
	
	$f.='<style> 
			.responseError{
			  	display:none;
			    border: 1px solid #EFD054;
			    color: red;
			    float: left;
			    margin-left: 20px;
			    margin-top: 20px;
			    overflow: hidden;
			    padding: 10px;
			    background: none repeat scroll 0 0 #FFF9D5;
			}
		</style>';
	$f.='<script>
			
			function trim(str) {
				var	str = str.replace(/^\s\s*/, ""),
					ws = /\s/,
					i = str.length;
				while (ws.test(str.charAt(--i)));
				return str.slice(0, i + 1);
			}
          	
			
		$(document).ready(function(){
		  $("#fResetPassword input").click(function(){
					$("#responseError").fadeOut();
		  });
		  
		 
		  //submit
		  $(function(){
		  	$("#fResetPassword").validationEngine();		  
			$("#fResetPassword").submit(function(){
				var str = $("#fResetPassword").serialize();
				var thaUrl 	= "http://'.site_url().'/member-panel-ajax";				
				 if($("#fResetPassword").validationEngine("validate") == true){				 	
					jQuery.post(thaUrl,{ 
			    		pKEY:"do_reset_password_member",
			    		lvalue:str
		              },function(data){
		              	if (trim(data)=="passwordNotMatch"){		                	
		                	$("#fResetPassword input[type=password]").val("");
		                	$("#responseError").html("Your password is not match!");
		                	$("#responseError").fadeIn();
		                }else if (trim(data)=="chaptaNotValid"){		                	
		                	$("#fResetPassword input[id=txt_chapta]").val("");
		                	$("#responseError").html("Please type the code correctly!");
		                	$("#responseError").fadeIn();
		                }else if (trim(data)=="ok"){		                	
		                	$("#fResetPassword input").val("");
		                	$("#btn_reset_password").slideUp();
		                	$("#responseError").css("color","#99CC33");
		                	$("#responseError").css("margin-left","0");
		                	$("#responseError").css("width","450px");
		                	$("#responseError").html("Your password is changed succesfully");
		                	$("#responseError").fadeIn();
		                	
		                	var url = "http://'.site_url().'";    
							$(location).attr("href",url);
		                	
		                }else{
		                	$("#responseError").html("There are something wrong. Please try again later!");
		                	$("#responseError").fadeIn();
		                }
		                
		                
					});
					return false;
				 }
			});
		  });
		})
      </script>';
	return $f;
}
function do_reset_password_member(){
	global $db;
	$fields = explode("&",$_POST['lvalue']);
  		$vars=array();
  		foreach($fields as $field){
  			$field_key_value = explode("=",$field);
			$key = urldecode($field_key_value[0]);
			$value = urldecode($field_key_value[1]);			
			$vars[$key]=$value;
  		}  	
  	//validasi
  	$id = $vars['md5id'];
	$q  = $db->prepare_query("SELECT lmember_id from lumonata_members where md5(lmember_id)=%s",$id);
	$r  = $db->do_query($q);
	$n  = $db->num_rows($r);
	$d  = $db->fetch_array($r);
	
	if ($n==0){
		echo "Oopss.... User not found";
	}elseif($vars['pwd1']!=$vars['pwd2']){
		echo "passwordIsNotMatch";	
	}elseif($vars['txt_chapta']!=$_SESSION['rand_code']){
  		echo "chaptaNotValid";  
	}else{
		$q = $db->prepare_query("UPDATE lumonata_members set lpassword=%s,lpass=%s WHERE lmember_id=%s",md5($vars['pwd1']),md5($vars['pwd1']),$d['lmember_id']);
		$r = $db->do_query($q);
		if($r){
			echo "ok";
		}else{
			echo "There are somthing wrong. Please try again later!";
		}
		
	}
}
?>