<?php 
function get_admin_product_setting($type='setting',$thetitle='',$tabs=array()){
        $post_id=0;
        $articletabtitle=explode("|",$thetitle);
        $tabs=array_merge(array('general'=>$articletabtitle[1],'payments'=>'Payments','shipping'=>'Shipping','taxes'=>'Taxes','tax_groups'=>'Tax Groups','manage_country'=>'Country'));
        if(is_contributor() || is_author()){
            //$tabs=array_slice($tabs, 0,1);
            foreach($tabs as $key=>$val){
                if(is_grant_app($key)){
                    $thetabs[$key]=$val;
                }
            }
            $tabs=$thetabs;
        }else{
             $tabs=$tabs;
        }
		        
        $alert='';
        /* Configure the tabs
            $the_tab is the selected tab */
        $tab_keys=array_keys($tabs);
        $tabb='';
        if(empty($_GET['tab']))
               $the_tab=$tab_keys[0];
        else
                $the_tab=$_GET['tab'];
       
        $articles_tabs=set_tabs($tabs,$the_tab);
        add_variable('tab',$articles_tabs);
        
        if($the_tab=='general'){
        	if(is_save_changes($_GET['state'])){
        		$status=false;
        		//Setting Language
        		if(isset($_POST['var_checkout_language'])){
        			$metadata_language=find_meta_data('language','product_setting',0);
        			if($metadata_language<>0){
        				update_meta_data('language',$_POST['var_checkout_language'],'product_setting',0);
        				$status1=true;
        			}else{
        				set_meta_data('language',$_POST['var_checkout_language'],'product_setting',0);
        				$status1=true;
        			}
        		}
        		
        		//Setting Currency
        		if(isset($_POST['var_currency'])){
        			$metadata_currency=find_meta_data('currency','product_setting',0);
        			if($metadata_currency<>0){
        				update_meta_data('currency',$_POST['var_currency'],'product_setting',0);
        				$status2=true;
        			}else{
        				set_meta_data('currency',$_POST['var_currency'],'product_setting',0);
        				$status2=true;
        			}
        		}
        		
        		//Setting Metrix
        		if(isset($_POST['var_unit_system'])){
        			$metadata_unit_system=find_meta_data('unit_system','product_setting',0);
        			if($metadata_unit_system<>0){
        				update_meta_data('unit_system',$_POST['var_unit_system'],'product_setting',0);
        				$status3=true;
        			}else{
        				set_meta_data('unit_system',$_POST['var_unit_system'],'product_setting',0);
        				$status3=true;
        			}
        		}
        		
        	    //Setting MIN Price
        		if(isset($_POST['min_price'])){
        			if(is_numeric($_POST['min_price'])){
        				$min_price=$_POST['min_price'];
        			}else{
        				$min_price=0;
        			}
        			$metadata_currency=find_meta_data('minimum_price','product_setting',0);
        			if($metadata_currency<>0){
        				update_meta_data('minimum_price',$min_price,'product_setting',0);
        				$status4=true;
        			}else{
        				set_meta_data('minimum_price',$min_price,'product_setting',0);
        				$status4=true;
        			}
        		}
        		
				if($status1==true || $status2==true || $status3==true || $status4==true){
					$alert="<div class=\"alert_green_form\">".UPDATE_SUCCESS."</div>";
				}else{
					$alert="<div class=\"alert_red_form\">".UPDATE_FAILED."</div>";
				}
        	}
        }
		if($the_tab=='taxes'){
         global $db;
         
         if(is_save_changes($_GET['state'])){
         	if(isset($_POST['varTaxesOnShipping'])){
         		$varTaxesOnShipping=$_POST['varTaxesOnShipping'];
         	}else{
         		$varTaxesOnShipping=0;
         	}
         	if(isset($_POST['varPricesIncludeTaxes'])){
         		$varPricesIncludeTaxes=$_POST['varPricesIncludeTaxes'];
         	}else{
         		$varPricesIncludeTaxes=0;
         	}
         	
         	$check_record=$db->prepare_query("SELECT * FROM lumonata_tax");
         	$query_check=$db->do_query($check_record);
         	if($db->num_rows($query_check)<>0){
         		$sql_insert=$db->prepare_query("Update lumonata_tax set taxes_on_shipping=%s,price_include_taxes=%s,ldlu=%s",
         										$varTaxesOnShipping,$varPricesIncludeTaxes,date("Y-m-d H:i:s"));
	         	$query_insert=$db->do_query($sql_insert);
	         	
	         	$data=$db->fetch_array($query_check);
	         	$id_tax=$data['ltax_id'];
         	}else{
         		$sql_insert=$db->prepare_query("INSERT INTO lumonata_tax(taxes_on_shipping,
         															price_include_taxes,
         															lcreated_by,
         															lcreated_date,
         															lusername,
         															ldlu)
         															VALUES(%s,%s,%s,%s,%s,%s)",
         															$varTaxesOnShipping,
         															$varPricesIncludeTaxes,
         															$_COOKIE['user_id'],
	  																date("Y-m-d H:i:s"),
	  																$_COOKIE['user_id'],
	  																date("Y-m-d H:i:s")
         															);
         	if(reset_order_id("lumonata_tax")){
         		$query_insert=$db->do_query($sql_insert);
         	}else{
        	 	$query_insert=$db->do_query($sql_insert);
         	}
         	$id_tax=mysql_insert_id();
         }
         
         if($query_insert){
         	$del_tax_detail=$db->prepare_query("DELETE from lumonata_tax_detail");
         	$query_del_detail_tax=$db->do_query($del_tax_detail);
         	$query_country=get_country();
         	while($data_country=$db->fetch_array($query_country)){
         		$id_country="country_".$data_country['lcountry_id'];
         		if(isset($_POST[$id_country])){
         			$sql_tax_group=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
         			$query_tax_group=$db->do_query($sql_tax_group);
         			while($data_tax_group=$db->fetch_array($query_tax_group)){
         				$variable="tax_".$data_country['lcountry_id']."_".$data_tax_group['ltax_groups_ID'];
         				if(isset($_POST[$variable])){
         					$charge=$_POST[$variable];
         				}else{
         					$charge=0;
         				}
         				$sql_insert_detail=$db->prepare_query("INSERT INTO lumonata_tax_detail(	ltax_id,
								         															lcountry_id,
								         															ltax_groups_ID,
								         															ltax_charge)
								         														VALUES(%s,%s,%s,%s)",
									         														$id_tax,
									         														$data_country['lcountry_id'],
									  																$data_tax_group['ltax_groups_ID'],
									  																$charge
									         				  										);
         				 if(reset_order_id("lumonata_tax_detail")){
				         	$query_insert_detail=$db->do_query($sql_insert_detail);
				         }else{
				         	$query_insert_detail=$db->do_query($sql_insert_detail);
				         }
         			}
         		}else{//echo "test";
         		}
         	}
         }
         }else{
         	//nothink to do
         }
      }
        
       
        return get_product_setting($the_tab,$type,$thetitle,$articles_tabs,$alert);
}

//Setting list
function get_product_setting($the_tab,$type='setting',$title,$setting_tabs,$alert=''){
        global $db;
        $list='';
        $option_viewed="";
		if($_COOKIE['user_type']=="contributor"){
            $button="<li>".button("button=save_changes",get_state_url("shoppingcart&sub=product_setting"))."</li>
            <li>".button("button=cancel",get_state_url('shoppingcart&sub=product_setting'))."</li>";
        }else{
			$button="<li>".button("button=save_changes",get_state_url("shoppingcart&sub=product_setting"))."</li>
			<li>".button("button=cancel",get_state_url('shoppingcart&sub=product_setting'))."</li>";
        }
        
        if($the_tab=='general'){
	        $list.="<h1>Product Setting</h1>
	        	<ul class=\"tabs\">".$setting_tabs."</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('shoppingcart&sub=product_setting')."\" method=\"post\" name=\"alist\">
                           <br clear=\"all\" />
                           ".$alert."
                           <input type=\"hidden\" name=\"state\" value=\"pages\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                          $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"list\">
                                ".get_content_product_setting($the_tab,$type='setting',$title,$setting_tabs)."
                			</div>
                        
                        
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                       </form>
                </div>";
        }else if($the_tab=='taxes'){
	        $list.="<h1>Product Setting</h1>
	        	<ul class=\"tabs\">".$setting_tabs."</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('shoppingcart&sub=product_setting&tab=taxes')."\" method=\"post\" name=\"alist\">
                           <br clear=\"all\" />
                           ".$alert."
                           <input type=\"hidden\" name=\"state\" value=\"pages\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                          $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"list\">
                                ".get_content_product_setting($the_tab,$type='setting',$title,$setting_tabs)."
                			</div>
                        
                        
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                       </form>
                </div>";
        }else{
        	$list.="<h1>Product Setting</h1>
	        	<ul class=\"tabs\">".$setting_tabs."</ul>
                <div class=\"tab_container\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('shoppingcart&sub=setting')."\" method=\"post\" name=\"alist\">
                        	<br clear=\"all\" />	
                        	<div class=\"list\">
                                ".get_content_product_setting($the_tab,$type='setting',$title,$setting_tabs)."
                            </div>
                       </form>
                </div>";
        }
            
        add_actions('section_title','ShoppingCart - Setting');
        return $list;
}

function get_content_product_setting($the_tab='',$type='setting',$title,$setting_tabs){
	if($the_tab=='payments'){
		$result=additional_data('Accept Payments','accept_payments',true);
		return $result;	
	}else if($the_tab=='shipping'){
		$result  =additional_data('Shipping Option','shipping_option_backend',false);
		$result .=additional_data('Add a Shipping Rate','shipping_rate',true);
		return $result;	
	}else if($the_tab=='taxes'){
		$result=additional_data('General','get_tax_general',true);
		$result.=additional_data('Add Country To Tax List','get_tax_country_grid',true);
		$result.=get_tax_list();
		return $result;	
	}else if($the_tab=='tax_groups'){
		$result=additional_data('Tax Groups','get_groups_tax',true);
		return $result;	
	}else if($the_tab=='manage_country'){
		$result=additional_data('Manage Country','manage_country',true);
		return $result;	
	}else{
		$result=additional_data('Checkout Language','checkout_language',true);
		$result.=additional_data('Currency and Units','currency_and_units',true);
		return $result;	
	}

}

 function manage_country($alert='',$id=''){
	global $db;
	$js= '<style>
				.status_triger,.status_trigger_as_group{
					cursor: pointer;
					text-decoration: underline;
					
				}
				.statusgroup,.status_as_group,{
					display: none;
				}
			</style>

			<script>
				$(document).ready(function(){
					$(".status_triger").click(function(){
						var id = $(this).attr("id");
						$(".statusgroup"+id).css("display","block");
						$(this).css("display","none");
					})
					$(".statusgroup").change(function(){
						var thaUrl = "http://'.SITE_URL.'/product-setting-ajax";
						var id = $(this).attr("id");
						var val = $(this).val();						
						var sts = $(".statusgroup"+id+" :selected").val();
						
						if(id=="")
						{}						
						else
						{					
							
						 	jQuery.post(thaUrl,{ 
						    		pKEY:"update_status_publish_country",
						    		id:id,
						    		status:sts
					         },function(html){
					           		$(".status_triger"+id).html(html);
									$(".status_triger"+id).css("display","block");
									$(".statusgroup"+id).css("display","none");
					        });
					
							
							
						}
						
						
					})
				})
			</script>
			
			<script>
				$(document).ready(function(){
					$(".status_trigger_as_group").click(function(){
						var id = $(this).attr("id");
						$(".status_as_group"+id).css("display","block");
						$(this).css("display","none");
					})
					$(".status_as_group").change(function(){
						var thaUrl = "http://'.SITE_URL.'/product-setting-ajax";
						var id = $(this).attr("id");
						var val = $(this).val();						
						var sts = $(".status_as_group"+id+" :selected").val();
						
						if(id=="")
						{}						
						else
						{					
							
						 	jQuery.post(thaUrl,{ 
						    		pKEY:"update_status_as_group_country",
						    		id:id,
						    		status:sts
					         },function(html){
					           		$(".status_trigger_as_group"+id).html(html);
									$(".status_trigger_as_group"+id).css("display","block");
									$(".status_as_group"+id).css("display","none");
					        });
					
							
							
						}
						
						
					})
				})
			</script>
			';
	$tr = '';
	$th   = '<tr><th style="width:200px;border-top:0px;border-bottom:0px;text-align:left">Country</th> <th style="width:50px;border-top:0px;border-bottom:0px;text-align:left">Order</th> <th style="width:100px;border-top:0px;border-bottom:0px;text-align:left">Publish</th> <th style="width:100px;border-top:0px;border-bottom:0px;text-align:left">Show as group</th></tr>';
	$q = $db->prepare_query("select * from lumonata_country order by lorder_id, lcountry ASC");
	$r = $db->do_query($q);
	
	while ($d=$db->fetch_array($r)){
		$tr .= '<tr style="background : #fff;">';
		$tr .= '<td style="padding:4px">'.$d['lcountry'].'</td>';
		$tr .= '<td style="padding:4px">'.$d['lorder_id'].'</td>';
		$tr .= '<td style="padding:4px">'.combo_status_publish_country($d['lcountry_id'],$d['lpublish']).'</td>';
		$tr .= '<td style="padding:4px">'.combo_status_show_as_group_country($d['lcountry_id'],$d['lshow_group']).'</td>';
		$tr .= '</tr>';
	}
	$html = ''.$js.'<table cellspacing="1" cellpading="1" style="background-color:#cccccc">'.$th.$tr.'</table>';
 	return $html;
 }
function combo_status_publish_country($id,$publish){
	$combo = '';
	if ($publish==1){
		$combo .= '<span id="'.$id.'" class="status_triger status_triger'.$id.'">Yes</span>
				  <select id="'.$id.'" class="statusgroup statusgroup'.$id.'" style="display: none;" name="statuspublish['.$id.']">
				  		<option value="0">No</option>
						<option value="1" selected="selected">Yes</option>
				  </select>';
	}else{
		$combo .= '<span id="'.$id.'" class="status_triger status_triger'.$id.'" style="color:red;">No</span>
				  <select id="'.$id.'" class="statusgroup statusgroup'.$id.'" style="display: none;" name="statuspublish['.$id.']">
				  		<option value="0" selected="selected">No</option>
						<option value="1" >Yes</option>
				  </select>';
	}
	
	return $combo;
}
function combo_status_show_as_group_country($id,$as_group){
	$combo = '';
	if ($as_group==1){
		$combo .= '<span id="'.$id.'" class="status_trigger_as_group status_trigger_as_group'.$id.'">Yes</span>
				  <select id="'.$id.'" class="status_as_group status_as_group'.$id.'" style="display: none;" name="statusasGroup['.$id.']">
				  		<option value="0">No</option>
						<option value="1" selected="selected">Yes</option>
				  </select>';
	}else{
		$combo .= '<span id="'.$id.'" class="status_trigger_as_group status_trigger_as_group'.$id.'" style="color:#ccc;">No</span>
				  <select id="'.$id.'" class="status_as_group status_as_group'.$id.'" style="display: none;" name="statusasGroup['.$id.']">
				  		<option value="0"  selected="selected">No</option>
						<option value="1" >Yes</option>
				  </select>';
	}
	
	return $combo;
}
function checkout_language(){
	$result='<fieldset><p>
				<strong>Checkout language</strong><p>Can\'t find the language your are looking for? <a href="#">Send us an email</a> and we\'ll add it for you.
				</p>
				<select id="var_checkout_language" class="valid" name="var_checkout_language">
				'.select_language().'
				</select>
			</p></fieldset>';
	return $result;
}

function select_language(){
	global $db;
	$result='';
	$data_language=get_meta_data('language','product_setting');
	$sql=$db->prepare_query("SELECT * from lumonata_languages order by llanguage_id ASC");
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){
		if($data['llanguage_id']==$data_language){
			$selected='selected="selected"';
		}else{
			$selected='';
		}
		$result.='<option value="'.$data['llanguage_id'].'" '.$selected.'>'.$data['llanguage_title'].'</option>';
	}
	return $result;
}

function currency_and_units(){
	$data_unit_system=get_meta_data('unit_system','product_setting');
	if($data_unit_system=='metric'){
		$selected1='selected="selected"';
		$selected2='';
	}else if($data_unit_system=='imperial'){
		$selected2='selected="selected"';
		$selected1='';
	}else{
		$selected2='';
		$selected1='';
	}
	
	$minimum_price = number_format(get_meta_data('minimum_price','product_setting'),2,'.','');
	$result='<fieldset><p>
			<label class="" for="var_currency"><strong>Currency</strong></label>
			</p>
			<p>
				<select id="var_currency" name="var_currency">
				<option value="">Select your currency..</option>
				'.select_currency().'
				</select>
			</p>
			<label for="var_unit_system"><strong>Unit system</strong></label></p>
			<p>
				<select id="var_unit_system" name="var_unit_system">
					<option value="">Select a unit system...</option>
					<option value="metric" '.$selected1.'>Kilograms, centimeters (metric)</option>
					<option value="imperial" '.$selected2.'>Pounds, inches (imperial)</option>
				</select>
			</p>
			<p><label><strong>Minimum Order</strong></label></p>
			<input type="text" class="small_textbox" name="min_price" value="'.$minimum_price.'"> '.get_symbol_currency().'
			</fieldset>';
	return $result;
}

function select_currency(){
	global $db;
	$result='';
	$data_currency=get_meta_data('currency','product_setting');
	$sql=$db->prepare_query("SELECT * from lumonata_currency order by lcurrency_id ASC");
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){
		if($data['lcurrency_id']==$data_currency){
			$selected='selected="selected"';
		}else{
			$selected='';
		}
		$result.='<option value="'.$data['lcurrency_id'].'" '.$selected.'>'.$data['lcurrency'].'('.$data['lcode'].')</option>';
	}
	return $result;
}

function accept_payments(){
		$bt='';
		$pp='';
		$cod='';
		$cq='';
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');
			
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
					if($val=='payments_bank_transfer'){
						$bt='style="display:none"';
					}
					if($val=='payments_paypal_standard'){
						$pp='style="display:none"';
			      	}
					if($val=='payments_cashondelivery'){
						$cod='style="display:none"';
					} 
					if($val=='payments_cheque'){
						$cq='style="display:none"';
					}
					  
		       	 }
		      }
		}
	$result='<fieldset>
			<div id="payment_line">
			<p><label class="" for="payment_method"><strong>Select a payment method to accept</strong></label><p>
			<select id="payment_method" name="payment_method">
				<option value="-">Select one</option>
				<option value="payment_offline_banktransfer" '.$bt.'>Bank transfer</option>
				<option value="payment_integration_paypalwebsitepaymentsstandard" '.$pp.'>PayPal website payments standard</option>
				<option value="payment_offline_cashondelivery" '.$cod.'>Cash on delivery</option>
				<option value="payment_offline_cheque" '.$cq.'>Cheque</option>
			</select></p>
			<p><input type="button" name="save" value="Active this method" class="button" id="button_payment"></p></div>
			</fieldset>	
			<p><h3>Active Payment Methods</h3></p>
			<p>
			<table cellspacing="1" style="background-color:#cccccc" cellpading="1" >
				<tr id="table_payment">
					<th style="width:200px;border-top:0px;border-bottom:0px">Payment Method</th>
					<th style="width:400px;border-top:0px;border-bottom:0px">Status</th>
					<th style="width:150px;border-top:0px;border-bottom:0px">Configure</th>
					<th style="width:50px;border-top:0px;border-bottom:0px"></th>
				</tr>
				'.get_payment_method().'
			</table>
			</p>
			
			<script>
			$(document).ready(function() {
				$("#button_payment").click(function(){
					var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
					var payment_selected=$("#payment_method option:selected").val();
					if(payment_selected=="-"){
					}else{
						 jQuery.post(thaUrl,{ 
						    		pKEY:"payments",
						    		lvalue:payment_selected
					           },function(data){

					           		$("#payment_line").html(data);
					           });
					}
				});
			});
			</script>
			';
		
	return $result;
}

function shipping_rate($shipping_id='',$alert=''){
	$currency=get_meta_data('currency','product_setting');
	$language=get_meta_data('language','product_setting');
	if($shipping_id<>''){
		global  $db;
		$query_shipping=get_shipping_edit($shipping_id);
		$data_shipping=$db->fetch_array($query_shipping);
		$result='<fieldset>
				<p>
				<span>
					<label for="parameterFrom"><strong> From ( and including )</strong></label>
					<input id="parameterFrom" class="small_textbox" type="text" value="'.$data_shipping['lformula'].'" name="parameterFrom">
					<span id="unitSymbol" class="unitSymbol">'.get_symbol_currency().'</span>
				</span>
				<span style="padding-left:10px">
					<label style="display:inline;" for="parameterCharge"><strong>Charge</strong></label>
					<input id="parameterCharge" class="small_textbox" type="text" value="'.$data_shipping['lprice'].'" name="parameterCharge">
					<span class="unitSymbol">'.get_symbol_currency().'</span> Shipping Costs 
					<label style="display:inline;"><strong>Shipping Tax</strong></label>
					<input id="shipping_tax" class="small_textbox" type="text" value="'.$data_shipping['lshipping_tax'].'" name="shipping_tax"><strong>%</strong>
				</span>
				</p>
				<p>
					<div class="field">
						<input id="btnEditShippingRate" type="button" value="Save shipping rate" class="button">
						<input class="button" type="button" value="cancel" onclick="location=\''.get_state_url('shoppingcart&sub=product_setting&tab=shipping').'\'">
					</div>	
				</p>
				</p></fieldset>
				<script>
					 $("#btnEditShippingRate").click(function(){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
				    	var parameter = $("#parameterFrom").val();
				    	var charge = $("#parameterCharge").val();
				    	var shipping_id = "'.$data_shipping['lshipping_id'].'";
				    	var the_shipping_tax = $("#shipping_tax").val();
				    	jQuery.post(thaUrl,{ 
				    		pKEY:"edit_shipping_rate",
				    		id:shipping_id,
				    		parameter_value:parameter,
				    		charge_value:charge,
				    		shipped_tax:the_shipping_tax
			           },function(data){
			           		$("#shipping_rate_details_0").html(data);
			           });
					});
				</script>';
	}else{
		$result='<fieldset>
				<label for="shipping_to_country"><strong>Shipping to country</strong></label>
				<p>
					<select id="cbocountry" class="largefont" name="cbocountry">
					'.select_country().'
					</select>
				</p>
				
				<label for="shipping_to_country"><strong>Shipping option</strong></label>
				<p>
				<select name="shipping_option"  id="shipping_option" class="largefont">'.shipping_option().'</select>
				</p>
				
				<p><label class="" style="display:inline;" for="shippingFormula"><strong>Shipping is based on</strong></label>
					<select id="shippingFormula" name="shippingFormula" style="margin-left: 4px;">
						<option value="price">Price</option>
						<option value="weight">Total Weight</option>
						<option value="amount">Number Of Items</option>
					</select>
				</p>
				<p>
				<span>
					<label for="parameterFrom"><strong> From ( and including )</strong></label>
					<input id="parameterFrom" class="small_textbox" type="text" value="" name="parameterFrom">
					<span id="unitSymbol" class="unitSymbol">'.get_symbol_currency().'</span>
				</span>
				<span style="padding-left:10px">
					<label style="display:inline;" for="parameterCharge"><strong>Charge</strong></label>
					<input id="parameterCharge" class="small_textbox" type="text" value="" name="parameterCharge">
					<span class="unitSymbol">'.get_symbol_currency().'</span> Shipping Costs &nbsp;
					<label style="display:inline;"><strong>Shipping Tax</strong></label>
					<input id="shipping_tax" class="small_textbox" type="text" value="0.00" name="shipping_tax"><strong>%</strong>
				</span>
				</p>
				<p>
					<div class="field">
						<input id="btnAddShippingRate" type="button" value="Add shipping rate" class="button">
					</div>	
				</p>
				</p></fieldset><p><h3>Active Shipping Rules</h3></p><span id="shipping_rate_list"><p>'.get_shipping_rate($alert).'</p></span>
				<script>
				$(document).ready(function() {
				
				$("#shippingFormula").change(function(){ 
					var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
			    	var value = $("#shippingFormula option:selected").val();
			    	jQuery.post(thaUrl,{ 
			    		pKEY:"shipping_based_on",
			    		lvalue:value
		           },function(data){
		           		$("#unitSymbol").html(data);
		           });
			    });
				
				status1="false";
				status2="false";
				$("#parameterFrom").blur(function(){
					if($(this).val()<0){
						$(this).css("border","1px solid #CD0A0A");
						status1="false";
					}else{
						$(this).css("border","1px solid #BBBBBB");
						status1="true";
					}
				})
				
				$("#parameterCharge").blur(function(){
					if($(this).val()<0){
						$(this).css("border","1px solid #CD0A0A");
						status2="false";
					}else{
						$(this).css("border","1px solid #BBBBBB");
						status2="true";
					}
				})

				    $("#btnAddShippingRate").click(function(){
				    if(status1=="true" && status2=="true"){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
				    	var value = $("#shippingFormula option:selected").val();
				    	var parameter = $("#parameterFrom").val();
				    	var charge = $("#parameterCharge").val();
				    	var the_shipping_tax = $("#shipping_tax").val();
				    	var country = $("#cbocountry option:selected").val();
				    	var shipping_option = $("#shipping_option option:selected").val();
				    	var currency = "'.$currency.'";
				    	var language = "'.$language.'";
				    	var symbol = $("#unitSymbol").text();
				    	
				    	jQuery.post(thaUrl,{ 
				    		pKEY:"shipping_rate",
				    		type_value:value,
				    		parameter_value:parameter,
				    		charge_value:charge,
				    		currency_value:currency,
				    		language_value:language,
				    		country_value:country,
				    		shipping_option_value:shipping_option,
				    		unit_symbol:symbol,
				    		shipped_tax:the_shipping_tax
			           },function(data){
			           		$("#shipping_rate_list").html(data);
			           });
			        }else{
						if($("#parameterCharge").val()<1){
							$("#parameterCharge").css("border","1px solid #CD0A0A");
							status1=true;
						}else{
							$("#parameterCharge").css("border","1px solid #BBBBBB");
							status2=false;
						}
						if($("#parameterFrom").val()<1){
							$("#parameterFrom").css("border","1px solid #CD0A0A");
							status2=true;
						}else{
							$("#parameterFrom").css("border","1px solid #BBBBBB");
							status2=false;
						}
					}   
			           
					});
				});
				</script>
				';
	}
	return $result;

}
function shipping_option_backend($shipping_option_id='',$alert=''){	
	$currency = '';
	$language=get_meta_data('language','product_setting');
	if($shipping_option_id<>''){
		global  $db;
		$q = $db->prepare_query("select * from lumonata_shipping_option where lshipping_option_id=%d",$shipping_option_id);
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);
		$result='<fieldset>
				<label for="shipping_name"><strong>Shipping Name</strong></label>
				<p>					
					<input style="width:360px;" type="text" id="shipping_name" name="shipping_name" value="'.$d['loption'].'">
				</p>
				
				<label for="shipping_desc"><strong>Description</strong></label>
				<p>
					<input style="width:360px;" type="text" id="shipping_desc" name="shipping_desc" value="'.$d['ldesc'].'">
				</p>
				
				
					<div class="field">
						<input id="btnEditShippingOption" type="button" value="Save shipping option" class="button">
						<input class="button" type="button" value="cancel" onclick="location=\''.get_state_url('shoppingcart&sub=product_setting&tab=shipping').'\'">
					</div>	
				</p>
				</p></fieldset>
				<script>
					 $("#btnEditShippingOption").click(function(){					 	
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";				    
				    	var shipping_option_id = "'.$d['lshipping_option_id'].'";
				    	var shipping_name =  $("#shipping_name").val();
				    	var shipping_desc =  $("#shipping_desc").val();
				    	
				    	jQuery.post(thaUrl,{ 
				    		pKEY:"save_edit_shipping_option",
				    		id:shipping_option_id,
				    		shipping_name:shipping_name,
				    		shipping_desc:shipping_desc
			           },function(data){
			           		var arr_data=data.split("[BREAK]");
			           		$("#shipping_option_backend_details_0").html(arr_data[0]);
			           		$("#shipping_option").html(arr_data[1]);
			           });
					});
				</script>';
	}else{
		$result='<fieldset>
				<label for="shipping_name"><strong>Shipping Name</strong></label>
				<p>
					<input style="width:360px;" type="text" id="shipping_name" name="shipping_name">
				</p>
				
				<label for="shipping_desc"><strong>Description</strong></label>
				<p>
					<input style="width:360px;" type="text" id="shipping_desc" name="shipping_desc">
				</p>
				
				<p>
					<div class="field">
						<input id="btnAddShippingOption" type="button" value="Add shipping option" class="button">
					</div>	
				</p>
				
				</p></fieldset><p><h3>Active Shipping Option</h3></p><span id="shipping_option_list"><p>'.get_table_shipping_option($alert).'</p></span>
				
				<script>
				$(document).ready(function() {
					function trim(str) {
						var	str = str.replace(/^\s\s*/, ""),
							ws = /\s/,
							i = str.length;
						while (ws.test(str.charAt(--i)));
						return str.slice(0, i + 1);
					}
					
				    $("#btnAddShippingOption").click(function(){
				    	var shipping_name =  $("#shipping_name").val();
				    	var shipping_desc =  $("#shipping_desc").val();
				    
				    if(trim(shipping_name)==""){
						$("#shipping_name").css("border","1px solid #CD0A0A");				   
				    }else{
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";			    	
				    	jQuery.post(thaUrl,{ 
				    		pKEY:"insert_shipping_option",
				    		shipping_name : shipping_name,
				    		shipping_desc : shipping_desc
			           },function(data){
			           		var arr_data=data.split("&");
			           		$("#shipping_option_list").html(arr_data[0]);
			           		$("#shipping_option").html(arr_data[1]);
			           });
			        }
			           
					});
				});
				</script>
				';
	}
	return $result;

}
function get_table_shipping_option($alert=''){
 	global $db;
 	if($alert=="1"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">At list one shipping is required.</div>";
 	}else if($alert=="2"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">Cannot mix shipping types in same country.</div>";
 	}else if($alert=="3"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">The shipping option your tried add already exists.</div>";
 	}else{
 		$status="";
 	}
 	$result = $status;
 	$result.= '<p><table cellspacing="1" style="background-color:#cccccc" cellpading="1" style="width:60%;" >';
 	$result.= '<tr>
 					<th style="width:300px;border-top:0px;border-bottom:0px;text-align:left">Shipping name</th> 
 					<th style="width:500px;border-top:0px;border-bottom:0px;text-align:left" colspan="2">Description</th>
 					
 			   </tr>';
 	$q = $db->prepare_query("SELECT * from lumonata_shipping_option order BY lorder ASC");
 	$r = $db->do_query($q);
 	while ($d=$db->fetch_array($r)){
 		$result.='<tr>
 				  	<td style="background-color:#ffffff;padding: 4px;">'.$d['loption'].'</td>
 				  	<td style="background-color:#ffffff;padding: 4px;width:500px;">'.$d['ldesc'].'</td>
 				  	<td style="background-color:#ffffff;padding: 4px;text-align:center"> 
 				  		<a class="edit_shipping_option_'.$d['lshipping_option_id'].'" rel="'.$d['lshipping_option_id'].'" style="cursor:pointer"> edit </a>
 				  		| 
 				  		<a id="del_shipping_option_'.$d['lshipping_option_id'].'" class="del_shipping_option" rel="'.$d['lshipping_option_id'].'" style="cursor:pointer">delete</a>
 				  	</td>
 				  	
 		  		  </tr>';
 		$result.=delete_shipping_option($d['lshipping_option_id']);
		
		$result.='<script>
		 			$(document).ready(function(){
		 				$(".edit_shipping_option_'.$d['lshipping_option_id'].'").click(function(){		 					
							var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
							var shipping_option_id=$(this).attr("rel");
							 jQuery.post(thaUrl,{ 
							    		pKEY:"edit_shipping_option",
							    		lvalue:shipping_option_id
						           },function(data){
						           		$("#shipping_option_backend_details_0").html(data);
						      });
						});
		 			
		 			
		 				$("#del_shipping_option_'.$d['lshipping_option_id'].'").click(function(){
							$("#delete_shipping_option_form_'.$d['lshipping_option_id'].'").dialog("open");
						});
						del_shipping_option_'.$d['lshipping_option_id'].'();
					});
					
					function del_shipping_option_'.$d['lshipping_option_id'].'(){
						 $("#delete_shipping_option_form_'.$d['lshipping_option_id'].'").dialog({
						      bgiframe: true,
						      autoOpen: false,
						      resizable: false,
						      height:170,
						      width:330,
						      modal: true,							      
						      buttons: {
						        "Yes, delete shipping option": function() {
						          var thaUrl 	= "http://'.SITE_URL.'/product-setting-ajax";
						          var shipping_option_id = "'.$d['lshipping_option_id'].'";
							    	jQuery.post(thaUrl,{ 
							    		pKEY:"del_shipping_option",
							    		shipping_option_id:shipping_option_id
						              },function(data){						              		
											$("#shipping_option_list").html(data);		
											$("#shipping_option option[value=\''.$d['lshipping_option_id'].'\']").remove();									
							        });
						           $(this).dialog("close");
						           
						          },Cancel: function() {
						            $(this).dialog("close");
						          }
						        }
						   })
					}
					</script>';
 	}
 	
 	
 	$result .= '</table></p>';
 	return $result;
 }
 
function select_country($country_id=''){
	global $db;
	$result='';
	if($country_id==''){
		$where = "";
	}else{
		$where = "where lcountry_id<>'".$country_id."'";
	}
	$sql=$db->prepare_query("SELECT * from lumonata_country $where order by lcountry_id ASC");
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){
		$result.='<option value="'.$data['lcountry_id'].'">'.$data['lcountry'].'</option>';
	}
	return $result;
}

 add_actions('product-setting-ajax_page', 'product_setting');
 
  function product_setting(){
  	global $db;
	add_actions('is_use_ajax', true);
	
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='update_status_publish_country')){
		if(isset($_POST['id']) && (isset($_POST['status']))){
			$id 		= $_POST['id'];
			$publish 	= $_POST['status'];
			
			$q = $db->prepare_query("UPDATE lumonata_country set lpublish=%s where lcountry_id=%s",$publish,$id);
			$r = $db->do_query($q);
			if ($r){
				if ($publish==1){
					echo 'Yes';
				}else{
					echo 'No';
				}
			}
		}
	}
	
  if(isset($_POST['pKEY']) && ($_POST['pKEY']=='update_status_as_group_country')){
		if(isset($_POST['id']) && (isset($_POST['status']))){
			$id 		= $_POST['id'];
			$publish 	= $_POST['status'];
			
			$q = $db->prepare_query("UPDATE lumonata_country set lshow_group=%s where lcountry_id=%s",$publish,$id);
			$r = $db->do_query($q);
			if ($r){
				if ($publish==1){
					echo 'Yes';
				}else{
					echo 'No';
				}
			}
		}
	}
	
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='payments')){
		if(isset($_POST['lvalue']) && $_POST['lvalue']=='payment_offline_banktransfer'){
			echo form_payment_offline_banktransfer();
		}
		
		if(isset($_POST['lvalue']) && $_POST['lvalue']=='payment_integration_paypalwebsitepaymentsstandard'){
			echo form_payment_paypal_standard();
		}
		
		if(isset($_POST['lvalue']) && $_POST['lvalue']=='payment_offline_cashondelivery'){
			echo form_payment_offline_cashondelivery();
		}
		
		if(isset($_POST['lvalue']) && $_POST['lvalue']=='payment_offline_cheque'){
			echo form_payment_offline_cheque();
		}
		
		
	}
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='payments_bank_transfer')){
		$lvalue_name[0]=$_POST['lvalue_name'];
		$lvalue_name[1]=$_POST['lvalue_text'];
		$lvalue_name[2]=$_POST['lvalue_mode'];
		
		$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		$status=false;
		/*if($lvalue_name[2]==0){
			$config="Test/Sandbox";
		}else{
			$config="Active";
		}*/
		
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');
			
			$count_p=0;
			
			$tmp_array_parent=array();
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
				      
			      	 if($val == 'payments_bank_transfer'){
			      	 	$tmp_array_parent['parent_payment'][$count_p]='payments_bank_transfer';
			      	 	$status=true;
			      	 }else{
					      $tmp_array_parent['parent_payment'][$count_p]=$val;
					     // echo "parent".$key.":".$val;
			      	 }
				     $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

					      	if($val == 'payments_bank_transfer'){
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$lvalue_name[$count_c];
					      	}else{
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      		//echo "child ".$subkey.":".$subval."<br>";
					      	}
					      	$count_c++;
				      	
				      }
				    $count_p++;
		       	 }
		      }
		      
		      if($status==false){
				  $tmp_array_parent['parent_payment'][$count_p]='payments_bank_transfer';
				  $tmp_array_parent['child_payment'][$count_p][$value[0]]=$lvalue_name[0];
			      $tmp_array_parent['child_payment'][$count_p][$value[1]]=$lvalue_name[1];
			      $tmp_array_parent['child_payment'][$count_p][$value[2]]=$lvalue_name[2];
		      }
		      
		      $val_payments_encode=json_encode($tmp_array_parent);
		      update_meta_data('payments',$val_payments_encode,'product_setting',0);
		}else{
			$var_array_parent_payment=array();
				$var_array_parent_payment['parent_payment'][0]='payments_bank_transfer';
				$var_array_parent_payment['child_payment'][0]['name']=$lvalue_name[0];
				$var_array_parent_payment['child_payment'][0]['text']=$lvalue_name[1];
				$var_array_parent_payment['child_payment'][0]['mode']=$lvalue_name[2];
				$val_payments_encode=json_encode($var_array_parent_payment);
	
				set_meta_data('payments',$val_payments_encode,'product_setting',0);
		}
		echo accept_payments();
	}
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='edit_payment')){
		if($_POST['lvalue']=='payments_bank_transfer'){
			echo form_payment_offline_banktransfer($_POST['lvalue']);
		}else if($_POST['lvalue']=='payments_paypal_standard'){
			echo form_payment_paypal_standard($_POST['lvalue']);
		}else if($_POST['lvalue']=='payments_cashondelivery'){
			echo form_payment_offline_cashondelivery($_POST['lvalue']);
		}else if($_POST['lvalue']=='payments_cheque'){
			echo form_payment_offline_cheque($_POST['lvalue']);
		}
		
	}
	
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='del_payment')){
		$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		
		//echo $_POST['lvalue'];
		
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');

			$count_p=0;
			
			$count_select=0;
			$select_array=array();
			$tmp_array_parent=array();
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
			      	 if($val==$_POST['lvalue']){
			      	 	//nothink to do
			      	 	  $count_c=0;
			      	 }else{
			      	 	  $count_c=0;
			      	 	  $select_array['parent_payment'][$count_select]=$val;
					      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
						      		//$array_payments['child_payment'][$count_select][$value[$count_c]]=$tmp_array_parent['child_payment'][$count_p][$value[$count_c]];
						      		//echo "child ".$subkey.":".$subval."<br>";
						      		 $select_array['child_payment'][$count_select][$value[$count_c]]=$subval;
						      	$count_c++;
					      	
					      }
					      $count_select++;
			      	 }
				    $count_p++;
		       	 }
		      }
		      
		      if($count_select==0){
		      	delete_meta_data('payments','product_setting',0);
		      }else{
		      	$val_payments_encode=json_encode($select_array);
		     	update_meta_data('payments',$val_payments_encode,'product_setting',0);
		      }
		}
		echo accept_payments();
	}
	
  if(isset($_POST['pKEY']) && ($_POST['pKEY']=='payments_paypal_standard')){
		$lvalue_name[0]=$_POST['lvalue_name'];
		$lvalue_name[1]=$_POST['lvalue_text'];
		$lvalue_name[2]=$_POST['lvalue_mode'];
		
		$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		
		
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');
			
			$count_p=0;
			$status=false;
			$tmp_array_parent=array();
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
						
			      	if($val == 'payments_paypal_standard'){
			      	 	$tmp_array_parent['parent_payment'][$count_p]='payments_paypal_standard';
			      	 	$status=true;
			      	 }else{
					      $tmp_array_parent['parent_payment'][$count_p]=$val;
			      	 }
					 
				     $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

				      		if($val == 'payments_paypal_standard'){
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$lvalue_name[$count_c];
					      	}else{
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      		//echo "child ".$subkey.":".$subval."<br>";
					      	}
					      	$count_c++;
				      	
				      }
				    $count_p++;
		       	 }
		      }
		      if($status==false){
			      $tmp_array_parent['parent_payment'][$count_p]='payments_paypal_standard';
				  $tmp_array_parent['child_payment'][$count_p][$value[0]]=$lvalue_name[0];
			      $tmp_array_parent['child_payment'][$count_p][$value[1]]=$lvalue_name[1];
			      $tmp_array_parent['child_payment'][$count_p][$value[2]]=$lvalue_name[2];
			      	      
		      }
		    	$val_payments_encode=json_encode($tmp_array_parent);	
		     	update_meta_data('payments',$val_payments_encode,'product_setting',0);
		}else{
				$var_array_parent_payment=array();
				$var_array_parent_payment['parent_payment'][0]='payments_paypal_standard';
				$var_array_parent_payment['child_payment'][0]['name']=$lvalue_name[0];
				$var_array_parent_payment['child_payment'][0]['text']=$lvalue_name[1];
				$var_array_parent_payment['child_payment'][0]['mode']=$lvalue_name[2];
				$val_payments_encode=json_encode($var_array_parent_payment);
	
				//echo $val_payments_encode;
				set_meta_data('payments',$val_payments_encode,'product_setting',0);
		}
		echo accept_payments();
  	}
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='payments_cashondelivery')){
		$lvalue_name[0]=$_POST['lvalue_name'];
		$lvalue_name[1]=$_POST['lvalue_text'];
		$lvalue_name[2]=$_POST['lvalue_mode'];
		
		$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		$status=false;
		
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');
			
			$count_p=0;
			
			$tmp_array_parent=array();
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
				      
			      	 if($val == 'payments_cashondelivery'){
			      	 	$tmp_array_parent['parent_payment'][$count_p]='payments_cashondelivery';
			      	 	$status=true;
			      	 }else{
					      $tmp_array_parent['parent_payment'][$count_p]=$val;
					     // echo "parent".$key.":".$val;
			      	 }
				     $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

					      	if($val == 'payments_cashondelivery'){
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$lvalue_name[$count_c];
					      	}else{
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      		//echo "child ".$subkey.":".$subval."<br>";
					      	}
					      	$count_c++;
				      	
				      }
				    $count_p++;
		       	 }
		      }
		      
		      if($status==false){
				  $tmp_array_parent['parent_payment'][$count_p]='payments_cashondelivery';
				  $tmp_array_parent['child_payment'][$count_p][$value[0]]=$lvalue_name[0];
			      $tmp_array_parent['child_payment'][$count_p][$value[1]]=$lvalue_name[1];
			      $tmp_array_parent['child_payment'][$count_p][$value[2]]=$lvalue_name[2];
		      }
		      
		      $val_payments_encode=json_encode($tmp_array_parent);
		      update_meta_data('payments',$val_payments_encode,'product_setting',0);
		}else{
			$var_array_parent_payment=array();
				$var_array_parent_payment['parent_payment'][0]='payments_cashondelivery';
				$var_array_parent_payment['child_payment'][0]['name']=$lvalue_name[0];
				$var_array_parent_payment['child_payment'][0]['text']=$lvalue_name[1];
				$var_array_parent_payment['child_payment'][0]['mode']=$lvalue_name[2];
				$val_payments_encode=json_encode($var_array_parent_payment);

				set_meta_data('payments',$val_payments_encode,'product_setting',0);
		}
		echo accept_payments();
  	}
  	
  if(isset($_POST['pKEY']) && ($_POST['pKEY']=='payments_cheque')){
		$lvalue_name[0]=$_POST['lvalue_name'];
		$lvalue_name[1]=$_POST['lvalue_text'];
		$lvalue_name[2]=$_POST['lvalue_mode'];
		
		$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
		$status=false;
		
		$metadata_payments=find_meta_data('payments','product_setting',0);
		if($metadata_payments<>0){
			$data_payments=get_meta_data('payments','product_setting');
			
			$count_p=0;
			
			$tmp_array_parent=array();
			$array_payments = json_decode( $data_payments,true);
		      if(is_array($array_payments)){
			      foreach($array_payments['parent_payment'] as $key=>$val){
				      
			      	 if($val == 'payments_cheque'){
			      	 	$tmp_array_parent['parent_payment'][$count_p]='payments_cheque';
			      	 	$status=true;
			      	 }else{
					      $tmp_array_parent['parent_payment'][$count_p]=$val;
					     // echo "parent".$key.":".$val;
			      	 }
				     $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

					      	if($val == 'payments_cheque'){
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$lvalue_name[$count_c];
					      	}else{
					      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      		//echo "child ".$subkey.":".$subval."<br>";
					      	}
					      	$count_c++;
				      	
				      }
				    $count_p++;
		       	 }
		      }
		      
		      if($status==false){
				  $tmp_array_parent['parent_payment'][$count_p]='payments_cheque';
				  $tmp_array_parent['child_payment'][$count_p][$value[0]]=$lvalue_name[0];
			      $tmp_array_parent['child_payment'][$count_p][$value[1]]=$lvalue_name[1];
			      $tmp_array_parent['child_payment'][$count_p][$value[2]]=$lvalue_name[2];
		      }
		      
		      $val_payments_encode=json_encode($tmp_array_parent);
		      update_meta_data('payments',$val_payments_encode,'product_setting',0);
		}else{
			$var_array_parent_payment=array();
				$var_array_parent_payment['parent_payment'][0]='payments_cheque';
				$var_array_parent_payment['child_payment'][0]['name']=$lvalue_name[0];
				$var_array_parent_payment['child_payment'][0]['text']=$lvalue_name[1];
				$var_array_parent_payment['child_payment'][0]['mode']=$lvalue_name[2];
				$val_payments_encode=json_encode($var_array_parent_payment);

				set_meta_data('payments',$val_payments_encode,'product_setting',0);
		}
		echo accept_payments();
  	}
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='shipping_based_on')){
		if($_POST['lvalue']=='weight'){
			$unit=get_unit_system();
			if($unit=='metric'){
				echo "Kilograms";
			}else{
				echo "Pounds";
			}
		}else if($_POST['lvalue']=='amount'){
			echo "Piece(s)";
		}else{
			echo get_symbol_currency();
		}
  	}
  	
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='shipping_rate')){
  		global $db;
 		/* ///ORI
  		$type_value = $_POST['type_value'];
  		$parameter_value = $_POST['parameter_value'];
  		$charge_value = $_POST['charge_value'];
  		$currency_value = $_POST['currency_value'];
  		$language_value = $_POST['language_value'];
  		$country_value = $_POST['country_value']; 
  		$unit_symbol = $_POST['unit_symbol'];
  		$shipped_tax = $_POST['shipped_tax'];
  		
  		$check_shipping=$db->prepare_query("SELECT * from lumonata_shipping where lcountry_id=%d order by lshipping_id DESC limit 1",$country_value);
 		$query_check_shipping=$db->do_query($check_shipping);
 		
 		if($db->num_rows($query_check_shipping)<>0){
 			$data_check_shipping=$db->fetch_array($query_check_shipping);
 			if($data_check_shipping['lshipping_type']==$type_value){
 				if($data_check_shipping['lformula']<$parameter_value){
		 				$sql=$db->prepare_query("INSERT INTO lumonata_shipping( lcountry_id,
			  																	lshipping_type,
			  																	lcurrency_id,
			  																	lformula,
			  																	unit_symbol,
			  																	lprice,
			  																	lshipping_tax,
			  																	lcreated_by,
			  																	lcreated_date,
			  																	lusername,
			  																	ldlu,
			  																	llang_id)
			  									VALUES(%d,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
			  																	$country_value,
			  																	$type_value,
			  																	$currency_value,
			  																	$parameter_value,
			  																	$unit_symbol,
			  																	$charge_value,
			  																	$shipped_tax,
			  																	$_COOKIE['user_id'],
			  																	date("Y-m-d H:i:s"),
			  																	$_COOKIE['user_id'],
			  																	date("Y-m-d H:i:s"),
			  																	$language_value
			  																	);
			  		
			  		if(reset_order_id("lumonata_shipping")){
			  			$db->do_query($sql);
			  		}else{
			  			$db->do_query($sql);
			  		}
			  		echo get_shipping_rate();
 				}else{
 					echo get_shipping_rate('3');
 				}
 			}else{
 				echo get_shipping_rate('2');
 			}
 		}else{
	  		$sql=$db->prepare_query("INSERT INTO lumonata_shipping( lcountry_id,
	  																	lshipping_type,
	  																	lcurrency_id,
	  																	lformula,
	  																	unit_symbol,
	  																	lprice,
	  																	lshipping_tax,
	  																	lcreated_by,
	  																	lcreated_date,
	  																	lusername,
	  																	ldlu,
	  																	llang_id)
	  									VALUES(%d,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
	  																	$country_value,
	  																	$type_value,
	  																	$currency_value,
	  																	$parameter_value,
	  																	$unit_symbol,
	  																	$charge_value,
	  																	$shipped_tax,
	  																	$_COOKIE['user_id'],
	  																	date("Y-m-d H:i:s"),
	  																	$_COOKIE['user_id'],
	  																	date("Y-m-d H:i:s"),
	  																	$language_value
	  																	);
	  		
	  		if(reset_order_id("lumonata_shipping")){
	  			$db->do_query($sql);
	  		}else{
	  			$db->do_query($sql);
	  		}
	  		echo get_shipping_rate();
  		}*/
  		
  		$type_value = $_POST['type_value'];
  		$parameter_value = $_POST['parameter_value'];
  		$charge_value = $_POST['charge_value'];
  		$currency_value = $_POST['currency_value'];
  		$language_value = $_POST['language_value'];
  		$country_value = $_POST['country_value']; 
  		$shipping_option_value = $_POST['shipping_option_value'];
  		$unit_symbol = $_POST['unit_symbol'];
  		$shipped_tax = $_POST['shipped_tax'];
  		
  		$check_shipping=$db->prepare_query("SELECT * from lumonata_shipping where lcountry_id=%d AND lshipping_option_id=%d order by lshipping_id DESC limit 1",$country_value,$shipping_option_value);
 		$query_check_shipping=$db->do_query($check_shipping);
 		
 		if($db->num_rows($query_check_shipping)<>0){
 			$data_check_shipping=$db->fetch_array($query_check_shipping);
 			if($data_check_shipping['lshipping_type']==$type_value){
 				if($data_check_shipping['lformula']<$parameter_value){
		 				$sql=$db->prepare_query("INSERT INTO lumonata_shipping( lcountry_id,
		 																		lshipping_option_id,
			  																	lshipping_type,
			  																	lcurrency_id,
			  																	lformula,
			  																	unit_symbol,
			  																	lprice,
			  																	lshipping_tax,
			  																	lcreated_by,
			  																	lcreated_date,
			  																	lusername,
			  																	ldlu,
			  																	llang_id)
			  									VALUES(%d,%d,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
			  																	$country_value,
			  																	$shipping_option_value,
			  																	$type_value,
			  																	$currency_value,
			  																	$parameter_value,
			  																	$unit_symbol,
			  																	$charge_value,
			  																	$shipped_tax,
			  																	$_COOKIE['user_id'],
			  																	date("Y-m-d H:i:s"),
			  																	$_COOKIE['user_id'],
			  																	date("Y-m-d H:i:s"),
			  																	$language_value
			  																	);
			  		
			  		if(reset_order_id("lumonata_shipping")){
			  			$db->do_query($sql);
			  		}else{
			  			$db->do_query($sql);
			  		}
			  		echo get_shipping_rate();
 				}else{
 					echo get_shipping_rate('3');
 				}
 			}else{
 				echo get_shipping_rate('2');
 			}
 		}else{
	  		$sql=$db->prepare_query("INSERT INTO lumonata_shipping( lcountry_id,
	  																	lshipping_option_id,
	  																	lshipping_type,
	  																	lcurrency_id,
	  																	lformula,
	  																	unit_symbol,
	  																	lprice,
	  																	lshipping_tax,
	  																	lcreated_by,
	  																	lcreated_date,
	  																	lusername,
	  																	ldlu,
	  																	llang_id)
	  									VALUES(%d,%d,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
	  																	$country_value,
	  																	$shipping_option_value,
	  																	$type_value,
	  																	$currency_value,
	  																	$parameter_value,
	  																	$unit_symbol,
	  																	$charge_value,
	  																	$shipped_tax,
	  																	$_COOKIE['user_id'],
	  																	date("Y-m-d H:i:s"),
	  																	$_COOKIE['user_id'],
	  																	date("Y-m-d H:i:s"),
	  																	$language_value
	  																	);
	  		
	  		if(reset_order_id("lumonata_shipping")){
	  			$db->do_query($sql);
	  		}else{
	  			$db->do_query($sql);
	  		}
	  		echo get_shipping_rate();
  		}
  		
  	}
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='del_shipping')){
  		global $db;
  		$query_shipping=get_shipping_price();
  		$row_shipping=$db->num_rows($query_shipping);
  		if($row_shipping>1){
  			$shipping_id=$_POST['lvalue'];
  			delete_shipping_record($shipping_id);
  			echo get_shipping_rate();
  		}else{
  			echo get_shipping_rate('1');
  		}
  	}
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='edit_shipping')){
	  	echo shipping_rate($_POST['lvalue']);
	}
	
  if(isset($_POST['pKEY']) && ($_POST['pKEY']=='edit_shipping_rate')){
  		global $db;
  		$shipping_id=$_POST['id'];
  		$parameter_value = $_POST['parameter_value'];
  		$charge_value = $_POST['charge_value'];
  		$shipped_tax = $_POST['shipped_tax'];
  		
  		/*$shipping_sql=$db->prepare_query("SELECT * from lumonata_shipping where lshipping_id=%d",$shipping_id);
  		$shipping_qry=$db->do_query($shipping_sql);
  		$shipping_data=$db->fetch_array($shipping_qry);
  		
  		if($parameter_value >= $shipping_data['lformula']){*/	
	  		$sql=$db->prepare_query("UPDATE lumonata_shipping set lformula=%s, lprice=%s, lshipping_tax=%s where lshipping_id=%d",$parameter_value,$charge_value,$shipped_tax,$shipping_id);
	  		$db->do_query($sql);
	  	
	  		echo shipping_rate();
  		/*}else{
  			echo shipping_rate('','3');
  		}*/
  		
	}
	
	///action ajax for shipping option
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='insert_shipping_option')){
  		global $db;
  		$sn = $_POST['shipping_name'];
  		$sd = $_POST['shipping_desc'];
  		
  		$q = $db->prepare_query("select * from lumonata_shipping_option where loption=%s",$sn);
  		$r = $db->do_query($q);
  		$n = $db->num_rows($r);
  		
  		if ($n==0){
  			$q = $db->prepare_query("INSERT INTO lumonata_shipping_option 
  									(loption,ldesc,lcreated_by,lcreated_date,lusername)
  									VALUES
  									(%s,%s,%s,%s,%s)",
  									$sn,$sd,$_COOKIE['user_id'],date("Y-m-d H:i:s"),$_COOKIE['user_id']
  									);
  			if(reset_order_id("lumonata_shipping_option")){
  				$db->do_query($q);
  			}else{
	  			$db->do_query($q);
	  		}
	  		echo get_table_shipping_option().'&'.shipping_option();
  		}else{
  			echo get_table_shipping_option(3);
  		}
	}
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='edit_shipping_option')){
	  	echo shipping_option_backend($_POST['lvalue']);
	}
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='save_edit_shipping_option')){
		global $db;
		$id = $_POST['id'];
		$sn = $_POST['shipping_name'];
		$sd = $_POST['shipping_desc'];
		
		$q = $db->prepare_query("update lumonata_shipping_option set 
					loption=%s,ldesc=%s,ldlu=%s WHERE lshipping_option_id=%d",$sn,$sd,date("Y-m-d H:i:s"),$id); 
		$r = $db->do_query($q);
		
		echo shipping_option_backend().'[BREAK]'.shipping_option();
		
	}
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='del_shipping_option')){
  		global $db;
  		$shipping_option_id = $_POST['shipping_option_id'];
  		//delete pada tabel sub :  lumonata_shipping
  		$q = $db->prepare_query("DELETE from lumonata_shipping where lshipping_option_id=%d",$shipping_option_id);
  		$r = $db->do_query($q);
  		//delete pada tabel master  : lumonata_shipping_option
  		$q = $db->prepare_query("DELETE from lumonata_shipping_option where lshipping_option_id=%d",$shipping_option_id);
  		$r = $db->do_query($q);
  		echo get_table_shipping_option();
  	}
  	///
	
	
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='create_tax_group')){
	  	global $db;
	  	
	  	$lvalue=$_POST['lvalue'];
	  	if(isset($_POST['set_default'])){
	  		$default=$_POST['set_default'];
	  		$sql_update=$db->prepare_query("UPDATE lumonata_tax_groups set ldefault=0");
	  		$query_update=$db->do_query($sql_update);
	  	}else{
	  		$sql_check=$db->prepare_query("SELECT * FROM lumonata_tax_groups");
	  		$query_check=$db->do_query($sql_check);
	  		if(($db->num_rows($query_check))<>0){
	  			$default=0;
	  		}else{
	  			$default=0;
	  		}
	  	}
	  	$sql=$db->prepare_query("INSERT INTO lumonata_tax_groups(ldescription,
	  															ldefault,
	  															lpost_by,
	  															lpost_date,
	  															lupdated_by,
	  															ldlu)
	  													VALUES(%s,%s,%s,%s,%s,%s)",
	  															$lvalue,
	  															$default,
	  															$_COOKIE['user_id'],
  																date("Y-m-d H:i:s"),
  																$_COOKIE['user_id'],
  																date("Y-m-d H:i:s")
  															);
		if(reset_order_id("lumonata_tax_groups")){
  			$db->do_query($sql);
  		}else{
  			$db->do_query($sql);
  		}
	  	
	  	echo get_groups_tax();
	}
	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='del_tax_group')){
		global $db;
  			$running=delete_tax_group_record($_POST['lvalue']);
  		if($running==1){
  			$sql_check1=$db->prepare_query("SELECT * FROM lumonata_tax_groups");
  			$query_check1=$db->do_query($sql_check1);
  			
			$sql_delete_tax_details=$db->prepare_query("DELETE FROM lumonata_tax_detail where ltax_groups_ID=%d",$_POST['lvalue']);
			$query_delete_tax_details=$db->do_query($sql_delete_tax_details);
			
  			echo get_groups_tax();
  		}else{
  			echo get_groups_tax('1');
  		}
	}
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='edit_tax_group')){
  		echo get_groups_tax('',$_POST['lvalue']);
  	}
  	
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='save_edit_tax_group')){
  		global $db;
  		
  		$sql_update=$db->prepare_query("UPDATE lumonata_tax_groups set ldescription = %s where ltax_groups_ID=%d",$_POST['lvalue'],$_POST['lid']);
	  	$query_update=$db->do_query($sql_update);
  		echo get_groups_tax();
  	}
  	  
  	if(isset($_POST['pKEY']) && ($_POST['pKEY']=='add_country_to_grid')){
  		global $db;
  		
  		$value_id=$_POST['lvalue'];
  		$QueryCountry=get_country($value_id);
  		$DataCountry=$db->fetch_array($QueryCountry);
  		$TaxGroup=GetTaxGroupToGrid($value_id);
  		$TaxGrid='<tr id="RowTaxGrid_'.$value_id.'">
					<td style="background-color:#ffffff;padding: 8px;">'.$DataCountry['lcountry'].'<input type="hidden" name="country_'.$value_id.'" value="'.$value_id.'"></td>
					<td style="background-color:#ffffff;padding: 4px;text-align:right;">'.$TaxGroup.'</td>
					<td style="background-color:#ffffff;padding: 4px;text-align:center">-</td>
					<td style="background-color:#ffffff;padding: 4px;text-align:center">';
  		if($value_id<>"247"){
  			$TaxGrid.='<a style="cursor:pointer" class="del_tax_grid" id="del_tax_grid_'.$value_id.'" rel="'.$value_id.'">delete</a>';
  		}
  		$TaxGrid.='</td></tr>';
  		$TaxGrid.='
 			<script>
 			$(document).ready(function(){
				$("#del_tax_grid_'.$value_id.'").click(function(){
					$("#RowTaxGrid_'.$value_id.'").remove();
					
				});
			});
 			</script>
 			';
  		echo $TaxGrid;
  	}
  	
  }
  
 function form_payment_offline_banktransfer($id_payment=''){ 
 	if(!empty($id_payment)){
 		
	 	$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
	 	
	 	$data_payments=get_meta_data('payments','product_setting');
		$count_p=0;
		
	 	$tmp_array_parent=array();
		$array_payments = json_decode( $data_payments,true);
	      if(is_array($array_payments)){
		      foreach($array_payments['parent_payment'] as $key=>$val){
			      // echo "parent".$key.":".$val;
		      	 if($val == $id_payment){
		      	 	  $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
							$tmp_count_p=$count_p;
					      	$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      	$count_c++;
				      	
				      }
		      	 }
			    $count_p++;
	       	 }
	      }
	      
	    if($tmp_array_parent['child_payment'][$tmp_count_p]['mode']==0){
	    	$test='checked="checked"';
	    	$active='';
	    }else{
	    	$active='checked="checked"';
	    	$test='';
	    }
	      
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="'.$tmp_array_parent['child_payment'][$tmp_count_p]['name'].'" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>Please use the following bank account number to pay for your items: BCC 000-00000000-00. Mention your order reference ID in your bank transfer so that we can find your payment and order quickly.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">'.$tmp_array_parent['child_payment'][$tmp_count_p]['text'].'</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode" '.$active.'>
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" '.$test.'>
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_bank_transfer" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}else{
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="Bank transfer" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>Please use the following bank account number to pay for your items: BCC 000-00000000-00. Mention your order reference ID in your bank transfer so that we can find your payment and order quickly.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">Please use the following bank account number to pay for your items: BCC 000-00000000-00. Mention your order reference ID in your bank transfer so that we can find your payment and order quickly.</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode">
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" checked="checked">
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_bank_transfer" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}
		$result.='<script>
			$(document).ready(function() {
			var param1=false;
			var param2=false;
			$("#payment_name").blur(function(){
				if($(this).val()<1){
					$(this).next(".validate_box").remove();
					$(this).after("<span class=\"validate_box validate_no\"><span>The Field Is Required</span></span>");
					param1=true;
 				}else{
					$(this).next(".validate_box").remove();
					param1=false;
				}
			});
			
			$("#payment_text").blur(function(){
				if($(this).val()<1){
					$(this).next(".test").remove();
					$(this).after("<p class=\"test\"><span class=\"validate_box validate_no\"><span>The Field Is Required</span></span></p>");
					param2=true;
 				}else{
					$(this).next(".test").remove();
					param2=false;
				}
			});
			
			$("#save_bank_transfer").click(function(){
					if(param1==false && param2==false){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
						payment_name=$("#payment_name").val();
						payment_text=$("#payment_text").val();
						payment_runmode_active=$("[name=\'payment_runmode\']:checked").val();
					 	jQuery.post(thaUrl,{ 
					    		pKEY:"payments_bank_transfer",
					    		lvalue_name:payment_name,
					    		lvalue_text:payment_text,
					    		lvalue_mode:payment_runmode_active
				           },function(data){
				           		//jQuery("#table_payment").after(data);
				           		$("#accept_payments_details_0").html(data);
				           });
 					}
				})
			});
			</script>
		';
 		return $result;
 }
 
 function get_payment_method(){
 	$value[0]='name';
	$value[1]='text';
	$value[2]='mode';
 	
 	$data_payments=get_meta_data('payments','product_setting');
	$count_p=0;

	$tmp_array_parent=array();
	$array_payments = json_decode($data_payments,true);
      if(is_array($array_payments)){
	      foreach($array_payments['parent_payment'] as $key=>$val){
		      $count_c=0;
		      $tmp_array_parent['parent_payment'][$count_p]=$val;
		     //echo "parent".$key.":".$val;
		     
		      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){

		      		$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
		      		//echo "child ".$subkey.":".$subval."<br>";
			      	$count_c++;
		      	
		      }
		    $count_p++;
       	 }
      }
 	$result='';
    for($j=0;$j<$count_p;$j++){

    	if($tmp_array_parent['child_payment'][$j]['mode']==0){
    		$status='TEST/SANDBOX';
    	}else{
    		$status='ACTIVE';
    	}

	 	$result.='<tr style="background-color:#cccccc">
							<td style="width:400px;border-top:0px;border-bottom:0px;background-color:#ffffff;padding-left:10px;padding-bottom:10px;padding-top:10px">'.$tmp_array_parent['child_payment'][$j]['name'].'</td>
							<td style="width:200px;border-top:0px;border-bottom:0px;background-color:#ffffff;padding-left:10px;text-align:center;padding-bottom:10px;padding-top:10px">'.$status.'</td>
							<td style="width:150px;border-top:0px;border-bottom:0px;background-color:#ffffff;padding-left:10px;text-align:center;padding-bottom:10px;padding-top:10px"><a class="payment_config" rel="'.$tmp_array_parent['parent_payment'][$j].'" style="cursor:pointer">configure</a></td>
							<td style="width:50px;border-top:0px;border-bottom:0px;background-color:#ffffff;padding-left:10px;text-align:center;padding-bottom:10px;padding-top:10px"><a id="del_payment_config_'.$tmp_array_parent['parent_payment'][$j].'" class="del_payment_config" rel="'.$tmp_array_parent['parent_payment'][$j].'" style="cursor:pointer">delete</a></td>
						</tr>'.delete_payment($tmp_array_parent['parent_payment'][$j]).'
						<link rel="stylesheet" href="http://'.SITE_URL.'/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css" type="text/css" media="screen" />
						<script>
						$(document).ready(function() {
							$(".payment_config").click(function(){
								var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
								var payment_type=$(this).attr("rel");
								 jQuery.post(thaUrl,{ 
								    		pKEY:"edit_payment",
								    		lvalue:payment_type
							           },function(data){
							           		$("#payment_line").html(data);
							           });
							});
							
							$("#del_payment_config_"+"'.$tmp_array_parent['parent_payment'][$j].'").click(function(){
								var payment_type=$(this).attr("rel");
								$("#delete_payment_form_'.$tmp_array_parent['parent_payment'][$j].'").dialog("open");
							});
							del_payment_'.$tmp_array_parent['parent_payment'][$j].'();
						});
						
						function del_payment_'.$tmp_array_parent['parent_payment'][$j].'(){
							 $("#delete_payment_form_'.$tmp_array_parent['parent_payment'][$j].'").dialog({
							      bgiframe: true,
							      autoOpen: false,
							      resizable: false,
							      height:170,
							      width:330,
							      modal: true,							      
							      buttons: {
							        "Yes, delete payment method": function() {
							          var thaUrl 	= "http://'.SITE_URL.'/product-setting-ajax";
							          var payment_type= "'.$tmp_array_parent['parent_payment'][$j].'";
								    	jQuery.post(thaUrl,{ 
								    		pKEY:"del_payment",
								    		lvalue:payment_type
							              },function(data){
												$("#accept_payments_details_0").html(data);
								        });
							           $(this).dialog("close");
							           
							          },Cancel: function() {
							            $(this).dialog("close");
							          }
							        }
							   })
						}
						</script>
						';
    }
 	
 	return $result;
 }
 
 function delete_payment($index){
   		$delete_it="
		  <div id=\"delete_payment_form_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this payment?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
   	
 function form_payment_paypal_standard($id_payment=''){ 
 	if(!empty($id_payment)){	
	 	$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
	 	
	 	$data_payments=get_meta_data('payments','product_setting');
		$count_p=0;

	 	$tmp_array_parent=array();
		$array_payments = json_decode( $data_payments,true);
	      if(is_array($array_payments)){
		      foreach($array_payments['parent_payment'] as $key=>$val){
		      	$count_c=0;
			      // echo "parent".$key.":".$val;
		      	 if($val == $id_payment){
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
							$tmp_count_p=$count_p;
					      	$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      	$count_c++;
				      	
				      }
		      	 }
			    $count_p++;
	       	 }
	      }
	      
	    if($tmp_array_parent['child_payment'][$tmp_count_p]['mode']==0){
	    	$test='checked="checked"';
	    	$active='';
	    }else{
	    	$active='checked="checked"';
	    	$test='';
	    }
	      
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="'.$tmp_array_parent['child_payment'][$tmp_count_p]['name'].'" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>PayPal Email Address</strong></label></p>
				<p><input id="payment_account" class="medium_textbox" type="text" name="payment_account" value="'.$tmp_array_parent['child_payment'][$tmp_count_p]['text'].'"></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode" '.$active.'>
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" '.$test.'>
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_paypal_standard" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}else{
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="PayPal website payments standard" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>PayPal Email Address</strong></label></p>
				<p><input id="payment_account" class="medium_textbox" type="text" name="payment_account" value=""></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode">
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" checked="checked">
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_paypal_standard" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}
		$result.='<script>
			$(document).ready(function() {
			var param1=false;
			var param2=true;
			$("#payment_name").blur(function(){
				if($(this).val()<1){
					$(this).next(".validate_box").remove();
					$(this).after("<span class=\"validate_box validate_no\"><span>The Field Is Required</span></span>");
					param1=true;
 				}else{
					$(this).next(".validate_box").remove();
					param1=false;
				}
			});
			
			$("#payment_account").blur(function(){
				if($(this).val()<1){
					$(this).next(".validate_box").remove();
					$(this).after("<span class=\"validate_box validate_no\"><span>The Field Is Required</span></span>");
					param2=true;
 				}else{
					$(this).next(".validate_box").remove();
					param2=false;
				}
			});
				var lengt_acount = $("#payment_account").val();
			$("#save_paypal_standard").click(function(){
					if(lengt_acount.length>1){
						param2=false;
	 				}
					if(param1==false && param2==false){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
						payment_name=$("#payment_name").val();
						payment_text=$("#payment_account").val();
						payment_runmode_active=$("[name=\'payment_runmode\']:checked").val();
						
					 	jQuery.post(thaUrl,{ 
					    		pKEY:"payments_paypal_standard",
					    		lvalue_name:payment_name,
					    		lvalue_text:payment_text,
					    		lvalue_mode:payment_runmode_active
				           },function(data){
				           		//jQuery("#table_payment").after(data);
				           		$("#accept_payments_details_0").html(data);
				           });
 					}
				})
			});
			</script>
		';
 		return $result;
 }
 
 function form_payment_offline_cashondelivery($id_payment=''){ 
 	if(!empty($id_payment)){
 		
	 	$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
	 	
	 	$data_payments=get_meta_data('payments','product_setting');
		$count_p=0;
		
	 	$tmp_array_parent=array();
		$array_payments = json_decode( $data_payments,true);
	      if(is_array($array_payments)){
		      foreach($array_payments['parent_payment'] as $key=>$val){
			      // echo "parent".$key.":".$val;
		      	 if($val == $id_payment){
		      	 	  $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
							$tmp_count_p=$count_p;
					      	$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      	$count_c++;
				      	
				      }
		      	 }
			    $count_p++;
	       	 }
	      }
	      
	    if($tmp_array_parent['child_payment'][$tmp_count_p]['mode']==0){
	    	$test='checked="checked"';
	    	$active='';
	    }else{
	    	$active='checked="checked"';
	    	$test='';
	    }
	      
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="'.$tmp_array_parent['child_payment'][$tmp_count_p]['name'].'" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>We will ship the ordered items to you and you will receive them when you pay for the order on delivery.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">'.$tmp_array_parent['child_payment'][$tmp_count_p]['text'].'</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode" '.$active.'>
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" '.$test.'>
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_cashondelivery" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}else{
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="Cash on delivery" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>We will ship the ordered items to you and you will receive them when you pay for the order on delivery.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">We will ship the ordered items to you and you will receive them when you pay for the order on delivery.</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode">
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" checked="checked">
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_cashondelivery" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}
		$result.='<script>
			$(document).ready(function() {
			var param1=false;
			var param2=false;
			$("#payment_name").blur(function(){
				if($(this).val()<1){
					$(this).next(".validate_box").remove();
					$(this).after("<span class=\"validate_box validate_no\"><span>The Field Is Required</span></span>");
					param1=true;
 				}else{
					$(this).next(".validate_box").remove();
					param1=false;
				}
			});
			
			$("#payment_text").blur(function(){
				if($(this).val()<1){
					$(this).next(".test").remove();
					$(this).after("<p class=\"test\"><span class=\"validate_box validate_no\"><span>The Field Is Required</span></span></p>");
					param2=true;
 				}else{
					$(this).next(".test").remove();
					param2=false;
				}
			});
			
			$("#save_cashondelivery").click(function(){
					if(param1==false && param2==false){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
						payment_name=$("#payment_name").val();
						payment_text=$("#payment_text").val();
						payment_runmode_active=$("[name=\'payment_runmode\']:checked").val();
					 	jQuery.post(thaUrl,{ 
					    		pKEY:"payments_cashondelivery",
					    		lvalue_name:payment_name,
					    		lvalue_text:payment_text,
					    		lvalue_mode:payment_runmode_active
				           },function(data){
				           		//jQuery("#table_payment").after(data);
				           		$("#accept_payments_details_0").html(data);
				           });
 					}
				})
			});
			</script>
		';
 		return $result;
 }
 
 function form_payment_offline_cheque($id_payment=''){ 
 	if(!empty($id_payment)){

	 	$value[0]='name';
		$value[1]='text';
		$value[2]='mode';
	 	
	 	$data_payments=get_meta_data('payments','product_setting');
		$count_p=0;
		
	 	$tmp_array_parent=array();
		$array_payments = json_decode( $data_payments,true);
	      if(is_array($array_payments)){
		      foreach($array_payments['parent_payment'] as $key=>$val){
			      // echo "parent".$key.":".$val;
		      	 if($val == $id_payment){
		      	 	  $count_c=0;
				      foreach($array_payments['child_payment'][$key] as $subkey=>$subval){
							$tmp_count_p=$count_p;
					      	$tmp_array_parent['child_payment'][$count_p][$value[$count_c]]=$subval;
					      	$count_c++;
				      	
				      }
		      	 }
			    $count_p++;
	       	 }
	      }
	      
	    if($tmp_array_parent['child_payment'][$tmp_count_p]['mode']==0){
	    	$test='checked="checked"';
	    	$active='';
	    }else{
	    	$active='checked="checked"';
	    	$test='';
	    }
	      
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="'.$tmp_array_parent['child_payment'][$tmp_count_p]['name'].'" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>We will ship the ordered items to you and you will receive them when you pay for the order on delivery.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">'.$tmp_array_parent['child_payment'][$tmp_count_p]['text'].'</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode" '.$active.'>
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" '.$test.'>
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_cheque" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}else{
 		$result='<p><label for="payment_name"><strong>Payment Method Name</strong></label></p>
				<p class="tip" style="font-size:11px">This is the name your clients will see when they choose a payment option</p>
				<p><input id="payment_name" class="medium_textbox" type="text" value="Cheque" name="payment_name"></p>
				<p><label class="" for="payment_text"><strong>We will ship the ordered items to you and you will receive them when you pay for the order on delivery.</strong></label></p>
				<p><textarea id="payment_text" name="payment_text" rows="8" style="width: 98%" class="textarea">We will ship your order once we clear your cheque.</textarea></p>
				<p><label class="" for="payment_runmode"><strong>Modus</strong></label></p>
				<p class="tip" style="font-size:11px">Set this to test/sandbox while you are testing</p>
				<p><input id="payment_runmode_active" type="radio" value="1" name="payment_runmode">
					<label class="checkboxlabel" for="payment_runmode_active">Active</label>
					<input id="payment_runmode_sandbox" type="radio" value="0" name="payment_runmode" checked="checked">
					<label class="checkboxlabel" for="payment_runmode_sandbox">Test/Sandbox</label>
				</p>
				<p><input id="save_cheque" class="btn_save_changes_enable" type="button" value="Save payment method" name="save_payment"> ';
		$result.=" <input class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\" onclick=\"location='".get_state_url('shoppingcart&sub=product_setting&tab=payments')."'\">";
 	}
		$result.='<script>
			$(document).ready(function() {
			var param1=false;
			var param2=false;
			$("#payment_name").blur(function(){
				if($(this).val()<1){
					$(this).next(".validate_box").remove();
					$(this).after("<span class=\"validate_box validate_no\"><span>The Field Is Required</span></span>");
					param1=true;
 				}else{
					$(this).next(".validate_box").remove();
					param1=false;
				}
			});
			
			$("#payment_text").blur(function(){
				if($(this).val()<1){
					$(this).next(".test").remove();
					$(this).after("<p class=\"test\"><span class=\"validate_box validate_no\"><span>The Field Is Required</span></span></p>");
					param2=true;
 				}else{
					$(this).next(".test").remove();
					param2=false;
				}
			});
			
			$("#save_cheque").click(function(){
					if(param1==false && param2==false){
						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
						payment_name=$("#payment_name").val();
						payment_text=$("#payment_text").val();
						payment_runmode_active=$("[name=\'payment_runmode\']:checked").val();
					 	jQuery.post(thaUrl,{ 
					    		pKEY:"payments_cheque",
					    		lvalue_name:payment_name,
					    		lvalue_text:payment_text,
					    		lvalue_mode:payment_runmode_active
				           },function(data){
				           		//jQuery("#table_payment").after(data);
				           		$("#accept_payments_details_0").html(data);
				           });
 					}
				})
			});
			</script>
		';
 		return $result;
 }
 
 function get_symbol_currency(){
 	global $db;
 	$data_currency=get_meta_data('currency','product_setting');
	$sql=$db->prepare_query("SELECT * from lumonata_currency where lcurrency_id=%d",$data_currency);
	$query=$db->do_query($sql);
	$data_currency=$db->fetch_array($query);
	
	return $data_currency['lcode'];
 }
 
function get_unit_system(){
 	$data_currency=get_meta_data('unit_system','product_setting');
 	
	return $data_currency;
 }
 
function get_shipping_rate($alert=''){
 	global $db;
 	if($alert=="1"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">At list one shipping is required.</div>";
 	}else if($alert=="2"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">Cannot mix shipping types in same country.</div>";
 	}else if($alert=="3"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">The shipping rule your tried add already exists.</div>";
 	}else{
 		$status="";
 	}
 	$result = $status;
 	$result.= '<p><table cellspacing="1" style="background-color:#cccccc" cellpading="1" >';
 	
 	$q_gby_country=get_shipping_price('','','lcountry_id');
	while ($d_gby_country=$db->fetch_array($q_gby_country)){
		$country_id  = $d_gby_country['lcountry_id'];
		$arr_country = $db->fetch_array(get_country($country_id));	
		
		$result		.= '<tr id="table_shipping"><th style="width:800px;border-top:0px;border-bottom:0px;text-align:left" colspan="6">'.$arr_country['lcountry'].'</th></tr>';
		$result		.= get_row_shipping_price($country_id);		
	}
 	$result .= '</table></p>';
 	return $result;
 }
 
function get_row_shipping_price($country_id){
 	global $db;
 	$tr = '';
 	$q	= get_shipping_price($country_id,'','lcountry_id,lshipping_option_id'); //group by lcountry_id,lshipping_option_id
	while ($d=$db->fetch_array($q)){
		$country_id  = $d['lcountry_id'];
		$so_id		 = $d['lshipping_option_id'];	
		$arr_so 	 = $db->fetch_array(get_shipping_option($so_id)); //array shipping option
		$rowspan	 = $db->num_rows(get_shipping_price($country_id,$so_id)); 
		
		$tr.= '<tr id="table_shipping">';
		$tr.= '<td rowspan="'.($rowspan+1).'" style="background-color:#ffffff;padding: 4px;">'.$arr_so['loption'].'</td>';  
		$tr.= get_td_shipping_price($country_id,$so_id);
		$tr.= '</tr>';	

		
	}
 	return $tr;
 }
 
function get_td_shipping_price($country_id,$shipping_option_id){
 	global $db;
 	$td 	= '';
 	$q		= get_shipping_price($country_id,$shipping_option_id); 
 	$temp	= 0;
	$num	= $db->num_rows($q);
	while ($d=$db->fetch_array($q)){
		$temp++;
		if($temp==$num){
			$up=' and up';
		}else{
			$up='';
		}
	 	if(($d['lshipping_type'])=='price'){
	 		$symbol=get_symbol_currency();
	 	}else if(($d['lshipping_type'])=='weight'){
	 		$unitsymbol=get_unit_system();
	 		if($unitsymbol=='metric'){
	 			$symbol='Kilograms';
	 		}else{
	 			$symbol='Pounds';
	 		}
	 	}else{
	 		$symbol=$d['unit_symbol'];
	 	}
		
	 	$del = '';
	 	if($d['lcountry_id']<>"247"){
			$del ='<a style="cursor:pointer" class="del_shipping" id="del_shipping_'.$d['lshipping_id'].'" rel="'.$d['lshipping_id'].'">delete</a>';
		}			
	 	
		$td.='<tr  id="table_shipping"><td style="background-color:#ffffff;padding: 4px;">From '.$d['lformula'].' '.$symbol.$up.'</td>
			  <td style="background-color:#ffffff;padding: 4px;">Charge '.$d['lprice'].' '.get_symbol_currency().'</td>
			  <td style="background-color:#ffffff;padding: 4px;">Shipping Tax '.$d['lshipping_tax'].'%</td>
			  <td style="background-color:#ffffff;padding: 4px;text-align:center"><a style="cursor:pointer" class="edit_shipping" rel="'.$d['lshipping_id'].'">edit</a></td>
			  <td style="background-color:#ffffff;padding: 4px;text-align:center"> '.$del.' </td>
			  </tr>
			  ';
		$td.=delete_shipping($d['lshipping_id']);
		
		$td.='<script>
		 			$(document).ready(function(){
		 				$(".edit_shipping").click(function(){
							var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
							var shipping_id=$(this).attr("rel");
							 jQuery.post(thaUrl,{ 
							    		pKEY:"edit_shipping",
							    		lvalue:shipping_id
						           },function(data){
						           		$("#shipping_rate_details_0").html(data);
						      });
						});
		 			
		 			
		 				$("#del_shipping_'.$d['lshipping_id'].'").click(function(){
							$("#delete_shipping_form_'.$d['lshipping_id'].'").dialog("open");
						});
						del_shipping_price_'.$d['lshipping_id'].'();
					});
					
					function del_shipping_price_'.$d['lshipping_id'].'(){
						 $("#delete_shipping_form_'.$d['lshipping_id'].'").dialog({
						      bgiframe: true,
						      autoOpen: false,
						      resizable: false,
						      height:170,
						      width:330,
						      modal: true,							      
						      buttons: {
						        "Yes, delete shipping rule": function() {
						          var thaUrl 	= "http://'.SITE_URL.'/product-setting-ajax";
						          var shipping_id = "'.$d['lshipping_id'].'";
							    	jQuery.post(thaUrl,{ 
							    		pKEY:"del_shipping",
							    		lvalue:shipping_id
						              },function(data){
											$("#shipping_rate_list").html(data);
							        });
						           $(this).dialog("close");
						           
						          },Cancel: function() {
						            $(this).dialog("close");
						          }
						        }
						   })
					}
					</script>';
	}
 	return $td;
 }
 
  function delete_shipping($index){
   		$delete_it="<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />
		  <div id=\"delete_shipping_form_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this shipping rule?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
   function delete_shipping_option($index){
   		$delete_it="<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />
		  <div id=\"delete_shipping_option_form_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this shipping option?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
 
 function get_shipping_price($country_id='',$shipping_option_id='',$group=''){
 	global $db;
 	if($country_id<>'' && $shipping_option_id<>''){
 		$where="Where lcountry_id=".$db->_real_escape($country_id)." AND lshipping_option_id=".$db->_real_escape($shipping_option_id);
 	}elseif($country_id<>''){
 		$where="Where lcountry_id=".$db->_real_escape($country_id);
 	}else{
 		$where="";
 	}
 	
  	if($group<>''){
 		$groupby=" Group By ".$group;
 	}else{
 		$groupby="";
 	}
 	
 	$sql=$db->prepare_query("SELECT * from lumonata_shipping ".$where.$groupby." order by lshipping_id ASC"); 
 	$query=$db->do_query($sql);
 	
 	return $query;
 }
 
 function get_shipping_edit($shipping_id=''){
 	global $db;
 	if($shipping_id<>''){
 		$where="Where lshipping_id=".$db->_real_escape($shipping_id);
 	}else{
 		$where="";
 	}
 	
 	$sql=$db->prepare_query("SELECT * from lumonata_shipping ".$where." order by lshipping_id ASC");
 	$query=$db->do_query($sql);
 	
 	return $query;
 }
 
 function get_country($id=''){
 	global $db;
 	if(!empty($id)){
 		$sql=$db->prepare_query("SELECT * from lumonata_country where lcountry_id=%s",$id);
 	}else{
 		$sql=$db->prepare_query("SELECT * from lumonata_country");
 	}
	$query=$db->do_query($sql);
	
	return $query;
 }
 
 function get_shipping_option($id=''){
 	global $db;
 	/*///ORI
 	if(!empty($id)){
 		$sql=$db->prepare_query("SELECT * from lumonata_shipping_option where lshipping_option_id=%d",$id);
 	}else{
 		$sql=$db->prepare_query("SELECT * from lumonata_shipping_option");
 	}
 	*/
 	$sql=$db->prepare_query("SELECT * from lumonata_shipping_option where lshipping_option_id=%d",$id);
 	$query=$db->do_query($sql);	
	return $query;
 }
 
 function delete_shipping_record($id=''){
 	global $db;
 	$sql=$db->prepare_query("DELETE FROM lumonata_shipping where lshipping_id=%d",$id);
 	$query=$db->do_query($sql);
 	
 	if($query){
 		return true;
 	}else{
 		return false;
 	}
 }
 
 function get_groups_tax($alert='',$id=''){
	if($alert=="1"){
 		$status="<div class=\"Alert_red_form\" style=\"background-color: #FF6666;border-radius: 5px 5px 5px 5px;color: #333333;font-size: 14px;height: auto;margin: 20px auto;padding: 5px;width: 96%;\">The default tax group cannot be delete.</div>";
 	}else{
 		$status="";
 	}
 	
 	global $db;
 	$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
 	$query=$db->do_query($sql);
 	
 	$result=$status.'<fieldset><div id="group_taxes"><table cellspacing="1" style="background-color:#cccccc" cellpading="1" >';
 	$result.='<tr>
				<th style="width:150px;border-top:0px;border-bottom:0px;text-align:left" colspan="2">Tax Group</th>
				<th style="width:100px;border-top:0px;border-bottom:0px;text-align:left;text-align:center">Default</th>
				<th style="width:100px;border-top:0px;border-bottom:0px;text-align:left"></th>
			</tr>';
 	while($data=$db->fetch_array($query)){
 		if($data['ldefault']==1){
 			$select='checked="checked"';
 		}else{
 			$select='';
 		}
	 	$result.='<tr>
					<td style="background-color:#ffffff;padding: 4px;" colspan="2">'.$data['ldescription'].'</td>
					<td style="background-color:#ffffff;padding: 4px;text-align:center"><input type="radio" name="set_default" value="'.$data['ltax_groups_ID'].'" '.$select.'></td>
					<td style="background-color:#ffffff;padding: 4px;text-align:center"><a style="cursor:pointer" rel="'.$data['ltax_groups_ID'].'" id="rename_tax_group_'.$data['ltax_groups_ID'].'">rename</a> | <a style="cursor:pointer" rel="'.$data['ltax_groups_ID'].'" id="del_tax_group_'.$data['ltax_groups_ID'].'">delete</a></td>
				</tr>'.delete_tax_group($data['ltax_groups_ID'],$data['ldescription']).'
				<script>
				$(document).ready(function(){
					$("#del_tax_group_'.$data['ltax_groups_ID'].'").click(function(){
						$("#delete_tax_group_form_'.$data['ltax_groups_ID'].'").dialog("open");
					});
 					del_tax_groups_'.$data['ltax_groups_ID'].'();
 					function del_tax_groups_'.$data['ltax_groups_ID'].'(){
						 $("#delete_tax_group_form_'.$data['ltax_groups_ID'].'").dialog({
						      bgiframe: true,
						      autoOpen: false,
						      resizable: false,
						      height:170,
						      width:330,
						      modal: true,							      
						      buttons: {
						        "Yes, delete this tax group": function() {
						          var thaUrl 	= "http://'.SITE_URL.'/product-setting-ajax";
						          var group_id = "'.$data['ltax_groups_ID'].'";
							    	jQuery.post(thaUrl,{ 
							    		pKEY:"del_tax_group",
							    		lvalue:group_id
						              },function(data){
						              		//alert(data);
											$("#get_groups_tax_details_0").html(data);
							        });
						           $(this).dialog("close");
						           
						          },Cancel: function() {
						            $(this).dialog("close");
						          }
						        }
						   })
					}
					
					$("#rename_tax_group_'.$data['ltax_groups_ID'].'").click(function(){
 							var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
							var tax_group_id = $("#rename_tax_group_'.$data['ltax_groups_ID'].'").attr("rel");
							 jQuery.post(thaUrl,{ 
							    		pKEY:"edit_tax_group",
							    		lvalue:tax_group_id
						           },function(data){
						           		$("#get_groups_tax_details_0").html(data);
						           		$("#group_taxes").hide();
						           		$("#tax_group_div").slideToggle();
						      });	
 					});
 					});
				</script>';
 	}
 	$result.='</table></p><p><input id="add_tax_groups" class="button" type="button" value="Add new tax group"></p></div>'.get_add_tax_group($id).'</fieldset>';
 	$result.='<script>
 				$(document).ready(function(){
 					$("#add_tax_groups").click(function(){
 						$("#tax_group_div").slideToggle();
 						$("#group_taxes").hide();
 					})
 					$("#btn_cancel_group").click(function(){
 						$("#group_taxes").slideToggle();
 						$("#tax_group_div").hide();
 					})
 					
					status1="false";
					$("#taxGroupName").blur(function(){
						if($(this).val()<1){
							$(this).css("border","1px solid #CD0A0A");
							status1="false";
						}else{
							$(this).css("border","1px solid #BBBBBB");
							status1="true";
						}
					})
					
					
	 					$("#create_tax_group").click(function(){
	 						var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
							var tax_group_name = $("#taxGroupName").val();
							var checkbox_value = $("#defaultTaxGroup:checked").val();
	 					if(status1=="true"){
							 jQuery.post(thaUrl,{ 
							    		pKEY:"create_tax_group",
							    		lvalue:tax_group_name,
							    		set_default:checkbox_value
						           },function(data){
						           		$("#tax_group_div").hide();
						           		$("#group_taxes").slideToggle();
						           		$("#get_groups_tax_details_0").html(data);
						      });
						}else{
 							if($("#taxGroupName").val()<1){
								$("#taxGroupName").css("border","1px solid #CD0A0A");
								status1="false";
							}else{
								$("#taxGroupName").css("border","1px solid #BBBBBB");
								status1="true";
							}
 						}
	 					});
 					
 				});
 				
 				
 			</script>';
 	return $result;
 }
 
 function get_add_tax_group($value=''){
 	if($value<>''){
 		global $db;
	 	$sql=$db->prepare_query("SELECT * from lumonata_tax_groups where ltax_groups_ID = %d",$value);
	 	$query=$db->do_query($sql);
	 	$data=$db->fetch_array($query);
	 	$result='<p>
 			<div id="tax_group_div" class="hidden is1" style="display: none;">
 				<input id="taxGroupName" class="medium_textbox" type="text" name="taxGroupName" value="'.$data['ldescription'].'">
 				<input id="taxGroupid" class="medium_textbox" type="hidden" name="taxGroupName" value="'.$data['ltax_groups_ID'].'">
 			</p>';
 		$result.='<p><input id="edit_tax_group" class="btn_save_changes_enable" type="button" value="Rename tax group" name="edit_tax_group"> ';
 		$result.="<input id=\"btn_cancel_group\" class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\"></p></div>";
 		$result.='<script>
 					status1="false";
					$("#taxGroupName").blur(function(){
						if($(this).val()<1){
							$(this).css("border","1px solid #CD0A0A");
							status1="false";
						}else{
							$(this).css("border","1px solid #BBBBBB");
							status1="true";
						}
					})
 		
 					$("#edit_tax_group").click(function(){
 						if(status1=="true"){
 							var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
							var tax_group_name = $("#taxGroupName").val();
							var tax_group_id = $("#taxGroupid").val();
							 jQuery.post(thaUrl,{ 
							    		pKEY:"save_edit_tax_group",
							    		lvalue:tax_group_name,
							    		lid:tax_group_id
						           },function(data){
						           		$("#get_groups_tax_details_0").html(data);;
						      });
						 }
 					});
				</script>';
 	}else{
 		$result='<p>
 			<div id="tax_group_div" class="hidden is1" style="display: none;">
 				<input id="taxGroupName" class="medium_textbox" type="text" name="taxGroupName">	
 			</p><p>
 				<input id="defaultTaxGroup" class="checkbox" type="checkbox" name="defaultTaxGroup" value="1">
				<label class="checkboxlabel" for="defaultTaxGroup"><strong>Make this the default tax group when adding new products</strong></label>
 			</p>';
 		$result.='<p><input id="create_tax_group" class="btn_save_changes_enable" type="button" value="Create tax group" name="create_tax_group"> ';
 		$result.="<input id=\"btn_cancel_group\" class=\"btn_cancel_enable\" type=\"button\" value=\"cancel\"></p></div>";
 	}
	
 	return $result;
 }
 
 function delete_tax_group($index='',$name){
   		$delete_it="<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />
		  <div id=\"delete_tax_group_form_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this tax group \"".$name."\"?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}
   	
  function delete_tax_group_record($id=''){
 	global $db;
 	$sql=$db->prepare_query("DELETE FROM lumonata_tax_groups where ltax_groups_ID=%d and ldefault=%d ",$id,0);
 	$query=$db->do_query($sql);
 	
 	return mysql_affected_rows();
 }
 
 function get_tax_general(){
 	global $db;
	$result='';
	$sql=$db->prepare_query("select * from lumonata_tax");
	$query=$db->do_query($sql);
	$data=$db->fetch_array($query);
	if($data['taxes_on_shipping']==1){
		$checked1='checked="checked"';
	}else{
		$checked1='';
	}
 	if($data['price_include_taxes']==1){
		$checked2='checked="checked"';
	}else{
		$checked2='';
	}
	
 	$result='<fieldset>
 				<p><input id="varTaxesOnShipping" type="checkbox" '.$checked1.' name="varTaxesOnShipping" value="1">
				<label class="checkboxlabel" for="varTaxesOnShipping">Charge taxes on shipping rates</label>
				</p><p>
				<input id="varPricesIncludeTaxes" type="checkbox" '.$checked2.' name="varPricesIncludeTaxes"  value="1">
				<label class="checkboxlabel" for="varPricesIncludeTaxes">All product prices are inclusive of taxes</label>
				</p>
 			</fieldset>';
 	
 	return $result;
 }
 
function get_tax_country_grid(){
 	$result='<fieldset>
 				<label for="shipping_to_country"><strong>Shipping to country</strong></label>
				<p>
					<select id="cbocountry" class="largefont" name="cbocountry">
					'.select_country().'
					</select>
				<span id="warning" style="display:none;color: #B43B3B;">This country is already in your list</span></p>
				<p><input id="btnAddCountryToTaxGrid" class="button" type="button" value="Add Country To Grid" name="btnAddCountryToTaxGrid"></p></fieldset>';
 	$result.='<script>
 			$(document).ready(function(){
				$("#btnAddCountryToTaxGrid").click(function(){
					var country_selected=$("#cbocountry option:selected").val();
					var thaUrl="http://'.SITE_URL.'/product-setting-ajax";
					if ( $("#RowTaxGrid_"+country_selected).size() > 0 ) { 
						$("#warning").css("display","inline");
					}else{
						$("#warning").css("display","none")
						jQuery.post(thaUrl,{ 
					    		pKEY:"add_country_to_grid",
					    		lvalue:country_selected
				           },function(data){
				           		$("#row_header_cell").after(data);
						});
					}
				})
			});	
 			</script>';
 	return $result;
 }
 
 function get_tax_list(){
 	$result='<h3>Active Taxes</h3>
  				<p><table cellspacing="1" cellpading="1" style="background-color:#cccccc" id="the_tax_table">';
 	$result.='<tr id="row_header_cell">
				<th style="width:250px;border-top:0px;border-bottom:0px;text-align:left">Destination</th>
				<th style="width:250px;border-top:0px;border-bottom:0px;text-align:center">Taxes to charge</th>
				<th style="width:150px;border-top:0px;border-bottom:0px;text-align:center">States & Provinces</th>
				<th style="width:100px;border-top:0px;border-bottom:0px;text-align:center"></th>
			</tr>';
 	$result.=get_content_tax_list();
 	$result.='</table></p>';

 	return $result;
 }
 
function get_content_tax_list(){
	global $db;
	$result='';
	$sql=$db->prepare_query("select * from lumonata_tax_detail group by lcountry_id order by ltax_detail_id");
	$query=$db->do_query($sql);
	while($data=$db->fetch_array($query)){
		$QueryCountry=get_country($data['lcountry_id']);
  		$DataCountry=$db->fetch_array($QueryCountry);
		$result.='<tr id="RowTaxGrid_'.$DataCountry['lcountry_id'].'">
						<td style="background-color:#ffffff;padding: 8px;">'.$DataCountry['lcountry'].'<input type="hidden" name="country_'.$data['lcountry_id'].'" value="'.$data['lcountry_id'].'"></td>
						<td style="background-color:#ffffff;padding: 4px;text-align:right;">'.GetTaxGroupToGridList($data['lcountry_id']).'</td>
						<td style="background-color:#ffffff;padding: 4px;text-align:center">-</td>
						<td style="background-color:#ffffff;padding: 4px;text-align:center">';
		if($data['lcountry_id']<>"247"){
			$result.='<a class="del_tax_grid" id="del_tax_grid_'.$DataCountry['lcountry_id'].'" rel="'.$DataCountry['lcountry_id'].'"  style="cursor:pointer">delete</a>';
		}
		$result.='</td></tr>'.delete_tax_grid_form($DataCountry['lcountry_id'],$DataCountry['lcountry']).'
					<script>
			 			$(document).ready(function(){
							$("#del_tax_grid_'.$DataCountry['lcountry_id'].'").click(function(){
								//$("#RowTaxGrid_'.$DataCountry['lcountry_id'].'").remove();
								$("#delete_tax_grid_form_'.$DataCountry['lcountry_id'].'").dialog("open");
							});
							
							del_tax_gridz_'.$DataCountry['lcountry_id'].'();
							
							function del_tax_gridz_'.$DataCountry['lcountry_id'].'(){	
						 		$("#delete_tax_grid_form_'.$DataCountry['lcountry_id'].'").dialog({
								  bgiframe: true,
								  autoOpen: false,
								  resizable: false,
								  height:170,
								  width:330,
								  modal: true,							      
								  buttons: {
									"Yes, delete this tax": function() {
									  		$("#RowTaxGrid_'.$DataCountry['lcountry_id'].'").remove();
										$(this).dialog("close");
						           
									 },Cancel: function() {
										$(this).dialog("close");
									 }
								  }
							   })
							}
						});
 					</script>';
	}
	return $result;
}

function delete_tax_grid_form($index='',$name){
   		$delete_it="<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/shopping_cart/js/jquery-ui/css/smoothness/jquery-ui-1.8.16.custom.css\" type=\"text/css\" media=\"screen\" />
		  <div id=\"delete_tax_grid_form_".$index."\" title=\"Please confirm\" style=\"display: none;\">
		  		<fieldset style=\"padding:0; border:0; margin-top:10px;\">
		    		<label for=\"caution\"><span style=\"font-size: 81.0%;\">Are you SURE you want to delete this tax \"".$name."\"?</span></label>
		    		<input type=\"hidden\" name=\"type\" value=\"".$index."\">
		  		</fieldset>
		  </div>";
   		return $delete_it;
   	}

function GetTaxGroupToGridList($idcountry){
	global $db;
	$result='';
	
 	$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
 	$query=$db->do_query($sql);
 	
 	$sql_tax_details=$db->prepare_query("SELECT * from lumonata_tax_detail where lcountry_id=%d",$idcountry);
 	$query_tax_details=$db->do_query($sql_tax_details);
 	
 	while($data=$db->fetch_array($query)){
 		$charge='0.00';
 		while ($data_tax_details=$db->fetch_array($query_tax_details)){
 			if($data_tax_details['ltax_groups_ID']==$data['ltax_groups_ID']){
 				$charge=$data_tax_details['ltax_charge'];
 				break;
 			}
 		}
 		$result.='<div style="padding:2px"><label class="" for="tax_'.$idcountry.'">'.$data['ldescription'].'</label>
					<input id="tax_'.$idcountry.'" class="input" size="5" type="text" name="tax_'.$idcountry.'_'.$data['ltax_groups_ID'].'" value="'.$charge.'" style="clear: both;color: #000000;margin: 0 3px;text-decoration: none;text-shadow: 1px 1px 0 #FFFFFF;text-align:right">
					%</div>';
 	}
 	return $result;
}
 
function GetTaxGroupToGrid($idcountry){
	global $db;
	$result='';
 	$sql=$db->prepare_query("SELECT * from lumonata_tax_groups order by ltax_groups_ID ASC");
 	$query=$db->do_query($sql);
 	while($data=$db->fetch_array($query)){
		$result.='<div style="padding:2px"><label class="" for="tax_'.$idcountry.'">'.$data['ldescription'].'</label>
					<input id="tax_'.$idcountry.'" class="input" size="5" type="text" name="tax_'.$idcountry.'_'.$data['ltax_groups_ID'].'" value="0.00" style="clear: both;color: #000000;margin: 0 3px;text-decoration: none;text-shadow: 1px 1px 0 #FFFFFF;text-align:right">
					%</div>';
 	}
 	return $result;
}

  ?>