<?php
/*
    Plugin Name: staff
    Plugin URL: 
    Description: It's plugin  is use for manage staff
    Author: Yana
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 23 August 2013
*/

/* PLUGIN NOTE :
 * lapp_name : staff
 * lkey	: staff_id, lvalue : {larticle_id}
 * lkey : staff_data, lvalue : json{order,img,name,position,desc}
 */

/*This is the first important step that you have to do.
 * Who are allowed to access the application * 
 */
add_privileges('administrator', 'staff', 'insert');
add_privileges('administrator', 'staff', 'update');
add_privileges('administrator', 'staff', 'delete');


/*Custom Icon Menu*/
function menu_css_staff(){
    return "<style type=\"text/css\">.lumonata_menu ul li.staff{
                                background:url('../lumonata-plugins/staff/images/ico-member.png') no-repeat left top;
                                }</style>";
}
/*Aply css this plugin*/
add_actions('header_elements','menu_css_staff');

/*Custom Icon Menu*/
 

/* 
 * Add sub menu under applications menu
 *  
 * */

add_main_menu(array('staff'=>'Staff'));
include ("staff_admin.php");
include ("staff_front.php");
add_actions("staff","staff_get_admin");
?>
