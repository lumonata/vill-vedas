<?php
if (isset($_POST['update_order']) && isset($_POST['state']) && $_POST['state']=='staff' ){
	staff_update_order();
	exit;
}
function staff_update_order(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];
	$app_name 	= 'staff';
 	
	foreach($items as $key=>$val){
        	$order =  $key+$start;
        	
			$q	= $db->prepare_query("SELECT * from  lumonata_additional_fields WHERE lapp_id=%d AND lapp_name=%s",$val,$app_name);    
			$r	= $db->do_query($q);
			$d	= $db->fetch_array($r);
			
			$lvalue 			= json_decode($d['lvalue'],true);
			$lvalue["order"] 	= staff_coztumize_order($order);
			$lvalue				= json_encode($lvalue);
		
			$qu = $db->prepare_query("UPDATE lumonata_additional_fields SET lvalue=%s WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s",$lvalue,$val,'staff_data',$app_name);
			
        	$db->do_query($qu);           
     }
}
function staff_coztumize_order($order){
	if (strlen($order)<2){
		$order = '000'.$order;
	}elseif((strlen($order)<3)){
		$order = '00'.$order;
	}elseif((strlen($order)<4)){
		$order = '0'.$order;
	}	
	return $order;
}
function staff_get_admin(){
        $post_id=0;

        if(is_add_new()){
			$table = "lumonata_additional_fields";
			$field = "lapp_id";
			$where = "where lapp_name='staff'";
			$post_id = staff_setCode($table,$field,$where);
            return staff_add_new($post_id) ;
        }elseif(is_edit()){ 
            return staff_edit($_GET['id']);
        }elseif(is_edit_all() && isset($_POST['select'])){
            return edit_page();
        }elseif(is_delete_all()){
                add_actions('section_title','Delete staff');
				$warning="<h1>Staff</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this staff:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these staff:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$table = "lumonata_additional_fields";
						$cond = "Where lapp_id = $val AND lapp_name='staff' AND lkey='staff_data'";
                        $d=staff_get_field_costum_table($table,$cond);
                        $warning.="<li>".$d['name']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('staff')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
                return $warning;
        }elseif(is_confirm_delete()){
                foreach($_POST['id'] as $key=>$val){
                        staff_delete_data($val);
                }
        }
        
        //Display Users Lists
        if(is_num_staff()>0){
                //add_actions('header_elements','get_javascript','jquery_ui');
                //add_actions('header_elements','get_javascript','articles_list');
               
                return staff_get_list_data();
        }else{			
        	return staff_add_new();
		}
    }
	
function staff_get_list_data($type='staff'){
	global $db;
	$app_name = 'staff';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('staff')."&page=";
/*	
	if(is_search()){ 	
		$sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
				WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s AND (A.larticle_title like %s OR AF.lvalue like %s)",
				$app_name,"%".$_POST['val_s']."%","%".$_POST['val_s']."%");
		$num_rows=count_rows($sql);	
	}else{		
        $url=get_state_url('staff')."&page=";
        $sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
				WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s ORDER BY A.larticle_id,AF.lapp_id",
				$app_name);
		$num_rows=count_rows($sql);
	}	
	
	if(is_search()){		
		 $sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
				WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s AND (A.larticle_title like %s OR AF.lvalue like %s) limit %d, %d",
				$app_name,"%".$_POST['val_s']."%","%".$_POST['val_s']."%",$limit,$viewed); 		
	}else{		
		$sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
				WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s ORDER BY A.larticle_id,AF.lapp_id limit %d, %d",
				$app_name,$limit,$viewed);
	}
*/	
	
	$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue",
			'staff_data',$app_name);		
	$num_rows=count_rows($sql);
		
	$sql=$db->prepare_query("select AF.* from lumonata_additional_fields AS AF 
			WHERE AF.lkey=%s AND AF.lapp_name=%s ORDER BY AF.lvalue limit %d, %d",
			'staff_data',$app_name,$limit,$viewed);
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("staff")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = 'http://'.SITE_URL.'/staff-ajax/';
	$list.="<h1>Staff</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('staff')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".staff_search_box($url_ajax,'list_item','state=staff&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('staff')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"staff\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>
						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />
								
								<!--<div class=\"pages_title\" style=\"width:25%;\">Article</div>-->
								
								<div class=\"title_category\">Avatar</div>
								<div class=\"pages_title\" style=\"width:25%;\">Name</div>		
								<div class=\"pages_title\" style='max-width:25%;' >Position</div>																
							</div>
							<div id=\"list_item\">";
	$list.=staff_list($result,$start_order);
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	//$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-admin/javascript/articles_list.js" ></script>';
	$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/staff/js/list.js" ></script>';
	$list .='<script type="text/javascript" language="javascript">
					$(document).ready( function(){
        
        $("input[name=select_all]").removeAttr("checked");
        $("input[name=select[]]").each(function(){
            $("input[name=select[]]").removeAttr("checked");
        });
        
        $("input[name=select_all]").click(function(){
                var checked_status = this.checked;
                
                $(".select").each(function(){
                        this.checked = checked_status;
                        if(checked_status){ //checked all chekcbox if select all checked
                            $("input[name=edit]").removeClass("btn_edit_disable");
                            $("input[name=edit]").addClass("btn_edit_enable");
                            $("input[name=edit]").removeAttr("disabled");
                            
                            $("input[name=delete]").removeClass("btn_delete_disable");
                            $("input[name=delete]").addClass("btn_delete_enable");
                            $("input[name=delete]").removeAttr("disabled");
                            
                            $("input[name=publish]").removeClass("btn_publish_disable");
                            $("input[name=publish]").addClass("btn_publish_enable");
                            $("input[name=publish]").removeAttr("disabled");
                            
                            $("input[name=unpublish]").removeClass("btn_save_changes_disable");
                            $("input[name=unpublish]").addClass("btn_save_changes_enable");
                            $("input[name=unpublish]").removeAttr("disabled");
                        }else{
                            $("input[name=edit]").removeClass("btn_edit_enable");
                            $("input[name=edit]").addClass("btn_edit_disable");
                            $("input[name=edit]").attr("disabled", "disabled");
                            
                            $("input[name=delete]").removeClass("btn_delete_enable");
                            $("input[name=delete]").addClass("btn_delete_disable");
                            $("input[name=delete]").attr("disabled", "disabled");
                            
                            $("input[name=publish]").removeClass("btn_publish_enable");
                            $("input[name=publish]").addClass("btn_publish_disable");
                            $("input[name=publish]").attr("disabled", "disabled");
                            
                            $("input[name=unpublish]").removeClass("btn_save_changes_enable");
                            $("input[name=unpublish]").addClass("btn_save_changes_disable");
                            $("input[name=unpublish]").attr("disabled", "disabled");
                            
                        }
                });
        });        
       
});
			</script>';
		
	add_actions('section_title','Staff');
	return $list;
}

function staff_list($result,$i=1){
        global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        while($d=$db->fetch_array($result)){
        		$article_id = get_additional_field($d['lapp_id'], 'staff_id',$d['lapp_name']);
				$name 	 	= get_article_title($article_id); 
				
				$ihd = get_image_staff($d['lapp_id']);
				$image= $ihd['img'];				
				
				if (!empty($image)){
					$image_url = 'http://'.SITE_URL.$image;					
					$image = '<img src="http://'.site_url().'/app/tb/tb.php?w=50&h=40&src='.$image_url.'"/>';
							
			
					$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['lapp_id']."\">
	                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['lapp_id']."\" style='margin-left:13px;' />
	                                
									
								<!--	<div class=\"pages_title\" style=\"width:25%;\">".$name."</div> -->
									
									<div class=\"title_category\">".$image."</div>
									<div class=\"pages_title\" style=\"width:25%;\">".$ihd['name']."</div>
									<div class=\"pages_title\" style='max-width:25%;'>".$ihd['position']."</div>
									
									
	                                <div class=\"the_navigation_list\">
	                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['lapp_id']."\">
	                                                <a href=\"".get_state_url('staff')."&prc=edit&id=".$article_id."&app_id=".$d['lapp_id']."\">Edit</a> |
	                                                <a href=\"javascript:;\" rel=\"delete_".$d['lapp_id']."\">Delete</a>
	
	                                        </div>
	                                </div>
	                                <script type=\"text/javascript\" language=\"javascript\">
	                                        $('#theitem_".$d['lapp_id']."').mouseover(function(){
	                                                $('#the_navigation_".$d['lapp_id']."').show();
	                                        });
	                                        $('#theitem_".$d['lapp_id']."').mouseout(function(){
	                                                $('#the_navigation_".$d['lapp_id']."').hide();
	                                        });
	                                </script>
	                                
	                        </div>";
	                $msg="Are you sure to delete ".$ihd['name']."?";
					$url = 'http://'.SITE_URL.'/staff-ajax/';
	                add_actions('admin_tail','staff_delete_confirmation_box',$d['lapp_id'],$msg,$url,"theitem_".$d['lapp_id'],'state=staff&prc=delete&id='.$d['lapp_id']);                
	                $i++;
                }	
                
        }
        return $list;
    }

function staff_add_new($post_id=0){
	staff_set_template();
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="	
	<li>".button("button=add_new",get_state_url('staff')."&prc=add_new")."</li>	
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url('staff'))."</li>";
		
	
	add_actions('section_title','Staff - Add New');
	add_variable('title_Form','Staff');
	add_variable('staff_option_article', staff_option_article());
	add_variable('staff_existing_image', staff_existing_image());
	

	if (isset($_POST['save_changes'])){
		//validation
		if ($_POST['article_id'][$i]==''){			
			$error = '<div class="error_red">Please choose article.</div>';
			add_variable('error',$error);
		}elseif (isset($_FILES['image']['name'][$i]) && ($_FILES['image']['name'][$i]=='') && (empty($_POST['exist_img'][$i]))){			
			$error = '<div class="error_red">Please choose image.</div>';
			add_variable('error',$error);
		}else{
			if (staff_save_add()){
				$error = '<div class="error_green">Add staff has save succesfully.</div>';
				add_variable('error',$error);
				
				add_variable('val_title','');
			}else{
				$error = '<div class="error_red">Something wrong, please try againt.</div>';
				add_variable('error',$error);
			}
		}
		
	}
	
	
	/* B Show Data*/
		$thaUrl = 'http://'.SITE_URL.'/staff-ajax/';
		add_variable('thaUrl',$thaUrl);
		add_variable('post_id',$post_id);
		add_variable('index_sp',0);
		add_variable('val_order',0);
	/* E Show Data*/
	
	add_variable('i',$i);
	add_variable('button',$button);
	parse_template('loopPage','lPage',false);
	 
	return staff_return_template();
	
}

function staff_edit($post_id=0){
		global $db;
        global $thepost;
        $index=0;
        $button="";
        staff_set_template();
		
            $button.="
			<li>".button("button=add_new",get_state_url('staff')."&prc=add_new")."</li>
           	<li>".button("button=save_changes&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('staff'))."</li>";

		$app_id = $_GET['app_id'];
		
		
        //set the page Title
        add_actions('section_title','Staff - Edit');
      
			//echo "is_single_edit";
				add_variable('title_Form','Edit staff');
				/* B Show Record */
				
				if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){
					//echo "Edit";
					$i = 0;
					$id = $_GET['id'];					
					
					if (staff_save_edit($i,$id)){
						$error = '<div class="error_green">Edit staff has succesfully.</div>';
						add_variable('error',$error);
						
					}else{
						$error = '<div class="error_red">Something wrong, please try again.</div>';
						add_variable('error',$error);
					}
				}
				
				$table = "lumonata_additional_fields";
				$cond = "Where lapp_id='".$app_id."' AND lkey='staff_data' AND lapp_name='staff'";
				$d = staff_get_field_costum_table($table,$cond);
				
				add_variable('staff_option_article',staff_option_article($post_id));
				add_variable('readonly','readonly="readonly"');
				add_variable('post_id',$post_id);
				add_variable('app_id',$app_id);
				
	
				$thaUrl = 'http://'.SITE_URL.'/staff-ajax/';
				add_variable('thaUrl',$thaUrl);
                add_variable('val_title',$d['name']);
                add_variable('val_subtitle',$d['position']);				
				add_variable('val_url',$d['desc']);				
				add_variable('val_order',$d['order']);
				$avatar = '';
				if (!empty($d['img'])){
					$url_avatar = 'http://'.SITE_URL.$d['img'];
					$avatar = '<img class="exist-img" src="http://'.site_url().'/app/tb/tb.php?w=50&h=50&src='.$url_avatar.'">';
				}
				add_variable('avatar','Current image : <br>'.$avatar);
				
				add_variable('staff_existing_image',staff_existing_image($index,$d['img']));	
				/*E Show Record  */

				add_variable('i',$index);
                parse_template('loopPage','lPage',false);
           
        
        
       
        add_variable('button',$button);
        return staff_return_template();
    }
		
function staff_save_add(){
	global $db;
	$i			=0;		
	$app_id 	= staff_setCode("lumonata_additional_fields","lapp_id", "where lapp_name='staff'");	
	$art_id		= $_POST['article_id'][$i];
	$name 		= $_POST['title'][$i];
	$position	= $_POST['sub_title'][$i];
	$desc 		= $_POST['url'][$i];
	$order		= $_POST['order'][$i];
	$order		= staff_coztumize_order($order);
	//image
	$img_name = '';
	if (isset($_FILES['image']['name'][$i]) && !empty($_FILES['image']['name'][$i])){
		$img = staff_upload_image($i,$art_id,$name);
		$img_name = $img['img_name'];
	}elseif (isset($_POST['exist_img'][$i]) && !empty($_POST['exist_img'][$i])){			
		$img_name = $_POST['exist_img'][$i];
	}else{
		$error= 'Please choose an image';
	}
	if ($img_name==''){
		return false;
	}
	//additional filed
	$app_name 	= 'staff';
	$key1		= 'staff_id';
	$key2		= 'staff_data';
	$val1		= $art_id;
	$val2		= array("order"=>$order,"post_id"=>$art_id,"img"=>$img_name,"name"=>$name,"position"=>$position,"desc"=>$desc);
	$val2		= json_encode($val2);	
	
	add_additional_field($app_id,$key1,$val1,$app_name);
	add_additional_field($app_id,$key2,$val2,$app_name);	
	return true;
		
}
		
function staff_save_edit($i,$article_id){
	global $db;
	$i=0;
	
	$app_id 	= $_POST['app_id'][$i];	
	$art_id		= $_POST['article_id'][$i];
	$name 		= $_POST['title'][$i];
	$position	= $_POST['sub_title'][$i];
	$desc 		= $_POST['url'][$i];
	$order		= $_POST['order'][$i];
	$order		= staff_coztumize_order($order);
	//image
	$img_name = '';
	if (isset($_FILES['image']['name'][$i]) && !empty($_FILES['image']['name'][$i])){
		$img = staff_upload_image($i,$art_id,$name);
		$img_name = $img['img_name'];
	}elseif (isset($_POST['exist_img'][$i]) && !empty($_POST['exist_img'][$i])){			
		$img_name = $_POST['exist_img'][$i];
	}else{
		$error= 'Please choose an image';
	}
	//additional filed
	$app_name 	= 'staff';
	$key1		= 'staff_id';
	$key2		= 'staff_data';
	$val1		= $art_id;
	$staff_data = array("order"=>$order,"post_id"=>$art_id,"img"=>$img_name,"name"=>$name,"position"=>$position,"desc"=>$desc);	
	$val2		= json_encode($staff_data);	
	
	edit_additional_field($app_id,$key1,$val1,$app_name);
	edit_additional_field($app_id,$key2,$val2,$app_name);	
	return true;
}

function staff_save_rule($i,$article_id,$app_name='staff',$action='add'){
	global $db;
	
	$categories = $_POST['categories'][$i];
	if ($action=='add'){
		$sql=$db->prepare_query("INSERT INTO lumonata_rule_relationship(
						lapp_id,
						lrule_id,
						lorder_id
						) Values (
						%d,%d,%d
						)",$article_id,$categories,0);
		$r=$db->do_query($sql);
	}else if ($action='edit'){
		$sql=$db->prepare_query("UPDATE lumonata_rule_relationship SET
						lrule_id=%d
						WHERE lapp_id=%d
						",$categories,$article_id);
		$r=$db->do_query($sql);
	
	}
}

function staff_delete_data($app_id){
	global $db;
	
	$query = $db->prepare_query("Delete From lumonata_additional_fields Where lapp_id = %d AND lapp_name=%s",$app_id,'staff');
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}

/*
function delete_header_image_size_price($article_id){
	global $db;
	delete_image_header_image($article_id);
	$query = $db->prepare_query("Delete From  lumonata_additional_fields Where lapp_id = %d",$article_id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}

function delete_image_header_image($article_id){
	global $db;
	$query = $db->prepare_query("Select * From lumonata_attachment Where larticle_id = %d",$article_id);
	$result = $db->do_query($query);
	$d = $db->fetch_array($result);
	$attach_loc = $d['lattach_loc'];
	if (!empty($attach_loc)){
		$file_name_1 = $attach_loc;
		$file_name_2 = $d['lattach_loc_thumb'];
		
		$destination1=PLUGINS_PATH."/slide/files/".$file_name_1;
		$destination2=PLUGINS_PATH."/slide/files/".$file_name_2;
		
		delete_file($destination1);
		delete_file($destination2);
		
		$query = $db->prepare_query("Delete From  lumonata_attachment Where larticle_id = %d",$article_id);
		$result = $db->do_query($query);
	}
	
}
*/	
function staff_upload_image($i,$article_id,$name){
	 global $db;
  	 $thealert="";
	 $i = 0;
	 $file_name = $_FILES['image']['name'][$i];
	 $file_size = $_FILES['image']['size'][$i];
	 $file_type = $_FILES['image']['type'][$i];
	 $file_source = $_FILES['image']['tmp_name'][$i];

	 if (!empty($file_name)){
		//delete_image_header_image($article_id);
	 
		 if(is_allow_file_size($file_size)){
			if(is_allow_file_type($file_type,'image')){
				 if (empty($name)){
					$fix_file_name=time();
					$name = time();
					$time = '';
				 }else{
					$fix_file_name=file_name_filter($name);
					$name = $name;
					$time = '-'.time();
				 }
				 $file_ext=file_name_filter($file_name,true);

				 $file_name_1=$fix_file_name.$time.$file_ext;
				 $file_name_2=$fix_file_name.$time.'-thumbnail'.$file_ext;

				 $destination1=PLUGINS_PATH."/staff/files/".$file_name_1;
				 $destination2=PLUGINS_PATH."/staff/files/".$file_name_2;
				 $width = 425; 
				 $height = 300;
				 upload_resize($file_source, $destination2, $file_type, $width,$height);
				 upload($file_source,$destination1);

				 
				$upload_date=date('Y-m-d H:i:s',time());
				$date_last_update=date('Y-m-d H:i:s',time());
				
				$alt_text = '';
				$caption = '';
				$title=rem_slashes($name);
				$alt_text=rem_slashes($alt_text);
				$caption=rem_slashes($caption);
				$mime_type=$file_type;
				//echo
				$file_name_1 = "/lumonata-plugins/staff/files/".$file_name_1; //complete with path
				$file_name_2 = "/lumonata-plugins/staff/files/".$file_name_2; //complete with path
				$sql=$db->prepare_query("INSERT INTO lumonata_attachment(
						larticle_id,
												
						lattach_loc,
						lattach_loc_thumb,
						ltitle,
						
						lalt_text,
						lcaption,
						upload_date,
						date_last_update,
						
						mime_type,
						lorder)
                        VALUES(
						%d,
						%s,%s,%s,
						%s,%s,%s,%s,
						%s,%d)",
						$article_id,
						$file_name_1,$file_name_2,$title,
						$alt_text,$caption,$upload_date,$date_last_update,
						$mime_type,1);
    
				if(reset_order_id("lumonata_attachment")){
					
					if ($db->do_query($sql)){
						$upload_img = array('status'=>'true','img_name'=>$file_name_1,'alert'=>'');
					}
				}
		   }else{
				$thealert="<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
				$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
		   }
	   }else{
				$thealert="<div class=\"alert_yellow\">The maximum file size is 2MB</div>";
				$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
						 
	   }
	   
	}else{
		$thealert = 'Image source is empty';
		$upload_img=array('status'=>'failed','img_name'=>'','alert'=>$thealert);
	}
	
	return $upload_img;
}

function staff_get_image($article_id,$status=''){
	global $db;
	$sql = $db->prepare_query("Select * From lumonata_attachment Where larticle_id =%d",$article_id);
	$r = $db->do_query($sql);
	$j = $db->num_rows($r);
	if (!empty($j)){
		$d = $db->fetch_array($r);
		if (empty($status)){
			$image = $d['lattach_loc'];
		}else if ($status=='thumb'){
			$image = $d['lattach_loc_thumb'];
		}else if ($status=='medium'){
			$image = $d['lattach_loc_medium'];
		}else if ($status=='large'){
			$image = $d['lattach_loc_large'];
		}  
		
		return $image;
	}else{
		return "";
	}
}

function staff_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".'staff'."/template_admin.html",'template_header_image');
        //set block
        add_block('loopPage','lPage','template_header_image');
        add_block('pageAddNew','pAddNew','template_header_image');
}

function staff_return_template($loop=false){
       
        parse_template('pageAddNew','pAddNew',$loop);
        return return_template('template_header_image');
}

function staff_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\">
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'post_search_staff',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}

function staff_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'post_delete_staff',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}

function is_num_staff(){
	global $db;
	$sql = $db->prepare_query("SELECT * From lumonata_additional_fields Where lapp_name=%s AND lkey=%s",'staff','staff_data');
	$num_rows=count_rows($sql);
	return $num_rows;
}

function staff_ajax(){
	global $db;
	$app_name = 'staff'; 
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_delete_staff'){
		$id = $_POST['val_id'];
		if (staff_delete_data($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_search_staff'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }	 
	    	 
	    /*
	    $sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
				WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s AND (A.larticle_title like %s OR AF.lvalue like %s)",
				'staff',"%".$_POST['val_s']."%","%".$_POST['val_s']."%");	
		*/	 

	    /*
	     $sql=$db->prepare_query("SELECT AF.* from lumonata_additional_fields AS AF 
				WHERE AF.lapp_name=%s AND AF.lvalue like %s GROUP BY AF.lvalue",
				'staff',"%".$_POST['val_s']."%");	   
	    
	     */
	
		$sql=$db->prepare_query("select A.*, AF.* from lumonata_articles AS A, lumonata_additional_fields AS AF 
					WHERE  A.larticle_id=AF.lvalue AND AF.lapp_name=%s ORDER BY A.larticle_id,AF.lapp_id",
					$app_name);   
	     
	    $r=$db->do_query($sql);
        if($db->num_rows($r) > 0){			
			echo staff_list($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='post_add_size_price'){
		$index = $_POST['val_index'];
		$index_sp = $_POST['val_index_sp'];
		$select_size = $_POST['val_select_size'];
		$price = $_POST['val_price'];

		$size_price_list = '
		<div class="list_item clearfix" id="size_price_list_'.$index.'_'.$index_sp.'" style="background:#eee; height:auto; padding:5px;">
			<div class="avatar block_size">'.$select_size.'</div>
			<div class="avatar block_price">'.$price.'</div>
			<div class="avatar block_button_delete">
				<input type="hidden" name="size_list['.$index.'][]" value="'.$select_size.'" class="medium_textbox size_list_'.$index.'" id="size_list_'.$index.'_'.$index_sp.'" />
				<input type="hidden" name="price_list['.$index.'][]" value="'.$price.'" class="medium_textbox" id="price_list_'.$index.'_'.$index_sp.'" />
				
				<input class="btn_delete_enable button_delete_sp" name="delete" value="Delete" type="button"
				rel="'.$index.'_'.$index_sp.'">
			</div>
		</div>';
		
		echo $size_price_list;
	}
	
}

function staff_option_article($selected_id=82){
	global $db;	
	$option = '<option value="">Select article</option>';
	$q = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s OR larticle_type=%s ORDER BY lorder",'pages','articles');
	$r = $db->do_query($q);
	
	while ($d=$db->fetch_array($r)){
		if ($d['larticle_id']==$selected_id){
			$option .= '<option selected value="'.$d['larticle_id'].'">'.$d['larticle_title'].'</option>';
		}else{
			$option .= '<option value="'.$d['larticle_id'].'">'.$d['larticle_title'].'</option>';
		}
		
	}
	
	return $option;
}
/*-----------ADDITIONAL FUNCTION, hi=staff -------------*/
function staff_existing_image($i=0,$selected_filename=''){
	global $db;
	$q = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s",'staff','staff_data');	
	$r = $db->do_query($q);
	$img = '';
	$prev_img = '';
	
	$arr_img = array();
	while ($d=$db->fetch_array($r)){
		$arr = json_decode($d['lvalue'],true);
		
		if (in_array($arr['img'],$arr_img)){
			
		}else{
			array_push($arr_img, $arr['img']);
		}		
	}	
	
	$j=0;
	foreach ($arr_img as $key=>$val){
		$filename = ROOT_PATH.$val;
		if (file_exists($filename)){
			$checked = '';
			$selected = '';
			if ($val==$selected_filename){
				$selected = 'selected';
				$checked = 'checked';
			}
			$image = 'http://'.site_url().$val;
			$img .= '<div class="a-ei">';
			$img .= '<img class="exist-img '.$selected.'" id="img_'.$j.'" src="http://'.site_url().'/app/tb/tb.php?w=50&h=50&src='.$image.'">';
			$img .= '<input '.$checked.' class="radio_exist_img" rel="'.$j.'" type="radio" name="exist_img['.$i.']" value="'.$val.'">';
			$img .= '</div>';
			$j++;
		}
	}
	return $img; 
}
function get_image_staff($app_id,$app_name='staff',$key='staff_data'){
	global $db;
	if (isset($_POST['jpKEY']) && ($_POST['jpKEY']=='post_search_staff')){
		$key_search = $_POST['val_s'];
		$query = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id=%d AND (lvalue like %s)",$app_name,$key,$app_id,'%'.$key_search.'%');
	}else{
		$query = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id=%d",$app_name,$key,$app_id);
	}	
	$result = $db->do_query($query);
	$data = $db->fetch_array($result);
	if (isset($data['lvalue']) && (!empty($data['lvalue']))){
		$arr = json_decode($data['lvalue'],true);
		return $arr;
	}else{
		return false;
	}
}
function staff_setCode($table,$field,$where=''){
	global $db;
	//echo 
	$query = $db->prepare_query("SELECT MAX(".$field.") from ".$table." ".$where);
	$result = $db->do_query($query);
	$data = $db->fetch_array($result);
	if($data[0]==NULL){
		return "1";	
	}else{
		$cnt=$data[0]+1;
		return $cnt;
	}
}
function staff_get_field_costum_table($table,$condition){
	//defined database class to global variable
	global $db;
	//echo 
	$sql=$db->prepare_query("SELECT *  
				FROM $table 
				$condition
				");
	$r=$db->do_query($sql);
	$d=$db->fetch_array($r);
	$arr = json_decode($d['lvalue'],true);
	
	return $arr;
}

add_actions("staff-ajax_page","staff_ajax");
?>