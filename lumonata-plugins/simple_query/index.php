<?php 
/*
    Plugin Name: Simple Query
    Plugin URL: 
    Description: It's plugin  is use get data tabel
    Author: Adi
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 15 September 2014
*/
function data_tabel($t,$w,$f='result_only'){
	global $db;
	$str = $db->prepare_query("select * from $t $w");
	$result = $db->do_query($str);
	if($f=='result_only') return $result;
	else if($f=='array') return $db->fetch_array($result);
	else if($f=='num_row') return $db->num_rows($result);
}
?>