<?php
/*
    Plugin Name: Social Media
   	Plugin URL: http://lumonata.com/
    Description: This plugin is use social media.
    Author: AM
    Author URL: http://lumonata.com/
    Version: 1.0.1
    
*/

add_privileges('administrator', 'social_media', 'insert');
add_privileges('administrator', 'social_media', 'update');
add_privileges('administrator', 'social_media', 'delete');

add_apps_menu(array('social_media'=>'Social Media'));

add_actions("social_media","plugin_social_media_admin");

function plugin_social_media_admin(){
	global $db;
	$post_by = $_COOKIE['user_id'];
	
	add_actions("header_elements","social_media_admin_CSS");
	set_template(PLUGINS_PATH."/social_media/template_admin.html",'the_social_media_admin');
	add_block('Bsocial_media_admin','bcua','the_social_media_admin');
	add_actions('section_title','Social Media');
	
	add_variable('app_title','Social Media');
	add_variable('save_changes_botton',save_changes_botton());
	
	$sqlc=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE larticle_type=%s"
			     ,'social_media');
	
	$rc=$db->do_query($sqlc);
	$nc=$db->num_rows($rc);
	if (empty($nc)){
		$qi=$db->prepare_query("Insert Into lumonata_articles
		(larticle_title,larticle_status,larticle_type,lsef,lorder,
		lpost_by,lpost_date,lupdated_by,ldlu) 
		values 
		(%s,%s,%s,%s,%d,
		%d,%s,%d,%s)",
		'social media','publish','social_media','social_media',1,
		$post_by,date("Y-m-d H:i:s"),$post_by,date("Y-m-d H:i:s"));
		$ri=$db->do_query($qi);
	}
	
	if (isset($_POST['save_changes'])){
		$article_id = $_POST['article_id'];
		$qd=$db->prepare_query("Delete From lumonata_additional_fields Where lapp_name=%s",'social_media');
		$rd=$db->do_query($qd);
		for($i=0;$i<count($_POST['name']);$i++){
			$name = $_POST['name'][$i];
			$title = $_POST['title'][$i];
			$url = $_POST['url'][$i];
			$logo_sef = $_POST['logo_sef'][$i];
			
     		$file_name = $_FILES['logo']['name'][$i];
         	$file_size = $_FILES['logo']['size'][$i];
         	$file_type = $_FILES['logo']['type'][$i];
         	$file_source = $_FILES['logo']['tmp_name'][$i];
         	
         	if (!empty($file_name)){
         		$file_ext=file_name_filter($file_name,true);
         		$logo_sef = $name.'-'.time().$file_ext;
         		$destination=PLUGINS_PATH."/social_media/images/".$logo_sef;
         		upload($file_source,$destination);
         	}
			
			add_additional_field($article_id,$name.'_title',$title,'social_media');
			add_additional_field($article_id,$name.'_url',$url,'social_media');
			add_additional_field($article_id,$name.'_logo',$logo_sef,'social_media');
		}
	}
	
	$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'social_media','social_media');
	
	$r=$db->do_query($sql);
	$f=$db->fetch_array($r);
	$article_id=$f['larticle_id'];
	add_variable("article_id",$article_id);
	
	$qaf=$db->prepare_query("Select * From lumonata_additional_fields Where lapp_id=%d And lapp_name=%s",$article_id,'social_media');
	$raf=$db->do_query($qaf);
	while ($daf=$db->fetch_array($raf)){
		$key=$daf['lkey'];
		add_variable("val_$key",$daf['lvalue']);
		
		$logo = 'http://'.SITE_URL.'/lumonata-plugins/social_media/images/'.$daf['lvalue'];
		$logo = '<img src="'.$logo.'" style="margin:8px 0;" /> <br />';
		$val_logo = "val_".$key."_img";
		add_variable("$val_logo",$logo);
		
		
	}	
	
	parse_template('Bsocial_media_admin','bcua',true);
	return return_template('the_social_media_admin');
		
}

function get_social_media($val){
	global $db;
	$sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'social_media','social_media');
	
	$r=$db->do_query($sql);
	$f=$db->fetch_array($r);
	$post_id=$f['larticle_id'];
	
	$url = $val."_url";
	$title = $val."_title";
	$logo = $val."_logo";
	$article_type = 'social_media';
	
	$url = get_additional_field( $post_id, $url, $article_type);
	$title = get_additional_field( $post_id, $title, $article_type);
	$logo = get_additional_field( $post_id, $logo, $article_type);

	$logo = 'http://'.SITE_URL.'/lumonata-plugins/social_media/images/'.$logo;
	$result = '
		<a href="'.$url.'" title="'.$title.'">
    		<img src="'.$logo.'" alt="'.$title.'" />
    	</a>
		';
	return $result;
}

function social_media_admin_CSS(){
	$text = '<style>
			.textbox {width: 90% !important;}
			.textarea_button { display:none;}
			.list_edit {border: none;}
			.validate_box span {color: #333333;}
			</style>
			';
	return $text;
}

function social_media_ajax(){
	session_start();
	add_actions('is_use_ajax', true);
	//echo "social_media_ajax";
	global $db;
	if (isset($_POST['pKEY']) and $_POST['pKEY']=='tesss'){
		echo "Tes";	
	}
}

add_actions('social-media-ajax_page', 'social_media_ajax');



function front_social($status=""){
	global $db;
	//set template
	if($status=="2nd"){
		set_template(PLUGINS_PATH."/social_media/template_social2nd.html",'template_social2nd');
	}else{
		set_template(PLUGINS_PATH."/social_media/template_social.html",'template_social');
	}
	//set block
	
	//print_r($_POST);
	if($status=="2nd"){
    	add_block('Bsocial2nd_block','b_social2nd','template_social2nd');
	}else{
		add_block('Bsocial_block','b_social','template_social');
	}
    
   $sql=$db->prepare_query("SELECT *
			     FROM lumonata_articles
			     WHERE lsef=%s and larticle_type=%s"
			     ,'social_media','social_media');
	
	$r=$db->do_query($sql);
	$f=$db->fetch_array($r);
	$post_id=$f['larticle_id'];
	
	$array = array("tripadvisor","instagram","twitter","facebook");
	foreach ($array as $key => $val){
		$url = $val."_url";
		$title = $val."_title";
		$logo = $val."_logo";
		$article_type = 'social_media';
		
		$url = get_additional_field( $post_id, $url, $article_type);
		$title = get_additional_field( $post_id, $title, $article_type);
		$logo = get_additional_field( $post_id, $logo, $article_type);
		add_variable($val."_title",$title);
		add_variable($val."_url",$url);
	}
	
	
	if($status=="2nd"){
    	parse_template('Bsocial2nd_block','b_social2nd',false);
    	return return_template('template_social2nd');	
	}else{
		parse_template('Bsocial_block','b_social',false);
    	return return_template('template_social');	
	}
    
}
?>