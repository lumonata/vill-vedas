<?php 
/*
    Plugin Name: Booking Engine
    Plugin URL: 
    Description: It's plugin  is use for Booking Engine
    Author: Adi
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 8 Mey 2014
*/

function menu_css_booking_engine(){
    return "<style type=\"text/css\">
    .lumonata_menu ul li.booking_engine{
	background:url('../lumonata-plugins/booking_engine/images/ico-themes.png') no-repeat left top;
	}
	a#booking_engine{
    background: url('../lumonata-plugins/booking_engine/images/ico-arrow.png') no-repeat scroll right center transparent;
    margin: 0 5px 0 0;
}
	</style>";
}
add_actions('header_elements','menu_css_booking_engine');

add_privileges('administrator', 'booking_engine', 'insert');
add_privileges('administrator', 'booking_engine', 'update');
add_privileges('administrator', 'booking_engine', 'delete');
/*
add_privileges('administrator', 'additional_service', 'insert');
add_privileges('administrator', 'additional_service', 'update');
add_privileges('administrator', 'additional_service', 'delete');*/


add_main_menu(array('booking_engine'=>'Booking Engine'));
add_sub_menu('booking_engine',
array(
	'allotment'=>'Availability Calendar',
	'season'=>'Season Period',
	'rate'=>'Rate',
	'villa-reservation'=>'Villa Reservation',
	'program-reservation'=>'Program Reservation',
	'program-package'=>'Program Package',
	'surcharge'=>'Surcharge',
	'early-bird'=>'Early Bird Promo',
	'discount-promo'=>'Discount Promo',
	'special-offer'=>'Special Offer',
	'additional-service'=>'Additional Service'
));


function is_booking_engine_menu(){
	$state = $_GET['state'];
	if($state=='booking_engine')return true;
	else return false;
}

function go_to_booking_engine(){
	$sub = $_GET['sub'];
	$url = "http://".SITE_URL."/booking-engine/$sub/";
	header("location:".$url);
}

?>