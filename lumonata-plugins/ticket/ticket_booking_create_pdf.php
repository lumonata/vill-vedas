<?php
require_once('../../lumonata_config.php');
require_once(ROOT_PATH.'/lumonata-functions/user.php');
require_once(ROOT_PATH.'/lumonata-functions/settings.php');
require_once(ROOT_PATH.'/lumonata-functions/fpdf17/fpdf.php');

if(!is_user_logged()){
 	exit('You have to login to access this page!');
}

$filter_booking = array('rid'=>'','sid'=>'','status'=>'','date_start'=>'','date_end'=>'');
if (ticket_booking_is_filter_view()){		
	if (isset($_POST['filter'])){			
		$filter_booking = array('rid'=>$_POST['rid'],'sid'=>$_POST['sid'],'status'=>$_POST['status'],'date_start'=>$_POST['date_start'],'date_end'=>$_POST['date_end']);
	}elseif (isset($_GET['filter'])){
		$filter_booking = array('rid'=>$_GET['rid'],'sid'=>$_GET['sid'],'status'=>$_GET['status'],'date_start'=>$_GET['date_start'],'date_end'=>$_GET['date_end']);
	}		
}
?>
<?php 
class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('images/logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Booking Report : '.date('Y-M-d'),0,0,'C');
    // Line break
    $this->Ln(20);
}
// Simple table
function BasicTable($header, $data)
{
	$w = array(35, 35, 35, 35,25,25,12,12,12,12,35);
	// Header
	$i=0;
	foreach($header as $col){
		if($i>=4 && $i<=5){
			$this->Cell(25,7,$col,1);		
		}elseif ($i>=6 && $i<=10){
			$this->Cell(12,7,$col,1);	
		}else{
			$this->Cell(35,7,$col,1);	
		}		
		$i++;
	}
	$this->Ln();
	// Data
	foreach($data as $row)
	{
		$i=0;
		foreach($row as $col){
			if($i>=4 && $i<=5){
				$this->Cell(25,6,$col,1);	
			}elseif ($i>=6 && $i<=10){
				$this->Cell(12,6,$col,1);
			}else{
				$this->Cell(35,6,$col,1);
			}
			$i++;
		}
		$this->Ln();
	}
	// Closing line
	$this->Cell(array_sum($w),0,'','T');
}

// Better table
function ImprovedTable($header, $data)
{
	// Column widths
	$w = array(40, 35, 40, 45);
	// Header
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1,0,'C');
	$this->Ln();
	// Data
	foreach($data as $row)
	{
		$this->Cell($w[0],6,$row[0],'LR');
		$this->Cell($w[1],6,$row[1],'LR');
		$this->Cell($w[2],6,$row[2],'LR',0,'R');
		$this->Cell($w[3],6,$row[3],'LR',0,'R');
		$this->Ln();
	}
	// Closing line
	$this->Cell(array_sum($w),0,'','T');
}

// Colored table
function FancyTable($header, $data)
{
	// Colors, line width and bold font
	$this->SetFillColor(255,0,0);
	$this->SetTextColor(255);
	$this->SetDrawColor(128,0,0);
	$this->SetLineWidth(.3);
	$this->SetFont('','B');
	// Header
	$w = array(40, 35, 40, 45);
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	$this->Ln();
	// Color and font restoration
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetFont('');
	// Data
	$fill = false;
	foreach($data as $row)
	{
		$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
		$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
		$this->Cell($w[2],6,$row[2],'LR',0,'R',$fill);
		$this->Cell($w[3],6,$row[3],'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	// Closing line
	$this->Cell(array_sum($w),0,'','T');
}
}
?>
<?php 
$pdf = new PDF();
$pdf->SetMargins(2,10,2);
$pdf->SetTitle('Booking Report : '.date('Y-M-d'));
// Column headings
$header = array('Booked By', 'Ticket No.', 'Depart From', 'Destination','Date', 'Time','Trip','Status','Adult','Child','Infant','Total');

// Data
$data = get_data_booking();

$pdf->SetFont('Arial','',11);
$pdf->AddPage('L');
$pdf->BasicTable($header,$data);
/*
$pdf->AddPage();
$pdf->ImprovedTable($header,$data);
$pdf->AddPage();
$pdf->FancyTable($header,$data);
*/
$pdf->Output('report-'.time().'.pdf','D');
?>
<?php 	
function get_data_booking(){
	global $db,$filter_booking;
	//order by
	if (isset($_GET['orderby'])){
		$field_orderby 	= $_GET['orderby'];
		$order_by 		= $field_orderby;
		$order 			= $_GET['order'];
	}else{
		$field_orderby 	= '';
		$order_by 		= 'A.bid';
		$order			= 'asc';
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;	
	
	
	if (ticket_booking_is_filter_view()){
		$where_filter = '';		
		if ($filter_booking['rid']!=''){
			$where_filter .= ' AND B.rid="'.$filter_booking['rid'].'"';
		}
		if ($filter_booking['sid']!=''){
			$where_filter .= ' AND B.sid="'.$filter_booking['sid'].'"';
		}
		if ($filter_booking['status']!=''){
			$where_filter .= ' AND A.status="'.$filter_booking['status'].'"';
		}
		
		if ($filter_booking['date_start']!='' && $filter_booking['date_end']!=''){
			//$where_filter .= ' AND B.date="'.$filter_booking['date_start'].'"';
			$where_filter .= 'AND B.date >="'.$filter_booking['date_start'].'"';
			$where_filter .= 'AND B.date <="'.$filter_booking['date_end'].'"';
		}else{
			if ($filter_booking['date_start']!=''){
				$where_filter .= 'AND B.date >="'.$filter_booking['date_start'].'"';		
			}
			if ($filter_booking['date_end']!=''){
				$where_filter .= 'AND B.date <="'.$filter_booking['date_end'].'"';		
			}
		}	
		
		
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.bid=B.bid AND A.booked_by=C.bbid) $where_filter
				GROUP BY B.bid
				ORDER BY $order_by
				");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.bid=B.bid AND A.booked_by=C.bbid) $where_filter
				GROUP BY B.bid
        		ORDER BY $order_by $order
        		limit %d, %d",$limit,$viewed);		
	/*
	}elseif(is_search()){ 	
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid) AND 
				(
					no_ticket like %s OR date like %s OR fname like %s 
				)
				GROUP BY B.bid
				ORDER BY $order_by $order
				",
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
		$num_rows=count_rows($sql);	
	*/
	}else{ 
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid)
				GROUP BY B.bid
				ORDER BY $order_by $order
				");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid)
				GROUP BY B.bid
        		ORDER BY $order_by $order
        		limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	while ($d=$db->fetch_array($result)){
		$book_by = $d['fname']==NULL?$d['booked_by']:$d['fname'];
		$data[] = array($book_by,$d['no_ticket'],$d['rfrom'],$d['rto'],$d['date'],$d['stime_departure'],$d['type'],$d['status'],$d['num_adult'],$d['num_child'],$d['num_infant'],$d['total_paid']);
	}
	return $data;
}

//just function
function ticket_booking_is_filter_view(){
	if (isset($_POST['filter']) || isset($_GET['filter'])){		
		return true;	
	}else{		
		return false;
	}		
}
?>
