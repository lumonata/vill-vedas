<?php
/*
    Plugin Name: ticket
    Plugin URL: 
    Description: It's plugin  is use for Pacha Express tickecting
    Author: Yana
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 9 September 2013
*/

/* PLUGIN NOTE :
 * 
 * 
 */

date_default_timezone_set("UTC");

/*This is the first important step that you have to do.
 * Who are allowed to access the application * 
 */
add_privileges('administrator', 'ticket', 'insert');
add_privileges('administrator', 'ticket', 'update');
add_privileges('administrator', 'ticket', 'delete');

add_privileges('administrator', 'calendar', 'insert');
add_privileges('administrator', 'calendar', 'update');
add_privileges('administrator', 'calendar', 'delete');

add_privileges('administrator', 'route', 'insert');
add_privileges('administrator', 'route', 'update');
add_privileges('administrator', 'route', 'delete');

add_privileges('administrator', 'schedule', 'insert');
add_privileges('administrator', 'schedule', 'update');
add_privileges('administrator', 'schedule', 'delete');

add_privileges('administrator', 'special_rate', 'insert');
add_privileges('administrator', 'special_rate', 'update');
add_privileges('administrator', 'special_rate', 'delete');

add_privileges('administrator', 'setting', 'insert');
add_privileges('administrator', 'setting', 'update');
add_privileges('administrator', 'setting', 'delete');

add_privileges('administrator', 'booking', 'insert');
add_privileges('administrator', 'booking', 'update');
add_privileges('administrator', 'booking', 'delete');

add_privileges('administrator', 'boat', 'insert');
add_privileges('administrator', 'boat', 'update');
add_privileges('administrator', 'boat', 'delete');

add_privileges('administrator', 'promo', 'insert');
add_privileges('administrator', 'promo', 'update');
add_privileges('administrator', 'promo', 'delete');

/*Custom Icon Menu*/
function ticket_css_menu(){
    return "<style type=\"text/css\">.lumonata_menu ul li.ticket{
                                background:url('../lumonata-plugins/ticket/images/icon-ticket.png') no-repeat left top;
                                background-size:24px 24px;
                                }</style>";
}
/*Aply css this plugin*/
add_actions('header_elements','ticket_css_menu');
/*Custom Icon Menu*/

add_main_menu(array('ticket'=>'Booking Engine'));
add_sub_menu('ticket',array('boat'=>'Boat','booking'=>'Booking','calendar'=>'Calendar','route'=>'Route & Price','schedule'=>'Schedule','promo'=>'Promo','special_rate'=>'Special Rate','setting'=>'Settings'));

//global var 
$pacha_node 		= array('a'=>'Amed','b'=>'Gili Trawangan','c'=>'Gili Meno','d'=>'Gili Air','e'=>'Bangsal');
$pacha_node_number 	= array(0=>'a',1=>'b',2=>'c',3=>'d',4=>'e');
$pacha_node_code	= array('a'=>0,'b'=>1,'c'=>2,'d'=>3,'e'=>4);

//admin
//update : search & promo
include ("ticket_availability_class.php");
include ("ticket_functions.php");
include ("ticket_setting.php");
include ("ticket_calendar.php");
include ("ticket_route.php");
include ("ticket_schedule.php");
include ("ticket_special_rate.php");
include ("ticket_paypal.php");
include ("ticket_booking.php");
include ("ticket_boat.php");
include ("ticket_promo.php");

//front
//include ("ticket_front_node.php");
include ("ticket_front.php");
?>
