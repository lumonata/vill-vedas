<?php
add_actions("ticket-front-ajax_page","ticket_front_ajax");
function ticket_front_index(){	
 	if (is_check_ticket()){
 		if (is_valid_param_search()){
 			return ticket_availability_result();
 		}else{
 			return 'parameter search tidak valid!';
 		}    	
    }elseif (is_submit_booking()){    	
    	return ticket_front_process_booking();
    }else{    	
    	//return ticket_availability_form_search();
    }
}
function ticket_availability_form_search(){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_form_search.html",'fs');
    //set block       
    add_block('fSearch','bform','fs');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');
    add_variable('option_destination_from', ticket_front_option_destination_from());    
    //parse
	parse_template('fSearch','bform',false);   
	//return      
    return return_template('fs');    
}
function ticket_availability_result(){
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Check ticket availability - '.web_title();      
	
	//var
	$trip_type 	= $_POST['trip_type'];
	$from		= $_POST['from'];
	$to			= $_POST['to'];
	$dep_date	= $_POST['dep_date'];
	$ret_date	= $trip_type=='1' ? $_POST['ret_date'] : '-';
	$num_adult	= $_POST['adult'];
	$num_child	= $_POST['child']=='' ? 0 :$_POST['child'];
	$num_infant	= $_POST['infant']=='' ? 0 :$_POST['infant'];
	
	$num_passenger = $num_adult + $num_child; //apakah tiap orang @ 1 ticket???
	
	if ($trip_type=='1'){		
		//go trip
		$rid_go			= ticket_front_route_id($from,$to);
		$status_go 		= is_ticket_available($rid_go,$dep_date,$num_adult,$num_child);
		
		//return trip
		$rid_return		= ticket_front_route_id($to,$from); 
		$status_return 	= is_ticket_available($rid_return,$ret_date,$num_adult,$num_child);
		$trip_summary 	= ticket_front_trip_summary($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
		if ($status_go && $status_return){
			$step_1  = ticket_front_step_1_return_trip($trip_type,$from,$to,$dep_date,'',$num_adult,$num_child,$num_infant,'departure');
			$step_1 .= ticket_front_step_1_return_trip($trip_type,$to,$from,$ret_date,'',$num_adult,$num_child,$num_infant,'return');
			$step_1 .= ticket_front_step_1_return_trip_pricing($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,'departure');
			/*
			$step_1  = ticket_front_step_1($trip_type,$from,$to,$dep_date,'',$num_adult,$num_child,$num_infant,'departure');
			$step_1 .= ticket_front_step_1($trip_type,$to,$from,$ret_date,'',$num_adult,$num_child,$num_infant,'return');
			*/
		/*
		}elseif ($status_go){
			$step_1  = ticket_front_step_1($trip_type,$from,$to,$dep_date,'',$num_adult,$num_child,$num_infant,'departure');
			$step_1 .= '#Return schedule is not available';
		}elseif($status_return){			
			$step_1  = ticket_front_step_1($trip_type,$from,$to,$ret_date,'',$num_adult,$num_child,$num_infant,'return');
			$step_1 .= '#Departure schedule is not available';
		*/
		}else{
			return ticket_availability_result_no($trip_summary); 
		}		
		
		$step_1			= $step_1;
		$step_2			= ticket_front_step_2($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
		$step_3			= ticket_front_step_3($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
		$step_4			= ticket_front_step_4($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
		return ticket_availability_result_yes($trip_type,$trip_summary,$step_1,$step_2,$step_3,$step_4);		
	}else{
		//one way trip
		$rid			= ticket_front_route_id($from,$to);
		$status			= is_ticket_available($rid,$dep_date,$num_adult,$num_child);			
		$trip_summary 	= ticket_front_trip_summary($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
		if ($status==true){				
			$step_1			= ticket_front_step_1($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);		
			$step_2			= ticket_front_step_2($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_3			= ticket_front_step_3($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_4			= ticket_front_step_4($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			return ticket_availability_result_yes($trip_type,$trip_summary,$step_1,$step_2,$step_3,$step_4);
		}else{			
			return ticket_availability_result_no($trip_summary);
		}
	}		
}
function ticket_availability_result_yes($trip_type,$trip_summary,$step_1,$step_2,$step_3,$step_4){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result.html",'rs');
    //set block       
    add_block('resultSearch','bResult','rs');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    add_variable('trip_summary',$trip_summary);
    add_variable('step_1',$step_1);
    add_variable('step_2',$step_2);
    add_variable('step_3',$step_3);
    add_variable('step_4',$step_4);
    
    //parse
	parse_template('resultSearch','bResult',false);   
	//return      
    return return_template('rs');    
}
function ticket_availability_result_no($summary=''){
	$html= '<div class="container" style="padding-top:20px;">
				<style>
					.widget-search{
						display:block !important;
					}
				</style>
				<div style="margin: 20px 10px;padding:10px;">			
					'.$summary.'
				</div>
				<p style="margin: 20px 10px;padding:10px;border:1px solid #ccc;" class="error">
					No schedule available!
				</p>
			</div>';
	return $html;
}
function ticket_front_process_booking(){
	global $actions;
	$actions->action['meta_title']['func_name'][0] = 'Reviews & Payment - '.web_title();      
	
	//var default
	$trip_type 	= $_POST['trip_type'];
	$from		= $_POST['from'];
	$to			= $_POST['to'];
	$dep_date	= $_POST['dep_date'];
	$ret_date	= $_POST['ret_date'];
	$num_adult	= $_POST['adult'];
	$num_child	= $_POST['child'];
	$num_infant	= $_POST['infant'];	
	
	//var result
	if (is_valid_data_booking()){
		$save_booking = ticket_front_save_booking();
		if (is_array($save_booking) && isset($save_booking['status']) && ($save_booking['status']==true)){
			
			return ticket_front_reviews($save_booking);
		}else{
			print_r($save_booking);
			return ticket_front_save_booking_failed();
		}
	}
}
function ticket_front_save_booking(){
	global $db;	
	//var from post
	$trip_type 	= $_POST['trip_type'];
	$from		= $_POST['from'];
	$to			= $_POST['to'];
	$dep_date	= $_POST['dep_date'];
	$ret_date	= $_POST['ret_date'];
	$num_adult	= $_POST['adult'];
	$num_child	= $_POST['child'];
	$num_infant	= $_POST['infant'];	
	
	$sid		= $_POST['schedule_id_departure'];//departure
	$rid		= ticket_front_get_schedule_by_id($sid,'rid');
	$total_dep	= ticket_calculate_total($trip_type,$rid,$sid,$num_adult,$num_child,$num_infant);
	
	$boat_id	= ticket_front_get_schedule_by_id($sid,'sboat');
	$boat		= ticket_boat_get_detail_by_id($boat_id,'bname');
	
	//B got special price?
	$got_sp		= is_get_special_rate($rid,$sid,$dep_date);
	$srid		= '0'; //special_rate_id
	if ($got_sp!=false){
		$dep_get_sp = true;
		$data_sp	= ticket_get_data_rows($got_sp);
		$srid		= $data_sp['srid'];
		$total_dep	= ticket_calculate_total_apply_special_price($trip_type,$num_adult,$num_child,$num_infant,$data_sp);
	}
	//E got special price?
	
	/* =============ini adalah cara lama yaitu harga pergi dan return dibedakan==================
	if ($trip_type=='1'){
		
		$sid_ret	= $_POST['schedule_id_return']; //return
		$rid_ret	= ticket_front_route_id($to, $from);//return
		
		$total_ret	= ticket_calculate_total($trip_type,$rid_ret,$sid_ret,$num_adult,$num_child,$num_infant);
		
		//B got special price?
		$got_sp_ret		= is_get_special_rate($rid_ret,$sid_ret,$ret_date);
		$srid_ret		= '';
		if ($got_sp_ret!=false){
			$ret_get_sp = true;
			$data_sp_ret= ticket_get_data_rows($got_sp_ret);
			$srid_ret	= $data_sp_ret['srid'];
			$total_ret	= ticket_calculate_total_apply_special_price($trip_type,$num_adult,$num_child,$num_infant,$data_sp_ret);
		}
		//E got special price?
		
		$total		= $total_dep + $total_ret;
			
	}else{
		$total		= $total_dep;
	}
	//============================================================================================	*/	
		
	$total		= $total_dep;
	
	$num_pass 	= $num_adult + $num_child + $num_infant;	
	
	$bby 		= generate_booking_by_id();
	$bid 		= get_next_increment_id('ticket_booking');		
	$no_ticket 	= generate_ticket_no($trip_type);
	$status		= 'pp';	
	
	//cek lagi sebelum simpan, apakah ticket msh tersedia atau sudah terbooking terlebih dahulu oleh applicant lain
	//untuk menghindari applicant melakukan booking pd waktu yg bersamaan
	if ($trip_type=='1'){		
		$sid_dep = $_POST['schedule_id_departure'];
		$rid_dep = ticket_front_get_schedule_by_id($sid_dep,'rid');	
		$date_dep= $dep_date;
			
		$sid_ret = $_POST['schedule_id_return'];
		$rid_ret = ticket_front_get_schedule_by_id($sid_ret,'rid');
		$date_ret= $ret_date;
		
		if ((is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true) &&
			(is_ticket_over_quota($rid_ret,$sid_ret,$date_ret,$num_adult,$num_child,$num_infant)==true)
			){			
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure & return is over quota');
			return $data;
		}elseif (is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure is over quota');
			return $data;
		}elseif(is_ticket_over_quota($rid_ret,$sid_ret,$date_ret,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket return is over quota');
			return data;
		}
	}elseif($trip_type=='0'){
		$sid_dep = $_POST['schedule_id_departure'];
		$rid_dep = ticket_front_get_schedule_by_id($sid_dep,'rid');	
		$date_dep= $dep_date;
		if (is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure is over quota');
			return $data;
		}
	}
	
	//insert ticket_booking
	$q = $db->prepare_query("INSERT INTO ticket_booking 
			(bid, type, no_ticket, 
			boat, num_passenger, total, 
			status, created_date, created_by, 
			booked_by, dlu,srid) 
			VALUES 
			(%d, %d, %s, 
			%s, %d, %d, 
			%s, %d, %s, 
			%s, %d,%d)",
			$bid,$trip_type,$no_ticket,
			$boat,$num_pass,$total,
			$status,time(),$bby,
			$bby,time(),$srid
		 );
	$r = $db->do_query($q);
	
	//insert detail booking
	if ($r){		
		//detail booking departure
		$schedule_detail = ticket_front_get_schedule_by_id($sid);
		$route_detail	 = ticket_front_get_route_by_id($schedule_detail['rid']);
		
		if ($trip_type=='1'){	
			$price_adult	= $route_detail['rprice_2way'];
			$price_child	= $route_detail['rprice_2way_child'];			
			//got special price?
			if (isset($dep_get_sp) && $dep_get_sp==true){
				$price_adult = $data_sp['srprice_2way'];
				$price_child = $data_sp['srprice_2way_child'];
			}		
		}elseif ($trip_type=='0'){	
			$price_adult	= $route_detail['rprice_1way'];
			$price_child	= $route_detail['rprice_1way_child'];			
			//got special price?
			if (isset($dep_get_sp) && $dep_get_sp==true){
				$price_adult = $data_sp['srprice_1way'];
				$price_child = $data_sp['srprice_1way_child'];
			}			
		}
		
		$total_adult	= $num_adult *  $price_adult;
		$total_child	= $num_child *  $price_child;
		$total_infant	= 0;
		$total_dep		= $total_adult + $total_child + $total_infant;
		
		if ($trip_type==1){			
			$q0 = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`,
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'departure',$bid,$rid,
				$from,$to,$sid,$schedule_detail['sparent'],
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['dep_date'],
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				$price_adult,$price_child,0,
				$total_adult,$total_child,$total_infant,
				$total_dep,$status,time(),
				$bby,time(),$srid				
				);
				
			//detail booking return
			$schedule_detail = ticket_front_get_schedule_by_id($sid_ret);
			$route_detail	 = ticket_front_get_route_by_id($schedule_detail['rid']);
			$price_adult	 = $route_detail['rprice_1way'];
			$price_child	 = $route_detail['rprice_1way_child'];
			
			//B got special price?
			if (isset($ret_get_sp) && $ret_get_sp==true){
				$price_adult = $data_sp_ret['srprice_1way'];
				$price_child = $data_sp_ret['srprice_1way_child'];
			}
			//E got special price?
			
			$total_adult	= $num_adult *  $price_adult;
			$total_child	= $num_child *  $price_child;
			$total_infant	= 0;
			$total_dep		= $total_adult + $total_child + $total_infant;
			$q1 = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`, 
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'return',$bid,$rid_ret,//rid_ret
				$to,$from,$sid_ret,$schedule_detail['sparent'],//sid_ret, from & to di tukar
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['ret_date'], //ret_date
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				0,0,0,
				0,0,0,
				0,$status,time(),
				$bby,time(),''				
				);
				
				//save
				$r0 = $db->do_query($q0);
				$r1 = $db->do_query($q1);				
				$insert_booking_detail = $r0 && $r1? true: false;
		}else{			
			$q = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`, 
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'departure',$bid,$rid,
				$from,$to,$sid,$schedule_detail['sparent'],
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['dep_date'],
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				$price_adult,$price_child,0,
				$total_adult,$total_child,$total_infant,
				$total_dep,$status,time(),
				$bby,time(),$srid				
				);
			$r = $db->do_query($q);
			$insert_booking_detail = $r? true: false;
		}
	}
	
	//insert passengger
	if ($insert_booking_detail){		
		for ($i=0;$i<$num_pass;$i++){
			$q = $db->prepare_query("INSERT INTO ticket_booking_passenger 
				(`bid`, `type`,`title`, 
				`fname`, `lname`, `country`, 
				`status`, `created_date`, `created_by`, 
				`dlu`) 
				VALUES
				(%d,%s,%s,
				%s,%s,%s,
				%s,%d,%s,
				%d)",
				$bid,$_POST['pass_type'][$i],'',
				$_POST['pass_fname'][$i],'',$_POST['pass_country'][$i],
				1,time(),$bby,
				time()
				);
			$r = $db->do_query($q);
		}
	}
	
	//insert aplicant/pemesan
	if ($insert_booking_detail){
		$q = $db->prepare_query("INSERT INTO ticket_booking_by 
				(`bbid`, `title`, `fname`, 
				`lname`, `address`, `country`, 
				`phone`, `email`, `created_date`, 
				`created_by`, `dlu`) 
				VALUES
				(%s,%s,%s,
				%s,%s,%s,
				%s,%s,%d,
				%s,%d)",
				$bby,$_POST['app_title'],$_POST['app_fname'],
				'',$_POST['app_address'],$_POST['app_country'],
				$_POST['app_phone'],$_POST['app_email'],time(),
				$bby,time()
				);
		$r = $db->do_query($q);
	}	
	
	//return data
	if ($insert_booking_detail){
		$data = array(
				'status'=>true,
				'booking_id'=>$bid,
				'booking_by'=>$bby,
				'no_ticket'=>$no_ticket,
				'trip_type'=>$trip_type,
				'from'=>$from,
				'to'=>$to,
				'dep_date'=>$dep_date,
				'ret_date'=>$ret_date,
				'total'=>$total
				);
	}else{
		//roll back all data that has inserted [belum]
		$data = array('status'=>false,'note'=>'Save data booking is failed');
	}
	return $data;
}
function ticket_front_reviews($data){	
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_reviews_data_booking.html",'frdb');
    //set block       
    add_block('reviewsBooking','bRbooking','frdb');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');      
    
    $trip_type = $data['trip_type']=='0' ? 'One way' : 'Return trip';        
    add_variable('no_ticket', $data['no_ticket']);
    add_variable('route', $data['from'].'-'.$data['to']);
    add_variable('trip_type',$trip_type);
    add_variable('dep_date',$data['dep_date']);
    add_variable('ret_date',$data['ret_date']);
  
    add_variable('row_data', ticket_front_get_list_passengger($data));
    add_variable('total', number_format($data['total'],0,',','.'));
    
    //paypal
    $paypal_link 	= ticket_setting_get_value('paypal-url');
	$paypal_account = ticket_setting_get_value('paypal-account');
	$total_usd 		= $data['total'] / ticket_setting_get_value('usd-rate'); 
	
	add_variable('paypal_url_action',$paypal_link);
    add_variable('paypal_account',$paypal_account);
    add_variable('total_in_usd',$total_usd);
    add_variable('paypal_currency','USD');
    add_variable('paypal_url_return','http://'.site_url().'/ticket-succesfully/'.$data['no_ticket']); //page to say : thanks to booking with us
    add_variable('paypal_url_cancel','http://'.site_url().'/ticket-unsuccesfully/'.$data['no_ticket']); //page to delete data booking if cancel
    add_variable('paypal_url_notify','http://'.site_url().'/ticket-paid-notify/'.$data['no_ticket']); //page to cover action if payment is succes (update status ticket to paid)
    
    //parse
	parse_template('reviewsBooking','bRbooking',false);   
	//return      
    return return_template('frdb');    
	
}
function ticket_front_save_booking_failed(){
	return 'ticket_front_save_booking_failed()';
}
//just function
function is_check_ticket(){
	if (isset($_POST['check_ticket'])){
		return true;	
	}else{
		return false;
	}
}
function is_submit_booking(){
	if (isset($_POST['submit_booking'])){
		return true;	
	}else{
		return false;
	}
}
function is_valid_data_booking(){
	
	return true;
}
function is_valid_param_search(){
	$not_required = array('child','infant');
	foreach ($_POST as $index => $val){		
		if ($_POST['trip_type']=='0'){
			if ($index!='ret_date'){
				if ($val==''){	
					if (!in_array($index, $not_required))		
					return false;
				}	
			}
		}else{
			if ($val==''){	
				if (!in_array($index, $not_required))		
				return false;
			}		
		}
		
	}
	return true;	
}
function ticket_front_get_list_passengger($data){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * FROM ticket_booking_detail WHERE bid=%d order by bdid ASC",$data['booking_id']);
	$r = $db->do_query($q);
	$price_per_adult = 0;
	$price_per_child = 0;
	while ($detail_booking = $db->fetch_array($r)){
		$price_per_adult = $price_per_adult + $detail_booking['price_per_adult']; //cover jg utk pulang pergi
		$price_per_child = $price_per_child + $detail_booking['price_per_child']; //cover jg utk pulang pergi
	}
	
	$q = $db->prepare_query("SELECT * from ticket_booking_passenger WHERE bid=%d ORDER BY bpid ASC",$data['booking_id']);
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		if ($d['type']=='adult'){
			$rate = $price_per_adult;
		}elseif ($d['type']=='child'){
			$rate = $price_per_child;
		}else{
			$rate = 'free';
		}
		$html .= '<tr><td>'.$d['fname'].'</td><td>'.$d['type'].'</td><td>'.number_format($rate,0,',','.').'</td></tr>';
	}
	return $html;
}
function ticket_front_get_schedule_by_id($sid,$field=''){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE sid=%d",$sid);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	if ($field!=''){
		if (isset($d[$field])) {
			return $d[$field];
		}else{
			return $d;
		}	
	}
	return $d;
}
function ticket_front_get_route_by_id($rid){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_route WHERE rid=%d",$rid);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	return $d;
}
function ticket_calculate_total($trip_type,$rid,$sid,$num_adult,$num_child=0,$num_infant=0){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_schedule, ticket_route WHERE ticket_schedule.rid=ticket_route.rid AND ticket_schedule.sid=%d",$sid);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	if ($trip_type=='1'){
		$total = ($num_adult * $d['rprice_2way']) + ($num_child * $d['rprice_2way_child']); //return price
	}elseif($trip_type=='0'){
		$total = ($num_adult * $d['rprice_1way']) + ($num_child * $d['rprice_1way_child']); //departure price only
	}else{
		return false;
	}		
	return $total;
}
function ticket_calculate_total_apply_special_price($trip_type,$num_adult,$num_child,$num_infant,$data_sp){ 
	if (is_array($data_sp) && !empty($data_sp)){
		if ($trip_type=='1'){
			$total = ($num_adult * $data_sp['srprice_2way']) + ($num_child * $data_sp['srprice_2way_child']); //return price
		}elseif($trip_type=='0'){
			$total = ($num_adult * $data_sp['srprice_1way']) + ($num_child * $data_sp['srprice_1way_child']); //departure price only
		}else{
			return false;
		}			
		return $total;
	}else{
		return false;
	}
}
function is_ticket_available($rid,$date,$num_adult,$num_child){
	global $db,$pacha_node;	
	//jadwal sore hny berlaku 1 juni - 15 september
	$start_date		= date('Y',strtotime($date)).'-06-01';
	$end_date		= date('Y',strtotime($date)).'-09-15';
	$date_from_user = $date;
	$is_inrange = check_date_in_range($start_date, $end_date, $date_from_user);
	
	if ($is_inrange){
		$q		= $db->prepare_query("SELECT * FROM ticket_schedule WHERE rid=%d AND status=%d",$rid,1); //ambil jadwal seluruhnya, pagi dan sore
	}else{		
		$q		= $db->prepare_query("SELECT * FROM ticket_schedule WHERE rid=%d AND status=%d AND sparent=%d",$rid,1,0); //ambil jadwal pagi saja
	}
	 
	$r		= $db->do_query($q);
	$num	= $db->num_rows($r);
	if ($num==0){
		return false;
	}else{
		while ($d=$db->fetch_array($r)){
			$sid 			= $d['sid'];			
			$allotment		= $d['sallotment']==0? ticket_route_get_allotment($rid) : $d['sallotment'];
			$nsb 			= ticket_front_num_schedule_booked($rid,$sid,$date);		
			
			$ticket = new ticketAvailability($rid, $sid, $date,$num_adult,$num_child);
			if ($ticket->is_available()){
				return true;
			}else{
				return false;
			}
			/*
			if (node_is_ticket_available($rid,$sid,$date,$num_pessenger)){
				return true;
			}
			*/
			/*
			$num_available	= $allotment - $nsb; 
			if ($num_available>=$num_pessenger){
				return true;
			}
			*/
		}
		return false;
	}	
}
function ticket_front_trip_summary($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_summary.html",'rsum');
    //set block       
    add_block('resultSummary','bSummary','rsum');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    //prepare var
    if ($trip_type=='0' || $ret_date==''){
    	$ret_date = '-';	    	
    }
   
    add_variable('trip_type', $trip_type);
    add_variable('trip_from', $from);
    add_variable('trip_to', $to);
    add_variable('route_name', $from.' - '.$to); 
    add_variable('departure_date', $dep_date); 
    add_variable('return_date', $ret_date); 
    
    add_variable('num_adult', $num_adult);
    add_variable('num_child', $num_child); 
    add_variable('num_infant',$num_infant);    
   
    //parse
	parse_template('resultSummary','bSummary',false);   
	//return      
    return return_template('rsum');    
}
function ticket_front_step_1($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,$schedule_type='departure'){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_1_onewaytrip.html",'st1_'.$schedule_type);
    //set block       
    add_block('stepOne','bStep','st1_'.$schedule_type);		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    //prepare var    
    $csr = '';
    if ($schedule_type=='return'){
    	 $csr = ticket_front_get_checkbox_skip_return();
    }
    
    add_variable('schedule_title', ucfirst($schedule_type));
    add_variable('schedule_date', $dep_date); 
    add_variable('table_id',$schedule_type); 
        
    add_variable('num_adult', $num_adult);
    add_variable('num_child', $num_child); 
    add_variable('num_infant',$num_infant); 
	
    add_variable('checkbox_skip_return', $csr);
    
    add_variable('row_data', ticket_front_schedule_list($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,$schedule_type));
   
    //parse
	parse_template('stepOne','bStep',false);   
	//return      
    return return_template('st1_'.$schedule_type);    
}
function ticket_front_step_1_return_trip($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,$schedule_type='departure'){	
	global $db;
	$html = '';	
	$rid	= ticket_front_route_id($from, $to);
	//ambil jadwal pagi saja
	$q		= $db->prepare_query("SELECT R.*,S.* FROM ticket_route as R,ticket_schedule as S WHERE R.rid=S.rid AND S.rid=%d AND S.status=%d AND S.sparent=%d ORDER BY sparent ASC",$rid,1,0);
		
	//start validasi jadwal sore :jadwal sore hny berlaku 1 juni - 15 september		
	$start_date		= date('Y',strtotime($dep_date)).'-06-01';
	$end_date		= date('Y',strtotime($dep_date)).'-09-15';
	$date_from_user = $dep_date;
	$is_inrange 	= check_date_in_range($start_date, $end_date, $date_from_user);
	if ($is_inrange){//ambil semua jadwal, pagi dan sore		
		$q		= $db->prepare_query("SELECT R.*,S.* FROM ticket_route as R,ticket_schedule as S WHERE R.rid=S.rid AND S.rid=%d AND S.status=%d ORDER BY sparent ASC",$rid,1);
	}
	//end validasi jadwal sore	
	
	$r		= $db->do_query($q);
	$num	= $db->num_rows($r);
	if ($num==0) return false;
	
	//list schedule
	$list_sc = '';
    while ($d=$db->fetch_array($r)){    
    	$ticket = new ticketAvailability($rid, $d['sid'], $dep_date,$num_adult,$num_child);
		if ($ticket->is_available()){	
    		$list_sc .= '<tr>
    					<td>
    						<input type="radio" value="'.$d['sid'].'" name="schedule_id_'.$schedule_type.'" id="rsid'.$d['sid'].'">
    						<label for="rsid'.$d['sid'].'"><span></span>&nbsp;&nbsp;</label>
    					</td>
    					<td>'.$d['sname'].'</td>
    					<td>'.$dep_date.'</td>
    					<td>'.$d['stime_departure'].'</td>
    				 </tr>';   
		}
    }//endwhile
	
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_1_returntrip.html",'st1_'.$schedule_type);
    //set block     	
    add_block('stepOne','bStep','st1_'.$schedule_type);		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    //prepare var      
    add_variable('schedule_title', ucfirst($schedule_type));
    add_variable('schedule_date', $dep_date); 
    add_variable('table_id',$schedule_type);       
    add_variable('row_data', $list_sc);
    //parse
	parse_template('stepOne','bStep',false);   
	//return      
    return return_template('st1_'.$schedule_type);    
}
function ticket_front_step_1_return_trip_pricing($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,$schedule_type='departure'){
	global $db;
	$html = '';	
	$rid	= ticket_front_route_id($from, $to);
	$q		= $db->prepare_query("SELECT R.*,S.* FROM ticket_route as R,ticket_schedule as S WHERE R.rid=S.rid AND S.rid=%d AND S.status=%d ORDER BY sparent ASC",$rid,1);
	$r		= $db->do_query($q);
	$num	= $db->num_rows($r);
	if ($num==0) return false;
	while($d=$db->fetch_array($r)){
			$sid 			= $d['sid'];						
			$ticket = new ticketAvailability($rid, $sid, $dep_date,$num_adult,$num_child);
			if ($ticket->is_available()){
				$price_per_adult = $d['rprice_2way'];
				$price_per_child = $d['rprice_2way_child']==0? 'free' : $d['rprice_2way_child'];
				$total_adult	 = ($num_adult*$d['rprice_2way']); 
				$total_child	 = ($num_child*$d['rprice_2way_child']); 			
				$total			 = $total_adult + $total_child;
				
				$total_adult	 = number_format($total_adult,0,',','.');
				$total_child	 = number_format($total_child,0,',','.');
				//if got special price
				$got_sp		= is_get_special_rate($rid,$sid,$dep_date);
				if ($got_sp!=false){
					$d_sp 			= ticket_get_data_rows($got_sp);					
					$sp_rate_adult  = $d_sp['rprice_2way'];
					$sp_rate_child  = $d_sp['rprice_2way_child']==0? 'free' : $d_sp['rprice_2way_child']; 				
					$sp_total_adult = ($num_adult*$d_sp['rprice_2way']); 
					$sp_total_child = ($num_child*$d_sp['rprice_2way_child']); 	
					$total			=  $sp_total_adult + $sp_total_child;
					
					$price_per_adult 	= '<span style="text-decoration: line-through;">'.number_format($price_per_adult,0,',','.').'</span><br>'.number_format($sp_rate_adult,0,'0','.');
					$price_per_child 	= '<span style="text-decoration: line-through;">'.number_format($price_per_child,0,',','.').'</span><br>'.number_format($sp_rate_child,0,',','.');
					$total_adult		= '<span style="text-decoration: line-through;">'.number_format($total_adult,0,',','.').'</span><br>'.number_format($sp_total_adult,0,',','.');
					$total_child		= '<span style="text-decoration: line-through;">'.number_format($total_child,0,',','.').'</span><br>'.number_format($sp_total_child,0,',','.');
				}					
			}
	}//endwhile
	
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_1_returntrip_pricing.html",'st1_pricing'.$schedule_type);
    //set block     	
    add_block('tbPricing','bStep','st1_pricing'.$schedule_type);		
    //add variable default
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    //add variable
    add_variable('num_adult', $num_adult);
    add_variable('num_child', $num_child); 
    add_variable('num_infant',$num_infant); 
    
    add_variable('price_per_adult', $price_per_adult);
    add_variable('price_per_child', $price_per_child);     
    
    add_variable('total_adult', $total_adult);
    add_variable('total_child', $total_child); 
    
    add_variable('total', number_format($total,0,',','.'));
    
	//parse
	parse_template('tbPricing','bStep',false);   
	//return      
    return return_template('st1_pricing'.$schedule_type);   
}
function ticket_front_step_2($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_2.html",'st2');
    //set block       
    add_block('stepTwo','bStep','st2');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');    
    
    //var
    $option_title 	= ticket_front_option_title();
    $option_country = ticket_front_option_country();
    $app_fname 		= '';
    $app_address 	= '';
    $app_phone 		= '';
    $app_email		= '';
    
    if (isset($_POST['app_fname'])){
    	$option_title 	= ticket_front_option_title();
    	$option_country = ticket_front_option_country();
    	$app_fname 		= $_POST['app_fname'];
    	$app_address 	= $_POST['app_address'];
    	$app_phone 		= $_POST['app_phone'];
    	$app_email		= $_POST['app_email'];
    }
    
    add_variable('option_title', $option_title);
    add_variable('app_fname', $app_fname); 
    add_variable('app_address',$app_address); 
    add_variable('option_country',$option_country);   
    add_variable('app_phone',$app_phone);   
    add_variable('app_email',$app_email);    
   
    //parse
	parse_template('stepTwo','bStep',false);   
	//return      
    return return_template('st2');    
}
function ticket_front_step_3($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_3.html",'st3');
    //set block       
    add_block('stepThree','bStep','st3');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');   
    
   	add_variable('row_data', ticket_front_form_passenger($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant));
   
    //parse
	parse_template('stepThree','bStep',false);   
	//return      
    return return_template('st3');    
}
function ticket_front_step_4($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant){
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/front_search_result_step_4.html",'st4');
    //set block       
    add_block('stepFour','bStep','st4');		
    //add variable
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');      
	
    if (isset($_POST['add_pickup'])){
		add_variable('add_pickup', $_POST['add_pickup']);
		add_variable('add_transfer', $_POST['add_transfer']);
	}
   
    //parse
	parse_template('stepFour','bStep',false);   
	//return      
    return return_template('st4');    
}
function ticket_front_schedule_list($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,$schedule_type){
	global $db;
	$html = '';		
	$rid	= ticket_front_route_id($from, $to);
	//ambil jadwal pagi saja
	$q		= $db->prepare_query("SELECT R.*,S.* FROM ticket_route as R,ticket_schedule as S WHERE R.rid=S.rid AND S.rid=%d AND S.status=%d AND S.sparent=%d ORDER BY sparent ASC",$rid,1,0);
		
	//start validasi jadwal sore :jadwal sore hny berlaku 1 juni - 15 september		
	$start_date		= date('Y',strtotime($dep_date)).'-06-01';
	$end_date		= date('Y',strtotime($dep_date)).'-09-15';
	$date_from_user = $dep_date;
	$is_inrange 	= check_date_in_range($start_date, $end_date, $date_from_user);
	if ($is_inrange){
		$q		= $db->prepare_query("SELECT R.*,S.* FROM ticket_route as R,ticket_schedule as S WHERE R.rid=S.rid AND S.rid=%d AND S.status=%d ORDER BY sparent ASC",$rid,1);
	}
	//end validasi jadwal sore	
	
	$r		= $db->do_query($q);
	$num	= $db->num_rows($r);
	if ($num==0){
		return false;
	}else{
		//prepare var
		$num_passenger 		= $num_adult + $num_child;
		$status_required 	= $schedule_type=='departure' ? 'required="required"' : '';
		$status_required	= '';
		while ($d=$db->fetch_array($r)){
			$sid 			= $d['sid'];
			$allotment		= $d['sallotment']==0?$d['rallotment']:$d['sallotment'];
			$nsb 			= ticket_front_num_schedule_booked($sid,$dep_date);
			$num_available	= $allotment - $nsb; 
			
			/*if ($num_available>=$num_passenger){*/
			$ticket = new ticketAvailability($rid, $sid, $dep_date,$num_adult,$num_child);
			if ($ticket->is_available()){
				$rate_child = $d['rprice_1way_child']==0? 'free' : number_format($d['rprice_1way_child'],0,',','.'); 
				$total 		= ($num_adult*$d['rprice_1way']) + ($num_child*$d['rprice_1way_child']);
				
				//B got special price or not
				$got_sp		= is_get_special_rate($rid,$sid,$dep_date);
				if ($got_sp!=false){
					$d_sp 	= ticket_get_data_rows($got_sp);
					$rate_child_sp 	= $d_sp['srprice_1way_child']==0? 'free' : number_format($d_sp['srprice_1way_child'],0,',','.');
					$total_sp		= ($num_adult*$d_sp['srprice_1way']) + ($num_child*$d_sp['srprice_1way_child']);
					$html .= ' <tr>
							    <td>
							    	<input name="schedule_id_'.$schedule_type.'" value="'.$sid.'" type="radio" id="rsid'.$d['sid'].'" '.$status_required.'>
							    	<label for="rsid'.$d['sid'].'"><span></span>&nbsp;&nbsp;</label>
							    </td>
							    <td>'.$d['sname'].'</td>
							    <td>'.$d['stime_departure'].'</td>	
							    <td>'.$d['stime_arrive'].'</td>		   
							    <td title="'.$d_sp['srname'].'"><span style="text-decoration: line-through;">'.number_format($d['rprice_1way'],0,',','.').'</span><br>'.number_format($d_sp['srprice_1way'],0,',','.').'</td>
							    <td title="'.$d_sp['srname'].'"><span style="text-decoration: line-through;">'.$rate_child.'</span><br>'.$rate_child_sp.'</td>
							    <td>free</td>		  
							    <td title="'.$d_sp['srname'].'"><span style="text-decoration: line-through;">'.number_format($total,0,',','.').'</span><br>'.number_format($total_sp,0,',','.').'</td>		    
		  					</tr>';
				}
				//E got special price or not
				
				else{
					$html .= ' <tr>
							    <td><input name="schedule_id_'.$schedule_type.'" value="'.$sid.'" type="radio" id="rsid'.$d['sid'].'" '.$status_required.'>
							    	<label for="rsid'.$d['sid'].'"><span></span>&nbsp;&nbsp;</label>
							    </td>
							    <td class="col-sname">'.$d['sname'].'</td>
							    <td>'.$d['stime_departure'].'</td>	
							    <td>'.$d['stime_arrive'].'</td>		   
							    <td class="col-price">'.number_format($d['rprice_1way'],0,',','.').'</td>
							    <td class="col-price">'.$rate_child.'</td>
							    <td class="col-price">free</td>		  
							    <td>'.number_format($total,0,',','.').'</td>		    
		  					</tr>';
				}
			}
		}
	}	
	return $html;
}
function ticket_front_form_passenger($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant){
	$html = '';
	
	for ($i=0;$i<$num_adult;$i++){
		$html.= '<tr>
					<td>
						<select name="pass_type[]">
							<option value="adult">Adult '.($i+1).'</option>							
						</select>
					</td>
					<td><input type="text" name="pass_fname[]" placeholder="Full Name" title="Full Name" value="" class="required"></td>
					<td>
						<select name="pass_country[]" title="Select country" class="required">
							<option value="" selected="selected">Select country</option>
							'.ticket_front_option_country().'						
						</select>
					</td>
				</tr>';
	}
	for ($i=0;$i<$num_child;$i++){
		$html.= '<tr>
					<td>
						<select name="pass_type[]">
							<option value="child">Child '.($i+1).'</option>							
						</select>
					</td>
					<td><input type="text" name="pass_fname[]" placeholder="Full Name" title="Full Name" value=""  class="required"></td>
					<td>
						<select name="pass_country[]" title="Select country" class="required">
							<option value="" selected="selected">Select country</option>
							'.ticket_front_option_country().'						
						</select>
					</td>
				</tr>';
	}
	for ($i=0;$i<$num_infant;$i++){
		$html.= '<tr>
					<td>
						<select name="pass_type[]">
							<option value="infant">Infant '.($i+1).'</option>							
						</select>
					</td>
					<td><input type="text" name="pass_fname[]" placeholder="Full Name" title="Full Name" value=""  class="required"></td>
					<td>
						<select name="pass_country[]" title="Select country" class="required">
							<option value="" selected="selected">Select country</option>
							'.ticket_front_option_country().'						
						</select>
					</td>
				</tr>';
	}
	return $html;
}
function ticket_front_num_schedule_booked($sid,$date){
	global $db,$pacha_node;
	$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE sid=%d AND date=%s AND status=%s",$sid,$date,'pd');
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if ($n==0){
		return $n;
	}else{
		$num = 0;
		while ($d=$db->fetch_array($r)){
			$num = $num + ($d['num_adult'] + $d['num_child']);
		}
		return $num;
	}
}
function ticket_front_route_id($from,$to){
	global $db;
	$q = $db->prepare_query("SELECT rid from ticket_route WHERE rfrom=%s AND rto=%s AND status=%d LIMIT 1",$from,$to,1);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	return $d['rid'];
}
function ticket_front_option_destination_from($selected=''){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_route WHERE status=%d group by rfrom order by sort_id ASC",1); 
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$status_selected = '';
		if ($selected==$d['rfrom']){
			$status_selected = 'selected="selected"';
		}
		$html .= '<option '.$status_selected.' value="'.$d['rfrom'].'">'.$d['rfrom'].'</option>';
	}	
	return $html;
}
function ticket_front_option_destination_to($from){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_route WHERE rfrom=%s AND status=%d order by sort_id ASC",$from,1); 
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){		
		$html .= '<option value="'.$d['rto'].'">'.$d['rto'].'</option>';
	}	
	return $html;
}
function ticket_front_option_country($selected=''){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from lumonata_country WHERE lpublish=%s order by lorder_id ASC,lcountry ASC",'1'); 
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){	
		$status_selected = '';
		if ($d['lcountry']==$selected){
			$status_selected = 'selected="selected"';
		}	
		$html .= '<option value="'.$d['lcountry'].'" '.$status_selected.'>'.$d['lcountry'].'</option>';
	}	
	return $html;
}
function ticket_front_option_title($selected=''){
	$titles = array('Mr.','Mrs.','Dr.');
	$html = '';
	for ($i=0;$i<count($titles);$i++){
		$status_selected = '';
		if ($titles[$i]==$selected){
			$status_selected = 'selected="selected"';
		}
		$html .='<option value="'.$titles[$i].'" '.$status_selected.'>'.$titles[$i].'</option>';
	}
	return $html;
}
function ticket_front_get_checkbox_skip_return(){
	$html = '<span class="skip"><input type="Checkbox" name="skip_return" value="1" id="skip_return"> Skip return trip &raquo; (<a href="#">?</a>)</span>';
	return $html;
}
//actions ajax
function ticket_front_ajax(){
	global $db;	
	add_actions('is_use_ajax', true);
	if (isset($_POST['pKEY']) && !empty($_POST['pKEY'])){
		$pkey = $_POST['pKEY'];
		switch ($pkey){
			case 'get_option_destination_to':
				echo ticket_front_option_destination_to($_POST['from']);
				break;
		}
	}
}
?>