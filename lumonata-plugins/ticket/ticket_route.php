<?php
if (isset($_POST['update_sort_id']) && isset($_POST['state']) && $_POST['state']=='ticket' && $_POST['sub']=='route' ){
	ticket_route_update_sort_id();
	exit;
}

add_actions('route','ticket_route_index');
add_actions("ticket-route-ajax_page","ticket_route_ajax");
function ticket_route_index(){	
	if(is_add_new()){			
		return ticket_route_new() ;
	}elseif(is_edit()){ 
		return ticket_route_edit($_GET['id']);
	}elseif(is_edit_all() && isset($_POST['select'])){
		
	}elseif(is_delete_all()){
		return ticket_route_delete_multiple(); 
	}elseif(is_confirm_delete()){
		foreach($_POST['id'] as $key=>$val){ticket_route_delete($val);}
	}
	
	//Display data route
    if(ticket_route_num()>0){
        return ticket_route_table_data();
    }else{			
        return ticket_route_new();
    }	
}
function ticket_route_new(){
	ticket_route_set_template('route_new.html','tplRouteNew','routeNew','rNew');
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="	
	<li>".button("button=add_new",get_state_url('ticket&sub=route')."&prc=add_new")."</li>	
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url('ticket&sub=route'))."</li>";
		
	
	add_actions('section_title','Route - Add New');
	add_variable('title_Form','Route & Price');	
	add_variable('option_port_from', ticket_get_port_option());
	add_variable('option_port_to', ticket_get_port_option());
	if (isset($_POST['save_changes'])){
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['rname'][$i]==''){			
			$error .= '<div class="error_red">Please type route name</div>';			
			$validation = false;
		}
		/*
		if ($_POST['rboat'][$i]==''){			
			$error .= '<div class="error_red">Please type boat name</div>';			
			$validation = false;
		}
		*/
		if ($_POST['rfrom'][$i]==''){			
			$error .= '<div class="error_red">Please type destination from</div>';			
			$validation = false;
		}
		if ($_POST['rto'][$i]==''){			
			$error .= '<div class="error_red">Please type destination to</div>';			
			$validation = false;
		}		
		if ($_POST['rprice_1way'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way trip</div>';			
			$validation = false;
		}
		if ($_POST['rprice_2way'][$i]==''){			
			$error .= '<div class="error_red">Please type price return trip</div>';			
			$validation = false;
		}
		if ($_POST['rprice_1way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price one way trip for child</div>';			
			$validation = false;
		}
		if ($_POST['rprice_2way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price return trip for child</div>';			
			$validation = false;
		}
		/*
		if ($_POST['rallotment'][$i]==''){			
			$error .= '<div class="error_red">Please type allotment</div>';			
			$validation = false;
		}
		*/
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_rname',$_POST['rname'][$i]);	
			//add_variable('val_rboat',$_POST['rboat'][$i]);	
					
			//add_variable('val_rfrom',$_POST['rfrom'][$i]);
			//add_variable('val_rto',$_POST['rto'][$i]);
			add_variable('option_port_from', ticket_get_port_option($_POST['rfrom'][$i]));
			add_variable('option_port_to', ticket_get_port_option($_POST['rto'][$i]));
			
			add_variable('val_rprice_1way',$_POST['rprice_1way'][$i]);
			add_variable('val_rprice_2way',$_POST['rprice_2way'][$i]);
			add_variable('val_rprice_1way_child',$_POST['rprice_1way_child'][$i]);
			add_variable('val_rprice_2way_child',$_POST['rprice_2way_child'][$i]);
			
			/*add_variable('val_rallotment',$_POST['rallotment'][$i]);*/	
		}
		
		//save
		if ($validation==true){
			if (ticket_route_insert()){
				$error = '<div class="error_green">Add route <code>'.$_POST['rname'][$i].'</code> has save succesfully.</div>';
				add_variable('error',$error);	
				add_variable('val_rname','');	
				add_variable('val_rboat','');	

				//add_variable('val_rfrom','');
				//add_variable('val_rto','');
				add_variable('option_port_from', ticket_get_port_option());
				add_variable('option_port_to', ticket_get_port_option());
				
				add_variable('val_rprice_1way','');
				add_variable('val_rprice_2way','');
				add_variable('val_rprice_1way_child','');
				add_variable('val_rprice_2way_child','');
				add_variable('val_rallotment','');				
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}		
	}
	
	add_variable('i',$i);
	add_variable('button',$button);
	parse_template('loopPage','lPage',false);
	 
	return ticket_route_return_template();
}
function ticket_route_edit($id){
	global $db;

	$index=0;
	$button="";
	ticket_route_set_template();

	$button.="
			<li>".button("button=add_new",get_state_url('ticket&sub=route')."&prc=add_new")."</li>
		   	<li>".button("button=save_changes&label=Save")."</li>
			<li>".button("button=cancel",get_state_url('ticket&sub=route'))."</li>";
	
	//set the page Title
	add_actions('section_title','Route - Edit');

	//echo "is_single_edit";
	add_variable('title_Form','Edit Route');
		
	if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){			
		$i 	= 0;
		$id = $_GET['id'];					
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['rname'][$i]==''){			
			$error .= '<div class="error_red">Please type route name</div>';			
			$validation = false;
		}
		/*
		if ($_POST['rboat'][$i]==''){			
			$error .= '<div class="error_red">Please type boat name</div>';			
			$validation = false;
		}
		*/
		if ($_POST['rfrom'][$i]==''){			
			$error .= '<div class="error_red">Please type destination from</div>';			
			$validation = false;
		}
		if ($_POST['rto'][$i]==''){			
			$error .= '<div class="error_red">Please type destination to</div>';			
			$validation = false;
		}		
		if ($_POST['rprice_1way'][$i]==''){			
			$error .= '<div class="error_red">Please type price 1 way</div>';			
			$validation = false;
		}
		if ($_POST['rprice_2way'][$i]==''){			
			$error .= '<div class="error_red">Please type price 2 way</div>';			
			$validation = false;
		}
		if ($_POST['rprice_1way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price 1 way for child</div>';			
			$validation = false;
		}
		if ($_POST['rprice_2way_child'][$i]==''){			
			$error .= '<div class="error_red">Please type price 2 way for child</div>';			
			$validation = false;
		}
		/*
		if ($_POST['rallotment'][$i]==''){			
			$error .= '<div class="error_red">Please type allotment</div>';			
			$validation = false;
		}
		*/
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_rname',$_POST['rname'][$i]);	
			/*add_variable('val_rboat',$_POST['rboat'][$i]);*/		
			add_variable('val_rfrom',$_POST['rfrom'][$i]);
			add_variable('val_rto',$_POST['rto'][$i]);
			add_variable('val_rprice_1way',$_POST['rprice_1way'][$i]);
			add_variable('val_rprice_2way',$_POST['rprice_2way'][$i]);
			add_variable('val_rprice_1way_child',$_POST['rprice_1way_child'][$i]);
			add_variable('val_rprice_2way_child',$_POST['rprice_2way_child'][$i]);
			/*add_variable('val_rallotment',$_POST['rallotment'][$i]);*/	
		}
		
		//save
		if ($validation==true){
			if (ticket_route_update()){
				$error = '<div class="error_green">'.$_POST['rname'][$i].' has been update succesfully.</div>';
				add_variable('error',$error);	
				add_variable('val_rname','');		
				add_variable('val_rfrom','');
				add_variable('val_rto','');
				add_variable('val_rprice_1way','');
				add_variable('val_rprice_2way','');
				add_variable('val_rallotment','');				
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}			
			
	}

	//show data from db
	$q = $db->prepare_query("SELECT * from ticket_route WHERE rid=%d",$id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);	
	add_variable('val_rid',$d['rid']);	
	add_variable('val_rname',$d['rname']);	
	add_variable('val_rboat',$d['rboat']);		
	
	//add_variable('val_rfrom',$d['rfrom']);
	//add_variable('val_rto',$d['rto']);
	add_variable('option_port_from', ticket_get_port_option($d['rfrom']));
	add_variable('option_port_to', ticket_get_port_option($d['rto']));
	
	add_variable('val_rprice_1way',$d['rprice_1way']);
	add_variable('val_rprice_2way',$d['rprice_2way']);
	add_variable('val_rprice_1way_child',$d['rprice_1way_child']);
	add_variable('val_rprice_2way_child',$d['rprice_2way_child']);
	add_variable('val_rallotment',$d['rallotment']);		
	if ($d['status']==1){
		add_variable('status_checked_publish','checked');	
	}else{
		add_variable('status_checked_not_publish','checked');
	}
	
	add_variable('i',$index);
	parse_template('loopPage','lPage',false);
	
	add_variable('button',$button);
	return ticket_route_return_template();
}
function ticket_route_table_data(){
	global $db;
	
	$app_name = 'ticket_route';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('ticket&sub=route')."&page=";
	
	if(is_search_route()){ 
		$key = 	$_POST['s']; 
		$sql=$db->prepare_query("SELECT * from ticket_route 
				WHERE  rname like %s OR rfrom like %s OR rto like %s OR rprice_1way like %s OR rprice_2way like %s OR rallotment like %s order by sort_id ASC",
				"%".$key."%","%".$key."%","%".$key."%","%".$key."%","%".$key."%","%".$key."%");
		$num_rows=count_rows($sql);	
	}else{ 
		$sql=$db->prepare_query("select * from ticket_route ORDER BY sort_id ASC");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("select * from ticket_route ORDER BY sort_id ASC limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("ticket&sub=route")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = 'http://'.SITE_URL.'/ticket-route-ajax/';
	$list.="<h1>Route & Price</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('ticket&sub=route')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".ticket_route_search_box($url_ajax,'list_item','state=ticket&sub=route&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('ticket&sub=route')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"ticket&sub=route\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>
						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />								
								
								<div class=\"pages_title\" style=\"width:25%;\">Name</div>	
								<!--<div class=\"pages_title\" style=\"width:20%;\">Boat</div>-->		
								<div class=\"pages_title\" style='max-width:18%;' >&nbsp;&nbsp;From</div>		
								<div class=\"pages_title\" style='max-width:18%;' >&nbsp;&nbsp;To</div>		
								<div class=\"pages_title\" style='max-width:15%;' >Price One Way</div>
								<div class=\"pages_title\" style='max-width:15%;' >Price Return</div>	
								<!--div class=\"pages_title\" style='max-width:5%;' >Allotment</div>-->							
							</div>
							<div id=\"list_item\">";
	$list.=ticket_route_list_data($result,$start_order);
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	
	$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/ticket/js/list.js" ></script>';	
	add_actions('section_title','Route & Price');
	return $list;
}
function ticket_route_list_data($result,$i=1){
 		global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        while($d=$db->fetch_array($result)){        		
				$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['rid']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['rid']."\" style='margin-left:13px;' />
                                <div class=\"pages_title\" style=\"width:25%;\">".$d['rname']."</div>
                                <!--<div class=\"pages_title\" style=\"width:20%;\">".$d['rboat']."</div>-->
								<div class=\"pages_title\" style=\"width:18%;\">".$d['rfrom']."</div>
								<div class=\"pages_title\" style=\"width:18%;\">".$d['rto']."</div>
								<div class=\"pages_title\" style=\"width:15%;\">".$d['rprice_1way']."</div>
								<div class=\"pages_title\" style=\"width:15%;\">".$d['rprice_2way']."</div>
								<!--<div class=\"pages_title\" style=\"width:5%;\">".$d['rallotment']."</div>-->
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['rid']."\">
                                                <a href=\"".get_state_url('ticket&sub=route')."&prc=edit&id=".$d['rid']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['rid']."\">Delete</a>

                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['rid']."').mouseover(function(){
                                                $('#the_navigation_".$d['rid']."').show();
                                        });
                                        $('#theitem_".$d['rid']."').mouseout(function(){
                                                $('#the_navigation_".$d['rid']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$d['rname']."?";
				$url = 'http://'.SITE_URL.'/ticket-route-ajax/';
                add_actions('admin_tail','ticket_route_delete_confirmation_box',$d['rid'],$msg,$url,"theitem_".$d['rid'],'state=ticket&sub=route&prc=delete&id='.$d['rid']);                
                $i++;                 
        }
        return $list;
}
function ticket_route_delete_multiple(){
	 global $db;
	 add_actions('section_title','Delete Route & Price');
				$warning="<h1>Route & Price</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this route:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these route:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$q = $db->prepare_query("SELECT * FROM ticket_route WHERE rid=%d",$val);
						$r = $db->do_query($q);
                        $d = $db->fetch_array($r);
                        $warning.="<li>".$d['rname']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('ticket&sub=route')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
    return $warning;	
}
function ticket_route_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\" style='position:relative;'>
						<input type='submit' name='search_route' value='yes' style='position:absolute;bottom:0;left:0;top:0;opacity:0;cursor:pointer;'>	
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"_search\" class=\"searchbutton\" value=\"yes\" />						
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'ticket_route_search',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}
//function only
function is_search_route(){
	if((isset($_POST['search_route']) && $_POST['search_route']=='yes'))
		return true;
	return false;		
}
function ticket_route_get_allotment($rid){
	global $db;
	$query = $db->prepare_query("SELECT * FROM ticket_route Where rid = %d",$rid);
	$result= $db->do_query($query);
	$data = $db->fetch_array($result);
	return $data['rallotment'];
}
function ticket_route_delete($id){
	global $db;
	$query = $db->prepare_query("DELETE FROM ticket_route Where rid = %d",$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
function ticket_route_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_route_delete',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_route_insert($i=0){
	global $db;
	$q = $db->prepare_query("INSERT INTO ticket_route 
			(rname,rboat,rfrom,rto,
			rprice_1way,rprice_2way,rprice_1way_child,rprice_2way_child,
			rallotment,sort_id,status,created_by,created_date) VALUES 
			(%s,%s,%s,%s,
			%d,%d,%d,%d,
			%d,%d,%d,%s,%d)",
			$_POST['rname'][$i],$_POST['rboat'][$i],$_POST['rfrom'][$i],$_POST['rto'][$i],			
			$_POST['rprice_1way'][$i],$_POST['rprice_2way'][$i],$_POST['rprice_1way_child'][$i],$_POST['rprice_2way_child'][$i],
			$_POST['rallotment'][$i],0,$_POST['status'][$i],$_COOKIE['username'],time()			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_route_update($i=0){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_route SET
			rname=%s,rboat=%s,rfrom=%s,rto=%s,
			rprice_1way=%d,rprice_2way=%d,rprice_1way_child=%d,rprice_2way_child=%d,
			rallotment=%d,status=%d,dlu=%d WHERE rid=%d",			
			$_POST['rname'][$i],$_POST['rboat'][$i],$_POST['rfrom'][$i],$_POST['rto'][$i],			
			$_POST['rprice_1way'][$i],$_POST['rprice_2way'][$i],$_POST['rprice_1way_child'][$i],$_POST['rprice_2way_child'][$i],
			$_POST['rallotment'][$i],$_POST['status'][$i],time(),$_POST['rid'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_route_num(){
	global $db;
	
	$q = $db->prepare_query("SELECT * FROM ticket_route");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	return $n;
}
function ticket_route_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".ticket_template_dir()."/route_new.html",'template_route_new');
        //set block
        add_block('loopPage','lPage','template_route_new');
        add_block('routeNew','rNew','template_route_new');
}

function ticket_route_return_template($loop=false){       
        parse_template('routeNew','rNew',$loop);
        return return_template('template_route_new');
}
function ticket_route_ajax(){
	global $db;
	$app_name = 'ticket_route'; 
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_route_delete'){
		$id = $_POST['val_id'];
		if (ticket_route_delete($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_route_search'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }	 

	    $sql=$db->prepare_query("select * from ticket_route 
				WHERE  rname like %s OR rfrom like %s OR rto like %s OR rprice_1way like %s OR rprice_2way like %s OR rallotment like %s order by sort_id ASC",
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
	 
		 $r=$db->do_query($sql);
        if($db->num_rows($r) > 0){			
			echo ticket_route_list_data($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}	
}
function ticket_route_update_sort_id(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];	
 	
	foreach($items as $key=>$val){
         $sql=$db->prepare_query("UPDATE ticket_route SET 
                                     sort_id=%d,dlu=%d WHERE rid=%d",
                                     $key+$start,time(),$val);
            $db->do_query($sql);     
    }
}
?>