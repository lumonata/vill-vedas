<?php 
function is_ticket_paid_notify(){	
	$url = explode('/',get_uri()); /* ticket-paid-notify/0201309300001 */		
	if(isset($url[0]) && ($url[0]=='ticket-paid-notify')){		
		return true;
	}else{			
		return false;
	}
}
function is_ticket_succesfully_paid(){	
	$url = explode('/',get_uri()); /* ticket-succesfully/0201309300001 */		
	if(isset($url[0]) && ($url[0]=='ticket-succesfully')){		
		return true;
	}else{			
		return false;
	}
}
function is_ticket_unsuccesfully_paid(){	
	$url = explode('/',get_uri()); /* ticket-unsuccesfully/0201309300001 */		
	if(isset($url[0]) && ($url[0]=='ticket-unsuccesfully')){		
		return true;
	}else{			
		return false;
	}
}
function get_no_ticket_from_paypal(){
	$url = explode('/',get_uri()); /* ticket-paid-notify/0201309300001 */	
	if(isset($url[1]) && !empty($url[1])){
		$no_ticket = $url[1]; 
		return $no_ticket;
	}else{
		return false;
	}	
}
function get_paypal_url_mode(){
	return ticket_setting_get_value('paypal-url');
}
function ticket_paid_notify(){
	// STEP 1: Read POST data
	// reading posted data from directly from $_POST causes serialization 
	// issues with array data in POST
	// reading raw POST data from input stream instead. 
	$raw_post_data = file_get_contents('php://input');
	$raw_post_array = explode('&', $raw_post_data);
	$myPost = array();
	foreach ($raw_post_array as $keyval) {
	  $keyval = explode ('=', $keyval);
	  if (count($keyval) == 2)
	     $myPost[$keyval[0]] = urldecode($keyval[1]);
	}
	// read the post from PayPal system and add 'cmd'
	$req = 'cmd=_notify-validate';
	if(function_exists('get_magic_quotes_gpc')) {
	   $get_magic_quotes_exists = true;
	} 
	foreach ($myPost as $key => $value) {        
	   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
	        $value = urlencode(stripslashes($value)); 
	   } else {
	        $value = urlencode($value);
	   }
	   $req .= "&$key=$value";
	}
	
	 
	// STEP 2: Post IPN data back to paypal to validate
	
	///$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
	$ch = curl_init(get_paypal_url_mode());
	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
	
	// In wamp like environments that do not come bundled with root authority certificates,
	// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
	// of the certificate as shown below.
	// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
	if( !($res = curl_exec($ch)) ) {
	    // error_log("Got " . curl_error($ch) . " when processing IPN data");
	    curl_close($ch);
	    exit;
	}
	curl_close($ch);
	 
	
	// STEP 3: Inspect IPN validation result and act accordingly
	
	if (strcmp ($res, "VERIFIED") == 0) {
	    // check whether the payment_status is Completed
	    // check that txn_id has not been previously processed
	    // check that receiver_email is your Primary PayPal email
	    // check that payment_amount/payment_currency are correct
	    // process payment		
		
	    // assign posted variables to local variables
	    $item_name 			= $_POST['item_name'];
	    $item_number 		= $_POST['item_number'];
	    $payment_status 	= $_POST['payment_status'];
	    $payment_amount 	= $_POST['mc_gross'];
	    $payment_currency 	= $_POST['mc_currency'];
	    $txn_id 			= $_POST['txn_id'];
	    $receiver_email 	= $_POST['receiver_email'];
	    $payer_email 		= $_POST['payer_email'];	    
	   
	    //update status booking to paid & send email
	    if (get_no_ticket_from_paypal()==false){
	    	exit;
	    }else{
	    	$no_ticket = get_no_ticket_from_paypal();
	    	$bid	   = ticket_get_booking_id_by_no_ticket($no_ticket);
	    	//ticket_save_payment_info($bid,$no_ticket,$status,$total,$to_rek,$metode,$frm_bank,$note);
	    	ipn_aksi($no_ticket,$payer_email);
	    	ticket_email_paypal_payment_confirmation($bid,$no_ticket,$_POST);
	    	ticket_send_softcopy_ticket($bid);
	    }	    
	} else if (strcmp ($res, "INVALID") == 0) {
	    // log for manual investigation	    
	}
}
function ipn_aksi($no_ticket,$payer_email){
	global $db;	
	$bid = ticket_get_booking_id_by_no_ticket($no_ticket);
	
	//update status payment 
	$sql_update_payment = $db->prepare_query("UPDATE ticket_booking SET status=%s WHERE bid=%d",'pd',$bid);
	$qry_update_payment = $db->do_query($sql_update_payment);
		
	//update status payment pada detail booking
	$sql_update_payment = $db->prepare_query("UPDATE ticket_booking_detail SET status=%s WHERE bid=%d",'pd',$bid);
	$qry_update_payment = $db->do_query($sql_update_payment);			
}
?>