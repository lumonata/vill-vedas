<?php
if (isset($_POST['update_sort_id']) && isset($_POST['state']) && $_POST['state']=='ticket' && $_POST['sub']=='schedule' ){
	ticket_schedule_update_sort_id();
	exit;
}

add_actions('schedule','ticket_schedule_index');
add_actions("ticket-schedule-ajax_page","ticket_schedule_ajax");
function ticket_schedule_index(){	
	if(is_add_new()){			
		return ticket_schedule_new() ;
	}elseif(is_edit()){ 
		return ticket_schedule_edit($_GET['id']);
	}elseif(is_edit_all() && isset($_POST['select'])){
		
	}elseif(is_delete_all()){
		return ticket_schedule_delete_multiple(); 
	}elseif(is_confirm_delete()){
		foreach($_POST['id'] as $key=>$val){ticket_schedule_delete($val);}
	}
	
	//Display data schedule
    if(ticket_schedule_num()>0){
        return ticket_schedule_table_data();
    }else{			
        return ticket_schedule_new();
    }	
}
function ticket_schedule_new(){
	ticket_schedule_set_template('schedule_new.html','tplscheduleNew','scheduleNew','rNew');
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="	
	<li>".button("button=add_new",get_state_url('ticket&sub=schedule')."&prc=add_new")."</li>	
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url('ticket&sub=schedule'))."</li>";
		
	
	add_actions('section_title','Schedule - Add New');
	add_variable('title_Form','Schedule');	
	if (isset($_POST['save_changes'])){
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['sname'][$i]==''){			
			$error .= '<div class="error_red">Please type schedule name</div>';			
			$validation = false;
		}
		if ($_POST['rid'][$i]==''){			
			$error .= '<div class="error_red">Please choose route</div>';			
			$validation = false;
		}
		if ($_POST['stime_departure'][$i]==''){			
			$error .= '<div class="error_red">Please type departure time</div>';			
			$validation = false;
		}
		if ($_POST['stime_arrive'][$i]==''){			
			$error .= '<div class="error_red">Please type arrive time</div>';			
			$validation = false;
		}		
		if (!isset($_POST['sparent'][$i])){			
			$error .= '<div class="error_red">Please choose Departure group</div>';			
			$validation = false;
		}	
		
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_sname',$_POST['sname'][$i]);	
			add_variable('val_route_option',ticket_route_option($_POST['rid'][$i]));			
			add_variable('val_stime_departure',$_POST['stime_departure'][$i]);
			add_variable('val_stime_arrive',$_POST['stime_arrive'][$i]); 		
		}
		
		//save
		if ($validation==true){
			if (ticket_schedule_insert()){
				$error = '<div class="error_green">Add schedule has save succesfully.</div>';
				add_variable('error',$error);	
				add_variable('val_sname','');	
				add_variable('val_route_option',ticket_route_option());			
				add_variable('val_stime_departure','');
				add_variable('val_stime_arrive','');		
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}		
	}
	
	add_variable('i',$i);
	add_variable('route_option',ticket_route_option());
	add_variable('boat_option',ticket_boat_option());
	add_variable('status_checked_publish','checked');
	add_variable('button',$button);	
	parse_template('loopPage','lPage',false);
	 
	return ticket_schedule_return_template();
}
function ticket_schedule_edit($id){
	global $db;

	$index=0;
	$button="";
	ticket_schedule_set_template();

	$button.="
			<li>".button("button=add_new",get_state_url('ticket&sub=schedule')."&prc=add_new")."</li>
		   	<li>".button("button=save_changes&label=Save")."</li>
			<li>".button("button=cancel",get_state_url('ticket&sub=schedule'))."</li>";
	
	//set the page Title
	add_actions('section_title','Schedule - Edit');

	//echo "is_single_edit";
	add_variable('title_Form','Edit schedule');
		
	if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){			
		$i 	= 0;
		$id = $_GET['id'];					
		$validation = true;		
		$error	= '';		
		//validation
		if ($_POST['sname'][$i]==''){			
			$error .= '<div class="error_red">Please type schedule name</div>';			
			$validation = false;
		}
		if ($_POST['rid'][$i]==''){			
			$error .= '<div class="error_red">Please choose route</div>';			
			$validation = false;
		}
		if ($_POST['boat_id'][$i]==''){			
			$error .= '<div class="error_red">Please choose boat</div>';			
			$validation = false;
		}
		if ($_POST['stime_departure'][$i]==''){			
			$error .= '<div class="error_red">Please type departure time</div>';			
			$validation = false;
		}
		if ($_POST['stime_arrive'][$i]==''){			
			$error .= '<div class="error_red">Please type arrive time</div>';			
			$validation = false;
		}		
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_sname',$_POST['sname'][$i]);	
			add_variable('val_route_option',ticket_route_option($_POST['rid'][$i]));
			add_variable('val_boat_option',ticket_boat_option($_POST['boat_id'][$i]));			
			add_variable('val_stime_departure',$_POST['stime_departure'][$i]);
			add_variable('val_stime_arrive',$_POST['stime_arrive'][$i]);	
		}
		
		//save
		if ($validation==true){
			if (ticket_schedule_update()){
				$error = '<div class="error_green">'.$_POST['sname'][$i].' has been update succesfully.</div>';
				add_variable('error',$error);								
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}			
			
	}

	//show data from db
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE sid=%d",$id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);	
	add_variable('val_sid',$d['sid']);	
	add_variable('val_sname',$d['sname']);	
	add_variable('route_option',ticket_route_option($d['rid']));	
	add_variable('boat_option',ticket_boat_option($d['sboat']));		
	add_variable('val_stime_departure',$d['stime_departure']);
	add_variable('val_stime_arrive',$d['stime_arrive']);
	
	
	if ($d['sparent']==0){
		add_variable('departure_checked_sunrise','checked');	
	}elseif ($d['sparent']==1){
		add_variable('departure_checked_sunset','checked');
	}
	
	if ($d['status']==1){
		add_variable('status_checked_publish','checked');	
	}else{
		add_variable('status_checked_not_publish','checked');
	}
	
	add_variable('i',$index);
	parse_template('loopPage','lPage',false);
	
	add_variable('button',$button);
	return ticket_schedule_return_template();
}
function ticket_schedule_table_data(){
	global $db;
	
	$app_name = 'ticket_schedule';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('ticket&sub=schedule')."&page=";
	
	if(is_search_schedule()){ 
		$key = 	$_POST['s'];		
		$sql=$db->prepare_query("SELECT *,ticket_schedule.status as s_status,ticket_master_boat.bname as bname,ticket_master_boat.ballotment as ballotment 
				FROM ticket_schedule,ticket_route,ticket_master_boat 
				WHERE ticket_schedule.rid=ticket_route.rid AND ticket_schedule.sboat=ticket_master_boat.boat_id 
				AND (sname like %s OR stime_departure like %s OR stime_arrive like %s)
				ORDER BY ticket_schedule.sort_id ASC",
				"%".$key."%","%".$key."%","%".$key."%");
		$num_rows=count_rows($sql);	
	}else{ 
		$sql=$db->prepare_query("SELECT * from ticket_schedule,ticket_route,ticket_master_boat 
				WHERE ticket_schedule.rid=ticket_route.rid AND ticket_schedule.sboat=ticket_master_boat.boat_id 
				ORDER BY ticket_schedule.sort_id ASC");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("SELECT *,ticket_schedule.status as s_status,ticket_master_boat.bname as bname,ticket_master_boat.ballotment as ballotment
        		FROM ticket_schedule,ticket_route,ticket_master_boat 
        		WHERE ticket_schedule.rid=ticket_route.rid AND ticket_schedule.sboat=ticket_master_boat.boat_id
        		ORDER BY ticket_schedule.sort_id ASC limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("ticket&sub=schedule")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = 'http://'.SITE_URL.'/ticket-schedule-ajax/';
	$list.="<h1>Schedules</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('ticket&sub=schedule')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".ticket_schedule_search_box($url_ajax,'list_item','state=ticket&sub=schedule&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('ticket&sub=schedule')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"ticket&sub=schedule\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />								
								<div class=\"pages_title\" style=\"width:15%;\">Departure</div>	
								<div class=\"pages_title\" style=\"width:20%;\">Route</div>	
								<div class=\"pages_title\" style=\"width:15%;\">&nbsp;&nbsp;Boat</div>
								<!--<div class=\"pages_title\" style=\"width:20%;\">&nbsp;&nbsp;Schedule</div>-->	
								<div class=\"pages_title\" style=\"width:15%;\">&nbsp;&nbsp;Departure</div>
								<div class=\"pages_title\" style=\"width:15%;\">&nbsp;&nbsp;Arrive</div>									
								<div class=\"pages_title\" style=\"width:10%;\">&nbsp;&nbsp;Status</div>								
							</div>
							<div id=\"list_item\">";
	$list.=ticket_schedule_list_data($result,$start_order);
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	
	$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/ticket/js/list.js" ></script>';	
	add_actions('section_title','Schedule');
	return $list;
}
function ticket_schedule_list_data($result,$i=1){
 		global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        while($d=$db->fetch_array($result)){     
        		$sparent		= $d['sparent']=='0' ? 'Sunrise' : 'Sunset';
        		$status_publish = $d['s_status']? 'Publish': 'Not publish';  		
				$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['sid']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['sid']."\" style='margin-left:13px;' />
                                <div class=\"pages_title\" style=\"width:15%;\">".$sparent."</div>	
                                <div class=\"pages_title\" style=\"width:20%;\">".$d['rname']."</div>	
                                <div class=\"pages_title\" style=\"width:15%;\">".$d['bname']."</div>
								<!--<div class=\"pages_title\" style=\"width:20%;\">".$d['sname']."</div>-->	
								<div class=\"pages_title\" style=\"width:15%;\">".$d['stime_departure']."</div>
								<div class=\"pages_title\" style=\"width:15%;\">".$d['stime_arrive']."</div>									
								<div class=\"pages_title\" style=\"width:10%;\">".$status_publish."</div>		
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['sid']."\">
                                                <a href=\"".get_state_url('ticket&sub=schedule')."&prc=edit&id=".$d['sid']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['sid']."\">Delete</a>

                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['sid']."').mouseover(function(){
                                                $('#the_navigation_".$d['sid']."').show();
                                        });
                                        $('#theitem_".$d['sid']."').mouseout(function(){
                                                $('#the_navigation_".$d['sid']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$d['sname']."?";
				$url = 'http://'.SITE_URL.'/ticket-schedule-ajax/';
                add_actions('admin_tail','ticket_schedule_delete_confirmation_box',$d['sid'],$msg,$url,"theitem_".$d['sid'],'state=ticket&sub=schedule&prc=delete&id='.$d['sid']);                
                $i++;                 
        }
        return $list;
}
function ticket_schedule_delete_multiple(){
	 global $db;
	 add_actions('section_title','Delete schedule');
				$warning="<h1>Schedule</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this schedule:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these schedules:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$q = $db->prepare_query("SELECT * FROM ticket_schedule WHERE sid=%d",$val);
						$r = $db->do_query($q);
                        $d = $db->fetch_array($r);
                        $warning.="<li>".$d['sname']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('ticket&sub=schedule')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
    return $warning;	
}
function ticket_schedule_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\" style='position:relative;'>
						<input type='submit' name='search_schedule' value='yes' style='position:absolute;bottom:0;left:0;top:0;opacity:0;cursor:pointer;'>	
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"_search\" class=\"searchbutton\" value=\"yes\" />						
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'ticket_schedule_search',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}
//function only
function is_search_schedule(){
	if((isset($_POST['search_schedule']) && $_POST['search_schedule']=='yes'))
		return true;
	return false;		
}
function ticket_schedule_delete($id){
	global $db;
	$query = $db->prepare_query("DELETE FROM ticket_schedule Where sid = %d",$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
function ticket_schedule_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_schedule_delete',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_schedule_insert($i=0){
	global $db;
	$allotment = ticket_boat_get_detail_by_id($_POST['boat_id'][$i],'ballotment');
	if ($allotment<1){
		return false;
	}
	$q = $db->prepare_query("INSERT INTO ticket_schedule 
			(rid,sparent,sname,stime_departure,stime_arrive,
			sort_id,status,created_by,created_date,
			sboat,sallotment) 
			VALUES 
			(%d,%d,%s,%s,%s,			
			%d,%d,%s,%d,
			%d,%d)",
			$_POST['rid'][$i],$_POST['sparent'][$i],$_POST['sname'][$i],$_POST['stime_departure'][$i],$_POST['stime_arrive'][$i],			
			0,$_POST['status'][$i],$_COOKIE['username'],time(),
			$_POST['boat_id'][$i],$allotment			
			);
	$r = $db->do_query($q);
	ticket_schedule_increment_sort_id();
	return $r;
}
function ticket_schedule_update($i=0){
	global $db;
	$allotment = ticket_boat_get_detail_by_id($_POST['boat_id'][$i],'ballotment');
	if ($allotment<1){
		return false;
	}
	$q = $db->prepare_query("UPDATE ticket_schedule SET
			rid=%d,sparent=%d,sname=%s,stime_departure=%s,stime_arrive=%s,
			status=%d,dlu=%d,
			sboat=%d,sallotment=%d 
			WHERE sid=%d",			
			$_POST['rid'][$i],$_POST['sparent'][$i],$_POST['sname'][$i],$_POST['stime_departure'][$i],$_POST['stime_arrive'][$i],				
			$_POST['status'][$i],time(),
			$_POST['boat_id'][$i],$allotment,	
			$_POST['sid'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_schedule_increment_sort_id(){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_schedule SET sort_id=sort_id+1");
	return $db->do_query($q);
}
function ticket_schedule_num(){
	global $db;
	
	$q = $db->prepare_query("SELECT * FROM ticket_schedule");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	return $n;
}
function ticket_schedule_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".ticket_template_dir()."/schedule_new.html",'template_schedule_new');
        //set block
        add_block('loopPage','lPage','template_schedule_new');
        add_block('scheduleNew','rNew','template_schedule_new');
}

function ticket_schedule_return_template($loop=false){       
        parse_template('scheduleNew','rNew',$loop);
        return return_template('template_schedule_new');
}
function ticket_schedule_ajax(){
	global $db;
	$app_name = 'ticket_schedule'; 
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_schedule_delete'){
		$id = $_POST['val_id'];
		if (ticket_schedule_delete($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_schedule_search'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }	 

	    $sql=$db->prepare_query("SELECT *,ticket_schedule.status as s_status,ticket_master_boat.bname as bname,ticket_master_boat.ballotment as ballotment 
				FROM ticket_schedule,ticket_route,ticket_master_boat 
				WHERE ticket_schedule.rid=ticket_route.rid AND ticket_schedule.sboat=ticket_master_boat.boat_id 
				AND (sname like %s OR stime_departure like %s OR stime_arrive like %s)
	    		ORDER BY ticket_schedule.sort_id ASC",	    
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
	 
		$r=$db->do_query($sql);
		
        if($db->num_rows($r) > 0){			
			echo ticket_schedule_list_data($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}	
}
function ticket_schedule_update_sort_id(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];	
 	
	foreach($items as $key=>$val){
         $sql=$db->prepare_query("UPDATE ticket_schedule SET 
                                     sort_id=%d,dlu=%d WHERE sid=%d",
                                     $key+$start,time(),$val);
            $db->do_query($sql);     
    }
}
?>