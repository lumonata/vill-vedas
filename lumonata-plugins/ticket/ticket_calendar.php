<?php
//prepare global variable
$filter_calendar = array('route'=>'','year'=>date('Y'),'month'=>date('m'));
if(isset($_POST['filter_calendar'])){	
	$filter_calendar = array('route'=>$_POST['filter_route'],'year'=>$_POST['filter_year'],'month'=>$_POST['filter_month']);
}

//add actions
add_actions('calendar','ticket_calendar_index');
add_actions("ticket-calendar-ajax_page","ticket_calendar_ajax");
?>
<?php 
function ticket_calendar_index(){	
	return ticket_calendar_table_data();
}
function ticket_calendar_table_data(){
	global $filter_calendar;	
	add_actions('section_title','Calendar');		
	ticket_calendar_set_template();
	add_variable('site_url', site_url());
	add_variable('web_name', get_meta_data('web_name'));
	
	add_variable('filter_option_route',ticket_route_option($filter_calendar['route']));
	add_variable('filter_option_year',ticket_calendar_option_year($filter_calendar['year']));
	add_variable('filter_option_month',ticket_calendar_option_month($filter_calendar['month']));
	
	add_variable('calendar',ticket_calendar_data());
	
	parse_template('loopPage','lPage',false);	 
	return ticket_calendar_return_template();
}

//just function
function ticket_calendar_data(){	
	return ticket_calendar_looping_route_table();
}
function ticket_calendar_looping_route_table(){
	global $db,$filter_calendar;
	
	if ($filter_calendar['route']==''){
		$q = $db->prepare_query("SELECT * from ticket_route WHERE status='1' order by sort_id");
	}else{
		$q = $db->prepare_query("SELECT * from ticket_route WHERE rid=%d AND status='1' order by sort_id",$filter_calendar['route']);
	}
	$r = $db->do_query($q);
	
	//set template
	set_template(PLUGINS_PATH."/".ticket_template_dir()."/calendar_loopTableRoute.html",'tbroute');
	//set block        
	add_block('loopTableRoute','lTroute','tbroute');	
	$i = 0;
	while($d=$db->fetch_array($r)){
		add_variable('i', $i);
		add_variable('rid', $d['rid']);
		add_variable('route_name', $d['rname']);
		add_variable('row_span',(ticket_calendar_schedule_num($d['rid'])+1)); 
		add_variable('tr_calendar_date_head', ticket_calendar_schedule_head($d['rid']));
		add_variable('tr_calendar_date_data', ticket_calendar_schedule_date($d['rid']));
		add_variable('tr_schedule_name', ticket_calendar_schedule_name($d['rid'],$i));
		parse_template('loopTableRoute','lTroute',true);
		$i++;
	}	
	return return_template('tbroute');
}
function ticket_calendar_schedule_num($rid){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE rid=%d order by sort_id ASC",$rid);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	return $n;
}
function ticket_calendar_schedule_name($rid,$i=0){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE rid=%d order by sparent ASC,sort_id ASC",$rid);
	$r = $db->do_query($q);	
	while ($d=$db->fetch_array($r)){
		$html .= '<tr>
				    <td id="sname'.$d['sid'].'">'.$d['sname'].'</td>
				    <td width="2"><a class="trigger-prev" rel="tb-calendar-data-'.$i.'">&nbsp;&nbsp;</a></td>
				    <td width="2"><a class="trigger-next" rel="tb-calendar-data-'.$i.'">&nbsp;&nbsp;</a></td>
				  </tr>';
		
	}
	return $html;
}
function ticket_calendar_schedule_head($rid){
	global $filter_calendar;
	$year 	 = $filter_calendar['year'];
	$month 	 = $filter_calendar['month'];
	$num_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	
	$today = date('Y-m-d');		
	$td = '';
	for ($i=1;$i<=$num_day;$i++){
		$day  = $i<10? '0'.$i:$i; 
		$date = $year.'-'.$month.'-'.$day;
		$class= $today==$date?'today':'';
		$class = '';
		$td .= ' <td class="head '.$class.'">'.$i.'<br>'.date("F", mktime(0, 0, 0, $month, 10)).'</td>';
	}	
	$html = '<tr>'.$td.'</tr>';
	return $html;
}
function ticket_calendar_schedule_date($rid){
	global $db,$filter_calendar;
	$year 	 = $filter_calendar['year'];
	$month 	 = $filter_calendar['month'];
	$num_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);	
	$today 	 = date('Y-m-d');	
	$q = $db->prepare_query("SELECT * from ticket_schedule WHERE rid=%d order by sparent ASC, sort_id ASC",$rid); 
	$r = $db->do_query($q);
	$html 	= '';	
	while ($d=$db->fetch_array($r)){
		$td 	= '';
		for ($i=1;$i<=$num_day;$i++){
			$day			= $i<10? '0'.$i:$i;
			$date			= $year.'-'.$month.'-'.$day;
			$num_booking 	= ticket_calendar_num_booking_per_schedule($d['sid'],$date);
			$num_allotment 	= ticket_get_allotment($d['sid']);
			$class			= '';
			if ($num_booking==$num_allotment){
				$class		= 'full';
			}elseif ($num_booking>$num_allotment){
				$class		= 'overload';
			}elseif($num_booking>0){
				$class		= 'some-book';
			}else{
				$class		= 'no-book';
			}
			
			if ($today==$date){
				$class .=' today';
			}
			
			//begin update use pacha system
			$TA = new ticketAvailability($rid, $d['sid'], $date, 0, 0); 
			$num_available_allotment = $TA->get_total_available_ticket_from_x_to_y(); 
			//$total_booking = $num_allotment - $num_available_allotment; //termasuk booking dari port sebelumnya
			//end update use pacha system
			
			$td .='<td class="'.$class.'">';
			$td .='<span id="d_'.$d['sid'].'_'.$date.'">'.$num_booking.'/'.$num_available_allotment.'</span>';
			
			if (strtotime($date)>=strtotime($today))			
			$td .='<span class="wrap">
						<span class="td-option">+</span>
						<span class="menu">
							<span class="">
								<a href="#" class="add-mb" data-rid="'.$rid.'" data-sid="'.$d['sid'].'" data-date="'.$date.'">
									Add Booking
								</a>
							</span>
							<span class="">
								<a href="'.get_state_url('ticket&sub=booking&filter=1&rid='.$rid.'&sparent='.$d['sparent'].'&sid='.$d['sid'].'&status=pd&date_start='.$date.'&date_end='.$date).'"> 
									Booking Detail
								</a>
							</span>
						</span>
					</span>';
			
			$td .='</td>';
		}
		$html .= '<tr>'.$td.'</tr>';	
	}	
	return $html;
}
function ticket_calendar_num_booking_per_schedule($sid,$date){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE sid=%d AND date=%s AND status=%s",$sid,$date,'pd');
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if ($n==0){
		return $n;
	}else{
		$num = 0;
		while ($d=$db->fetch_array($r)){
			$num = $num + ($d['num_adult'] + $d['num_child']);
		}
		return $num;
	}
}
function ticket_calendar_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".ticket_template_dir()."/calendar_view.html",'template_calendar_view');
        //set block
        //add_block('loopPage','lPage','template_calendar_view');
        add_block('calendarView','calView','template_calendar_view');
}
function ticket_calendar_return_template($loop=false){       
        parse_template('calendarView','calView',$loop);
        return return_template('template_calendar_view');
}
function ticket_calendar_option_year($selected=''){
	$this_year = date('Y');
	$five_year = $this_year + 5;	
	$html = '';
	for ($i=2013;$i<$five_year;$i++){
		$status = $i==$selected? 'selected': '';
		$html .= '<option '.$status.' value="'.$i.'">'.$i.'</option>';
	}
	return $html;
}
function ticket_calendar_option_month($selected=''){
	$month1 = array('01','02','03','04','05','06','07','08','09','10','11','12');
	$month2 = array('January','February','March','April','May','June','July','August','September','October','Nopember','December');	
	$html = '';
	for ($i=0;$i<count($month1);$i++){
		$status = $month1[$i]==$selected? 'selected': '';
		$html .= '<option '.$status.' value="'.$month1[$i].'">'.$month2[$i].'</option>';
	}
	return $html;
}
function ticket_calendar_get_form_manual_booking(){
	global $db;
	$sid = $_POST['sid'];
	$date= $_POST['day']; $arr_date = explode('-',$date);
	$q = $db->prepare_query("SELECT *,ticket_route.rname from ticket_schedule,ticket_route WHERE ticket_schedule.rid=ticket_route.rid AND sid=%d  LIMIT %d",$sid,1);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	//set template    
    set_template(PLUGINS_PATH."/".ticket_template_dir()."/calendar_form_manual_booking.html",'cal_fmb');
    //set block       
    add_block('fManualBooking','bFMB','cal_fmb');		
    //add variable default
    add_variable('site_url', site_url()); 
    add_variable('plugin_url', site_url().'/lumonata-plugins/ticket');
      
    //add variable
    add_variable('rid', $d['rid']);
    add_variable('sid', $d['sid']);
    add_variable('rname', $d['rname']); 
    add_variable('sname', $d['sname']);
    add_variable('date', $date);
    add_variable('time', $d['stime_departure']);
    add_variable('min_date', 'new Date('.$arr_date[0].', '.$arr_date[1].' - 1, '.$arr_date[2].')');
    //parse
	parse_template('fManualBooking','bFMB',false);   
	//return      
    return return_template('cal_fmb');    
}
function form_manual_booking_css(){
	$css = '<style>							
				.form-manual-booking {						
					padding:0;			    
					overflow: auto;
					top: 40px;		
					bottom: 40px;				
				    left: auto;				    			  
				    right: auto;
				    min-width:960px;				    	
				    max-width:960px;		        
				}				
				.form-manual-booking .f-result-availability {
				    margin: 20px 10px;
				    padding-bottom: 10px;
				}
				.form-manual-booking .tb-result th{
					background:none;					
				}
				.f-result-availability .pemesan table:nth-child(2){
					width:auto !important;
				}
				.f-result-availability .pemesan table select{
					width:200px;
				}
		</style>';
	return $css;
}
function form_manual_booking_js($dep_sid=''){ /* valiasi agar tidak double post (koneksi yg lemoet), tanda loading, dll*/
	$js = '<script>
			jQuery(document).ready(function(){
				var radio = jQuery(":radio[value='.$dep_sid.']");				
				radio.attr("checked","checked");
				jQuery(":radio[name=schedule_id_departure]").not(radio).parent().parent().hide();				
				
				jQuery("html, body").css("overflowY", "hidden");				
				
				jQuery("#overlay").click(function(){
					jQuery("#form-manual-booking").fadeOut();
					jQuery("#overlay").fadeOut();
					jQuery("html, body").css("overflowY", "auto");
				});
				
				jQuery("form[name=search_result] [name=submit_booking]").click(function(){					
					var param =  serializePost("form[name=search_result]");
						param.pKEY = "ticket_calendar_manual_booking";						
					$.post(url,param,function(data){
						if (data.status==true){					
							jQuery("#overlay").trigger("click");
							location.reload();									
						}else{
							alert(data.note);
						}
					},"json");				
					
					return false;
				});
			});
			
			function serializePost(form) {
			    var data = {};
			    form = jQuery(form).serializeArray();
			    for (var i = form.length; i--;) {
			        var name = form[i].name;
			        var value = form[i].value;
			        var index = name.indexOf("[]");
			        if (index > -1) {
			            name = name.substring(0, index);
			            if (!(name in data)) {
			                data[name] = [];
			            }
			            data[name].push(value);
			        }
			        else
			            data[name] = value;
			    }
			    return data;
			}
	</script>';
	return $js;
}



function ticket_calendar_manual_booking_check_avaibility(){
	$trip_type	= $_POST['trip_type'];
	$dep_rid 	= $_POST['dep_rid'];
	$dep_sid 	= $_POST['dep_sid'];
	$dep_date 	= $_POST['dep_date'];
	$ret_date 	= $_POST['ret_date'];
		
	$num_adult 	= $_POST['num_adult'];
	$num_child 	= $_POST['num_child'];
	$num_infant	= $_POST['num_infant'];
	
	$dep_rid_detail	= ticket_front_get_route_by_id($dep_rid);
	$from			= $dep_rid_detail['rfrom'];
	$to				= $dep_rid_detail['rto'];
	$css			= form_manual_booking_css();
	$js				= form_manual_booking_js($dep_sid);
	$TA = new ticketAvailability($dep_rid, $dep_sid, $dep_date, $num_adult, $num_child);
	if ($TA->is_available()){
		if ($trip_type=='1'){
			//cek return avaibility								
			$ret_rid		= ticket_front_route_id($to,$from); 
			
			$status_return 	= is_ticket_available($ret_rid,$ret_date,$num_adult,$num_child);
			if ($status_return==false){
				$data = array('status'=>false,'note'=>'Return ticket on '.$ret_date.' is not available!');				
			}else{
				$trip_summary 	= ticket_front_trip_summary($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);				
				$step_1  		= ticket_front_step_1_return_trip($trip_type,$from,$to,$dep_date,'',$num_adult,$num_child,$num_infant,'departure');
				$step_1 		.= ticket_front_step_1_return_trip($trip_type,$to,$from,$ret_date,'',$num_adult,$num_child,$num_infant,'return');
				$step_1 		.= ticket_front_step_1_return_trip_pricing($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant,'departure');				
				$step_2			= ticket_front_step_2($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
				$step_3			= ticket_front_step_3($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
				$step_4			= ticket_front_step_4($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
				
				$fmb 			= ticket_availability_result_yes($trip_type,$trip_summary,$step_1,$step_2,$step_3,$step_4);
				$data = array('status'=>true,'note'=>'Departure & Return ticket is available!','form_manual_booking'=>$css.$fmb.$js);
			}
		}else{
			$trip_summary 	= ticket_front_trip_summary($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_1			= ticket_front_step_1($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_2			= ticket_front_step_2($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_3			= ticket_front_step_3($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			$step_4			= ticket_front_step_4($trip_type,$from,$to,$dep_date,$ret_date,$num_adult,$num_child,$num_infant);
			
			$fmb 			= ticket_availability_result_yes($trip_type,$trip_summary,$step_1,$step_2,$step_3,$step_4);
			$data = array('status'=>true,'note'=>'Departure ticket on '.$dep_date.' is available!','form_manual_booking'=>$css.$fmb.$js);
		}
	}else{
		$data = array('status'=>false,'note'=>'Departure ticket on '.$dep_date.' is not available!');	
	}	
	return $data;
}
function ticket_calendar_manual_booking(){
	global $db;
	$boat		= 'Pacha Express';
	//var from post
	$trip_type 	= $_POST['trip_type'];
	$from		= $_POST['from'];
	$to			= $_POST['to'];
	$dep_date	= $_POST['dep_date'];
	$ret_date	= $_POST['ret_date'];
	$num_adult	= $_POST['adult'];
	$num_child	= $_POST['child'];
	$num_infant	= $_POST['infant'];	
	
	//validasi disini
	if ($trip_type=='1'){
		if (!isset($_POST['schedule_id_return'])){
			$data = array('status'=>false,'note'=>'Please select return scedule.');
			return $data;
		}
	}
	
	//prepare var
	$sid		= $_POST['schedule_id_departure'];//departure
	$rid		= ticket_front_get_schedule_by_id($sid,'rid');
	$total_dep	= ticket_calculate_total($trip_type,$rid,$sid,$num_adult,$num_child,$num_infant);
	
	//B got special price?
	$got_sp		= is_get_special_rate($rid,$sid,$dep_date);
	$srid		= '0'; //special_rate_id
	if ($got_sp!=false){
		$dep_get_sp = true;
		$data_sp	= ticket_get_data_rows($got_sp);
		$srid		= $data_sp['srid'];
		$total_dep	= ticket_calculate_total_apply_special_price($trip_type,$num_adult,$num_child,$num_infant,$data_sp);
	}
	//E got special price?
	
	/* =============ini adalah cara lama yaitu harga pergi dan return dibedakan==================
	if ($trip_type=='1'){
		
		$sid_ret	= $_POST['schedule_id_return']; //return
		$rid_ret	= ticket_front_route_id($to, $from);//return
		
		$total_ret	= ticket_calculate_total($trip_type,$rid_ret,$sid_ret,$num_adult,$num_child,$num_infant);
		
		//B got special price?
		$got_sp_ret		= is_get_special_rate($rid_ret,$sid_ret,$ret_date);
		$srid_ret		= '';
		if ($got_sp_ret!=false){
			$ret_get_sp = true;
			$data_sp_ret= ticket_get_data_rows($got_sp_ret);
			$srid_ret	= $data_sp_ret['srid'];
			$total_ret	= ticket_calculate_total_apply_special_price($trip_type,$num_adult,$num_child,$num_infant,$data_sp_ret);
		}
		//E got special price?
		
		$total		= $total_dep + $total_ret;
			
	}else{
		$total		= $total_dep;
	}
	//============================================================================================	*/	
		
	$total		= $total_dep;
	
	$num_pass 	= $num_adult + $num_child + $num_infant;	
	
	$bby 		= generate_booking_by_id();
	$bid 		= get_next_increment_id('ticket_booking');		
	$no_ticket 	= generate_ticket_no($trip_type);
	$status		= 'pd';	
	
	//cek lagi sebelum simpan, apakah ticket msh tersedia atau sudah terbooking terlebih dahulu oleh applicant lain
	//untuk menghindari applicant melakukan booking pd waktu yg bersamaan
	if ($trip_type=='1'){		
		$sid_dep = $_POST['schedule_id_departure'];
		$rid_dep = ticket_front_get_schedule_by_id($sid_dep,'rid');	
		$date_dep= $dep_date;
			
		$sid_ret = $_POST['schedule_id_return'];
		$rid_ret = ticket_front_get_schedule_by_id($sid_ret,'rid');
		$date_ret= $ret_date;
		
		if ((is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true) &&
			(is_ticket_over_quota($rid_ret,$sid_ret,$date_ret,$num_adult,$num_child,$num_infant)==true)
			){			
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure & return is over quota');
			return $data;
		}elseif (is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure is over quota');
			return $data;
		}elseif(is_ticket_over_quota($rid_ret,$sid_ret,$date_ret,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket return is over quota');
			return data;
		}
	}elseif($trip_type=='0'){
		$sid_dep = $_POST['schedule_id_departure'];
		$rid_dep = ticket_front_get_schedule_by_id($sid_dep,'rid');	
		$date_dep= $dep_date;
		if (is_ticket_over_quota($rid_dep,$sid_dep,$date_dep,$num_adult,$num_child,$num_infant)==true){
			$data = array('status'=>false,'note'=>'Save data booking is failed','reason'=>'Ticket departure is over quota');
			return $data;
		}
	}
	
	//insert ticket_booking
	$q = $db->prepare_query("INSERT INTO ticket_booking 
			(bid, type, no_ticket, 
			boat, num_passenger, total, 
			status, created_date, created_by, 
			booked_by, dlu,srid) 
			VALUES 
			(%d, %d, %s, 
			%s, %d, %d, 
			%s, %d, %s, 
			%s, %d,%d)",
			$bid,$trip_type,$no_ticket,
			$boat,$num_pass,$total,
			$status,time(),$_COOKIE['username'],
			$bby,time(),$srid
		 );
	$r = $db->do_query($q);
	
	//insert detail booking
	if ($r){		
		//detail booking departure
		$schedule_detail = ticket_front_get_schedule_by_id($sid);
		$route_detail	 = ticket_front_get_route_by_id($schedule_detail['rid']);
		
		if ($trip_type=='1'){	
			$price_adult	= $route_detail['rprice_2way'];
			$price_child	= $route_detail['rprice_2way_child'];			
			//got special price?
			if (isset($dep_get_sp) && $dep_get_sp==true){
				$price_adult = $data_sp['srprice_2way'];
				$price_child = $data_sp['srprice_2way_child'];
			}		
		}elseif ($trip_type=='0'){	
			$price_adult	= $route_detail['rprice_1way'];
			$price_child	= $route_detail['rprice_1way_child'];			
			//got special price?
			if (isset($dep_get_sp) && $dep_get_sp==true){
				$price_adult = $data_sp['srprice_1way'];
				$price_child = $data_sp['srprice_1way_child'];
			}			
		}
		
		$total_adult	= $num_adult *  $price_adult;
		$total_child	= $num_child *  $price_child;
		$total_infant	= 0;
		$total_dep		= $total_adult + $total_child + $total_infant;
		
		if ($trip_type==1){			
			$q0 = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`,
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'departure',$bid,$rid,
				$from,$to,$sid,$schedule_detail['sparent'],
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['dep_date'],
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				$price_adult,$price_child,0,
				$total_adult,$total_child,$total_infant,
				$total_dep,$status,time(),
				$_COOKIE['username'],time(),$srid				
				);
				
			//detail booking return
			$schedule_detail = ticket_front_get_schedule_by_id($sid_ret);
			$route_detail	 = ticket_front_get_route_by_id($schedule_detail['rid']);
			$price_adult	 = $route_detail['rprice_1way'];
			$price_child	 = $route_detail['rprice_1way_child'];
			
			//B got special price?
			if (isset($ret_get_sp) && $ret_get_sp==true){
				$price_adult = $data_sp_ret['srprice_1way'];
				$price_child = $data_sp_ret['srprice_1way_child'];
			}
			//E got special price?
			
			$total_adult	= $num_adult *  $price_adult;
			$total_child	= $num_child *  $price_child;
			$total_infant	= 0;
			$total_dep		= $total_adult + $total_child + $total_infant;
			$q1 = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`, 
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'return',$bid,$rid_ret,//rid_ret
				$to,$from,$sid_ret,$schedule_detail['sparent'],//sid_ret, from & to di tukar
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['ret_date'], //ret_date
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				0,0,0,
				0,0,0,
				0,$status,time(),
				$_COOKIE['username'],time(),''				
				);
				
				//save
				$r0 = $db->do_query($q0);
				$r1 = $db->do_query($q1);				
				$insert_booking_detail = $r0 && $r1? true: false;
		}else{			
			$q = $db->prepare_query("INSERT INTO ticket_booking_detail 
				(`bdtype`,`bid`, `rid`, 
				`rfrom`, `rto`, `sid`, `sparent`, 
				`stime_departure`, `stime_arrive`, `date`, 
				`boat`, `num_adult`, `num_child`, 
				`num_infant`, `address_pickup`, `address_transfer`, 
				`price_per_adult`, `price_per_child`, `price_per_infant`, 
				`total_adult`, `total_child`, `total_infant`, 
				`total`, `status`, `created_date`,
				`created_by`, `dlu`, `srid`) VALUES 
				(%s,%d,%d,
				%s,%s,%d,%s,
				%s,%s,%s,
				%s,%d,%d,
				%d,%s,%s,
				%d,%d,%d,
				%d,%d,%d,
				%d,%s,%d,
				%s,%d,%d)",
				'departure',$bid,$rid,
				$from,$to,$sid,$schedule_detail['sparent'],
				$schedule_detail['stime_departure'],$schedule_detail['stime_arrive'],$_POST['dep_date'],
				$boat,$num_adult,$num_child,
				$num_infant,$_POST['add_pickup'],$_POST['add_transfer'],
				$price_adult,$price_child,0,
				$total_adult,$total_child,$total_infant,
				$total_dep,$status,time(),
				$_COOKIE['username'],time(),$srid				
				);
			$r = $db->do_query($q);
			$insert_booking_detail = $r? true: false;
		}
	}
	
	//insert passengger
	if ($insert_booking_detail){		
		for ($i=0;$i<$num_pass;$i++){
			$q = $db->prepare_query("INSERT INTO ticket_booking_passenger 
				(`bid`, `type`,`title`, 
				`fname`, `lname`, `country`, 
				`status`, `created_date`, `created_by`, 
				`dlu`) 
				VALUES
				(%d,%s,%s,
				%s,%s,%s,
				%s,%d,%s,
				%d)",
				$bid,$_POST['pass_type'][$i],'',
				$_POST['pass_fname'][$i],'',$_POST['pass_country'][$i],
				1,time(),$bby,
				time()
				);
			$r = $db->do_query($q);
		}
	}
	
	//insert aplicant/pemesan
	if ($insert_booking_detail){
		$q = $db->prepare_query("INSERT INTO ticket_booking_by 
				(`bbid`, `title`, `fname`, 
				`lname`, `address`, `country`, 
				`phone`, `email`, `created_date`, 
				`created_by`, `dlu`) 
				VALUES
				(%s,%s,%s,
				%s,%s,%s,
				%s,%s,%d,
				%s,%d)",
				$bby,$_POST['app_title'],$_POST['app_fname'],
				'',$_POST['app_address'],$_POST['app_country'],
				$_POST['app_phone'],$_POST['app_email'],time(),
				$_COOKIE['username'],time()
				);
		$r = $db->do_query($q);
	}	
	
	//return data
	if ($insert_booking_detail){
		$data = array(
				'status'=>true,
				'booking_id'=>$bid,
				'booking_by'=>$bby,
				'no_ticket'=>$no_ticket,
				'trip_type'=>$trip_type,
				'from'=>$from,
				'to'=>$to,
				'dep_date'=>$dep_date,
				'ret_date'=>$ret_date,
				'total'=>$total
				);
	}else{
		//roll back all data that has inserted [belum]
		$data = array('status'=>false,'note'=>'Save data booking is failed');
	}
	return $data;
}
?>
<?php
function ticket_calendar_ajax(){
	global $db;	
	add_actions('is_use_ajax', true);
	if(!is_user_logged()){
	 	exit('You have to login to access this page!');
	}
	
	//ticket_calendar_get_form_manual_booking
	if (isset($_POST['pKEY']) and $_POST['pKEY'] =='ticket_calendar_get_form_manual_booking'){
		echo ticket_calendar_get_form_manual_booking();	
		exit;	
	}
	
	//manual booking check avaibility
	if (isset($_POST['pKEY']) and $_POST['pKEY']=='ticket_calendar_manual_booking_check_avaibility'){
		$data = ticket_calendar_manual_booking_check_avaibility();
		echo json_encode($data);
		exit;
	}
		
	//proses manual booking
	if (isset($_POST['pKEY']) and $_POST['pKEY'] =='ticket_calendar_manual_booking'){
		$data = ticket_calendar_manual_booking();
		echo json_encode($data);
		exit;
	}	
}
?>