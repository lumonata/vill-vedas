<?php
	$filter_booking = array('rid'=>'','sparent'=>'','sid'=>'','status'=>'','date_start'=>'','date_end'=>'');
	if (ticket_booking_is_filter_view()){		
		if (isset($_POST['filter'])){			
			$filter_booking = array('rid'=>$_POST['rid'],'sparent'=>$_POST['sparent'],'sid'=>$_POST['sid'],'status'=>$_POST['status'],'date_start'=>$_POST['date_start'],'date_end'=>$_POST['date_end']);
		}elseif (isset($_GET['filter'])){
			$filter_booking = array('rid'=>$_GET['rid'],'sparent'=>$_GET['sparent'],'sid'=>$_GET['sid'],'status'=>$_GET['status'],'date_start'=>$_GET['date_start'],'date_end'=>$_GET['date_end']);
		}		
	}	
?>
<?php 
add_actions('booking','ticket_booking_index');
add_actions("ticket-booking-ajax_page","ticket_booking_ajax");
?>
<?php
function ticket_booking_index(){	
	return ticket_booking_table_data();
}
function ticket_booking_table_data(){
	global $db, $filter_booking;	
	add_actions('section_title','Booking Report');
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//order by
	if (isset($_GET['orderby'])){
		$field_orderby 	= $_GET['orderby'];
		$order_by 		= $field_orderby;
		$order 			= $_GET['order'];
	}else{
		$field_orderby 	= '';
		$order_by 		= 'A.bid';
		$order			= 'asc';
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;	
	$url=get_state_url('ticket&sub=booking')."&page=";
	
	if (ticket_booking_is_filter_view()){
		$url=get_state_url('ticket&sub=booking&filter=1&rid='.$filter_booking['rid'].'&sparent='.$filter_booking['sparent'].'&sid='.$filter_booking['sid'].'&status='.$filter_booking['status'].'&date_start='.$filter_booking['date_start'].'&date_end='.$filter_booking['date_end'].'&orderby='.$field_orderby.'&order='.$order)."&page=";
	}
	
	if (ticket_booking_is_filter_view()){
		$where_filter = '';		
		if ($filter_booking['rid']!=''){
			$where_filter .= ' AND B.rid="'.$filter_booking['rid'].'"';
		}
		if ($filter_booking['sparent']!=''){
			$where_filter .= ' AND B.sparent="'.$filter_booking['sparent'].'"';
		}
		if ($filter_booking['sid']!=''){
			$where_filter .= ' AND B.sid="'.$filter_booking['sid'].'"';
		}
		if ($filter_booking['status']!=''){
			$where_filter .= ' AND A.status="'.$filter_booking['status'].'"';
		}
		
		if ($filter_booking['date_start']!='' && $filter_booking['date_end']!=''){
			//$where_filter .= ' AND B.date="'.$filter_booking['date_start'].'"';
			$where_filter .= 'AND B.date >="'.$filter_booking['date_start'].'"';
			$where_filter .= 'AND B.date <="'.$filter_booking['date_end'].'"';
		}else{
			if ($filter_booking['date_start']!=''){
				$where_filter .= 'AND B.date >="'.$filter_booking['date_start'].'"';		
			}
			if ($filter_booking['date_end']!=''){
				$where_filter .= 'AND B.date <="'.$filter_booking['date_end'].'"';		
			}
		}	
		
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.bid=B.bid AND A.booked_by=C.bbid) $where_filter
				GROUP BY B.bid
				ORDER BY $order_by
				");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.bid=B.bid AND A.booked_by=C.bbid) $where_filter
				GROUP BY B.bid
        		ORDER BY $order_by $order
        		limit %d, %d",$limit,$viewed);		
	}elseif(is_search_booking()){ 
		$key = 	$_POST['s'];  	
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid) AND 
				(
					no_ticket like %s OR date like %s OR fname like %s 
				)
				GROUP BY B.bid
				ORDER BY $order_by $order
				",
				"%".$key."%","%".$key."%","%".$key."%");
		$num_rows=count_rows($sql);	
	}else{ 
		$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid)
				GROUP BY B.bid
				ORDER BY $order_by $order
				");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid)
				GROUP BY B.bid
        		ORDER BY $order_by $order
        		limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<!--<li>".button("button=add_new",get_state_url("ticket&sub=booking")."&prc=add_new")."</li>-->
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	$button = '';
	$filter = ticket_booking_filter_navigation();
	
	$url_ajax = 'http://'.SITE_URL.'/ticket-booking-ajax/';
	$list.="<h1>Booking</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('ticket&sub=booking')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".ticket_booking_search_box($url_ajax,'list_item','state=ticket&sub=booking&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('ticket&sub=booking')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"ticket&sub=booking\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
											$filter
									</ul>
							</div>						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />								
								<div class=\"pages_title\" style=\"width:10%;\"><a href='".ticket_booking_header_link('fname')."'>Booked By</a></div>	
								<div class=\"pages_title\" style=\"width:10%;\"><a href='".ticket_booking_header_link('no_ticket')."'>Ticket No.</a></div>	
								<div class=\"pages_title\" style=\"width:10%;\"><a href='".ticket_booking_header_link('rfrom')."'>Depart From</a></div>
								<div class=\"pages_title\" style=\"width:10%;\"><a href='".ticket_booking_header_link('rto')."'>Destination</a></div>									
								<div class=\"pages_title\" style=\"width:100px;text-align:center\"><a href='".ticket_booking_header_link('date')."'>Date</a></div>	
								<div class=\"pages_title\" style=\"width:80px;text-align:center\"><a href='".ticket_booking_header_link('stime_departure')."'>Time</a></div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\"><a href='".ticket_booking_header_link('type')."'>Trip</a></div>
								<div class=\"pages_title\" style=\"width:100px;text-align:center\"><a href='".ticket_booking_header_link('A.status')."'>Status</a></div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\"><a href='".ticket_booking_header_link('num_adult')."'>Adult</a></div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\"><a href='".ticket_booking_header_link('num_child')."'>Child</a></div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\"><a href='".ticket_booking_header_link('num_infant')."'>Infant</a></div>								
								<div class=\"pages_title\" style=\"width:80px;text-align:right\"><a href='".ticket_booking_header_link('total_paid')."'>Total</a></div>		
														
							</div>
							<div id=\"list_item\">";
	$list.=ticket_booking_list_data($result,$start_order);	
	
	$link_pdf = $num_rows > 0 ? "<a class='download-pdf' href='".ticket_booking_pdf_link()."' id=''>Download pdf</a>" : '';
	
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									".$link_pdf."
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	
	$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/ticket/js/booking.js" ></script>';		
	$list .=ticket_booking_view();
	return $list;
}
function ticket_booking_list_data($result,$i=1){
 		global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        //start list
        $total_adult = 0;
		$total_child = 0;
		$total_infant = 0;
		$total = 0;
        while($d=$db->fetch_array($result)){  
        		//summary
        		$total_adult = $total_adult + $d['num_adult'];
				$total_child = $total_child + $d['num_child'];
				$total_infant = $total_infant + $d['num_infant'];
				$total = $total + $d['total_paid'];
        		
        		$booked_by = !empty($d['fname']) ? $d['title'].' '.$d['fname'] : $d['booked_by']; 
        		$booked_by = trim($booked_by);    
        		$booking_status= ticket_booking_status($d['status']);  
        		$trip_type = $d['type']=='0'? 'one way': 'return';  	
        		
        		$link_cancel = '';
        		if ($d['status']=='pd'){
        			$link_cancel = "<a href=\"javascript:;\" rel=\"cancel_".$d['bid']."\">Cancel Booking</a> |";
        		}        			  		
				
        		$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['bid']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['bid']."\" style='margin-left:13px;' />
                                <div class=\"pages_title\" style=\"width:10%;\">".$booked_by."</div>	
								<div class=\"pages_title\" style=\"width:10%;\">".$d['no_ticket']."</div>	
								<div class=\"pages_title\" style=\"width:10%;\">".$d['rfrom']."</div>
								<div class=\"pages_title\" style=\"width:10%;\">".$d['rto']."</div>									
								<div class=\"pages_title\" style=\"width:100px;text-align:center\">".$d['date']."</div>
								<div class=\"pages_title\" style=\"width:80px;text-align:center\">".$d['stime_departure']."</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$trip_type."</div>	
								<div class=\"pages_title\" style=\"width:100px;text-align:center\">".$booking_status."</div>							
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$d['num_adult']."</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$d['num_child']."</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$d['num_infant']."</div>								
								<div class=\"pages_title\" style=\"width:80px;text-align:right\">".number_format($d['total_paid'],0,',','.')."</div>
										
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['bid']."\">
                                                <a href='#' rel='".$d['bid'].":".$d['sid']."' class='trigger_show_detail'>Show Detail</a> |
                                                ".$link_cancel."
                                                <a href=\"javascript:;\" rel=\"delete_".$d['bid']."\">Delete</a>
                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['bid']."').mouseover(function(){
                                                $('#the_navigation_".$d['bid']."').show();
                                        });
                                        $('#theitem_".$d['bid']."').mouseout(function(){
                                                $('#the_navigation_".$d['bid']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$d['no_ticket']."?";
				$url = 'http://'.SITE_URL.'/ticket-booking-ajax/';
                add_actions('admin_tail','ticket_booking_delete_confirmation_box',$d['bid'],$msg,$url,"theitem_".$d['bid'],'state=ticket&sub=booking&prc=delete&id='.$d['bid']);                
				
                $msg="Are you sure to cancel ".$d['no_ticket']." for ".$booked_by."?";
                add_actions('admin_tail','ticket_booking_cancel_confirmation_box',$d['bid'],$msg,$url,"theitem_".$d['bid'],'state=ticket&sub=booking&prc=cancel&id='.$d['bid']);
                $i++;                 
        }
        $list.=ticket_booking_summary_data($total_adult,$total_child,$total_infant,$total);
        return $list;
}
function ticket_booking_summary_data($total_adult,$total_child,$total_infant,$total){	
	$list="<div class=\"list_item clearfix\" id=\"theitem_summary\" style='background:#ccc;'>
                                <input type=\"checkbox\" class=\"title_checkbox select\" style='margin-left:13px;opacity: 0;' />
                                <div class=\"pages_title\" style=\"width:10%;\">&nbsp;</div>	
								<div class=\"pages_title\" style=\"width:10%;\">&nbsp;</div>	
								<div class=\"pages_title\" style=\"width:10%;\">&nbsp;</div>
								<div class=\"pages_title\" style=\"width:10%;\">&nbsp;</div>									
								<div class=\"pages_title\" style=\"width:100px;text-align:center\">&nbsp;</div>
								<div class=\"pages_title\" style=\"width:80px;text-align:center\">&nbsp;</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">&nbsp;</div>	
								<div class=\"pages_title\" style=\"width:100px;text-align:center\">&nbsp;</div>							
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$total_adult."</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$total_child."</div>
								<div class=\"pages_title\" style=\"width:50px;text-align:center\">".$total_infant."</div>								
								<div class=\"pages_title\" style=\"width:80px;text-align:right\">".number_format($total,0,',','.')."</div>								
             </div>";
	return $list;
}
function ticket_booking_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').live('click',function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							////$('select').show();
						   ////$('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_booking_delete',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_booking_cancel_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_cancel=yes&cancel_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"cancel_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_cancel\" value=\"yes\" class=\"button\" id=\"cancel_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_cancel\" value=\"no\" class=\"button\" id=\"cancel_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"cancel_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=cancel_".$id."]').click(function(){
							$('#cancel_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=cancel_".$id."]').live('click',function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#cancel_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#cancel_confirmation_wrapper_".$id."').show('fast');

						});
					});
					/*
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
					*/
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#cancel_no_".$id."').click(function(){
						$('select').show();
					    $('#cancel_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#cancel_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#cancel_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#cancel_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_booking_cancel',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								//$('#cancel_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#cancel_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_booking_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\" style='position:relative;'>
						<input type='submit' name='search_booking' value='yes' style='position:absolute;bottom:0;left:0;top:0;opacity:0;cursor:pointer;'>	
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"_search\" class=\"searchbutton\" value=\"yes\" />						
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'ticket_booking_search',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}
function ticket_booking_view(){
	//set template
	set_template(PLUGINS_PATH."/".ticket_template_dir()."/booking_view.html",'bV');
	//set block        
	add_block('bookingView','bView','bV');	
	add_variable('site_url', site_url());
	parse_template('bookingView', 'bView');
	
	return return_template('bV');
}
//function only
function is_search_booking(){
	if((isset($_POST['search_booking']) && $_POST['search_booking']=='yes'))
		return true;
	return false;		
}
function ticket_booking_filter_navigation(){
	global $db, $filter_booking; 
	$html = '';	
	$html .= '<select name="sparent" style="vertical-align:middle;" id="select_sparent"><option value="">All Departure</option>'.ticket_schedule_parent_option($filter_booking['sparent']).'</select> ';
	$html .= '<select name="rid" style="vertical-align:middle;" id="select_route"><option value="">All route</option>'.ticket_route_option($filter_booking['rid']).'</select> ';
	$html .= '<select name="sid" style="vertical-align:middle;min-width: 160px;display:none;" id="select_schedule"  rel="all"><option value="">All schedule</option>'.ticket_get_schedule_option($filter_booking['sid']).'</select> ';
	$html .= '<select name="status" style="vertical-align:middle;"><option value="">All status</option>'.ticket_status_option($filter_booking['status']).'</select> ';
	$html .= '<input  name="date_start" type="text" placeholder="Start date" value="'.$filter_booking['date_start'].'"  class="medium_textbox dpicker" style="min-width:120px;width:120px;vertical-align:middle;height:18px !important;"> ';
	$html .= '<input  name="date_end" type="text" placeholder="End date" value="'.$filter_booking['date_end'].'"  class="medium_textbox dpicker" style="min-width:120px;width:120px;vertical-align:middle;height:18px !important;"> ';
	$html .= '<input name="filter" type="submit" value="filter" style="display:inline-block;padding:4px 6px;vertical-align:middle;">';	 
	return $html;
}
function ticket_booking_is_filter_view(){
	if (isset($_POST['filter']) || isset($_GET['filter'])){
		return true;	
	}else{
		return false;
	}		
}
function ticket_booking_status($status){
	$status_array = array('pp'=>'unpaid','pd'=>'confirmed','cl'=>'cancelled','ar'=>'archive');
	
	if (isset($status_array[$status])){
		return $status_array[$status];
	}else{
		return $status;
	}
}
function ticket_booking_header_link($orderby){	
	global $filter_booking;	
	if (isset($_GET)){
		$param = '';
		foreach ($_GET as $index => $val){
			if ($index!='orderby' || $index!='order'){
				$param .= $index.'='.$val.'&';
			}
			
		}
		$order = 'asc';
		if (isset($_GET['order'])){
			if ($_GET['order']=='asc'){
				$order = 'desc';
			}
		}
		$param .= 'orderby='.$orderby.'&order='.$order;
		$url = 'http://'.site_url().'/lumonata-admin?'.$param;
	}else{		
		$url = $_SERVER['REQUEST_URI'].'&orderby='.$orderby;
	}
	if (ticket_booking_is_filter_view()){
		$param_filter = '&filter=1';
		foreach ($filter_booking as $index => $val) {
			$param_filter.='&'.$index.'='.$val;
		}
		$url.=$param_filter;
	}	
	return $url;
}
function ticket_booking_pdf_link(){	
	global $filter_booking;	
	$app = '/lumonata-plugins/ticket/ticket_booking_create_pdf.php?';
	if (isset($_GET)){
		$param = '';
		foreach ($_GET as $index => $val){
			if ($index!='orderby' || $index!='order'){
				$param .= $index.'='.$val.'&';
			}
			
		}		
		if (isset($_GET['orderby']))
			$param .= 'orderby='.$_GET['orderby'].'&order='.$_GET['order'];		
	}else{		
		$param = '&orderby=A.bid';
	}
	if (ticket_booking_is_filter_view()){
		$param_filter = '&filter=1';
		foreach ($filter_booking as $index => $val) {
			$param_filter.='&'.$index.'='.$val;
		}
		$param.=$param_filter;
	}	
	$param.='&act=create_pdf';
	
	$url = 'http://'.site_url().$app.$param;
	return $url;
}
function ticket_booking_delete($id){
	global $db;	
	$sql	=  'DELETE a.*, b.*, c.*,d.*
				FROM 				
					ticket_booking as a, ticket_booking_detail as b, ticket_booking_by as c,ticket_booking_passenger as d				
				WHERE 
					a.bid = b.bid AND
					a.booked_by = c.bbid AND
					a.bid = d.bid AND
					a.bid=%d';
	$query 	= $db->prepare_query($sql,$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
function ticket_booking_cancel($id){
	global $db;	
	$sql	=  'UPDATE				
					ticket_booking as a, ticket_booking_detail as b
				SET
					a.status=%s,a.dlu=%d,
					b.status=%s,b.dlu=%d				
				WHERE 
					a.bid = b.bid AND
					a.bid=%d';
	$query 	= $db->prepare_query($sql,'cl',time(),'cl',time(),$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
?>
<?php 
//mostly ajax function is here
function ticket_booking_show_detail($param){
	global $db;	
	
	set_template(PLUGINS_PATH."/".ticket_template_dir()."/booking_detail.html",'bD');		
	add_block('loopBookingDetail','bDetail','bD');	
	add_block('bookingDetail','bDetail','bD');	
	
	add_variable('site_url', site_url());
	
	//applicant_name
	$q = $db->prepare_query("SELECT ticket_booking_by.*,B.no_ticket,B.status as booking_status from ticket_booking as B,ticket_booking_by 
							WHERE B.booked_by=ticket_booking_by.bbid AND bid=%d",$param['bid']);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	$app_name = !empty($d['fname'])? $d['title'].' '.$d['fname'] : $d['bbid'];	
	$cbs = $d['booking_status']=='pd'?'confirmed':'unconfirmed';
	
	add_variable('no_ticket', $d['no_ticket']);	
	add_variable('class_booking_status',$cbs); //confirmed,unconfirmed,cancelled
	add_variable('booking_status_message',ucfirst($cbs));	
	add_variable('app_name',trim($app_name));
	add_variable('app_address', $d['address']);	
	add_variable('app_phone',  $d['phone']);
	add_variable('app_email',  $d['email']);	
	
	//booking detail	
	$q = $db->prepare_query("SELECT D.* from ticket_booking AS B,ticket_booking_detail AS D 
							WHERE B.bid=D.bid AND B.bid=%d ORDER BY bdid ASC",$param['bid']);
	$r = $db->do_query($q);	
	$num_adult = 0;
	$num_child = 0;
	$num_infant= 0;	
	while ($d=$db->fetch_array($r)){			
		add_variable('from', $d['rfrom']);
		add_variable('to', $d['rto']);
		add_variable('date', $d['date']);
		add_variable('time_dep', $d['stime_departure']);
		add_variable('price_per_adult', number_format($d['price_per_adult'],0,',','.'));
		add_variable('price_per_child', number_format($d['price_per_child'],0,',','.'));
		add_variable('total', number_format($d['total'],0,',','.'));
		parse_template('loopBookingDetail', 'bDetail',true);		
		
		$num_adult = $d['num_adult'];	
		$num_child = $d['num_child'];
		$num_infant= $d['num_infant']; 
		$address_pickup = $d['address_pickup']; 
		$address_transfer = $d['address_transfer'];
	}
	add_variable('num_adult', $num_adult);
	add_variable('num_child', $num_child );
	add_variable('num_infant', $num_infant);
	
	add_variable('pickup_address', $address_pickup);
	add_variable('transfer_address', $address_transfer);
	
	//passenger detail	
	$q = $db->prepare_query("SELECT * from ticket_booking_passenger 
							WHERE bid=%d ORDER BY bpid ASC",$param['bid']);
	$r = $db->do_query($q);	
	$data_passenger = '';
	while ($d=$db->fetch_array($r)){			
		$data_passenger .= '<tr>
								<td>'.$d['fname'].'</td>
								<td align="center">'.$d['country'].'</td>
								<td align="center">'.$d['type'].'</td>
							</tr>';
	}
	add_variable('row_data_passenger', $data_passenger);
	
	parse_template('bookingDetail', 'bDetail');	
	return return_template('bD');
}
function ticket_booking_create_pdf($param){
	
}
?>
<?php
function ticket_booking_ajax(){
	global $db,$filter_booking;	
	add_actions('is_use_ajax', true);
	if(!is_user_logged()){
	 	exit('You have to login to access this page!');
	}
		
	//show detail
	if (isset($_GET['pKEY']) and $_GET['pKEY'] =='show_detail'){
		echo ticket_booking_show_detail($_GET);
	}	
	
	//delete
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_booking_delete'){
		$id = $_POST['val_id'];
		if (ticket_booking_delete($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	//cancel booking
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_booking_cancel'){
		$id = $_POST['val_id'];
		if (ticket_booking_cancel($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	//search booking
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_booking_search'){
	   	$sql=$db->prepare_query("SELECT *,A.total as total_paid
				FROM ticket_booking as A,ticket_booking_detail AS B ,ticket_booking_by AS C								
				WHERE (A.status!='ar' AND A.bid=B.bid AND A.booked_by=C.bbid) AND 
				(
					no_ticket like %s OR date like %s OR fname like %s 
				)
				GROUP BY B.bid
				ORDER BY A.bid ASC
				",
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
	 
		$r=$db->do_query($sql);
        if($db->num_rows($r) > 0){			
			echo ticket_booking_list_data($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}	
}
?>