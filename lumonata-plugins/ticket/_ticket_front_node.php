<?php
function node_is_ticket_available($rid,$sid,$date,$num_passenger){
	$sparent 	= ticket_front_get_schedule_by_id($sid,'sparent');
	$av_quota	= node_get_available_quota($rid,$sparent,$date);
	if ($av_quota>=$num_passenger){
		return true;
	}else{
		return false; 
	}
	
}

function node_get_available_quota($rid,$schedule_parent,$date){
	/*node = titik keberangkatan, nq = num quota, nb = num booked, aq = available quota*/
	global $pacha_node;
	$from = node_get_detail_route($rid,'rfrom');
	$selected_node = array_search($from, $pacha_node);
	$nb = 0;
	$nq = ticket_get_allotment_from_route($rid);
	foreach ($pacha_node as $node => $val){
		if ($node!=$selected_node){
			$nb = $nb + node_get_num_booking($selected_node,$node,$schedule_parent,$date);
		}
	}
	$aq = $nq-$nb;
	return $aq;
}
function node_get_num_booking($from,$to,$schedule_parent,$date){//a,b,schedule_parent,date
	global $db;
	$rid = node_get_route_id_by_node($from,$to);
	$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE rid=%d AND sparent=%d AND date=%s",$rid,$schedule_parent,$date);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	
	if ($n==0){
		return $n;
	}else{
		$nb = 0;
		while ($d = $db->fetch_array($r)){
			$nb = $nb + $d['num_adult'] + $d['num_child'];
		}
		return $nb;
	}
}
function node_get_detail_route($rid,$field=''){
	global $db;
	$q = $db->prepare_query("SELECT * FROM ticket_route WHERE rid=%d",$rid);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	if ($field!=''){
		if (isset($d[$field])){
			return $d[$field];
		}else{
			return false;
		}
	}else{
		return $d;
	}
}
function node_get_route_id_by_node($node_from,$node_to){
	global $pacha_node;
	$rid = ticket_front_route_id($pacha_node[$node_from], $pacha_node[$node_to]);
	return $rid;
}
?>