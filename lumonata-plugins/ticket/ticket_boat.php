<?php
if (isset($_POST['update_sort_id']) && isset($_POST['state']) && $_POST['state']=='ticket' && $_POST['sub']=='boat' ){
	ticket_boat_update_sort_id();
	exit;
}

add_actions('boat','ticket_boat_index');
add_actions("ticket-boat-ajax_page","ticket_boat_ajax");
function ticket_boat_index(){	
	if(is_add_new()){			
		return ticket_boat_new() ;
	}elseif(is_edit()){ 
		return ticket_boat_edit($_GET['id']);
	}elseif(is_edit_all() && isset($_POST['select'])){
		
	}elseif(is_delete_all()){
		return ticket_boat_delete_multiple(); 
	}elseif(is_confirm_delete()){
		foreach($_POST['id'] as $key=>$val){ticket_boat_delete($val);}
	}
	
	//Display data boat
    if(ticket_boat_num()>0){
        return ticket_boat_table_data();
    }else{			
        return ticket_boat_new();
    }	
}
function ticket_boat_new(){
	ticket_boat_set_template('boat_new.html','tplboatNew','boatNew','rNew');
	$i 		= 0;
	$id 	= '';
	$button	= "";
	$button	.="	
	<li>".button("button=add_new",get_state_url('ticket&sub=boat')."&prc=add_new")."</li>	
	<li>".button("button=save_changes&label=Save")."</li>
	<li>".button("button=cancel",get_state_url('ticket&sub=boat'))."</li>";
		
	
	add_actions('section_title','Boat - Add New');
	add_variable('title_Form','Boat');	
	if (isset($_POST['save_changes'])){
		$validation = true;		
		$error	= '';
		//validation
		if ($_POST['bname'][$i]==''){			
			$error .= '<div class="error_red">Please type boat name</div>';			
			$validation = false;
		}
		if ($_POST['ballotment'][$i]==''){			
			$error .= '<div class="error_red">Please type allotment</div>';			
			$validation = false;
		}	
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_bname',$_POST['bname'][$i]);	
			add_variable('val_bcode',$_POST['bcode'][$i]);			
			add_variable('val_ballotment',$_POST['ballotment'][$i]);
		}
		
		//save
		if ($validation==true){
			if (ticket_boat_insert()){
				$error = '<div class="error_green">Add boat <code>'.$_POST['bname'][$i].'</code> has save succesfully.</div>';
				add_variable('error',$error);	
				add_variable('val_bname','');	
				add_variable('val_bcode','');	
				add_variable('val_ballotment','');	
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}		
	}
	
	add_variable('i',$i);	
	add_variable('status_checked_publish','checked');
	add_variable('style_other_option','display:none;');
	add_variable('button',$button);	
	parse_template('loopPage','lPage',false);
	 
	return ticket_boat_return_template();
}
function ticket_boat_edit($id){
	global $db;

	$index=0;
	$button="";
	ticket_boat_set_template();

	$button.="
			<li>".button("button=add_new",get_state_url('ticket&sub=boat')."&prc=add_new")."</li>
		   	<li>".button("button=save_changes&label=Save")."</li>
			<li>".button("button=cancel",get_state_url('ticket&sub=boat'))."</li>";
	
	//set the page Title
	add_actions('section_title','Boat - Edit');

	//echo "is_single_edit";
	add_variable('title_Form','Edit boat');
		
	if (isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit'){			
		$i 	= 0;
		$id = $_GET['id'];					
		$validation = true;		
		$error	= '';		
		//validation
		if ($_POST['bname'][$i]==''){			
			$error .= '<div class="error_red">Please type boat name</div>';			
			$validation = false;
		}
		if ($_POST['ballotment'][$i]==''){			
			$error .= '<div class="error_red">Please type allotment</div>';			
			$validation = false;
		}
		
		if ($validation==false){
			add_variable('error',$error);
			add_variable('val_bname',$_POST['bname'][$i]);	
			add_variable('val_bcode',$_POST['bcode'][$i]);			
			add_variable('val_ballotment',$_POST['ballotment'][$i]);	
		}
		
		//save
		if ($validation==true){
			if (ticket_boat_update()){
				$msg_update = $_POST['bname'][$i].' has been updated succesfully.';
				if (isset($_POST['update_allotment_all_schedule'])){
					if (ticket_boat_update_schedule_allotment()){
						$msg_update .= '<br>Update is applied to all related schedules.'; 
					}
				}
				$error = '<div class="error_green">'.$msg_update.'</div>';
				add_variable('error',$error);								
			}else{
				$error = '<div class="error_red">Something wrong, please try again.</div>';
				add_variable('error',$error);
			}
		}			
			
	}

	//show data from db
	$q = $db->prepare_query("SELECT * from ticket_master_boat WHERE boat_id=%d",$id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);	
	add_variable('val_boat_id',$d['boat_id']);	
	add_variable('val_bname',$d['bname']);	
	add_variable('val_bcode',$d['bcode']);	
	add_variable('val_ballotment',$d['ballotment']);	
	
	if ($d['status']==1){
		add_variable('status_checked_publish','checked');	
	}else{
		add_variable('status_checked_not_publish','checked');
	}
	
	add_variable('i',$index);
	parse_template('loopPage','lPage',false);
	
	add_variable('button',$button);
	return ticket_boat_return_template();
}
function ticket_boat_table_data(){
	global $db;
	
	$app_name = 'ticket_boat';
	$list='';
	$option_viewed="";
	$data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
	
	if(isset($_POST['data_to_show']))
		$show_data=$_POST['data_to_show'];
	elseif(isset($_GET['data_to_show']))
		$show_data=$_GET['data_to_show'];
   
	
	foreach($data_to_show as $key=>$val){
		if(isset($show_data)){
			if($show_data==$key){
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
			}else{
				$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
			}
		}elseif($key=='all'){
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
		}else{
			$option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
		}
	}
	
	
	
	if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
		$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	}else{
		$w="";
	}
	
	//paging system
	$viewed=list_viewed();
	if(isset($_GET['page'])){
		$page= $_GET['page'];
	}else{
		$page=1;
	}
	
	$limit=($page-1)*$viewed;
	$url=get_state_url('ticket&sub=boat')."&page=";	
	
	if(is_search_boat()){ 
		$key = 	$_POST['s'];
		$sql=$db->prepare_query("select * from ticket_master_boat 
				WHERE  bname like %s OR bcode like %s OR ballotment like %s order by sort_id ASC",
				"%".$key."%","%".$key."%","%".$key."%");
		$num_rows=count_rows($sql);	
	}else{ 
		$sql=$db->prepare_query("select * from ticket_master_boat ORDER BY sort_id ASC");
		$num_rows=count_rows($sql);
		       
        $sql=$db->prepare_query("select * from ticket_master_boat ORDER BY sort_id ASC limit %d, %d",$limit,$viewed);		
	}		
	
	$result=$db->do_query($sql);
	
	$start_order=($page - 1) * $viewed + 1; //start order number
	$button="
		<li>".button("button=add_new",get_state_url("ticket&sub=boat")."&prc=add_new")."</li>
		<li>".button('button=delete&type=submit&enable=false')."</li>
		<!--li>".button('button=unpublish&type=submit&enable=false')."</li-->";
	
	
	$url_ajax = 'http://'.SITE_URL.'/ticket-boat-ajax/';
	$list.="<h1>Boat</h1>
			<div class=\"tab_container\"> 	
				<div class=\"single_content\">
					<div id=\"response\"></div>
					<form action=\"".get_state_url('ticket&sub=boat')."\" method=\"post\" name=\"alist\">
					   <div class=\"button_right\">
							".ticket_boat_search_box($url_ajax,'list_item','state=ticket&sub=boat&prc=search&','right','alert_green_form','Search')."
							
					   </div>
					   <br clear=\"all\" />
					    <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('ticket&sub=boat')."\" />
					   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
					   <input type=\"hidden\" name=\"state\" value=\"ticket&sub=boat\" />
						<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
									<ul class=\"button_navigation\">
											$button
									</ul>
							</div>						   
						</div>
						<!--div class=\"status_to_show\">Show: $option_viewed</div-->
						<div class=\"list\">
							<div class=\"list_title\">
								<input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />								
								<div class=\"pages_title\" style=\"width:20%;\">Name</div>	
								<div class=\"pages_title\" style=\"width:20%;\">Code</div>										
								<div class=\"pages_title\" style=\"width:20%;\">&nbsp;&nbsp;Allotment</div>																
								<div class=\"pages_title\" style=\"width:15%;\">&nbsp;&nbsp;Status</div>								
							</div>
							<div id=\"list_item\">";
	$list.=ticket_boat_list_data($result,$start_order);
	$list.="		</div>
						</div>
					</form>
					
					<div class=\"button_wrapper clearfix\">
							<div class=\"button_left\">
								<ul class=\"button_navigation\">
									
								</ul>   
							</div>
					</div>
					<div class=\"paging_right\">
								".paging($url,$num_rows,$page,$viewed,5)."
					</div>
				</div>
			</div>
		";
	
	$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/ticket/js/list.js" ></script>';	
	add_actions('section_title','Boat');
	return $list;
}
function ticket_boat_list_data($result,$i=1){
 		global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No data found</div>";
        }
        
        while($d=$db->fetch_array($result)){            		
        		$status_publish = $d['status']? 'Publish': 'Not publish';  		
				$list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['boat_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['boat_id']."\" style='margin-left:13px;' />
                                <div class=\"pages_title\" style=\"width:20%;\">".$d['bname']."</div>	
                                <div class=\"pages_title\" style=\"width:20%;\">".$d['bcode']."</div>	
								<div class=\"pages_title\" style=\"width:20%;\">".$d['ballotment']."</div>																		
								<div class=\"pages_title\" style=\"width:15%;\">".$status_publish."</div>		
								
								
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['boat_id']."\">
                                                <a href=\"".get_state_url('ticket&sub=boat')."&prc=edit&id=".$d['boat_id']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['boat_id']."\">Delete</a>

                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['boat_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['boat_id']."').show();
                                        });
                                        $('#theitem_".$d['boat_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['boat_id']."').hide();
                                        });
                                </script>
                                
                        </div>";
                $msg="Are you sure to delete ".$d['bname']."?";
				$url = 'http://'.SITE_URL.'/ticket-boat-ajax/';
                add_actions('admin_tail','ticket_boat_delete_confirmation_box',$d['boat_id'],$msg,$url,"theitem_".$d['boat_id'],'state=ticket&sub=boat&prc=delete&id='.$d['boat_id']);                
                $i++;                 
        }
        return $list;
}
function ticket_boat_delete_multiple(){
	 global $db;
	 add_actions('section_title','Delete boat');
				$warning="<h1>Boat</h1>
                <div class=\"tab_container\">
                    <div class=\"single_content\">";
                $warning.="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this boat:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these boats:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
						$q = $db->prepare_query("SELECT * FROM ticket_master_boat WHERE boat_id=%d",$val);
						$r = $db->do_query($q);
                        $d = $db->fetch_array($r);
                        $warning.="<li>".$d['bname']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$val."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('ticket&sub=boat')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
				$warning.="</div></div>";
                
    return $warning;	
}
function ticket_boat_search_box($url='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
						<input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\" style='position:relative;'>						
						<input type='submit' name='search_boat' value='yes' style='position:absolute;bottom:0;left:0;top:0;opacity:0;cursor:pointer;'>
						<input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"_search\" class=\"searchbutton\" value=\"yes\" />
					</div>
				</div>
				<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
					<img src=\"". get_theme_img() ."/loader.gif\"  />
				</div>
				";
			
	if(!empty($url)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					var s = $('input[name=s]').val();
					var parameter='".$param."s='+s;
					
					$('#search_loader').show();
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{
							jpKEY : 'ticket_boat_search',
							val_s : s
						}, function(data){
								$('#".$results_id."').html(data);
								$('#search_loader').hide();
						});
					
					
					$('#response').html('');
					
				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}
//function only
function is_search_boat(){		
	if((isset($_POST['search_boat']) && $_POST['search_boat']=='yes'))
		return true;
	return false;		
}
function ticket_boat_delete($id){
	global $db;
	$query = $db->prepare_query("DELETE FROM ticket_master_boat Where boat_id = %d",$id);
	$result = $db->do_query($query);
	if($result){
		return true;
	}else{
		return false;
	}
}
function ticket_boat_delete_confirmation_box($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						$.post('".$url."', '".$var_no."', function(theResponse){
							$('#response').html(theResponse);
						});
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
				$('#delete_yes_".$id."').click(function(){
					
					var thaUrl = '".$url."';
					//alert (thaUrl);
					jQuery.post(thaUrl,{ 
							jpKEY : 'ticket_boat_delete',
							val_id : ".$id."
						}, function(data){
							//alert(data);
							if (data='true'){							
								$('#delete_confirmation_wrapper_".$id."').hide('fast'); 
								$('#".$close_frameid."').css('background','#FF6666');
								$('#".$close_frameid."').delay(500);
								$('#".$close_frameid."').fadeOut(700);
								setTimeout(
									function(){
										location.reload(true);
									}, 1500);
									
								return false;
							}
						});
					
					
					/*
					$('select').show();
				    $.post('".$url."', '".$var."', function(theResponse){
						$('#response').html(theResponse);
					});
				    $('#delete_confirmation_wrapper_".$id."').hide('fast');
				    $('#".$close_frameid."').css('background','#FF6666');
				    $('#".$close_frameid."').delay(500);
				    $('#".$close_frameid."').fadeOut(700);
				    setTimeout(
				    	function(){
				  			location.reload(true);
                    	}, 1500);
                    	
				    return false;
					*/
					
				});
			    });
		";
		$delbox.="</script>";
		
		return $delbox;
	}
function ticket_boat_option($selected_boat=''){
	global $db;
	$html = '';
	$q = $db->prepare_query("SELECT * from ticket_master_boat where status=%s order by sort_id ASC",'1');
	$r = $db->do_query($q);
	while ($d=$db->fetch_array($r)){
		$selected = '';
		if ($d['boat_id']==$selected_boat){
			$selected = 'selected';
		}
		$html .='<option value="'.$d['boat_id'].'" '.$selected.'>'.$d['bname'].'</option>';
	}
	return $html;
}
function ticket_boat_get_detail_by_id($boat_id,$field=''){
	global $db;
	$q = $db->prepare_query("SELECT * from ticket_master_boat WHERE boat_id=%d",$boat_id);
	$r = $db->do_query($q);
	$d = $db->fetch_array($r);
	if ($field!=''){
		if (isset($d[$field])) {
			return $d[$field];
		}else{
			return $d;
		}	
	}
	return $d;	
}
function ticket_boat_insert($i=0){
	global $db;
	$q = $db->prepare_query("INSERT INTO ticket_master_boat 
			(bname,ballotment,bcode,
			sort_id,status,created_by,created_date) VALUES 
			(%s,%d,%s,			
			%d,%d,%s,%d)",
			$_POST['bname'][$i],$_POST['ballotment'][$i],$_POST['bcode'][$i],			
			0,$_POST['status'][$i],$_COOKIE['username'],time()			
			);
	$r = $db->do_query($q);
	ticket_boat_increment_sort_id();
	return $r;
}
function ticket_boat_update($i=0){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_master_boat SET
			bname=%s,ballotment=%d,bcode=%s,
			status=%d,dlu=%d WHERE boat_id=%d",			
			$_POST['bname'][$i],$_POST['ballotment'][$i],$_POST['bcode'][$i],				
			$_POST['status'][$i],time(),$_POST['boat_id'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_boat_update_schedule_allotment($i=0){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_schedule SET sallotment=%d WHERE sboat=%d",			
			$_POST['ballotment'][$i],$_POST['boat_id'][$i]			
			);
	$r = $db->do_query($q);
	return $r;
}
function ticket_boat_increment_sort_id(){
	global $db;
	$q = $db->prepare_query("UPDATE ticket_master_boat SET sort_id=sort_id+1");
	return $db->do_query($q);
}
function ticket_boat_num(){
	global $db;
	
	$q = $db->prepare_query("SELECT * FROM ticket_master_boat");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	return $n;
}
function ticket_boat_set_template(){
        //set template
        set_template(PLUGINS_PATH."/".ticket_template_dir()."/boat_new.html",'template_boat_new');
        //set block
        add_block('loopPage','lPage','template_boat_new');
        add_block('boatNew','rNew','template_boat_new');
}

function ticket_boat_return_template($loop=false){       
        parse_template('boatNew','rNew',$loop);
        return return_template('template_boat_new');
}
function ticket_boat_ajax(){
	global $db;
	$app_name = 'ticket_boat'; 
	add_actions('is_use_ajax', true);
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_boat_delete'){
		$id = $_POST['val_id'];
		if (ticket_boat_delete($id)){
			echo "true";
		}else{
			echo "false";
		}
	}	
	
	
	if (isset($_POST['jpKEY']) and $_POST['jpKEY'] =='ticket_boat_search'){
		if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
			$w=" lpost_by=".$_COOKIE['user_id']." AND ";    
	    }else{
			$w="";
	    }	 

	    $sql=$db->prepare_query("select * from ticket_master_boat 
				WHERE				
				(
				bname like %s OR
				bcode like %s OR 
				
				ballotment like %s			
				)
				order by sort_id ASC",
	    
				"%".$_POST['val_s']."%","%".$_POST['val_s']."%","%".$_POST['val_s']."%");
	 
		 $r=$db->do_query($sql);
        if($db->num_rows($r) > 0){			
			echo ticket_boat_list_data($r);
        }else{
            echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['val_s']."</em>. Check your spellling or try another terms</div>";
        }
	}	
}
function ticket_boat_update_sort_id(){
	global $db;
	$items 		= $_POST['theitem'];
	$start 		= $_POST['start'];	
 	
	foreach($items as $key=>$val){
         $sql=$db->prepare_query("UPDATE ticket_master_boat SET 
                                     sort_id=%d,dlu=%d WHERE boat_id=%d",
                                     $key+$start,time(),$val);
            $db->do_query($sql);     
    }
}
?>