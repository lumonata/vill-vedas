<?php 
class ticketAvailability{	
	var $rid,$sid,$sparent,$from,$to,$date;
	var $node_from,$node_to; //a/b/c//d/e
	var $num_adult,$num_child,$num_passenger;
	var $allotment;
	
	function ticketAvailability($rid,$sid,$date,$num_adult,$num_child){
		global $pacha_node;
		$route 				= ticket_front_get_route_by_id($rid);	
		
		$this->rid 			= $rid;
		$this->sid 			= $sid;
		$this->allotment 	= ticket_get_allotment($sid);
		$this->sparent 		= ticket_front_get_schedule_by_id($sid,'sparent');
		$this->from 		= $route['rfrom'];
		$this->to 			= $route['rto'];
		$this->date 		= $date;
		
		$this->num_adult 	= $num_adult;
		$this->num_child 	= $num_child;
		$this->num_passenger= $num_adult + $num_child;
		
		$this->node_from	= array_search(strtolower($this->from), array_map('strtolower', $pacha_node)); 
		$this->node_to		= array_search(strtolower($this->to ), array_map('strtolower', $pacha_node)); 
	}	
	function get_total_booking_to_destination($node_to){	
		global $db,$pacha_node;		
		$to= $pacha_node[$node_to];		
		$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE status=%s AND sparent=%d AND date=%s AND rto=%s",'pd',$this->sid,$this->sparent,$this->date,$to); 
		$r = $db->do_query($q);
		$n = $db->num_rows($r);
		if ($n==0){
			return $n;
		}else{
			$n = 0;
			while ($d=$db->fetch_array($r)){
				$n=$n+$d['num_adult']+$d['num_child'];
			}	
			return $n;
		}		
	}
	function get_total_booking_from_x_to_y($node_from,$node_to,$debug=0){
		global $db,$pacha_node;
		$rid = ticket_front_route_id($pacha_node[$node_from], $pacha_node[$node_to]);
		$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE status=%s AND rid=%d AND sparent=%d AND date=%s",'pd',$rid,$this->sparent,$this->date);
		$r = $db->do_query($q);
		
		if ($debug==1){
			//echo $q;
		}
		
		$n = $db->num_rows($r);
		if ($n==0){
			return $n;
		}else{
			$n = 0;
			while ($d=$db->fetch_array($r)){
				$n=$n+$d['num_adult']+$d['num_child'];
			}	
			return $n;			
		}
	}
	function TB($node_from,$node_to,$debug=0){
			//total booking dari $node_from menuju $node_to,
			//fungsi ini sama dg get_total_booking_from_x_to_y, hny nama fungsi yg diperpendek
			global $db,$pacha_node;
			$rid = ticket_front_route_id($pacha_node[$node_from], $pacha_node[$node_to]);
			$q = $db->prepare_query("SELECT * from ticket_booking_detail WHERE status=%s AND rid=%d AND sparent=%d AND date=%s",'pd',$rid,$this->sparent,$this->date);
			$r = $db->do_query($q);
			
			if ($debug==1){
				//echo $q;
			}
			
			$n = $db->num_rows($r);
			if ($n==0){
				return $n;
			}else{
				$n = 0;
				while ($d=$db->fetch_array($r)){
					$n=$n+$d['num_adult']+$d['num_child'];
				}	
				return $n;			
			}
		}
	function get_total_booking_the_next_node($node_from,$node_to){
		//jumlah ticket yg sudah terbooking utk setiap node yg akan dilewati dari $node_from menuju $node_to
		global $pacha_node,$pacha_node_number,$pacha_node_code;
		$index_from = $pacha_node_code[$node_from];
		$index_to 	= $pacha_node_code[$node_to];
		
		$total= 0;		
		if ($index_from<$index_to){
			$diff=$index_to-$index_from; 
			if ($diff>1){				
				for($i=0;$i<$diff;$i++){
					if ($i!=0){
						$from  = $pacha_node_number[$index_from+$i];	
						for($j=0;$j<count($pacha_node);$j++){							
							$to	   = $pacha_node_number[$j];
							if ($from!=$to){			 
								$total = $total + $this->get_total_booking_from_x_to_y($from,$to);
								//echo '<br> $this->get_total_booking_from_x_to_y('.$from.','.$to.')'. $this->get_total_booking_from_x_to_y($from,$to,$debug=1).'<br>';
							}						
						}						
					}
				}
			}	
			//echo '<br>Total : '.$total.'<br>';		
			return $total;
			
		}elseif($index_from>$index_to){
			$diff= $index_from-$index_to;
			switch ($diff){
				case 1 : $num_inc = 3; break;
				case 2 : $num_inc = 2; break;
				case 3 : $num_inc = 1; break;
				case 4 : $num_inc = 0; break;	
				default : $num_inc = 0; break;
			}
			//echo 'Num inc'. $num_inc.'<br>';
			for ($i=0;$i<$num_inc;$i++){	
				if ($i!=0){			
					if ($index_from==4){
						$start = $i;
					}else{
						$start = $index_from + $i;
					}					
					if ($start>4){
						$start = $start - 5;
					}
					$from  = $pacha_node_number[$start];	
					
					for($j=0;$j<count($pacha_node);$j++){							
						$to	   = $pacha_node_number[$j];
						if ($from!=$to){			 
							$total = $total + $this->get_total_booking_from_x_to_y($from,$to);
							//echo '<br> $this->get_total_booking_from_x_to_y('.$from.','.$to.')'. $this->get_total_booking_from_x_to_y($from,$to).'<br>';
						}						
					}		
				}
			}
			return $total;
		}
		return $total;		
	}
	
	function _get_total_available_allotment_from_node($node){		 
		/*fungsi ini ga dipake, msh ada bug pd NB3*/
		//$pacha_node = array('a'=>'Amed','b'=>'Gili Trawangan','c'=>'Gili Meno','d'=>'Gili Air','e'=>'Bangsal');
		//$av[a] = QQ  - (NB[ab]+NB[ac]+NB[ad]+NB[ae]) - (NB[eb]+NB[ec]+NB[ed]) - (NB[db]+NB[dc]) - NB[cb] 
		global $db,$pacha_node,$pacha_node_number;
		$num_node = count($pacha_node);
		
		//$nb1 = (NB[ab]+NB[ac]+NB[ad]+NB[ae])
		$nb1 = 0;			
		foreach ($pacha_node as $index => $val){
			if ($index!=$node){
				$nb1 = $nb1 + $this->get_total_booking_from_x_to_y($node, $index);
				//'<br>$this->get_total_booking_from_x_to_y('.$node.', '.$index.') : '.$this->get_total_booking_from_x_to_y($node, $index);
			}			
		}
		
		//get index node from
		$i=0;
		$selected_index = 0;
		foreach ($pacha_node as $index => $val){
			if ($index==$node){
				$selected_index = $i;
			}
			$i++;
		}
		//echo '<br>Selected index'.$selected_index.'<br>';
		//$nb2=(NB[eb]+NB[ec]+NB[ed])
		$nb2=0;		
		if ($selected_index==0){
			$index_from = $num_node-1;	
		/*				
		}elseif($selected_index==($num_node-1)){
			$index_from = 0;
		*/
		}else{
			$index_from = $selected_index -1 ;
		}		
		$node_from = $pacha_node_number[$index_from];
		$i=0;
		//echo '<br>NB2 :';
		foreach ($pacha_node as $index => $val){
			if (($index!=$node) && $i<=($num_node-1)){ 
				$nb2 = $nb2 + $this->get_total_booking_from_x_to_y($node_from, $index);
				//echo '<br>$this->get_total_booking_from_'.$node_from.'_to_'.$index.'('.$node_from.', '.$index.') : '.$this->get_total_booking_from_x_to_y($node_from, $index);
			}
			$i++;
		}
		
		//$nb3 = (NB[db]+NB[dc])
		$nb3=0;				
		if ($selected_index==0){
			$index_from = $num_node-2;	
		}elseif($selected_index==1){
			$index_from = $num_node-1;			
		}elseif($selected_index==($num_node-1)){
			$index_from = $num_node-3;		
		}else{
			$index_from = $selected_index-2;
		}		
		$node_from = $pacha_node_number[$index_from];
		//echo '<br>NB3 :';
		$i=0;		
		foreach ($pacha_node as $index => $val){
			if($selected_index==($num_node-1) && ($index!=$node) && $i<($num_node-2)){ //jika berangkat dari node terahir (e/bangsal)
				$nb3 = $nb3 + $this->get_total_booking_from_x_to_y($node_from, $index);	
				//echo '<br>$this->get_total_booking_from_x_to_y('.$node_from.', '.$index.') : '.$this->get_total_booking_from_x_to_y($node_from, $index);	
			}elseif(($index!=$node) && $i>=($num_node-2)){
				$nb3 = $nb3 + $this->get_total_booking_from_x_to_y($node_from, $index);				
				//echo '<br>$this->get_total_booking_from_x_to_y('.$node_from.', '.$index.') : '.$this->get_total_booking_from_x_to_y($node_from, $index);
			}
			$i++;
		}
		
		//$nb4=NB[cb]
		$nb4=0;
		switch ($selected_index){
			case '0':
				$nb4 = $nb4 + $this->get_total_booking_from_x_to_y($pacha_node_number[2], $pacha_node_number[1]); break;
			case '1':
				$nb4 = $nb4 + $this->get_total_booking_from_x_to_y($pacha_node_number[3], $pacha_node_number[2]); break;
			case '2':
				$nb4 = $nb4 + $this->get_total_booking_from_x_to_y($pacha_node_number[4], $pacha_node_number[3]); break;
			case '3':
				$nb4 = $nb4 + $this->get_total_booking_from_x_to_y($pacha_node_number[0], $pacha_node_number[4]); break;		
			case '4':
				$nb4 = $nb4 + $this->get_total_booking_from_x_to_y($pacha_node_number[1], $pacha_node_number[0]); break;				
		}
		
		//total
		/*
		echo '<br>node : '.$node;
		echo '<br>index : '.$selected_index;
		echo '<br>nb1 : '.$nb1;
		echo '<br>nb2 : '.$nb2;
		echo '<br>nb3 : '.$nb3;
		echo '<br>nb4 : '.$nb4;
		echo '<br><hr><br>';
		*/
		$a  = $this->allotment - ($nb1) - ($nb2) - ($nb3) - ($nb4);
		return $a;			 
	}
	
	function get_total_available_allotment_from_node($node){		
		switch($node){
			case 'a':				
				$tb =   ($this->TB('a','b')+$this->TB('a','c')+$this->TB('a','d')+$this->TB('a','e')) + ($this->TB('e','b')+$this->TB('e','c')+$this->TB('e','d')) + ($this->TB('d','b')+$this->TB('d','c')) + $this->TB('c','b');
				break;
			case 'b':
				$tb = 	($this->TB('b','c')+$this->TB('b','d')+$this->TB('b','e')+$this->TB('b','a')) + ($this->TB('a','c')+$this->TB('a','d')+$this->TB('a','e')) + ($this->TB('e','c')+$this->TB('e','d')) + $this->TB('d','c');
				break;
			case 'c':	
				$tb = 	($this->TB('c','d')+$this->TB('c','e')+$this->TB('c','a')+$this->TB('c','b')) + ($this->TB('b','d')+$this->TB('b','e')+$this->TB('b','a')) + ($this->TB('a','d')+$this->TB('a','e')) + $this->TB('e','d');
				break;
			case 'd':	
				$tb = 	($this->TB('d','e')+$this->TB('d','a')+$this->TB('d','b')+$this->TB('d','c')) + ($this->TB('c','e')+$this->TB('c','a')+$this->TB('c','b')) + ($this->TB('b','e')+$this->TB('b','a')) + $this->TB('a','e');
				break;
			case 'e':
				$tb = 	($this->TB('e','a')+$this->TB('e','b')+$this->TB('e','c')+$this->TB('e','d')) + ($this->TB('d','a')+$this->TB('d','b')+$this->TB('d','c')) + ($this->TB('c','a')+$this->TB('c','b')) + $this->TB('b','a');
				break;
		}
		$a = $this->allotment - $tb;	
		return $a;
	}
	
	function get_total_available_allotment_to_node($node_to){
		$a = $this->allotment - $this->get_total_booking_to_destination($node_to);
		return $a;
	}
	
	function get_total_available_ticket_from_x_to_y($node_from,$node_to){	
		$available_from = $this->get_total_available_allotment_from_node($node_from);
		$available_from = $available_from - $this->get_total_booking_the_next_node($node_from,$node_to);
		$available_to	= $this->get_total_available_allotment_to_node($node_to);
		
		if ($available_from<=$available_to){
			return $available_from;
		}else{
			return $available_to;
		}
	}
	
	//function proses for front
	function is_available(){		
		//echo 'allotment :'. $this->allotment.'<br>';
		//echo 'total_available_ticket_from_'.$this->node_from.'_to_'.$this->node_to.' :'. $this->get_total_available_ticket_from_x_to_y($this->node_from,$this->node_to).'<br>';
		//echo 'num_passenger :'. $this->num_passenger.'<br>';
		
		if ($this->num_passenger <= $this->get_total_available_ticket_from_x_to_y($this->node_from,$this->node_to)){
			return true;
		}else{
			return false;
		}
	}

	
}
?>