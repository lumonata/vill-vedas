<?php
/*
    Plugin Name: Gallery
    Plugin URL: 
    Description: This gallery
    Author: AM
    Author URL: 
    Version: 1.0
    
*/

/*This is the first important step that you have to do.
 * Who are allowed to access the application
 * */
add_privileges('administrator', 'gallery', 'insert');
add_privileges('administrator', 'gallery', 'update');
add_privileges('administrator', 'gallery', 'delete');


/*Custom Icon Menu*/
function menu_css_gallery(){
    return "<style type=\"text/css\">.lumonata_menu ul li.gallery{
                                background:url('../lumonata-plugins/gallery/images/ico-gallery.png') no-repeat left top;
                                }</style>";
}
/*Aply css this plugin*/
add_actions('header_elements','menu_css_gallery');
/*Custom Icon Menu*/
/* 
 * Add menu
 *  
 * */

add_main_menu(array('gallery'=>'Gallery'));
include ("gallery-admin.php");
include ("gallery-front.php");
include ("gallery-front-2.php");
?>
