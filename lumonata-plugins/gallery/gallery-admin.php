<?php
include 'functions.php';
if (isset($_POST['update_order']) && isset($_POST['state']) && $_POST['state']=='gallery' ){
	update_articles_order($_POST['theitem'],$_POST['start'],$_POST['state']);
	exit;
}else{
	add_actions("gallery","get_admin_gallery");	
}

function get_admin_gallery(){
		///jika diklik load more
        if(isset($_POST['loadMore']) && ($_POST['loadMore']=='gallery') ){
			get_gallery_list_ajax();			
			exit;
		}
		add_actions("header_elements","gallery_CSS");
		global $db;
		
        $post_id=0;
        //Publish or Save Draft Actions
        if(is_save_draft() || is_publish() || is_save_changes()){
            //set status and hook defined actions
            if(is_save_draft()){
                $status='draft';
                run_actions('page_draft');
            }elseif(is_publish()){
                $status='publish';
                run_actions('page_publish');
                
                if(isset($_POST['select'])){
                    foreach($_POST['select'] as $key=>$val){
                        update_articles_status($val,'publish');
                    }
                }
            }
            
            if(is_add_new()){
                echo "is_add_new";
                //Hook Add New Actions
                if(is_save_draft()){
                    run_actions('page_addnew_draft');
                }elseif(is_publish()){
                    run_actions('page_addnew_publish');
                }
                if(isset($_POST['allow_comments'][0]))
                    $comments="allowed";
                else
                    $comments="not_allowed";
                    
                    
                $title=$_POST['title'][0];
                
                if(!is_saved()){
                    //save the article and then syncronize with the attachment data if user do upload files    
                    save_article($title,$_POST['post'][0],$status,"pages",$comments,$_POST['sef_box'][0],$_POST['share_option'][0]);
                    $post_id=mysql_insert_id();
                    attachment_sync($_POST['post_id'][0],$post_id);
                    
                    //insert additional fields
                    if(isset($_POST['additional_fields'])){
                        foreach($_POST['additional_fields'] as $key=>$val){
                           foreach($val as $subkey=>$subval){
                                add_additional_field($post_id,$key,$subval,'pages');
                           }
                        }
                    }
                }else{
                    //Update the article because the data is already saved before
                    update_article($_POST['post_id'][0],$title,$_POST['post'][0],$status,"pages",$comments,$_POST['share_option'][0]);
                    
                	
                    
                    //update additional fields
                    if(isset($_POST['additional_fields'])){
                        foreach($_POST['additional_fields'] as $key=>$val){
                           foreach($val as $subkey=>$subval){
                                edit_additional_field($_POST['post_id'][0],$key,$subval,'pages');
                           }
                        }
                    }
                }
            }elseif(is_edit()){
            	//echo "is_edit";
                //Hook Single Edit Actions
                if(is_save_draft()){
                    run_actions('page_edit_draft');
                }elseif(is_publish()){
                    run_actions('page_edit_publish');
                }
                
                if(isset($_POST['allow_comments'][0]))
                    $comments="allowed";
                else
                    $comments="not_allowed";
                    
                
                //$title=$_POST['title'][0];
                
                //Update the article
                //update_article($_POST['post_id'][0],$title,$_POST['post'][0],$status,"pages",$comments,$_POST['share_option'][0]);
                
               	//B Update
	                	$index = 0;
	                	if (isset($_GET['tab']) and $_GET['tab']=='categories'){
	                		update_gallery_categories($index);
	                	}else{
	                		update_gallery($index);	
	                	}
	                	
				//E Update
                
                //update additional fields
                if(isset($_POST['additional_fields'])){
                    
                    foreach($_POST['additional_fields'] as $key=>$val){
                       foreach($val as $subkey=>$subval){
                            //edit_additional_field($_POST['post_id'][0],$key,$subval,'pages');
                       }
                    }
                }
            }elseif(is_edit_all()){
            	//echo "is_edit_all";
                //Hook Edit All Actions
                if(is_save_draft()){
                    run_actions('page_editall_draft');
                }elseif(is_publish()){
                    run_actions('page_editall_publish');
                }
                
                //Update the articles
                foreach($_POST['post_id'] as $index=>$value){
                    if(isset($_POST['allow_comments'][$index]))
                        $comments="allowed";
                    else
                        $comments="not_allowed";
                   
                    
                    //$title=$_POST['title'][$index];
                    //update_article($_POST['post_id'][$index],$title,$_POST['post'][$index],$status,"pages",$comments,$_POST['share_option'][$index]);
                    
                    //B Update
                		if (isset($_GET['tab']) and $_GET['tab']=='categories'){
	                		update_gallery_categories($index);
	                	}else{
	                		update_gallery($index);	
	                	}
					//E Update
                    
                    //update additional fields
                    if(isset($_POST['additional_fields'])){
                        foreach($_POST['additional_fields'] as $key=>$val){
                            //edit_additional_field($_POST['post_id'][$index],$key,$_POST['additional_fields'][$key][$index],'pages');
                        }
                    }
                   
                }
                
                
            }
        }elseif(is_unpublish()){
            if(isset($_POST['select'])){
                foreach($_POST['select'] as $key=>$val){
                    update_articles_status($val,'unpublish');
                }
            }
        }
        
        //Automatic to display add new when there is no records on database
        if(is_num_articles('type=gallery')==0 && !isset($_GET['prc']) && !isset($_GET['tab'])){
            //header("location:".get_state_url('gallery')."&prc=add_new");
            header("location:".get_state_url('gallery')."&tab=quick-add-new&prc=add_new");
        }
                
        
        //Display add new form
		if(is_add_new() and isset($_GET['tab']) and $_GET['tab']=='quick-add-new'){
            //return add_new_page($post_id) ;
            return  add_new_gallery_quick($post_id);
        }else if(is_add_new() and isset($_GET['tab']) and $_GET['tab']=='categories'){
            //return add_new_page($post_id) ;
            return  add_new_gallery_categories($post_id);
        }else if(is_add_new()){
            //return add_new_page($post_id) ;
            return  add_new_gallery($post_id);
        }elseif(is_edit()){ 
            if(is_contributor() || is_author()){
                if(is_num_articles("id=".$_GET['id'])>0){
                    return edit_gallery($_GET['id']);
                }else{
                    return "<div class=\"alert_red_form\">You don't have an authorization to access this page</div>";
                }
            }else{
            	if (isset($_GET['tab']) and $_GET['tab']=='categories'){
            		return edit_gallery_categories($_GET['id']);	
            	}else{
            		return edit_gallery($_GET['id']);
            	}
                
            }
        }elseif(is_edit_all() && isset($_POST['select']) and isset($_GET['tab']) and $_GET['tab']=='categories'){
            return edit_gallery_categories();
        }elseif(is_edit_all() && isset($_POST['select'])){
            return edit_gallery();
        }elseif(is_delete_all() and isset($_GET['tab']) and $_GET['tab']=='categories'){
                add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this gallery categorie:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these gallery categories:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                	
                        //$d=fetch_artciles("id=".$val);
                        
                        $sql=$db->prepare_query("select * from lumonata_rules		                
		                Where lrule_id=%d",$val);
                        $r=$db->do_query($sql);
                        $d=$db->fetch_array($r);
                        
                        $warning.="<li>".$d['lname']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$d['lrule_id']."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('gallery&tab=categories')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
                return $warning;
        }elseif(is_delete_all()){
                add_actions('section_title','Delete Comments');
                $warning="<form action=\"\" method=\"post\">";
                if(count($_POST['select'])==1)
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete this Gallery:</strong>";
                else
                        $warning.="<div class=\"alert_red_form\"><strong>Are you sure want to delete these Gallery:</strong>";
                        
                $warning.="<ol>";	
                foreach($_POST['select'] as $key=>$val){
                	
                        //$d=fetch_artciles("id=".$val);
                        
                        $sql=$db->prepare_query("select * from lumonata_articles		                
		                Where larticle_id=%d",$val);
                        $r=$db->do_query($sql);
                        $d=$db->fetch_array($r);
 
                        
                        $warning.="<li>".$d['larticle_title']."</li>";
                        $warning.="<input type=\"hidden\" name=\"id[]\" value=\"".$d['larticle_id']."\">";
                }
                $warning.="</ol></div>";
                $warning.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
                $warning.="<input type=\"submit\" name=\"confirm_delete\" value=\"Yes\" class=\"button\" />";
                $warning.="<input type=\"button\" name=\"confirm_delete\" value=\"No\" class=\"button\" onclick=\"location='".get_state_url('gallery')."'\" />";
                $warning.="</div>";
                $warning.="</form>";
                
                return $warning;
        }elseif(is_confirm_delete() and isset($_GET['tab']) and $_GET['tab']=='categories'){
                foreach($_POST['id'] as $key=>$val){
                       delete_rule($val);
                }
        }elseif(is_confirm_delete()){
                foreach($_POST['id'] as $key=>$val){
                       	$app_name = 'gallery';
                       	$file_name = get_additional_field($val, 'image', $app_name);
                       	$destination1=PLUGINS_PATH."/gallery/files/".$file_name;
	                 	$destination2=PLUGINS_PATH."/gallery/files/thumbs/".$file_name;
	                 	$destination3=PLUGINS_PATH."/gallery/files/thumbs2/".$file_name;
                       	delete_file($destination1);
                       	delete_file($destination2);
                       	delete_file($destination3);
                       	delete_additional_field($val, $app_name);
                       	delete_rules_relationship("app_id=$val");
                       	delete_article($val,$app_name);
                }
        }
        
        //Display Users Lists
        //if(is_num_articles('type=gallery')>0){
                add_actions('header_elements','get_javascript','jquery_ui');
                //add_actions('header_elements','get_javascript','articles_list');
                
                if (isset($_GET['tab']) and $_GET['tab']=='gallery'){
                	return get_gallery_list('gallery');
                }else if (isset($_GET['tab']) and $_GET['tab']=='categories'){
                	return get_gallery_categories_list();
                }else{
                	return get_gallery_list();
                }
        //}
                
    }

    
        function get_gallery_categories_list($type='gallery'){    
        global $db;
        $list='';
        $option_viewed="";
        $data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
        
        if(isset($_POST['data_to_show']))
            $show_data=$_POST['data_to_show'];
        elseif(isset($_GET['data_to_show']))
            $show_data=$_GET['data_to_show'];
       
        
        foreach($data_to_show as $key=>$val){
            if(isset($show_data)){
                if($show_data==$key){
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
                }else{
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
                }
            }elseif($key=='all'){
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
            }else{
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
            }
        }
        
        
        
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }
        
        if(is_search()){
                $sql=$db->prepare_query("select * from lumonata_articles where $w larticle_type=%s and (larticle_title like %s or larticle_content like %s)",$type,"%".$_POST['s']."%","%".$_POST['s']."%");
                $num_rows=count_rows($sql);
                
        }else{
                if((isset($_POST['data_to_show']) && $_POST['data_to_show']!="all") || (isset($_GET['data_to_show']) && $_GET['data_to_show']!="all")){
                    //setup paging system
                    if(isset($_POST['data_to_show']))
                        $show_data=$_POST['data_to_show'];
                    else
                        $show_data=$_GET['data_to_show'];
                        
                    $url=get_state_url('gallery&tab=categories')."&data_to_show=$show_data&page=";
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$show_data,$type);
                }else{
                    $where=$db->prepare_query("WHERE $w lgroup=%s",$type);
                    //setup paging system
                    $url=get_state_url('gallery&tab=categories')."&page=";
                }    
                //$num_rows=count_rows("select * from lumonata_articles $where");
                $num_rows=count_rows("select * from lumonata_rules
               $where
                ");
        }
        //echo list_viewed();
        $viewed=list_viewed();
        if(isset($_GET['page'])){
            $page= $_GET['page'];
        }else{
            $page=1;
        }
        
        $limit=($page-1)*$viewed;
        if(is_search()){
        		/*
                $sql=$db->prepare_query("select * 
                from lumonata_articles where $w larticle_type=%s and 
                (larticle_title like %s or larticle_content like %s) limit %d, %d",
                $type,"%".$_POST['s']."%","%".$_POST['s']."%",$limit,$viewed);
                */
        	
                $sql=$db->prepare_query("select * from lumonata_rules
                where
                (lcategory like %s or ldescription like %s)
                order by lorder limit %d, %d","%".$_POST['s']."%","%".$_POST['s']."%",$limit,$viewed);
               
        }else{
                if(isset($_POST['data_to_show']) && $_POST['data_to_show']!="all")
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$_POST['data_to_show'],$type);
                else
                    $where=$db->prepare_query("WHERE $w lgroup=%s",$type);
                    
                //$sql=$db->prepare_query("select * from lumonata_articles $where order by lorder limit %d, %d",$limit,$viewed);
                $sql=$db->prepare_query("select * from lumonata_rules
                $where
                order by lorder limit %d, %d",$limit,$viewed);
               
        }
        
        //if($viewed*$page > $num_rows && $num_rows!=0 && $page>1)
        //    header("location:".$url."1");
        
        $result=$db->do_query($sql);
        
        $start_order=($page - 1) * $viewed + 1; //start order number
        if($_COOKIE['user_type']=="contributor"){
            $button="
            		<li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li>
                    <li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>";
        }else{
            $button="
            		<li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li>
                    <li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>
                    ";
        }
        $thaUrl= 'http://'.SITE_URL.'/gallery-categories-ajax';
        
        
		$gallery_tabs=gallery_tabs();
        $list.="<h1>Gallery Categories</h1>
         <ul class=\"tabs\" style=\"border-bottom:none;\">$gallery_tabs</ul>
                <div class=\"tab_container\">
                    <div class=\"single_content\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('gallery&tab=categories')."\" method=\"post\" name=\"alist\">
                           <div class=\"button_right\">
                                 ".search_box_cam_gallery_categories($thaUrl,'list_item','gallery&tab=categories&prc=search&','right','alert_green_form','Search')."
                                
                           </div>
                           <br clear=\"all\" />
                           
                           <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
                           <input type=\"hidden\" name=\"state\" value=\"pages\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                                $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"list\">
                                <div class=\"list_title\">
                                    <input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\" />
                                    <div class=\"rule_parent\">Parent</div>
                                    <div class=\"rule_name\">Name</div>
                                    <div class=\"rule_description\" >Description</div>
                                </div>
                                <div id=\"list_taxonomy\">";
                                	$list.=gallery_categories_list($result,$start_order);
                $list.="		</div>
                			</div>
                        </form>
                        
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                        <div class=\"paging_right\">
                                    ". paging($url,$num_rows,$page,$viewed,5)."
                        </div>
                    </div>
                </div>
            <script type=\"text/javascript\" language=\"javascript\">
                
                
            </script>";
            
        add_actions('section_title','Gallery');
        return $list;
    }

   
    
    
function get_gallery_list($type='gallery'){
        global $db;
        $list='';
        $option_viewed="";
        $data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
        
        if(isset($_POST['data_to_show']))
            $show_data=$_POST['data_to_show'];
        elseif(isset($_GET['data_to_show']))
            $show_data=$_GET['data_to_show'];
       
        
        foreach($data_to_show as $key=>$val){
            if(isset($show_data)){
                if($show_data==$key){
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
                }else{
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
                }
            }elseif($key=='all'){
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
            }else{
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
            }
        }
        
        
        
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }
        
       
        //echo list_viewed();
        $viewed=list_viewed();
        if(isset($_GET['page'])){
            $page= $_GET['page'];
        }else{
            $page=1;
        }
        
        $limit=($page-1)*$viewed;
        if(is_search()){

        		$sql=$db->prepare_query("select * from lumonata_articles
                where larticle_type=%s AND
                (larticle_title like %s)
                order by lorder ",$type,"%".$_POST['s']."%");
               	$num_rows=count_rows($sql);
               	
                $sql=$db->prepare_query("select * from lumonata_articles
                where larticle_type=%s AND
                (larticle_title like %s)
                order by lorder limit %d, %d",$type,"%".$_POST['s']."%",$limit,$viewed);
        }else{
                if(isset($_POST['data_to_show']) && $_POST['data_to_show']!="all")
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$_POST['data_to_show'],$type);
                else
                    $where=$db->prepare_query("WHERE $w larticle_type=%s",$type);

                    
                $sql=$db->prepare_query("select * from lumonata_articles
                $where
                order by lorder");
                $num_rows=count_rows($sql);
                
                $sql=$db->prepare_query("select * from lumonata_articles
                $where
                order by lorder limit %d, %d",$limit,$viewed);
               
        }

        
        $result=$db->do_query($sql);
        
        $start_order=($page - 1) * $viewed + 1; //start order number
        $link ='http://localhost/sukhavati/lumonata-admin/upload-media.php?tab=from-computer&type=image&textarea_id=0&post_id=184';
        if($_COOKIE['user_type']=="contributor"){
            $button="
            		<!--<li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>-->
            		<li>".button("button=add_new&label=Add New",get_state_url('gallery&tab=quick-add-new')."&prc=add_new")."</li>
                    <li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>";
        }else{
            $button="
            		<!--<li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>-->
            		<li>".button("button=add_new&label=Add New",get_state_url('gallery&tab=quick-add-new')."&prc=add_new")."</li>
                    <li>".button('button=edit&type=submit&enable=false')."</li>
                    <li>".button('button=delete&type=submit&enable=false')."</li>
                    ";
        }
        
        
        $thaUrl= 'http://'.SITE_URL.'/gallery-ajax';
        $gallery_tabs=gallery_tabs();
        $list.= get_gallery_load_more_js();
        
        $list.="<h1>Gallery</h1>
         <ul class=\"tabs\" style=\"border-bottom:none;\">$gallery_tabs</ul>
                <div class=\"tab_container\">
                    <div class=\"single_content\">
                        <div id=\"response\"></div>
                        <form action=\"".get_state_url('gallery')."\" method=\"post\" name=\"alist\">
                           <div class=\"button_right\">
                                 ".search_box_cam_gallery($thaUrl,'list_item','state=gallery&prc=search&','right','alert_green_form','Search')."
                                
                           </div>
                           <br clear=\"all\" />
                           <input type=\"hidden\" name=\"thaURL\" value=\"".get_state_url('gallery')."\" />
						   <input type=\"hidden\" name=\"siteURL\" value=\"http://".SITE_URL."\" />
						   <input type=\"hidden\" name=\"start_order\" value=\"$start_order\" />
						   <input type=\"hidden\" name=\"state\" value=\"gallery\" />
                            <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                        <ul class=\"button_navigation\">
                                                $button
                                        </ul>
                                </div>
                               
                            </div>
                            <div class=\"list\">
                                <div class=\"list_title\">
                                    <input type=\"checkbox\" name=\"select_all\" class=\"title_checkbox\" style=\"margin-left:7px;\"/>
                                    <div class=\"article_title\" style=\"width: 300px;\">Name</div>
                                    <div class=\"list_author\" style=\"width: 180px;\">Category</div>
                                    <div class=\"list_author\" style=\"width: 120px;\">File</div>
                                    <div class=\"list_date\" style=\"width: 120px;\">Date</div>
                                </div>
                                <div id=\"list_item\">";
				$list.=gallery_list($result,$start_order);
                                	
                $list .= "</div>";
       
				if ($num_rows>$viewed){ $list .= get_gallery_load_more_link($page);}     
			    $url  = $thaUrl;     	               	
				$list.="		
						</div>
								</form>
                        
                        <div class=\"button_wrapper clearfix\">
                                <div class=\"button_left\">
                                    <ul class=\"button_navigation\">
                                         $button
                                    </ul>   
                                </div>
                        </div>
                       	<!-- 
						<div class=\"paging_right\">
									".paging($url,$num_rows,$page,$viewed,5)."
						</div>
						-->
                    </div>
                </div>
            <script type=\"text/javascript\" language=\"javascript\">
                
                
            </script>";
			$list .='<script type="text/javascript" src="http://'.site_url().'/lumonata-plugins/gallery/js/list.js" ></script>';
            
        add_actions('section_title','Gallery');
        add_actions('admin_tail',popup_gallery_delete());
        return $list;
    }

function get_gallery_list_ajax($type='gallery'){
    global $db;
        $list='';
        $option_viewed="";
        $data_to_show=array('all'=>'All','publish'=>'Publish','unpublish'=>'Unpublish','draft'=>'Draft');
        
        if(isset($_POST['data_to_show']))
            $show_data=$_POST['data_to_show'];
        elseif(isset($_GET['data_to_show']))
            $show_data=$_GET['data_to_show'];
       
        
        foreach($data_to_show as $key=>$val){
            if(isset($show_data)){
                if($show_data==$key){
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\" />$val";
                }else{
                    $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
                }
            }elseif($key=='all'){
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\" checked=\"checked\"  />$val";
            }else{
                $option_viewed.="<input type=\"radio\" name=\"data_to_show\" value=\"$key\"  />$val";
            }
        }
        
        
        
        if($_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author'){
            $w=" lpost_by=".$_COOKIE['user_id']." AND ";    
        }else{
            $w="";
        }
        
        //paging system
		$viewed=list_viewed();
		if(isset($_GET['page'])){
			$page= $_GET['page'];
		}else{
			$page=1;
		}
		$page = $_POST['page']+1;   
		
		$limit=($page-1)*$viewed;
		$url=get_state_url('header_image')."&page=";
        
		
		if(is_search()){
        	
                $sql=$db->prepare_query("select * from lumonata_articles
                where
                (larticle_title like %s)
                order by lorder limit %d, %d","%".$_POST['s']."%",$limit,$viewed);
               
        }else{
                if(isset($_POST['data_to_show']) && $_POST['data_to_show']!="all")
                    $where=$db->prepare_query(" WHERE $w larticle_status=%s AND larticle_type=%s",$_POST['data_to_show'],$type);
                else
                    $where=$db->prepare_query("WHERE $w larticle_type=%s",$type);
                    
                //$sql=$db->prepare_query("select * from lumonata_articles $where order by lorder limit %d, %d",$limit,$viewed);
                //echo
                $sql=$db->prepare_query("select * from lumonata_articles
                $where
                order by lorder limit %d, %d",$limit,$viewed);
               
        }
        
        
		$result=$db->do_query($sql);
		$numData = $db->num_rows($result);
		$start_order=($page - 1) * $viewed + 1; //start order number       
		
		if($numData>0){
	        $list .= gallery_list($result);
	        $list .= get_gallery_load_more_link($page);
		} 
	
		echo $list;
    }
    
    function gallery_list($result,$i=1){
        global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No Gallery found</div>";
        }
        
        while($d=$db->fetch_array($result)){
        		
        		/*
                if($d['larticle_status']!='publish')
                    $status=" - <strong style=\"color:red;\">".ucfirst($d['larticle_status'])."</strong>";
                else
                    $status="";
                

                if($d['lshare_to']==0){
                    $share_to="<span style=\"font-size:10px;\">Everyone</span>";
                }else{
                    $share_data=get_friend_list_by_id($d['lshare_to']);
                    $share_to="<span style=\"font-size:10px;\">".$share_data['llist_name']."</span>";
                }   
                */
        	
        		$qc = $db->prepare_query("Select * From lumonata_rule_relationship rr 
        		Inner Join lumonata_rules r On rr.lrule_id = r.lrule_id 
        		Where rr.lapp_id=%d",$d['larticle_id']);
        		$rc = $db->do_query($qc);
        		$dc = $db->fetch_array($rc);
        		$sefc = $dc['lsef'];
        		$file = '';
        		$app_name = 'gallery';
        		if ($sefc=='photo'){
        			$image = get_additional_field($d['larticle_id'], 'image', $app_name);
        			$sizeSTR = 'w=50';
					$url = 'http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
					$url_thumb = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-plugins/gallery/files/thumbs/'.$image;
					$exit_image = '
					<a href="'.$url.'" rel="view_image" class="cboxElement">
					<img src="'.$url_thumb.'">
					</a><br />
					';
        			$file = $exit_image;
        		}else if ($sefc=='video'){
        			$embed = get_additional_field($d['larticle_id'], 'embed', $app_name);
        			$url_thumb = 'http://'.SITE_URL.'/lumonata-plugins/gallery/images/ico-video.png';
        			$url_player = get_url_embed($embed,1);
					$url_thumb = get_url_embed($embed,2);
					$exit_embed = '
					<a href="'.$url_player.'" class="iframe_new cboxElement">
					<img src="'.$url_thumb.'" style="width:50px;">
					</a><br />
					';
        			$file = $exit_embed;
        		}else{
        			$image = get_additional_field($d['larticle_id'], 'image', $app_name);
        			$sizeSTR = 'w=50';
        			$url = 'http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
        			$url_thumb = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-plugins/gallery/files/thumbs/'.$image;
        			$exit_image = '
        			<a href="'.$url.'" rel="view_image" class="cboxElement">
        			<img src="'.$url_thumb.'">
        			</a><br />
        			';
        			$file = $exit_image;
        		}
                $categorie= $dc['lname'];
                $name = $d['larticle_title'];
                $date = date(get_date_format(),strtotime($d['lpost_date']));
                $list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['larticle_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['larticle_id']."\" />
                                 	 	 	
                                	
                                	<div class=\"article_title\" style=\"width: 300px;\">$name</div>
                                	<div class=\"list_author\" style=\"width: 180px;\">$categorie</div>
                                    <div class=\"list_author\" style=\"width: 120px;\">$file</div>
                                    <div class=\"list_date\" style=\"width: 120px;\">$date</div>

                                
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['larticle_id']."\">
                                                <a href=\"".get_state_url('gallery')."&prc=edit&id=".$d['larticle_id']."\">Edit</a> |
                                                <!--<a href=\"javascript:;\" rel=\"delete_".$d['larticle_id']."\">Delete</a>--> 
                                                <a href=\"javascript:;\" class=\"delete_link\" id=\"".$d['larticle_id']."\" rel=\"".$name."\">Delete</a>
                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                		$('a[rel=view_image]').colorbox();
                                		$('.iframe_new').colorbox({iframe:true, width:'560px', height:'340px'});
                                        $('#theitem_".$d['larticle_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['larticle_id']."').show();
                                        });
                                        $('#theitem_".$d['larticle_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['larticle_id']."').hide();
                                        });
                                </script>
                                
                        </div>";
                //delete_confirmation_box()
                


                $msg="Are sure want to delete  ".$d['larticle_title']."?";
                $thaUrl= 'http://'.SITE_URL.'/gallery-ajax';
                add_actions('admin_tail','delete_confirmation_box_cam_gallery',$d['larticle_id'],$msg,$thaUrl,"theitem_".$d['larticle_id'],'state=pages&prc=delete&id='.$d['larticle_id']);
                //delete_confirmation_box($d['larticle_id'],"Are sure want to delete ".$d['larticle_title']."?","articles.php","theitem_".$d['larticle_id'],'state=pages&prc=delete&id='.$d['larticle_id'])
                $i++;
        }
        return $list;
    }
    
  
    
        
function gallery_categories_list($result,$i=1){
        global $db;
        $list='';
        
   		if($db->num_rows($result)==0){
        	if(isset($_POST['s']))
        		return "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
        	else 
        		return "<div class=\"alert_yellow_form\">No Gallery Categories found</div>";
        }
        
        while($d=$db->fetch_array($result)){
        		
        		/*
                if($d['larticle_status']!='publish')
                    $status=" - <strong style=\"color:red;\">".ucfirst($d['larticle_status'])."</strong>";
                else
                    $status="";
                

                if($d['lshare_to']==0){
                    $share_to="<span style=\"font-size:10px;\">Everyone</span>";
                }else{
                    $share_data=get_friend_list_by_id($d['lshare_to']);
                    $share_to="<span style=\"font-size:10px;\">".$share_data['llist_name']."</span>";
                }   
                */
        	
                //$user_fetched=fetch_user($d['lpost_by']);
                $parent=get_rule_structure($d['lrule_id'],$d['lrule_id']);    
                $list.="<div class=\"list_item clearfix\" id=\"theitem_".$d['lrule_id']."\">
                                <input type=\"checkbox\" name=\"select[]\" class=\"title_checkbox select\" value=\"".$d['lrule_id']."\" />
                                 	 	 	
                                	
                                
                                <div class=\"rule_parent\" >".$parent."</div>
	                            <div class=\"rule_name\">".$d['lname']."</div>
	                            <div class=\"rule_description\">".$d['ldescription']."</div>

                                
                                <div class=\"the_navigation_list\">
                                        <div class=\"list_navigation\" style=\"display:none;\" id=\"the_navigation_".$d['lrule_id']."\">
                                                <a href=\"".get_state_url('gallery&tab=categories')."&prc=edit&id=".$d['lrule_id']."\">Edit</a> |
                                                <a href=\"javascript:;\" rel=\"delete_".$d['lrule_id']."\">Delete</a> 
                                        </div>
                                </div>
                                <script type=\"text/javascript\" language=\"javascript\">
                                        $('#theitem_".$d['lrule_id']."').mouseover(function(){
                                                $('#the_navigation_".$d['lrule_id']."').show();
                                        });
                                        $('#theitem_".$d['lrule_id']."').mouseout(function(){
                                                $('#the_navigation_".$d['lrule_id']."').hide();
                                        });
                                </script>
                                
                        </div>";
                //delete_confirmation_box()
                $msg="Are sure want to delete ".$d['lname']."?";
                $thaUrl= 'http://'.SITE_URL.'/gallery-categories-ajax';
                add_actions('admin_tail','delete_confirmation_box_cam_gallery_categories',$d['lrule_id'],$msg,$thaUrl,"theitem_".$d['lrule_id'],'state=pages&prc=delete&id='.$d['lrule_id']);
                //delete_confirmation_box($d['larticle_id'],"Are sure want to delete ".$d['larticle_title']."?","articles.php","theitem_".$d['larticle_id'],'state=pages&prc=delete&id='.$d['larticle_id'])
                $i++;
        }
        return $list;
    }

  

  function add_new_gallery_quick($post_id=0){
		//echo $post_id;
		global $thepost;
        global $db;
        $args=array($index=0,$post_id);
        set_gallery_quick_template();
        add_variable('app_title','Add New Gallery');
        
        $thepost->post_id=$post_id;
        $thepost->post_index=$index;
        
        $button="";
        if(!is_contributor()){
            $button.="
            <li>".button("button=insert&label=Save")."</li>
            <!--li>".button("button=publish")."</li-->
            <li>".button("button=cancel",get_state_url('gallery')."&tab=gallery")."</li>
            <!--<li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>-->
            <li>".button("button=add_new&label=Add New",get_state_url('gallery&tab=quick-add-new')."&prc=add_new")."</li>
            ";
                        
        }else{
            $button.="
            <li>".button("button=save_draft&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('gallery')."&tab=gallery")."</li>
            <!--<li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>-->
            <li>".button("button=add_new&label=Add New",get_state_url('gallery&tab=quick-add-new')."&prc=add_new")."</li>
            ";
        }
        
        // Set Tabs
        $gallery_tabs=gallery_tabs();
        add_variable('gallery_tabs',$gallery_tabs);
        
        add_variable('url_plugin',SITE_URL.'/lumonata-plugins/gallery');
        
        //set the page Title
        add_actions('section_title','Gallery - Add New');
        if(is_save_draft() || is_publish()){
        	
            if(!is_saved())
                $post_id=$post_id;
            else
                $post_id=$_POST['post_id'][0];
            
            //Get The Permalink
            //if(is_permalink()){
                if(isset($_POST['sef_box'][0]))
                    $sef=$_POST['sef_box'][0];
                else 
                    $sef="";
                    
                $_POST['index']=0;
                
                if(strlen($sef)>50)$more="...";else $more="";
                
                $sef_scheme="<div id=\"sef_scheme_0\">";
                $sef_scheme.="<strong>Permalink:</strong> 
                        	  http://".site_url()."/
                        	  <span id=\"the_sef_".$_POST['index']."\">
                    			  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
                                      substr($sef,0,50).$more.
                                  "</span>/
                                  <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
                              <span>
                              	<input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" />/
                              	<input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              </span>
                              
                              <script type=\"text/javascript\">
                              		$('#the_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                              			
                    				});
                    				$('#edit_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                    				});
                    				$('#done_edit_sef_".$_POST['index']."').click(function(){
                    					                    					
                    					var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
                    						
                    					if(new_sef.length>50){
                    						var more='...'
                    						
                    					}else{
                        					var more='';
                        					
                        				}
                        				
                              			$('#the_sef_".$_POST['index']."').show();
                              			$('#sef_box_".$_POST['index']."').hide();
                              			$.post('articles.php',
                              			{ 'update_sef' 	: 'true',
             							  'post_id' 	: ".$post_id.",
             							  'type' 		: 'pages',
             							  'title' 		: $('input[name=title[0]]').val(),
             							  'new_sef'	 	: new_sef },
                              			function(theResponse){
                              				if(theResponse=='BAD'){
                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
                    						}else if(theResponse=='OK'){
                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
             								}
             							});
             							
             							
                    				});
                              </script>"; 
                    $sef_scheme.="</div>";
            //}
            add_variable('textarea',textarea('post[0]',0,rem_slashes($_POST['post'][0]),$post_id));
            add_variable('title',rem_slashes($_POST['title'][0]));
            add_variable('is_saved',"<input type=\"hidden\" name=\"article_saved\" value=\"1\" />");
        }else{
        	
        	if (isset($_POST['insert'])){
        		header("location:".get_state_url('gallery').'&tab=gallery');
        	}
            
        }
        
        
        //B For Form
        add_variable('post_index',0);
        add_variable('i',0);
		add_variable('textarea',textarea('theDescription[0]',0));
		add_variable('display_image','display:none;');
		add_variable('display_video','display:none;');
		$acco_type_id = 0;
		$index = 0;
		add_variable('thaUrl','http://'.SITE_URL.'/gallery-quick-ajax');
		add_variable('thisUrl','http://'.SITE_URL.'/lumonata-admin/?state=gallery&tab=quick-add-new&prc=add_new');
		//add_variable('additional_service_list',additional_service_list($acco_type_id,$index,$article_id=0));
		//add_variable('option_cat_id',option_cat_id(0,''));
		//add_variable('option_acco_id',option_acco_id(0,''));
        //E For Form
        $rule = 'categories';
        $group = 'gallery';
        $subsite = 'arunna';
		$rule_id = '';
        if(isset($_GET['category'])){
        	$rule_id=$_GET['category'];
        }
        $thecategory_parent_option = recursive_taxonomy(0,$rule,$group,'select',array($rule_id));
        add_variable('thecategory_parent_option',$thecategory_parent_option);
        
        add_variable('button',$button);
        parse_template('loopPage','lPage',false);
         
        return return_page_template();
        
    }
 function add_new_gallery($post_id=0){
		//echo $post_id;
		global $thepost;
        global $db;
        $args=array($index=0,$post_id);
        set_gallery_template();
        add_variable('app_title','Add New Gallery');
        
        $thepost->post_id=$post_id;
        $thepost->post_index=$index;
        
        $button="";
        if(!is_contributor()){
            $button.="
            <li>".button("button=insert&label=Save")."</li>
            <!--li>".button("button=publish")."</li-->
            <li>".button("button=cancel",get_state_url('gallery'))."</li>
            <li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>
            ";
                        
        }else{
            $button.="
            <li>".button("button=save_draft&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('gallery'))."</li>
            <li>".button("button=add_new",get_state_url('gallery')."&prc=add_new")."</li>";
        }
        
        // Set Tabs
        $gallery_tabs=gallery_tabs();
        add_variable('gallery_tabs',$gallery_tabs);
        
        //set the page Title
        add_actions('section_title','Gallery - Add New');
        if(is_save_draft() || is_publish()){
        	
            if(!is_saved())
                $post_id=$post_id;
            else
                $post_id=$_POST['post_id'][0];
            
            //Get The Permalink
            //if(is_permalink()){
                if(isset($_POST['sef_box'][0]))
                    $sef=$_POST['sef_box'][0];
                else 
                    $sef="";
                    
                $_POST['index']=0;
                
                if(strlen($sef)>50)$more="...";else $more="";
                
                $sef_scheme="<div id=\"sef_scheme_0\">";
                $sef_scheme.="<strong>Permalink:</strong> 
                        	  http://".site_url()."/
                        	  <span id=\"the_sef_".$_POST['index']."\">
                    			  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
                                      substr($sef,0,50).$more.
                                  "</span>/
                                  <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
                              <span>
                              	<input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" />/
                              	<input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              </span>
                              
                              <script type=\"text/javascript\">
                              		$('#the_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                              			
                    				});
                    				$('#edit_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                    				});
                    				$('#done_edit_sef_".$_POST['index']."').click(function(){
                    					                    					
                    					var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
                    						
                    					if(new_sef.length>50){
                    						var more='...'
                    						
                    					}else{
                        					var more='';
                        					
                        				}
                        				
                              			$('#the_sef_".$_POST['index']."').show();
                              			$('#sef_box_".$_POST['index']."').hide();
                              			$.post('articles.php',
                              			{ 'update_sef' 	: 'true',
             							  'post_id' 	: ".$post_id.",
             							  'type' 		: 'pages',
             							  'title' 		: $('input[name=title[0]]').val(),
             							  'new_sef'	 	: new_sef },
                              			function(theResponse){
                              				if(theResponse=='BAD'){
                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
                    						}else if(theResponse=='OK'){
                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
             								}
             							});
             							
             							
                    				});
                              </script>"; 
                    $sef_scheme.="</div>";
            //}
            add_variable('textarea',textarea('post[0]',0,rem_slashes($_POST['post'][0]),$post_id));
            add_variable('title',rem_slashes($_POST['title'][0]));
            add_variable('is_saved',"<input type=\"hidden\" name=\"article_saved\" value=\"1\" />");
        }else{
        	
        	if (isset($_POST['insert'])){
        		//echo "add_new_gallery";
        		//print_r($_POST);
        		$i = 0;
				$user_id = $_COOKIE['user_id'];
        		$user_fetched=fetch_user($_COOKIE['user_id']);
        		$display_name = $user_fetched['ldisplay_name'];
        		
        		$article_id = setCode("lumonata_articles","larticle_id");
        		$app_name = 'gallery';
        		$thecategory_parent ="";
        		$thetitle ="";
        		$theimage ="";
        		$theembed ="";
        		$theDescription ="";

        		if(isset($_POST['thecategory_parent'][$i])){$thecategory_parent=$_POST['thecategory_parent'][$i];}else{$thecategory_parent="";}
	        	if(isset($_POST['thetitle'][$i])){ $thetitle =$_POST['thetitle'][$i];}else{$thetitle="";}
	        	if(isset($_POST['theembed'][$i])){ $theembed =$_POST['theembed'][$i];}else{$theembed="";}
	        	if(isset($_POST['theDescription'][$i])){ $theDescription =$_POST['theDescription'][$i];}else{$theDescription="";}
	        	
        		 
				

		        $file_name = $_FILES['theimage']['name'][$i];
		        $file_size = $_FILES['theimage']['size'][$i];
		        $file_type = $_FILES['theimage']['type'][$i];
		        $file_source = $_FILES['theimage']['tmp_name'][$i];
		        
		        if (!empty($file_name)){
		         if(is_allow_file_size($file_size)){
					if(is_allow_file_type($file_type,'image')){
			            	
			                 $image_sef=file_name_filter($thetitle."-".time());
			                 $file_ext=file_name_filter($file_name,true);
			                 
			                 $file_name=$image_sef. $file_ext;
			
			                 $destination1=PLUGINS_PATH."/gallery/files/".$file_name;
			                 $destination2=PLUGINS_PATH."/gallery/files/thumbs/".$file_name;
			                 $destination3=PLUGINS_PATH."/gallery/files/thumbs2/".$file_name;
			                 
			                 upload_resize($file_source,$destination3,$file_type,thumbnail_image_width(),thumbnail_image_height());
			                 upload_resize($file_source,$destination2,$file_type,medium_image_width(),medium_image_height());
			                 upload_resize($file_source,$destination1,$file_type,large_image_width(),large_image_height());
			                 $theimage = $file_name;
			                 
			            }
					}
		        }

        		$sql=$db->prepare_query("INSERT INTO lumonata_articles
				(
				larticle_id,larticle_title,larticle_status,larticle_type,lsef,
			 	lorder,lpost_by,lpost_date,larticle_content
				)
				VALUES 
				(
				%d,%s,%s,%s,%s,
				%d,%d,%s,%s
				)",
				$article_id,$thetitle,'publish',$app_name,'',
				1,$_COOKIE['user_id'],date("Y-m-d H:i:s"),$theDescription
				);
				
				
				if(reset_order_id("lumonata_articles")){
					$result = $db->do_query($sql);
				}
				
				if ($result){
					add_additional_field($article_id, 'image', $theimage, $app_name);
					
					$q=$db->prepare_query("Insert Into lumonata_additional_fields (lapp_id,lkey,lvalue,lapp_name) Values (%d,%s,'$theembed',%s)",$article_id,'embed',$app_name);
					$r=$db->do_query($q);
					
					$q=$db->prepare_query("Insert Into lumonata_rule_relationship (lapp_id,lrule_id) Values (%d,%d)",$article_id,$thecategory_parent);
					$r=$db->do_query($q);
					
				}	
			
        	}
            
        }
        
        
        //B For Form
        add_variable('post_index',0);
        add_variable('i',0);
		add_variable('textarea',textarea('theDescription[0]',0));
		add_variable('display_image','display:none;');
		add_variable('display_video','display:none;');
		$acco_type_id = 0;
		$index = 0;
		add_variable('thaUrl','http://'.SITE_URL.'/gallery-ajax');
		//add_variable('additional_service_list',additional_service_list($acco_type_id,$index,$article_id=0));
		//add_variable('option_cat_id',option_cat_id(0,''));
		//add_variable('option_acco_id',option_acco_id(0,''));
        //E For Form
        $rule = 'categories';
        $group = 'gallery';
        $subsite = 'arunna';
        $thecategory_parent_option = recursive_taxonomy(0,$rule,$group);
        add_variable('thecategory_parent_option',$thecategory_parent_option);
        
        add_variable('button',$button);
        parse_template('loopPage','lPage',false);
         
        return return_page_template();
        
    }

    

function add_new_gallery_categories($post_id=0){
		//echo $post_id;
		global $thepost;
        global $db;
        $args=array($index=0,$post_id);
        set_gallery_categories_template();
        add_variable('app_title','Add New Gallery Categories');
        
        $thepost->post_id=$post_id;
        $thepost->post_index=$index;
        
        $button="";
        if(!is_contributor()){
            $button.="
            <li>".button("button=insert&label=Save")."</li>
            <!--li>".button("button=publish")."</li-->
            <li>".button("button=cancel",get_state_url('gallery&tab=categories'))."</li>
            <li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li>
            ";
                        
        }else{
            $button.="
            <li>".button("button=save_draft&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('gallery&tab=categories'))."</li>
            <li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li>";
        }
        
        
        // Set Tabs
        $gallery_tabs=gallery_tabs();
        add_variable('gallery_tabs',$gallery_tabs);
        
        //set the page Title
        add_actions('section_title','Gallery Categories - Add New');
        if(is_save_draft() || is_publish()){
        	
            if(!is_saved())
                $post_id=$post_id;
            else
                $post_id=$_POST['post_id'][0];
            
            //Get The Permalink
            //if(is_permalink()){
                if(isset($_POST['sef_box'][0]))
                    $sef=$_POST['sef_box'][0];
                else 
                    $sef="";
                    
                $_POST['index']=0;
                
                if(strlen($sef)>50)$more="...";else $more="";
                
                $sef_scheme="<div id=\"sef_scheme_0\">";
                $sef_scheme.="<strong>Permalink:</strong> 
                        	  http://".site_url()."/
                        	  <span id=\"the_sef_".$_POST['index']."\">
                    			  <span id=\"the_sef_content_".$_POST['index']."\"  style=\"background:#FFCC66;cursor:pointer;\">".
                                      substr($sef,0,50).$more.
                                  "</span>/
                                  <input type=\"button\" value=\"Edit\" id=\"edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              <span id=\"sef_box_".$_POST['index']."\" style=\"display:none;\">
                              <span>
                              	<input type=\"text\" name=\"sef_box[".$_POST['index']."]\" value=\"".$sef."\" style=\"border:1px solid #CCC;width:300px;font-size:11px;\" />/
                              	<input type=\"button\" value=\"Done\" id=\"done_edit_sef_".$_POST['index']."\" class=\"button_bold\">
                              </span>
                              </span>
                              
                              <script type=\"text/javascript\">
                              		$('#the_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                              			
                    				});
                    				$('#edit_sef_".$_POST['index']."').click(function(){
                              			$('#the_sef_".$_POST['index']."').hide();
                              			$('#sef_box_".$_POST['index']."').show();
                    				});
                    				$('#done_edit_sef_".$_POST['index']."').click(function(){
                    					                    					
                    					var new_sef=$('input[name=sef_box[".$_POST['index']."]]').val();
                    						
                    					if(new_sef.length>50){
                    						var more='...'
                    						
                    					}else{
                        					var more='';
                        					
                        				}
                        				
                              			$('#the_sef_".$_POST['index']."').show();
                              			$('#sef_box_".$_POST['index']."').hide();
                              			$.post('articles.php',
                              			{ 'update_sef' 	: 'true',
             							  'post_id' 	: ".$post_id.",
             							  'type' 		: 'pages',
             							  'title' 		: $('input[name=title[0]]').val(),
             							  'new_sef'	 	: new_sef },
                              			function(theResponse){
                              				if(theResponse=='BAD'){
                    							$('input[name=sef_box[".$_POST['index']."]]').val('".$sef."');
                    							$('#the_sef_content_".$_POST['index']."').html('".substr($sef, 0,50)."');
                    						}else if(theResponse=='OK'){
                    							$('#the_sef_content_".$_POST['index']."').html(new_sef.substr(0,50)+more);
             								}
             							});
             							
             							
                    				});
                              </script>"; 
                    $sef_scheme.="</div>";
            //}
            add_variable('textarea',textarea('post[0]',0,rem_slashes($_POST['post'][0]),$post_id));
            add_variable('title',rem_slashes($_POST['title'][0]));
            add_variable('is_saved',"<input type=\"hidden\" name=\"article_saved\" value=\"1\" />");
        }else{
        	
        	if (isset($_POST['insert'])){
        		//echo "add_new_gallery";
        		
        		///*
        		$user_id = $_COOKIE['user_id'];
        		$user_fetched=fetch_user($_COOKIE['user_id']);
        		$display_name = $user_fetched['ldisplay_name'];
        		$thecategory ="";
        		$thecategory_parent ="";
        		$theDescription ="";
        		
        		
	        	if(isset($_POST['thecategory'][0])){ $thecategory =$_POST['thecategory'][0];}else{$thecategory="";}
	        	if(isset($_POST['thecategory_parent'][0])){$thecategory_parent=$_POST['thecategory_parent'][0];}else{$thecategory_parent="";}
	        	if(isset($_POST['theDescription'][0])){$theDescription=$_POST['theDescription'][0];}else{$theDescription="";}

	        	$rule = 'categories';
	        	$group = 'gallery';
	        	$subsite = 'arunna';
				insert_rules($thecategory_parent,$thecategory,$theDescription,$rule,$group,false,$subsite);
        		 
        		
			
        	}
        	
        }
        	
            
        
        
        add_variable('post_index',0);
        add_variable('i',0);
        add_variable('textarea',textarea('theDescription[0]',0));
        $rule = 'categories';
        $group = 'gallery';
        $subsite = 'arunna';
        $thecategory_parent_option = recursive_taxonomy(0,$rule,$group);
        add_variable('thecategory_parent_option',$thecategory_parent_option);
        
        add_variable('button',$button);
        parse_template('loopPage','lPage',false);
         
        return return_page_template();
        
    }

   
function edit_gallery($post_id=0){
		//echo "edit_gallery";
        global $thepost;
        global $db;
        $index=0;
        $button="";
        set_gallery_template();
        
        
 		if(isset($_POST['select']) && count($_POST['select'])>1){
           add_variable('app_title','Edit Gallery');
        }else{
           add_variable('app_title','Edit Gallery');
        }
        
        //save_changes_botton()
        /*
        if(!is_contributor())
            $button.="
            <li>".button("button=publish&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('villa&sub=gallery'))."</li>";
                        
        else
            $button.="
            <li>".button("button=publish&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('villa&sub=gallery'))."</li>";
        */
        
        if(!is_contributor())
            $button.="
            <li>".save_changes_botton()."</li>
            <li>".button("button=cancel",get_state_url('villa&sub=gallery'))."</li>";
                        
        else
            $button.="
            <li>".save_changes_botton()."</li>
            <li>".button("button=cancel",get_state_url('villa&sub=gallery'))."</li>";
            

        // Set Tabs
        $gallery_tabs=gallery_tabs();
        add_variable('gallery_tabs',$gallery_tabs);
        
        //set the page Title
        add_actions('section_title','Gallery - Edit');
        
        if(is_edit_all()){
            foreach($_POST['select'] as $index=>$post_id){
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;
                $i = $index;
                $id = $post_id; 
                
                /* B Show Record */
            	if ($index%2==0){
                	add_variable('am_style_background','style="background: #fafafa"');
                }else{
                	add_variable('am_style_background','style="background: #FFF"');
                }
                add_variable('post_id',$thepost->post_id);
                add_variable('post_index',$thepost->post_index);
                add_variable('thaUrl','http://'.SITE_URL.'/gallery-ajax');
                
                
                
                
                $sqlr = $db->prepare_query("SELECT *
							FROM lumonata_articles
							WHERE 
							larticle_id=%d",$thepost->post_id);
		
        		$rr=$db->do_query($sqlr);
        		$d=$db->fetch_array($rr);
                $article_id=$d['larticle_id']; 
                

        		
				
				add_variable('textarea',textarea('theDescription['.$i.']',$i,rem_slashes($d['larticle_content']),$id));
                
        		$qc = $db->prepare_query("Select * From lumonata_rule_relationship rr 
        		Inner Join lumonata_rules r On rr.lrule_id = r.lrule_id 
        		Where rr.lapp_id=%d",$d['larticle_id']);
        		$rc = $db->do_query($qc);
        		$dc = $db->fetch_array($rc);
        		$rule_id = $dc['lrule_id'];
        		$sefc = $dc['lsef'];
        		$file = '';
        		$app_name = 'gallery';
        		$image = get_additional_field($d['larticle_id'], 'image', $app_name);
        		$embed = get_additional_field($d['larticle_id'], 'embed', $app_name);
                $name = $d['larticle_title'];
        		
        		$sizeSTR = 'w=100';
				$url = 'http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$url_thumb = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-plugins/gallery/files/thumbs/'.$image;
				$exit_image = '
				<a href="'.$url.'" rel="view_image" class="cboxElement">
				<img src="'.$url_thumb.'">
				</a><br />
				';
				
				$url_thumb = 'http://'.SITE_URL.'/lumonata-plugins/gallery/images/ico-video.png';
				//$url_player = get_url_embed($embed,1);
				//$url_thumb = get_url_embed($embed,2);
				$url_player = (!empty($embed)? get_url_embed($embed,1):'');
				$url_thumb =  (!empty($embed)? get_url_embed($embed,2):'');
				$exit_embed = '
				<a href="'.$url_player.'" class="iframe_new  cboxElement">
				<img src="'.$url_thumb.'" style="width:100px;">
				</a><br />
				';
				

        		add_variable('val_thetitle',$name);
            	if (!empty($image)){
        			add_variable('exit_image',$exit_image);	
        		}else{
        			add_variable('exit_image','');	
        		}
				if (!empty($embed)){
					add_variable('exit_embed',$exit_embed);	
					add_variable('val_theembed',$embed);
				}else{
					add_variable('exit_embed','');	
					add_variable('val_theembed','');
				}
				
				if ($rule_id==14){
					add_variable('display_video','display:block;');
					add_variable('display_image','display:none;');
				}else if ($rule_id==16){
					add_variable('display_video','display:none;');
					add_variable('display_image','display:block;');
				}else{
					add_variable('display_video','display:none;');
					add_variable('display_image','display:none;');
				}
				
				
				$rule = 'categories';
	        	$group = 'gallery';
	        	$subsite = 'arunna';
        		$thecategory_parent_option = recursive_taxonomy($i,$rule,$group,'select',array($rule_id));
        		add_variable('thecategory_parent_option',$thecategory_parent_option);
        		
                
				/* E Show Record */
  				add_variable('is_edit_all',"<input type=\"hidden\" name=\"edit\" value=\"Edit\">");
                parse_template('loopPage','lPage',true);
            }
           
        }else{
        	//echo "is_edit";
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;
                $i = $index;
                $id = $post_id; 

                
                /* B Show Record */
                add_variable('post_id',$thepost->post_id);
                add_variable('post_index',$thepost->post_index);
                add_variable('thaUrl','http://'.SITE_URL.'/gallery-ajax');
                
                $sqlr = $db->prepare_query("SELECT *
							FROM lumonata_articles
							WHERE 
							larticle_id=%d",$thepost->post_id);
		
        		$rr=$db->do_query($sqlr);
        		$d=$db->fetch_array($rr);
                $article_id=$d['larticle_id']; 
                

        		
				
				add_variable('textarea',textarea('theDescription['.$i.']',$i,rem_slashes($d['larticle_content']),$id));
                
        		$qc = $db->prepare_query("Select * From lumonata_rule_relationship rr 
        		Inner Join lumonata_rules r On rr.lrule_id = r.lrule_id 
        		Where rr.lapp_id=%d",$d['larticle_id']);
        		$rc = $db->do_query($qc);
        		$dc = $db->fetch_array($rc);
        		$rule_id = $dc['lrule_id'];
        		$sefc = $dc['lsef'];
        		$file = '';
        		$app_name = 'gallery';
        		$image = get_additional_field($d['larticle_id'], 'image', $app_name);
        		$embed = get_additional_field($d['larticle_id'], 'embed', $app_name);
				//print_r($embed).'#';
                $name = $d['larticle_title'];
        		
        		$sizeSTR = 'w=100';
				$url = 'http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
				$url_thumb = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-plugins/gallery/files/thumbs/'.$image;
				$exit_image = '
				<a href="'.$url.'" rel="view_image" class="cboxElement">
				<img src="'.$url_thumb.'">
				</a><br />
				';
				
				$url_thumb = 'http://'.SITE_URL.'/lumonata-plugins/gallery/images/ico-video.png';
				$url_player = (!empty($embed)? get_url_embed($embed,1):'');
				$url_thumb =  (!empty($embed)? get_url_embed($embed,2):'');
				$exit_embed = '
				<a href="'.$url_player.'" class="iframe_new  cboxElement">
				<img src="'.$url_thumb.'" style="width:100px;">
				</a><br />
				';
				

        		add_variable('val_thetitle',$name);
        		if (!empty($image)){
        			add_variable('exit_image',$exit_image);	
        		}else{
        			add_variable('exit_image','');	
        		}
				if (!empty($embed)){
					add_variable('exit_embed',$exit_embed);	
					add_variable('val_theembed',$embed);
				}else{
					add_variable('exit_embed','');	
					add_variable('val_theembed','');
				}
				
				if ($rule_id==14){
					add_variable('display_video','display:block;');
					add_variable('display_image','display:none;');
				}else if ($rule_id==16){
					add_variable('display_video','display:none;');
					add_variable('display_image','display:block;');
				}else{
					add_variable('display_video','display:none;');
					add_variable('display_image','display:none;');
				}
				$rule = 'categories';
	        	$group = 'gallery';
	        	$subsite = 'arunna';
        		$thecategory_parent_option = recursive_taxonomy($i,$rule,$group,'select',array($rule_id));
        		add_variable('thecategory_parent_option',$thecategory_parent_option);
        		
                
				/* E Show Record */

                parse_template('loopPage','lPage',false);
           
        }
        
       
        add_variable('button',$button);
        return return_page_template();
    }
        
function edit_gallery_categories($post_id=0){
		//echo "edit_gallery";
        global $thepost;
        global $db;
        $index=0;
        $button="";
        set_gallery_categories_template();
        
        
 		if(isset($_POST['select']) && count($_POST['select'])>1){
           add_variable('app_title','Edit Gallery Categories');
        }else{
           add_variable('app_title','Edit Gallery Category');
        }
        
        //save_changes_botton()
        /*
        if(!is_contributor())
            $button.="
            <li>".button("button=publish&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('gallery'))."</li>";
                        
        else
            $button.="
            <li>".button("button=publish&label=Save")."</li>
            <li>".button("button=cancel",get_state_url('gallery'))."</li>";
        */
        
        if(!is_contributor())
            $button.="
            <!--li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li-->
            <li>".save_changes_botton()."</li>
            <li>".button("button=cancel",get_state_url('gallery&tab=categories'))."</li>";
                        
        else
            $button.="
            <!--li>".button("button=add_new",get_state_url('gallery&tab=categories')."&prc=add_new")."</li-->
            <li>".save_changes_botton()."</li>
            <li>".button("button=cancel",get_state_url('gallery&tab=categories'))."</li>";
            
		// Set Tabs
        $gallery_tabs=gallery_tabs();
        add_variable('gallery_tabs',$gallery_tabs);
        
        //set the page Title
        add_actions('section_title','Gallery - Edit');
        
        if(is_edit_all()){
            foreach($_POST['select'] as $index=>$post_id){
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;
                $i = $index;
                $cat_id = $post_id;
                
                if(is_save_draft() || is_publish()){
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($_POST['post'][$index]),$post_id));
                    add_variable('title',rem_slashes($_POST['title'][$index]));
                }else{
                    $data_articles=fetch_artciles("id=".$post_id);
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($data_articles['larticle_content']),$post_id));
                    add_variable('title',rem_slashes($data_articles['larticle_title']));
                }
                
                
                /* B Show Record */
                
                if ($index%2==0){
                	add_variable('am_style_background','style="background: #fafafa"');
                }else{
                	add_variable('am_style_background','style="background: #FFF"');
                }
                
                add_variable('thaUrl','http://'.SITE_URL.'/gallery-categories-ajax');
                
                add_variable('surecharge_desc_ID',$thepost->post_id);
                add_variable('post_id',$thepost->post_id);
                add_variable('post_index',$thepost->post_index);
                
                
            	$sqlr = $db->prepare_query("SELECT *
							FROM lumonata_rules
							WHERE 
							lrule_id=%d",$thepost->post_id);
		
        		$rr=$db->do_query($sqlr);
        		$datar=$db->fetch_array($rr);
                
                
        	    add_variable('val_thecategory',$datar['lname']);
        		add_variable('textarea',textarea('theDescription['.$i.']',$i,rem_slashes($datar['ldescription']),$cat_id));
        		$rule = 'categories';
	        	$group = 'gallery';
	        	$subsite = 'arunna';
        		$thecategory_parent_option = recursive_taxonomy($i,$rule,$group,'select',array($datar['lparent']));
        		add_variable('thecategory_parent_option',$thecategory_parent_option);
                
				/* E Show Record */

                add_variable('i',$index);
                add_variable('is_edit_all',"<input type=\"hidden\" name=\"edit\" value=\"Edit\">");
                
                add_variable('additional_data',attemp_actions('page_additional_data_'.$index));
                parse_template('loopPage','lPage',true);
            }
           
        }else{
        	//echo "is_edit";
                $thepost->post_id=$post_id;
                $thepost->post_index=$index;
                $i = $index;
                $cat_id = $post_id;
                
                /*
                if(is_save_draft() || is_publish()){
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($_POST['post'][$index]),$post_id));
                    add_variable('title',rem_slashes($_POST['title'][$index]));
                }else{
                    $data_articles=fetch_artciles("id=".$post_id);
                    add_variable('textarea',textarea('post['.$index.']',$index,rem_slashes($data_articles['larticle_content']),$post_id));
                    add_variable('title',rem_slashes($data_articles['larticle_title']));
                    
                }
                */
                
                
                
                
                /* B Show Record */
                add_variable('surecharge_desc_ID',$thepost->post_id);
                add_variable('post_id',$thepost->post_id);
                add_variable('post_index',$thepost->post_index);
                add_variable('thaUrl','http://'.SITE_URL.'/gallery-categories-ajax');
                
                $sqlr = $db->prepare_query("SELECT *
							FROM lumonata_rules
							WHERE 
							lrule_id=%d",$thepost->post_id);
		
        		$rr=$db->do_query($sqlr);
        		$datar=$db->fetch_array($rr);
                
  
        		add_variable('val_thecategory',$datar['lname']);
        		add_variable('textarea',textarea('theDescription['.$i.']',$i,rem_slashes($datar['ldescription']),$cat_id));
        		$rule = 'categories';
	        	$group = 'gallery';
	        	$subsite = 'arunna';
        		$thecategory_parent_option = recursive_taxonomy($i,$rule,$group,'select',array($datar['lparent']));
        		add_variable('thecategory_parent_option',$thecategory_parent_option);
        		//add_variable('textareaTop',textarea('theDescriptionTop['.$i.']','theDescriptionTop-'.$i,rem_slashes($datar['ldescription_top']),$cat_id));
                //add_variable('textarea',textarea('theDescription[0]',0,$datar['ldescription']));
				/* E Show Record */
				add_variable('post_index',$i);
                parse_template('loopPage','lPage',false);
           
        }
        
       
        add_variable('button',$button);
        return return_page_template();
    }

function update_gallery($post_index){
	global $db;
	/* B Update */
		if(is_save_changes()){
		$i=$post_index;
			
		$user_id = $_COOKIE['user_id'];
        $user_fetched=fetch_user($_COOKIE['user_id']);
        $display_name = $user_fetched['ldisplay_name'];
        
        $article_id = $_POST['post_id'][$i];
        $app_name = 'gallery';
        $thecategory_parent ="";
        $thetitle ="";
        $theimage ="";
        $theembed ="";
        $theDescription ="";

        if(isset($_POST['thecategory_parent'][$i])){$thecategory_parent=$_POST['thecategory_parent'][$i];}else{$thecategory_parent="";}
        if(isset($_POST['thetitle'][$i])){ $thetitle =$_POST['thetitle'][$i];}else{$thetitle="";}
        if(isset($_POST['theembed'][$i])){ $theembed =$_POST['theembed'][$i];}else{$theembed="";}
        if(isset($_POST['theDescription'][$i])){ $theDescription =$_POST['theDescription'][$i];}else{$theDescription="";}
        
		$file_name = $_FILES['theimage']['name'][$i];
        $file_size = $_FILES['theimage']['size'][$i];
        $file_type = $_FILES['theimage']['type'][$i];
        $file_source = $_FILES['theimage']['tmp_name'][$i];
        
        if (!empty($file_name)){
         if(is_allow_file_size($file_size)){
			if(is_allow_file_type($file_type,'image')){
	            	
	                 $image_sef=file_name_filter($thetitle."-".time());
	                 $file_ext=file_name_filter($file_name,true);
	                 
	                 $file_name=$image_sef. $file_ext;
	
	                 $destination1=PLUGINS_PATH."/gallery/files/".$file_name;
	                 $destination2=PLUGINS_PATH."/gallery/files/thumbs/".$file_name;
	                 $destination3=PLUGINS_PATH."/gallery/files/thumbs2/".$file_name;
	                 
	                 upload_resize($file_source,$destination3,$file_type,thumbnail_image_width(),thumbnail_image_height());
	                 upload_resize($file_source,$destination2,$file_type,medium_image_width(),medium_image_height());
	                 upload_resize($file_source,$destination1,$file_type,large_image_width(),large_image_height());
	                 $theimage = $file_name;
	                 
	            }
			}
        }

        $sql=$db->prepare_query("Update lumonata_articles Set
        larticle_title=%s,
        larticle_content=%s
		Where larticle_id=%d",
        $thetitle,
        $theDescription,
		$article_id);
		$result=$db->do_query($sql);
		
		if ($result){
			if (!empty($theimage)){
				edit_additional_field($article_id, 'image', $theimage, $app_name);	
			}
			if (!empty($theembed)){
				//echo "M".$theembed;
				//edit_additional_field($article_id, 'embed', $theembed, $app_name);
				//echo
				$qe=$db->prepare_query("Update lumonata_additional_fields Set lvalue='$theembed' Where lapp_id=%d And lkey=%s",$article_id,'embed');
				$re=$db->do_query($qe);
			}
			
			if ($thecategory_parent==14){
				edit_additional_field($article_id, 'image', '', $app_name);	
			}else if ($thecategory_parent==16){
				edit_additional_field($article_id, 'embed', '', $app_name);	
			}

			$qd=$db->prepare_query("Delete From lumonata_rule_relationship Where lapp_id=%d",$article_id);;
			$rd=$db->do_query($qd);
			
			$q=$db->prepare_query("Insert Into lumonata_rule_relationship (lapp_id,lrule_id) Values (%d,%d)",$article_id,$thecategory_parent);
			$r=$db->do_query($q);
			
		}	
		
	}
}

function update_gallery_categories($post_index){
	global $db;
	/* B Update */
	if(is_save_changes()){
		$i = $post_index;
		$user_id = $_COOKIE['user_id'];
        $user_fetched=fetch_user($_COOKIE['user_id']);
        $display_name = $user_fetched['ldisplay_name'];
        $rule_id = $_POST['post_id'][$i];
        $thecategory ="";
        $thecategory_parent ="";
        $theDescription ="";
        
        
        if(isset($_POST['thecategory'][$i])){ $thecategory =$_POST['thecategory'][$i];}else{$thecategory="";}
        if(isset($_POST['thecategory_parent'][$i])){$thecategory_parent=$_POST['thecategory_parent'][$i];}else{$thecategory_parent="";}
        if(isset($_POST['theDescription'][$i])){$theDescription=$_POST['theDescription'][$i];}else{$theDescription="";}

        $rule = 'categories';
        $group = 'gallery';
        $subsite = 'arunna';
		update_rules($rule_id,$thecategory_parent,$thecategory,$theDescription,$rule,$group,$subsite);
	}
/* E Update */
}
    function set_gallery_quick_template(){
		//add_actions('header_elements','get_css_inc','ui-darkness/jquery-ui.css');
		//add_variable('header_elements',attemp_actions('header_elements'));
		
        //set template
        set_template(PLUGINS_PATH."/gallery/template_admin_gallery_quick.html",'pages');
	    //add_variable('jquery_ui_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/js/jquery-ui-1.8.14.custom.min.js\" ></script>");
		//add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/css/ui-darkness/jquery-ui-1.8.14.custom.css\" type=\"text/css\" media=\"screen\" />");
        //set block
        add_block('loopPage','lPage','pages');
        add_block('pageAddNew','pAddNew','pages');
    }
    
  	function set_gallery_template(){
		//add_actions('header_elements','get_css_inc','ui-darkness/jquery-ui.css');
		//add_variable('header_elements',attemp_actions('header_elements'));
		
        //set template
        set_template(PLUGINS_PATH."/gallery/template_admin_gallery.html",'pages');
	    //add_variable('jquery_ui_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/js/jquery-ui-1.8.14.custom.min.js\" ></script>");
		//add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/css/ui-darkness/jquery-ui-1.8.14.custom.css\" type=\"text/css\" media=\"screen\" />");
        //set block
        add_block('loopPage','lPage','pages');
        add_block('pageAddNew','pAddNew','pages');
    } 
    
    
    function set_gallery_categories_template(){
		//add_actions('header_elements','get_css_inc','ui-darkness/jquery-ui.css');
		//add_variable('header_elements',attemp_actions('header_elements'));
		
        //set template
        set_template(PLUGINS_PATH."/gallery/template_admin_gallery_categories.html",'pages');
	    //add_variable('jquery_ui_js',"<script type=\"text/javascript\" src=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/js/jquery-ui-1.8.14.custom.min.js\" ></script>");
		//add_variable('jquery_ui_css',"<link rel=\"stylesheet\" href=\"http://".SITE_URL."/lumonata-plugins/spa_reservation/js/jquery-ui/css/ui-darkness/jquery-ui-1.8.14.custom.css\" type=\"text/css\" media=\"screen\" />");
        //set block
        add_block('loopPage','lPage','pages');
        add_block('pageAddNew','pAddNew','pages');
}
    
    
    
function gallery_tabs(){
	$tabs=array('gallery'=>'Gallery',
        'categories'=>'Categories');
        $tab_keys=array_keys($tabs);
        $tabb='';
        if(empty($_GET['tab']))
               $the_tab=$tab_keys[0];
        else
                $the_tab=$_GET['tab'];
        $gallery_tabs=set_tabs($tabs,$the_tab);
	return $gallery_tabs;
}

function gallery_CSS(){
	$text = '<style>
			.blockSurechargeDescription ul {padding-left: 0px;}
			.blockSurechargeDescription ul li {list-style: none outside none; margin-right: 10px;}
			
	
			.blockPersonalDetail .textbox { width:98% !important;}
			.blockSpaPackage .textbox { width:97% !important; min-width:0;}
			.blockSpaPackage select { margin:3px 0; width:100%; padding:6px; height: 32px !important;}
			.blockBookingDetail .textbox { width:97% !important; min-width:0;}
			.blockPersonalDetail .select { margin:3px 0; width:100%; padding:6px; height: 34px !important;}
			
			#meta_data_details_0 .select { margin:3px 0; width:100%; padding:6px; height: 32px !important;}
			
			.blockSpaPackage { width:50% !important; float:left;}
			.blockBookingDetail { width:49% !important;float:right;}
			
			.blockBookingDetail .date { width:46% !important; min-width:0;}
			.blockBookingDetail .time { width:46% !important; min-width:0; float:right;}
			
			.textarea_button { display:none;}
			</style>
			';
	return $text;
}

function search_box_cam_gallery($keyup_action='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){		
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
					    <input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\">
					    <input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
					</div>
             	</div>
             	<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
             		<img src=\"". get_theme_img() ."/loader.gif\"  />
             	</div>
             	";
		    
	if(!empty($keyup_action)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
				
					var s = $(this).val();
					thaUrl = '$keyup_action';
					$('#search_loader').show();
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					jQuery.post(thaUrl,{ 
		                pKEY:'set_search',
		                s:s
			        },function(data){
			        	$('#".$results_id."').html(data);
						$('#search_loader').hide();
			        	//alert(data);
			        });

				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}



function search_box_cam_gallery_categories($keyup_action='',$results_id='',$param='',$pos='left',$class='alert_green',$text='Search'){		
	$searchbox="<div class=\"search_box clearfix\" style=\"float:$pos;\">
					<div class=\"textwrap\">
					    <input type=\"text\" name=\"s\" class=\"searchtext\" value=\"".$text."\" />
					</div>
					<div class=\"buttonwrap\">
					    <input type=\"image\" src=\"". get_theme_img() ."/ico-search.png\" name=\"search\" class=\"searchbutton\" value=\"yes\" />
					</div>
             	</div>
             	<div style=\"float:$pos;margin:10px;display:none;\" id=\"search_loader\">
             		<img src=\"". get_theme_img() ."/loader.gif\"  />
             	</div>
             	";
		    
	if(!empty($keyup_action)){
		$searchbox.="<script type=\"text/javascript\">
			$(function(){
				
				$('.searchtext').keyup(function(){
					//alert('$keyup_action');
					var s = $(this).val();
					thaUrl = '$keyup_action';
					$('#search_loader').show();
					$('#$results_id').html('<div class=".$class.">Searching...</div>');
					jQuery.post(thaUrl,{ 
		                pKEY:'set_search',
		                s:s
			        },function(data){
			        	//alert (data);
			        	$('#".$results_id."').html(data);
						$('#search_loader').hide();
			        	//alert(data);
			        });

				});
				
				
			});
			
			$(function(){
				$('.searchtext').focus(function(){
					$('.searchtext').val('');
				});
			});
			$(function(){
				var search_text='".$text."';
				$('.searchtext').blur(function(){
					$('.searchtext').val($(this).val()==''?search_text:$(this).val());
				});
			});
			</script>";
	}	    
	return $searchbox;
}


function delete_confirmation_box_cam_gallery($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						
						
						
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						//alert(\"$url\");
						//alert(\"$id\");
						
						
						thaUrl = '$url';
						var id = $id;
						jQuery.post(thaUrl,{ 
				                pKEY:'set_delete',
				                val:id
					    },function(data){
					    	//alert(data);
				        });

						
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
						    setTimeout(
						    	function(){
						  			location.reload(true);
		                    	}, 1500);
		                    	
						    return false;
				    	});
			 		});
				";
		$delbox.="</script>";
		
		return $delbox;
}


function delete_confirmation_box_cam_gallery_categories($id,$msg,$url,$close_frameid,$var='',$var_no=''){
		if(empty($var))
			$var="confirm_delete=yes&delete_id=".$id;
		elseif($var=='url')
			$var='';
		else
			$var=$var;
			
		$delbox="<div id=\"delete_confirmation_wrapper_$id\" style=\"display:none;\">";
			$delbox.="<div class=\"fade\"></div>";
			$delbox.="<div class=\"popup_block\">";
				$delbox.="<div class=\"popup\">";
					$delbox.="<div class=\"alert_yellow\">$msg</div>";
					$delbox.="<div style=\"text-align:right;margin:10px 5px 0 0;\">";
						$delbox.="<button type=\"submit\" name=\"confirm_delete\" value=\"yes\" class=\"button\" id=\"delete_yes_".$id."\">Yes</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"no\" class=\"button\" id=\"delete_no_".$id."\">No</button>";
						$delbox.="<button type=\"button\" name=\"confirm_delete\" value=\"cancel\" class=\"button\" id=\"cancel_".$id."\">Cancel</button>";
						$delbox.="<input type=\"hidden\" name=\"delete_id\" value=\"$id\" />";
					$delbox.="</div>";
				$delbox.="</div>";
			$delbox.="</div>";
		$delbox.="</div>";
		
		
		$delbox.="<script type=\"text/javascript\">";
		$delbox.="$(function(){
						$('input[id=delete_".$id."]').click(function(){
							$('#delete_confirmation_wrapper_".$id."').show('fast');
							
						});
					});
			
					$(function(){
						$('a[rel=delete_".$id."]').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#delete_".$id."').click(function(){
							$('select').hide();
							theWidth=document.body.clientWidth;
							theHeight=document.body.clientHeight;
							$('.fade').css('width',theWidth);
							$('.fade').css('height',theHeight);
							$('#delete_confirmation_wrapper_".$id."').show('fast');

						});
					});
					
					$(function(){
						$('#cancel_".$id."').click(function(){
							$('select').show();
						    $('#delete_confirmation_wrapper_".$id."').hide('fast');
						    
						});
					});
			";
			
		if(empty($var_no)){	
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					});
				});";
		}else{
			$delbox.="$(function(){
					$('#delete_no_".$id."').click(function(){
						$('select').show();
						
						
						
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
					    return false;
					});
				});";
		}
		$delbox.="$(function(){
					$('#delete_yes_".$id."').click(function(){
						$('select').show();
						
						//alert(\"$url\");
						//alert(\"$id\");
						
						
						thaUrl = '$url';
						var id = $id;
						jQuery.post(thaUrl,{ 
				                pKEY:'set_delete',
				                val:id
					    },function(data){
					    	//alert(data);
				        });

						
					    $('#delete_confirmation_wrapper_".$id."').hide('fast');
					    $('#".$close_frameid."').css('background','#FF6666');
					    $('#".$close_frameid."').delay(500);
					    $('#".$close_frameid."').fadeOut(700);
						    setTimeout(
						    	function(){
						  			location.reload(true);
		                    	}, 1500);
		                    	
						    return false;
				    	});
			 		});
				";
		$delbox.="</script>";
		
		return $delbox;
}

function gallery_quick_ajax(){
	add_actions('is_use_ajax', true);
	//echo "gallery_quick_ajax";
	global $db;
	if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){
		//echo "gallery_quick_ajax";
		$pic = $_FILES['pic'];
		$file_name = $pic['name'];
		$file_size = $pic['size'];
		//$file_type = $pic['type'];
		$file_type = 'image/'.get_extension($file_name);
		$file_source = $pic['tmp_name'];
                    
		if(is_allow_file_type($file_type,"image")){
			if(is_allow_file_size($file_size)){
				$thetitle = file_name_filter($file_name,false);
				$image_sef=file_name_filter($thetitle."-".time());
                $file_ext=file_name_filter($file_name,true);
                 
                $file_name=$image_sef. $file_ext;

                $destination1=PLUGINS_PATH."/gallery/files/".$file_name;
                $destination2=PLUGINS_PATH."/gallery/files/thumbs/".$file_name;
                $destination3=PLUGINS_PATH."/gallery/files/thumbs2/".$file_name;
                 
                upload_resize($file_source,$destination3,$file_type,thumbnail_image_width(),thumbnail_image_height());
                upload_resize($file_source,$destination2,$file_type,medium_image_width(),medium_image_height());
                //upload_resize($file_source,$destination2,$file_type,900,900);
                upload_resize($file_source,$destination1,$file_type,large_image_width(),large_image_height());
                $theimage = $file_name;
                
			}
		}
		
		$article_id = setCode("lumonata_articles","larticle_id");
        $app_name = 'gallery';
        $thecategory_parent =$_POST['parent'];
        $thetitle =$thetitle;
        $theimage =$theimage;
        $theembed ="";
        $theDescription ="";
        		
		$sql=$db->prepare_query("INSERT INTO lumonata_articles
		(
		larticle_id,larticle_title,larticle_status,larticle_type,lsef,
	 	lorder,lpost_by,lpost_date,larticle_content
		)
		VALUES 
		(
		%d,%s,%s,%s,%s,
		%d,%d,%s,%s
		)",
		$article_id,$thetitle,'publish',$app_name,'',
		1,$_COOKIE['user_id'],date("Y-m-d H:i:s"),$theDescription
		);
		
		
		if(reset_order_id("lumonata_articles")){
			$result = $db->do_query($sql);
		}
		
		if ($result){
			add_additional_field($article_id, 'image', $theimage, $app_name);
			
			$q=$db->prepare_query("Insert Into lumonata_additional_fields (lapp_id,lkey,lvalue,lapp_name) Values (%d,%s,'$theembed',%s)",$article_id,'embed',$app_name);
			$r=$db->do_query($q);
			
			$q=$db->prepare_query("Insert Into lumonata_rule_relationship (lapp_id,lrule_id) Values (%d,%d)",$article_id,$thecategory_parent);
			$r=$db->do_query($q);
			$error='';
			$str='';
			echo json_encode(array('status'=>$str,'error'=>$error));
		}
		
	}
}

function gallery_ajax(){
	add_actions('is_use_ajax', true);
	//echo "spa_reservation_ajax";
	global $db;
	if ($_POST['pKEY']=='set_search'){
		 $sql=$db->prepare_query("select * from lumonata_articles
                where larticle_type=%s And
                (larticle_title like %s)
                order by lorder",'gallery',"%".$_POST['s']."%");
	    	$r=$db->do_query($sql);
            if($db->num_rows($r) > 0){
		    	echo gallery_list($r);
            }else{
                echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
            }
            
	}
	
	if ($_POST['pKEY']=='set_delete'){
		$val = $_POST['val'];
		$app_name = 'gallery';
		delete_additional_field($val, $app_name);
		delete_rules_relationship("app_id=$val");
		delete_article($val,$app_name);
	}
	
	if (isset($_POST['app_id'])){
		$val = $_POST['app_id'];
		$app_name = 'gallery';
		delete_additional_field($val, $app_name);
		delete_rules_relationship("app_id=$val");
		delete_article($val,$app_name);
	}
	
	if ($_POST['pKEY']=='chose_additional_service_price'){
		$acco_type_id = $_POST['val_acco_type_id'];
		$id = $_POST['val_id'];
		$qty = $_POST['val_qty'];
		$day = $_POST['val_day'];
		$duration = '';
		$ex = explode('_', $id);
		$rule_id = $ex[0];
		$article_id = $ex[1];
		$price = 0;
		$pure_spa = false;
		$add = $_POST['val_add'];
		
		$query_c=$db->prepare_query("Select * From lumonata_rules Where lrule_id=%d",$rule_id);
		$result_c=$db->do_query($query_c);
		$data_c=$db->fetch_array($result_c);
		$parent=$data_c['lparent'];
		
		
	
		
		$query_c=$db->prepare_query("Select * From lumonata_rules Where lrule_id=%d",$rule_id);
		$result_c=$db->do_query($query_c);
		$data_c=$db->fetch_array($result_c);
		$parent=$data_c['lparent'];

		
		$query=$db->prepare_query("Select a.* From lumonata_articles a
		Inner Join lumonata_additional_fields af On a.larticle_id = af.lapp_id 
		Where a.larticle_id=%d 
		And a.larticle_status=%s
		And af.lvalue<>%s
		Order by lorder",$article_id,'publish','');
		$result=$db->do_query($query);
		$jum=$db->num_rows($result);
		if (!empty($jum)){
			$data_a=$db->fetch_array($result);
			
			if ($parent==13){
				if (isset($ex[2])){
					$arr_pt = $ex[2];
					
					$price_temp = get_additional_field($data_a['larticle_id'],'additional_service_pure_spa_price','additional_service');
					$time_temp = get_additional_field($data_a['larticle_id'],'additional_service_pure_spa_time','additional_service');
					
					$exp = explode(';', $price_temp);
					$ext = explode(';', $time_temp);
					$jum_exp = count($exp);
					
					
					$detail_package_spa = '';
					if ($jum_exp > 1){
						$jum_exp = $jum_exp - 1;
						for ($ii=0;$ii<$jum_exp;$ii++){
							if ($arr_pt==$ii){
								$price = $exp[$ii];
								$duration = $ext[$ii];
								$pure_spa = true;
								break;
							}
						}
					}
				}
				
			}elseif ($article_id==119){
				$price = get_additional_field($data_a['larticle_id'],'additional_service_price','additional_service');
				$price = $price * $day;
			}else{
				$price = get_additional_field($data_a['larticle_id'],'additional_service_price','additional_service');
			}	
		}
		
		$tot_price = $price * $qty;
		
		
		echo $tot_price;
		
		//echo "M";
	}
	
	if ($_POST['pKEY']=='set_number_format'){
		$val = $_POST['val'];
		$value = number_format($val,2);
		echo $value;
	}
	
	
	
}

function gallery_categories_ajax(){
	add_actions('is_use_ajax', true);
	//echo "spa_reservation_ajax";
	global $db;
	if ($_POST['pKEY']=='set_search'){
		 $sql=$db->prepare_query("select * from lumonata_rules
                where lgroup=%s And 
                (lname like %s or ldescription like %s)
                order by lorder",'gallery',"%".$_POST['s']."%","%".$_POST['s']."%");
	    	$r=$db->do_query($sql);
            if($db->num_rows($r) > 0){
		    	echo gallery_categories_list($r);
            }else{
                echo "<div class=\"alert_yellow_form\">No result found for <em>".$_POST['s']."</em>. Check your spellling or try another terms</div>";
            }
            
	}
	
	if ($_POST['pKEY']=='set_delete'){
		$val = $_POST['val'];
		delete_rule($val);
	}
}

function get_extension($file_name){
	$ext = explode('.', $file_name);
	$ext = array_pop($ext);
	return strtolower($ext);
}




function get_gallery_load_more_js($type="articles"){    	
    	$type = $_GET['state'];
    	$js= "
						<script type=\"text/javascript\">
							$(function(){
								$('.loadMore').live(\"click\",function(){
									var PAGE = $(this).attr(\"id\");
									var TYPE = \"$type\";
									if(PAGE){
										$(\"#loadMore\"+PAGE).html('<img src=\"http://".TEMPLATE_URL."/images/loading.gif\" border=\"0\">');		
										$.ajax({
											type: \"POST\",
											url: \"http://".SITE_URL."/lumonata-admin/?state=".$_GET['state']."&category_name=".(isset($_GET['category_name'])?$_GET['category_name']:'')."\",
											data: \"page=\"+ PAGE + \"&loadMore=\" + TYPE,
											cache: false,
											success: function(html){																						
												$(\"#list_item\").append(html);
												$(\"#loadMore\"+PAGE).remove(); // removing old more button
											}
										});
									}else{
										$(\".morebox\").html('The End');// no results
									}
									return false;
								});
							});
						</script>
          			  ";
    	return $js;
}
function get_gallery_load_more_link($pageID=''){
    	 $link = '
          			<div id="loadMore'.$pageID.'" class="morebox">
						<a href="#" class="loadMore" id="'.$pageID.'" rel="1">Load more...</a>
					</div>          		
          		   ';
    	 return $link;
}
function popup_gallery_delete(){
		 $pop = '   
		 	<div style="display: none;" id="delete_confirmation_wrapper">
			<div class="fade"></div>
			<div class="popup_block">
				<div class="popup">
					<div class="alert_yellow">Are sure want to delete Welcome to Arunna?</div>
					<div style="text-align:right;margin:10px 5px 0 0;">
						<button id="delete_yes" class="button" value="yes" name="confirm_delete" type="submit">Yes</button>
						<button id="delete_no" class="button" value="no" name="confirm_delete" type="button">No</button>
						<button id="delete_cancel" class="button" value="cancel" name="confirm_delete" type="button">Cancel</button>						
					</div>
				</div>
			</div>
			</div> ';
		 echo $pop;
		
}

add_actions('gallery-quick-ajax_page', 'gallery_quick_ajax');
add_actions('gallery-ajax_page', 'gallery_ajax');
add_actions('gallery-categories-ajax_page', 'gallery_categories_ajax');
?>