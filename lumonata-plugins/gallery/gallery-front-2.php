<?php

function front_gallery(){
	global $db;
	$rule_id = 62;

	$q = $db->prepare_query('select d.lvalue image from lumonata_rules a 
									inner join lumonata_rule_relationship b on a.lrule_id=b.lrule_id
									inner join lumonata_articles c on b.lapp_id = c.larticle_id 
									inner join lumonata_additional_fields d on c.larticle_id=d.lapp_id
									where a.lrule_id=%d and c.larticle_type=%s and d.lkey=%s and d.lapp_name=%s
									order by c.lorder
									',$rule_id,'gallery','image','gallery');
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n>0){
		set_template(PLUGINS_PATH."/gallery/template_front_gallery_vedas.html",'template_detail_gallery');
		add_block('item_gallery_nav','item_gallery_navb','template_detail_gallery');
		add_block('item_gallery','item_galleryb','template_detail_gallery');
		add_block('detail_gallery','detail_galleryb','template_detail_gallery');
		$id =  post_to_id();
		$title = get_article_title($id);
		add_variable('title',$title);
		add_variable('meta_title', $title.' - '.trim(web_title()));  
		while ($dt = $db->fetch_array($r)) {
			$image = $dt['image'];
			$sizeSTR = 'w=220&h=220';
			$item_gallery = 'http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;
			$item_gallery_nav = 'http://'.SITE_URL.'/lumonata-content/files/tb/tb.php?'.$sizeSTR.'&src=http://'.SITE_URL.'/lumonata-plugins/gallery/files/'.$image;

			add_variable('src_item_gallery',$item_gallery);
			add_variable('src_item_gallery_nav',$item_gallery_nav);

			parse_template('item_gallery','item_galleryb',true);
			parse_template('item_gallery_nav','item_gallery_navb',true);
		}
		return return_template('detail_gallery','detail_galleryb',false);	
	}
}

?>