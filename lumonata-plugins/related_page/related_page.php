<?php
/*
    Plugin Name: Related page
    Plugin URL: 
    Description: It's plugin  is use to set related page.
    Author: Yana
    Author URL: http://www.dasmara.com
    Version: 1.0.0
    Created Date : 03 Juni 2013
*/

/* PLUGIN NOTE :
 * lapp_name 	: related_page
 * lkey 		: rp_data
 * lvalue 		: array{array{order,type,label,target,link,permalink},array{}} **type : costume_url,pages,articles
 * lapp_id 		: larticle_id
 */

add_privileges('administrator', 'related_page', 'insert');
add_privileges('administrator', 'related_page', 'update');
add_privileges('administrator', 'related_page', 'delete');

add_actions('header_elements','related_page_css');
add_actions('admin_tail','related_page_js');

//action
if(is_save_draft() || is_publish()){	
	related_page_action();	
}
//form
if(is_edit_all() && !(is_save_draft() || is_publish())){
    foreach($_POST['select'] as $index=>$post_id){       
        add_actions('articles_additional_data_'.$index,'additional_data','Related Links','related_page');
        add_actions('page_additional_data_'.$index,'additional_data','Related Links','related_page');        
    }
}else{  		
		add_actions('articles_additional_data','additional_data','Related Links','related_page');
        add_actions('page_additional_data','additional_data','Related Links','related_page');      
       
}

/*------------ALL FUNCTION NEEDED IS HERE------------*/
function related_page(){
    global $thepost;
    $i=$thepost->post_index;
    $post_id=$thepost->post_id;
    if(isset($_POST['related_page'][$i]['link'])){  
    	//ambil current id yg tersimpan
    	//$post_id = get_next_increment_article_id()-1;
    	if (isset($_GET['prc']) && ($_GET['prc']=='edit') && (isset($_GET['id'])) && ($_GET['id']!='')){
    		$post_id = $_GET['id'];
    	}elseif (is_saved() || isset($_GET['prc']) && ($_GET['prc']=='add_new')){    			
    		$post_id = get_next_increment_article_id()-1;
    	}else{
    		$post_id = $post_id;
    	}    	
    	$form  = form_related_page_default($i);
        $form .= '<div id="container_related_page_'.$i.'">'.get_related_page($post_id,$i).'</div>';
        $form .= rp_button_add($i);
        return $form;     
    }else{       
        if(is_edit() || is_edit_all()){        	
			$form  = form_related_page_default($i);
        	$form .= '<div id="container_related_page_'.$i.'">'.get_related_page($post_id,$i).'</div>';
        	$form .= rp_button_add($i);
        	return $form;       
        }else{        	
        	$form  = form_related_page_default($i);
        	$form .= '<div id="container_related_page_'.$i.'"></div>';
        	$form .= rp_button_add($i);
        	return $form;       
        }
    }
}

/*--------------DESIGN-------------*/
function form_related_page_default($i){
	$html = '<div style="display:none;">';
	$html.=	'<div id="hidden_form_rp_pages_'.$i.'">
					<p> <input type="hidden" name="related_page['.$i.'][type][]" value="pages">
						'.rp_link($type='pages',$i).'   
        				'.rp_label($i).'
        				'.rp_target($i).'
        				'.tombol_close_form_rp($i).' 				
        			</p>
        	</div>';
	$html.=	'<div id="hidden_form_rp_articles_'.$i.'">
					<p> <input type="hidden" name="related_page['.$i.'][type][]" value="articles">
						'.rp_link($type='articles',$i).'   
        				'.rp_label($i).'
        				'.rp_target($i).'
        				'.tombol_close_form_rp($i).'        				
        			</p>
        	</div>';
	$html.=	'<div id="hidden_form_rp_costume_url_'.$i.'">
					<p> <input type="hidden" name="related_page['.$i.'][type][]" value="costume_url">
						'.rp_link($type='costume_url',$i).'
        				'.rp_label($i).'
        				'.rp_target($i).'
        				'.tombol_close_form_rp($i).'      				
        			</p>
        	</div>';
	$html.= '</div>';
	return $html;
}
function rp_link($type,$i,$current_value=''){
	global $db;
	$html = '';
	switch($type){
		case 'costume_url':
			$html = '<input type="text" name="related_page['.$i.'][link][]" placeholder="URL" value="'.$current_value.'" style="margin-left:10px;height:24px;width:296px;">';
			break;
		case 'articles':
			$q = $db->prepare_query("SELECT * FROM lumonata_rules WHERE lrule=%s AND lgroup=%s ORDER BY lname ASC",'categories','articles');
			$r = $db->do_query($q);		
			$html = '<select name="related_page['.$i.'][link][]"  style="width:300px;margin-left:10px">';
			while ($d=$db->fetch_array($r)){
				if ($d['lrule_id']==$current_value){
					$html.='<option value="'.$d['lrule_id'].'" selected>'.$d['lname'].'</option>';
				}else{
					$html.='<option value="'.$d['lrule_id'].'">'.$d['lname'].'</option>';
				}
			}
			$html .= '</select>';
			break;
		case 'pages':
			$q = $db->prepare_query("SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC",'pages','publish');
			$r = $db->do_query($q);		
			$html = '<select name="related_page['.$i.'][link][]"  style="width:300px;margin-left:10px">';
			while ($d=$db->fetch_array($r)){
				if ($d['larticle_id']==$current_value){
					$html.='<option value="'.$d['larticle_id'].'" selected>'.$d['larticle_title'].'</option>';
				}else{
					$html.='<option value="'.$d['larticle_id'].'">'.$d['larticle_title'].'</option>';
				}
			}
			$html .= '</select>';
		break;
	}
	return $html;
}
function rp_label($i=0,$current_val=''){
	$html ='<input type="text" value="'.$current_val.'" name="related_page['.$i.'][label][]" placeholder="Label" style="margin-left:10px;height:24px;width:300px;">';
	return $html;
}
function rp_target($i=0,$current_value=''){
	$html = '<select name="related_page['.$i.'][target][]"  style="width:100px;margin-left:10px">';
	$arr_value = array('_self','_blank');
	for($a=0;$a<count($arr_value);$a++){
		if($arr_value[$a]==$current_value){
			$html.='<option value="'.$arr_value[$a].'" selected>'.$arr_value[$a].'</option>';
		}else{
			$html.='<option value="'.$arr_value[$a].'">'.$arr_value[$a].'</option>';
		}
	}
	$html .= '</select>';
	return $html;
}
function tombol_close_form_rp($i=0){
	$html = '<input type="button" value="X" class="button_close_x">';
	return $html;
}
function rp_button_add($i=0){
	$html = '<div class="rp_button_set"> 
			<div class="button-add-rp add_rp_pages" data-index="'.$i.'" data-type="pages">+ Pages</div>
			<div class="button-add-rp add_rp_pages" data-index="'.$i.'" data-type="articles">+ Articles</div>
			<div class="button-add-rp add_rp_pages" data-index="'.$i.'" data-type="costume_url">+ Costume URL</div>
			</div>';
	return $html;
}
function related_page_css(){
	$css = '<style>
			.button_close_x{
				margin-left:10px;
				cursor:pointer;
				font-weight:bold;
				border-radius:12px;
				-moz-border-radius:12px;
				-webkit-border-radius:12px;
				height: 24px;   
    			width: 24px;
				border:1px solid #ccc;
				background-color:#eee;
				vertical-align: middle;
			}
			.button_close_x:hover{
				color:#fff;
				border:1px solid #000;
				background-color:#333;
			}
			.button-add-rp{
				display:inline-block;
				cursor:pointer;
				min-width:60px;
				padding:5px;
				background-color:#eee;
				border:1px solid #ccc;
			}
			.button-add-rp:first-child{
				margin-left:10px;
			}
			</style>';	
	return $css;
}
function related_page_js(){
	$js ='<script>
			$(document).ready(function(){
				//add_form_rp
				$(".add_rp_pages").click(function(){
					var type	= $(this).attr("data-type");
					var index 	= $(this).attr("data-index");					
					add_form_related_page(type,index);
				});				
				function add_form_related_page(type,index){
					var data= "";
					var id 	= "#container_related_page_"+index;
					
					if(type=="pages"){
						data = $("#hidden_form_rp_pages_"+index).html();												
						$(id).append(data);						
					}else if(type=="articles"){
						data = $("#hidden_form_rp_articles_"+index).html();												
						$(id).append(data);						
					}else{
						data = $("#hidden_form_rp_costume_url_"+index).html();												
						$(id).append(data);						
					}
					console.log(data);
					return false;
				}
				
				//close_form_rp				
				$(".button_close_x").live("click", function() {
				    $(this).parent().remove();
				    return false;
				});
			});
		</script>';
	return $js;
}

function get_related_page($id,$i=0){
 	global $db;
 	if ($id==''){
 		return; 		
 	}
 	$q = $db->prepare_query("SELECT * from lumonata_additional_fields WHERE lapp_name=%s AND lkey=%s AND lapp_id=%d",'related_page','rp_data',$id);
 	$r = $db->do_query($q);
 	$n = $db->num_rows($r);
 	if($n==0){
 		return;
 	}else{
 		$d		= $db->fetch_array($r);
 		$data	= json_decode($d['lvalue'],true);
 		
 		if(!is_array($data)){
 			return;
 		}else{ 
 			$html = '';
 			$num_items = count($data); 
 			for($a=0;$a<$num_items;$a++){ 				 	
 				$type = $data[$a]['type'];
 				if ($type=='pages'){
 					$link = str_replace('?page_id=','',$data[$a]['link']);
 				}elseif($type=='articles'){
 					$link = str_replace('?app_name=articles&amp;cat_id=','',$data[$a]['link']);
 					$link = str_replace('?app_name=articles&cat_id=','',$link);
 				}else{
 					$link = $data[$a]['link'];
 				}
 				$label = $data[$a]['label'];
 				$target = $data[$a]['target'];
 							
 				$html.='<p> <input type="hidden" name="related_page['.$i.'][type][]" value="'.$type.'">
						'.rp_link($data[$a]['type'],$i,$link).'   
        				'.rp_label($i,$label).'
        				'.rp_target($i,$target).'
        				'.tombol_close_form_rp($i).' 				
        				</p>';
 			}
 			return $html;			
 		} 		 	
 	} 	
}

/*--------------------ACTION - INSERT - UPDATE - DELETE------------------------*/
function get_next_increment_article_id(){
	global $db;
	$q = $db->prepare_query("select auto_increment from information_schema.TABLES where TABLE_NAME ='lumonata_articles' and TABLE_SCHEMA='".DBNAME."'");
 	$r = $db->do_query($q);
 	$d = $db->fetch_array($r);
 	$id = $d['auto_increment']; 	
 	return $id;
}
function get_link_and_permalink($type,$val){
	global $db;
	switch ($type){
		case 'pages';
			$q = $db->prepare_query("SELECT lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d",$type,$val);
			$r = $db->do_query($q);
			$d = $db->fetch_array($r);
			$arr_link = array('?page_id='.$val.'',$d['lsef']);
			break;
		case 'articles':
			$q = $db->prepare_query("SELECT lsef FROM lumonata_rules WHERE lrule_id=%d",$val);
			$r = $db->do_query($q);
			$d = $db->fetch_array($r);
			$arr_link = array('?app_name=articles&cat_id='.$val.'','articles/'.$d['lsef']);
			break;
		case 'costume_url':
			$arr_link = array($val,$val);
			break;
		default:
			$arr_link = array($val,$val);
			break;
	}	
	return $arr_link;
}
function related_page_action(){	
	$key 		= 'rp_data';
	$app_name 	= 'related_page'; 
	if(is_add_new()){
		$i=0;
		if(!is_saved()){
			$app_id = get_next_increment_article_id();			
            if(isset($_POST['related_page'][$i])){            
            	$num_items = count($_POST['related_page'][$i]['link']);	            	
            	$no=0;
            	for ($a=0;$a<$num_items;$a++){            		
            		//ignore empty label
            		if($_POST['related_page'][$i]['label'][$a]!=''){
            			$arr_link = get_link_and_permalink($_POST['related_page'][$i]['type'][$a],$_POST['related_page'][$i]['link'][$a]);
            			$arr_items[$no] = array('order'=>$no,'type'=>$_POST['related_page'][$i]['type'][$a],'label'=>$_POST['related_page'][$i]['label'][$a],'target'=>$_POST['related_page'][$i]['target'][$a],'link'=>$arr_link[0],'permalink'=>$arr_link[1]);
            			$no++;
            		}		
            	} 
            	if (isset($arr_items)){
            		$val = json_encode($arr_items);
            		add_additional_field($app_id, $key, $val, $app_name);            		
            	}            	          
            }
		}else{
			$app_id = $_POST['post_id'][$i];
            if(isset($_POST['related_page'][$i])){            
            	$num_items = count($_POST['related_page'][$i]['link']);	            	
            	$no=0;
            	for ($a=0;$a<$num_items;$a++){            		
            		//ignore empty label
            		if($_POST['related_page'][$i]['label'][$a]!=''){
            			$arr_link = get_link_and_permalink($_POST['related_page'][$i]['type'][$a],$_POST['related_page'][$i]['link'][$a]);
            			$arr_items[$no] = array('order'=>$no,'type'=>$_POST['related_page'][$i]['type'][$a],'label'=>$_POST['related_page'][$i]['label'][$a],'target'=>$_POST['related_page'][$i]['target'][$a],'link'=>$arr_link[0],'permalink'=>$arr_link[1]);
            			$no++;
            		}		
            	} 
            	if (isset($arr_items)){
            		$val = json_encode($arr_items);
            		edit_additional_field($app_id, $key, $val, $app_name);            		
            	}            	          
            }
		}
	}elseif(is_edit()){
			$i=0;
			$app_id = $_POST['post_id'][$i];
            if(isset($_POST['related_page'][$i])){            
            	$num_items = count($_POST['related_page'][$i]['link']);	            	
            	$no=0;
            	for ($a=0;$a<$num_items;$a++){            		
            		//ignore empty label
            		if($_POST['related_page'][$i]['label'][$a]!=''){
            			$arr_link = get_link_and_permalink($_POST['related_page'][$i]['type'][$a],$_POST['related_page'][$i]['link'][$a]);
            			$arr_items[$no] = array('order'=>$no,'type'=>$_POST['related_page'][$i]['type'][$a],'label'=>$_POST['related_page'][$i]['label'][$a],'target'=>$_POST['related_page'][$i]['target'][$a],'link'=>$arr_link[0],'permalink'=>$arr_link[1]);
            			$no++;
            		}		
            	} 
            	if (isset($arr_items) && !empty($arr_items)){
            		$val = json_encode($arr_items);
            		edit_additional_field($app_id, $key, $val, $app_name);            		
            	}else{
            		delete_additional_field($app_id, $app_name);
            	}            	          
            }
	}elseif(is_edit_all()){
		foreach($_POST['post_id'] as $index=>$value){
			$i=$index;
			$app_id = $value;
            if(isset($_POST['related_page'][$i])){            
            	$num_items = count($_POST['related_page'][$i]['link']);	            	
            	$no=0;
            	for ($a=0;$a<$num_items;$a++){            		
            		//ignore empty label
            		if($_POST['related_page'][$i]['label'][$a]!=''){
            			$arr_link = get_link_and_permalink($_POST['related_page'][$i]['type'][$a],$_POST['related_page'][$i]['link'][$a]);
            			$arr_items[$no] = array('order'=>$no,'type'=>$_POST['related_page'][$i]['type'][$a],'label'=>$_POST['related_page'][$i]['label'][$a],'target'=>$_POST['related_page'][$i]['target'][$a],'link'=>$arr_link[0],'permalink'=>$arr_link[1]);
            			$no++;
            		}		
            	} 
            	if (isset($arr_items) && !empty($arr_items)){
            		$val = json_encode($arr_items);
            		edit_additional_field($app_id, $key, $val, $app_name);            		
            	}else{
            		delete_additional_field($app_id, $app_name);
            	}     
			}
		}
	}	
}
/*--------------------FRONT------------------------*/
get_related_page_front();
function get_related_page_front($app_id=''){
	global $db;			
	if (is_home()){
		$id=9;		
	}elseif ($app_id==''){		
		$id = post_to_id();
	}else{
		$id = $app_id;
	}
	
	if ($id==''){		
		return $arr = array(false,'It is have not related links');
	}
	$value = get_additional_field($id, 'rp_data', 'related_page');	
	if ($value!=''){
		$arr = json_decode($value,true);
		if(is_array($arr) && !empty($arr)){
			$num_items = count($arr);
			$li = '';
			for($a=0;$a<$num_items;$a++){
				$type 	= $arr[$a]['type'];
				$label 	= $arr[$a]['label'];
				$target	= $arr[$a]['target'];
				$link 	= $arr[$a]['link'];
				$perma	= $arr[$a]['permalink'];
				
				switch($type){
					case 'pages':
						if(is_permalink()){
							$link = 'http://'.site_url().'/'.$perma.'/';
						}else{
							$link = 'http://'.site_url().'/'.$link.'/';
						}
						break;
					case 'articles':
						if(is_permalink()){
							$link = 'http://'.site_url().'/'.$perma.'/';
						}else{
							$link = 'http://'.site_url().'/'.$link.'/';
						}
						break;
					default :
						$link = $link;
						break;
				}
					
				$li .= '<li><a title="'.$label.'" href="'.$link.'" target="'.$target.'">'.$label.'</a></li>';
			}
			if ($li!=''){
				$html = '<ul>'.$li.'</ul>';
				return $arr = array(true,$html);	
			}else{
				return $arr = array(false,'It is have not any related link');
			}			
		}	
	}
	return $arr = array(false,'It is have not any related link');
}
?>