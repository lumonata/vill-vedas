// JavaScript Document
jQuery(document).ready(function(e) {
	set_event_reservation();
});

function set_event_reservation(){
	set_timeframe();
	jQuery('#btn_contactUs').click(function(){
		jQuery('.error').removeClass('error');jQuery(".notif-span").text('').removeClass('success-info error-info');
		var cIn = jQuery("[name=cIn]").val();
		var cOut = jQuery("[name=cOut]").val();
		var adult = jQuery("[name=adult]").val();
		var child = jQuery("[name=child]").val();
		
		var name = jQuery("[name=name]").val();	
		var email = jQuery("[name=email]").val();
		var phone = jQuery("[name=phone]").val();
		var subject = jQuery("[name=subject]").val();
		var message = jQuery("[name=message]").val();
		var verCode = jQuery("[name=verCode]").val();
		var error = 0;
		var txt_error = '';
		
		if(cIn==''){
			error++;
			txt_error += 'Check In';
			jQuery("[name=cIn]").addClass('error');
		}
		
		if(cOut==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Check Out';
			jQuery("[name=cOut]").addClass('error');
		}
		
		if(adult==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Adult';
			jQuery("[name=adult]").addClass('error');
		}
		
		if(name==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Name';
			jQuery("[name=name]").addClass('error');
		}
		
		if(email==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Email';
			jQuery("[name=email]").addClass('error');
		}
		
		if(subject==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Subject';
			jQuery("[name=subject]").addClass('error');
		}
		
		if(message==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Message';
			jQuery("[name=message]").addClass('error');
		}
		
		
		
		if(verCode==''){
			error++;
			if(txt_error!='')txt_error +=', ';
			txt_error += 'Capcha';
			jQuery("[name=verCode]").addClass('error');
		}
		var txt_email_error = '';
		if(email!=''){
			if(validateTheEmail(email)==false)	{
				error++;
				if(txt_error!=''){
					txt_email_error += ' and email format is false';
				}else{
					txt_email_error += 'Email format is false';
				}
				jQuery("[name=email]").addClass('error');
			}
		}
		
		if(adult!='' && !jQuery.isNumeric(adult)){
			error++;
			if(txt_error!=''){
				txt_email_error += ' and Adult must numeric';
			}else{
				txt_email_error += 'Adult must numeric';
			}
			jQuery("[name=adult]").addClass('error');
		}
		
		if(child!='' && !jQuery.isNumeric(child)){
			error++;
			if(txt_error!='' && txt_email_error==''){
				txt_email_error += ' and children must numeric.';
			}else if(txt_error!='' && txt_email_error!=''){
				txt_email_error = '. Adult and Children must numeric.';
			}else{
				txt_email_error += (txt_email_error!=''?', ':'')+'Children must numeric';
			}
			jQuery("[name=child]").addClass('error');
		}
		
		
		if(error==0){
			jQuery("#btn_contactUs").val("PLEASE WAIT...");
			var url = 'http://'+jQuery("[name=site_url]").val()+'/simple-reservation-send-ajax';
			var data = new Object();
				data.pKEY = 'is_use_ajax';
				data.cIn 			= cIn;
				data.cOut			= cOut;
				data.adult 		= adult;
				data.child			= child;
				data.name 		= name;
				data.email		= email;
				data.phone		= phone;
				data.subject	= subject;
				data.message	= message;
				data.verCode	= verCode;
			jQuery.post(url,data,function(response){
				var result = jQuery.parseJSON(response);
				if(result.process=='success'){
					jQuery("[name=name],[name=email],[name=phone],[name=subject],[name=message],[name=verCode],[name=cIn],[name=cOut],[name=child],[name=adult]").val('');
					jQuery(".notif-span").text(result.data).addClass('success-info');	
				}else if(result.process=='failed'){
					jQuery(".notif-span").text(result.data).addClass('error-info');	
				}
				jQuery(".chapta").attr('src','http://'+jQuery("[name=site_url]").val()+'/val-image.php');
				jQuery("#btn_contactUs").val("SEND INQUIRY");
			
			});
			//do send
		}else{
			if(txt_error!='')	jQuery(".notif-span").text(txt_error+' is required'+txt_email_error).addClass('error-info');	
			else jQuery(".notif-span").text(txt_email_error).addClass('error-info');	
		}	
	});
	
}

function set_timeframe(){
	if(jQuery('[name=cIn]').length){
		new Timeframe('calendars', {
        startField: 'cIn',
        endField: 'cOut',
        earliest: new Date(),
		format: '%d %b %Y',
        resetButton: 'reset' });
		
		jQuery('#calendars #calendars_menu').before('<div class="just-triangular"></div>')
		
		jQuery('[name=cIn]').click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 1}, 200).removeClass('cin-act cout-act').addClass('show-calendar cin-act');	
			jQuery(".overlay-calendar").fadeIn(80);
		});
		
		jQuery('[name=cOut]').click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 1}, 200).removeClass('cin-act cout-act').addClass('show-calendar cout-act');	
			jQuery(".overlay-calendar").fadeIn(80);
		});
		
		jQuery(".overlay-calendar").click(function(){
			jQuery('#calendars').css({opacity: 1.0, visibility: "visible"}).animate({opacity: 0}, 200).removeClass('show-calendar');
					jQuery(".wrap-src-home").css('z-index',100);
			jQuery(this).fadeOut(80);
		});
		
	}	
}

function trim(str) {
    var	str = str.replace(/^\s\s*/, ''),
            ws = /\s/,
            i = str.length;
    while (ws.test(str.charAt(--i)));
    return str.slice(0, i + 1);
}

function validateTheEmail(address) {
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var address;
    if(reg.test(address) == false) {
       return false;
    }else{
       return true;
    }
}