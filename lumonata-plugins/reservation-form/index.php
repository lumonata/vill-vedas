<?php 
/*
    Plugin Name: Reservation Form
    Plugin URL: 
    Description: It's plugin  is use for Reservation only send email to admin
    Author: Adi
    Author URL: http://lumonata.com/about-us
    Version: 1.0.0
    Created Date : 26 March 2015
*/

function simple_reservation_form(){
	session_start();
	add_variable('script_reservation_form',"<script src=\"http://".SITE_URL."/lumonata-plugins/reservation-form/js/script.js\" type=\"text/javascript\"></script>");
	set_template(PLUGINS_PATH."/reservation-form/template.html",'template_reservation_form');
	add_block('reservation_form_block','reser_form_block','template_reservation_form');
	$id =  post_to_id();
	$title = get_article_title($id);
	add_variable('title',$title);
	add_variable('meta_title', $title.' - '.trim(web_title()));  
	return return_template('reservation_form_block','reser_form_block',false);	
}

function simple_reservation_send(){
	global $db;
	session_start();
	$cIn = $_POST['cIn'];
	$cOut = $_POST['cOut'];
	$adult = $_POST['adult'];
	$child = $_POST['child'];
	
	$name 		= $_POST['name'];
	$email 		= $_POST['email'];
	$subject 	= $_POST['subject'];
	$phone 		= $_POST['phone'];
	$message 	= $_POST['message'];
	$verCode 	= $_POST['verCode'];
	$return = array();
	//print_r($_SESSION);
	if($verCode == $_SESSION['rand_code']){
		$message = 	content_message_reservation_form();
		require_once(ROOT_PATH.'/lumonata-plugins/reservation/class.email_reservation.php');
		$email_reservation = new email_reservation();
		$content_email = $email_reservation->set_content_to_main_email_template($subject,$message);
		//echo $content_email; exit;
		$data_contact = data_tabel('lumonata_articles',"where lsef='contact-us' and larticle_type='contact-us'",'array');
		$post_id = $data_contact['larticle_id'];
		$article_type = $data_contact['larticle_type'];
		$contact_us_email = get_additional_field($post_id, 'contact_us_email', $article_type);
		$contact_us_email_cc = get_additional_field($post_id, 'contact_us_cc', $article_type);
		
		$data_smtp = get_smtp_info();
		$email_smtp = $data_smtp['email'];
		$pass_smtp = $data_smtp['pass'];
		
		$web_name = get_meta_data('web_title');
		$SMTP_SERVER = get_meta_data('smtp');
		require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.phpmailer.php');
		require_once(ROOT_PATH.'/lumonata-functions/phpMailer/class.smtp.php');
		$mail = new PHPMailer(true);
		
		try {
			$mail->SMTPDebug  = 1;
			$mail->IsSMTP();
			$mail->Host = $SMTP_SERVER;
			$mail->SMTPAuth = true;
			$mail->Port       = 2525; 
			$mail->Username   = $email_smtp;
			$mail->Password   = $pass_smtp;
			$mail->AddReplyTo($email, $name);
			$mail->AddAddress($contact_us_email,$web_name);
			if($contact_us_email_cc!=''){
				$mail->AddAddress($contact_us_email_cc,$web_name);
			}
			$mail->SetFrom($email_smtp);
			$mail->Subject = $subject;
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($content_email);
			$mail->Send();
			$return['process'] = 'success';
			$return['data'] = 'Message sent successfully. We will reply shortly.';
		}catch (phpmailerException $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
		}catch (Exception $e) {
			$return['process'] = 'failed';
			$return['data'] = 'Send email failed please try again later';
		}
		//echo $content_email; exit;
		//echo 'capcha is oke';
	}else{
		$return['process'] = 'failed';
		$return['data'] = 'Capcha code is not match';	
	}
	echo json_encode($return);	
}

function content_message_reservation_form(){
	set_template(PLUGINS_PATH."/reservation-form/mail.html",'the_mail_reservation_form');
	add_block('Bmail_reservation_form','bm_rm','the_mail_reservation_form');
	
	add_variable('cIn',$_POST['cIn']);
	add_variable('cOut',$_POST['cOut']);
	add_variable('adult',$_POST['adult']);
	add_variable('child',(trim($_POST['child']) ==''? 0 : $_POST['child']));
	add_variable('name',$_POST['name']);
	add_variable('email',$_POST['email']);
	add_variable('phone',$_POST['phone']);
	add_variable('message', nl2br($_POST['message']));
	
	
	parse_template('Bmail_reservation_form','bm_rm',false);
	$theMailContent = return_template('the_mail_reservation_form');
	return $theMailContent;
}


add_actions('simple-reservation-send-ajax_page', 'simple_reservation_send');

?>