<?php 
/*
    Plugin Name: Testimonail Front
    Plugin URL: http://www.lumonata.com/
    Description: Show article category testimonail to front page
    Author: Adi Juliartha
    Author URL: http://www.lumonata.com/about-us/
    Version: 1.0
*/

function front_footer_testimonial(){
	global $db;
	
	set_template(PLUGINS_PATH."/testimonial_front/template.html",'template_tf');
	add_block('List_Testimonial','lt','template_tf');
	add_block('List_Testimonial_Mobile','ltm','template_tf');
	add_block('B_Testimonial','bt','template_tf'); 
	
	$data_category 	= data_tabel('lumonata_rules',"where lname='Testimonials' and lrule='categories' and lgroup='articles'",'array');
	$category_id 	= $data_category['lrule_id']; 
	
	$str = $db->prepare_query("SELECT c.larticle_id, c.larticle_title, c.larticle_content
							  FROM lumonata_rule_relationship as a,lumonata_rules as b,lumonata_articles as c
							  where  a.lrule_id=b.lrule_id and b.lrule = %s and b.lgroup = %s and 
							  a.lapp_id = c.larticle_id and a.lrule_id=%d
							  order by rand() limit 3",'categories','articles',$category_id);
							  
							  
	$result = $db->do_query($str);						  
	$num = $db->num_rows($result);
	if($num>0){
		$list_num= 0;
		while($data_testimonial = $db->fetch_array($result)){
			add_variable('title',$data_testimonial['larticle_title']);
			$content = str_replace('"','',$data_testimonial['larticle_content']);
			//$content = strip_tags($content,"<p>");
			//echo $content.'#';
			add_variable('content',$content);			
			parse_template('List_Testimonial','lt',true);
			
			if($list_num==0)add_variable('active','active');
			else add_variable('active','');
			parse_template('List_Testimonial_Mobile','ltm',true);
			$list_num++;
			
		}
		return return_template('B_Testimonial','bt',false);		
	}else{
		return "<div class=\"not-found-testimonial\"></div>";
	}
	
	
	//print_r($data_category);
	
	
}

?>