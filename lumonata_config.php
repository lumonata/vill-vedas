<?	
	/*
		Define the database setting here
	*/	
	
	/*MySQL Hostname*/
	define('HOSTNAME','localhost');
	/*MySQL Database User Name*/
	define('DBUSER','root');
	/*MySQL Database Password*/
	define('DBPASSWORD','');
	/*MySQL Database Name*/
	define('DBNAME','villa_vedas');
		
	define('ERR_DEBUG',true);
				
	if(!defined('ROOT_PATH'))
		define('ROOT_PATH',dirname(__FILE__));
		
	require_once(ROOT_PATH."/lumonata-functions/error_handler.php");			
	require_once(ROOT_PATH."/lumonata-classes/db.php");
?>